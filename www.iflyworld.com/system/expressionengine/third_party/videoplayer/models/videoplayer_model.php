<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_model extends CI_Model {

	var $default_accounts = array(
			'youtube' => array(
				'enabled' => false,
				'is_authenticated' => false,
				'authsub_session_token' => false,
			),
			'vimeo' => array(
				'enabled' => false,
				'is_authenticated' => false,
				'oauth_access_token' => false,
				'oauth_access_token_secret' => false,
			)
		);
	/**
	 * Constructor
	 *
	 */
	function __construct()
	{
		parent::__construct();
		
		$this->site_id = $this->config->item('site_id');
	}

	public function get_accounts($site_id = false)
	{
		if(!$site_id){
			$site_id = $this->site_id;
		}
		
		$this->db->where('site_id', $site_id);
		$query = $this->db->get('videoplayer_accounts');

		$accounts = $query->result();

		return $accounts;
	}

	public function get_account($service, $site_id = false)
	{
		if($this->db->table_exists('videoplayer_accounts'))
		{
			if(!$site_id){
				$site_id = $this->site_id;
			}

			$this->db->where('service', $service);
			$this->db->where('site_id', $site_id);

			$query = $this->db->get('videoplayer_accounts');

			if($query->num_rows() > 0)
			{
			   	return $query->row();
			}
		}

		return $this->account_default_row();
	}

	public function account_default_row($site_id = false)
	{
		if(!$site_id){
			$site_id = $this->site_id;
		}
		
		$fields = $this->db->field_data('videoplayer_accounts');

		$row = new stdClass();
		
		foreach($fields as $field)
		{
			switch($field->name)
			{
				case 'order':
				case 'enabled':
				case 'is_authenticated':
				$row->{$field->name} = 0;
				break;
				
				default:
				$row->{$field->name} = $field->default;	
			}			
		}
		
		$row->site_id = $site_id;

		return $row;
	}

	public function save_account($service, $data, $site_id = false)
	{	
		if(!$site_id){
			$site_id = $this->site_id;
		}
		
		if(isset($data->id))
		{
			unset($data->id);
		}
		
		$this->db->where('service', $service);
		$this->db->where('site_id', $site_id);

		$query = $this->db->get('videoplayer_accounts');

		if($query->num_rows() > 0)
		{
			$this->db->where('service', $service);
			$this->db->where('site_id', $site_id);
			$this->db->update('videoplayer_accounts', $data);
		}
		else
		{
			// account doesn't exist
			if(is_object($data))
			{
				$data->service = $service;
				$data->site_id = $this->site_id;
			}
			else
			{
				$data['service'] = $service;
				$data['site_id'] = $this->site_id;
			}

			$this->db->insert('videoplayer_accounts', $data);
		}
	}
}

// END Videoplayer_model class

/* End of file videoplayer_model.php */
/* Location: ./system/expressionengine/third_party/videoplayer/models/videoplayer_model.php */