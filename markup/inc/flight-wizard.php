				<article class="box">

					<a href="#" class="photo"><img src="images/img03.jpg" width="185" height="158" alt="image description"></a>
					<div class="description">
                    	<h1>Flight Wizard</h1>
						<p>When do you want to fly?<br />Find flight packages &amp; times.</p>
						<form action="#" class="uniform quickbook">
							<fieldset>
                                <div class="row">
                                <?php include ('inc/form-elements/select-tunnel-locations.php'); ?>
                                </div>
								<div class="row">
									<a href="#" class="btn date" id="book-date" title="What day for the reservation" ><em></em><span>FLIGHT DATE</span></a>
                                    <div id="calendar-quickbook"></div>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
								</div>
								<div class="row">
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
								</div>
								<div class="row">
									<input type="submit" value="Find Flights" class="find-flights">
									<a href="#" class="btn"><em></em><span>MORE INFO</span></a>
								</div>
							</fieldset>
						</form>
					</div>
				</article><!-- /.box -->