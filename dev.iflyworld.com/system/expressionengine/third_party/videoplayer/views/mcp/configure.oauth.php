<h4><?php echo $this->lang->line('configure'); ?> <?php echo ucwords($service_key)?></h4>
<p><?php echo $this->lang->line('connect_your_ee_to'); ?> <?php echo ucwords($service_key)?></p>
<p><a href="<?php echo $helper->authentication_url('oauth', $service_key)?>" class="videoplayer-btn submit"><?php echo $this->lang->line('connect_to'); ?> <?php echo ucwords($service_key)?></a></p>
