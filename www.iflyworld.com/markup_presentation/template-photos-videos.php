<?php include("inc/head.php"); ?>
<body class="photo-video info-page">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">
			<div class="content-wrapper clearfix">
				<section class=" clearfix">
			<h1 class="uppercase">Find Your Photos and Videos</h1>
						<form action="#" class="find-photovid">
							<fieldset>
                                <div class="row">
                                <select name="TUNNEL LOCATION" title="Select your tunnel location" id="flyer-location" class="locations">
                                    <option>iFLY Hollywood</option>
                                    <option>Hollywood, CA</option>
                                    <option>Orlando, FL</option>
                                    <option>Seattle, WA</option>
                                    <option>Ogden, UT</option>
                                    <option>San Francisco, CA</option>
                                    <option>Find the closest location</option>
                                    <option selected="selected">FLIGHT LOCATION</option>
                                </select>
									<input type="text" name="name" placeholder="Enter Reservation #" class=""><span>or</span>
									<a href="#" class="btn date" id="book-date" title="Find the day that you flew" ><em></em><span>DATE</span></a>
                                    <div id="calendar-quickbook"></div>
                                <select name="FLIGHT TIME" title="Find the time that you flew" id="photovid-flight-time" class="">
                                    <option>9:00 AM</option>
                                    <option>10:00 AM</option>
                                    <option>11:00 AM</option>
                                    <option>12:00 PM</option>
                                    <option>1:00 PM</option>
                                    <option>2:00 PM</option>
                                    <option>3:00 PM</option>
                                    <option>4:00 PM</option>
                                    <option>5:00 PM</option>
                                    <option>6:00 PM</option>
                                    <option>7:00 PM</option>
                                    <option>8:00 PM</option>
                                    <option>9:00 PM</option>
                                    <option>10:00 PM</option>
                                    <option>11:00 PM</option>
                                    <option>12:00 AM</option>
                                    <option selected="selected">TIME</option>
                                </select>
								</div>
                                <div class="row"><a href="#" class="btn red"><em></em><span>PHOTOS</span></a><a href="#" class="btn black"><em></em><span>VIDEOS</span></a></div>
							</fieldset>
						</form>

			<!--<img src="images/404.jpg" alt="404 error"  class="rounded-corners gradient-border">-->
			</section></div>
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<script type="text/javascript" src="js/photovid.js"></script>
	
</body>
</html>