<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// include base class
if ( ! class_exists('Low_search_base'))
{
	require_once(PATH_THIRD.'low_search/base.low_search.php');
}

/**
 * Low Search Module class
 *
 * @package        low_search
 * @author         Lodewijk Schutte <hi@gotolow.com>
 * @link           http://gotolow.com/addons/low-search
 * @copyright      Copyright (c) 2012, Low
 */
class Low_search extends Low_search_base {

	// --------------------------------------------------------------------
	// PROPERTIES
	// --------------------------------------------------------------------

	/**
	 * Query Parameters
	 *
	 * @access      private
	 * @var         array
	 */
	private $params = array();

	/**
	 * Collection ids to limit the search by
	 * - Empty array for all collections
	 *
	 * @access      private
	 * @var         array
	 */
	private $collection_ids = array();

	/**
	 * Site ids to limit the search by
	 * - Empty array for all sites
	 *
	 * @access      private
	 * @var         array
	 */
	private $site_ids = array();

	/**
	 * Entry ids to limit the search by:
	 * - FALSE for no limit
	 * - empty array to trigger no_results
	 *
	 * @access      private
	 * @var         mixed
	 */
	private $entry_ids = FALSE;

	// --------------------------------------------------------------------
	// METHODS
	// --------------------------------------------------------------------

	/**
	 * Show search form
	 *
	 * @access      public
	 * @return      string
	 */
	public function form()
	{
		// --------------------------------------
		// Load up language file
		// --------------------------------------

		$this->EE->lang->loadfile($this->package);

		// --------------------------------------
		// Initiate data array for form creation
		// --------------------------------------

		$data = array(
			'id'     => $this->EE->TMPL->fetch_param('form_id'),
			'class'  => $this->EE->TMPL->fetch_param('form_class'),
			'secure' => ($this->EE->TMPL->fetch_param('secure', 'yes') == 'yes')
		);

		// --------------------------------------
		// Compose parameter array to add to hidden fields
		// --------------------------------------

		// @TODO: Allow all other params, too?
		$native_params = array('site');

		foreach (array_merge($this->custom_params, $native_params) AS $key)
		{
			$this->params[$key] = $this->EE->TMPL->fetch_param($key, '');
		}

		// --------------------------------------
		// Define default hidden fields
		// --------------------------------------

		$data['hidden_fields'] = array(
			'ACT'    => $this->EE->functions->fetch_action_id($this->class_name, 'catch_search'),
			'params' => $this->encode(array_filter($this->params))
		);

		// --------------------------------------
		// Check for existing query
		// --------------------------------------

		if ($query = $this->decode($this->EE->TMPL->fetch_param('query')))
		{
			$this->_log('form tag - valid query found');

			// Query overrides given parameters
			$this->params = array_merge($this->params, $query);
		}
		else
		{
			$this->_log('form tag - no or invalid query given');
		}

		// --------------------------------------
		// Prep params for template variables
		// --------------------------------------

		$vars = array();

		foreach ($this->params AS $key => $val)
		{
			// Be on the safe side
			if ( ! is_string($val)) continue;

			$vars[$this->prefix.$key.':raw'] = $val;
			$vars[$this->prefix.$key] = low_format($val);

			// Backward compatibility; non-prefixed vars
			if (in_array($key, $this->custom_params))
			{
				$vars[$key] = $vars[$this->prefix.$key];
			}
		}

		// --------------------------------------
		// Get search collections for this site
		// --------------------------------------

		// Check to see whether we actually need to get collections
		$get_collections    = FALSE;
		$collections        = array();
		$active_collections = array();

		// Get them only if the var pair exists
		foreach ($this->EE->TMPL->var_pair AS $key => $val)
		{
			// That's the one
			if (substr($key, 0, 11) == 'collections')
			{
				$this->_log('collections variable pair found');

				$get_collections = TRUE;

				// Is the 'show' parameter set?
				// If so, get the ids
				if (isset($val['show']))
				{
					$get_collections = $val['show'];
				}
			}
		}

		// Get the collections if necessary
		if ($get_collections !== FALSE)
		{
			$this->_log('getting search collection details');

			// --------------------------------------
			// Possibly limit by ids
			// --------------------------------------

			$collections = $this->_filter_collections('site_id', array_values($this->EE->TMPL->site_ids));

			if ($get_collections !== TRUE)
			{
				list($show, $in) = low_explode_param($get_collections);
				$key = low_array_is_numeric($show) ? 'collection_id' : 'collection_name';
				$collections = $this->_filter_collections($key, $show, $in, $collections);
			}

			// --------------------------------------
			// Define collection meta data
			// --------------------------------------

			$meta = array(
				'collection_count'  => 0,
				'total_collections' => count($collections)
			);

			// --------------------------------------
			// Get array of active collections
			// --------------------------------------

			if ( ! empty($this->params['collection']))
			{
				if (is_string($this->params['collection']))
				{
					list($active_collections, $in) = low_explode_param($this->params['collection']);
				}
				elseif (is_array($this->params['collection']))
				{
					$active_collections = $this->params['collection'];
				}
			}

			// --------------------------------------
			// Loop thru collections, modify rows
			// --------------------------------------

			// Numeric collections?
			$attr = low_array_is_numeric($active_collections) ? 'collection_id' : 'collection_name';

			foreach ($collections AS &$row)
			{
				// Forget some
				unset($row['site_id'], $row['settings']);

				// Make strings html-safe
				$row = array_map('htmlspecialchars', $row);

				// Is collection selected?
				$row['collection_is_active'] = in_array($row[$attr], $active_collections) ? 'y' : '';

				// Increment collection count
				$meta['collection_count']++;

				// Merge meta with row
				$row = array_merge($row, $meta);
			}
		}

		// --------------------------------------
		// Add collections to vars array
		// --------------------------------------

		$vars['collections'] = $vars[$this->prefix.'collections'] = $collections;

		// --------------------------------------
		// Add additional vars to vars array
		// --------------------------------------

		$vars['error_message'] = lang($this->EE->session->flashdata('error_message'));

		// --------------------------------------
		// Parse it now
		// --------------------------------------

		$tagdata = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $vars);
		$tagdata = $this->_post_parse($tagdata);

		// --------------------------------------
		// Return output: parsed tagdata wrapped around form
		// --------------------------------------

		return $this->EE->functions->form_declaration($data)
			 . $tagdata
			 . '</form>';
	}

	// --------------------------------------------------------------------

	/**
	 * Show search results
	 *
	 * @access      public
	 * @return      string
	 */
	public function results()
	{
		// --------------------------------------
		// Avoid no_results conflict
		// --------------------------------------

		$this->_prep_no_results();

		// --------------------------------------
		// If query parameter is set but empty or invalid,
		// show no_results and abort
		// --------------------------------------

		$query = $this->EE->TMPL->fetch_param('query');
		$this->params = $this->decode($query);

		// If query="" param was set, but invalid
		if ($query !== FALSE && empty($this->params))
		{
			$what = (strlen($query)) ? 'invalid' : 'empty';
			$this->_log("Query parameter set, but {$what}");
			return $this->EE->TMPL->no_results();
		}

		// Make sure params are still an array
		if ( ! is_array($this->params))
		{
			$this->params = array();
		}

		// --------------------------------------
		// Consolidate all parameters
		// --------------------------------------

		$this->params = array_merge($this->params, $this->EE->TMPL->tagparams);

		// -------------------------------------
		// 'low_search_pre_search' hook.
		//  - Do something just before the search is executed
		// -------------------------------------

		if ($this->EE->extensions->active_hook('low_search_pre_search') === TRUE)
		{
			$this->params = $this->EE->extensions->call('low_search_pre_search', $this->params);
			if ($this->EE->extensions->end_script === TRUE) return $this->EE->TMPL->tagdata;
		}

		// --------------------------------------
		// Make sure these params are set
		// --------------------------------------

		foreach ($this->custom_params AS $key)
		{
			if ( ! array_key_exists($key, $this->params))
			{
				$this->params[$key] = NULL;
			}
		}

		// --------------------------------------
		// Check validity of search mode
		// --------------------------------------

		if ( ! in_array($this->params['search_mode'], $this->search_modes()))
		{
			$this->params['search_mode'] = $this->get_settings('default_search_mode');
		}

		// --------------------------------------
		// Check minimum score
		// --------------------------------------

		if ($this->params['min_score'])
		{
			if ($include_min_score = (substr($this->params['min_score'], 0, 1) == '='))
			{
				$this->params['min_score'] = trim($this->params['min_score'], '=');
			}

			// Force proper value
			$this->params['min_score'] = floatval($this->params['min_score']);
		}

		// --------------------------------------
		// Ranges and Relationships - will modify $this->entry_ids
		// Collection IDs - will modify $this->collection_ids
		// --------------------------------------

		$this->_get_collection_ids();
		$this->_get_site_ids();
		$this->_get_ranges();
		$this->_get_relationships();

		// --------------------------------------
		// Check the entry ids array
		// --------------------------------------

		if (is_array($this->entry_ids))
		{
			// Clean up
			$this->entry_ids = array_unique(array_filter($this->entry_ids));

			// If empty after cleanup, return no results
			if (empty($this->entry_ids))
			{
				$this->_log('Empty set of entry ids given, returning no results');
				return $this->EE->TMPL->no_results();
			}

			$this->_log('Limiting search by entry ids');
		}

		// --------------------------------------
		// Analyze keywords
		// --------------------------------------

		$raw_keywords = (string) @$this->params['keywords'];

		// --------------------------------------
		// Handle the keywords just like the index_text
		// --------------------------------------

		$clean_keywords = low_clean_string($raw_keywords, $this->get_settings('ignore_words'));
		$raw_keywords   = low_clean_string($raw_keywords);

		// --------------------------------------
		// Only perform actual search if keywords are given
		// --------------------------------------

		$do_search = strlen($clean_keywords) ? TRUE : FALSE;

		// --------------------------------------
		// Are we searching?
		// --------------------------------------

		if ($do_search)
		{
			// --------------------------------------
			// Analyse keywords for stop words
			// --------------------------------------

			$use_fulltext = TRUE;

			if ($this->params['loose_ends'] == 'both')
			{
				// Substring search forces fallback method
				$use_fulltext = FALSE;
			}
			else
			{
				// Get stop words from extension
				if ($stop_words = $this->get_settings('stop_words'))
				{
					// Clean stopwords and convert into array
					$stop_words = array_unique(preg_split('/\s+/', low_clean_string($stop_words)));
				}
				else
				{
					// If none, make it an empty array for below code
					$stop_words = array();
				}

				// Check if the keywords contain stop words
				// or fall below the minimum word length threshold
				// if so, don't use fulltext search
				foreach (explode(' ', $clean_keywords) AS $word)
				{
					$word_length = function_exists('mb_strlen') ? mb_strlen($word) : strlen($word);

					if ($word_length < $this->get_settings('min_word_length') OR in_array($word, $stop_words))
					{
						$use_fulltext = FALSE;
						break;
					}
				}
			}

			// --------------------------------------
			// Begin composing query
			// --------------------------------------

			if ($use_fulltext)
			{
				// Create fulltext keywords based on clean keywords
				switch ($this->params['search_mode'])
				{
					case 'exact':
						$ft_keywords = '"'.$raw_keywords.'"';
					break;

					case 'all':
						$ft_keywords = '+'.str_replace(' ', ' +', $clean_keywords);
					break;

					default:
					case 'any':
						$ft_keywords = $clean_keywords;
					break;
				}

				// Account for loose ends
				if ($this->params['search_mode'] != 'exact' && $this->params['loose_ends'])
				{
					$ft_keywords = str_replace(' ', '* ', $ft_keywords).'*';
				}
			}

			$this->EE->db->select(($use_fulltext
				? "entry_id, collection_id, MATCH(index_text) AGAINST('{$clean_keywords}') AS score"
				: 'entry_id, collection_id, index_text'), FALSE)
				->from($this->EE->low_search_index_model->table());

			// --------------------------------------
			// Filters used by both searches
			// --------------------------------------

			// Multiple keywords and/or
			$andor = ($this->params['search_mode'] == 'all') ? 'AND' : 'OR';

			// Limit query by collection
			if ($this->collection_ids)
			{
				$this->EE->db->where_in('collection_id', $this->collection_ids);
			}

			// Limit query by site
			if ($this->site_ids)
			{
				$this->EE->db->where_in('site_id', array_values($this->site_ids));
			}

			// Limit by exact match
			if ($this->params['search_mode'] == 'exact' && ! $use_fulltext)
			{
				$this->_where_exact($raw_keywords);
			}

			// Account for quoted keywords
			if ($this->params['search_mode'] != 'exact' && preg_match_all('#"(.*?)"#', $this->params['keywords'], $matches))
			{
				$this->_where_exact($matches[1], $andor);
			}

			// If entry ids were set, limit it to those
			if (is_array($this->entry_ids) && ! empty($this->entry_ids))
			{
				$this->EE->db->where_in('entry_id', $this->entry_ids);
			}

			if ($use_fulltext)
			{
				// Actual fulltext search
				$this->EE->db->where("MATCH(index_text) AGAINST('{$ft_keywords}' IN BOOLEAN MODE)", NULL, FALSE);
				$this->EE->db->order_by('score', 'desc');

				// Limit by min_score
				if (isset($this->params['min_score']))
				{
					$oper = $include_min_score ? '>=' : '>';
					$this->EE->db->having("score {$oper}", $this->params['min_score']);
				}
			}
			else
			{
				// Fallback search
				if ($this->params['search_mode'] != 'exact')
				{
					$this->_where_exact(explode(' ', $clean_keywords), $andor, $this->params['loose_ends']);
				}
			}

			// --------------------------------------
			// Perform the search
			// --------------------------------------

			$this->_log('starting search '.($use_fulltext ? '(fulltext)' : '(fallback)'));
			$query = $this->EE->db->get();

			// --------------------------------------
			// If the search had no results, return no results bit
			// --------------------------------------

			if ($query->num_rows == 0)
			{
				$this->_log('Searched but found nothing. Returning no results.');
				return $this->EE->TMPL->no_results();
			}

			// --------------------------------------
			// If we do have results, continue
			// --------------------------------------

			if ($use_fulltext)
			{
				// Get results like $results[entry_id] = array(entry_id, collection_id, score)
				$results = low_associate_results($query->result_array(), 'entry_id');
			}
			else
			{
				// Calculate scores ourselves
				$results = array();

				// --------------------------------------
				// Loop thru results, calculate score
				// based on total words / word count
				// --------------------------------------

				$this->_log('Calculating relevance score');

				foreach ($query->result() AS $row)
				{
					// Calculate score
					$score = 0;
					$keyword_array = explode(' ', $clean_keywords);

					// Check occurrence of each word in index_text
					// Added score is number of occurrences / total words * 10
					foreach ($keyword_array AS $word)
					{
						// Account for loose ends
						if ($this->params['loose_ends'] === 'both')
						{
							$found = substr_count($row->index_text, $word);
						}
						else
						{
							$word  = preg_quote($word) . ($this->params['loose_ends'] ? '.*?' : '');
							$found = preg_match_all("/\s{$word}\s/", $row->index_text, $matches);
						}

						if ($found)
						{
							// Removes weight
							$text = preg_replace('/^\|(.+?)\|.*$/m', '$1', $row->index_text);

							// Safe word count
							$word_count = count(explode(' ', trim($text)));

							// Add score
							$score += ($found / $word_count) * 100;
						}
					}

					// End score is cumulative score / number of keywords
					$score = $score / count($keyword_array);

					// Skip entries that fall below the threshold
					if (isset($this->params['min_score']))
					{
						if (($include_min_score && $score < $this->params['min_score']) ||
							( ! $include_min_score && $score <= $this->params['min_score'])) continue;
					}

					// Add row to results only if the entry doesn't exist yet
					// or if existing score is lower than this one
					if ( ! array_key_exists($row->entry_id, $results) || $results[$row->entry_id]['score'] < $score)
					{
						$results[$row->entry_id] = array(
							'entry_id'      => $row->entry_id,
							'collection_id' => $row->collection_id,
							'score'         => $score
						);
					}
				}
			}

			// Clean up
			unset($query);

			// --------------------------------------
			// Check existing entry_id parameter
			// --------------------------------------

			if ( ! empty($this->params['entry_id']))
			{
				$this->_log('entry_id parameter found, filtering search results accordingly');

				// Get the parameter value
				list($ids, $in) = low_explode_param($this->params['entry_id']);

				// Either remove $ids from $entry_ids OR limit $entry_ids to $ids
				$method = $in ? 'array_intersect' : 'array_diff';

				// Get list of entry ids that should be listed
				$entry_ids = $method(array_keys($results), $ids);

				// Get rid of unlisted references in $results
				foreach (array_diff(array_keys($results), $entry_ids) AS $entry_id)
				{
					unset($results[$entry_id]);
				}
			}

			// --------------------------------------
			// If we have results, put them in the tagparam
			// --------------------------------------

			if ( ! empty($results))
			{
				// --------------------------------------
				// Make sure collections are registered to cache
				// --------------------------------------

				$collection_ids = array_unique(low_flatten_results($results, 'collection_id'));
				$collections    = $this->_filter_collections('collection_id', $collection_ids);
				$collections    = low_associate_results($collections, 'collection_id');

				// --------------------------------------
				// Modify scores for each collection
				// --------------------------------------

				$modifiers = array_unique(low_flatten_results($collections, 'modifier'));

				if ( ! (count($modifiers) == 1 && $modifiers[0] == 1.0))
				{
					$this->_log('Applying collection modifier to search results');

					foreach ($results AS &$row)
					{
						if ($mod = (float) $collections[$row['collection_id']]['modifier'])
						{
							$row['score'] = $row['score'] * $mod;
						}
					}

					// Re-sort
					uasort($results, 'low_by_score');
				}

				// --------------------------------------
				// Add results to cache, so extension can look this up
				// --------------------------------------

				$this->_log('Caching search results');
				low_set_cache($this->package, 'results', $results);

				// --------------------------------------
				// Is there a custom sort order?
				// If so, set the entry_id param instead of the fixed_order
				// --------------------------------------

				// Check if orderby is given in posted parameters
				$orderby = ( ! empty($this->params['orderby']))
				         ? (string) $this->params['orderby']
				         : $this->EE->TMPL->fetch_param('orderby', 'low_search_score');

				$par = 'entry_id';
				if (substr($orderby, 0, 16) == 'low_search_score')
				{
					$par = 'fixed_order';

					$this->_set_param('orderby');
					$this->_set_param('sort');
				}

				$this->_log("Setting {$par} parameter");
				$this->_set_param($par, implode('|', array_keys($results)));

				// Clean up
				unset($results);
			}
			else
			{
				$this->_log('Results negated by existing entry_id parameter');
				return $this->EE->TMPL->no_results();
			}

		} // End if ($do_search === TRUE)
		else
		{
			$this->_log("No search, just filter entries by given parameters");

			// --------------------------------------
			// No Low Search means no excerpt from collection,
			// so we need to get the excerpt field from the
			// channel preferences. We'll do that here and store
			// the ids in cache for the extension to use
			// --------------------------------------

			// Get channel excerpt data from DB
			$this->EE->db->select('ch.channel_name, ch.channel_id, ch.search_excerpt')->from('channels ch');

			// Filter by given collections
			if ($this->collection_ids)
			{
				$this->EE->db->from($this->EE->low_search_collection_model->table().' co')
					         ->where('ch.channel_id = co.channel_id')
					         ->where_in('co.collection_id', $this->collection_ids);
			}

			// Get the data
			$channels = $this->EE->db->get()->result_array();
			$excerpts = low_flatten_results($channels, 'search_excerpt', 'channel_id');

			// Register excerpt data to cache
			low_set_cache($this->package, 'excerpts', $excerpts);

			// --------------------------------------
			// If collections were given, we need to filter
			// the channel entries by channel, so we'll set
			// the channel parameter based on collections
			// --------------------------------------

			if ($this->params['collection'])
			{
				$channels = array_unique(low_flatten_results($channels, 'channel_name'));
				$this->_set_param('channel', implode('|', $channels));
			}

			// Remove low_search_score from the orderby parameter and adjust sort param accordingly
			if ( ! empty($this->params['orderby']) && substr($this->params['orderby'], 0, 16) == 'low_search_score')
			{
				$this->_set_param('orderby', preg_replace('#^low_search_score\|?#', '', $this->params['orderby']));

				if ( ! empty($this->params['sort']))
				{
					$this->_set_param('sort', preg_replace('#^(asc|desc)\|?#i', '', $this->params['sort']));
				}
			}

			// --------------------------------------
			// Check existing entry_id parameter
			// --------------------------------------

			if (is_array($this->entry_ids) && ! empty($this->entry_ids))
			{
				if ( ! empty($this->params['entry_id']))
				{
					// Get the parameter value
					list($ids, $in) = low_explode_param($this->params['entry_id']);

					// Either remove $ids from $entry_ids OR limit $entry_ids to $ids
					$method = $in ? 'array_intersect' : 'array_diff';

					// Get list of entry ids that should be listed
					$this->entry_ids = $method($this->entry_ids, $ids);
				}

				$this->_set_param('entry_id', implode('|', $this->entry_ids));
			}

		} // End if ($do_search === FALSE)

		// --------------------------------------
		// Optionally log search
		// --------------------------------------

		if ($this->EE->TMPL->fetch_param('log_search') == 'yes' &&
			! preg_match('#/P\d+/?$#', $this->EE->uri->uri_string()))
		{
			$this->_log_search(array_filter($this->params));
		}

		// --------------------------------------
		// Set misc tagparams
		// --------------------------------------

		$ignore = array_merge(
			$this->custom_params,
			array('query', 'log_search', 'entry_id', 'fixed_order')
		);

		foreach ($this->params AS $key => $val)
		{
			// Skip the ones we know are pointless for the Channel module or we checked already
			if (in_array($key, $ignore)) continue;

			$this->_set_param($key, $val);
		}

		// --------------------------------------
		// Keep track of params
		// --------------------------------------

		low_set_cache($this->package, 'params', $this->params);

		// --------------------------------------
		// Call Channel::entries
		// --------------------------------------

		$tagdata = $this->_channel_entries();
		$tagdata = $this->_post_parse($tagdata);

		return $tagdata;
	}

	// --------------------------------------------------------------------

	/**
	 * Display search collections
	 *
	 * @access      public
	 * @return      string
	 */
	public function collections()
	{
		// --------------------------------------
		// Check site
		// --------------------------------------

		$site_ids = $this->EE->TMPL->site_ids;

		// --------------------------------------
		// Get collections
		// --------------------------------------

		$rows = $this->_filter_collections('site_id', $site_ids);

		// --------------------------------------
		// Parse template
		// --------------------------------------

		return $rows
			? $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $rows)
			: $this->EE->TMPL->no_results();
	}

	// --------------------------------------------------------------------

	/**
	 * Display given keywords
	 *
	 * @access      public
	 * @return      string
	 */
	public function keywords()
	{
		// Do we have a valid encoded array?
		if ($query = $this->decode($this->EE->TMPL->fetch_param('query')))
		{
			// Make sure keywords exist in decoded array
			$keywords = array_key_exists('keywords', $query) ? $query['keywords'] : '';
		}
		else
		{
			$keywords = '';
		}

		// Check if keywords need to be encoded
		$format = $this->EE->TMPL->fetch_param('format', 'html');

		// Return formatted keywords
		return low_format($keywords, $format);
	}

	// --------------------------------------------------------------------

	/**
	 * Display popular keywords
	 *
	 * @access      public
	 * @return      string
	 */
	public function popular()
	{
		// --------------------------------------
		// Filter by site
		// --------------------------------------

		$this->EE->db->where('site_id', $this->site_id);

		// --------------------------------------
		// Limiting?
		// --------------------------------------

		if ( ! ($limit = (int) $this->EE->TMPL->fetch_param('limit')))
		{
			$limit = 10;
		}

		$this->EE->db->limit($limit);

		// --------------------------------------
		// Get terms
		// --------------------------------------

		if ($rows = $this->EE->low_search_log_model->get_popular_keywords())
		{
			// Get orderby and sort params
			$orderby = $this->EE->TMPL->fetch_param('orderby', 'search_count');
			$sort    = $this->EE->TMPL->fetch_param('sort', (($orderby == 'search_count') ? 'desc' : 'asc'));

			foreach ($rows AS &$row)
			{
				$kw = $row['keywords'];
				$row['keywords_raw']   = $kw;
				$row['keywords']       = low_format($kw, 'html');
				$row['keywords_url']   = low_format($kw, 'url');
				$row['keywords_clean'] = low_format($kw, 'clean');
				$row['keywords_param'] = low_format($kw, 'ee-encode');
			}

			// Different orderby?
			switch ($this->EE->TMPL->fetch_param('orderby'))
			{
				case 'keywords':
					usort($rows, 'low_by_keywords');
					if ($sort == 'desc') $rows = array_reverse($rows);
				break;

				case 'random':
					shuffle($rows);
				break;

				default:
					if ($sort == 'asc') $rows = array_reverse($rows);
			}
		}

		return $rows
			? $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $rows)
			: $this->EE->TMPL->no_results();
	}

	// --------------------------------------------------------------------

	/**
	 * Generate Open Search URL
	 *
	 * @access      public
	 * @return      string
	 */
	public function url()
	{
		// --------------------------------------
		// Are we returning an encoded URL or not?
		// --------------------------------------

		$encode = ! ($this->EE->TMPL->fetch_param('encode', 'yes') == 'no');

		// --------------------------------------
		// Check for given query
		// --------------------------------------

		$query = $this->decode($this->EE->TMPL->fetch_param('query'));

		// --------------------------------------
		// Params to ignore
		// --------------------------------------

		$ignore = array('query', 'encode', 'cache', 'refresh', 'parse');

		// --------------------------------------
		// Loop through tagparams and add them to the query string
		// --------------------------------------

		// init query string
		$qs = $query ? $query : array();

		// Override with tagparams
		foreach ($this->EE->TMPL->tagparams AS $key => $val)
		{
			if (in_array($key, $ignore)) continue;
			$qs[$key] = low_format($val, 'ee-decode');
		}

		// --------------------------------------
		// Use encoded string or not
		// --------------------------------------

		if ($encode)
		{
			// Get the result page from the params
			$url = $this->_create_url($qs);
		}
		else
		{
			// Build non-encoded URL
			$url = $this->EE->functions->fetch_site_index()
			     . QUERY_MARKER.'ACT='
			     . $this->EE->functions->fetch_action_id($this->package, 'catch_search')
			     . AMP.http_build_query($qs, '', AMP);
		}

		return $url;
	}

	// --------------------------------------------------------------------
	// ACT METHODS
	// --------------------------------------------------------------------

	/**
	 * Build collection index
	 *
	 * @access      public
	 * @return      string
	 */
	public function build_index()
	{
		// License key must be given
		$license_key = $this->get_settings('license_key');
		$given_key   = $this->EE->input->get_post('key');

		if ($given_key && $license_key == $given_key)
		{
			return $this->_build_index();
		}
		else
		{
			show_error($this->EE->lang->line('not_authorized'));
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Catch search form submission
	 *
	 * @access      public
	 * @return      void
	 */
	public function catch_search()
	{
		// --------------------------------------
		// Check if XID was posted and if it's valid
		// --------------------------------------

		if (($xid = $this->EE->input->post('XID')) && ! $this->EE->security->secure_forms_check($xid))
		{
			return $this->EE->output->show_user_error('general', array($this->EE->lang->line('not_authorized')));
		}

		// --------------------------------------
		// Initiate data array; will be encrypted
		// and put in the URI later
		// --------------------------------------

		$data = array();

		if ($params = $this->EE->input->post('params'))
		{
			$data = $this->decode($params);
		}

		// --------------------------------------
		// Check if custom params are posted
		// --------------------------------------

		foreach ($this->custom_params AS $key)
		{
			if ($val = $this->EE->input->get_post($key))
			{
				$data[$key] = is_array($val) ? implode('|', array_filter($val)) : $val;
			}
		}

		// --------------------------------------
		// Check other data
		// --------------------------------------

		foreach (array_merge($_GET, $_POST) AS $key => $val)
		{
			// Keys to skip
			if (in_array($key, array('ACT', 'XID', 'params', 'site_id'))) continue;

			// Add post var to data
			$data[$key] = is_array($val) ? implode('|', array_filter($val)) : $val;
		}

		// --------------------------------------
		// Check for required parameter
		// --------------------------------------

		if (isset($data['required']))
		{
			// Get required as array
			list($required, $in) = low_explode_param($data['required']);

			foreach ($required AS $req)
			{
				// Break out when empty
				// @TODO: enhance for multiple fields
				if (empty($data[$req]))
				{
					$this->_go_back($req.'_missing');
				}
			}

			// remove from data
			unset($data['required']);
		}

		// --------------------------------------
		// Check for require_all parameter
		// --------------------------------------

		if (isset($data['require_all']))
		{
			// Get require_all as array
			list($all, $in) = low_explode_param($data['require_all']);

			foreach ($all AS $key)
			{
				if ( ! isset($data[$key])) continue;

				// Search params need &&, other need &
				$amp = (substr($key, 0, 7) == 'search:') ? '&&'	: '&';
				$data[$key] = str_replace('|', $amp, $data[$key]);
			}

			// Remove from data
			unset($data['require_all']);
		}

		// --------------------------------------
		// Clean up the data array
		// --------------------------------------

		$data = array_filter($data);

		// --------------------------------------
		// Optionally log search query
		// --------------------------------------

		$this->_log_search($data);

		// --------------------------------------
		// Result URI: result page & cleaned up data, encoded
		// --------------------------------------

		$url = $this->_create_url($data);

		// --------------------------------------
		// Redirect to result page
		// --------------------------------------

		// Empty out flashdata to avoid serving of JSON for ajax request
		if (AJAX_REQUEST && count($this->EE->session->flashdata))
		{
			$this->EE->session->flashdata = array();
		}

		$this->EE->functions->redirect($url);
	}

	// --------------------------------------------------------------------
	// PRIVATE METHODS
	// --------------------------------------------------------------------

	/**
	 * Set TMPL parameter
	 *
	 * @access     private
	 * @param      string
	 * @param      string
	 * @return     void
	 */
	private function _set_param($key, $val = '')
	{
		// Check for search fields and add parameter to either tagparams or search_fields
		if (substr($key, 0, 7) == 'search:')
		{
			$this->EE->TMPL->search_fields[substr($key, 7)] = $val;
		}
		else
		{
			$this->EE->TMPL->tagparams[$key] = $val;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Create URL for given page and encoded query
	 *
	 * @access     private
	 * @param      array
	 * @return     string
	 */
	private function _create_url($query = array())
	{
		// --------------------------------------
		// If no page, get default
		// --------------------------------------

		$page = isset($query['result_page'])
		      ? $query['result_page']
		      : $this->get_settings('default_result_page');

		// Remove trailing slash
		$page = rtrim($page, '/');

		// Custom query position?
		if (strpos($page, '%s') === FALSE)
		{
			$page .= '/%s';
		}

		$url = sprintf($page, $this->encode($query));

		// --------------------------------------
		// If URI isn't a full url, make it so
		// --------------------------------------

		if ( ! preg_match('/^https?:\/\//', $url))
		{
			$url = $this->EE->functions->create_url($url);
		}

		return $url;
	}

	// --------------------------------------------------------------------

	/**
	 * Log given search parameters
	 *
	 * @access      private
	 * @param       array
	 * @return      void
	 */
	private function _log_search($data = array())
	{
		if (($search_log_size = $this->get_settings('search_log_size')) !== '0' && is_numeric($search_log_size))
		{
			$keywords = isset($data['keywords']) ? $data['keywords'] : '';

			// Don't add keywords to log parameters
			unset($data['keywords']);

			// Log search
			$this->EE->low_search_log_model->insert(array(
				'site_id'     => $this->site_id,
				'member_id'   => $this->EE->session->userdata['member_id'],
				'search_date' => $this->EE->localize->now,
				'ip_address'  => $this->EE->session->userdata['ip_address'],
				'keywords'    => $keywords,
				'parameters'  => $this->encode($data, FALSE)
			));

			// Prune log
			// Rand trick borrowed from native search module
			if ((rand() % 100) < 5)
			{
				$this->EE->low_search_log_model->prune($this->site_id, $search_log_size);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Check for {if low_search_no_results}
	 *
	 * @access      private
	 * @return      void
	 */
	private function _prep_no_results()
	{
		// Shortcut to tagdata
		$td =& $this->EE->TMPL->tagdata;
		$open = "if {$this->package}_no_results";
		$close = '/if';

		// Check if there is a custom no_results conditional
		if (strpos($td, $open) !== FALSE && preg_match('#'.LD.$open.RD.'(.*?)'.LD.$close.RD.'#s', $td, $match))
		{
			$this->_log("Prepping {$open} conditional");

			// Check if there are conditionals inside of that
			if (stristr($match[1], LD.'if'))
			{
				$match[0] = $this->EE->functions->full_tag($match[0], $td, LD.'if', LD.'\/if'.RD);
			}

			// Set template's no_results data to found chunk
			$this->EE->TMPL->no_results = substr($match[0], strlen(LD.$open.RD), -strlen(LD.$close.RD));

			// Remove no_results conditional from tagdata
			$td = str_replace($match[0], '', $td);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Loads the Channel module and runs its entries() method
	 *
	 * @access      private
	 * @return      void
	 */
	private function _channel_entries()
	{
		// --------------------------------------
		// Set parameter so extension kicks in
		// --------------------------------------

		$this->EE->TMPL->tagparams['low_search'] = 'yes';

		// --------------------------------------
		// Make sure the following params are set
		// --------------------------------------

		$set_params = array(
			'dynamic'  => 'no',
			'paginate' => 'bottom'
		);

		foreach ($set_params AS $key => $val)
		{
			if ( ! $this->EE->TMPL->fetch_param($key))
			{
				$this->EE->TMPL->tagparams[$key] = $val;
			}
		}

		// --------------------------------------
		// Take care of related entries
		// --------------------------------------

		// We must do this, 'cause the template engine only does it for
		// channel:entries or search:search_results. The bastard.
		$this->EE->TMPL->tagdata = $this->EE->TMPL->assign_relationship_data($this->EE->TMPL->tagdata);

		// Add related markers to single vars to trigger replacement
		foreach ($this->EE->TMPL->related_markers AS $var)
		{
			$this->EE->TMPL->var_single[$var] = $var;
		}

		// --------------------------------------
		// Get channel module
		// --------------------------------------

		$this->_log('Calling the channel module');

		if ( ! class_exists('channel'))
		{
			require_once PATH_MOD.'channel/mod.channel'.EXT;
		}

		// --------------------------------------
		// Create new Channel instance
		// --------------------------------------

		$channel = new Channel;

		// --------------------------------------
		// Let the Channel module do all the heavy lifting
		// --------------------------------------

		return $channel->entries();
	}

	// --------------------------------------------------------------------

	/**
	 * Get field id for given field short name
	 *
	 * @access      private
	 * @param       string
	 * @return      int
	 */
	private function _get_field_id($str)
	{
		if ( ! ($fields = low_get_cache('channel', 'custom_channel_fields')))
		{
			$this->_log('Getting channel field info from API');

			$this->EE->load->library('api');
			$this->EE->api->instantiate('channel_fields');

			$fields = $this->EE->api_channel_fields->fetch_custom_channel_fields();

			foreach ($fields AS $key => $val)
			{
				low_set_cache('channel', $key, $val);
			}

			$fields = $fields['custom_channel_fields'];
		}

		return isset($fields[$this->site_id][$str]) ? $fields[$this->site_id][$str] : 0;
	}

	/**
	 * Check whether given string is a date field
	 *
	 * @access      private
	 * @param       string
	 * @return      bool
	 */
	private function _is_date_field($str)
	{
		$fields = low_get_cache('channel', 'date_fields');
		return isset($fields[$this->site_id][$str]);
	}

	/**
	 * Check whether given string is a relationship field
	 *
	 * @access      private
	 * @param       string
	 * @return      bool
	 */
	private function _is_rel_field($str)
	{
		$fields = low_get_cache('channel', 'relationship_fields');
		return isset($fields[$this->site_id][$str]);
	}

	/**
	 * Check whether given field id is a playa field
	 *
	 * @access      private
	 * @param       int
	 * @return      bool
	 */
	private function _is_playa_field($id)
	{
		$fields = low_get_cache('channel', 'pair_custom_fields');
		return (@$fields[$this->site_id][$id] == 'playa');
	}

	// --------------------------------------------------------------------

	/**
	 * Redirect to referrer with some flashdata
	 *
	 * @access      private
	 * @param       string
	 * @return      void
	 */
	private function _go_back($with_message)
	{
		$this->EE->session->set_flashdata('error_message', $with_message);
		$this->EE->functions->redirect($_SERVER['HTTP_REFERER']);
	}

	// --------------------------------------------------------------------

	/**
	 * Search parameters for range:field params and return set of ids that match it
	 *
	 * @access      private
	 * @param       array
	 * @return      mixed
	 */
	private function _get_ranges()
	{
		// --------------------------------------
		// Bail out if no entry ids
		// --------------------------------------

		if ( ! $this->_has_ids()) return;

		// --------------------------------------
		// initiate ranges
		// --------------------------------------

		$ranges = $range_ids = array();

		// --------------------------------------
		// Check parameters and analyse range params
		// --------------------------------------

		foreach ($this->params AS $key => $val)
		{
			if ( ! preg_match('#^range-?(from|to)?:([\w\-]+)$#', $key, $match)) continue;

			// Init this range
			$from = $to = NULL;
			$fromto = $match[1];
			$field  = $match[2];

			// range:field="x;y" syntax
			if ( ! $fromto)
			{
				if (strpos($val, ';') !== FALSE)
				{
					list($from, $to) = explode(';', $val, 2);
				}
			}
			else
			{
				// Sets $from or $to to the right value
				$$fromto = $val;
			}

			if ( ! is_null($from))
			{
				$ranges[$field]['from'] = $from;
			}

			if ( ! is_null($to))
			{
				$ranges[$field]['to'] = $to;
			}
		}

		// --------------------------------------
		// Exit of no ranges were defined
		// --------------------------------------

		if ( ! $ranges) return;

		// --------------------------------------
		// Ranges found, go query them
		// --------------------------------------

		// Initiate array of WHERE clauses
		$where = array();

		foreach ($ranges AS $field => $range)
		{
			// Skip if field isn't a valid one
			if ( ! ($field_id = $this->_get_field_id($field))) continue;

			$sql_field = 'field_id_'.$field_id;

			if (isset($range['from']) && $range['from'])
			{
				// Convert to timestamp if datefield and not numeric
				if ($this->_is_date_field($field) && ! is_numeric($range['from']))
				{
					$range['from'] = ($time = strtotime($range['from'])) ? $time : $range['from'];
				}

				$where[] = "({$sql_field} >= '".$this->EE->db->escape_str($range['from'])."')";
			}

			if (isset($range['to']) && $range['to'])
			{
				// Convert to timestamp if datefield and not numeric
				if ($this->_is_date_field($field) && ! is_numeric($range['to']))
				{
					$range['to'] = ($time = strtotime($range['to'])) ? $time : $range['to'];
				}

				$where[] = "({$sql_field} <= '".$this->EE->db->escape_str($range['to'])."')";
			}
		}

		if ($where)
		{
			$this->_log('Limiting entry_ids by ranges');

			// Filter by channel ids
			if ($this->collection_ids)
			{
				$set = $this->_filter_collections('collection_id', $this->collection_ids);

				// Filter by channel (increases performance big time)
				if ($channels = array_unique(low_flatten_results($set, 'channel_id')))
				{
					$this->EE->db->where_in('channel_id', $channels);
				}
			}

			$this->EE->db->where(implode(' AND ', $where));

			// Possibly limit by entry ids
			if ( ! empty($this->entry_ids))
			{
				$this->EE->db->where_in('entry_id', $this->entry_ids);
			}

			// Query the rest of the ids
			$query = $this->EE->db->select('entry_id')->from('channel_data')->get();
			$range_ids = array_merge($range_ids, low_flatten_results($query->result_array(), 'entry_id'));

			// Adds found range ids to
			$this->_add_to_ids(array_unique($range_ids));
		}
	}

	/**
	 * Search parameters for (parents|children):field params and return set of ids that match it
	 *
	 * @access      private
	 * @return      mixed
	 */
	private function _get_relationships()
	{
		// --------------------------------------
		// Bail out if no entry ids
		// --------------------------------------

		if ( ! $this->_has_ids()) return;

		foreach ($this->params AS $key => $val)
		{
			if ( ! $val || ! preg_match('#^(parent|child):([\w\-]+)$#', $key, $match)) continue;

			// List out match
			list($param, $type, $field) = $match;

			// Get the field id, skip if non-existent
			if ( ! ($field_id = $this->_get_field_id($field))) continue;

			// Do we need to check all values?
			$all = FALSE;

			if (strpos($val, '&'))
			{
				$all = TRUE;
				$val = str_replace('&', '|', $val);
			}

			// Get the parameter properly
			list($ids, $in) = low_explode_param($val);

			// Init vars
			$rel_ids = $table = FALSE;
			$get_children = ($type == 'parent');

			// Native relationship field
			if ($this->_is_rel_field($field))
			{
				// Set the table & attributes
				$table = 'relationships';
				$select = $get_children ? 'rel_child_id' : 'rel_parent_id';
				$where  = $get_children ? 'rel_parent_id' : 'rel_child_id';
			}
			elseif ($this->_is_playa_field($field_id))
			{
				// Set the table
				$table = 'playa_relationships';
				$select = $get_children ? 'child_entry_id' : 'parent_entry_id';
				$where  = $get_children ? 'parent_entry_id' : 'child_entry_id';

				// Focus on specific field
				$this->EE->db->where('parent_field_id', $field_id);
			}

			// Log it
			$this->_log("Limiting entry_ids by {$type} relationship");

			// Execute query
			if ($table)
			{
				$this->EE->db->select($select.' AS entry_id')
				            ->from($table)
				            ->{$in ? 'where_in' : 'where_not_in'}($where, $ids);

				// Limit by already existing ids
				if ( ! empty($this->entry_ids))
				{
					$this->EE->db->where_in($select, $this->entry_ids);
				}

				// Do the having-trick to account for *all* given entry ids
				if ($in && $all)
				{
					$this->EE->db->select('COUNT(*) AS num')
					             ->group_by($select)
					             ->having('num', count($ids));
				}

				$query   = $this->EE->db->get();
				$rel_ids = low_flatten_results($query->result_array(), 'entry_id');
			}

			// Add it
			$this->_add_to_ids($rel_ids);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Add given IDs to $this->entry_ids, refining it the more it is called
	 *
	 * @access      private
	 * @param       mixed
	 * @return      void
	 */
	private function _add_to_ids($ids = FALSE)
	{
		// Do nothing if no ids are given
		if ($ids === FALSE) return;

		// Set this->entry_ids to empty array if empty
		if (is_array($ids))
		{
			if (empty($ids))
			{
				$this->entry_ids = array();
			}
			else
			{
				$this->entry_ids
					= (is_array($this->entry_ids))
					? array_intersect($this->entry_ids, $ids)
					: $ids;
			}
		}
	}

	/**
	 * Check if there are entry ids given or an empty set, which means no_results
	 *
	 * @access      private
	 * @return      bool
	 */
	private function _has_ids()
	{
		return ! (is_array($this->entry_ids) && empty($this->entry_ids));
	}

	// --------------------------------------------------------------------

	/**
	 * Add where clause to current query for exact matching
	 *
	 * @access      private
	 * @param       mixed
	 * @param       string
	 * @param       bool
	 * @return      void
	 */
	private function _where_exact($terms, $andor = 'AND', $loose = FALSE)
	{
		// Init where clause
		$where = array();

		// Look at terms
		if ( ! is_array($terms))
		{
			$terms = array($terms);
		}

		// Make unique
		$terms = array_unique($terms);

		// Loop through the terms and process them
		foreach ($terms AS $term)
		{
			// Clean it first
			$term = low_clean_string($term);

			// Skip if empty or single dash
			if ( ! strlen($term)) continue;

			// // REGEX method
			// // Account for loose ends
			// if ($loose)
			// {
			// 	$term .= '[^\s|\|]*';
			// }
			//
			// // Add to where clause
			// $where[] = " (index_text REGEXP '[[:<:]]{$term}[[:>:]]') ";

			// LIKE method
			// Not loose requires a space after the search term
			if ($loose === 'both')
			{
				// No nothing
			}
			elseif ( !! $loose === TRUE)
			{
				$term = " {$term}";
			}
			else
			{
				$term = " {$term} ";
			}

			// Add to where clause
			// Use space before term to force word lookup
			// instead of substring
			$where[] = " (index_text LIKE '%{$term}%') ";
		}

		// Add to where clause
		if ($where)
		{
			$sql_where = implode($andor, $where);
			$this->EE->db->where("({$sql_where})", '', FALSE);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Get collections from DB or Cache
	 *
	 * @access      private
	 * @return      array
	 */
	private function _get_collections()
	{
		if ( ! ($set = low_get_cache($this->package, 'collections')))
		{
			$this->EE->db->order_by('collection_label', 'asc');
			$set = $this->EE->low_search_collection_model->get_all();

			foreach ($set AS &$row)
			{
				$row['settings'] = $this->decode($row['settings'], FALSE);
			}

			$set = low_associate_results($set, 'collection_id');

			low_set_cache($this->package, 'collections', $set);
		}

		return $set;
	}

	/**
	 * Filter collections by given key and values
	 *
	 * @access      private
	 * @param       string
	 * @param       mixed
	 * @param       bool
	 * @param       array
	 * @return      array
	 */
	private function _filter_collections($key, $value, $in = TRUE, $set = array())
	{
		// Initiate output
		$filtered = array();

		// If set is not given, get all
		if ( ! $set)
		{
			$set = $this->_get_collections();
		}

		// Force the value to be an array
		if ( ! is_array($value))
		{
			$value = array($value);
		}

		// Loop through items in set and filter it
		foreach ($set AS $row)
		{
			if ($in === in_array($row[$key], $value))
			{
				$filtered[] = $row;
			}
		}

		// Return the filtered set
		return $filtered;
	}

	// --------------------------------------------------------------------

	/**
	 * Get collection ids by parameter
	 *
	 * @access      private
	 * @return      void
	 */
	private function _get_collection_ids()
	{
		// Bail out if no collection is given
		if (empty($this->params['collection'])) return;

		$in = TRUE;
		$collection = $this->params['collection'];

		// If not array, it's a parameter. Convert to array and bool (inlclusive or not)
		if ( ! is_array($collection))
		{
			list($collection, $in) = low_explode_param($collection);
		}

		// If it's anything other than an inclusive array of ids, get the details
		if ( ! ($numeric = low_array_is_numeric($collection) && $in))
		{
			// Check parameter to filter by
			$key = $numeric ? 'collection_id' : 'collection_name';

			// Get collections and flatten to ids
			$collection_ids = low_flatten_results($this->_filter_collections($key, $collection, $in), 'collection_id');
		}
		else
		{
			// Copy numeric array as is
			$collection_ids = $collection;
		}

		$this->collection_ids = $collection_ids;
	}

	// --------------------------------------------------------------------

	/**
	 * Get site ids by parameter
	 *
	 * @access      private
	 * @return      void
	 */
	private function _get_site_ids()
	{
		if (empty($this->params['site']))
		{
			// No site param? limit to current site only
			$this->site_ids[] = $this->site_id;
		}
		else
		{
			// Read sites from parameter
			list($sites, $in) = low_explode_param($this->params['site']);

			// Shortcut to all sites
			$all_sites = $this->EE->TMPL->sites;

			// Numeric?
			$check = low_array_is_numeric($sites) ? 'key' : 'val';

			// Loop through all sites and add some of them
			foreach ($all_sites AS $key => $val)
			{
				if ($in === in_array($$check, $sites))
				{
					$this->site_ids[$val] = $key;
				}
			}

			// And set to global TMPL
			$this->EE->TMPL->site_ids = $this->site_ids;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Post parse tagdata
	 *
	 * @access      private
	 * @param       string
	 * @return      string
	 */
	private function _post_parse($tagdata)
	{
		// CLean up prefixed variables
		$tagdata = preg_replace('#'.LD.$this->prefix.'.*?'.RD.'#i', '', $tagdata);

		// Prep {if foo IN (bar)} conditionals
		$tagdata = low_prep_in_conditionals($tagdata);

		return $tagdata;
	}

	// --------------------------------------------------------------------

	/**
	 * Log message to Template Logger
	 *
	 * @access     private
	 * @param      string
	 * @return     void
	 */
	private function _log($msg)
	{
		$this->EE->TMPL->log_item("Low Search: {$msg}");
	}

} // End Class

/* End of file mod.low_search.php */