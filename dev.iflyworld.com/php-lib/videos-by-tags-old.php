<?php

//error_log("-------------- videos-by-tags ------");
//error_log('POST:');
//error_log(print_r($_POST,true));

$tunnel = $_POST['tunnel'];
$page = $_POST['page'];
switch(strtolower($tunnel)) {
    case 'ifo' : $keyword = "Orlando"; break;
    case 'seat': $keyword = "Seattle"; break;
    case 'sfb' : $keyword = "SF Bay"; break;
    case 'hwd' : $keyword = "Hollywood"; break;
    case 'aus' : $keyword = "Austin"; break;
    default    : $keyword = "Orlando"; break;
}

require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Gdata_YouTube');

if (!isset($_SESSION)) session_start();
//session_destroy();
//die;

//$videos = getVideos("Seattle",1);

$videos = getVideos($keyword, $page);

$json_result = json_encode($videos,JSON_FORCE_OBJECT);
echo $json_result;

exit;

//echo "<pre>";
//print_r($videos);
//echo "</pre>";
//
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
//

function getVideos($keyword, $page) {
    $resultsPerPage = 12;
    $yt = new Zend_Gdata_YouTube();
    $yt->setMajorProtocolVersion(2);

    $result = array();
    $result['count'] = 0;
    $result['media'] = array();

    if (!isset($_SESSION['media'])) $_SESSION['media'] = array(); // initialize this array if it does not exist
    if (!isset($_SESSION['media'][$keyword])) $_SESSION['media'][$keyword] = array();
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")])) $_SESSION['media'][$keyword][date("Y-m-d")] = array(); // session cache only good for one day
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['count'])) $_SESSION['media'][$keyword][date("Y-m-d")]['count'] = 0;

    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['page'])) $_SESSION['media'][$keyword][date("Y-m-d")]['page'] = array();
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page])) $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page] = array();

    if (count($_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page]) != 0) {
        $result['count'] =$_SESSION['media'][$keyword][date("Y-m-d")]['count'] ;
        $result['media'] = $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page];
        return $result; // we alredy retrieved all the videos. just retrieve from cache
    }

    if ($_SESSION['media'][$keyword][date("Y-m-d")]['count'] !=0 ) {
        if ($page * $resultsPerPage + 1 > $_SESSION['media'][$keyword][date("Y-m-d")]['count']) return $result;
    }

    $url = "https://gdata.youtube.com/feeds/api/users/iflytunnelvision/playlists?v=2";
    $query = $yt->newVideoQuery($url);
    $feed = $yt->getVideoFeed($query); // get list of playlists

    $latestPlaylist = NULL;
    $playlistDate = null;
    foreach($feed as $entry) {

       //var_dump($entry);
        $playlistName = $entry->getTitle()->getText();
        // find matching keyword (ie.  Orlando)
        if (strstr($playlistName, $keyword)) {
            if (!$playlistDate){
                $latestPlaylist = $entry;
                $latestUpdated = $entry->getUpdated()->getText();
                $playlistDate =  strtotime($latestUpdated);
            } else if ($playlistDate < $entry->getUpdated()->getText() ){
                $latestPlaylist = $entry;
                $latestUpdated = $entry->getUpdated()->getText();
                $playlistDate =  strtotime($latestUpdated);
            }
        }
    }




    // with the most recent playlist...
    if ($latestPlaylist != NULL) {

        $id = $latestPlaylist->getId()->getText();
        $idarray = explode(":",$id);
        if (is_array($idarray)){
            $id = $idarray[count($idarray)-1];
        }

        $url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&start-index=" . (($page * $resultsPerPage) + 1) . "&max-results=" . $resultsPerPage . "";
        //echo $url;
        $query = $yt->newVideoQuery($url);

        $feed = $yt->getVideoFeed($query); // get list of playlists

        $result['count'] = $feed->getTotalResults()->getText();
        $_SESSION['media'][$keyword][date("Y-m-d")]['count'] = $feed->getTotalResults()->getText();


         foreach($feed->getEntry() as $entry) {
			//error_log("entry:");
			//error_log(print_r($entry,true));
            //var_dump($entry);
             $videoObject = array();
             $videoThumbnails = $entry->getVideoThumbnails();
             $videoObject['videoTitle'] = $entry->getVideoTitle();
             $videoObject['videoDescription'] = $entry->getVideoDescription();
             $videoObject['videoID'] = $entry->getVideoId();
             $videoObject['videoURL'] = $entry->getVideoWatchPageUrl();
             $videoObject['videoThumb'] = $videoThumbnails[0]['url'];
             $videos[] = $videoObject;
         }



         $result['results_per_page'] = $resultsPerPage;
         $result['media'] = $videos;
         $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page] = $videos;
    }

    //error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : result: " . print_r($result, true));
    return $result;
}



?>
