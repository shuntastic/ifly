<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'videoplayer/config.php';

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_mcp {

	/* constructor*/

	function __construct()
	{
		// Make a local reference to the ExpressionEngine super object

		$this->EE =& get_instance();
		
		
		$this->site_id = $this->EE->config->item('site_id');

		// load libraries & models
		
		
		$this->EE->load->library('videoplayer_lib');
		
		$this->EE->load->model('videoplayer_model');
		
		// load helper

		if (! class_exists('Videoplayer_helper'))
		{
			require_once PATH_THIRD.'videoplayer/helper.php';
		}

		$this->helper = new Videoplayer_helper;
		
	}

	// --------------------------------------------------------------------

	/**
	 * List accounts
	 *
	 * @access	public
	 * @return	string
	 */
	function index()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		$this->helper->insert_css_file('videoplayer.mcp.css');

		$this->EE->cp->set_variable('cp_page_title', VIDEOPLAYER_NAME);

		$vars['videos'] = array();


		// get services
		
		$services = $this->EE->videoplayer_lib->get_services($this->site_id);

		$vars['services'] = $services;


		// get accounts

		$accounts = array();

		foreach($vars['services'] as $service_key => $service)
		{
			$accounts[$service_key] = $this->EE->videoplayer_model->get_account($service_key, $this->site_id);
		}

		$vars['accounts'] = $accounts;


		// helper

		$vars['helper'] = $this->helper;


		// return view

		return $this->EE->load->view('mcp/index', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Configure accounts
	 *
	 * @access	public
	 * @return	string
	 */
	function configure()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		$this->helper->insert_css_file('videoplayer.mcp.css');
		
		$success = $this->EE->input->get('success');

		if($success)
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('account_setup_success'));
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer');
		}
		
		// get selected service

		$vars['service_key'] = $this->EE->input->get('service');


		// page title

		$this->EE->cp->set_variable('cp_page_title', "Configure ".ucwords($vars['service_key']));

		$this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer', VIDEOPLAYER_NAME);


		// get services
		
		$services = $this->EE->videoplayer_lib->get_services($this->site_id);
		
		$vars['services'] = $services;
		
		//var_dump($vars['services']['youtube']);

		$service = $vars['services'][$vars['service_key']];


		$vars['profile'] = $service->get_profile();

		// get account

		$vars['account'] = $this->EE->videoplayer_model->get_account($vars['service_key'], $this->site_id);


		// auth_mode

		$vars['auth_mode'] = $vars['services'][$vars['service_key']]->auth_mode;



		// load view

		$vars['helper'] = $this->helper;

		if(!$service->ping())
		{
			$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure_api'.AMP.'service='.$vars['service_key']);

		}

		return $this->EE->load->view('mcp/configure', $vars, true);


	}

	// --------------------------------------------------------------------

	/**
	 * Configure API
	 *
	 * @access	public
	 * @return	string
	 */
	function configure_api()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		
		// get selected service

		$service_key = $this->EE->input->get('service');


		// page title

		$this->EE->cp->set_variable('cp_page_title', "Configure ".ucwords($service_key));

		$this->EE->cp->set_breadcrumb(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer', VIDEOPLAYER_NAME);


		// get services

		$services = $this->EE->videoplayer_lib->get_services($this->site_id);

		$service = $services[$service_key];

		// get accounts

		$account = $this->EE->videoplayer_model->get_account($service_key, $this->site_id);
		

		if(!$account)
		{
			$account->api_key = "";
			$account->api_secret = "";
		}


		// auth_mode

		$auth_mode = $service->auth_mode;

		
		// save api

		if($this->EE->input->post('save_api_'.$service_key))
		{		
			$account->api_key = $this->EE->input->post($service_key.'_api_key');
			$account->api_secret = $this->EE->input->post($service_key.'_api_secret');

			$this->EE->videoplayer_model->save_account($service_key, $account, $this->site_id);
			
			$services = $this->EE->videoplayer_lib->get_services($this->site_id);

			$service = $services[$service_key];

			if($service->ping())
			{
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('api_success'));
				$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure'.AMP.'service='.$service_key);
			}
			else
			{
				$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('api_failure'));
				$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure_api'.AMP.'service='.$service_key);
			}
		}

		$vars = array();
		$vars['service_key'] 	= $service_key;
		$vars['service'] 		= $service;
		$vars['account'] 		= $account;



		return $this->EE->load->view('mcp/configure.api.php', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Switch an account to enabled or disabled
	 *
	 * @access	public
	 * @return	string
	 */
	function switch_enabled()
	{

		// get status

		$enabled = $this->EE->input->get('enabled');

		if($enabled == "yes" || $enabled === true)
		{
			$enabled = true;
		}
		else
		{
			$enabled = false;
		}


		// get service

		$service = $this->EE->input->get('service');


		// save service preference

		$data = array(
			'enabled' => $enabled
		);

		$this->EE->videoplayer_model->save_account($service, $data, $this->site_id);


		// redirect to preferences

		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer');
	}

	// --------------------------------------------------------------------

	/**
	 * Reset authentication for a given account
	 *
	 * @access	public
	 * @return	string
	 */
	function reset()
	{
		// get service

		$service = $this->EE->input->get('service');


		// get account

		$account = $this->EE->videoplayer_model->get_account($service, $this->site_id);


		// reset fields
		unset($account->id);
		$account->enabled = false;
		$account->is_authenticated = false;
		$account->authsub_session_token = '';
		$account->oauth_access_token = '';
		$account->oauth_access_token_secret = '';


		// save account

		$this->EE->videoplayer_model->save_account($service, $account, $this->site_id);


		// redirect

		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure'.AMP.'service='.$service);
	}
}

/* END Class */

/* End of file mcp.videoplayer.php */
/* Location: ./system/expressionengine/third_party/videoplayer/mcp.videoplayer.php */