<?php

require_once PATH_THIRD.'videoplayer/config.php';

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

$lang = array(

	// videoplayer
	
	'videoplayer_module_name' 			=> VIDEOPLAYER_NAME,
	'videoplayer_module_description' 	=> 'Connectez-vous à vos comptes YouTube et Vimeo', 
	'license_key' 						=> "License Key",
	
	
	// endpoint
	
	'error_occured' => "Une erreur est survenue",
	'cant_access_videoplayer' => "Impossible d'accéder à Video Player",
	
	
	// mcp.videoplayer
	
	'account_setup_success' => "Votre compte a été configuré avec succès",
	'api_success' => "L'API a été configurée avec succès",
	'api_failure' => "API key ou secret invalides",
	
	
	// views
		
		// box/box
		
		'close' => "Fermer",
		'search' => "Rechercher",
		'videos' => "Vidéos",
		'favorites' => "Favoris",
		'configure' => "Configurer",
		'my_videos' => "Mes vidéos",
		'search_video' => "Rechercher une vidéo",
		'select_video' => "Sélectionner",
		'cancel' => "Annuler",
		
		
		// box/preview
		
		'fullscreen' => "Plein écran",
		'add_favorite' => "Ajouter aux favoris",
		'no_description' => "Aucune description",
		
		
		// box/videos
		
		'from' => "de",
		'plays' => "lectures",
		'date' => "Date",
		'load_more_videos' => "Charger plus de vidéos",
		'loading_videos' => "Chargement des vidéos",
		'search_vimeo_videos' => "Rechercher des vidéos Vimeo",
		'search_youtube_videos' => "Rechercher des vidéos YouTube",
		'no_videos' => "Aucune vidéo",
		
		
		// field/field
		
		'add_video' => "Ajouter une vidéo",
		'change_video' => "Changer",
		'remove_video' => "Supprimer",
		'videoplayer_disabled' => "Video Player est désactivé car aucun service vidéo n'est configuré",
		
		
		// mcp/configure.api
		
		'configure_api' => "Configurer API",
		'no_youtube_key' => "Vous n'avez pas de clé développeur YouTube",
		'no_vimeo_key' => "Vous n'avez pas de clé développeur Vimeo",
		'register_one' => "Créez-en une",
		'continue' => "Continuer",
		
		
		// mcp/configure.authsub
		// mcp/configure.oauth
		
		'connect_your_ee_to' => "Vous devez connecter votre site ExpressionEngine à ",
		'connect_to' => "Se connecter à",
		
		
		// mcp/configure
		
		'is_configured' => "est configuré",
		'display_name' => "Nom d'affichage",
		'username' => "Nom d'utilisateur",
		'you_may_want_to' => "Vous devez vous ",
		'disconnect' => "déconnecter",
		'if_another_account' => "si vous voulez lier votre site à un autre compte",
		'api_configuration' => "Configuration API",
		
		
		// mcp/index
		
		'setup_video_services' => "Configuration des services vidéo",
		'disable' => "Désactiver",
		'enable' => "Activer",
);
