<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer {

	var $return_data = '';

	/**
	 * Constructor
	 *
	 */
	function Videoplayer()
	{

		$this->EE =& get_instance();

		// load videoplayer library

		if (! class_exists('Videoplayer_helper'))
		{
			require_once PATH_THIRD.'videoplayer/helper.php';
		}

		$this->helper = new Videoplayer_helper;


		$this->EE->load->library('videoplayer_lib');


		// constructor calls details()

		$this->details();
	}

	// --------------------------------------------------------------------

	/**
	 * Parameters
	 *
	 * @access	private
	 * @return	void
	 */
	private function _parameters()
	{
		$params = array();

		if(isset($this->EE->TMPL))
		{
			$parameters = array(
			
				// common
				
				'url',
				'default_size',
				
				
				// youtube + vimeo
				
				'width',
				'height',
				'autoplay',
				'loop',
				
				
				// youtube
				
				'autohide',
				'rel',
				'related_videos',
				'youtube_autohide',
				'youtube_cc_load_policy',
				'youtube_color',
				'youtube_controls',
				'youtube_disablekb',
				'youtube_enablejsapi',
				'youtube_end',
				'youtube_fs',
				'youtube_iv_load_policy',
				'youtube_modestbranding',
				'youtube_playerapiid',
				'youtube_rel',
				'youtube_showinfo',
				'youtube_start',
				'youtube_theme',
				
				
				//vimeo
				
				'vimeo_color',
				'vimeo_portrait',
				'vimeo_title',
				'vimeo_byline'
			);
		
			$return = array();
			
			foreach($parameters as $parameter)
			{
				if($this->EE->TMPL->fetch_param($parameter))
				{
					$return[$parameter] = $this->EE->TMPL->fetch_param($parameter);
				}
			}
		}

		return $return;
	}

	// --------------------------------------------------------------------

	/**
	 * Check if video exists
	 *
	 * @access	public
	 * @return	bool
	 */
	function video_exists()
	{
		$url = $this->EE->TMPL->fetch_param('url');

		return $this->EE->videoplayer_lib->video_exists($url);
	}

	// --------------------------------------------------------------------

	/**
	 * Front Endpoint for Ajax Calls
	 *
	 * @access	public
	 * @return	tagdata
	 */

	function ajax()
	{
		$this->EE->load->library('videoplayer_lib');

		$this->helper->endpoint_output();
	}

	// --------------------------------------------------------------------

	/**
	 * Details
	 *
	 * @access	public
	 * @return	tagdata
	 */
	function details()
	{
		$out = "";

		if(!isset($this->EE->TMPL->tagdata))
		{
			return $this->return_data = "Video error";
		}

		$tagdata = $this->EE->TMPL->tagdata;

		$params = $this->_parameters();

		$video = $this->EE->videoplayer_lib->get_video($params);


		// rendering pair

		if(!$video)
		{
			return $this->return_data = "Video error";
		}


		// no tagdata ? return the embed

		if(!$tagdata)
		{
			return $this->return_data = $video['embed'];
		}


		// parse tagdata

		$local_date = $video['date'];

		if (preg_match_all("#".LD."date format=[\"|'](.+?)[\"|']".RD."#", $tagdata, $matches))
		{
			foreach ($matches['1'] as $match)
			{
				$tagdata = preg_replace("#".LD."date format=.+?".RD."#", $this->EE->localize->decode_date($match, $local_date), $tagdata, 1);
			}
		}

		$conditionals = $this->EE->functions->prep_conditionals($tagdata, $video);

		$out = $this->EE->functions->var_swap($conditionals, $video);

		return $this->return_data = $out;
	}

	// --------------------------------------------------------------------

	/**
	 * Single - alias for details()
	 *
	 * @access	public
	 * @return	tagdata
	 */

	function single()
	{
		return $this->details();
	}

	// --------------------------------------------------------------------

	/**
	 * Pair - alias for details()
	 *
	 * @access	public
	 * @return	tagdata
	 */

	function pair()
	{
		return $this->details();
	}

}

/* END Class */

/* End of file mod.videoplayer.php */
/* Location: ./system/expressionengine/third_party/videoplayer/mod.videoplayer.php */