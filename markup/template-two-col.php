<?php include("inc/head.php"); ?>
<body class="info-page two-col">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<article>
						<h2>This could be a promotion</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu libero non tortor euismod interdum. Maecenas vestibulum orci tincidunt purus placerat eget porta nulla posuere. Nullam molestie elementum dui eu volutpat. Integer tempus ultricies iaculis. Aliquam vestibulum egestas ultrices.</p>
						<p><a href="#" class="btn red"><em></em><span>1 day special offer</span></a></p>
					</article>
					
					<aside class="gradient-border drop-shadow rounded-corners">
						<img src="/images/temp-fliers.jpg" alt="Some Text">
					</aside>

				</section>
				
				<section class="clearfix">
					
					<article>
						<h2>This could be a promotion</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu libero non tortor euismod interdum. Maecenas vestibulum orci tincidunt purus placerat eget porta nulla posuere. Nullam molestie elementum dui eu volutpat. Integer tempus ultricies iaculis. Aliquam vestibulum egestas ultrices.</p>
						<p><a href="#" class="btn red"><em></em><span>1 day special offer</span></a></p>
					</article>
					
					<aside class="gradient-border drop-shadow rounded-corners">
						<img src="/images/temp-fliers.jpg" alt="Some Text">
					</aside>

				</section>

				<section class="clearfix">
					
					<article>
						<h2>This could be a promotion</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu libero non tortor euismod interdum. Maecenas vestibulum orci tincidunt purus placerat eget porta nulla posuere. Nullam molestie elementum dui eu volutpat. Integer tempus ultricies iaculis. Aliquam vestibulum egestas ultrices.</p>
						<p><a href="#" class="btn red"><em></em><span>1 day special offer</span></a></p>
					</article>
					
					<aside class="gradient-border drop-shadow rounded-corners">
						<img src="/images/temp-fliers.jpg" alt="Some Text">
					</aside>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>