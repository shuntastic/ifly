<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step5">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step5" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next" href="#">&gt;</a>
				<div class="single-step">

				<div class="continue">
					<a href="#" class="btn"><em></em><span>CONTINUE SHOPPING</span></a>
				</div>
				
				<h1 class='underline'>YOUR CART</h1>

				<table id="your-cart" class="main">
					<thead>
						<tr>
							<th>Quantity</th>
							<th>Item</th>
							<th>Description</th>
							<th>#Flyers</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<span class="qty">1</span>
								<a href="#" class="btn red"><em></em><span>REMOVE</span></a>
							</td>
							<td>
								<h3>Earn Your Wings</h3>
								<p>for 2 first time flyers at iFLY Utah on July 7, 2012 at 11:00AM</p>
							</td>
							<td>
								<p>Earn Your Wings with our most popular first time flyer package. Includes two flights for one person in the wind tunnel.<a href="#" class="tooltip ir" data-title="some title" data-content="some content">more info</a></p>
							</td>
							<td>
								<p>1</p>
							</td>
							<td>$59.95</td>
							<td>$59.95</td>
						</tr>
						<tr>
							<td>
								<span class="qty">1</span>
								<a href="#" class="btn red"><em></em><span>REMOVE</span></a>
							</td>
							<td>
								<h3>Earn Your Wings</h3>
								<p>for 2 first time flyers at iFLY Utah on July 7, 2012 at 11:00AM</p>
							</td>
							<td>
								<p>Earn Your Wings with our most popular first time flyer package. Includes two flights for one person in the wind tunnel.<a href="#" class="tooltip ir" data-title="some title" data-content="some content">more info</a></p>
							</td>
							<td>
								<p>1</p>
							</td>
							<td>$59.95</td>
							<td>$59.95</td>
						</tr>
					</tbody>
				</table>

				<div id="promo-code">
					<form>
						<fieldset>
							<legend>ENTER PROMOTIONAL CODE</legend>
							<input type="text" name="promo" value="ENTER PROMOTIONAL CODE" class="text">
							<input type="submit" value="ENTER" class="enter">
						</fieldset>
					</form>
				</div>

				<div id="restrictions">
					<h3>Restrictions and Requirements</h3>
					<ul>
						<li>Scheduled flights may be transferred or rescheduled with 48 hours advance notice.</li>
						<li>Flyers are required to check-in 60 minutes before their scheduled flight time.</li>
						<li>All participants must sign our Release of Liability and present a valid state-issued ID.</li>
						<li>People with prior back injuries or shoulder dislocations should not fly!</li>
						<li>Participants under 6 feet tall must weigh less than 230 pounds.</li>
						<li>Participants over 6 feet tall must weigh less than 250 pounds.</li>
						<li>Additional restrictions may apply.</li>
						<li>Participants under 18 years of age must have a parent or legal guardian sign an iFLY release of liability and the parent or legal guardian must present* a valid state-issued ID.* (new paragraph) *If the parent or legal guardian can not attend, the participant must provide an iFLY release of liability signed by a parent or legal guardian and a photocopy of the parent or legal guardian’s valid state-issued ID.</li>
					</ul>
					<p>*If the parent or legal guardian can not attend, the participant must provide an iFLY release of liability signed by a parent or legal guardian and a photocopy of the parent or legal guardian's valid state-issued ID.</p>
				</div>

				<div id="summary">

					<table class="main">
						<tr class="sales-total">
							<th>Sales Total</th>
							<td></td>
						</tr>
						<tr class="discount">
							<th>Discount</th>
							<td></td>
						</tr>
						<tr class="sub-total">
							<th>Sub Total</th>
							<td></td>
						</tr>
						<tr class="tax">
							<th>Tax</th>
							<td></td>
						</tr>
						<tr class="fee">
							<th>Fee</th>
							<td></td>
						</tr>
						<tr class="grand-total">
							<th>Grand Total</th>
							<td></td>
						</tr>
						
					</table>

					<form class="uniform">
						<input type="submit" value="checkout" class="checkout">
						<label>I accept the restrictions<br />and requirements.</label>
						<input type="checkbox" name="restrictions" value="yes">
						
					</form>

				</div>
				
				
			</div>

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step5();
		});
	</script>
	
</body>
</html>