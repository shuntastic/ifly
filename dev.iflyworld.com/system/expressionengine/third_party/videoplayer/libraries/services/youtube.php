<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

include_once(PATH_THIRD."videoplayer/libraries/videoplayer_service.php");

class Videoplayer_youtube extends Videoplayer_service {

	var $service_name		= "YouTube";
	var $service_key		= "youtube";
	var $auth_mode		 	= "authsub";
	var $api_key 			= false;
	var $api_key_title 		= "Developer Key";
	var $register_key_url 	= "https://code.google.com/apis/youtube/dashboard";
	var $universal_url 		= "http://www.youtube.com/embed/%s?wmode=transparent";
	var $username 			= "default";

	// --------------------------------------------------------------------

	function __construct()
	{
		$this->EE =& get_instance();
        $this->EE->load->library('zend_lib');
	}

	// --------------------------------------------------------------------

	function _getApiObject()
	{

        $this->EE->load->library('zend_lib');

		$this->EE->load->library('logger');

        $this->EE->zend_lib->load(PATH_THIRD.'videoplayer/libraries/sdks/youtube/Zend/Gdata/YouTube');
        $this->EE->zend_lib->load(PATH_THIRD.'videoplayer/libraries/sdks/youtube/Zend/Gdata/AuthSub');

        $authsub = new Zend_GData_AuthSub();
        
        $this->EE->load->model('videoplayer_model');
        
        $account = $this->EE->videoplayer_model->get_account($this->service_key, $this->site_id);

        if(!$account)
        {
	        return false;
        }
        
		$session_token = $account->authsub_session_token;

		$httpClient = $authsub->getHttpClient($session_token);

		if(empty($account->api_key))
		{
			$account->api_key = "empty"; // fake false api key
		}

		$yt = new Zend_Gdata_YouTube($httpClient, false, false, $account->api_key);

		return $yt;
	}

	// --------------------------------------------------------------------

	function authsub($endpoint_url)
	{

        $this->EE->zend_lib->load(PATH_THIRD.'videoplayer/libraries/sdks/youtube/Zend/Gdata/AuthSub');

        $authsub = new Zend_GData_AuthSub();

		// start a new session

		if(!isset($_SESSION))
		{
			session_start();
		}


		$service = $this->EE->input->get_post('service');
		$site_id = $this->EE->input->get_post('site_id');

		if(!isset($_SESSION['authsub_state']))
		{
			$_SESSION['authsub_state'] = NULL;
		}

		if($this->EE->input->get('redirect'))
		{
			$_SESSION['redirect'] = $this->EE->input->get('redirect');
		}
		

		$callback_url = $endpoint_url."&method=authsub&service=".$service."&site_id=".$site_id;

		switch($_SESSION['authsub_state'])
		{
			case NULL:
			    $next = $callback_url;
			    $scope = 'http://gdata.youtube.com';
			    $secure = false;
			    $session = true;
			    $url = $authsub->getAuthSubTokenUri($next, $scope, $secure, $session);
			    $_SESSION['authsub_state'] = 1;
			   	redirect($url);
			    break;

		    case 1:

				if($this->EE->input->get('token'))
				{
					$session_token = $authsub->getAuthSubSessionToken($_GET['token']);

					$data = array(
						'authsub_session_token' => $session_token
					);

					$this->EE->videoplayer_model->save_account($service, $data, $site_id);

					if($this->is_authenticated())
					{
						$data = array(
							'enabled' => true,
							'is_authenticated' => true
						);

						$this->EE->videoplayer_model->save_account($service, $data, $site_id);
					}
				}

	       		$_SESSION['authsub_state'] = NULL;

				if($_SESSION['redirect'] != NULL)
				{
	        		$redirect = unserialize(base64_decode($_SESSION['redirect']));
	        		$_SESSION['redirect'] = NULL;

	        		redirect($redirect);
				}
			    break;
		}



	}

	// --------------------------------------------------------------------

	function get_video_id($url)
	{
		// check if url works with this service and extract video_id
		$video_id = false;

		$regexp = array('/^https?:\/\/(www\.youtube\.com|youtube\.com|youtu\.be).*\/(watch\?v=)?(.*)/', 3);


		if(preg_match($regexp[0], $url, $matches, PREG_OFFSET_CAPTURE) > 0)
		{

			// regexp match key

			$match_key = $regexp[1];


			// define video id

			$video_id = $matches[$match_key][0];


			// Fixes the youtube &feature_gdata bug

			if(strpos($video_id, "&"))
			{
				$video_id = substr($video_id, 0, strpos($video_id, "&"));
			}
		}

		// here we should have a valid video_id or false if service not matching

		return $video_id;

	}

	// --------------------------------------------------------------------

	function is_favorite($video_id)
	{

		$videos = $this->get_favorites(0,0);

		foreach($videos as $v)
		{

			if($v['id'] == $video_id)
			{
				return true;
			}
		}

		return false;
	}

	// --------------------------------------------------------------------

	function add_favorite($video_id)
	{
        $yt = $this->_getApiObject();

		$favoritesFeed = $yt->getUserFavorites($this->username);

		$videoEntry = $yt->getVideoEntry($video_id);

		$yt->insertEntry($videoEntry, $favoritesFeed->getSelfLink()->href);
	}

	// --------------------------------------------------------------------

	function remove_favorite($video_id)
	{
        $yt = $this->_getApiObject();

        $yt->setMajorProtocolVersion(2);
        
		$favoritesFeed = $yt->getUserFavorites($this->username);

		// $videoEntry = $yt->getVideoEntry($video_id);
		
		foreach($favoritesFeed as $vidEntry)
		{
			if($vidEntry->getVideoId() == $video_id)
			{
				$vidEntry->delete();
			}
		}
	}

	// --------------------------------------------------------------------

	function get_favorites($page, $per_page)
	{
		try {

			$startIndex = ($page - 1) * $per_page;

			if($startIndex > 0)
			{
				$startIndex++;
			}

	        $yt = $this->_getApiObject();

			$loc = "http://gdata.youtube.com/feeds/api/users/default/favorites";

			$query = $yt->newVideoQuery($loc);

			$query->setStartIndex($startIndex);
			$query->setMaxResults($per_page);

			$favorites = $yt->getVideoFeed($query);
			$favorites = $yt->retrieveAllEntriesForFeed($favorites);

			$videos = array();

			foreach($favorites as $fav)
			{
				$video = $this->developerdata($fav);

				array_push($videos, $video);
			}

			return $videos;
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/youtube.php ';
			$msg .= '"get_favorites" method ';
			$msg .= 'returned this error : ';
			$msg .= strip_tags($e->getMessage());
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}
	}

	// --------------------------------------------------------------------

	function get_videos($user = 'default')
	{

		try {
	        $yt = $this->_getApiObject();

			$loc = "http://gdata.youtube.com/feeds/api/users/".$user."/uploads";
			$query = $yt->newVideoQuery($loc);
			$query->setMaxResults(50);

			$uploads = $yt->getVideoFeed($query);
			$uploads = $yt->retrieveAllEntriesForFeed($uploads);

			$videos = array();

			foreach($uploads as $item)
			{
				$video = $this->developerdata($item);

				array_push($videos, $video);
			}

			return $videos;
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/youtube.php ';
			$msg .= '"get_videos" method ';
			$msg .= 'returned this error : ';
			$msg .= strip_tags($e->getMessage());
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Search Youtube videos
	 *
	 * @access	private
	 * @param	string
	 * @return	array
	 */
	function search($q, $page = 1, $per_page = 18)
	{
		try {


			$startIndex = ($page - 1) * $per_page;

			if($startIndex > 0)
			{
				$startIndex++;
			}

			$videos = array();

	        $yt = $this->_getApiObject();

	  		$yt->setMajorProtocolVersion(2);

			$query = $yt->newVideoQuery();
			$query->setSafeSearch('none');
			$query->setVideoQuery($q);
			$query->setStartIndex($startIndex);
			$query->setMaxResults($per_page);

			$videoFeed = $yt->getVideoFeed($query->getQueryUrl(2));

			foreach($videoFeed as $item)
			{
				$video = $this->developerdata($item);

				array_push($videos, $video);
			}

			return $videos;

		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/youtube.php ';
			$msg .= '"search" method ';
			$msg .= 'returned this error : ';
			$msg .= strip_tags($e->getMessage());

			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}

	}


	// --------------------------------------------------------------------

	/**
	 * Get Developer Meta Data
	 *
	 * @access	private
	 * @param	array
	 * @return	array
	 */

	private function developerdata($item)
	{
		$video = array();

		$thumbnails = $item->getVideoThumbnails();

		$video['id'] 			= (string) $item->getVideoId();
		$video['title'] 		= (string) $item->getVideoTitle();
		$video['service_name'] 	= "YouTube";
		$video['username'] 		= (string) $item->author[0]->name;
		$video['date'] 			= (string) strftime("%d/%m/%Y", strtotime($item->getPublished()));
		$video['plays'] 		= (string) $item->getVideoViewCount();

		if(!$video['plays'])
		{
			$video['plays'] = 0;
		}

		if(isset($thumbnails[0]))
		{
			$video['thumbnail'] 	= (string) $thumbnails[0]['url'];
		}

		$video['video_page'] 	= (string) $item->getVideoWatchPageUrl();


		// post process metadata

		$video = $this->post_process_video($video);

		return $video;
	}

	// --------------------------------------------------------------------

	/**
	 * Get Video Meta Data from Youtube
	 *
	 * @access	private
	 * @param	array
	 * @return	array
	 */
	function metadata($video)
	{
		try {
			// get video entry
	
	        $yt = $this->_getApiObject();
	        
			$url = "http://gdata.youtube.com/feeds/api/videos/".$video['id']."?format=5";
			
			$videoEntry = $yt->getVideoEntry($video['id']);
			
	
			// assign new variables
			
			
			// title
	
			$video['title'] 					= (string) $videoEntry->getVideoTitle();
			
			
			// description
			
			$video['description'] 				= (string) $videoEntry->getVideoDescription();
			$video['description'] 				= nl2br($video['description']);
			
			
			// author
			
			$author = $videoEntry->getAuthor();
			$author = $author[0];
			
			$video['username'] 					= (string) $author->name->text;
			$video['author'] 					= (string) $author->name->text;
			
			
			// date
			
			$video['date'] 						= (string) strtotime($videoEntry->published->text);
			
			
			// plays
			
			$video['plays'] 					= (string) $videoEntry->statistics->viewCount;
			
			
			// duration in seconds
			
			$video['duration'] 					= (string) $videoEntry->mediaGroup->duration->seconds;
			
			
			// thumbnails		
	
			$video['thumbnail'] 				= (string) $videoEntry->mediaGroup->thumbnail[1]->url;
			$video['thumbnail_large'] 			= (string) $videoEntry->mediaGroup->thumbnail[0]->url;
			
			$video['youtube_thumbnail_0'] 		= (string) $videoEntry->mediaGroup->thumbnail[0]->url; // large
			$video['youtube_thumbnail_1'] 		= (string) $videoEntry->mediaGroup->thumbnail[1]->url; // small 1
			$video['youtube_thumbnail_2'] 		= (string) $videoEntry->mediaGroup->thumbnail[2]->url; // small 2
			$video['youtube_thumbnail_3'] 		= (string) $videoEntry->mediaGroup->thumbnail[3]->url; // small 3
			
			return $video;
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/youtube.php ';
			$msg .= '"metadata" method ';
			$msg .= 'returned this error : ';
			$msg .= strip_tags($e->getMessage());
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
			
			return $video;
		}
	}

	// --------------------------------------------------------------------
	
	function get_profile()
	{
		try {
	        $yt = $this->_getApiObject();
	        	
			$yt->setMajorProtocolVersion(2);
			
			$profile = array(
				'display_name' => "",
				'username' => ""
			);
	
			$userProfileEntry = $yt->getUserProfile($this->username);
			
			if($userProfileEntry)
			{		
				$profile['display_name'] = $userProfileEntry->title->text;
				$profile['username'] = $userProfileEntry->getUsername()->text;	
			}
			
			return $profile;
        }
        catch (Exception $e)
        {
        	// echo $e->code;
            //echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";
            return false;
        }
	}

	// --------------------------------------------------------------------

	function is_authenticated()
	{
        try
        {
	        $yt = $this->_getApiObject();

			$loc = "http://gdata.youtube.com/feeds/api/users/default/favorites";

			$query = $yt->newVideoQuery($loc);

			$favorites = $yt->getVideoFeed($query);
			$favorites = $yt->retrieveAllEntriesForFeed($favorites);

			return true;
        }
        catch (Exception $e)
        {
           // echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";

           return false;
        }
	}

	// --------------------------------------------------------------------

	function ping()
	{
		try
        {
	        $yt = $this->_getApiObject();

	        if(!$yt)
	        {
		        return false;
	        }

			$loc = "https://gdata.youtube.com/feeds/api/standardfeeds/top_rated";
			
			$query = $yt->newVideoQuery($loc);
			$query->setMaxResults(50);

			$uploads = $yt->getVideoFeed($query);
			$uploads = $yt->retrieveAllEntriesForFeed($uploads);

			$videos = array();

			foreach($uploads as $item)
			{
				$video = array();

				$thumbnails = $item->getVideoThumbnails();

				$video['id'] 			= (string) $item->getVideoId();
				$video['title'] 		= (string) $item->getVideoTitle();
				$video['service_name'] 	= "YouTube";
				$video['username'] 		= (string) $item->author[0]->name;
				$video['plays'] 		= (string) $item->getPublished();

				if(isset($thumbnails[0]))
				{
					$video['thumbnail'] 	= (string) $thumbnails[0]['url'];
				}

				array_push($videos, $video);
			}

			return true;
        }
        catch (Exception $e)
        {

			$msg = 'videoplayer/lirabries/services/youtube.php ';
			$msg .= '"ping" method ';
			$msg .= 'returned this error : ';
			$msg .= strip_tags($e->getMessage());
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}

            return false;
        }
	}
}

// END Videoplayer_youtube class

/* End of file youtube.php */
/* Location: ./system/expressionengine/third_party/videoplayer/libraries/services/youtube.php */