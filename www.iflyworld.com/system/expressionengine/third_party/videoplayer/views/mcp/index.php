<h3><?php echo $this->lang->line('setup_video_services'); ?></h3>

<div class="videoplayer-mcp-accounts">
	<table>
		<?php
		foreach($accounts as $service_key => $account)
		{
			$enabled = false;
			$is_authenticated = false;

			if($account)
			{
				$enabled = $account->enabled;
				$is_authenticated = $account->is_authenticated;
			}

			?>
			<tr>
				<td class="videoplayer-mcp-col-status">
					<?php
					if($enabled && $is_authenticated)
					{
						?>
						<a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=switch_enabled'.AMP.'service='.$service_key.AMP.'enabled=no'?>" class="videoplayer-mcp-enabled"><?php echo $this->lang->line('disable'); ?></a>
						<?php
					}
					else
					{
						?>
						<a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=switch_enabled'.AMP.'service='.$service_key.AMP.'enabled=yes'?>" class="videoplayer-mcp-disabled"><?php echo $this->lang->line('enable'); ?></a>
						<?php
					}
					?>
				</td>
				<td>
					<span class="videoplayer-mcp-service-<?php echo $service_key?>"><?php echo $services[$service_key]->service_name; ?></span>
				</td>


				<td class="videoplayer-mcp-col-configure">
					<a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure'.AMP.'service='.$service_key?>" class="videoplayer-btn"><?php echo $this->lang->line('configure'); ?></a>
				</td>


				<td class="videoplayer-mcp-col-enable">
					<?php
					if($enabled && $is_authenticated == true)
					{
						?>
						<a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=switch_enabled'.AMP.'service='.$service_key.AMP.'enabled=no'?>" class="videoplayer-btn<?php
						if(!$account->is_authenticated)
						{
							echo ' videoplayer-btn-disabled';
						}
						?>"><?php echo $this->lang->line('disable'); ?></a>
						<?php
					}
					else
					{
						?>
						<a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=switch_enabled'.AMP.'service='.$service_key.AMP.'enabled=yes'?>" class="videoplayer-btn<?php
						if(!$is_authenticated)
						{
							echo ' videoplayer-btn-disabled';
						}
						?>"><?php echo $this->lang->line('enable'); ?></a>
						<?php
					}
					?>

				</td>
			</tr>
			<?php
		}
		?>
	</table>
</div>