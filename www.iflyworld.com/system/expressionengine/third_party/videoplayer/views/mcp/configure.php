<div class="videoplayer-mcp-configure-details">

	<?php


	if($account->is_authenticated)
	{
		?>
		<h4><?php echo ucwords($service_key)?> <?php echo $this->lang->line('is_configured'); ?></h4>
		
		<ul>
			<li><strong><?php echo $this->lang->line('display_name'); ?> :</strong> <?php echo $profile['display_name']?></li>
			<li><strong><?php echo $this->lang->line('username'); ?> :</strong> <?php echo $profile['username']?></li>
		</ul>
		
		<p><?php echo $this->lang->line('you_may_want_to'); ?> <a href="<?php echo BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=reset'.AMP.'service='.$service_key?>"><?php echo $this->lang->line('disconnect'); ?></a> <?php echo $this->lang->line('if_another_account'); ?>.</p>
		
		<?php
	}
	else
	{
		// configure authentication

		switch($auth_mode)
		{
			case "auth":
			$this->load->view('mcp/configure.auth.php');
			break;

			case "oauth":
			$this->load->view('mcp/configure.oauth.php');
			break;

			case "authsub":
			$this->load->view('mcp/configure.authsub.php');
			break;
		}
		?>
		<p><?php echo anchor(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure_api'.AMP.'service='.$service_key, $this->lang->line('api_configuration'))?></p>

		<?php
	}
	?>

</div>
