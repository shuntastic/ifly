		<header id="header">
			<div class="logo"><a href="#">I Fly indoor skydiving</a></div>
			<div class="header-block">
				<div class="header-top">
					<strong class="phone">1-800-555-5555</strong>
					<nav class="add-nav">
						<ul>
							<li><a href="#">Shop Now</a></li>
							<li><a href="#">Waiver</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</nav>
				</div><!-- /.header-top -->
				<div class="header-holder">
					<ul class="social">
						<li><a class="facebook" href="#">facebook</a></li>
						<li><a class="twitter" href="#">twitter</a></li>
					</ul>
					<form action="#" class="search-form">
						<fieldset>
							<input class="text" type="text" placeholder="SEARCH" />
							<input class="btn-search" type="submit" value="go" />
						</fieldset>
					</form>
				</div>
				<nav id="nav">
					<ul>
						<li id="what">
							<a href="#"><span class="ir">What is iFly?</span></a>
							<div class="drop">
								<ul>
									<li><a href="#">HOW IT WORKS</a></li>
									<li><a href="#">ABOUT iFLY</a></li>
									<li><a href="#">VIDEO TESTIMONIALS </a></li>
									<li><a href="#">RATINGS &amp; REVIEWS</a></li>
								</ul>
							</div>
						</li>
						<li id="shop">
							<a href="#"><span class="ir">>SHOP NOW</span></a>
						</li>
						<li id="media">
							<a href="#"><span class="ir">>PICS &amp; VIDS</span></a>
							<div class="drop">
								<ul>
									<li><a href="#">FIND YOUR PHOTOS &amp; VIDEOS</a></li>
									<li><a href="#">iFLY PHOTOS &amp; VIDEOS</a></li>
								</ul>
							</div>
						</li>
						<li id="find">
							<a href="#"><span class="ir">>FIND A TUNNEL</span></a>
						</li>
					</ul>
				</nav>
			</div><!-- /.header-block -->
		</header>