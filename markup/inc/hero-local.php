                <div class="visual-block">
                        <div class="visual-holder">
                                <ul id="slider">
                                        <li>
                                                <article class="video-block">
                                                        <h2>EVER DREAM OF FLYING?</h2>
                                                        <p>iFLY indoor skydiving makes the dream of flight a reality.</p>
                                                        <p>Press play to fly.</p>
                                                        <div class="video"><a href="#yP9GXL68w2w"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
                                                </article>
                                        </li>
                                        <li>
                                                <article class="video-block">
                                                        <h2>First Time Flyers</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br />Nam neque turpis, sodales sit amet tempor at, vulputate et urna.<br />Duis luctus, lectus a vehicula pharetra.</p>
							<div class="static-image"><a href="#"><img src="images/local-hero-holder.png" width="500" height="350" alt="image description"></a></div>
                                                </article>
                                        </li>
                                        <li>
                                                <article class="video-block">
                                                        <h2>Who Can Fly</h2>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
                                                        <div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
                                                </article>
                                        </li>
                                        <li>
                                                <article class="video-block">
                                                        <h2>Instructors</h2>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
                                                        <div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
                                                </article>
                                        </li>
                                        <li>
                                                <article class="video-block">
                                                        <h2>I.B.A. Moves</h2>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
                                                        <div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
                                                </article>
                                        </li>
                                </ul><!-- /#slider -->

				<div id="video-modal">
					<div class="video-wrapper">
						<a href="#" class="btn red" title="close" >
						<em></em>
						<span>close</span>
						</a>
						<!-- <a class="btn-prev" href="#">&lt;</a>
						<a class="btn-next" href="#">&gt;</a> -->
						<div class="video-player">
							<div id="yt-player"></div>
						</div>
					</div><!-- /.video-wrapper -->
				</div><!-- /#video-modal -->

				<div class="menu-block local">
					<div id="city-scape"></div>
					<nav class="menu">
						<ul>
							<li class="active"><a href="#1">Lorem ipsum DOLOR</a></li>
							<li><a href="#2">Lorem ipsum DOLOR</a></li>
							<li><a href="#3">Lorem ipsum DOLOR</a></li>
							<li><a href="#4">Lorem ipsum DOLOR</a></li>
							<li><a href="#5">Lorem ipsum DOLOR</a></li>
						</ul>
					</nav>
				</div>

				<div class="menu-block global">
					<nav class="menu">
						<ul>
							<li class="active"><a href="#1">What Is It?</a></li>
							<li><a href="#2">First Time Flyers</a></li>
							<li><a href="#3">Who Can Fly</a></li>
							<li><a href="#4">Instructors</a></li>
							<li><a href="#5">I.B.A. Moves</a></li>
						</ul>
					</nav>
				</div>

			</div>
		
		</div><!-- /.visual-block -->