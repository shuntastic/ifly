<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8">
		<meta name="format-detection" content="telephone=no">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>IFly</title>
		<link media="all" rel="stylesheet" type="text/css" href="/css/boilerplate.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/anythingslider.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/uniform.ifly.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/colorbox.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/calendar.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/all.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/pages.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/local-sites.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/photovid.css">
		<link media="all" rel="stylesheet" type="text/css" href="/css/booking.css">
		<script type="text/javascript" src="/js/libs/jquery-1.8.0.min.js" ></script>
		<script type="text/javascript" src="/js/libs/modernizr-2.5.3-respond-1.1.0.min.js" ></script>
		<!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="css/ie.css" media="screen"/><![endif]-->


	</head>