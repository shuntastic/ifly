<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * ExpressionEngine - by EllisLab
 *
 * @package		ExpressionEngine
 * @author		EllisLab Dev Team
 * @copyright	Copyright (c) 2003 - 2012, EllisLab, Inc.
 * @license		http://expressionengine.com/user_guide/license.html
 * @link		http://expressionengine.com
 * @since		Version 2.0
 * @filesource
 */
 
// ------------------------------------------------------------------------

/**
 * ExpressionEngine Sites Class
 *
 * @package		ExpressionEngine
 * @subpackage	Core
 * @category	Core
 * @author		EllisLab Dev Team
 * @link		http://expressionengine.com
 */
class EE_Sites { 

	public $num_sites_allowed = 'Uh5UYwdnDn4ARlEKAWUIVQth';
	public $the_sites_allowed = 'AToIZ1ZADWUDaQZLVQYANARm';
	public $sites_allowed_num = 'ARQEagFZD1lWAwVVDG8FOg';
}
// END CLASS

/* End of file Sites.php */
/* Location: ./system/expressionengine/libraries/Sites.php */