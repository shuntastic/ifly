<h4><?php echo $this->lang->line('configure_api'); ?></h4>

<p><?php echo $this->lang->line('no_'.$service->service_key.'_key'); ?> ? <?php echo anchor($service->register_key_url, $this->lang->line('register_one'), 'target="_blank"'); ?>.</p>

<?php echo form_open('C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer'.AMP.'method=configure_api'.AMP.'service='.$service_key)?>

	<?php
	if(isset($service->api_key))
	{
		?>
		<p>
			<label>
				<strong><?php echo $service->api_key_title?></strong>
				<input type="text" name="<?php echo $service_key?>_api_key" value="<?php echo $account->api_key; ?>" />
			</label>
		</p>
		<?php
	}
	?>

	<?php
	if(isset($service->api_secret))
	{
		?>
		<p>
			<label><strong><?php echo $service->api_secret_title?></strong><input type="text" name="<?php echo $service_key?>_api_secret" value="<?php echo $account->api_secret; ?>" /></label>
		</p>
		<?php
	}
	?>


	<p>
		<?php
		echo form_submit('save_api_'.$service_key, $this->lang->line('continue'), 'class="videoplayer-btn submit"');
		?>
	</p>

<?php echo form_close()?>