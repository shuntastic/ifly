var locationHandler = {
	geocoder: null,
	locations: [{name:'iFly Hollywood',lat:34.092809, long:-118.328661, tunnel:'ifh'},{name:'iFly Orlando',lat:28.538336, long:-81.379236, tunnel:'ifo'},{name:'iFly Seattle',lat:47.60621, long:-122.332071, tunnel:'ifs'},{name:'iFly Ogden',lat:41.223, long:-111.97383, tunnel:'ifogden'},{name:'iFly San Francisco',lat:37.77493, long:-122.419416, tunnel:'ifsf'},{name:'iFly Austin',lat:30.267153, long:-97.743061, tunnel:'ifa'}],
	init: function() {
		//$('div.popup-block h1').html('');
		$('div.popup-block h1').addClass('loading');
		geocoder = new google.maps.Geocoder();
		if(google.loader.ClientLocation) {
			//console.log('google.loader.ClientLocation Found');
            visitor_lat = google.loader.ClientLocation.latitude;
            visitor_lon = google.loader.ClientLocation.longitude;
			locationHandler.find_closest_location(visitor_lat,visitor_lon);
		} else {
			//console.log('google.loader.ClientLocation NULL');
			navigator.geolocation.getCurrentPosition(locationHandler.onSuccess, locationHandler.onError);
//			geocoder = new google.maps.Geocoder();
		}
	},
	onSuccess: function(position) {
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		locationHandler.find_closest_location(lat,lng);
		var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
		geocoder.geocode({'latLng': latlng}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
			//console.log('results: '+results)
			if (results[1]) {
				//formatted address
				//console.log(results[0].formatted_address);
			 
				//find country name
				for (var i=0; i<results[0].address_components.length; i++) {
					for (var b=0;b<results[0].address_components[i].types.length;b++) {
						//there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
						if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
							//this is the object you are looking for
							city= results[0].address_components[i];
							break;
						}
					}
				}
			//city data
			// console.log(city.short_name + " " + city.long_name+'<br />');
			
			} else {
			 console.log("No results found");
			}
			} else {
			 console.log("Geocoder failed due to: " + status);
			}
		});
	},
	onError: function(e){
		console.log("Geocoder failed: "+e)
		buildDropdown();
},
	rad: function (x) {return x*Math.PI/180;},
	find_closest_location: function(tLat,tLong) {
		var lat = tLat;
		var lng = tLong;
		var R = 6371; // radius of earth in km
		var distances = [];
		var closest = -1;
		for( i=0;i<locationHandler.locations.length; i++ ) {
			var mlat = locationHandler.locations[i].lat;
			var mlng = locationHandler.locations[i].long;
			var dLat  = locationHandler.rad(mlat - lat);
			var dLong = locationHandler.rad(mlng - lng);
			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(locationHandler.rad(lat)) * Math.cos(locationHandler.rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;
			distances[i] = d;
			locationHandler.locations[i].distance = d;
			if ( closest == -1 || d < distances[closest] ) {
				closest = i;
			}
		}
		function compare(a,b) {
		  if (a.distance < b.distance)
			 return -1;
		  if (a.distance > b.distance)
			return 1;
		  return 0;
		}
		locationHandler.locations.sort(compare);
		buildDropdown();
	}	
};

function buildDropdown() {
//	<li class="active"><a href="#" class="first-child">Hollywood, CA</a></li>
//	<li><a href="#">Orlando, FL</a></li>
//	<li><a href="#">Seattle, WA</a></li>
//	<li><a href="#">Ogden, UT</a></li>
//	<li><a href="#" class="last-child">San Francisco, CA</a></li>
	$('ul#tunnel-locator').html('');
	$('div.popup-block h1').removeClass('loading');
	$('div.popup-block h1').html(locationHandler.locations[0].name)
	$.each(locationHandler.locations, function(i,v) {
		var newItem;
		if(i==0) {
			newItem = $('<li class="active"><a href="#" class="first-child">'+locationHandler.locations[i].name+'</a></li>');
		} else if (i == locationHandler.locations.length-1) {
			newItem = $('<li><a href="#" class="last-child">'+locationHandler.locations[i].name+'</a></li>');
		} else {
			newItem = $('<li><a href="#">'+locationHandler.locations[i].name+'</a></li>');
		}
		$('ul#tunnel-locator').append(newItem);
		
		newItem.click(function() {
			var currentName = locationHandler.locations[i].name;
			$('div.popup-block h1').html(currentName);
			$(this).parent('ul#tunnel-locator').css('display','none');
		});
	});
}

//Get the latitude and the longitude;

  function initialize() {
//	geocoder = new google.maps.Geocoder();

  }

  function codeLatLng(lat, lng) {
  }


$(document).ready(function(){
	locationHandler.init();
//	$("#getguest_form").submit(function(event){
//		var $form = $(this),
//		$inputs = $form.find("input"),
//		serializedData = $form.serialize();
//		$inputs.attr("disabled", "disabled");
//		$.ajax({
//			url: "curl_api.php",
//			type: "post",
//			data: serializedData,
//			success: function(response, textStatus, jqXHR){
//				console.log("success function fired");
//				console.log(response);
//			},
//			error: function(jqXHR, textStatus, errorThrown){ 
//				console.log("error function fired");
//				console.log("This error occured: " + textStatus, errorThrown);
//			},
//			complete: function(){
//				console.log("complete function fired");
//				$inputs.removeAttr("disabled");   
//			}
//		});
//		event.preventDefault(); 
//	});
});
