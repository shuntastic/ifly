<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

include_once(PATH_THIRD."videoplayer/libraries/videoplayer_service.php");

class Videoplayer_vimeo extends Videoplayer_service {

	var $service_name 		= "Vimeo";
	var $service_key 		= "vimeo";
	var $auth_mode 			= "oauth";
	var $universal_url 		= "http://player.vimeo.com/video/%s";
	var $api_key 			= false;
	var $api_secret			= false;
	var $register_key_url	= "https://developer.vimeo.com/apps/new";
	var $token 				= false;
	var $token_secret		= false;
	var $account_set		= false;

	/**
	 * Constructor
	 *
	 */
	function __construct($params=array())
	{
		$this->EE =& get_instance();
		
		if(isset($params['site_id']))
		{
			$this->site_id = $params['site_id'];
		}
	}

	// --------------------------------------------------------------------

	function _getApiObject()
	{
		$vimeo = false;
		
		if (class_exists('phpVimeo') === false)
		{
			require_once(PATH_THIRD.'videoplayer/libraries/sdks/vimeo/vimeo.php');
		}

		$this->EE->load->model('videoplayer_model');
		
		$this->EE->load->library('logger');

        $account = $this->EE->videoplayer_model->get_account($this->service_key, $this->site_id);
        
        $this->account_set = true;

        if($account)
        {
			$vimeo = new phpVimeo($account->api_key, $account->api_secret);

			$token = $account->oauth_access_token;
			$token_secret = $account->oauth_access_token_secret;

			if($token && $token_secret)
			{
				$this->_token = $token;
				$this->_token_secret = $token_secret;

				$vimeo->setToken($token, $token_secret);
			}
        }
        
        return $vimeo;
	}

	// --------------------------------------------------------------------


	/**
	 * Remove favorite
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function remove_favorite($video_id)
	{
		$vimeo = $this->_getApiObject();

		$params = array(
			'video_id' 		=> $video_id,
			'like'			=> 0
		);

		$vimeo->call('vimeo.videos.setLike', $params);
	}

	// --------------------------------------------------------------------

	/**
	 * Add favorite
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function add_favorite($video_id)
	{
		$vimeo = $this->_getApiObject();

		$params = array(
			'video_id' 		=> $video_id,
			'like'			=> 1
		);

		$vimeo->call('vimeo.videos.setLike', $params);
	}


	// --------------------------------------------------------------------

	/**
	 * Is favorite ?
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	function is_favorite($video_id)
	{
		$videos = $this->get_favorites(0,0);

		if($videos)
		{
			foreach($videos as $v)
			{

				if($v['id'] == $video_id)
				{
					return true;
				}
			}
		}

		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * Oauth
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function oauth($endpoint_url)
	{
		$this->EE->load->helper('url');

		if(!isset($_SESSION))
		{
			session_start();
		}


		$service = $this->EE->input->get_post('service');
		$site_id = $this->EE->input->get_post('site_id');
		
		$redirect = $this->EE->input->get('redirect');

		if(!isset($_SESSION['redirect']))
		{
			$_SESSION['redirect'] = NULL;
		}

		if($_SESSION['redirect'] == NULL)
		{
			$_SESSION['redirect'] = $redirect;
		}

		$callback_url = $endpoint_url."&method=oauth&service=".$service."&site_id=".$site_id;


		// Set up variables
		$state=false;
		if(isset($_SESSION['oauth_state']))
		{
			$state = $_SESSION['oauth_state'];
		} else {
			$_SESSION['oauth_state'] = NULL;
		}

		$request_token= false;
		if(isset($_SESSION['oauth_request_token']))
		{
			$request_token = $_SESSION['oauth_request_token'];
		}else {
			$_SESSION['oauth_request_token'] = NULL;
		}

		$access_token=false;

		if(isset($_SESSION['oauth_access_token']))
		{
			$access_token = $_SESSION['oauth_access_token'];
		} else {
			$_SESSION['oauth_access_token'] = NULL;
		}

		if(!isset($_SESSION['oauth_access_token_secret']))
		{
			$_SESSION['oauth_access_token_secret'] = NULL;
		}

		// Coming back
		if(isset($_REQUEST['oauth_token']))
		{
			if ($_REQUEST['oauth_token'] != NULL && $_SESSION['oauth_state'] === 'start')
			{
			    $_SESSION['oauth_state'] = $state = 'returned';
			}
		}

		// If we have an access token, set it
		if ($_SESSION['oauth_access_token'] != null)
		{
		    $this->services[$service]->setToken($_SESSION['oauth_access_token'], $_SESSION['oauth_access_token_secret']);
		}

        $vimeo = $this->_getApiObject();

		switch ($_SESSION['oauth_state'])
		{
		    default:

		        // Get a new request token
		        
		        $token = $vimeo->getRequestToken($callback_url);
		        
		        //var_dump($token);

		        $_SESSION['oauth_request_token'] = $token['oauth_token'];
		        $_SESSION['oauth_request_token_secret'] = $token['oauth_token_secret'];
		        $_SESSION['oauth_state'] = 'start';


		        // build authorize link

		        $authorize_link = $vimeo->getAuthorizeUrl($token['oauth_token'], 'write');

		        break;

		    case 'returned':

		       if ($_SESSION['oauth_access_token'] === NULL && $_SESSION['oauth_access_token_secret'] === NULL)
		       {

		            $vimeo->setToken($_SESSION['oauth_request_token'], $_SESSION['oauth_request_token_secret']);
		            $token = $vimeo->getAccessToken($_REQUEST['oauth_verifier']);

		            $_SESSION['oauth_access_token'] = $token['oauth_token'];
		            $_SESSION['oauth_access_token_secret'] = $token['oauth_token_secret'];
		            $_SESSION['oauth_state'] = 'done';

		            $vimeo->setToken($_SESSION['oauth_access_token'], $_SESSION['oauth_access_token_secret']);
		        }

		        try
		        {

					$data = array(
						'oauth_access_token' => $_SESSION['oauth_access_token'],
						'oauth_access_token_secret' => $_SESSION['oauth_access_token_secret']
					);

					$this->EE->videoplayer_model->save_account($service, $data, $site_id);

		        	if($this->is_authenticated())
		        	{
						$data = array(
							'enabled' => true,
							'is_authenticated' => true
						);

						$this->EE->videoplayer_model->save_account($service, $data, $site_id);
		        	}


		        	$r = $vimeo->call('vimeo.test.login');

			        $_SESSION["oauth_access_token"] = NULL;
			        $_SESSION["oauth_access_token_secret"] = NULL;
					$_SESSION['oauth_state'] = NULL;

					if($_SESSION['redirect'] != NULL)
					{
		        		$redirect = unserialize(base64_decode($_SESSION['redirect']));
		        		$_SESSION['redirect'] = NULL;
		        		redirect($redirect);
					}
		        }
		        catch (VimeoAPIException $e)
		        {
		            echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";
		        }

		        break;
		}

		if(isset($authorize_link))
		{
			redirect($authorize_link);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Get video id from url
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function get_video_id($url)
	{
		// check if url works with this service and extract video_id
		$video_id = false;

		$regexp = array('/^https?:\/\/(www\.)?vimeo\.com\/([0-9]*)/', 2);


		if(preg_match($regexp[0], $url, $matches, PREG_OFFSET_CAPTURE) > 0)
		{

			// regexp match key

			$match_key = $regexp[1];


			// define video id

			$video_id = $matches[$match_key][0];


			// Fixes the youtube &feature_gdata bug

			if(strpos($video_id, "&"))
			{
				$video_id = substr($video_id, 0, strpos($video_id, "&"));
			}
		}

		// here we should have a valid video_id or false if service not matching

		return $video_id;
	}

	// --------------------------------------------------------------------

	/**
	 * Search videos
	 *
	 * @access	public
	 * @param	string
	 * @param	integer
	 * @param	integer
	 * @return	array
	 */
	function search($q, $page, $per_page)
	{

		try {

			if(!isset($_SESSION))
			{
				session_start();
			}


			$videos = array();

			$vimeo = $this->_getApiObject();

			// $vimeo->enableCache('file', $this->cache_path(), 300);

			$params = array(
				'query' 		=> $q,
				'full_response'	=> 1,
				'page' 			=> $page,
				'per_page'		=> $per_page,
				'format' 		=> 'php'
			);

			$r = $vimeo->call('vimeo.videos.search', $params);

			if($r)
			{
				if(isset($r->videos->video))
				{
					foreach($r->videos->video as $v)
					{
						$video = $this->developerdata($v);
						array_push($videos, $video);
					}

					return $videos;
				}
			}
			else
			{
				throw new Exception("vimeo.search");
			}
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/vimeo.php ';
			$msg .= '"search" method ';
			$msg .= 'returned this error : ';
			$msg .= $e->getMessage();
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);	
			}
		}

	}

	// --------------------------------------------------------------------

	/**
	 * Get favorite videos
	 *
	 * @access	public
	 * @param	integer
	 * @param	integer
	 * @return	array
	 */
	function get_favorites($page, $per_page)
	{
		try {

			$videos = array();

			$vimeo = $this->_getApiObject();

			$params = array(
				'full_response'	=> 1,
				'format' 		=> 'php'
			);

			if($page > 0)
			{
				$params['page'] = $page;
			}

			if($per_page > 0)
			{
				$params['per_page'] = $per_page;
			}


			//$vimeo->enableCache('file', $this->cache_path(), 5);

			$r = $vimeo->call('vimeo.videos.getLikes', $params);

			if($r)
			{
				if(isset($r->videos->video))
				{
					foreach($r->videos->video as $v)
					{

						$video = $this->developerdata($v);

						array_push($videos, $video);
					}

					return $videos;
				}
			}
			else
			{
				throw new Exception("vimeo.videos.getLikes ".print_r($r, true));
			}
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/vimeo.php ';
			$msg .= '"get_favorites" method ';
			$msg .= 'returned this error : ';
			$msg .= $e->getMessage();

			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Get my videos
	 *
	 * @access	public
	 * @param	integer
	 * @param	integer
	 * @return	array
	 */
	function get_videos()
	{

		try {
			$videos = array();

			$vimeo = $this->_getApiObject();

			$params = array(
				'full_response'	=> true,
				'per_page' 		=> 20,
				'format' 		=> 'php'
			);

			// $vimeo->enableCache('file', $this->cache_path(), 5);

			$r = $vimeo->call('vimeo.videos.getUploaded', $params);

			if($r)
			{
				if(isset($r->videos->video))
				{
					foreach($r->videos->video as $v)
					{
						$video = $this->developerdata($v);

						array_push($videos, $video);
					}

					return $videos;
				}
			}
			else
			{
				throw new Exception("vimeo.videos.getUploaded ".print_r($r, true));
			}
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/vimeo.php ';
			$msg .= '"get_videos" method ';
			$msg .= 'returned this error : ';
			$msg .= $e->getMessage();
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Get video developer data
	 *
	 * @access	private
	 * @param	array
	 * @return	array
	 */
	private function developerdata($item)
	{
		$video = array();
		$video['service_name'] 	= "Vimeo";
		$video['id'] 		= (string) $item->id;
		$video['title'] 		= (string) $item->title;
		$video['description'] 	= (string) $item->description;
		$video['username']		= (string) $item->owner->display_name;
		$video['author'] 		= (string) $item->owner->display_name;
		$video['date'] 			= (string) strftime("%d/%m/%Y", strtotime($item->upload_date));
		$video['plays']			= (string) $item->number_of_plays;
		$video['duration'] 		= (string) $item->duration;
		$video['thumbnail'] 	= (string) $item->thumbnails->thumbnail[0]->_content;


		// find the video url

		$v_urls = $item->urls->url;

		foreach($v_urls as $v_url)
		{
			if($v_url->type == "video")
			{
				$video['video_page'] = (string) $v_url->_content;
			}
		}

		return $video;
	}

	// --------------------------------------------------------------------

	/**
	 * Get video meta data
	 *
	 * @access	private
	 * @param	array
	 * @return	array
	 */
	function metadata($video)
	{
		// api call

		try {

			$videos = array();

			$vimeo = $this->_getApiObject();
			
			$method = 'vimeo.videos.getInfo';

			$params = array(
				'video_id' => $video['id']
			);
			
			$vimeo->enableCache(phpVimeo::CACHE_FILE, $this->cache_path(), 600);

			$r = $vimeo->call($method, $params);

			if($r)
			{
				$v = $r->video[0];

				// assign new variables
				
				$video['title'] 			= (string) $v->title;
				$video['description'] 		= (string) $v->description;
				$video['username'] 			= (string) $v->owner->username;
				$video['author'] 			= (string) $v->owner->display_name;
				$video['date'] 				= (string) $v->upload_date;
				$video['plays'] 			= (string) $v->number_of_plays;
				$video['duration'] 			= (string) $v->duration;
				$video['thumbnail'] 		= (string) $v->thumbnails->thumbnail[0]->_content;
				
				
				// try to get XL thubmnail
				
				if(isset($v->thumbnails->thumbnail[3]->_content))
				{					
					if($v->thumbnails->thumbnail[3]->_content !== false)
					{
						$video['thumbnail_large'] 	= (string) $v->thumbnails->thumbnail[3]->_content;	
					}
				}
				
				if($video['thumbnail_large'] == false && isset($v->thumbnails->thumbnail[2]->_content))
				{
					// Fallback to L size if XL doesn't exists
					
					$video['thumbnail_large'] 	= (string) $v->thumbnails->thumbnail[2]->_content;	
				}
				
				$video['vimeo_thumbnail_0'] 		= (string) $v->thumbnails->thumbnail[0]->_content;
				
				if(isset($v->thumbnails->thumbnail[1]->_content))
				{
					$video['vimeo_thumbnail_1'] = (string) $v->thumbnails->thumbnail[1]->_content;
				}
				if(isset($v->thumbnails->thumbnail[2]->_content))
				{
					$video['vimeo_thumbnail_2'] = (string) $v->thumbnails->thumbnail[2]->_content;
				}
				
				if(isset($v->thumbnails->thumbnail[3]->_content))
				{
					$video['vimeo_thumbnail_3'] = (string) $v->thumbnails->thumbnail[3]->_content;
				}
				
				$video['date'] = strtotime($video['date']);
				
				return $video;
			}
			else
			{
				$error = $method;
				$error .= print_r($r, true);
				
				throw new Exception($error);
			}
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/vimeo.php ';
			$msg .= '"metadata" method ';
			$msg .= 'returned this error : ';
			$msg .= $e->getMessage();
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}

		return false;
	}

	// --------------------------------------------------------------------
	
	function get_profile()
	{
		$profile = array(
			'display_name' => "",
			'username' => ""
		);	

		try {

			$videos = array();

			$vimeo = $this->_getApiObject();
			
			$method = 'vimeo.people.getInfo';

			$params = array();


			//$vimeo->enableCache('file', $this->cache_path(), 5);

			$r = $vimeo->call($method, $params);

			if($r)
			{
				$profile['display_name'] = $r->person->display_name;
				$profile['username'] = $r->person->username;
			}
			else
			{
				$error = $method;
				$error .= print_r($r, true);
				
				throw new Exception($error);
			}
		}
		catch(Exception $e)
		{
			$msg = 'videoplayer/lirabries/services/vimeo.php ';
			$msg .= '"get_profile" method ';
			$msg .= 'returned this error : ';
			$msg .= $e->getMessage();
			
			if(method_exists($this->EE->logger, 'developer'))
			{
				$this->EE->logger->developer($msg, true);
			}
		}
		
		return $profile;
	}
	
	// --------------------------------------------------------------------
	/**
	 * Are we authenticated on vimeo ?
	 *
	 * @access	private
	 * @return	bool
	 */
	function is_authenticated()
	{
        try
        {
			$vimeo = $this->_getApiObject();


			$params = array(
				'format' 		=> 'php'
			);

			$r = $vimeo->call('vimeo.videos.getLikes', $params);

			return true;
        }
        catch (Exception $e)
        {
           // echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";

           return false;
        }
	}

	// --------------------------------------------------------------------

	/**
	 * Ping checks if we provided correct API Key & Secret
	 *
	 * @access	private
	 * @return	bool
	 */
	function ping()
	{
		$vimeo = $this->_getApiObject();

		if(!$vimeo)
		{
			return false;
		}

		try
        {
			$r = $vimeo->call('vimeo.test.echo');
			return true;
        }
        catch (VimeoAPIException $e)
        {
            // echo "Encountered an API error -- code {$e->getCode()} - {$e->getMessage()}";
            return false;
        }
	}
}

// END Videoplayer_vimeo class

/* End of file vimeo.php */
/* Location: ./system/expressionengine/third_party/videoplayer/libraries/services/vimeo.php */