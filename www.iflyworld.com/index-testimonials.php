<!DOCTYPE html>   
<html> 
<head>
<meta property="fb:admins" content="80717587543" />

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>iFLY</title>
<link href="https://plus.google.com/104652435906847083124" rel="publisher" />


	<meta name="description" content="">
	<meta name="keywords" content="" />
	<meta name="author" content="">
<meta name="viewport" content="width=device-width; initial-scale=0.2; maximum-scale=1.0; user-scalable=1;">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
    
	 <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33782739-1']);
  _gaq.push(['_setDomainName', 'iflyworld.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

     
      
	<link rel="stylesheet" href="css/styles.css">
    
    <script>
	
	start = 0;
	end = 18;
	
	function getDocHeight() {
    var D = document;
    return Math.max(
    Math.max(D.body.scrollHeight, D.documentElement.scrollHeight),
    Math.max(D.body.offsetHeight, D.documentElement.offsetHeight),
    Math.max(D.body.clientHeight, D.documentElement.clientHeight)
	)};

	jQuery(function($){
			getMore();
			getMore();
			
			 
			
	});
	
 
	$(window).scroll(function() {
       if($(window).scrollTop() + $(window).height() == getDocHeight()) {
          getMore();
       }
   });

	
	function getMore(){
			var url = "recent_uploads.php?start=" + start + "&end=" + end;
			start = start + 18;
			end = end + 18;
			
			$.get(url,
				function(response){				  
					 
					$('#video').html( $('#video').html() + response);					 					
					
			});
			
	}
	
	</script>
    
    <style type="text/css">
<!--
.style1 {font-size: 24px}
-->
    </style>
</head>
<body>
	
	<div id="wrapper" style="max-width: 9500px;">
		<div id="lines1"></div>
		<div id="background01"></div>
		<div id="background02"></div>
		
		<div id="lines2"></div>
		<header>
			<h2 align="center">IN FLIGHT MOVIES</h2>
		</header>
        
        
        
      
        
  <section id="video" class="container" style="background-color:rgba(0,0,0,0.3);">
  
    <div style="width:900px; margin:15px;">
  
      <h3 align="left">Come and experience indoor skydiving at iFLY!</h3>
         <p align="left">It is safe for   kids, challenging for adults, exciting for teens and realistic for   skydivers. No experience necessary and it is great fun for all ages,   three and up!
            
           Our vertical wind tunnels allow you to soar on a column of air  in a controlled environment 
        where you experience the   realistic thrill of freefall.  
        Simply put, <strong>iFLY is flying</strong>. <br>
         &nbsp;</p>
         <h3 align="left">Scroll down to hear what some recent flyers 
         had to say about their flights</h3>
         </div>
  </section>
  
  <div id="morebtn" align="center" onClick="getMore();" style="margin:10px; cursor:pointer;"><span class="button style1">more...</span>
  </div>
  <footer>
			<div align="center">
			  <ul>
			    <li id="footer_logo">
			      © Copyright 2007-2012 iFLY USA				</li>
			    <li id="footer_facebook"></li>
			    <li id="footer_twitter"></li>
		      </ul>
    </div>
  </footer>
	</div>
</body>
</html>