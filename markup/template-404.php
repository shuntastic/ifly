<?php include("inc/head.php"); ?>
<body class="info-page one-col">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<aside>
						<img src="/images/404.jpg" alt="404 error"  class="rounded-corners gradient-border">
					</aside>

					<article>
						<h1 class="uppercase">Um . . . Congratulations. <br>You broke the internet. </h1>
						<p>The page you're looking for no longer exists. Please use the navigation above to find what you're looking for. </p>
					</article>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>