-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 27, 2012 at 12:34 AM
-- Server version: 5.1.44
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iflyworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(2, 'Cp_analytics_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '2.0.7'),
(3, 'Mx_extended_content_menu_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3'),
(4, 'Template_variables_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.2.1'),
(5, 'Nsm_morphine_theme_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '2.0.3');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(24, 'Navee', 'add_navigation_handler'),
(25, 'Playa_mcp', 'filter_entries'),
(28, 'Freeform', 'save_form'),
(29, 'Mailinglist', 'insert_new_email'),
(30, 'Mailinglist', 'authorize_email'),
(31, 'Mailinglist', 'unsubscribe'),
(32, 'Videoplayer', 'ajax');

-- --------------------------------------------------------

--
-- Table structure for table `exp_better_pages_hidden`
--

CREATE TABLE IF NOT EXISTS `exp_better_pages_hidden` (
  `bpHidden_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `template_id` int(10) DEFAULT NULL,
  `field_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`bpHidden_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_better_pages_hidden`
--

INSERT INTO `exp_better_pages_hidden` (`bpHidden_id`, `site_id`, `group_id`, `template_id`, `field_id`) VALUES
(2, 1, 6, 34, 29),
(3, 1, 6, 34, 64),
(4, 1, 6, 36, 64),
(5, 1, 6, 35, 29);

-- --------------------------------------------------------

--
-- Table structure for table `exp_better_pages_templates`
--

CREATE TABLE IF NOT EXISTS `exp_better_pages_templates` (
  `bpTemplate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `template_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`bpTemplate_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_better_pages_templates`
--

INSERT INTO `exp_better_pages_templates` (`bpTemplate_id`, `site_id`, `group_id`, `template_id`) VALUES
(2, 1, 6, 34),
(3, 1, 6, 36),
(4, 1, 6, 35);

-- --------------------------------------------------------

--
-- Table structure for table `exp_better_pages_thumbs`
--

CREATE TABLE IF NOT EXISTS `exp_better_pages_thumbs` (
  `bp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `upload_dir_id` int(10) DEFAULT NULL,
  `template_id` int(10) DEFAULT NULL,
  `thumb` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`bp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_better_pages_thumbs`
--

INSERT INTO `exp_better_pages_thumbs` (`bp_id`, `site_id`, `upload_dir_id`, `template_id`, `thumb`) VALUES
(2, 1, 1, 18, 'temp_file_template-image1.png');

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_captcha`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_categories`
--

INSERT INTO `exp_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`) VALUES
(1, 1, 1, 0, 'Image', 'image', '', '0', 1),
(2, 1, 1, 0, 'Video', 'video', '', '0', 2),
(3, 1, 1, 0, 'iFrame', 'iframe', '', '0', 3);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_category_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_category_field_data`
--

INSERT INTO `exp_category_field_data` (`cat_id`, `site_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_category_groups`
--

INSERT INTO `exp_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `exclude_group`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`) VALUES
(1, 1, 'Media Type', 'c', 0, 'all', '6|7', '6|7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_category_posts`
--

INSERT INTO `exp_category_posts` (`entry_id`, `cat_id`) VALUES
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'tunnels', 'Tunnels', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(2, 1, 'hero', 'Hero', 'http://dev.iflyworld.com/index.php', NULL, 'en', 0, 0, 0, 0, NULL, 1, 'open', 2, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'tiles', 'Tiles', 'http://dev.iflyworld.com/index.php', '', 'en', 1, 0, 1351275818, 0, NULL, 1, 'open', 3, 13, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(4, 1, 'flier_type', 'Flier Type Detail', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 4, 20, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(6, 1, 'reviews', 'Flyer Feedback', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 5, 24, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(7, 1, 'template_one', 'Template - 1 Column', 'http://dev.iflyworld.com/index.php', '', 'en', 5, 0, 1351199791, 0, NULL, 1, 'open', 6, 29, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 37),
(8, 1, 'events', 'Events', 'http://dev.iflyworld.com/index.php', NULL, 'en', 1, 0, 1348268399, 0, NULL, 1, 'open', 7, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(10, 1, 'template_two', 'Template - 2 Column', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, '1', 1, 'open', 6, 29, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(11, 3, 'call_to_action', 'Call to Action', 'http://dev.iflyworld.com/index.php', '', 'en', 1, 0, 1349910370, 0, '', 2, 'open', 9, 43, '', 'n', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'n', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(12, 3, 'events', 'Events', 'http://dev.iflyworld.com/index.php', NULL, 'en', 1, 0, 1348268399, 0, '', 2, 'open', 10, 0, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(13, 3, 'tiles', 'Tiles', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, '', 2, 'open', 11, 47, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(14, 3, 'tunnels', 'Tunnels', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, '', 2, 'open', 12, 52, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(15, 4, 'call_to_action', 'Call to Action', 'http://dev.iflyworld.com/index.php', '', 'en', 1, 0, 1349910370, 0, '', 2, 'open', 9, 43, '', 'n', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'n', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(16, 1, 'booking', 'Booking Pages', 'http://ifly.dev/', '', 'en', 6, 0, 1351107282, 0, '', 1, 'open', 13, 62, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 32),
(17, 1, 'home_page', 'Home Page', 'http://ifly.dev/', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 14, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(18, 1, 'curated_media', 'Curated Media', 'http://ifly.dev/', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 15, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(19, 1, 'flyer_guide', 'Flight Info - Flyer Guide', 'http://ifly.dev/', '', 'en', 1, 0, 1351294379, 0, '', 1, 'open', 16, 81, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 43),
(20, 1, 'flight_info_gift_cards', 'Flight Info - Gift Cards', 'http://ifly.dev/', '', 'en', 1, 0, 1351292784, 0, '', 1, 'open', 17, 83, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 41),
(21, 1, 'tunnels_other', 'Tunnels - non particpating', 'http://ifly.dev/', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 18, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_37` text,
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_41` text,
  `field_ft_41` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  `field_id_43` text NOT NULL,
  `field_ft_43` tinytext,
  `field_id_44` text NOT NULL,
  `field_ft_44` tinytext,
  `field_id_45` text NOT NULL,
  `field_ft_45` tinytext,
  `field_id_46` text NOT NULL,
  `field_ft_46` tinytext,
  `field_id_47` text NOT NULL,
  `field_ft_47` tinytext,
  `field_id_48` text NOT NULL,
  `field_ft_48` tinytext,
  `field_id_49` text NOT NULL,
  `field_ft_49` tinytext,
  `field_id_51` text NOT NULL,
  `field_ft_51` tinytext,
  `field_id_52` text NOT NULL,
  `field_ft_52` tinytext,
  `field_id_53` text NOT NULL,
  `field_ft_53` tinytext,
  `field_id_54` text NOT NULL,
  `field_ft_54` tinytext,
  `field_id_55` text NOT NULL,
  `field_ft_55` tinytext,
  `field_id_56` text NOT NULL,
  `field_ft_56` tinytext,
  `field_id_57` text NOT NULL,
  `field_ft_57` tinytext,
  `field_id_58` text NOT NULL,
  `field_ft_58` tinytext,
  `field_id_59` text NOT NULL,
  `field_ft_59` tinytext,
  `field_id_60` text NOT NULL,
  `field_ft_60` tinytext,
  `field_id_61` text NOT NULL,
  `field_ft_61` tinytext,
  `field_id_62` text,
  `field_ft_62` tinytext,
  `field_id_63` text,
  `field_ft_63` tinytext,
  `field_id_64` text,
  `field_ft_64` tinytext,
  `field_id_65` text,
  `field_ft_65` tinytext,
  `field_id_66` text,
  `field_ft_66` tinytext,
  `field_id_67` text,
  `field_ft_67` tinytext,
  `field_id_68` text,
  `field_ft_68` tinytext,
  `field_id_69` text,
  `field_ft_69` tinytext,
  `field_id_70` text,
  `field_ft_70` tinytext,
  `field_id_71` text,
  `field_ft_71` tinytext,
  `field_id_72` text,
  `field_ft_72` tinytext,
  `field_id_73` text,
  `field_ft_73` tinytext,
  `field_id_74` text,
  `field_ft_74` tinytext,
  `field_id_75` text,
  `field_ft_75` tinytext,
  `field_id_76` text,
  `field_ft_76` tinytext,
  `field_id_77` text,
  `field_ft_77` tinytext,
  `field_id_78` text,
  `field_ft_78` tinytext,
  `field_id_79` text,
  `field_ft_79` tinytext,
  `field_id_80` text,
  `field_ft_80` tinytext,
  `field_id_81` text,
  `field_ft_81` tinytext,
  `field_id_82` text,
  `field_ft_82` tinytext,
  `field_id_83` text,
  `field_ft_83` tinytext,
  `field_id_84` text,
  `field_ft_84` tinytext,
  `field_id_85` text,
  `field_ft_85` tinytext,
  `field_id_86` text,
  `field_ft_86` tinytext,
  `field_id_87` text,
  `field_ft_87` tinytext,
  `field_id_88` text,
  `field_ft_88` tinytext,
  `field_id_89` text,
  `field_ft_89` tinytext,
  `field_id_90` text,
  `field_ft_90` tinytext,
  `field_id_91` text,
  `field_ft_91` tinytext,
  `field_id_92` text,
  `field_ft_92` tinytext,
  `field_id_93` text,
  `field_ft_93` tinytext,
  `field_id_94` text,
  `field_ft_94` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_11`, `field_ft_11`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_17`, `field_ft_17`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_29`, `field_ft_29`, `field_id_30`, `field_ft_30`, `field_id_33`, `field_ft_33`, `field_id_34`, `field_ft_34`, `field_id_35`, `field_ft_35`, `field_id_37`, `field_ft_37`, `field_id_38`, `field_ft_38`, `field_id_39`, `field_ft_39`, `field_id_40`, `field_ft_40`, `field_id_41`, `field_ft_41`, `field_id_42`, `field_ft_42`, `field_id_43`, `field_ft_43`, `field_id_44`, `field_ft_44`, `field_id_45`, `field_ft_45`, `field_id_46`, `field_ft_46`, `field_id_47`, `field_ft_47`, `field_id_48`, `field_ft_48`, `field_id_49`, `field_ft_49`, `field_id_51`, `field_ft_51`, `field_id_52`, `field_ft_52`, `field_id_53`, `field_ft_53`, `field_id_54`, `field_ft_54`, `field_id_55`, `field_ft_55`, `field_id_56`, `field_ft_56`, `field_id_57`, `field_ft_57`, `field_id_58`, `field_ft_58`, `field_id_59`, `field_ft_59`, `field_id_60`, `field_ft_60`, `field_id_61`, `field_ft_61`, `field_id_62`, `field_ft_62`, `field_id_63`, `field_ft_63`, `field_id_64`, `field_ft_64`, `field_id_65`, `field_ft_65`, `field_id_66`, `field_ft_66`, `field_id_67`, `field_ft_67`, `field_id_68`, `field_ft_68`, `field_id_69`, `field_ft_69`, `field_id_70`, `field_ft_70`, `field_id_71`, `field_ft_71`, `field_id_72`, `field_ft_72`, `field_id_73`, `field_ft_73`, `field_id_74`, `field_ft_74`, `field_id_75`, `field_ft_75`, `field_id_76`, `field_ft_76`, `field_id_77`, `field_ft_77`, `field_id_78`, `field_ft_78`, `field_id_79`, `field_ft_79`, `field_id_80`, `field_ft_80`, `field_id_81`, `field_ft_81`, `field_id_82`, `field_ft_82`, `field_id_83`, `field_ft_83`, `field_id_84`, `field_ft_84`, `field_id_85`, `field_ft_85`, `field_id_86`, `field_ft_86`, `field_id_87`, `field_ft_87`, `field_id_88`, `field_ft_88`, `field_id_89`, `field_ft_89`, `field_id_90`, `field_ft_90`, `field_id_91`, `field_ft_91`, `field_id_92`, `field_ft_92`, `field_id_93`, `field_ft_93`, `field_id_94`, `field_ft_94`) VALUES
(1, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '{"start_date":"2012-09-21","start_time":"17:30","end_time":"18:30","end_date":"2012-09-21","all_day":"n"}', 'xhtml', '<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>\n<p>We will be creating a media frenzy around women in the sport of skydiving and celebrating women in flight!</p>\n<p>$35 registration (See breakdown below.)</p>\n<p>$500/30 minutes (before taxes) including coaching/organizing (Melanie Curtis, Melissa Nelson, Kimberly Winslow, Brianne Thompson, Amy Chemelecki and Cat Adam</p>\n<p>Lots of load organizing and Huck Jams to promote the big sister little sister bonding!</p>\n<p>After flight activities for those who want to continue the bonding experience, will include: a sleep in, wine tasting,yoga and more...</p>\n<p>Registration cost break down:\n\nNOTE: Anyone wanting to make an additional donation and meet the President of the LFL, (who is a skydiver affected by this auto-immune disease), we will have a donation box ready and you can make out your donation directly to the LFL.</p><ul><li>$10- T-shirt\n\n</li><li>$5- Yoga Instructor\n</li><li>$5- Massage\n</li><li>$5- Meal\n</li><li>$9- Essential Items\n</li><li>$1- Charity Donation - Leap for LUPUS Foundation - A charity started by skydivers!&nbsp;</li></ul>', 'xhtml', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis arcu nibh. Morbi gravida eros vitae leo pulvinar in dictum dolor volutpat. Nunc auctor ante id mauris porttitor iaculis. Curabitur mattis aliquam purus at convallis. Mauris eget risus non nibh placerat aliquam quis sed est. Aliquam erat volutpat. Donec turpis urna, tempor vel bibendum sed, dignissim at sem.</span></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis arcu nibh. Morbi gravida eros vitae leo pulvinar in dictum dolor volutpat. Nunc auctor ante id mauris porttitor iaculis. Curabitur mattis aliquam purus at convallis. Mauris eget risus non nibh placerat aliquam quis sed est. Aliquam erat volutpat. Donec turpis urna, tempor vel bibendum sed, dignissim at sem.\n</p>', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(3, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​Some default copy</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Contact Form', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(4, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Pick which type of flyer you are to get more info or to book a flight.', 'none', '<p>​This is some page copy.</p>\n', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(5, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Choose when you want to fly, what type of flyer you are, and how many of you are going.\nClick “Next” to see our personal recommendations for you.', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(6, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(7, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(8, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(9, 1, 16, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(10, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​Lorem ipsum blah blah blah</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Contact Form', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(11, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​This is some FAQ copy.</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Contact Form', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(12, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​This is some copy.</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '3|', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'http://vimeo.com/4385750', 'xhtml', '35', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(13, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '34', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(14, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '3|', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '34', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(15, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span></p><p><span><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>\n</span></p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'http://vimeo.com/50927467', 'xhtml', '35', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(16, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '<p>​This is some copy.</p>', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '3|', 'xhtml', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'http://vimeo.com/50927467', 'xhtml', '35', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(17, 1, 3, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</span>', 'xhtml', '1 day special offer', 'none', '', 'none', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'xhtml', 'Image', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(18, 1, 20, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<ul><li>Never expires</li><li>Works online and at your iFLY location</li><li>Arrives by email within 1 hour</li></ul>', 'xhtml', 'The perfect gift for the person who has everything', 'none', '1', 'none', '1', 'none', '<p>​Check your balance</p>', 'xhtml', 'here.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none'),
(19, 1, 19, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '1', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Pick Which Type of Flyer You Are to Get More Info or Book a Flight', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'none');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_channel_entries_autosave`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=95 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'tunnel_brief_description', 'Brief Description', 'Some instructions will live here.', 'rte', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(2, 1, 1, 'tunnel_thumbnail_image', 'Thumbnail Image', 'Image dimensions : 100px x 100px', 'file', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(3, 1, 1, 'tunnel_address', 'Address', 'Physical Address', 'textarea', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(4, 1, 1, 'tunnel_copyright', 'Copyright', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(5, 1, 1, 'tunnel_contact_email', 'Contact Email', 'Email address where email correspondence should be directed.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(6, 1, 1, 'tunnel_contact_phone', 'Contact Phone', 'Main phone number to handle enquiries.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(7, 1, 1, 'tunnel_facebook', 'Social Media - Facebook Page', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(8, 1, 1, 'tunnel_twitter', 'Social Media - Twitter Account', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(9, 1, 2, 'hero_tab_link', 'Tab Link', 'Maximum 17 characters. This is used to populate the navigation links.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 17, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(10, 1, 2, 'hero_description', 'Main Description', 'This will appear above the image or video.', 'rte', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(11, 1, 2, 'hero_image', 'Image', 'Choose this field if no video is to be used.', 'file', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(13, 1, 3, 'tile_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(14, 1, 3, 'tile_button', 'Button', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 20, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtpOjk7aToxO2k6MTA7fX0='),
(15, 1, 3, 'tile_image', 'Image', 'Dimensions : 320px x 180px', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(17, 1, 3, 'tile_iframe_link', 'iFrame Link', 'Link to external content you want to include.\n\nFormat : http://example.com/page.html', 'text', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(20, 1, 4, 'flier_detail_images', 'Image Gallery', 'Maximum of 4 images.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtpOjE1O2k6MTtpOjE2O2k6MjtpOjE3O2k6MztpOjE4O319'),
(21, 1, 4, 'flier_detail_copy', 'Detail Page : Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(24, 1, 5, 'testimonial_copy', 'Testimonial', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(25, 1, 5, 'testimonial_image', 'Image', '', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(29, 1, 6, 'info_image', 'Image', 'If present this image will be floated to the right of the main content.', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(30, 1, 6, 'info_copy', 'Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(33, 1, 7, 'event_date', 'Event Date', '', 'low_events', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjI6IjMwIjtzOjE2OiJkZWZhdWx0X2R1cmF0aW9uIjtzOjI6IjYwIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(34, 1, 7, 'event_description', 'Event Description', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(35, 1, 7, 'event_lede', 'Event Lede', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(37, 1, 1, 'tunnel_geo_location', 'Geo Location', '', 'mx_google_map', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 9, 'any', 'YToxMjp7czo4OiJsYXRpdHVkZSI7czoxNzoiNDQuMDYxOTMyOTc4NjUzNDgiO3M6OToibG9uZ2l0dWRlIjtzOjE5OiItMTIxLjI3NTg0NDU3Mzk3NDYxIjtzOjQ6Inpvb20iO3M6MjoiMTMiO3M6MTA6Im1heF9wb2ludHMiO3M6MToiMSI7czo0OiJpY29uIjtzOjg6ImhvbWUucG5nIjtzOjk6InNsaWRlX2JhciI7czoxOiJ5IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(38, 1, 8, 'cta_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(39, 1, 1, 'tunnel_hours', 'Tunnel Hours', '', 'text', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(40, 1, 2, 'hero_type', 'Hero Type', '', 'radio', 'Image\nVideo', 'n', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(41, 1, 5, 'testimonial_type', 'Media Type', '', 'radio', 'Image\nVideo', 'n', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(42, 1, 6, 'info_form', 'Forms', '', 'freeform', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 6, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(43, 3, 9, 'cta_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(44, 3, 10, 'event_date', 'Event Date', '', 'low_events', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjI6IjMwIjtzOjE2OiJkZWZhdWx0X2R1cmF0aW9uIjtzOjI6IjYwIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(45, 3, 10, 'event_description', 'Event Description', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(46, 3, 10, 'event_lede', 'Event Lede', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(47, 3, 11, 'tile_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(48, 3, 11, 'tile_button_text', 'Button Text', 'Maximum 20 characters.', 'text', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 20, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(49, 3, 11, 'tile_image', 'Image', 'Dimensions : 320px x 180px', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(51, 3, 11, 'tile_iframe_link', 'iFrame Link', 'Link to external content you want to include.\n\nFormat : http://example.com/page.html', 'text', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(52, 3, 12, 'tunnel_brief_description', 'Brief Description', 'Some instructions will live here.', 'rte', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(53, 3, 12, 'tunnel_thumbnail_image', 'Thumbnail Image', 'Image dimensions : 100px x 100px', 'file', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(54, 3, 12, 'tunnel_address', 'Address', 'Physical Address', 'textarea', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(55, 3, 12, 'tunnel_copyright', 'Copyright', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(56, 3, 12, 'tunnel_contact_email', 'Contact Email', 'Email address where email correspondence should be directed.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(57, 3, 12, 'tunnel_contact_phone', 'Contact Phone', 'Main phone number to handle enquiries.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(58, 3, 12, 'tunnel_facebook', 'Social Media - Facebook Page', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(59, 3, 12, 'tunnel_twitter', 'Social Media - Twitter Account', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(60, 3, 12, 'tunnel_geo_location', 'Geo Location', '', 'mx_google_map', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 9, 'any', 'YToxMjp7czo4OiJsYXRpdHVkZSI7czoxNzoiNDQuMDYxOTMyOTc4NjUzNDgiO3M6OToibG9uZ2l0dWRlIjtzOjE5OiItMTIxLjI3NTg0NDU3Mzk3NDYxIjtzOjQ6Inpvb20iO3M6MjoiMTMiO3M6MTA6Im1heF9wb2ludHMiO3M6MToiMSI7czo0OiJpY29uIjtzOjg6ImhvbWUucG5nIjtzOjk6InNsaWRlX2JhciI7czoxOiJ5IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(61, 3, 12, 'tunnel_hours', 'Tunnel Hours', '', 'text', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(62, 1, 13, 'sub_heading', 'Sub Heading', 'This will display directly below the title and will be centered on the page. You are allowed up to 128 characters.', 'textarea', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(63, 1, 13, 'page_copy', 'Page Copy', 'Get stuck in. There are no fixed rules for this field, just keep in mind the page and layout it is meant to appear in.', 'rte', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 10, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(64, 1, 6, 'info_video', 'Video', '', 'videoplayer', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(65, 1, 6, 'info_template', 'Template', '', 'better_pages', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 5, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(66, 1, 2, 'hero_video', 'Video', '', 'videoplayer', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 5, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(67, 1, 2, 'hero_button_text', 'Button Text', 'This text will appear under the play icon in the video thumbnail. Maximum of 18 characters.', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 18, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(68, 1, 14, 'home_heros', 'Heros', '', 'playa', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YToxMjp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoxOiIyIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(69, 1, 14, 'home_tiles', 'Tiles', '', 'playa', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YToxMjp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoxOiIzIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(70, 1, 14, 'home_feedback_video', 'Feedback - Video', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO2k6MjtzOjE6IjMiO319'),
(71, 1, 14, 'home_feedback_reviews', 'Feedback - Reviews', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtpOjQ7aToxO2k6NTtpOjI7aTo2O319'),
(72, 1, 14, 'home_flight_wizard', 'Flight Wizard', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjciO2k6MTtzOjE6IjgiO319'),
(73, 1, 3, 'tile_video', 'Video', '', 'videoplayer', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 5, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(74, 1, 3, 'tile_type', 'Tile Type', '', 'radio', 'Image\nVideo\niFrame\nCalendar', 'n', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(75, 1, 15, 'media_type', 'Media Type', '', 'radio', 'Image\nVideo', 'n', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(76, 1, 15, 'gallery_image', 'Image', 'Maximum dimensions 840px x 500px.\n\nCMS will automatically create a thumbnail.', 'file', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(77, 1, 15, 'gallery_video', 'Video', '', 'videoplayer', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(78, 1, 14, 'home_curated_media', 'Curated Media', '', 'playa', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YToxMjp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoyOiIxOCI7fXM6Nzoib3JkZXJieSI7czo1OiJ0aXRsZSI7czo0OiJzb3J0IjtzOjM6IkFTQyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(79, 1, 14, 'home_curated_media_title', 'Curated Media Title', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(80, 1, 4, 'form', 'Form', 'If you want to include a form in the page footer.', 'freeform', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(81, 1, 16, 'fg_types', 'Types', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjtpOjI7czoyOiIxMyI7aTozO3M6MjoiMTQiO319'),
(82, 1, 4, 'videos_for_gallery', 'Videos for Gallery', 'Maximum 4', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjQ6e2k6MDtpOjE5O2k6MTtpOjIwO2k6MjtpOjIxO2k6MztpOjIyO319'),
(83, 1, 17, 'gift_copy', 'Copy', '', 'rte', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(84, 1, 17, 'gift_sub', 'Sub Headline', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(85, 1, 17, 'gift_button_redeem', 'Button - Redeem Gift Card', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjI6IjIzIjtpOjE7czoyOiIyNCI7fX0='),
(86, 1, 17, 'gift_button_buy', 'Button  - Buy Digital Gift Card', '', 'matrix', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MToiMSI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjI6IjI1IjtpOjE7czoyOiIyNiI7fX0='),
(87, 1, 17, 'gift_check_balance_copy', 'Check Balance Copy', '', 'rte', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 5, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(88, 1, 17, 'gift_modal_link', 'Check Balance Link text to launch modal window', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(89, 1, 16, 'fg_sub', 'fg_sub', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(90, 1, 18, 't_descriptor', 'Descriptor', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(91, 1, 18, 't_hours', 'Hours', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(92, 1, 18, 't_phone', 'Phone', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(93, 1, 18, 't_location', 'Location', '', 'mx_google_map', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YToxMjp7czo4OiJsYXRpdHVkZSI7czoxNzoiNDQuMDYxOTMyOTc4NjUzNDgiO3M6OToibG9uZ2l0dWRlIjtzOjE5OiItMTIxLjI3NTg0NDU3Mzk3NDYxIjtzOjQ6Inpvb20iO3M6MjoiMTMiO3M6MTA6Im1heF9wb2ludHMiO3M6MToiMyI7czo0OiJpY29uIjtzOjA6IiI7czo5OiJzbGlkZV9iYXIiO3M6MToieSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(94, 1, 18, 't_web_link', 'Web Link', '', 'text', '', '0', 0, 0, 'channel', 16, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_member_groups`
--

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`) VALUES
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(7, 6),
(7, 7),
(7, 8),
(7, 9);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 8, 1, 0, NULL, '108.13.107.180', 'Test Event 1', 'test-event-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348268399, 'n', '2012', '09', '21', 0, 0, 20120925214000, 0, 0),
(3, 1, 7, 1, 0, NULL, '98.149.249.30', 'Test entry', 'test-entry', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1349988505, 'n', '2012', '10', '11', 0, 0, 20121011154825, 0, 0),
(4, 1, 16, 1, 0, NULL, '127.0.0.1', 'Flyer Guide', 'flyer-guide', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351042864, 'n', '2012', '10', '23', 0, 0, 20121024193805, 0, 0),
(5, 1, 16, 1, 0, NULL, '127.0.0.1', 'Booking Details', 'booking-details', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351107076, 'n', '2012', '10', '24', 0, 0, 20121024143116, 0, 0),
(6, 1, 16, 1, 0, NULL, '127.0.0.1', 'Here are your recommended packages', 'recommended-packages', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351107110, 'n', '2012', '10', '24', 0, 0, 20121024143150, 0, 0),
(7, 1, 16, 1, 0, NULL, '127.0.0.1', 'Pick a date & time', 'pick-a-date-time', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351107141, 'n', '2012', '10', '24', 0, 0, 20121024143221, 0, 0),
(8, 1, 16, 1, 0, NULL, '127.0.0.1', 'Your Cart', 'your-cart', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351107231, 'n', '2012', '10', '24', 0, 0, 20121024143351, 0, 0),
(9, 1, 16, 1, 0, NULL, '127.0.0.1', 'Confirmation', 'confirmation', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351107282, 'n', '2012', '10', '24', 0, 0, 20121024143442, 0, 0),
(10, 1, 7, 1, 0, NULL, '127.0.0.1', 'What is iFly?', 'what-is-ifly', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351109869, 'n', '2012', '10', '24', 0, 0, 20121024202050, 0, 0),
(11, 1, 7, 1, 0, NULL, '127.0.0.1', 'FAQs', 'faqs', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1351110205, 'n', '2012', '10', '24', 0, 0, 20121024152325, 0, 0),
(12, 1, 7, 1, 0, NULL, '127.0.0.1', 'FAQs', 'faqs1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351118416, 'n', '2012', '10', '24', 0, 0, 20121025171717, 0, 0),
(13, 1, 7, 1, 0, NULL, '127.0.0.1', 'The Sport of Flying', 'the-sport-of-flying', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351125038, 'n', '2012', '10', '24', 0, 0, 20121024193038, 0, 0),
(14, 1, 7, 1, 0, NULL, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351125370, 'n', '2012', '10', '24', 0, 0, 20121024193610, 0, 0),
(15, 1, 7, 1, 0, NULL, '127.0.0.1', 'The Flying Experience', 'the-flying-experience', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351126308, 'n', '2012', '10', '24', 0, 0, 20121024195148, 0, 0),
(16, 1, 7, 1, 0, NULL, '127.0.0.1', 'Page Title', 'page-title', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351199791, 'n', '2012', '10', '25', 0, 0, 20121025161631, 0, 0),
(17, 1, 3, 1, 0, NULL, '127.0.0.1', 'BOOK NOW AND GET MORE FLY TIME', 'book-now-and-get-more-fly-time', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351275818, 'n', '2012', '10', '26', 0, 0, 20121026132338, 0, 0),
(18, 1, 20, 1, 0, NULL, '127.0.0.1', 'GET AN iFLY GIFT CARD', 'gift-cards', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351292784, 'n', '2012', '10', '26', 0, 0, 20121026180624, 0, 0),
(19, 1, 19, 1, 0, NULL, '127.0.0.1', 'Flyer Guide', 'flyer-guide', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1351294379, 'n', '2012', '10', '26', 0, 0, 20121026183259, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_comments`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_comment_subscriptions`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_analytics`
--

CREATE TABLE IF NOT EXISTS `exp_cp_analytics` (
  `site_id` int(5) unsigned NOT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `profile` text,
  `settings` text,
  `hourly_cache` text,
  `daily_cache` text,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_cp_analytics`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=146 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'admin', '86.164.24.131', 1346761990, 'Logged in'),
(2, 1, 1, 'admin', '86.164.24.131', 1346762054, 'Logged out'),
(3, 1, 1, 'admin', '86.164.24.131', 1346762088, 'Logged in'),
(4, 1, 1, 'admin', '86.164.24.131', 1346762416, 'Channel Created:&nbsp;&nbsp;Tunnel'),
(5, 1, 1, 'admin', '86.164.24.131', 1346762440, 'Field Group Created:&nbsp;Tunnel Info'),
(6, 1, 1, 'admin', '86.164.24.131', 1346763201, 'Field Group Created:&nbsp;Hero'),
(7, 1, 1, 'admin', '86.164.24.131', 1346763479, 'Member Group Created:&nbsp;&nbsp;Global Manager'),
(8, 1, 1, 'admin', '86.164.24.131', 1346763525, 'Category Group Created:&nbsp;&nbsp;Hero Type'),
(9, 1, 1, 'admin', '86.164.24.131', 1346764383, 'Channel Created:&nbsp;&nbsp;Hero'),
(10, 1, 1, 'admin', '86.164.24.131', 1346764557, 'Member profile created:&nbsp;&nbsp;manager'),
(11, 1, 1, 'admin', '86.164.24.131', 1346764610, 'Logged out'),
(12, 1, 2, 'manager', '86.164.24.131', 1346764654, 'Logged in'),
(13, 1, 2, 'manager', '86.164.24.131', 1346764705, 'Logged out'),
(14, 1, 1, 'admin', '86.164.24.131', 1346764721, 'Logged in'),
(15, 1, 1, 'admin', '86.164.24.131', 1346764759, 'Member Group Updated:&nbsp;&nbsp;Global Manager'),
(16, 1, 1, 'admin', '86.164.24.131', 1346764768, 'Logged out'),
(17, 1, 1, 'admin', '86.164.24.131', 1346766803, 'Logged in'),
(18, 1, 1, 'admin', '86.164.24.131', 1346766830, 'Field Group Created:&nbsp;Promotional Tiles'),
(19, 1, 1, 'admin', '86.164.24.131', 1346767253, 'Logged in'),
(20, 1, 1, 'admin', '86.164.24.131', 1346767592, 'Channel Created:&nbsp;&nbsp;Promotional Tile'),
(21, 1, 1, 'admin', '86.164.24.131', 1346767746, 'Channel Created:&nbsp;&nbsp;Flier Type'),
(22, 1, 1, 'admin', '86.164.24.131', 1346767764, 'Field Group Created:&nbsp;Flier Type'),
(23, 1, 1, 'admin', '86.164.24.131', 1346768263, 'Channel Created:&nbsp;&nbsp;Gift Cards'),
(24, 1, 1, 'admin', '86.164.24.131', 1346768308, 'Field Group Created:&nbsp;Testimonials'),
(25, 1, 1, 'admin', '86.164.24.131', 1346768548, 'Channel Created:&nbsp;&nbsp;Testimonials'),
(26, 1, 1, 'admin', '86.164.24.131', 1346768657, 'Field Group Created:&nbsp;Information Page'),
(27, 1, 1, 'admin', '86.164.24.131', 1346769043, 'Channel Created:&nbsp;&nbsp;Information page'),
(28, 1, 2, 'manager', '97.79.130.126', 1346786891, 'Logged in'),
(29, 1, 1, 'admin', '97.79.130.126', 1346789502, 'Logged in'),
(30, 1, 1, 'admin', '81.151.194.18', 1346790082, 'Logged in'),
(31, 1, 1, 'admin', '97.79.130.126', 1346799889, 'Logged in'),
(32, 1, 1, 'admin', '97.79.130.126', 1346799962, 'Logged out'),
(33, 1, 1, 'admin', '97.79.130.126', 1346948766, 'Logged in'),
(34, 1, 2, 'manager', '97.79.130.126', 1346948799, 'Logged in'),
(35, 1, 2, 'manager', '97.79.130.126', 1346953462, 'Logged in'),
(36, 1, 2, 'manager', '24.153.178.154', 1346964328, 'Logged in'),
(37, 1, 2, 'manager', '97.79.130.126', 1347046865, 'Logged in'),
(38, 1, 1, 'admin', '97.79.130.126', 1347982573, 'Logged in'),
(39, 1, 1, 'admin', '97.79.130.126', 1347993819, 'Logged in'),
(40, 1, 1, 'admin', '97.79.130.126', 1347994592, 'Logged in'),
(41, 1, 1, 'admin', '97.79.130.126', 1348006417, 'Logged in'),
(42, 1, 1, 'admin', '108.13.107.180', 1348008095, 'Logged in'),
(43, 1, 1, 'admin', '108.13.107.180', 1348017639, 'Logged in'),
(44, 1, 1, 'admin', '97.79.130.126', 1348068386, 'Logged in'),
(45, 1, 1, 'admin', '97.79.130.126', 1348068960, 'Member profile created:&nbsp;&nbsp;stuartw'),
(46, 1, 1, 'admin', '97.79.130.126', 1348069070, 'Member profile created:&nbsp;&nbsp;nayladp'),
(47, 1, 1, 'admin', '97.79.130.126', 1348069123, 'Member profile created:&nbsp;&nbsp;axelz'),
(48, 1, 1, 'admin', '97.79.130.126', 1348070946, 'Member profile created:&nbsp;&nbsp;chew'),
(49, 1, 1, 'admin', '97.79.130.126', 1348070997, 'Member profile created:&nbsp;&nbsp;craigb'),
(50, 1, 1, 'admin', '97.79.130.126', 1348077384, 'Logged in'),
(51, 1, 1, 'admin', '24.153.178.154', 1348084032, 'Logged in'),
(52, 1, 1, 'admin', '24.153.178.154', 1348085079, 'Logged out'),
(53, 1, 3, 'stuartw', '24.153.178.154', 1348085114, 'Logged in'),
(54, 1, 1, 'admin', '98.149.249.30', 1348268234, 'Logged in'),
(55, 1, 1, 'admin', '98.149.249.30', 1348268316, 'Channel Created:&nbsp;&nbsp;Events'),
(56, 1, 1, 'admin', '98.149.249.30', 1348268337, 'Field Group Created:&nbsp;Events'),
(57, 1, 1, 'admin', '108.13.107.180', 1348602988, 'Logged in'),
(58, 1, 1, 'admin', '108.13.107.180', 1348608590, 'Logged in'),
(59, 1, 1, 'admin', '108.13.107.180', 1348768409, 'Logged in'),
(60, 1, 1, 'admin', '108.13.107.180', 1348856240, 'Logged in'),
(61, 1, 1, 'admin', '97.79.130.126', 1348864730, 'Logged in'),
(62, 1, 1, 'admin', '97.79.130.126', 1348864980, 'Member profile created:&nbsp;&nbsp;royh'),
(63, 1, 1, 'admin', '97.79.130.126', 1348865151, 'Logged out'),
(64, 1, 3, 'stuartw', '97.79.130.126', 1349112303, 'Logged in'),
(65, 1, 5, 'axelz', '24.153.178.154', 1349123350, 'Logged in'),
(66, 1, 1, 'admin', '108.13.107.180', 1349123591, 'Logged in'),
(67, 1, 3, 'stuartw', '70.124.65.165', 1349150896, 'Logged in'),
(68, 1, 4, 'nayladp', '74.66.251.75', 1349189165, 'Logged in'),
(69, 1, 7, 'craigb', '97.79.130.126', 1349196638, 'Logged in'),
(70, 1, 1, 'admin', '108.13.107.180', 1349197344, 'Logged in'),
(71, 1, 1, 'admin', '108.13.107.180', 1349197390, 'Logged out'),
(72, 1, 7, 'craigb', '108.13.107.180', 1349197423, 'Logged in'),
(73, 1, 7, 'craigb', '108.13.107.180', 1349198883, 'Logged out'),
(74, 1, 1, 'admin', '108.13.107.180', 1349198900, 'Logged in'),
(75, 1, 1, 'admin', '108.13.107.180', 1349198989, 'Channel Created:&nbsp;&nbsp;Call to Action Copy'),
(76, 1, 1, 'admin', '108.13.107.180', 1349199017, 'Field Group Created:&nbsp;Call to Action Copy'),
(77, 1, 1, 'admin', '108.13.107.180', 1349199400, 'Member Group Updated:&nbsp;&nbsp;Global Manager'),
(78, 1, 7, 'craigb', '97.79.130.126', 1349201034, 'Logged out'),
(79, 1, 7, 'craigb', '97.79.130.126', 1349281049, 'Logged in'),
(80, 1, 7, 'craigb', '24.153.178.154', 1349293531, 'Logged in'),
(81, 1, 7, 'craigb', '24.153.178.154', 1349296166, 'Logged out'),
(82, 1, 7, 'craigb', '97.79.130.126', 1349457025, 'Logged in'),
(83, 1, 7, 'craigb', '97.79.130.126', 1349461004, 'Logged out'),
(84, 1, 7, 'craigb', '97.79.130.126', 1349461180, 'Logged in'),
(85, 1, 7, 'craigb', '97.79.130.126', 1349717081, 'Logged in'),
(86, 1, 1, 'admin', '98.149.249.30', 1349724189, 'Logged in'),
(87, 1, 7, 'craigb', '97.79.130.126', 1349799433, 'Logged in'),
(88, 1, 7, 'craigb', '97.79.130.126', 1349894200, 'Logged in'),
(89, 1, 7, 'craigb', '97.79.130.126', 1349907507, 'Logged in'),
(90, 1, 1, 'admin', '98.149.249.30', 1349907513, 'Logged in'),
(91, 1, 7, 'craigb', '97.79.130.126', 1349911931, 'Logged out'),
(92, 1, 1, 'admin', '98.149.249.30', 1349912163, 'Logged in'),
(93, 1, 1, 'admin', '98.149.249.30', 1349913768, 'Custom Field Deleted:&nbsp;iFrame'),
(94, 1, 1, 'admin', '98.149.249.30', 1349914143, 'Custom Field Deleted:&nbsp;iFrame'),
(95, 1, 1, 'admin', '98.149.249.30', 1349916650, 'Channel Created:&nbsp;&nbsp;Template - 2 Column'),
(96, 1, 8, 'royh', '24.153.178.154', 1349933752, 'Logged in'),
(97, 1, 8, 'royh', '24.153.178.154', 1349933868, 'Logged out'),
(98, 1, 8, 'royh', '24.153.178.154', 1349933869, 'Logged out'),
(99, 1, 1, 'admin', '173.198.122.173', 1349979906, 'Logged in'),
(100, 1, 1, 'admin', '173.198.122.173', 1349980057, 'Logged in'),
(101, 1, 7, 'craigb', '97.79.130.126', 1349985299, 'Logged in'),
(102, 1, 1, 'admin', '98.149.249.30', 1349995083, 'Logged out'),
(103, 1, 1, 'admin', '98.149.249.30', 1349998940, 'Logged in'),
(104, 1, 1, 'admin', '108.13.107.180', 1350598790, 'Logged in'),
(105, 1, 1, 'admin', '127.0.0.1', 1350937053, 'Logged in'),
(106, 1, 1, 'admin', '127.0.0.1', 1350941824, 'Logged in'),
(107, 1, 1, 'admin', '127.0.0.1', 1350946906, 'Site Created&nbsp;&nbsp;Seattle'),
(108, 1, 1, 'admin', '127.0.0.1', 1350946968, 'Site Created&nbsp;&nbsp;Venice Beach'),
(109, 3, 1, 'admin', '127.0.0.1', 1350947026, 'Site Deleted:&nbsp;&nbsp;Seattle'),
(110, 1, 1, 'admin', '127.0.0.1', 1351019588, 'Logged in'),
(111, 3, 1, 'admin', '127.0.0.1', 1351022669, 'Site Created&nbsp;&nbsp;Orlando'),
(112, 1, 1, 'admin', '127.0.0.1', 1351040483, 'Logged in'),
(113, 1, 1, 'admin', '127.0.0.1', 1351040626, 'Field Group Created:&nbsp;One Column Page'),
(114, 1, 1, 'admin', '127.0.0.1', 1351042888, 'Channel Created:&nbsp;&nbsp;Booking Pages'),
(115, 1, 1, 'admin', '127.0.0.1', 1351098140, 'Logged in'),
(116, 1, 1, 'admin', '127.0.0.1', 1351114382, 'Logged in'),
(117, 1, 1, 'admin', '127.0.0.1', 1351114719, 'Custom Field Deleted:&nbsp;Hero'),
(118, 1, 1, 'admin', '127.0.0.1', 1351114734, 'Custom Field Deleted:&nbsp;iFrame'),
(119, 1, 1, 'admin', '127.0.0.1', 1351125000, 'Logged in'),
(120, 1, 1, 'admin', '127.0.0.1', 1351184356, 'Logged in'),
(121, 1, 1, 'admin', '127.0.0.1', 1351193564, 'Logged in'),
(122, 1, 1, 'admin', '127.0.0.1', 1351197919, 'Logged in'),
(123, 1, 1, 'admin', '127.0.0.1', 1351273052, 'Logged in'),
(124, 1, 1, 'admin', '127.0.0.1', 1351273659, 'Member Group Created:&nbsp;&nbsp;Local Manager'),
(125, 1, 1, 'admin', '127.0.0.1', 1351274680, 'Field Group Created:&nbsp;Home Page'),
(126, 1, 1, 'admin', '127.0.0.1', 1351275142, 'Channel Created:&nbsp;&nbsp;Home Page'),
(127, 1, 1, 'admin', '127.0.0.1', 1351276467, 'Field Group Created:&nbsp;Curated Media'),
(128, 1, 1, 'admin', '127.0.0.1', 1351277350, 'Channel Created:&nbsp;&nbsp;Curated Media'),
(129, 1, 1, 'admin', '127.0.0.1', 1351281519, 'Custom Field Deleted:&nbsp;Booking Flow : Pop-up Copy'),
(130, 1, 1, 'admin', '127.0.0.1', 1351281525, 'Custom Field Deleted:&nbsp;Booking Flow : Pop-up Image'),
(131, 1, 1, 'admin', '127.0.0.1', 1351281669, 'Custom Field Deleted:&nbsp;Overview Page : Image'),
(132, 1, 1, 'admin', '127.0.0.1', 1351281675, 'Custom Field Deleted:&nbsp;Overview Page : Copy'),
(133, 1, 1, 'admin', '127.0.0.1', 1351281746, 'Channel Created:&nbsp;&nbsp;Flyer Type Overview Page'),
(134, 1, 1, 'admin', '127.0.0.1', 1351281790, 'Field Group Created:&nbsp;Flyer Overview Page'),
(135, 1, 1, 'admin', '127.0.0.1', 1351283064, 'Field Group Created:&nbsp;Flight Info - Gift Card'),
(136, 1, 1, 'admin', '127.0.0.1', 1351283917, 'Channel Created:&nbsp;&nbsp;Flight Info - Gift Cards'),
(137, 1, 1, 'admin', '127.0.0.1', 1351290812, 'Logged in'),
(138, 1, 1, 'admin', '127.0.0.1', 1351290948, 'Logged out'),
(139, 1, 1, 'admin', '127.0.0.1', 1351291568, 'Logged in'),
(140, 1, 1, 'admin', '127.0.0.1', 1351291623, 'Logged in'),
(141, 1, 1, 'admin', '127.0.0.1', 1351291639, 'Channel Deleted:&nbsp;&nbsp;Call to Action'),
(142, 1, 1, 'admin', '127.0.0.1', 1351292342, 'Channel Deleted:&nbsp;&nbsp;What is iFly - Gift Cards'),
(143, 1, 1, 'admin', '127.0.0.1', 1351297536, 'Field Group Created:&nbsp;Tunnels - non participating'),
(144, 1, 1, 'admin', '127.0.0.1', 1351297775, 'Channel Created:&nbsp;&nbsp;Tunnels - non particpating'),
(145, 1, 1, 'admin', '127.0.0.1', 1351297927, 'Field Group Created:&nbsp;Find Location');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_cp_search_index`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_developer_log`
--

INSERT INTO `exp_developer_log` (`log_id`, `timestamp`, `viewed`, `description`, `function`, `line`, `file`, `deprecated_since`, `use_instead`) VALUES
(1, 1349980638, 'y', 'videoplayer/lirabries/services/youtube.php "ping" method returned this error : Expected response code 200, got 403\n\n\nInvalid developer key\n\n\nInvalid developer key\nError 403\n\n\n', NULL, NULL, NULL, NULL, NULL),
(2, 1351114635, 'y', NULL, 'get_upload_preferences()', 336, '/Applications/XAMPP/xamppfiles/htdocs/coloringbook/ifly/dev/system/expressionengine/third_party/better_pages/ft.better_pages.php', '2.2', 'File_upload_preferences_model::get_file_upload_preferences()'),
(3, 1351114635, 'y', NULL, 'get_upload_preferences()', 522, '/Applications/XAMPP/xamppfiles/htdocs/coloringbook/ifly/dev/system/expressionengine/models/tools_model.php', '2.4', 'File_upload_preferences_model::get_file_upload_preferences() to support config variable overrides'),
(4, 1351116185, 'y', 'videoplayer/lirabries/services/vimeo.php "get_profile" method returned this error : Invalid consumer key', NULL, NULL, NULL, NULL, NULL),
(5, 1351116412, 'y', 'videoplayer/lirabries/services/vimeo.php "get_profile" method returned this error : User not found', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_email_cache`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_email_cache_mg`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_email_cache_ml`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_email_console_cache`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_email_tracker`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_entry_ping_status`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_entry_versioning`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(5, 'Navee_ext', 'entry_submission_end', 'entry_submission_end', 's:0:"";', 10, '2.2.5', 'y'),
(6, 'Navee_ext', 'delete_entries_loop', 'delete_entries_loop', 's:0:"";', 10, '2.2.5', 'y'),
(7, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y'),
(8, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.4.3', 'y'),
(9, 'Mx_title_control_ext', 'cp_js_end', 'cp_js_end', 'a:38:{s:15:"title_default_9";s:8:"Headline";s:19:"url_title_default_9";s:0:"";s:15:"title_default_8";s:0:"";s:19:"url_title_default_8";s:0:"";s:15:"title_default_4";s:0:"";s:19:"url_title_default_4";s:0:"";s:15:"title_default_5";s:0:"";s:19:"url_title_default_5";s:0:"";s:15:"title_default_2";s:8:"Headline";s:19:"url_title_default_2";s:0:"";s:15:"title_default_7";s:0:"";s:19:"url_title_default_7";s:0:"";s:15:"title_default_3";s:8:"Headline";s:19:"url_title_default_3";s:0:"";s:15:"title_default_6";s:0:"";s:19:"url_title_default_6";s:0:"";s:15:"title_default_1";s:11:"Tunnel Name";s:19:"url_title_default_1";s:0:"";s:13:"multilanguage";s:1:"n";s:9:"max_title";s:3:"100";s:15:"title_pattern_9";s:0:"";s:19:"url_title_pattern_9";s:0:"";s:15:"title_pattern_8";s:0:"";s:19:"url_title_pattern_8";s:0:"";s:15:"title_pattern_4";s:0:"";s:19:"url_title_pattern_4";s:0:"";s:15:"title_pattern_5";s:0:"";s:19:"url_title_pattern_5";s:0:"";s:15:"title_pattern_2";s:0:"";s:19:"url_title_pattern_2";s:0:"";s:15:"title_pattern_7";s:0:"";s:19:"url_title_pattern_7";s:0:"";s:15:"title_pattern_3";s:0:"";s:19:"url_title_pattern_3";s:0:"";s:15:"title_pattern_6";s:0:"";s:19:"url_title_pattern_6";s:0:"";s:15:"title_pattern_1";s:0:"";s:19:"url_title_pattern_1";s:0:"";}', 1, '2.8.0', 'y'),
(10, 'Mx_title_control_ext', 'entry_submission_end', 'entry_submission_end', 'a:38:{s:15:"title_default_9";s:8:"Headline";s:19:"url_title_default_9";s:0:"";s:15:"title_default_8";s:0:"";s:19:"url_title_default_8";s:0:"";s:15:"title_default_4";s:0:"";s:19:"url_title_default_4";s:0:"";s:15:"title_default_5";s:0:"";s:19:"url_title_default_5";s:0:"";s:15:"title_default_2";s:8:"Headline";s:19:"url_title_default_2";s:0:"";s:15:"title_default_7";s:0:"";s:19:"url_title_default_7";s:0:"";s:15:"title_default_3";s:8:"Headline";s:19:"url_title_default_3";s:0:"";s:15:"title_default_6";s:0:"";s:19:"url_title_default_6";s:0:"";s:15:"title_default_1";s:11:"Tunnel Name";s:19:"url_title_default_1";s:0:"";s:13:"multilanguage";s:1:"n";s:9:"max_title";s:3:"100";s:15:"title_pattern_9";s:0:"";s:19:"url_title_pattern_9";s:0:"";s:15:"title_pattern_8";s:0:"";s:19:"url_title_pattern_8";s:0:"";s:15:"title_pattern_4";s:0:"";s:19:"url_title_pattern_4";s:0:"";s:15:"title_pattern_5";s:0:"";s:19:"url_title_pattern_5";s:0:"";s:15:"title_pattern_2";s:0:"";s:19:"url_title_pattern_2";s:0:"";s:15:"title_pattern_7";s:0:"";s:19:"url_title_pattern_7";s:0:"";s:15:"title_pattern_3";s:0:"";s:19:"url_title_pattern_3";s:0:"";s:15:"title_pattern_6";s:0:"";s:19:"url_title_pattern_6";s:0:"";s:15:"title_pattern_1";s:0:"";s:19:"url_title_pattern_1";s:0:"";}', 1, '2.8.0', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(11, 'mx_google_map', '1.4', 'YTo5OntzOjg6ImxhdGl0dWRlIjtzOjE3OiI0NC4wNjE5MzI5Nzg2NTM0OCI7czo5OiJsb25naXR1ZGUiO3M6MTk6Ii0xMjEuMjc1ODQ0NTczOTc0NjEiO3M6NDoiem9vbSI7czoyOiIxMyI7czoxMDoibWF4X3BvaW50cyI7czoxOiIzIjtzOjQ6Imljb24iO3M6MDoiIjtzOjk6InNsaWRlX2JhciI7czoxOiJ5IjtzOjE4OiJwYXRoX21hcmtlcnNfaWNvbnMiO3M6MTA0OiIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL2NvbG9yaW5nYm9vay9pZmx5L2Rldi90aGVtZXMvdGhpcmRfcGFydHkvbXhfZ29vZ2xlX21hcC9tYXBzLWljb25zLyI7czoxNzoidXJsX21hcmtlcnNfaWNvbnMiO3M6Njk6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS90aGVtZXMvdGhpcmRfcGFydHkvbXhfZ29vZ2xlX21hcC9tYXBzLWljb25zLyI7czo2OiJzdWJtaXQiO3M6NjoiU3VibWl0Ijt9', 'y'),
(12, 'navee', '2.2.5', 'YTowOnt9', 'n'),
(13, 'playa', '4.3.3', 'YTowOnt9', 'y'),
(15, 'matrix', '2.4.3', 'YTowOnt9', 'y'),
(16, 'low_events', '1.0.4', 'YTowOnt9', 'n'),
(17, 'better_pages', '1.1.5', 'YTozOntzOjE2OiJ1cGxvYWRfZGlyZWN0b3J5IjtzOjE6IjEiO3M6MTk6ImZpZWxkX2hpZGluZ19tZXRob2QiO3M6NjoicmVtb3ZlIjtzOjY6InN1Ym1pdCI7czo2OiJTdWJtaXQiO30=', 'y'),
(18, 'freeform', '4.0.7', 'YTowOnt9', 'n'),
(19, 'videoplayer', '3.1.1', 'YToxOntzOjk6InZpZGVvX3VybCI7czowOiIiO30=', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=283 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(10, 4, 'none'),
(11, 4, 'br'),
(12, 4, 'xhtml'),
(13, 5, 'none'),
(14, 5, 'br'),
(15, 5, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(28, 10, 'none'),
(29, 10, 'br'),
(30, 10, 'xhtml'),
(31, 11, 'none'),
(32, 11, 'br'),
(33, 11, 'xhtml'),
(37, 13, 'none'),
(38, 13, 'br'),
(39, 13, 'xhtml'),
(40, 14, 'none'),
(41, 14, 'br'),
(42, 14, 'xhtml'),
(43, 15, 'none'),
(44, 15, 'br'),
(45, 15, 'xhtml'),
(49, 17, 'none'),
(50, 17, 'br'),
(51, 17, 'xhtml'),
(58, 20, 'none'),
(59, 20, 'br'),
(60, 20, 'xhtml'),
(61, 21, 'none'),
(62, 21, 'br'),
(63, 21, 'xhtml'),
(70, 24, 'none'),
(71, 24, 'br'),
(72, 24, 'xhtml'),
(73, 25, 'none'),
(74, 25, 'br'),
(75, 25, 'xhtml'),
(85, 29, 'none'),
(86, 29, 'br'),
(87, 29, 'xhtml'),
(88, 30, 'none'),
(89, 30, 'br'),
(90, 30, 'xhtml'),
(97, 33, 'none'),
(98, 33, 'br'),
(99, 33, 'xhtml'),
(100, 34, 'none'),
(101, 34, 'br'),
(102, 34, 'xhtml'),
(103, 35, 'none'),
(104, 35, 'br'),
(105, 35, 'xhtml'),
(109, 37, 'none'),
(110, 37, 'br'),
(111, 37, 'xhtml'),
(112, 38, 'none'),
(113, 38, 'br'),
(114, 38, 'xhtml'),
(115, 39, 'none'),
(116, 39, 'br'),
(117, 39, 'xhtml'),
(118, 40, 'none'),
(119, 40, 'br'),
(120, 40, 'xhtml'),
(121, 41, 'none'),
(122, 41, 'br'),
(123, 41, 'xhtml'),
(124, 42, 'none'),
(125, 42, 'br'),
(126, 42, 'xhtml'),
(127, 43, 'none'),
(128, 43, 'br'),
(129, 43, 'xhtml'),
(130, 44, 'none'),
(131, 44, 'br'),
(132, 44, 'xhtml'),
(133, 45, 'none'),
(134, 45, 'br'),
(135, 45, 'xhtml'),
(136, 46, 'none'),
(137, 46, 'br'),
(138, 46, 'xhtml'),
(139, 47, 'none'),
(140, 47, 'br'),
(141, 47, 'xhtml'),
(142, 48, 'none'),
(143, 48, 'br'),
(144, 48, 'xhtml'),
(145, 49, 'none'),
(146, 49, 'br'),
(147, 49, 'xhtml'),
(151, 51, 'none'),
(152, 51, 'br'),
(153, 51, 'xhtml'),
(154, 52, 'none'),
(155, 52, 'br'),
(156, 52, 'xhtml'),
(157, 53, 'none'),
(158, 53, 'br'),
(159, 53, 'xhtml'),
(160, 54, 'none'),
(161, 54, 'br'),
(162, 54, 'xhtml'),
(163, 55, 'none'),
(164, 55, 'br'),
(165, 55, 'xhtml'),
(166, 56, 'none'),
(167, 56, 'br'),
(168, 56, 'xhtml'),
(169, 57, 'none'),
(170, 57, 'br'),
(171, 57, 'xhtml'),
(172, 58, 'none'),
(173, 58, 'br'),
(174, 58, 'xhtml'),
(175, 59, 'none'),
(176, 59, 'br'),
(177, 59, 'xhtml'),
(178, 60, 'none'),
(179, 60, 'br'),
(180, 60, 'xhtml'),
(181, 61, 'none'),
(182, 61, 'br'),
(183, 61, 'xhtml'),
(184, 62, 'none'),
(185, 62, 'br'),
(186, 62, 'xhtml'),
(187, 63, 'none'),
(188, 63, 'br'),
(189, 63, 'xhtml'),
(190, 64, 'none'),
(191, 64, 'br'),
(192, 64, 'xhtml'),
(193, 65, 'none'),
(194, 65, 'br'),
(195, 65, 'xhtml'),
(196, 66, 'none'),
(197, 66, 'br'),
(198, 66, 'xhtml'),
(199, 67, 'none'),
(200, 67, 'br'),
(201, 67, 'xhtml'),
(202, 68, 'none'),
(203, 68, 'br'),
(204, 68, 'xhtml'),
(205, 69, 'none'),
(206, 69, 'br'),
(207, 69, 'xhtml'),
(208, 70, 'none'),
(209, 70, 'br'),
(210, 70, 'xhtml'),
(211, 71, 'none'),
(212, 71, 'br'),
(213, 71, 'xhtml'),
(214, 72, 'none'),
(215, 72, 'br'),
(216, 72, 'xhtml'),
(217, 73, 'none'),
(218, 73, 'br'),
(219, 73, 'xhtml'),
(220, 74, 'none'),
(221, 74, 'br'),
(222, 74, 'xhtml'),
(223, 75, 'none'),
(224, 75, 'br'),
(225, 75, 'xhtml'),
(226, 76, 'none'),
(227, 76, 'br'),
(228, 76, 'xhtml'),
(229, 77, 'none'),
(230, 77, 'br'),
(231, 77, 'xhtml'),
(232, 78, 'none'),
(233, 78, 'br'),
(234, 78, 'xhtml'),
(235, 79, 'none'),
(236, 79, 'br'),
(237, 79, 'xhtml'),
(238, 80, 'none'),
(239, 80, 'br'),
(240, 80, 'xhtml'),
(241, 81, 'none'),
(242, 81, 'br'),
(243, 81, 'xhtml'),
(244, 82, 'none'),
(245, 82, 'br'),
(246, 82, 'xhtml'),
(247, 83, 'none'),
(248, 83, 'br'),
(249, 83, 'xhtml'),
(250, 84, 'none'),
(251, 84, 'br'),
(252, 84, 'xhtml'),
(253, 85, 'none'),
(254, 85, 'br'),
(255, 85, 'xhtml'),
(256, 86, 'none'),
(257, 86, 'br'),
(258, 86, 'xhtml'),
(259, 87, 'none'),
(260, 87, 'br'),
(261, 87, 'xhtml'),
(262, 88, 'none'),
(263, 88, 'br'),
(264, 88, 'xhtml'),
(265, 89, 'none'),
(266, 89, 'br'),
(267, 89, 'xhtml'),
(268, 90, 'none'),
(269, 90, 'br'),
(270, 90, 'xhtml'),
(271, 91, 'none'),
(272, 91, 'br'),
(273, 91, 'xhtml'),
(274, 92, 'none'),
(275, 92, 'br'),
(276, 92, 'xhtml'),
(277, 93, 'none'),
(278, 93, 'br'),
(279, 93, 'xhtml'),
(280, 94, 'none'),
(281, 94, 'br'),
(282, 94, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Tunnel Info'),
(2, 1, 'Hero'),
(3, 1, 'Tiles'),
(4, 1, 'Flier Type'),
(5, 1, 'Flyer Feedback'),
(6, 1, 'One Column Pages'),
(7, 1, 'Events'),
(8, 1, 'Call to Action Copy'),
(9, 3, 'Call to Action Copy'),
(10, 3, 'Events'),
(11, 3, 'Tiles'),
(12, 3, 'Tunnel Info'),
(13, 1, 'Booking Pages'),
(14, 1, 'Home Page'),
(15, 1, 'Curated Media'),
(16, 1, 'Flyer Overview Page'),
(17, 1, 'Flight Info - Gift Card'),
(18, 1, 'Tunnels - non participating'),
(19, 1, 'Find Location');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, '1-col.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/1-col.jpg', 'image/jpeg', '1-col.jpg', 623790, NULL, NULL, NULL, 1, 1348858260, 1, 1348858260, '2368 1369'),
(2, 1, 'img01.png', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img01.png', 'image/png', 'img01.png', 191024, NULL, NULL, NULL, 1, 1349724484, 1, 1349987779, '368 764'),
(3, 1, 'calendar_sprite.png', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/calendar_sprite.png', 'image/png', 'calendar_sprite.png', 3543, '', '', '', 1, 1349987904, 1, 1349987968, '19 57'),
(4, 1, 'img07-right.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img07.jpg', 'image/jpeg', 'img07.jpg', 21921, '', '', '', 1, 1349998974, 1, 1349999032, '313 186'),
(5, 1, 'right.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img05.jpg', 'image/jpeg', 'img05.jpg', 17792, '', '', '', 1, 1349999077, 1, 1349999182, '313 186'),
(6, 1, 'temp-fliers.jpg', 1, '/Applications/XAMPP/xamppfiles/htdocs/coloringbook/ifly/dev/uploads/temp-fliers.jpg', 'image/jpeg', 'temp-fliers.jpg', 52367, NULL, NULL, NULL, 1, 1351294547, 1, 1351294547, '335 310');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_file_categories`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_file_dimensions`
--

INSERT INTO `exp_file_dimensions` (`id`, `site_id`, `upload_location_id`, `title`, `short_name`, `resize_type`, `width`, `height`, `watermark_id`) VALUES
(1, 1, 3, 'Thumbnail', 'Thumbnail', 'constrain', 175, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_file_watermarks`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_layouts`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_freeform_composer_layouts`
--

INSERT INTO `exp_freeform_composer_layouts` (`composer_id`, `composer_data`, `site_id`, `preview`, `entry_date`, `edit_date`) VALUES
(1, '{"rows":[[[{"type":"field","fieldId":"1","required":"no"},{"type":"nonfield_paragraph","html":"This is a sample paragraph to show you how easy it is to build a form with Freeform Pro."},{"type":"field","fieldId":"2","required":"no"},{"type":"field","fieldId":"3","required":"no"},{"type":"field","fieldId":"4","required":"no"},{"type":"nonfield_submit","html":"Submit"}]]],"fields":["1","2","3","4"]}', 1, 'n', 1351118415, 0),
(2, '{"rows":[[[{"type":"nonfield_title"},{"type":"nonfield_paragraph","html":"This is some new copy."},{"type":"field","fieldId":"1","required":"no"},{"type":"field","fieldId":"2","required":"no"},{"type":"field","fieldId":"3","required":"no"},{"type":"field","fieldId":"4","required":"no"},{"type":"nonfield_submit","html":"Submit"}]]],"fields":["1","2","3","4"]}', 1, 'n', 1351125388, 1351200742);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_freeform_composer_templates`
--

INSERT INTO `exp_freeform_composer_templates` (`template_id`, `site_id`, `template_name`, `template_label`, `template_description`, `enable_template`, `template_data`, `param_data`) VALUES
(1, 1, 'what_is_ifly', 'What is iFly', '', 'y', '<style type="text/css">\n	.ff_composer * {\n		-webkit-box-sizing	: border-box;\n		-moz-box-sizing		: border-box;\n		box-sizing			: border-box;\n	}\n	.ff_composer .line,\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{overflow:hidden;}\n	.ff_composer .unit{float:left; padding:10px;}\n	.ff_composer .unit_right{float:right;}\n	.ff_composer .size1of1{float:none;}\n	.ff_composer .size1of2{width:50%;}\n	.ff_composer .size1of3{width:33.33333%;}\n	.ff_composer .size2of3{width:66.66666%;}\n	.ff_composer .size1of4{width:25%;}\n	.ff_composer .size3of4{width:75%;}\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{float:none;width:auto;}\n	.ff_composer p,\n	.ff_composer h1,\n	.ff_composer h2,\n	.ff_composer h3,\n	.ff_composer h4,\n	.ff_composer h5,\n	.ff_composer h6 {margin-top: 0;}\n	.ff_composer .required_item {margin-left: 4px; color:red;}\n	.ff_composer textarea,\n	.ff_composer input[type="text"],\n	.ff_composer input[type="email"],\n	.ff_composer input[type="url"],\n	.ff_composer input[type="number"],\n	.ff_composer input[type="password"],\n	.ff_composer input[type="search"] {width: 75%;}\n	.ff_composer ul.dynamic_recipients {list-style: none; padding:0;}\n	.ff_composer .field_label {font-weight: bold;}\n</style>\n<div class="ff_composer">\n{composer:page}\n	{composer:rows}\n			<div class="line">\n		{composer:columns}\n				<div class="unit size1of{composer:column_total}">\n				{if composer:field_total == 0}\n					 \n				{/if}\n			{composer:fields}\n				{if composer:field_label}\n					{if composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n						<p>\n							<strong>{composer:field_label}</strong>\n						</p>\n						{/if}\n					{if:else}\n						<label class="field_label" {if composer:field_name != ''''}for="freeform_{composer:field_name}"{/if}>\n							{composer:field_label}{if composer:field_required}<span class="required_item">*</span>{/if}\n						</label>\n					{/if}\n				{/if}\n				{if composer:field_output}\n					{if composer:field_type == ''nonfield_title''}\n						<h2>{composer:field_output}</h2>\n					{if:elseif composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n								{freeform:captcha}<br />\n								<input type="text" name="captcha" value=""\n									   size="20"   maxlength="20" style="width:140px;" />\n						{/if}\n					{if:else}\n						<p>{composer:field_output}</p>\n					{/if}\n				{/if}\n			{/composer:fields}\n				</div>\n		{/composer:columns}\n			</div>\n	{/composer:rows}\n{/composer:page}\n</div>\n', '[]'),
(2, 1, 'send_us_a_note', 'Send us a note', '', 'y', '<style type="text/css">\n	.ff_composer * {\n		-webkit-box-sizing	: border-box;\n		-moz-box-sizing		: border-box;\n		box-sizing			: border-box;\n	}\n	.ff_composer .line,\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{overflow:hidden;}\n	.ff_composer .unit{float:left; padding:10px;}\n	.ff_composer .unit_right{float:right;}\n	.ff_composer .size1of1{float:none;}\n	.ff_composer .size1of2{width:50%;}\n	.ff_composer .size1of3{width:33.33333%;}\n	.ff_composer .size2of3{width:66.66666%;}\n	.ff_composer .size1of4{width:25%;}\n	.ff_composer .size3of4{width:75%;}\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{float:none;width:auto;}\n	.ff_composer p,\n	.ff_composer h1,\n	.ff_composer h2,\n	.ff_composer h3,\n	.ff_composer h4,\n	.ff_composer h5,\n	.ff_composer h6 {margin-top: 0;}\n	.ff_composer .required_item {margin-left: 4px; color:red;}\n	.ff_composer textarea,\n	.ff_composer input[type="text"],\n	.ff_composer input[type="email"],\n	.ff_composer input[type="url"],\n	.ff_composer input[type="number"],\n	.ff_composer input[type="password"],\n	.ff_composer input[type="search"] {width: 75%;}\n	.ff_composer ul.dynamic_recipients {list-style: none; padding:0;}\n	.ff_composer .field_label {font-weight: bold;}\n</style>\n<div class="ff_composer">\n{composer:page}\n	{composer:rows}\n			<div class="line">\n		{composer:columns}\n				<div class="unit size1of{composer:column_total}">\n				{if composer:field_total == 0}\n					 \n				{/if}\n			{composer:fields}\n				{if composer:field_label}\n					{if composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n						<p>\n							<strong>{composer:field_label}</strong>\n						</p>\n						{/if}\n					{if:else}\n						<label class="field_label" {if composer:field_name != ''''}for="freeform_{composer:field_name}"{/if}>\n							{composer:field_label}{if composer:field_required}<span class="required_item">*</span>{/if}\n						</label>\n					{/if}\n				{/if}\n				{if composer:field_output}\n					{if composer:field_type == ''nonfield_title''}\n						<h2>{composer:field_output}</h2>\n					{if:elseif composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n								{freeform:captcha}<br />\n								<input type="text" name="captcha" value=""\n									   size="20"   maxlength="20" style="width:140px;" />\n						{/if}\n					{if:else}\n						<p>{composer:field_output}</p>\n					{/if}\n				{/if}\n			{/composer:fields}\n				</div>\n		{/composer:columns}\n			</div>\n	{/composer:rows}\n{/composer:page}\n</div>\n', '[]'),
(3, 1, 'email_capture', 'Email Capture', 'Description', 'y', '<style type="text/css">\n	.ff_composer * {\n		-webkit-box-sizing	: border-box;\n		-moz-box-sizing		: border-box;\n		box-sizing			: border-box;\n	}\n	.ff_composer .line,\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{overflow:hidden;}\n	.ff_composer .unit{float:left; padding:10px;}\n	.ff_composer .unit_right{float:right;}\n	.ff_composer .size1of1{float:none;}\n	.ff_composer .size1of2{width:50%;}\n	.ff_composer .size1of3{width:33.33333%;}\n	.ff_composer .size2of3{width:66.66666%;}\n	.ff_composer .size1of4{width:25%;}\n	.ff_composer .size3of4{width:75%;}\n	.ff_composer .line:last-child,\n	.ff_composer .last_unit{float:none;width:auto;}\n	.ff_composer p,\n	.ff_composer h1,\n	.ff_composer h2,\n	.ff_composer h3,\n	.ff_composer h4,\n	.ff_composer h5,\n	.ff_composer h6 {margin-top: 0;}\n	.ff_composer .required_item {margin-left: 4px; color:red;}\n	.ff_composer textarea,\n	.ff_composer input[type="text"],\n	.ff_composer input[type="email"],\n	.ff_composer input[type="url"],\n	.ff_composer input[type="number"],\n	.ff_composer input[type="password"],\n	.ff_composer input[type="search"] {width: 75%;}\n	.ff_composer ul.dynamic_recipients {list-style: none; padding:0;}\n	.ff_composer .field_label {font-weight: bold;}\n</style>\n<div class="ff_composer">\n{composer:page}\n	{composer:rows}\n			<div class="line">\n		{composer:columns}\n				<div class="unit size1of{composer:column_total}">\n				{if composer:field_total == 0}\n					 \n				{/if}\n			{composer:fields}\n				{if composer:field_label}\n					{if composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n						<p>\n							<strong>{composer:field_label}</strong>\n						</p>\n						{/if}\n					{if:else}\n						<label class="field_label" {if composer:field_name != ''''}for="freeform_{composer:field_name}"{/if}>\n							{composer:field_label}{if composer:field_required}<span class="required_item">*</span>{/if}\n						</label>\n					{/if}\n				{/if}\n				{if composer:field_output}\n					{if composer:field_type == ''nonfield_title''}\n						<h2>{composer:field_output}</h2>\n					{if:elseif composer:field_type == ''nonfield_captcha''}\n						{if freeform:captcha}\n								{freeform:captcha}<br />\n								<input type="text" name="captcha" value=""\n									   size="20"   maxlength="20" style="width:140px;" />\n						{/if}\n					{if:else}\n						<p>{composer:field_output}</p>\n					{/if}\n				{/if}\n			{/composer:fields}\n				</div>\n		{/composer:columns}\n			</div>\n	{/composer:rows}\n{/composer:page}\n</div>\n', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `site_id`, `field_name`, `field_label`, `field_type`, `settings`, `author_id`, `entry_date`, `edit_date`, `required`, `submissions_page`, `moderation_page`, `composer_use`, `field_description`) VALUES
(1, 1, 'first_name', 'First Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s first name.'),
(2, 1, 'last_name', 'Last Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s last name.'),
(3, 1, 'email', 'Email', 'text', '{"field_length":150,"field_content_type":"email"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.'),
(4, 1, 'user_message', 'Message', 'textarea', '{"field_ta_rows":6}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s message.'),
(5, 1, 'home_drop_zone', 'Home Drop Zone', 'text', '{"field_length":"150","field_content_type":"any","disallow_html_rendering":"y"}', 1, 1349915979, 0, 'n', 'y', 'y', 'y', ''),
(6, 1, 'number_of_times_you_have_jumped', 'Number of times you have jumped', 'text', '{"field_length":"150","field_content_type":"any","disallow_html_rendering":"y"}', 1, 1349916019, 0, 'n', 'y', 'y', 'y', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `exp_freeform_fieldtypes`
--

INSERT INTO `exp_freeform_fieldtypes` (`fieldtype_id`, `fieldtype_name`, `settings`, `default_field`, `version`) VALUES
(1, 'file_upload', '[]', 'n', '4.0.7'),
(2, 'mailinglist', '[]', 'n', '4.0.7'),
(3, 'text', '[]', 'n', '4.0.7'),
(4, 'textarea', '[]', 'n', '4.0.7'),
(5, 'checkbox', '[]', 'n', '4.0.7'),
(6, 'checkbox_group', '[]', 'n', '4.0.7'),
(7, 'country_select', '[]', 'n', '4.0.7'),
(8, 'file_upload', '[]', 'n', '4.0.7'),
(9, 'hidden', '[]', 'n', '4.0.7'),
(10, 'mailinglist', '[]', 'n', '4.0.7'),
(11, 'multiselect', '[]', 'n', '4.0.7'),
(12, 'province_select', '[]', 'n', '4.0.7'),
(13, 'radio', '[]', 'n', '4.0.7'),
(14, 'select', '[]', 'n', '4.0.7'),
(15, 'state_select', '[]', 'n', '4.0.7'),
(16, 'text', '[]', 'n', '4.0.7'),
(17, 'textarea', '[]', 'n', '4.0.7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_file_uploads`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_freeform_file_uploads`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_forms`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_freeform_forms`
--

INSERT INTO `exp_freeform_forms` (`form_id`, `site_id`, `form_name`, `form_label`, `default_status`, `notify_user`, `notify_admin`, `user_email_field`, `user_notification_id`, `admin_notification_id`, `admin_notification_email`, `form_description`, `field_ids`, `field_order`, `template_id`, `composer_id`, `author_id`, `entry_date`, `edit_date`, `settings`) VALUES
(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'alex@e-digitalgroup.com', 'This is a basic contact form.', '1|2|3|4', '1|2|4|3', 0, 0, 1, 1349915768, 1349987408, NULL),
(2, 1, 'event_registration', 'Event Registration', 'pending', 'y', 'y', '3', 0, 0, 'alex@e-digitalgroup.com', '', '1|2|3|4|5|6', '3|1|2|5|6', 0, 1, 1, 1349915910, 1351118415, NULL),
(3, 1, 'send_us_a_message', 'Send Us a Message', 'pending', 'n', 'y', '', 1, 0, 'alex@e-digitalgroup.com', 'Used on About Us page', '1|2|3|4|5', NULL, 0, 2, 1, 1351125361, 1351200850, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_1`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_1` text,
  `form_field_2` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_form_entries_1`
--

INSERT INTO `exp_freeform_form_entries_1` (`entry_id`, `site_id`, `author_id`, `complete`, `ip_address`, `entry_date`, `edit_date`, `status`, `form_field_1`, `form_field_2`, `form_field_3`, `form_field_4`) VALUES
(1, 1, 0, 'y', '127.0.0.1', 1349915768, 0, 'pending', 'Jake', 'Solspace', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_2`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_1` text,
  `form_field_2` text,
  `form_field_3` text,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_freeform_form_entries_2`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_3`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_3` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_1` text,
  `form_field_2` text,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_5` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_freeform_form_entries_3`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_multipage_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_freeform_multipage_hashes`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_notification_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_notification_templates`
--

INSERT INTO `exp_freeform_notification_templates` (`notification_id`, `site_id`, `notification_name`, `notification_label`, `notification_description`, `wordwrap`, `allow_html`, `from_name`, `from_email`, `reply_to_email`, `email_subject`, `include_attachments`, `template_data`) VALUES
(1, 1, 'admin', 'Admin', 'Admin will be notified', 'y', 'n', 'iflyworld', 'alex@e-digitalgroup.com', 'alex@e-digitalgroup.com', 'Form sent', 'n', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `exp_freeform_params`
--

INSERT INTO `exp_freeform_params` (`params_id`, `entry_date`, `data`) VALUES
(16, 1351193580, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(17, 1351193622, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(18, 1351193673, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(19, 1351196340, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(20, 1351196347, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(21, 1351196352, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(22, 1351196410, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(23, 1351196447, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(24, 1351198118, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}'),
(25, 1351200749, '{"form_id":"3","edit":false,"entry_id":0,"secure_action":false,"secure_return":false,"require_captcha":false,"require_ip":true,"return":"","inline_error_return":"what-is-ifly\\/about-us","error_page":"","ajax":true,"restrict_edit_to_author":true,"inline_errors":false,"prevent_duplicate_on":"","prevent_duplicate_per_site":false,"secure_duplicate_redirect":false,"duplicate_redirect":"","error_on_duplicate":false,"required":"","matching_fields":"","last_page":true,"multipage":false,"redirect_on_timeout":true,"redirect_on_timeout_to":"","page_marker":"page","multipage_page":"","paging_url":"","multipage_page_names":"","admin_notify":"alex@e-digitalgroup.com","admin_cc_notify":"","admin_bcc_notify":"","notify_user":false,"notify_admin":true,"notify_on_edit":false,"user_email_field":"","recipients":false,"recipients_limit":"3","recipient_user_input":false,"recipient_user_limit":"3","recipient_template":"","recipient_user_template":"","admin_notification_template":"0","user_notification_template":"0","status":"pending","allow_status_edit":false,"recipients_list":[]}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_preferences`
--

INSERT INTO `exp_freeform_preferences` (`preference_id`, `preference_name`, `preference_value`, `site_id`) VALUES
(1, 'ffp', 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_freeform_user_email`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_global_variables`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img'),
(6, 3, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(7, 3, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(8, 3, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(9, 3, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(10, 3, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img'),
(11, 4, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(12, 4, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(13, 4, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(14, 4, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(15, 4, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(17, 1, 1, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:33;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:35;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:34;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(18, 1, 6, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:33;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:35;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:34;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(23, 1, 1, 6, 'a:5:{s:7:"publish";a:7:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:24;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:41;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(24, 1, 6, 6, 'a:5:{s:7:"publish";a:7:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:24;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:41;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(27, 1, 1, 10, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:64;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:65;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(28, 1, 6, 10, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:64;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:65;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:5:"pages";a:2:{s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(29, 1, 1, 16, 'a:4:{s:7:"publish";a:11:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:62;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:63;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(32, 1, 1, 7, 'a:3:{s:7:"publish";a:17:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:65;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:64;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:6:"extras";a:3:{s:10:"_tab_label";s:6:"Extras";s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(33, 1, 6, 7, 'a:3:{s:7:"publish";a:17:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:65;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:64;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:6:"extras";a:3:{s:10:"_tab_label";s:6:"Extras";s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(36, 1, 1, 2, 'a:4:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:40;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:66;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:67;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(37, 1, 6, 2, 'a:4:{s:7:"publish";a:12:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:40;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:66;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:67;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(44, 1, 1, 3, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:74;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:73;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(45, 1, 6, 3, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:74;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:73;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(46, 1, 7, 3, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:74;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:73;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(47, 1, 1, 17, 'a:6:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:68;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:69;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:70;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:71;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:72;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:79;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:78;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(48, 1, 6, 17, 'a:6:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:68;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:69;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:70;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:71;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:72;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:79;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:78;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(49, 1, 7, 17, 'a:6:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:68;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:69;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:70;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:71;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:72;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:79;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:78;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(50, 1, 1, 4, 'a:4:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:82;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:21;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:80;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:11:"ee_required";a:3:{s:10:"_tab_label";s:11:"EE Required";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(51, 1, 6, 4, 'a:4:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:82;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:21;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:80;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:11:"ee_required";a:3:{s:10:"_tab_label";s:11:"EE Required";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(52, 1, 7, 4, 'a:4:{s:7:"publish";a:14:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:82;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:21;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:80;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:11:"ee_required";a:3:{s:10:"_tab_label";s:11:"EE Required";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"pages";s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');
INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(53, 1, 1, 20, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:84;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:83;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:85;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:86;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:87;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:88;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(54, 1, 6, 20, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:84;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:83;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:85;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:86;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:87;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:88;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(55, 1, 7, 20, 'a:6:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:84;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:83;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:85;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:86;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:87;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:88;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(56, 1, 1, 19, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:89;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:81;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(57, 1, 6, 19, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:89;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:81;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(58, 1, 7, 19, 'a:6:{s:7:"publish";a:5:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:89;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:81;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:5:"pages";a:3:{s:10:"_tab_label";s:5:"Pages";s:16:"pages__pages_uri";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:24:"pages__pages_template_id";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_events`
--

CREATE TABLE IF NOT EXISTS `exp_low_events` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL,
  `field_id` int(6) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `all_day` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`event_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_low_events`
--

INSERT INTO `exp_low_events` (`event_id`, `site_id`, `entry_id`, `field_id`, `start_date`, `start_time`, `end_date`, `end_time`, `all_day`) VALUES
(1, 1, 1, 33, '2012-09-21', '17:30:00', '2012-09-21', '18:30:00', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_mailing_list`
--

CREATE TABLE IF NOT EXISTS `exp_mailing_list` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `list_id` int(7) unsigned NOT NULL,
  `authcode` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `list_id` (`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_mailing_list`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_mailing_lists`
--

CREATE TABLE IF NOT EXISTS `exp_mailing_lists` (
  `list_id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `list_name` varchar(40) NOT NULL,
  `list_title` varchar(100) NOT NULL,
  `list_template` text NOT NULL,
  PRIMARY KEY (`list_id`),
  KEY `list_name` (`list_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_mailing_lists`
--

INSERT INTO `exp_mailing_lists` (`list_id`, `list_name`, `list_title`, `list_template`) VALUES
(1, 'default', 'Default Mailing List', '{message_text}\n\nTo remove your email from this mailing list, click here:\n{if html_email}<a href=\\"{unsubscribe_url}\\">{unsubscribe_url}</a>{/if}\n{if plain_email}{unsubscribe_url}{/if}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_mailing_list_queue`
--

CREATE TABLE IF NOT EXISTS `exp_mailing_list_queue` (
  `queue_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `list_id` int(7) unsigned NOT NULL DEFAULT '0',
  `authcode` varchar(10) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`queue_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_mailing_list_queue`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `exp_matrix_cols`
--

INSERT INTO `exp_matrix_cols` (`col_id`, `site_id`, `field_id`, `var_id`, `col_name`, `col_label`, `col_instructions`, `col_type`, `col_required`, `col_search`, `col_order`, `col_width`, `col_settings`) VALUES
(1, 1, 70, NULL, 'title', 'Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(2, 1, 70, NULL, 'desc', 'Description', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(3, 1, 70, NULL, 'button', 'Button Text', '', 'text', 'n', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(4, 1, 71, NULL, 'title', 'Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(5, 1, 71, NULL, 'thumbnail', 'Thumbnail', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
(6, 1, 71, NULL, 'button', 'Button Text', '', 'text', 'n', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(7, 1, 72, NULL, 'title', 'Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(8, 1, 72, NULL, 'desc', 'Descriptor', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(9, 1, 14, NULL, 'text', 'Button Text', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(10, 1, 14, NULL, 'link', 'Button Link', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(11, 1, 81, NULL, 'title', 'Title', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(12, 1, 81, NULL, 'image', 'Image', '', 'file', 'y', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9'),
(13, 1, 81, NULL, 'copy', 'Copy', '', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(14, 1, 81, NULL, 'link', 'Link', '', 'text', 'y', 'n', 3, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(15, 1, 20, NULL, 'image1', 'Image 1', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
(16, 1, 20, NULL, 'image2', 'Image 2', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
(17, 1, 20, NULL, 'image3', 'Image 3', '', 'file', 'n', 'n', 2, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
(18, 1, 20, NULL, 'image4', 'Image 4', '', 'file', 'n', 'n', 3, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czozOiJhbGwiO30='),
(19, 1, 82, NULL, 'video1', 'Video 1', '', 'videoplayer', 'n', 'n', 0, '33%', 'YTowOnt9'),
(20, 1, 82, NULL, 'video2', 'Video 2', '', 'videoplayer', 'n', 'n', 1, '', 'YTowOnt9'),
(21, 1, 82, NULL, 'video3', 'Video 3', '', 'videoplayer', 'n', 'n', 2, '', 'YTowOnt9'),
(22, 1, 82, NULL, 'video4', 'Video 4', '', 'videoplayer', 'n', 'n', 3, '', 'YTowOnt9'),
(23, 1, 85, NULL, 'text', 'Button Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(24, 1, 85, NULL, 'link', 'Button link', '', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(25, 1, 86, NULL, 'text', 'Button Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9'),
(26, 1, 86, NULL, 'link', 'Button Link', '', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjc6ImNvbnRlbnQiO3M6MzoiYWxsIjt9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` text,
  `col_id_3` text,
  `col_id_4` text,
  `col_id_5` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` text,
  `col_id_13` text,
  `col_id_14` text,
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_matrix_data`
--

INSERT INTO `exp_matrix_data` (`row_id`, `site_id`, `entry_id`, `field_id`, `var_id`, `row_order`, `col_id_1`, `col_id_2`, `col_id_3`, `col_id_4`, `col_id_5`, `col_id_6`, `col_id_7`, `col_id_8`, `col_id_9`, `col_id_10`, `col_id_11`, `col_id_12`, `col_id_13`, `col_id_14`, `col_id_15`, `col_id_16`, `col_id_17`, `col_id_18`, `col_id_19`, `col_id_20`, `col_id_21`, `col_id_22`, `col_id_23`, `col_id_24`, `col_id_25`, `col_id_26`) VALUES
(1, 1, 18, 85, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Redeem a Gift Card', '/book-now/redeem-a-gift-card', NULL, NULL),
(2, 1, 18, 86, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Buy a Digital Gift Card', '/book-now/buy-a-gift-card'),
(3, 1, 19, 81, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'First Time Flyer', '{filedir_1}temp-fliers.jpg', 'Never flown at iFLY before…this is you. Click ‘More Info’ to see all first-time flyer packages, or click ‘Book Now.''', '/flight-info/first-time-flyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 19, 81, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Return Flyer', '{filedir_1}temp-fliers.jpg', 'If you’ve flown once before, congrats! You’re a return flyer. Click ‘More Info’ to see all return flyer packages, or click ‘Book Now.’', '/flight-info/return-flyer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'admin', 'Administrator', '64e320695e63747956853c5d68c8bb7f392281f3ca1d58d6c134460b55eb862f7b2bc80d33135f33e8a2d651c951cef213592acbd61aaa8356c68952bae62294', 'e`V3h?d*c|_y)7ihu;}ju;Hq(,mP[X0Vl7h?>~/,)=gsi9RaE%4c+-e?t%B[L6`0UP6e"^5@G^uNSd3{psJD0v;N}o0~FhNfl+Yn3,Fz[Ly%ZKl!@Ye''yat<G$T{K@W]', '8a5ca7a4428eacc18fc10ed403e10635a379b2bf', '6d21c7eb6207056521a9d5ac83d4ad7475498a46', NULL, 'alex@e-digitalgroup.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '80.239.243.112', 1345582335, 1351283785, 1351297636, 18, 0, 0, 0, 1351294680, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'NavEE|index.php?S=f8802fafbb0b7ed45cb030dbf0f9e9f4b492b928&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=navee|1\nChannels|index.php?S=b45d9146af5f0774d2ed1bfef2cffff29237387f&amp;D=cp&amp;C=admin_content&M=channel_management|2\nChannel Fields|index.php?S=b45d9146af5f0774d2ed1bfef2cffff29237387f&amp;D=cp&amp;C=admin_content&M=field_group_management|3\nPages|index.php?S=28ed0bd6cb57799375c9130f489fe482f0d4fbb4&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=pages|4\nForms|index.php?S=d0f24925d72b57c0be05e6c16ae454fc94ca9551&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=freeform|5', 'y', 0, 'y', 0),
(2, 6, 'manager', 'General Manager', 'bc2a6799509e6d9319fbe2a9bfbc6644d0e479c35c7a01f4e2c8a1f88ccfed77be2e4bd577d08b672fe3a410c05f60428c58387a15d59e83f250a95c6ffb0d0b', 'q~hbzN`jv%~s5E:mYOJ''kPdWiHkV6''.PEg=D+0O3w@!-<jL^p<mqgj,GSH,\\~;!hz%M''I?#*MDwhMir&h4lRCPkVbG.9"Q+^y`^+]gbS]K18N/;M2jwC06U/d4XTS}n_', '0ab575063776fa9a7c98f994b1a852849f1a64d8', '89170e40425a95bfb986a60e94c7864730a1a51a', NULL, 'manager@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '86.164.24.131', 1346764557, 1346964329, 1347046866, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'y', 0, 'y', 0),
(3, 6, 'stuartw', 'Stuart Wallock', 'a94acdcaafd7aba83c1515922d0996ffba8f42d5b8edccd07212702b96c0b7298737eaf536d6f48ff10a2718d5de491b486a7bf208197e18f4c605e1837ce535', 'Ww5Xxvw2NJBG3m?N~$v;VzD@0Uq4&"sovkHXWk^@[)<x5\\JZW%et6~&h~c6D)}sO6T!Rd`qm2<@irWPOlliZ#b7NqEBjC*R._U[;Vh4wq2#^''EP1I4Zebk/1}I,[>[lb', '95cc8921861795e780c2c77a48d66ef2f50c8cf4', 'a9d1a2e0a5acfb3f8897cb1107764b1bae202cb8', NULL, 'stuart@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348068960, 1349114597, 1349150899, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(4, 6, 'nayladp', 'Nayla Della Penna', '412e3675a4c1030f4f00ca40dba9d656837c97d419fa567f21e884f77068c3462c97ecd3e56926decbf981a7ae0538a1bb0c71cf8be2f8ca6a550e9d6b0bdd46', 'z`VpN>R&SiE0a1]F4_rO)]u=qpp[GlMrk6#GxGHH#Z-?Vi?cJw[(7S{SFJX\\XTehu*`geRQezBk}2{x\\uIkEYpNL9kiBBY2z7&hV?cN1If0=&{r!E*gY5BM+?weXV"Sy', '9c79715ae643ed0ffa9271f2d97737c9afb8fed9', '6c65dc13c9878f0ad27bb10d6d2f041e76bcb2be', NULL, 'ndelpen@nyc.rr.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348069070, 1349189166, 1349190903, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(5, 6, 'axelz', 'Axel Zohman', 'b806298a50106195d39db32949cee77fd4a5eb925d9330d8f4a7e1be1780cf3c74a8126ed2561ec3b5dadf51683abc2051b8d9b67f88231ae4f2acafdb935678', 'sM}>^E.jFrafi/e$LRCE2BpW''c@''}w]YW&]QmOT@^s,az9prOv-q)8xgnEmeesxkL?]@_C~`#(lyIn+7U82B2:^5RW/rta"{`=izh@jg\\hL-Kp0Ac%2~Df5(z,szy9HM', '177a1c0db85cd4da44ce026a6b9bf45db376bd64', 'ba3ac3eed2d62b8eda7e759a3ab9674693ab97ec', NULL, 'axel@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348069123, 1349123352, 1349123352, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(6, 6, 'chew', 'Che Wilcox', '525c82b34012818639c2e869a4d884df1a3115c2', '', 'd5b4155b8c8104fc84fac103b327fc9d12d4c61e', NULL, NULL, 'che@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348070946, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(7, 6, 'craigb', 'Craig Buxton', 'ab3a5066d166a14321acecedfd8206f37d87981ead9358eda9aa859eef4f8c3ea1f2c3b032d4e24071d2fa3e05cc29127711dc3d9dacfc61b6b47a065253a9c9', 'K:~o"/!Q=3:4:ST!jqr(qHTRm9UidhdHtrR8~i,=7O/K$>H`FZAS??44CSZ&VsJtItEg;9m-T+Y1\\l+(''GckC2iqiyBzLD8!Jvq2!S%7G.<T}]-+}o#F!o`0:3?|St{#', '645282baad77c5639fcffaece6155e4dcc0de75e', '6e6189aa4031d3411d14338955424df5af1bf7fa', NULL, 'Craig@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348070997, 1349911931, 1349985684, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'y', 0, 'y', 0),
(8, 6, 'royh', 'Roy Hughes', '77ab6e29a92db518e0182addee4ec2e610bae64de686483e589720f2ae21595e56b37ffcbc569c7b5d50e197f33765272d9cc6a2576a9b7c445a93c5b66953fb', 'fNrVA)\\Xf`82GS$pSz]P*>CLED9(VELcHff.1GOe{Pkj)988?"!R4&Bw_O''N/&#VK#72d8ftmg\\1]9ee=pphu4Em0]3^saWTX,]''WGsRbRK7&TzAO*zaJ4yMGgT@h7tp', '6d99e8944426f8139e27763e9c96a9e7b3d8c9cd', '5b31ea61a57a40fe9d9f2c638560bdc1f0648a38', NULL, 'roy@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348864980, 1349933753, 1349933753, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_member_bulletin_board`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_member_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(1, 3, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(1, 4, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(2, 3, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(2, 4, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 3, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 4, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 3, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 4, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(5, 3, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(5, 4, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Global Manager', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'y', 'y', 'n', 'n', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(6, 3, 'Global Manager', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'y', 'y', 'n', 'n', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(6, 4, 'Global Manager', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'y', 'y', 'n', 'n', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(7, 1, 'Local Manager', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'y', 'y', 'n', 'n', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0),
(2, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(3, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(4, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(5, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(6, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(7, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(8, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_search`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_message_attachments`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_message_copies`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_message_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', ''),
(7, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_message_listed`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`) VALUES
(1, 'Comment', '2.3', 'y', 'n'),
(2, 'Email', '2.0', 'n', 'n'),
(3, 'Emoticon', '2.0', 'n', 'n'),
(4, 'Jquery', '1.0', 'n', 'n'),
(5, 'Rss', '2.0', 'n', 'n'),
(6, 'Safecracker', '2.1', 'y', 'n'),
(7, 'Search', '2.2', 'n', 'n'),
(8, 'Channel', '2.0.1', 'n', 'n'),
(9, 'Member', '2.1', 'n', 'n'),
(10, 'Stats', '2.0', 'n', 'n'),
(11, 'Rte', '1.0', 'y', 'n'),
(12, 'Mx_google_map', '1.4', 'y', 'n'),
(13, 'Navee', '2.2.5', 'y', 'n'),
(14, 'Playa', '4.3.3', 'n', 'n'),
(15, 'Seo_lite', '1.3.6', 'y', 'y'),
(17, 'Cp_analytics', '2.0.7', 'y', 'n'),
(18, 'Low_events', '1.0.4', 'n', 'n'),
(20, 'Freeform', '4.0.7', 'y', 'n'),
(21, 'Mailinglist', '3.1', 'y', 'n'),
(22, 'Pages', '2.2', 'y', 'y'),
(23, 'Videoplayer', '3.1.1', 'y', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(7, 1),
(7, 6),
(7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `exp_mx_google_map`
--

CREATE TABLE IF NOT EXISTS `exp_mx_google_map` (
  `point_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` varchar(10) NOT NULL DEFAULT '',
  `latitude` varchar(50) NOT NULL DEFAULT '',
  `longitude` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `zipcode` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(50) NOT NULL DEFAULT '',
  `field_id` varchar(10) NOT NULL DEFAULT '',
  `icon` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_mx_google_map`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_mx_google_map_fields`
--

CREATE TABLE IF NOT EXISTS `exp_mx_google_map_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL,
  `field_name` varchar(128) NOT NULL DEFAULT '',
  `field_label` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_maxl` varchar(50) NOT NULL DEFAULT '',
  `field_pattern` varchar(256) NOT NULL DEFAULT '',
  `field_order` int(10) DEFAULT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_mx_google_map_fields`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_navee`
--

CREATE TABLE IF NOT EXISTS `exp_navee` (
  `navee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `template` int(10) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `include` tinyint(4) DEFAULT NULL,
  `passive` tinyint(4) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `rel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `custom_kids` varchar(255) DEFAULT NULL,
  `access_key` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`navee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `exp_navee`
--

INSERT INTO `exp_navee` (`navee_id`, `navigation_id`, `site_id`, `entry_id`, `channel_id`, `template`, `type`, `parent`, `text`, `link`, `class`, `id`, `sort`, `include`, `passive`, `datecreated`, `dateupdated`, `ip_address`, `member_id`, `rel`, `name`, `target`, `title`, `regex`, `custom`, `custom_kids`, `access_key`) VALUES
(1, 1, 1, 0, 0, 17, 'manual', 0, 'What is iFly?', '/what-is-ifly/', '', 'what', 1, 1, 0, '2012-10-11 12:32:04', '2012-10-11 12:32:04', '173.198.122.173', 1, '', '', '', '', '', '', '<span>', ''),
(2, 1, 1, 0, 0, 19, 'manual', 1, 'The Flying Experience', '/what-is-ifly/the-flying-experience', '', '', 2, 1, 0, '2012-10-11 12:34:37', '2012-10-11 12:34:37', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(3, 1, 1, 0, 0, 19, 'manual', 1, 'FAQs', '/what-is-ifly/faqs', '', '', 5, 1, 0, '2012-10-11 12:36:06', '2012-10-11 12:36:06', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(4, 1, 1, 0, 0, 1, 'manual', 1, 'Video Testimonials', '/what-is-ifly/video-testimonials', '', '', 3, 1, 0, '2012-10-11 12:36:30', '2012-10-11 12:36:30', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(5, 1, 1, 0, 0, 1, 'manual', 1, 'Ratings & Reviews', '/what-is-ifly/ratings-and-reviews', '', '', 4, 1, 0, '2012-10-11 12:36:56', '2012-10-11 12:36:56', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(6, 1, 1, 0, 0, 19, 'manual', 0, 'Book Now', '/book-now/', '', 'shop', 3, 1, 0, '2012-10-11 12:40:17', '2012-10-11 12:40:17', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(7, 1, 1, 0, 0, 19, 'manual', 0, 'Flight Info', '/flight-info/', '', 'media', 4, 1, 0, '2012-10-11 12:40:40', '2012-10-11 12:40:40', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(8, 1, 1, 0, 0, 19, 'manual', 7, 'Flyer Guide', '/flight-info/flyer-guide', '', '', 1, 1, 0, '2012-10-11 12:41:12', '2012-10-11 12:41:12', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(9, 1, 1, 0, 0, 19, 'manual', 7, 'First Time Flyer', '/flight-info/first-time-flyer', '', '', 2, 1, 0, '2012-10-11 12:41:42', '2012-10-11 12:41:42', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(10, 1, 1, 0, 0, 19, 'manual', 0, 'Find a Tunnel', '/find-a-tunnel/', '', 'find', 5, 1, 0, '2012-10-11 12:42:05', '2012-10-11 12:42:05', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(11, 2, 1, 0, 0, 1, 'manual', 0, 'What is iFly?', '/what-is-ifly/', '', '', 1, 1, 0, '2012-10-11 12:43:17', '2012-10-11 12:43:17', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(13, 2, 1, 0, 0, 19, 'manual', 11, 'The Flying Experience', '/what-is-ifly/the-flying-experience', '', '', 1, 1, 0, '2012-10-11 12:43:54', '2012-10-11 12:43:54', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(14, 2, 1, 0, 0, 1, 'manual', 11, 'Video Testimonials', '/what-is-ifly/video-testimonials', '', '', 3, 1, 0, '2012-10-11 12:44:20', '2012-10-11 12:44:20', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(15, 2, 1, 0, 0, 1, 'manual', 11, 'Ratings & Reviews', '/what-is-ifly/ratings-and-reviews', '', '', 4, 1, 0, '2012-10-11 12:44:39', '2012-10-11 12:44:39', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(16, 2, 1, 0, 0, 19, 'manual', 0, 'Flight Info', '/flight-info/', '', '', 4, 1, 0, '2012-10-11 12:44:55', '2012-10-11 12:44:55', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(17, 2, 1, 0, 0, 19, 'manual', 16, 'Flyer Guide', '/flight-info/flyer-guide', '', '', 1, 1, 0, '2012-10-11 12:45:14', '2012-10-11 12:45:14', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(18, 2, 1, 0, 0, 19, 'manual', 16, 'First Time Flyer', '/flight-info/first-time-flyer', '', '', 2, 1, 0, '2012-10-11 12:45:34', '2012-10-11 12:45:34', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(19, 2, 1, 0, 0, 19, 'manual', 16, 'Return Flyer', '/flight-info/return-flyer', '', '', 3, 1, 0, '2012-10-11 12:45:57', '2012-10-11 12:45:57', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(20, 2, 1, 0, 0, 19, 'manual', 16, 'Experienced Flyer', '/flight-info/experienced-flyer', '', '', 4, 1, 0, '2012-10-11 12:46:25', '2012-10-11 12:46:25', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(21, 2, 1, 0, 0, 19, 'manual', 16, 'Group or Party', '/flight-info/group-or-party', '', '', 5, 1, 0, '2012-10-11 12:46:52', '2012-10-11 12:46:52', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(22, 2, 1, 0, 0, 19, 'manual', 16, 'iFly Pics & Videos', '/flight-info/ifly-pics-and-videos', '', '', 6, 1, 0, '2012-10-11 12:47:19', '2012-10-11 12:47:19', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(23, 2, 1, 0, 0, 19, 'manual', 16, 'Gift Cards', '/flight-info/gift-cards', '', '', 7, 1, 0, '2012-10-11 12:47:48', '2012-10-11 12:47:48', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(24, 2, 1, 0, 0, 19, 'manual', 0, 'Book Now', '/book-now/', '', '', 2, 1, 0, '2012-10-11 12:48:01', '2012-10-11 12:48:01', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(25, 2, 1, 0, 0, 19, 'manual', 24, 'Book your Flight', '/book-now/book-your-flight', '', '', 1, 1, 0, '2012-10-11 12:48:21', '2012-10-11 12:48:21', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(26, 2, 1, 0, 0, 19, 'manual', 24, 'Buy a Gift Card', '/book-now/buy-a-gift-card', '', '', 2, 1, 0, '2012-10-11 12:48:38', '2012-10-11 12:48:38', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(27, 2, 1, 0, 0, 1, 'manual', 0, 'My iFly', '/my-ifly/', '', '', 3, 1, 0, '2012-10-11 12:49:36', '2012-10-11 12:49:36', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(30, 2, 1, 0, 0, 1, 'manual', 27, 'My Profile', '/my-ifly/my-profile', '', '', 3, 1, 0, '2012-10-11 12:50:52', '2012-10-11 12:50:52', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(33, 2, 1, 0, 0, 19, 'manual', 27, 'I.B.A Training Hours', 'http://iba.com/about', '', '', 4, 1, 0, '2012-10-11 12:53:00', '2012-10-11 12:53:00', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(34, 2, 1, 0, 0, 1, 'manual', 0, 'Legal', '/legal/', '', '', 6, 1, 0, '2012-10-11 12:53:11', '2012-10-11 12:53:11', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(35, 2, 1, 0, 0, 1, 'manual', 34, 'Waiver', '/legal/waiver', '', '', 1, 1, 0, '2012-10-11 12:53:33', '2012-10-11 12:53:33', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(36, 2, 1, 0, 0, 1, 'manual', 34, 'Terms', '/legal/terms', '', '', 2, 1, 0, '2012-10-11 12:53:54', '2012-10-11 12:53:54', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(37, 2, 1, 0, 0, 1, 'manual', 34, 'Privacy', '/legal/privacy', '', '', 3, 1, 0, '2012-10-11 12:54:19', '2012-10-11 12:54:19', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(38, 3, 1, 0, 0, 1, 'manual', 0, 'Book Now', '/book-now/', '', '', 1, 1, 0, '2012-10-23 19:17:05', '2012-10-23 19:17:05', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(39, 3, 1, 0, 0, 1, 'manual', 0, 'Waiver', '/legal/waiver', '', '', 2, 1, 0, '2012-10-23 19:17:25', '2012-10-23 19:17:25', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(40, 3, 1, 0, 0, 1, 'manual', 0, 'Contact Us', '/about-ifly/contact-us', '', '', 3, 1, 0, '2012-10-23 19:17:43', '2012-10-23 19:17:43', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(41, 1, 1, 0, 0, 19, 'manual', 1, 'The sport of Flying', '/what-is-ifly/the-sport-of-flying', '', '', 6, 1, 0, '2012-10-24 23:22:28', '2012-10-24 23:22:28', '127.0.0.1', 1, '', '', '', '', '', '', '', ''),
(42, 1, 1, 0, 0, 19, 'manual', 1, 'About Us', '/what-is-ifly/about-us', '', '', 7, 1, 0, '2012-10-24 23:23:11', '2012-10-24 23:23:11', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(43, 1, 1, 0, 0, 19, 'manual', 6, 'Book Your Flight', '/book-now/book-your-flight', '', '', 1, 1, 0, '2012-10-24 23:24:15', '2012-10-24 23:24:15', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(44, 1, 1, 0, 0, 19, 'manual', 6, 'Buy a Gift Card', '/book-now/buy-a-gift-card', '', '', 2, 1, 0, '2012-10-24 23:24:44', '2012-10-24 23:24:44', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(45, 1, 1, 0, 0, 19, 'manual', 6, 'Redeem a Gift Card', '/book-now/redeem-a-gift-card', '', '', 3, 1, 0, '2012-10-24 23:25:07', '2012-10-24 23:25:07', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(46, 1, 1, 0, 0, 19, 'manual', 6, 'Buy Pics & Videos', '/book-now/buy-pics-and-videos', '', '', 4, 1, 0, '2012-10-24 23:25:46', '2012-10-24 23:25:46', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(47, 1, 1, 0, 0, 19, 'manual', 7, 'Return Flyer', '/flight-info/return-flyer', '', '', 3, 1, 0, '2012-10-24 23:27:34', '2012-10-24 23:27:34', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(48, 1, 1, 0, 0, 19, 'manual', 7, 'Experienced Flyer', '/flight-info/experienced-flyer', '', '', 4, 1, 0, '2012-10-24 23:28:02', '2012-10-24 23:28:02', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(49, 1, 1, 0, 0, 19, 'manual', 7, 'Group or Party', '/flight-info/group-or-party', '', '', 5, 1, 0, '2012-10-24 23:28:27', '2012-10-24 23:28:27', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(50, 1, 1, 0, 0, 19, 'manual', 7, 'iFly Pics & Videos', '/flight-info/ifly-pics-and-videos', '', '', 6, 1, 0, '2012-10-24 23:28:57', '2012-10-24 23:28:57', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(51, 1, 1, 0, 0, 19, 'manual', 7, 'Gift Cards', '/flight-info/gift-cards', '', '', 7, 1, 0, '2012-10-24 23:29:26', '2012-10-24 23:29:26', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(53, 2, 1, 0, 0, 19, 'manual', 24, 'Redeem a Gift Card', '/book-now/redeem-a-gift-card', '', '', 3, 1, 0, '2012-10-24 23:40:02', '2012-10-24 23:40:02', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(54, 2, 1, 0, 0, 19, 'manual', 24, 'Buy Pics & Videos', '/book-now/buy-pics-and-videos', '', '', 4, 1, 0, '2012-10-24 23:40:21', '2012-10-24 23:40:21', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, ''),
(57, 1, 1, 0, 0, 19, 'manual', 1, 'Overview', '/what-is-ifly/overview', '', '', 1, 1, 0, '2012-10-25 21:21:53', '2012-10-25 21:21:53', '127.0.0.1', 1, '', '', '', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_cache`
--

CREATE TABLE IF NOT EXISTS `exp_navee_cache` (
  `navee_cache_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navigation_id` int(10) DEFAULT NULL,
  `group_id` smallint(4) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `recursive` tinyint(4) DEFAULT NULL,
  `ignore_include` tinyint(4) DEFAULT NULL,
  `start_from_parent` tinyint(4) DEFAULT NULL,
  `start_from_kid` tinyint(4) DEFAULT NULL,
  `single_parent` tinyint(4) DEFAULT NULL,
  `cache` longtext,
  PRIMARY KEY (`navee_cache_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `exp_navee_cache`
--

INSERT INTO `exp_navee_cache` (`navee_cache_id`, `site_id`, `navigation_id`, `group_id`, `parent`, `recursive`, `ignore_include`, `start_from_parent`, `start_from_kid`, `single_parent`, `cache`) VALUES
(9, 1, 2, 3, 0, 1, 0, 0, 0, 0, 'a:5:{i:0;a:21:{s:4:"link";s:14:"/what-is-ifly/";s:8:"navee_id";s:2:"11";s:6:"parent";s:1:"0";s:4:"text";s:13:"What is iFly?";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:3:{i:0;a:21:{s:4:"link";s:35:"/what-is-ifly/the-flying-experience";s:8:"navee_id";s:2:"13";s:6:"parent";s:2:"11";s:4:"text";s:21:"The Flying Experience";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:32:"/what-is-ifly/video-testimonials";s:8:"navee_id";s:2:"14";s:6:"parent";s:2:"11";s:4:"text";s:18:"Video Testimonials";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:33:"/what-is-ifly/ratings-and-reviews";s:8:"navee_id";s:2:"15";s:6:"parent";s:2:"11";s:4:"text";s:17:"Ratings & Reviews";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:1;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:2:"24";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:4:{i:0;a:21:{s:4:"link";s:26:"/book-now/book-your-flight";s:8:"navee_id";s:2:"25";s:6:"parent";s:2:"24";s:4:"text";s:16:"Book your Flight";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:25:"/book-now/buy-a-gift-card";s:8:"navee_id";s:2:"26";s:6:"parent";s:2:"24";s:4:"text";s:15:"Buy a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:28:"/book-now/redeem-a-gift-card";s:8:"navee_id";s:2:"53";s:6:"parent";s:2:"24";s:4:"text";s:18:"Redeem a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:29:"/book-now/buy-pics-and-videos";s:8:"navee_id";s:2:"54";s:6:"parent";s:2:"24";s:4:"text";s:17:"Buy Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:2;a:21:{s:4:"link";s:9:"/my-ifly/";s:8:"navee_id";s:2:"27";s:6:"parent";s:1:"0";s:4:"text";s:7:"My iFly";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:2:{i:0;a:21:{s:4:"link";s:19:"/my-ifly/my-profile";s:8:"navee_id";s:2:"30";s:6:"parent";s:2:"27";s:4:"text";s:10:"My Profile";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:20:"http://iba.com/about";s:8:"navee_id";s:2:"33";s:6:"parent";s:2:"27";s:4:"text";s:20:"I.B.A Training Hours";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:3;a:21:{s:4:"link";s:13:"/flight-info/";s:8:"navee_id";s:2:"16";s:6:"parent";s:1:"0";s:4:"text";s:11:"Flight Info";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:24:"/flight-info/flyer-guide";s:8:"navee_id";s:2:"17";s:6:"parent";s:2:"16";s:4:"text";s:11:"Flyer Guide";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:29:"/flight-info/first-time-flyer";s:8:"navee_id";s:2:"18";s:6:"parent";s:2:"16";s:4:"text";s:16:"First Time Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:25:"/flight-info/return-flyer";s:8:"navee_id";s:2:"19";s:6:"parent";s:2:"16";s:4:"text";s:12:"Return Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:30:"/flight-info/experienced-flyer";s:8:"navee_id";s:2:"20";s:6:"parent";s:2:"16";s:4:"text";s:17:"Experienced Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:27:"/flight-info/group-or-party";s:8:"navee_id";s:2:"21";s:6:"parent";s:2:"16";s:4:"text";s:14:"Group or Party";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/flight-info/ifly-pics-and-videos";s:8:"navee_id";s:2:"22";s:6:"parent";s:2:"16";s:4:"text";s:18:"iFly Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:23:"/flight-info/gift-cards";s:8:"navee_id";s:2:"23";s:6:"parent";s:2:"16";s:4:"text";s:10:"Gift Cards";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:4;a:21:{s:4:"link";s:7:"/legal/";s:8:"navee_id";s:2:"34";s:6:"parent";s:1:"0";s:4:"text";s:5:"Legal";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:3:{i:0;a:21:{s:4:"link";s:13:"/legal/waiver";s:8:"navee_id";s:2:"35";s:6:"parent";s:2:"34";s:4:"text";s:6:"Waiver";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:12:"/legal/terms";s:8:"navee_id";s:2:"36";s:6:"parent";s:2:"34";s:4:"text";s:5:"Terms";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:14:"/legal/privacy";s:8:"navee_id";s:2:"37";s:6:"parent";s:2:"34";s:4:"text";s:7:"Privacy";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}}'),
(13, 1, 2, 1, 0, 1, 0, 0, 0, 0, 'a:5:{i:0;a:21:{s:4:"link";s:14:"/what-is-ifly/";s:8:"navee_id";s:2:"11";s:6:"parent";s:1:"0";s:4:"text";s:13:"What is iFly?";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:3:{i:0;a:21:{s:4:"link";s:35:"/what-is-ifly/the-flying-experience";s:8:"navee_id";s:2:"13";s:6:"parent";s:2:"11";s:4:"text";s:21:"The Flying Experience";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:32:"/what-is-ifly/video-testimonials";s:8:"navee_id";s:2:"14";s:6:"parent";s:2:"11";s:4:"text";s:18:"Video Testimonials";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:33:"/what-is-ifly/ratings-and-reviews";s:8:"navee_id";s:2:"15";s:6:"parent";s:2:"11";s:4:"text";s:17:"Ratings & Reviews";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:1;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:2:"24";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:4:{i:0;a:21:{s:4:"link";s:26:"/book-now/book-your-flight";s:8:"navee_id";s:2:"25";s:6:"parent";s:2:"24";s:4:"text";s:16:"Book your Flight";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:25:"/book-now/buy-a-gift-card";s:8:"navee_id";s:2:"26";s:6:"parent";s:2:"24";s:4:"text";s:15:"Buy a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:28:"/book-now/redeem-a-gift-card";s:8:"navee_id";s:2:"53";s:6:"parent";s:2:"24";s:4:"text";s:18:"Redeem a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:29:"/book-now/buy-pics-and-videos";s:8:"navee_id";s:2:"54";s:6:"parent";s:2:"24";s:4:"text";s:17:"Buy Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:2;a:21:{s:4:"link";s:9:"/my-ifly/";s:8:"navee_id";s:2:"27";s:6:"parent";s:1:"0";s:4:"text";s:7:"My iFly";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:2:{i:0;a:21:{s:4:"link";s:19:"/my-ifly/my-profile";s:8:"navee_id";s:2:"30";s:6:"parent";s:2:"27";s:4:"text";s:10:"My Profile";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:20:"http://iba.com/about";s:8:"navee_id";s:2:"33";s:6:"parent";s:2:"27";s:4:"text";s:20:"I.B.A Training Hours";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:3;a:21:{s:4:"link";s:13:"/flight-info/";s:8:"navee_id";s:2:"16";s:6:"parent";s:1:"0";s:4:"text";s:11:"Flight Info";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:24:"/flight-info/flyer-guide";s:8:"navee_id";s:2:"17";s:6:"parent";s:2:"16";s:4:"text";s:11:"Flyer Guide";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:29:"/flight-info/first-time-flyer";s:8:"navee_id";s:2:"18";s:6:"parent";s:2:"16";s:4:"text";s:16:"First Time Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:25:"/flight-info/return-flyer";s:8:"navee_id";s:2:"19";s:6:"parent";s:2:"16";s:4:"text";s:12:"Return Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:30:"/flight-info/experienced-flyer";s:8:"navee_id";s:2:"20";s:6:"parent";s:2:"16";s:4:"text";s:17:"Experienced Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:27:"/flight-info/group-or-party";s:8:"navee_id";s:2:"21";s:6:"parent";s:2:"16";s:4:"text";s:14:"Group or Party";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/flight-info/ifly-pics-and-videos";s:8:"navee_id";s:2:"22";s:6:"parent";s:2:"16";s:4:"text";s:18:"iFly Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:23:"/flight-info/gift-cards";s:8:"navee_id";s:2:"23";s:6:"parent";s:2:"16";s:4:"text";s:10:"Gift Cards";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:4;a:21:{s:4:"link";s:7:"/legal/";s:8:"navee_id";s:2:"34";s:6:"parent";s:1:"0";s:4:"text";s:5:"Legal";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:3:{i:0;a:21:{s:4:"link";s:13:"/legal/waiver";s:8:"navee_id";s:2:"35";s:6:"parent";s:2:"34";s:4:"text";s:6:"Waiver";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:12:"/legal/terms";s:8:"navee_id";s:2:"36";s:6:"parent";s:2:"34";s:4:"text";s:5:"Terms";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:14:"/legal/privacy";s:8:"navee_id";s:2:"37";s:6:"parent";s:2:"34";s:4:"text";s:7:"Privacy";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}}'),
(20, 1, 1, 1, 0, 1, 0, 0, 0, 0, 'a:4:{i:0;a:21:{s:4:"link";s:14:"/what-is-ifly/";s:8:"navee_id";s:1:"1";s:6:"parent";s:1:"0";s:4:"text";s:13:"What is iFly?";s:5:"class";s:0:"";s:2:"id";s:4:"what";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:6:"<span>";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:22:"/what-is-ifly/overview";s:8:"navee_id";s:2:"57";s:6:"parent";s:1:"1";s:4:"text";s:8:"Overview";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:35:"/what-is-ifly/the-flying-experience";s:8:"navee_id";s:1:"2";s:6:"parent";s:1:"1";s:4:"text";s:21:"The Flying Experience";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:32:"/what-is-ifly/video-testimonials";s:8:"navee_id";s:1:"4";s:6:"parent";s:1:"1";s:4:"text";s:18:"Video Testimonials";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:33:"/what-is-ifly/ratings-and-reviews";s:8:"navee_id";s:1:"5";s:6:"parent";s:1:"1";s:4:"text";s:17:"Ratings & Reviews";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:18:"/what-is-ifly/faqs";s:8:"navee_id";s:1:"3";s:6:"parent";s:1:"1";s:4:"text";s:4:"FAQs";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/what-is-ifly/the-sport-of-flying";s:8:"navee_id";s:2:"41";s:6:"parent";s:1:"1";s:4:"text";s:19:"The sport of Flying";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:22:"/what-is-ifly/about-us";s:8:"navee_id";s:2:"42";s:6:"parent";s:1:"1";s:4:"text";s:8:"About Us";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:1;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:1:"6";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:4:"shop";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:4:{i:0;a:21:{s:4:"link";s:26:"/book-now/book-your-flight";s:8:"navee_id";s:2:"43";s:6:"parent";s:1:"6";s:4:"text";s:16:"Book Your Flight";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:25:"/book-now/buy-a-gift-card";s:8:"navee_id";s:2:"44";s:6:"parent";s:1:"6";s:4:"text";s:15:"Buy a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:28:"/book-now/redeem-a-gift-card";s:8:"navee_id";s:2:"45";s:6:"parent";s:1:"6";s:4:"text";s:18:"Redeem a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:29:"/book-now/buy-pics-and-videos";s:8:"navee_id";s:2:"46";s:6:"parent";s:1:"6";s:4:"text";s:17:"Buy Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:2;a:21:{s:4:"link";s:13:"/flight-info/";s:8:"navee_id";s:1:"7";s:6:"parent";s:1:"0";s:4:"text";s:11:"Flight Info";s:5:"class";s:0:"";s:2:"id";s:5:"media";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:24:"/flight-info/flyer-guide";s:8:"navee_id";s:1:"8";s:6:"parent";s:1:"7";s:4:"text";s:11:"Flyer Guide";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:29:"/flight-info/first-time-flyer";s:8:"navee_id";s:1:"9";s:6:"parent";s:1:"7";s:4:"text";s:16:"First Time Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:25:"/flight-info/return-flyer";s:8:"navee_id";s:2:"47";s:6:"parent";s:1:"7";s:4:"text";s:12:"Return Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:30:"/flight-info/experienced-flyer";s:8:"navee_id";s:2:"48";s:6:"parent";s:1:"7";s:4:"text";s:17:"Experienced Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:27:"/flight-info/group-or-party";s:8:"navee_id";s:2:"49";s:6:"parent";s:1:"7";s:4:"text";s:14:"Group or Party";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/flight-info/ifly-pics-and-videos";s:8:"navee_id";s:2:"50";s:6:"parent";s:1:"7";s:4:"text";s:18:"iFly Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:23:"/flight-info/gift-cards";s:8:"navee_id";s:2:"51";s:6:"parent";s:1:"7";s:4:"text";s:10:"Gift Cards";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:3;a:21:{s:4:"link";s:15:"/find-a-tunnel/";s:8:"navee_id";s:2:"10";s:6:"parent";s:1:"0";s:4:"text";s:13:"Find a Tunnel";s:5:"class";s:0:"";s:2:"id";s:4:"find";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}'),
(22, 1, 1, 3, 0, 1, 0, 0, 0, 0, 'a:4:{i:0;a:21:{s:4:"link";s:14:"/what-is-ifly/";s:8:"navee_id";s:1:"1";s:6:"parent";s:1:"0";s:4:"text";s:13:"What is iFly?";s:5:"class";s:0:"";s:2:"id";s:4:"what";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:6:"<span>";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:22:"/what-is-ifly/overview";s:8:"navee_id";s:2:"57";s:6:"parent";s:1:"1";s:4:"text";s:8:"Overview";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:35:"/what-is-ifly/the-flying-experience";s:8:"navee_id";s:1:"2";s:6:"parent";s:1:"1";s:4:"text";s:21:"The Flying Experience";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:32:"/what-is-ifly/video-testimonials";s:8:"navee_id";s:1:"4";s:6:"parent";s:1:"1";s:4:"text";s:18:"Video Testimonials";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:33:"/what-is-ifly/ratings-and-reviews";s:8:"navee_id";s:1:"5";s:6:"parent";s:1:"1";s:4:"text";s:17:"Ratings & Reviews";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:18:"/what-is-ifly/faqs";s:8:"navee_id";s:1:"3";s:6:"parent";s:1:"1";s:4:"text";s:4:"FAQs";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/what-is-ifly/the-sport-of-flying";s:8:"navee_id";s:2:"41";s:6:"parent";s:1:"1";s:4:"text";s:19:"The sport of Flying";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:22:"/what-is-ifly/about-us";s:8:"navee_id";s:2:"42";s:6:"parent";s:1:"1";s:4:"text";s:8:"About Us";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:1;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:1:"6";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:4:"shop";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:4:{i:0;a:21:{s:4:"link";s:26:"/book-now/book-your-flight";s:8:"navee_id";s:2:"43";s:6:"parent";s:1:"6";s:4:"text";s:16:"Book Your Flight";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:25:"/book-now/buy-a-gift-card";s:8:"navee_id";s:2:"44";s:6:"parent";s:1:"6";s:4:"text";s:15:"Buy a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:28:"/book-now/redeem-a-gift-card";s:8:"navee_id";s:2:"45";s:6:"parent";s:1:"6";s:4:"text";s:18:"Redeem a Gift Card";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:29:"/book-now/buy-pics-and-videos";s:8:"navee_id";s:2:"46";s:6:"parent";s:1:"6";s:4:"text";s:17:"Buy Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:2;a:21:{s:4:"link";s:13:"/flight-info/";s:8:"navee_id";s:1:"7";s:6:"parent";s:1:"0";s:4:"text";s:11:"Flight Info";s:5:"class";s:0:"";s:2:"id";s:5:"media";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:7:{i:0;a:21:{s:4:"link";s:24:"/flight-info/flyer-guide";s:8:"navee_id";s:1:"8";s:6:"parent";s:1:"7";s:4:"text";s:11:"Flyer Guide";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:29:"/flight-info/first-time-flyer";s:8:"navee_id";s:1:"9";s:6:"parent";s:1:"7";s:4:"text";s:16:"First Time Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:25:"/flight-info/return-flyer";s:8:"navee_id";s:2:"47";s:6:"parent";s:1:"7";s:4:"text";s:12:"Return Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:3;a:21:{s:4:"link";s:30:"/flight-info/experienced-flyer";s:8:"navee_id";s:2:"48";s:6:"parent";s:1:"7";s:4:"text";s:17:"Experienced Flyer";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"4";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:4;a:21:{s:4:"link";s:27:"/flight-info/group-or-party";s:8:"navee_id";s:2:"49";s:6:"parent";s:1:"7";s:4:"text";s:14:"Group or Party";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:5;a:21:{s:4:"link";s:33:"/flight-info/ifly-pics-and-videos";s:8:"navee_id";s:2:"50";s:6:"parent";s:1:"7";s:4:"text";s:18:"iFly Pics & Videos";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"6";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:6;a:21:{s:4:"link";s:23:"/flight-info/gift-cards";s:8:"navee_id";s:2:"51";s:6:"parent";s:1:"7";s:4:"text";s:10:"Gift Cards";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"7";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}}i:3;a:21:{s:4:"link";s:15:"/find-a-tunnel/";s:8:"navee_id";s:2:"10";s:6:"parent";s:1:"0";s:4:"text";s:13:"Find a Tunnel";s:5:"class";s:0:"";s:2:"id";s:4:"find";s:4:"sort";s:1:"5";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";s:0:"";s:6:"custom";s:0:"";s:11:"custom_kids";s:0:"";s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}'),
(23, 1, 3, 3, 0, 1, 0, 0, 0, 0, 'a:3:{i:0;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:2:"38";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:13:"/legal/waiver";s:8:"navee_id";s:2:"39";s:6:"parent";s:1:"0";s:4:"text";s:6:"Waiver";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:22:"/about-ifly/contact-us";s:8:"navee_id";s:2:"40";s:6:"parent";s:1:"0";s:4:"text";s:10:"Contact Us";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}'),
(24, 1, 3, 1, 0, 1, 0, 0, 0, 0, 'a:3:{i:0;a:21:{s:4:"link";s:10:"/book-now/";s:8:"navee_id";s:2:"38";s:6:"parent";s:1:"0";s:4:"text";s:8:"Book Now";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"1";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:1;a:21:{s:4:"link";s:13:"/legal/waiver";s:8:"navee_id";s:2:"39";s:6:"parent";s:1:"0";s:4:"text";s:6:"Waiver";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"2";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}i:2;a:21:{s:4:"link";s:22:"/about-ifly/contact-us";s:8:"navee_id";s:2:"40";s:6:"parent";s:1:"0";s:4:"text";s:10:"Contact Us";s:5:"class";s:0:"";s:2:"id";s:0:"";s:4:"sort";s:1:"3";s:7:"include";s:1:"1";s:7:"passive";s:1:"0";s:3:"rel";s:0:"";s:4:"name";s:0:"";s:6:"target";s:0:"";s:10:"access_key";s:0:"";s:5:"title";s:0:"";s:5:"regex";N;s:6:"custom";s:0:"";s:11:"custom_kids";N;s:8:"entry_id";s:1:"0";s:10:"channel_id";s:1:"0";s:9:"link_type";s:6:"manual";s:4:"kids";a:0:{}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_config`
--

CREATE TABLE IF NOT EXISTS `exp_navee_config` (
  `navee_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  PRIMARY KEY (`navee_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_navee_config`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_members`
--

CREATE TABLE IF NOT EXISTS `exp_navee_members` (
  `navee_mem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navee_id` int(10) DEFAULT NULL,
  `members` text,
  PRIMARY KEY (`navee_mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_navee_members`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_navs`
--

CREATE TABLE IF NOT EXISTS `exp_navee_navs` (
  `navigation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `nav_title` varchar(255) DEFAULT NULL,
  `nav_name` varchar(255) DEFAULT NULL,
  `nav_description` varchar(255) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`navigation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_navee_navs`
--

INSERT INTO `exp_navee_navs` (`navigation_id`, `site_id`, `nav_title`, `nav_name`, `nav_description`, `datecreated`, `dateupdated`, `ip_address`, `member_id`) VALUES
(1, 1, 'global_nav_header', 'Global Header Navigation', 'Navigation for global site', '2012-10-11 12:29:18', '2012-10-11 12:55:53', '173.198.122.173', 1),
(2, 1, 'global_nav_footer', 'Global Footer Navigation', 'Global Footer Navigation', '2012-10-11 12:42:49', '2012-10-11 12:42:49', '173.198.122.173', 1),
(3, 1, 'global_nav_tertiary', 'Tertiary Nav', 'Appears top right head of page.', '2012-10-23 19:15:50', '2012-10-23 19:20:28', '127.0.0.1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_nsm_addon_settings`
--

CREATE TABLE IF NOT EXISTS `exp_nsm_addon_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) unsigned NOT NULL DEFAULT '1',
  `addon_id` varchar(255) NOT NULL,
  `settings` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_nsm_addon_settings`
--

INSERT INTO `exp_nsm_addon_settings` (`id`, `site_id`, `addon_id`, `settings`) VALUES
(1, 1, 'nsm_pp_workflow', '{"enabled":"1","channels":{"9":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"8":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"4":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"5":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"2":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"7":{"enabled":"1","next_review":"60","email_author":"1","recipients":"","state":"pending"},"3":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"6":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"1":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"}},"notifications":{"from_name":"","from_email":"","subject":"","message":""}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_nsm_pp_workflow_entries`
--

CREATE TABLE IF NOT EXISTS `exp_nsm_pp_workflow_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `entry_state` varchar(10) DEFAULT NULL,
  `last_review_date` int(10) DEFAULT NULL,
  `next_review_date` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_nsm_pp_workflow_entries`
--

INSERT INTO `exp_nsm_pp_workflow_entries` (`id`, `entry_id`, `channel_id`, `entry_state`, `last_review_date`, `next_review_date`, `site_id`) VALUES
(1, 3, 7, 'pending', 0, 1355172482, 1),
(2, 4, 16, '', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=123 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(84, 3, 0, 'n', '', '127.0.0.1', 1350951896, ''),
(85, 3, 0, 'n', '', '127.0.0.1', 1350951896, ''),
(86, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(88, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(93, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(95, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(97, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(100, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(101, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(105, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(106, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(107, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(108, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(109, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(110, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(111, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(113, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(118, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(120, 1, 0, 'n', '', '127.0.0.1', 1351295843, ''),
(121, 1, 1, 'n', 'Administrator', '127.0.0.1', 1351294684, ''),
(122, 1, 0, 'n', '', '127.0.0.1', 1351295843, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_pages_configuration`
--

CREATE TABLE IF NOT EXISTS `exp_pages_configuration` (
  `configuration_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(8) unsigned NOT NULL DEFAULT '1',
  `configuration_name` varchar(60) NOT NULL,
  `configuration_value` varchar(100) NOT NULL,
  PRIMARY KEY (`configuration_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_pages_configuration`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `exp_password_lockout`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_ping_servers`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_playa_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_playa_relationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_relationships`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_remember_me`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_reset_password`
--

INSERT INTO `exp_reset_password` (`reset_id`, `member_id`, `resetcode`, `date`) VALUES
(2, 1, 'Z4U5jiZy', 1346761428);

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_revision_tracker`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_search`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_search_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=572 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `ip_address`, `hash`) VALUES
(373, 1351283554, '127.0.0.1', 'b99472f89560a68c3253a282e33c2eeff4c351bb'),
(374, 1351283555, '127.0.0.1', 'd3e093337d0d70bdc981d5d08c666b5ecb1ac2ac'),
(375, 1351283559, '127.0.0.1', '9b2030aaac382398fa582a80e524c8107e87b606'),
(376, 1351283715, '127.0.0.1', 'c95de1317044838d4e056409914fd922287c0ad2'),
(377, 1351283716, '127.0.0.1', 'a339770c65c4500c9514c34841a20a52a2bc29fb'),
(378, 1351283717, '127.0.0.1', 'dfd44d8b2d32a5e2f1c81488cb8bda9b16f4f6e0'),
(379, 1351283718, '127.0.0.1', 'b808ba695266290133e15094e97a81a93d17ee3c'),
(380, 1351283785, '127.0.0.1', '773cbf0cdcdb197364f722ee131c216255193b72'),
(381, 1351283809, '127.0.0.1', '3d2b8cc25ec47c1318fcd09aa086ea7e3414e0bb'),
(382, 1351283811, '127.0.0.1', '5636f39241b349097a42b021bbf3f913db7f1f54'),
(383, 1351283812, '127.0.0.1', '8494a5bb83a6658ccc6b89365109d36bdd8d9827'),
(384, 1351283836, '127.0.0.1', 'c9fee01f48b2dc65a88a93b84d0e3257fd38be49'),
(385, 1351283838, '127.0.0.1', 'ccf7e19c0bcd329f64e349bbba89f5551c165b73'),
(386, 1351283845, '127.0.0.1', 'dbb2ff17432e850cede0889e7224d3c20b6ac679'),
(387, 1351283852, '127.0.0.1', 'ea33d82ec30d88528a97fc38fff9062191610dfe'),
(388, 1351283853, '127.0.0.1', 'c663a9f8ae5195e883ca0b8ce0bf778e0d24c9c8'),
(389, 1351283854, '127.0.0.1', '1eb0b9435dcd2bf81557ae4179c9d560b805d70d'),
(390, 1351283855, '127.0.0.1', 'bf24bcf52e653922eb5b4b683c31a14280697d14'),
(391, 1351283868, '127.0.0.1', 'f2e2eaf9a39d03da3bf8a7a04c210b4554a0f0fe'),
(392, 1351283897, '127.0.0.1', '35425bd1fbdc00809543c008ad9380b98180b628'),
(393, 1351283917, '127.0.0.1', '0ae09981220caad26882f338f492be585b15a515'),
(394, 1351283917, '127.0.0.1', '11cf0acadc2ef3f32746314a93b2ee8dc1419a96'),
(395, 1351283923, '127.0.0.1', '98a1d0b378e2a9e83a4afe75eb19bc5fa9f6f239'),
(396, 1351283924, '127.0.0.1', '8600e63587e3d049788219eb7a77a42d7aed8ff0'),
(397, 1351283925, '127.0.0.1', '4c9e47fee947f277ed7cd1a94051ffd10a99c48b'),
(398, 1351283925, '127.0.0.1', '1e9310ed5926990ffa4584c6328579dbeee8a2f0'),
(399, 1351283993, '127.0.0.1', 'bf2e6585eae840c27ee0d7a0d88b5ce421823cb1'),
(400, 1351283999, '127.0.0.1', '73ac10c74d445ebc9d8b95f913a7a8d5e9a7bc82'),
(401, 1351284012, '127.0.0.1', 'f8f3df6d24090f66c75fa2ad8b5028be9ee9f23f'),
(402, 1351290812, '127.0.0.1', 'ac805f06b6515641442ad8021f2de4e3c0eedf4d'),
(403, 1351290949, '127.0.0.1', '72e56c310156bd34ab9ad28dd6746e7285e00d71'),
(404, 1351290949, '127.0.0.1', '34b40dd67a4a96e3ca2870686d4cc19df1a970b4'),
(405, 1351291038, '127.0.0.1', '4dbe082bbda538505f28efbe3289a8b2fa5f9c24'),
(406, 1351291040, '127.0.0.1', 'baadd5492787b8ef19e287ed287cea78c9f3f8d4'),
(407, 1351291569, '127.0.0.1', 'bf860bd0099c26571126016fdd636274097e774f'),
(408, 1351291569, '127.0.0.1', '2f2b5ae8094d454d9aa6eb3b13fd9a5d1f65a67a'),
(409, 1351291576, '127.0.0.1', 'd906760278594059d07e9c057325252ee0ab7f25'),
(410, 1351291595, '127.0.0.1', '6e9b8706659ccbbe91bd0941b905317d9812d4ca'),
(411, 1351291623, '127.0.0.1', '1a3ac711c34a032a1b0eef82274e7fbdebc02b4b'),
(412, 1351291623, '127.0.0.1', '08ea23388462484783d5e5dcd1abda8de5859f22'),
(413, 1351291629, '127.0.0.1', 'f4c481b4e9570e963369d23e6fff153e320a25e1'),
(414, 1351291636, '127.0.0.1', '9cd417db6a4c90cb7ce839d033cac2f259a55cbc'),
(415, 1351291639, '127.0.0.1', '2373714ecc894ff0c3ffeb49de4e08ba8be01a28'),
(416, 1351291639, '127.0.0.1', 'a0aa0eca8a967b1fec87e2ec84d204408ad2cf02'),
(417, 1351291669, '127.0.0.1', 'e63f752ba5ec7ce90d3cae304b893d51a2fe5438'),
(418, 1351291672, '127.0.0.1', '974acc40db1762682d104e5ce0e5cb6f3cd093e2'),
(419, 1351291676, '127.0.0.1', 'f1823b64ad96149ed9db2cf3f90ffeffdd1bbbbf'),
(420, 1351291685, '127.0.0.1', 'c617d1a1369f9555c65eadb07a9d952f299deea3'),
(421, 1351291686, '127.0.0.1', '12b504de7bec8246986ff8c78cc076a27182c2fe'),
(422, 1351291865, '127.0.0.1', 'c64587a0b3cd9b9e325b0e3b33e803f9fece3033'),
(423, 1351292002, '127.0.0.1', 'd14099ee5f785d3d882afc11dccc976d863e503b'),
(424, 1351292045, '127.0.0.1', 'ee73f7577a4dcbc83f709ace8c1533126cd62fc7'),
(425, 1351292046, '127.0.0.1', '7a8519b6ae4d712c54021b8b5b503ca1403ffe39'),
(426, 1351292054, '127.0.0.1', 'b59863aa015469433a40e2750bc9566d4d5b75ef'),
(427, 1351292066, '127.0.0.1', '5ba905ce5d78d47d90579a47c3d9dcbfaaac511a'),
(428, 1351292073, '127.0.0.1', '7f98ca7c31e047c7d67af1879dbb790f4908b05c'),
(429, 1351292074, '127.0.0.1', '7bf6955a63b235a0726bbae8bc31a40c5700ea8f'),
(430, 1351292077, '127.0.0.1', '41f09da0335fde9928281b04b99400823fc9f51e'),
(431, 1351292269, '127.0.0.1', 'f91f14ce64fd84836014a12aec4d6ca7196f35cf'),
(432, 1351292283, '127.0.0.1', 'c59b7a1da06ab2c18f37695b32dad5b36a043ff3'),
(433, 1351292318, '127.0.0.1', '3ee7b2cac65fe2daf67c1ed17459552ecadf3a7f'),
(434, 1351292321, '127.0.0.1', '346cd6459cd3f08162ca76d883ad6bff9cf6a101'),
(435, 1351292324, '127.0.0.1', '3338216c89bf030241778cafa68ee7851d69ceb2'),
(436, 1351292327, '127.0.0.1', 'dde12ee2833c997f4257b8f32eb854d81752c813'),
(437, 1351292332, '127.0.0.1', '3319c3b474288c4385b2628a900a9a49b45d0e93'),
(438, 1351292335, '127.0.0.1', '777776f094930592edb0475f5f9b040080229940'),
(439, 1351292340, '127.0.0.1', 'ba77fd0a2eb5fc6f3f101fcee7c728f3cc9c59eb'),
(440, 1351292342, '127.0.0.1', 'b7c668ca959fbecb8deaa2b50cb8044496457e75'),
(441, 1351292343, '127.0.0.1', '68f999fabd9da6661ca98748ecfa22bfeed56b09'),
(442, 1351292404, '127.0.0.1', '610dde0f281c84c1721e63124ce68114b179ea14'),
(443, 1351292410, '127.0.0.1', '95a54b7435ad5dea82a252bba4d27b07e8411c1e'),
(444, 1351292519, '127.0.0.1', '2e23cf0ef9ecacec80e28bce2412c5363743ffc0'),
(445, 1351292541, '127.0.0.1', '38c530cc4da6ced1aacb0f87ba98d59ae9b5ed10'),
(446, 1351292546, '127.0.0.1', 'ca5b805214ead19e939fbdce9984a24e47f7c420'),
(447, 1351292572, '127.0.0.1', '6cb2446202c36b460e3d14f0af0a2ca0a35d3426'),
(448, 1351292583, '127.0.0.1', 'b8839bf979c51e233d121e1ad7b9d4a8845e1d97'),
(449, 1351292610, '127.0.0.1', '7ea56f61259e7529faaa5b45b78609be675b1df3'),
(450, 1351292620, '127.0.0.1', 'cdff7d7379ab0e8ba183cf045fdf53ee10044bb3'),
(451, 1351292625, '127.0.0.1', 'a4d4263998bcd63b9d03195f377a7dfe1096451d'),
(452, 1351292626, '127.0.0.1', '677662a33d77e587d808c40ea0a857b98ef4ba8d'),
(453, 1351292628, '127.0.0.1', '052bc04baedac85d8689434ca316e049a75dec1a'),
(454, 1351292633, '127.0.0.1', '5ee0a92f9521551921496c898bdd205fb1232f1a'),
(455, 1351292634, '127.0.0.1', 'e53764713cabee9d29641505921bf7743b9b0e0c'),
(456, 1351292637, '127.0.0.1', 'a05deabf8b4f759247057c7cc44deed3606110b2'),
(457, 1351292642, '127.0.0.1', '473849fec7c736a182824118835c5e96733db59b'),
(458, 1351292642, '127.0.0.1', '2608c724b5473f2d723a2a9adca8a74c638ce2fa'),
(459, 1351292803, '127.0.0.1', '34e21d3101c68f9a7ee00723e10587b8da9445fb'),
(460, 1351292804, '127.0.0.1', '708913266b60d15656216b07d63e19baa0c3ffd1'),
(461, 1351292804, '127.0.0.1', '02a2fe40a044813ce1f9fa6d9ddc9f9eb927f17a'),
(462, 1351292805, '127.0.0.1', '58bc750e3b367244bdb0974994d92a5a5be220de'),
(463, 1351292855, '127.0.0.1', 'a3ca6a6c097cbbbf94346126524cb4f270b7bf50'),
(464, 1351292876, '127.0.0.1', 'dc4f1ee740a961e951f801468734fa1104e7db9a'),
(465, 1351292938, '127.0.0.1', '6ad8fb8ab7b42dda41e0ddd5d73acb1969eb4974'),
(466, 1351292965, '127.0.0.1', '14dbe3b40a0ea964ac7e5ea905bd968c6721e603'),
(467, 1351292965, '127.0.0.1', '3e640f35e0491981caa816c7da3bc2228b50a649'),
(468, 1351292971, '127.0.0.1', 'a20565afad60384caf04a05c9c2b1cc137fcdb44'),
(469, 1351292976, '127.0.0.1', '34c90565b38d9f5a18d5f93e6413275cdad13a45'),
(470, 1351292983, '127.0.0.1', '089c34c42c1d2fc8251bc1fe051c923e1d7d9cec'),
(471, 1351292983, '127.0.0.1', 'f567f0b6e66bdb24b21791239785155c1c155d70'),
(472, 1351292986, '127.0.0.1', '47cb4f93656bb36b0318c3d4ba51c26d72ed5602'),
(473, 1351293079, '127.0.0.1', '1c95acd1999cdc845689b416d67248f9c47d6e6f'),
(474, 1351293086, '127.0.0.1', '78a6e0e4b7eb9934e8c67dca2f46e1d5506ba26d'),
(475, 1351293090, '127.0.0.1', 'e05b6241b1a649a983fdc78aee42f6bfd326e723'),
(476, 1351294258, '127.0.0.1', '7680e5296e5328578e6eb1eb178f3aa7a1497504'),
(477, 1351294261, '127.0.0.1', '5c41f1bcc031d90581fc0569b3cb255369121a6c'),
(478, 1351294266, '127.0.0.1', '0d99497bbd922f3ca1a4429ccb3adf2e05aa8b16'),
(479, 1351294310, '127.0.0.1', '0cc4c795685b7f2322c4f3b4cdae3dbb1c4abdab'),
(480, 1351294311, '127.0.0.1', 'fd8ff3a21c55451fddaf7a0c12fd2eeb8d2a9737'),
(481, 1351294318, '127.0.0.1', '7831740e69005a06fb16d8f0338f1eb1e1cd4547'),
(482, 1351294318, '127.0.0.1', '8e1dadd8753fc105dddba8dbca9fc5ca7341c3a0'),
(483, 1351294319, '127.0.0.1', 'ebd8d10d47e1c2d075179ba14e7a237b083b241e'),
(484, 1351294319, '127.0.0.1', '8d0ee9747e45ff7c0536cd529929a8b76358a0eb'),
(485, 1351294349, '127.0.0.1', '5f72255e22f37fe88bcbba1670ceaae8e4bf84a3'),
(486, 1351294353, '127.0.0.1', '9068a628f82bab65266cf917a58dcf57c9dce0f7'),
(487, 1351294356, '127.0.0.1', '053c1823d4d6195113b6a8280cb599baada432e8'),
(488, 1351294362, '127.0.0.1', 'a348ecb9dc00720ae0679a9ddf25aee063a796f9'),
(489, 1351294369, '127.0.0.1', '4bea808a592886866b037c02b694d9bcd401ed75'),
(490, 1351294381, '127.0.0.1', '12a8cc389161b7894da84a96c3d53ffc7e3b5ead'),
(491, 1351294381, '127.0.0.1', 'd41acb960fc8d75a655363ca87bdd08ed3072040'),
(492, 1351294385, '127.0.0.1', 'cc72333a9656b0d285a548f2046068ab8d9b7f5e'),
(493, 1351294388, '127.0.0.1', '556c8a5798a4e7c85ceae4f351263a8f7d92e31c'),
(494, 1351294391, '127.0.0.1', 'c5799dcc510424704f881ee2b7d344423b5c8725'),
(495, 1351294393, '127.0.0.1', 'b34ae73548f5b5c7bfd9f9ac18b44213d3e81432'),
(496, 1351294413, '127.0.0.1', '6cda037f06c0075fde26579b4498ea2dedde93ea'),
(497, 1351294414, '127.0.0.1', 'f0fd2ef7039dc246903e7bf933483e97f5bec76f'),
(498, 1351294435, '127.0.0.1', '2b873626a2fe61c1b21a6429f8ff72e887e9986b'),
(499, 1351294437, '127.0.0.1', 'db34c2210cb0ca7acf2463431fe36eee4966f90f'),
(500, 1351294437, '127.0.0.1', '70c73594ac12fc9469e202d951856877b963dcb5'),
(501, 1351294437, '127.0.0.1', '642871143b8937e8dcecfaeae6b678a1c7f33373'),
(502, 1351294459, '127.0.0.1', 'daa588953b8504e2b47dfd0cc3af19e9441ed9e4'),
(503, 1351294513, '127.0.0.1', '8fa6280ee1302b46fe12acc3011b9248b02c09a6'),
(504, 1351294517, '127.0.0.1', '0277281dac0882fb1a9f08d987c595e8bb6330a3'),
(505, 1351294519, '127.0.0.1', 'df48697fe6f7fb246c114957d694e17d8df6fe26'),
(506, 1351294548, '127.0.0.1', '8228defacdd3b5a702caa0e39d550d9c32f17079'),
(507, 1351294559, '127.0.0.1', 'f6c6dbebab332fc1ec89da7ac63ab069838a7c88'),
(508, 1351294559, '127.0.0.1', '518eae29321b1ab50b2a12786b503ea9afa2c5b9'),
(509, 1351294559, '127.0.0.1', '4cfefed7c8a1b886a781c33daa5b034194360e03'),
(510, 1351294559, '127.0.0.1', 'bd6be2154a39170e75e4a699a8c2f7898902b089'),
(511, 1351294664, '127.0.0.1', '6d95490466a9570d7e60ca6796a57d6a20abdb4d'),
(512, 1351294665, '127.0.0.1', '18fee986dfc0b2e3abb0441f4d51451354f47d8f'),
(513, 1351294667, '127.0.0.1', '5bbf1a5d165cfc1d35fc936659fc7cd63b695867'),
(514, 1351294680, '127.0.0.1', '283bb113852c9bf267d012605def8100dbab2ff1'),
(515, 1351294680, '127.0.0.1', '4f1880c6dc70bd2c7391715cdaea18d42d6ee780'),
(516, 1351294689, '127.0.0.1', 'f04b750d377c5e2713f2f2834dd844a9ec04a4fd'),
(517, 1351295033, '127.0.0.1', '9905047b2645972239d9b1b5c9f4436e10d7f7ae'),
(518, 1351295044, '127.0.0.1', '2ef7904954ff9c8f5515e16875fb04fdca819324'),
(519, 1351295167, '127.0.0.1', 'bd6374f8a85ab7785ba5d51886a288be4cf7173d'),
(520, 1351295335, '127.0.0.1', '852edb3987535ed90b4472adf3a3113342716c03'),
(521, 1351295343, '127.0.0.1', '5bc41e63ae83859e9da5746420835dab63655a75'),
(522, 1351295346, '127.0.0.1', 'b20e3de1bafe4a09f029fbba44b57de0ed1ec3af'),
(523, 1351296199, '127.0.0.1', 'efa1432c0c906a4f6b6b30e68750fa6ca525e04a'),
(524, 1351296207, '127.0.0.1', '7d4e155e4e0de3d8f6489ea69a230b6268524a8d'),
(525, 1351297331, '127.0.0.1', '3cf0532400fb1c1a22cd73b24d0bb03a84e55fb3'),
(526, 1351297390, '127.0.0.1', '571ef7211290019027a39c368a5e7d0ef76b6b5b'),
(527, 1351297393, '127.0.0.1', '180d069e3ec039df444c7f6a35098e7aab12d568'),
(528, 1351297395, '127.0.0.1', 'c5ef122be18c66a9a1adaa2c39e077e9ce22f544'),
(529, 1351297396, '127.0.0.1', 'b66862a5b1c814addbab08c303c117add0aa42a8'),
(530, 1351297513, '127.0.0.1', '9544c84712079d2e25303d74d3eebd3c52c50097'),
(531, 1351297522, '127.0.0.1', '44b4e34f95226fed251cf32538e9c9ad19622f97'),
(532, 1351297536, '127.0.0.1', 'c57c75cad5b394d1659b91146b42168f9ed579ac'),
(533, 1351297537, '127.0.0.1', '82aec9559eb0b810779661f396c7d7ddd46c42a4'),
(534, 1351297541, '127.0.0.1', '7a01fded4ecd4c57015a10ab459534495168ef50'),
(535, 1351297543, '127.0.0.1', '180a6edc5a3e891992ba95fac4cf18bd68e04145'),
(536, 1351297614, '127.0.0.1', '5643a8cf5e2b1d83336d5146ba791fc96246e974'),
(537, 1351297616, '127.0.0.1', '81d8e7222a39846b68902bbc3858d901b6a92e57'),
(538, 1351297620, '127.0.0.1', '81c219ffe5acbef8d02944cd7c27bfcefd4c9206'),
(539, 1351297630, '127.0.0.1', 'a05c2cd76ec64d75868da3ab252aa793f11c1567'),
(540, 1351297631, '127.0.0.1', 'f113eb51d5a81683ae4748a8411f278c93c203f5'),
(541, 1351297636, '127.0.0.1', 'cf658a16305278a77ecaaa60bee7acdcba2798d5'),
(542, 1351297643, '127.0.0.1', '19f565482b5eefb953bccc846b6016eca52ff530'),
(543, 1351297644, '127.0.0.1', '7a87f22469732e00eceac4df9ca52d5e4bcba09c'),
(544, 1351297650, '127.0.0.1', '9e6d0a6321fdf65d928dca4c0c6776e970c82d1d'),
(545, 1351297659, '127.0.0.1', '13bd35da59abc49739782d529f225188e6d45b69'),
(546, 1351297661, '127.0.0.1', 'c0cb5c0e363a3cd89273d1ed6b5d753a5363d9a3'),
(547, 1351297673, '127.0.0.1', '7736ef5b70d30a537541a7de77e9f89f33084fac'),
(548, 1351297703, '127.0.0.1', 'b1468987a5c36ce680ed5af9fbab558efea935a3'),
(549, 1351297704, '127.0.0.1', '68344c8f1438c4f6323da7984a3bd7020949a4a7'),
(550, 1351297710, '127.0.0.1', '9e478a31a2dc2b45dbbaa34a94bbc135f99fca48'),
(551, 1351297737, '127.0.0.1', '38991908a13ce473be95132c268cf0d082133d19'),
(552, 1351297739, '127.0.0.1', '12a7c443e25b86ad1242480a8ba60ccbb3cc6e81'),
(553, 1351297747, '127.0.0.1', '75f20cde6eeb91e3fa0759156fd49885ce4dcd4f'),
(554, 1351297750, '127.0.0.1', '7d44aace70298e12dacb949ef994e68d966a06da'),
(555, 1351297775, '127.0.0.1', 'd66fe77ea85ec3260f223eae9fa83cb37f1b38cc'),
(556, 1351297776, '127.0.0.1', '0ec5033db1b0414d07af2ffcddd58ad123146b19'),
(557, 1351297783, '127.0.0.1', '93a95d191d65293519f0fe4489336eee20dd13f9'),
(558, 1351297784, '127.0.0.1', 'b01f8f56d37d204b7064cc90d0c5b719d26a8c6d'),
(559, 1351297785, '127.0.0.1', '7a9061914f6f2f6481ee6232366043b2e2fb2776'),
(560, 1351297785, '127.0.0.1', '01db943536013818d07ca590f6ee44dbf244da0b'),
(561, 1351297808, '127.0.0.1', '2a424e4704971d6402519227c1cb8b4249be4113'),
(562, 1351297809, '127.0.0.1', '8d46b5bc47355e6fd7b4f69b388f62995c55230b'),
(563, 1351297810, '127.0.0.1', 'e5f3d232c0a4b5a70b3bf826f4275d083d0ee002'),
(564, 1351297811, '127.0.0.1', 'e760cfb0f9ffb221b37069a47e99c919687ea81d'),
(565, 1351297878, '127.0.0.1', '18870bae436f13ffec793b9d8f1fcef6ca12b197'),
(566, 1351297914, '127.0.0.1', '520c59706baa5227c5ebcf0b4503163a6c492eef'),
(567, 1351297921, '127.0.0.1', '7d8ef24b5f6b4e2c080f92e8ed04f583ee897a61'),
(568, 1351297927, '127.0.0.1', '83353c3372456ad9a0104fa97e34269fc1d9d9aa'),
(569, 1351297928, '127.0.0.1', '6059218814dba53043bcb7032fa4376707d4216b'),
(570, 1351297933, '127.0.0.1', 'b752cd8c419e7e9f52ad878db8ff1543ba344606'),
(571, 1351297935, '127.0.0.1', '209b6a7ef07a1aaa8c2f43308e3c00a3c2e4027b');

-- --------------------------------------------------------

--
-- Table structure for table `exp_seolite_config`
--

CREATE TABLE IF NOT EXISTS `exp_seolite_config` (
  `seolite_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned DEFAULT NULL,
  `template` text,
  `default_keywords` varchar(1024) NOT NULL,
  `default_description` varchar(1024) NOT NULL,
  `default_title_postfix` varchar(60) NOT NULL,
  PRIMARY KEY (`seolite_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_seolite_config`
--

INSERT INTO `exp_seolite_config` (`seolite_config_id`, `site_id`, `template`, `default_keywords`, `default_description`, `default_title_postfix`) VALUES
(1, 1, '<title>{title}{site_name}</title>\n<meta name=''keywords'' content=''{meta_keywords}'' />\n<meta name=''description'' content=''{meta_description}'' />\n<link rel=''canonical'' href=''{canonical_url}'' />\n<!-- generated by seo_lite -->', 'your, default, keywords, here', 'Your default description here', ' |&nbsp;');

-- --------------------------------------------------------

--
-- Table structure for table `exp_seolite_content`
--

CREATE TABLE IF NOT EXISTS `exp_seolite_content` (
  `seolite_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) NOT NULL,
  `entry_id` int(10) NOT NULL,
  `title` varchar(1024) DEFAULT NULL,
  `keywords` varchar(1024) NOT NULL,
  `description` text,
  PRIMARY KEY (`seolite_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `exp_seolite_content`
--

INSERT INTO `exp_seolite_content` (`seolite_content_id`, `site_id`, `entry_id`, `title`, `keywords`, `description`) VALUES
(1, 1, 1, '', '', ''),
(2, 1, 2, '', '', ''),
(3, 1, 3, '', '', ''),
(4, 1, 4, '', '', ''),
(5, 1, 5, '', '', ''),
(6, 1, 6, '', '', ''),
(7, 1, 7, '', '', ''),
(8, 1, 8, '', '', ''),
(9, 1, 9, '', '', ''),
(10, 1, 10, '', '', ''),
(11, 1, 11, '', '', ''),
(12, 1, 12, '', '', ''),
(13, 1, 13, '', '', ''),
(14, 1, 14, '', '', ''),
(15, 1, 15, '', '', ''),
(16, 1, 16, '', '', ''),
(17, 1, 17, '', '', ''),
(18, 1, 18, '', '', ''),
(19, 1, 19, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_sessions`
--

INSERT INTO `exp_sessions` (`session_id`, `member_id`, `admin_sess`, `ip_address`, `user_agent`, `last_activity`) VALUES
('6eee28cee912e76a6ebc17f4039339778a1fd2f4', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2', 1351297936);

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  `site_pages` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`, `site_pages`) VALUES
(1, 'iFlyWorld', 'default_site', NULL, 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MTU6Imh0dHA6Ly9pZmx5LmRldiI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyMzoiaHR0cDovL2lmbHkuZGV2L3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyMzoiYWxleEBlLWRpZ2l0YWxncm91cC5jb20iO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjQxOiJodHRwOi8vZGV2LmlmbHl3b3JsZC5jb20vaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjQ4OiJDOlxpbmV0cHViXHd3d3Jvb3RcZGV2aWZseXdvcmxkXGltYWdlc1xjYXB0Y2hhc1wiO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjc6InJlZnJlc2giO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjM6IlVNNiI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MTY6ImRheWxpZ2h0X3NhdmluZ3MiO3M6MToieSI7czoyMToiZGVmYXVsdF9zaXRlX3RpbWV6b25lIjtzOjM6IlVNNiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czo0MDoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo2NzoiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy9jb2xvcmluZ2Jvb2svaWZseS9kZXYvdGhlbWVzLyI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDA6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjQ3OiJDOlxpbmV0cHViXHd3d3Jvb3RcZGV2aWZseXdvcmxkXGltYWdlc1xhdmF0YXJzXCI7czoxNjoiYXZhdGFyX21heF93aWR0aCI7czozOiIxMDAiO3M6MTc6ImF2YXRhcl9tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMzoiYXZhdGFyX21heF9rYiI7czoyOiI1MCI7czoxMzoiZW5hYmxlX3Bob3RvcyI7czoxOiJuIjtzOjk6InBob3RvX3VybCI7czo0NjoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxMDoicGhvdG9fcGF0aCI7czo1MzoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFxpbWFnZXNcbWVtYmVyX3Bob3Rvc1wiO3M6MTU6InBob3RvX21heF93aWR0aCI7czozOiIxMDAiO3M6MTY6InBob3RvX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEyOiJwaG90b19tYXhfa2IiO3M6MjoiNTAiO3M6MTY6ImFsbG93X3NpZ25hdHVyZXMiO3M6MToieSI7czoxMzoic2lnX21heGxlbmd0aCI7czozOiI1MDAiO3M6MjE6InNpZ19hbGxvd19pbWdfaG90bGluayI7czoxOiJuIjtzOjIwOiJzaWdfYWxsb3dfaW1nX3VwbG9hZCI7czoxOiJuIjtzOjExOiJzaWdfaW1nX3VybCI7czo1NDoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6NjE6IkM6XGluZXRwdWJcd3d3cm9vdFxkZXZpZmx5d29ybGRcaW1hZ2VzXHNpZ25hdHVyZV9hdHRhY2htZW50c1wiO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo1NDoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFxpbWFnZXNccG1fYXR0YWNobWVudHNcIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo4MzoiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy9jb2xvcmluZ2Jvb2svaWZseS9kZXYvdGVtcGxhdGVzL2RlZmF1bHRfc2l0ZS8iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJ5IjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjY5OiIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL2NvbG9yaW5nYm9vay9pZmx5L2Rldi9pbmRleC5waHAiO3M6MzI6ImEyZWJhOGFkMDI3ODcwMGRkNGU5YzY0NjEzZTIyZTJmIjtzOjc6ImVtYWlsZWQiO2E6MDp7fX0=', 'YToxOntpOjE7YTozOntzOjM6InVybCI7czoxNjoiaHR0cDovL2lmbHkuZGV2LyI7czo0OiJ1cmlzIjthOjc6e2k6NDtzOjIxOiIvYm9vay1ub3cvZmx5ZXItZ3VpZGUiO2k6MTA7czoxMzoiL3doYXQtaXMtaWZseSI7aToxMjtzOjE4OiIvd2hhdC1pcy1pZmx5L2ZhcXMiO2k6MTM7czozMzoiL3doYXQtaXMtaWZseS90aGUtc3BvcnQtb2YtZmx5aW5nIjtpOjE0O3M6MjI6Ii93aGF0LWlzLWlmbHkvYWJvdXQtdXMiO2k6MTU7czozNToiL3doYXQtaXMtaWZseS90aGUtZmx5aW5nLWV4cGVyaWVuY2UiO2k6MTY7czoyMjoiL3doYXQtaXMtaWZseS9vdmVydmlldyI7fXM6OToidGVtcGxhdGVzIjthOjc6e2k6NDtzOjI6IjMzIjtpOjEwO3M6MjoiMzMiO2k6MTI7czoyOiIzNSI7aToxMztzOjI6IjM0IjtpOjE0O3M6MjoiMzQiO2k6MTU7czoyOiIzNSI7aToxNjtzOjI6IjM1Ijt9fX0='),
(3, 'Venice Beach', 'ifly_venice', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMjoiaHR0cDovL3ZlbmljZS5pZmx5LmRldiI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyMzoiaHR0cDovL2lmbHkuZGV2L3RoZW1lcy8iO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjY3OiIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL2NvbG9yaW5nYm9vay9pZmx5L2Rldi90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MjM6ImFsZXhAZS1kaWdpdGFsZ3JvdXAuY29tIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czo0MToiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfcGF0aCI7czo0ODoiQzovaW5ldHB1Yi93d3dyb290L2RldmlmbHl3b3JsZC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6Im4iO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo3OiJyZWZyZXNoIjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVTTYiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czozOiJVTTYiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czo0MDoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6Mzk6Imh0dHA6Ly9pZmx5LmRldi91cGxvYWRzL3N5c3RlbS9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6ODM6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQ1OiJodHRwOi8vaWZseS5kZXYvdXBsb2Fkcy9zeXN0ZW0vbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6ODk6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTM6Imh0dHA6Ly9pZmx5LmRldi91cGxvYWRzL3N5c3RlbS9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6OTc6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo5MDoiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy9jb2xvcmluZ2Jvb2svaWZseS9kZXYvdXBsb2Fkcy9zeXN0ZW0vcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToibiI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjA6IiI7czo4OiJzaXRlXzQwNCI7czo5OiI0MDQvaW5kZXgiO3M6MTk6InNhdmVfdG1wbF9yZXZpc2lvbnMiO3M6MToibiI7czoxODoibWF4X3RtcGxfcmV2aXNpb25zIjtzOjE6IjUiO3M6MTE6InN0cmljdF91cmxzIjtzOjE6Im4iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToxOntzOjcyOiIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL2NvbG9yaW5nYm9vay9pZmx5L3ZlbmljZS9pbmRleC5waHAiO3M6MzI6ImUwZDFhYjZlNTZmNGZjMDkwMzFkNzFiOTY5NjhiY2Q1Ijt9', 'YToxOntpOjM7YToxOntzOjM6InVybCI7czoyMzoiaHR0cDovL3ZlbmljZS5pZmx5LmRldi8iO319'),
(4, 'Orlando', 'ifly_orlando', 'Orlando', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoxNToiaHR0cDovL2lmbHkuZGV2IjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjIzOiJodHRwOi8vaWZseS5kZXYvdGhlbWVzLyI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6Njc6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czoyMzoiYWxleEBlLWRpZ2l0YWxncm91cC5jb20iO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjQxOiJodHRwOi8vZGV2LmlmbHl3b3JsZC5jb20vaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjQ4OiJDOi9pbmV0cHViL3d3d3Jvb3QvZGV2aWZseXdvcmxkL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjc6InJlZnJlc2giO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjM6IlVNNiI7czoxMzoic2VydmVyX29mZnNldCI7czowOiIiO3M6MTY6ImRheWxpZ2h0X3NhdmluZ3MiO3M6MToieSI7czoyMToiZGVmYXVsdF9zaXRlX3RpbWV6b25lIjtzOjM6IlVNNiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjEzOiJtYWlsX3Byb3RvY29sIjtzOjQ6Im1haWwiO3M6MTE6InNtdHBfc2VydmVyIjtzOjA6IiI7czoxMzoic210cF91c2VybmFtZSI7czowOiIiO3M6MTM6InNtdHBfcGFzc3dvcmQiO3M6MDoiIjtzOjExOiJlbWFpbF9kZWJ1ZyI7czoxOiJuIjtzOjEzOiJlbWFpbF9jaGFyc2V0IjtzOjU6InV0Zi04IjtzOjE1OiJlbWFpbF9iYXRjaG1vZGUiO3M6MToibiI7czoxNjoiZW1haWxfYmF0Y2hfc2l6ZSI7czowOiIiO3M6MTE6Im1haWxfZm9ybWF0IjtzOjU6InBsYWluIjtzOjk6IndvcmRfd3JhcCI7czoxOiJ5IjtzOjIyOiJlbWFpbF9jb25zb2xlX3RpbWVsb2NrIjtzOjE6IjUiO3M6MjI6ImxvZ19lbWFpbF9jb25zb2xlX21zZ3MiO3M6MToieSI7czo4OiJjcF90aGVtZSI7czo3OiJkZWZhdWx0IjtzOjIxOiJlbWFpbF9tb2R1bGVfY2FwdGNoYXMiO3M6MToibiI7czoxNjoibG9nX3NlYXJjaF90ZXJtcyI7czoxOiJ5IjtzOjEyOiJzZWN1cmVfZm9ybXMiO3M6MToieSI7czoxOToiZGVueV9kdXBsaWNhdGVfZGF0YSI7czoxOiJ5IjtzOjI0OiJyZWRpcmVjdF9zdWJtaXR0ZWRfbGlua3MiO3M6MToibiI7czoxNjoiZW5hYmxlX2NlbnNvcmluZyI7czoxOiJuIjtzOjE0OiJjZW5zb3JlZF93b3JkcyI7czowOiIiO3M6MTg6ImNlbnNvcl9yZXBsYWNlbWVudCI7czowOiIiO3M6MTA6ImJhbm5lZF9pcHMiO3M6MDoiIjtzOjEzOiJiYW5uZWRfZW1haWxzIjtzOjA6IiI7czoxNjoiYmFubmVkX3VzZXJuYW1lcyI7czowOiIiO3M6MTk6ImJhbm5lZF9zY3JlZW5fbmFtZXMiO3M6MDoiIjtzOjEwOiJiYW5fYWN0aW9uIjtzOjg6InJlc3RyaWN0IjtzOjExOiJiYW5fbWVzc2FnZSI7czozNDoiVGhpcyBzaXRlIGlzIGN1cnJlbnRseSB1bmF2YWlsYWJsZSI7czoxNToiYmFuX2Rlc3RpbmF0aW9uIjtzOjIxOiJodHRwOi8vd3d3LnlhaG9vLmNvbS8iO3M6MTY6ImVuYWJsZV9lbW90aWNvbnMiO3M6MToieSI7czoxMjoiZW1vdGljb25fdXJsIjtzOjQwOiJodHRwOi8vZGV2LmlmbHl3b3JsZC5jb20vaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTE6InJ0ZV9lbmFibGVkIjtzOjE6InkiO3M6MjI6InJ0ZV9kZWZhdWx0X3Rvb2xzZXRfaWQiO3M6MToiMSI7fQ==', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6Mzk6Imh0dHA6Ly9pZmx5LmRldi91cGxvYWRzL3N5c3RlbS9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6ODM6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQ1OiJodHRwOi8vaWZseS5kZXYvdXBsb2Fkcy9zeXN0ZW0vbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6ODk6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTM6Imh0dHA6Ly9pZmx5LmRldi91cGxvYWRzL3N5c3RlbS9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6OTc6Ii9BcHBsaWNhdGlvbnMvWEFNUFAveGFtcHBmaWxlcy9odGRvY3MvY29sb3Jpbmdib29rL2lmbHkvZGV2L3VwbG9hZHMvc3lzdGVtL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo5MDoiL0FwcGxpY2F0aW9ucy9YQU1QUC94YW1wcGZpbGVzL2h0ZG9jcy9jb2xvcmluZ2Jvb2svaWZseS9kZXYvdXBsb2Fkcy9zeXN0ZW0vcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToibiI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjA6IiI7czo4OiJzaXRlXzQwNCI7czo5OiI0MDQvaW5kZXgiO3M6MTk6InNhdmVfdG1wbF9yZXZpc2lvbnMiO3M6MToibiI7czoxODoibWF4X3RtcGxfcmV2aXNpb25zIjtzOjE6IjUiO3M6MTE6InN0cmljdF91cmxzIjtzOjE6Im4iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToxOntzOjczOiIvQXBwbGljYXRpb25zL1hBTVBQL3hhbXBwZmlsZXMvaHRkb2NzL2NvbG9yaW5nYm9vay9pZmx5L29ybGFuZG8vaW5kZXgucGhwIjtzOjMyOiI0YTNhNjg0OTliOTM0ODc0NjdhMzMzYzM0MTJlODIwMiI7fQ==', 'YToxOntpOjQ7YToxOntzOjM6InVybCI7czoxNjoiaHR0cDovL2lmbHkuZGV2LyI7fX0=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_snippets`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}'),
(17, 3, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(18, 3, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(19, 3, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(20, 3, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(21, 3, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(22, 3, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(23, 3, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(24, 3, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(25, 3, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(26, 3, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(27, 3, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(28, 3, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(29, 3, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(30, 3, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(31, 3, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(32, 3, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}'),
(33, 4, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(34, 4, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(35, 4, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(36, 4, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(37, 4, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(38, 4, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(39, 4, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(40, 4, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(41, 4, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(42, 4, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(43, 4, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(44, 4, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(45, 4, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(46, 4, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(47, 4, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(48, 4, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 8, 8, 'Roy Hughes', 15, 0, 0, 0, 1351294379, 0, 0, 1351295843, 19, 1351295268, 1351546960),
(3, 3, 8, 8, 'Roy Hughes', 0, 0, 0, 0, 0, 0, 0, 1351021720, 5, 1346789616, 1351552904),
(4, 4, 8, 8, 'Roy Hughes', 2, 0, 0, 0, 0, 0, 0, 1351023038, 5, 1346789616, 1351627834);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000'),
(3, 3, 2, 'open', 1, '009933'),
(4, 3, 2, 'closed', 2, '990000'),
(5, 3, 3, 'open', 1, '009933'),
(6, 3, 3, 'closed', 2, '990000');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses'),
(2, 3, 'Statuses'),
(3, 3, 'Statuses');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_status_no_access`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(3, 3, 2, 'index', 'y', 'webpage', '<p>This is the home page</p>', '', 1350951435, 1, 'n', 0, '', 'n', 'n', 'o', 169),
(4, 3, 2, 'events', 'n', 'webpage', '	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">\n	<head>\n	<title>calendar</title>\n	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n	<link media="all" rel="stylesheet" type="text/css" href="/css/all.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/anythingslider.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/calendar.css">\n	<link rel="stylesheet" href="/css/calendar.css" media="screen" />\n        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>\n        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>\n	</head>\n	<body>\n<div class="content-block">\n\n			<section class="block-section">\n				<article class="block">\n					<div class="block-holder">\n                        			<div class="event-overlay"></div>\n						{exp:low_events:calendar channel="events" date="{segment_2}"}\n  <div id="calendar">\n    <div id="title">\n      <a class="prev" href="{path="events/{prev_month_url}"}" title="{prev_month format=''%F %Y''}">&larr;</a>\n      <strong>{this_month format="%F %Y"}</strong>\n      <a class="next" href="{path="events/{next_month_url}"}" title="{next_month format=''%F %Y''}">&rarr;</a>\n    </div>\n<table> \n    <thead>\n      <tr>\n        {weekdays}<th scope="col">{weekday_1}</th>{/weekdays}\n      </tr>\n    </thead>\n    <tbody>\n      {weeks}\n        <tr{if is_given_week} class="given-week"{/if}>\n          {days}\n            <td class="{if is_current}current{/if}{if is_given} given{/if}{if is_today} today{/if} {if events_on_day}highlight{/if}">\n              {if events_on_day}\n                <a href="{path="events/{day_url}"}">{day_number}</a>\n              {if:else}\n                <span>{day_number}</span>\n              {/if}\n            </td>\n          {/days}\n        </tr>\n      {/weeks}\n    </tbody>\n  </table>\n</div><!-- /#calendar -->\n{/exp:low_events:calendar}\n					</div>\n				</article>\n                <article class="block event-tile">\n                    <div class="block-holder">\n                        <div class="event-overlay"></div>\n                        <div id="calendar-widget"></div>\n                        <p>Choose a date above.</p>\n                        <h2>iFLY EVENTS</h2>\n                        <!--<a href="#" class="btn red"><em></em><span>See all Events</span></a>-->\n                    </div>\n                </article>\n				<article class="block">\n					<div class="block-holder">\n						<img src="images/img07.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>LOREM IPSUM DOLOR SIT AMIT.</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>LEARN MORE</span></a>\n					</div>\n				</article>\n			</section><!-- /.block-section -->\n\n			<section class="gallery-block">\n				<h1>OUR FAVORITE iFLY PHOTOS &amp; VIDEOS</h1>\n				<div class="gallery">\n					<div class="gholder">\n					</div>\n				</div>\n			</section><!-- /.gallery-block -->\n\n			<div class="btn-block">\n				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>\n			</div><!-- /.btn-block -->\n\n		</div>	\n<script src="/js/calendar.js"></script>\n	</body>\n</html>', '', 1348609318, 0, 'n', 0, '', 'n', 'n', 'o', 38),
(6, 1, 3, 'index', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page booking one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<article>\n						{exp:channel:entries channel="booking" limit="1" disable="categories|category_fields|member_data|pagination"} \n							<h1>{title}</h1>\n							<h2 class="note">{sub_heading}</h2>\n							{page_copy}\n						{/exp:channel:entries}\n						\n					</article>\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', '', 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 9),
(9, 1, 6, 'footer', 'y', 'webpage', '	<footer id="footer">\n		<div class="block-holder">\n			<div class="block">\n				<h4>What is iFLY</h4>\n				<nav class="footer-list">\n					<ul>\n						<li><a href="#">The Flying Experience</a></li>\n						<li><a href="#">VIDEO TESTIMONIALS </a></li>\n						<li><a href="#">RATINGS &amp; REVIEWS</a></li>\n					</ul>\n				</nav>\n			</div>\n			<div class="block">\n				<h4>Book Now</h4>\n				<nav class="footer-list">\n					<ul>\n						<li><a href="#">Book Your Flight</a></li>\n						<li><a href="#">Buy a Gift Card</a></li>\n						<li><a href="#">Redeem a Gift Card</a></li>\n						<li><a href="#">Buy Pics &amp; Vids</a></li>\n					</ul>\n				</nav>\n			</div>\n			<div class="block">\n				<h4>My iFLY</h4>\n				<nav class="footer-list">\n					<ul>\n						<li><a href="#">My Profile</a></li>\n						<li><a href="#">I.B.A Training Hours</a></li>\n					</ul>\n				</nav>\n			</div>\n			<div class="block">\n				<h4>Flight Info</h4>\n				<nav class="footer-list">\n					<ul>\n						<li><a href="#">Flyer Guide</a></li>\n						<li><a href="#">First-Time Flyer</a></li>\n						<li><a href="#">Return Flyer</a></li>\n						<li><a href="#">Experienced Flyer</a></li>\n						<li><a href="#">Group or Party</a></li>\n						<li><a href="#">iFLY Pics &amp; Vids</a></li>\n						<li><a href="#">Gift Cards</a></li>\n					</ul>\n				</nav>\n			</div>\n			\n			<div class="block last">\n				<h4>Legal</h4>\n				<nav class="footer-list">\n					<ul>\n						<li><a href="#">Tunnel Waiver</a></li>\n						<li><a href="#">Terms</a></li>\n						<li><a href="#">Privacy</a></li>\n					</ul>\n				</nav>\n			</div>\n		</div>\n		<p>Copyright &copy; 2012 SkyVenture. Vivamus adipiscing ultrices lacus, ut feugiat nunc adipiscing id. </p>\n	</footer>\n	', NULL, 1351045044, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(10, 1, 6, 'head', 'y', 'webpage', '<!DOCTYPE html>\n	<html>\n	<head>\n		<meta charset="utf-8">\n		<meta name="format-detection" content="telephone=no">\n		<meta name="viewport" content="width=device-width, initial-scale=1">\n		<title>IFly</title>\n		<link media="all" rel="stylesheet" type="text/css" href="/css/boilerplate.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/anythingslider.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/uniform.ifly.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/colorbox.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/calendar.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/all.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/pages.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/local-sites.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/photovid.css">\n		<link media="all" rel="stylesheet" type="text/css" href="/css/booking.css">\n		<script type="text/javascript" src="/js/libs/jquery-1.8.0.min.js" ></script>\n		<script type="text/javascript" src="/js/libs/modernizr-2.5.3-respond-1.1.0.min.js" ></script>\n		<!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="/css/ie.css" media="screen"/><![endif]-->\n\n\n	</head>', NULL, 1351045044, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(11, 1, 6, 'header', 'y', 'webpage', '		<header id="header">\n			<div class="logo"><a href="/home.php">I Fly indoor skydiving</a></div>\n			<div class="logo alt">I Fly indoor skydiving</div>\n			<div class="header-block">\n				<div class="header-top">\n					<strong class="phone">1-855-and-iFLY</strong>\n					<nav class="add-nav">\n						<ul>\n							<li><a href="/book-now/">Book Now</a></li>\n							<li><a href="#">Waiver</a></li>\n							<li><a href="#">Contact Us</a></li>\n						</ul>\n					</nav>\n				</div><!-- /.header-top -->\n				<div class="header-holder">\n					<ul class="social">\n						<li><a class="facebook" href="#">facebook</a></li>\n						<li><a class="twitter" href="#">twitter</a></li>\n					</ul>\n					<form action="#" class="search-form">\n						<fieldset>\n							<input class="text" type="text" placeholder="SEARCH" />\n							<input class="btn-search" type="submit" value="go" />\n						</fieldset>\n					</form>\n				</div>\n				<nav id="nav">\n					<ul>\n						<li id="what">\n							<a href="#"><span class="ir">What is iFly?</span></a>\n							<div class="drop">\n								<ul>\n									<li><a href="#">The Flying Experience</a></li>\n									<li><a href="#">VIDEO TESTIMONIALS </a></li>\n									<li><a href="#">RATINGS &amp; REVIEWS</a></li>\n									<li><a href="#">FAQs</a></li>\n									<li><a href="#">The Sport of Flying</a></li>\n									<li><a href="#">About Us</a></li>\n								</ul>\n							</div>\n						</li>\n						<li id="shop">\n							<a href="/book-now/"><span class="ir">BOOK NOW</span></a>\n							<div class="drop">\n								<ul>\n									<li><a href="#">Book Your Flight</a></li>\n									<li><a href="#">Buy a Gift Card</a></li>\n									<li><a href="#">Redeem a Gift Card</a></li>\n									<li><a href="#">Buy Pics &amp; Vids</a></li>\n								</ul>\n							</div>\n						</li>\n						<li id="media">\n							<a href="#"><span class="ir">FLIGHT INFO</span></a>\n							<div class="drop">\n								<ul>\n									<li><a href="#">Flyer Guide</a></li>\n									<li><a href="#">First-Time Flyer</a></li>\n									<li><a href="#">Return Flyer</a></li>\n									<li><a href="#">Experienced Flyer</a></li>\n									<li><a href="#">Group or Party</a></li>\n									<li><a href="#">iFLY Pics &amp; Vids</a></li>\n									<li><a href="#">Gift Cards</a></li>\n								</ul>\n							</div>\n						</li>\n						<li id="find">\n							<a href="#"><span class="ir">>FIND A LOCATION</span></a>\n						</li>\n					</ul>\n				</nav>\n			</div><!-- /.header-block -->\n		</header>', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 1),
(12, 1, 6, 'modals', 'y', 'webpage', '<div class="modal lp">\n	<div id="large-party" class="overlay" >\n		<h2>Information for large groups</h2>\n		<p>This is some information informing customer that currently system can only handle up to 12 people.</p>\n		<form id="lp" name="" method="get" class="validate">\n			<fieldset>\n				<div class="clearfix">\n					<label for="name">Name</label>\n					<input type="text" name="name" class="text required">\n				</div>\n				<div class="clearfix">\n					<label for="email">Email</label>\n					<input type="text" name="email" class="text required email">\n				</div>\n				<div class="clearfix">\n					<label for="phone">Phone</label>\n					<input type="text" name="phone" class="text required number">\n				</div>\n				<div class="clearfix">\n					<label for="message">Message</label>\n					<textarea class="text"></textarea>\n				</div>\n				<div class="clearfix">\n					<input type="submit" value="submit" class="submit">\n				</div>\n			</fieldset>\n		</form>\n	</div>\n</div>\n\n<div class="modal legal">\n	<div class="overlay" >\n		<h2>Legal Discalaimer</h2>\n		<p>Lorem Ipsum</p>\n		\n	</div>\n</div>\n\n<div class="tooltip">\n	<div id="tooltip" class="overlay event">\n		<a class="closeButton" href="#">close</a>\n		<h2></h2>\n		<p></p>\n	</div>\n</div>', NULL, 1351045044, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(13, 1, 6, 'scripts', 'y', 'webpage', '		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>\n		<script type="text/javascript" src="/js/libs/swfobject.js"></script>\n		<script type="text/javascript" src="/js/libs/jquery.uniform.min.js"></script>\n		<script type="text/javascript" src="/js/libs/jquery.colorbox.js" ></script>\n		<script type="text/javascript" src="/js/jquery.main.js" ></script>\n		<script type="text/javascript" src="/js/plugins.js" ></script>\n		<script type="text/javascript" src="/js/script.js" ></script>', NULL, 1351045044, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(14, 1, 6, 'index', 'y', 'webpage', '', NULL, 1351045044, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(16, 1, 7, 'index', 'y', 'webpage', '{embed="inc/head"}\n<body>\n\n	<div id="skyline"></div>\n\n	\n	<div id="parallax-wrapper">\n		<div id="parallax">\n			<a href="http://www.adobe.com/go/getflash">\n            	<img src="http://www.adobe.com/img/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />\n        	</a>\n		</div>\n	</div>\n	<!-- /#parallax-wrapper-->\n\n	<div id="wrapper">\n\n		{embed="inc/header"}\n		\n		{embed="inc/hero"}\n\n	</div><!-- /#wrapper -->\n\n	<div id="main" role="main">\n\n		<div class="content-block box-section">\n\n			<div class="box-holder">\n\n				{embed="inc/flight-wizard"}\n\n				{embed="inc/reviews"}\n\n			</div><!-- /.box-holder -->\n\n		</div><!-- /.box-section -->\n\n		<div class="content-block metal-bg">\n\n			{embed="inc/tiles"}			\n\n			{embed="inc/gallery"}\n\n			<div class="btn-block">\n				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>\n			</div><!-- /.btn-block -->\n\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', '', 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 24),
(17, 1, 8, 'index', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<aside>\n						<div class="video-wrapper">\n							<a class="btn-prev" href="#">&lt;</a>\n							<a class="btn-next" href="#">&gt;</a>\n							<div class="video-player">\n								<div id="yt-player" data-ytid="yP9GXL68w2w"></div>\n							</div>\n						</div><!-- /.video-wrapper -->\n					</aside>\n\n					<article>\n						<h1>How does it work</h1>\n						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu libero non tortor euismod interdum. Maecenas vestibulum orci tincidunt purus placerat eget porta nulla posuere. Nullam molestie elementum dui eu volutpat. Integer tempus ultricies iaculis. Aliquam vestibulum egestas ultrices. Nullam iaculis, tortor in rutrum sagittis, justo neque lobortis velit, ut sagittis neque magna eu ligula. Nulla a ornare libero. Donec accumsan elit nec mauris tincidunt ac aliquam magna viverra. Donec non ipsum ut augue cursus scelerisque ac vel nisl. Integer eleifend odio suscipit massa mattis placerat. Cras nisi purus, egestas ac bibendum id, condimentum rutrum mi. In in quam urna, at sodales urna. Proin nec ligula non sapien ultricies vehicula. Nullam auctor varius urna non lobortis. Nunc eu nisl vitae dui pretium interdum. Curabitur a neque a odio vehicula venenatis.</p>\n						<p>Nulla mattis congue dolor, eu mattis nisl eleifend non. Phasellus volutpat cursus felis at vulputate. Duis orci libero, interdum eget varius fermentum, fermentum non libero. Proin sit amet est ipsum, vel dictum purus. Curabitur gravida commodo vulputate. Pellentesque vel velit vel magna ultricies faucibus ut quis lectus. Suspendisse in nunc turpis. Etiam tristique rutrum vestibulum. Etiam erat odio, porta in placerat nec, faucibus vitae lectus.</p>\n					</article>\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', '', 1351117916, 1, 'n', 0, '', 'n', 'n', 'o', 5),
(19, 1, 9, 'index', 'y', 'webpage', '{embed="inc/head"}\n<body class="info-page booking step1">\n	\n	{embed="inc/header"}\n\n	{embed="inc/progress-indicator"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<div class="continue">\n					<a href="#" class="btn"><em></em><span>Check gift card balance</span></a>\n				</div>\n\n				<h1 class="underline">Flyer Guide</h1>\n				<h2 class="note">Pick which type of flyer you are to get more info or to book a flight.</h2>\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>first time flyer</h2>\n							<p>Never flown at iFLY before…this is you. Choose from five different packages. Click on ''More info'' to see all the first-time flyer packages, or click on ''Book Now'' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Return Flyer</h2>\n							<p>If you''ve flown with us once before, congratulations! You''re a return flyer. Click on ''More info'' to see all the return flyer packages, or click on ''Book Now'' if you already know what you want.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Experienced Flyer</h2>\n							<p> You''re ''experienced'' if you’re an iFLY regular, a competitive skydiver, or a member of the military with skydiving training. Click ''More info'' to see them all, or ''Book Now'' of you know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Group</h2>\n							<p>If you''re looking to fly six or more people, click on ''More info'' to see all group flyer packages, or click on ''Book Now'' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->\n\n				<div class="divider"></div>\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Buy a Gift Card</h2>\n							<p>Can''t decide? Buy a gift card. You can choose a specific dollar amount or choose a package. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>SHOP NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/img/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Redeem a Gift Card</h2>\n							<p>Someone gave you a gift card? Lucky you! Click ''Redeem Now'' to book your flight, or ''Check gift card balance'' to see how much is on your card.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>REDEEM NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->	\n\n			</div><!-- /.content-wrapper -->\n\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modal"}\n	\n</body>\n</html>', '', 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 3),
(20, 1, 6, 'hero', 'y', 'webpage', '                <div class="visual-block">\n                        <div class="visual-holder">\n                                <ul id="slider">\n                                        <li>\n                                                <article class="video-block">\n                                                        <h2>EVER DREAM OF FLYING?</h2>\n                                                        <p>iFLY indoor skydiving makes the dream of flight a reality.</p>\n                                                        <p>Press play to fly.</p>\n                                                        <div class="video"><a href="#yP9GXL68w2w"><img src="/img/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>\n                                                </article>\n                                        </li>\n                                        <li>\n                                                <article class="video-block">\n                                                        <h2>First Time Flyers</h2>\n                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>\n                                                        <div class="video"><a href="#"><img src="/img/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>\n                                                </article>\n                                        </li>\n                                        <li>\n                                                <article class="video-block">\n                                                        <h2>Who Can Fly</h2>\n                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>\n                                                        <div class="video"><a href="#"><img src="/img/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>\n                                                </article>\n                                        </li>\n                                        <li>\n                                                <article class="video-block">\n                                                        <h2>Instructors</h2>\n                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>\n                                                        <div class="video"><a href="#"><img src="/img/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>\n                                                </article>\n                                        </li>\n                                        <li>\n                                                <article class="video-block">\n                                                        <h2>I.B.A. Moves</h2>\n                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>\n                                                        <div class="video"><a href="#"><img src="/img/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>\n                                                </article>\n                                        </li>\n                                </ul><!-- /#slider -->\n\n				<div id="video-modal">\n					<div class="video-wrapper">\n						<a href="#" class="btn red" title="close" >\n						<em></em>\n						<span>close</span>\n						</a>\n						<!-- <a class="btn-prev" href="#">&lt;</a>\n						<a class="btn-next" href="#">&gt;</a> -->\n						<div class="video-player">\n							<div id="yt-player"></div>\n						</div>\n					</div><!-- /.video-wrapper -->\n				</div><!-- /#video-modal -->\n\n				<div class="menu-block local">\n					<div id="city-scape"></div>\n					<nav class="menu">\n						<ul>\n							<li class="active"><a href="#1">Lorem ipsum DOLOR</a></li>\n							<li><a href="#2">Lorem ipsum DOLOR</a></li>\n							<li><a href="#3">Lorem ipsum DOLOR</a></li>\n							<li><a href="#4">Lorem ipsum DOLOR</a></li>\n							<li><a href="#5">Lorem ipsum DOLOR</a></li>\n						</ul>\n					</nav>\n				</div>\n\n				<div class="menu-block global">\n					<nav class="menu">\n						<ul>\n							<li class="active"><a href="#1">What Is It?</a></li>\n							<li><a href="#2">First Time Flyers</a></li>\n							<li><a href="#3">Who Can Fly</a></li>\n							<li><a href="#4">Instructors</a></li>\n							<li><a href="#5">I.B.A. Moves</a></li>\n						</ul>\n					</nav>\n				</div>\n\n			</div>\n		\n		</div><!-- /.visual-block -->', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(21, 1, 6, 'flight-wizard', 'y', 'webpage', '				<article class="box">\n\n					<a href="#" class="photo"><img src="/img/img03.jpg" width="185" height="158" alt="image description"></a>\n					<div class="description">\n                    	<h1>Flight Wizard</h1>\n						<p>When do you want to fly? Find your flight packages and times.</p>\n						<form action="#" class="uniform quickbook">\n							<fieldset>\n                                <div class="row">\n                                {embed="form-elements/select-tunnel-locations"}\n                                </div>\n								<div class="row">\n									<a href="#" class="btn date" id="book-date" title="What day for the reservation" ><em></em><span>FLIGHT DATE</span></a>\n                                    <div id="calendar-quickbook"></div>\n									{embed="form-elements/select-how-many-flyers"}\n								</div>\n								<div class="row">\n									{embed="form-elements/select-flyer-type"}\n								</div>\n								<div class="row">\n									<input type="submit" value="Find Flights" class="find-flights">\n									<a href="#" class="btn"><em></em><span>MORE INFO</span></a>\n								</div>\n							</fieldset>\n						</form>\n					</div>\n				</article><!-- /.box -->', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(22, 1, 6, 'reviews', 'y', 'webpage', '				<article class="box alt">\n					<a href="#" class="photo"><img src="/img/img04.jpg" width="184" height="158" alt="image description"></a>\n					<div class="description">\n						<h1>Flyer Feedback</h1>\n						<blockquote>\n							<q>Best birthday ever! We were actually flying! I can’t wait...</q>\n							<a href="#" class="more">READ MORE</a>\n							<cite>-John Smith, 7/19/12</cite>\n						</blockquote>\n						<div class="rating">\n							<span class="star-rating star1">X-star review</span>\n						</div>\n						<a href="#" class="btn red"><em></em><span>MORE REVIEWS</span></a>\n					</div>\n				</article><!-- /.box -->', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(23, 1, 6, 'tiles', 'y', 'webpage', '			<section class="block-section">\n				<article class="block">\n					<div class="block-holder">\n						<img src="/img/img05.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>Book now and get more fly time</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>1 day special offer</span></a>\n					</div>\n				</article>\n				<article class="block">\n					<div class="block-holder">\n						<img src="/img/img06.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>FIND US ON FACEBOOK</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>ifly on FACEBOOK</span></a>\n					</div>\n				</article>\n				<article class="block">\n					<div class="block-holder">\n						<img src="/img/img07.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>LOREM IPSUM DOLOR SIT AMIT.</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>LEARN MORE</span></a>\n					</div>\n				</article>\n			</section><!-- /.block-section -->', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(24, 1, 6, 'gallery', 'y', 'webpage', '			<section class="gallery-block">\n				<h1>OUR FAVORITE iFLY PHOTOS &amp; VIDEOS</h1>\n				<div class="gallery">\n					<div class="gholder">\n						<a class="btn-prev" href="#">&lt;</a>\n						<a class="btn-next" href="#">&gt;</a>\n						<div class="gmask">\n							<ul>\n								<li><a href="/img/img08.jpg"><img src="/img/img08.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img09.jpg"><img src="/img/img09.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img10.jpg"><img src="/img/img10.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img11.jpg"><img src="/img/img11.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img12.jpg"><img src="/img/img12.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img08.jpg"><img src="/img/img08.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img09.jpg"><img src="/img/img09.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img10.jpg"><img src="/img/img10.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img11.jpg"><img src="/img/img11.jpg" width="174" height="105" alt="image description"></a></li>\n								<li><a href="/img/img12.jpg"><img src="/img/img12.jpg" width="174" height="105" alt="image description"></a></li>\n							</ul>\n						</div>\n					</div>\n				</div>\n			</section><!-- /.gallery-block -->', NULL, 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(25, 1, 10, 'select-tunnel-locations', 'y', 'webpage', '								<select name="TUNNEL LOCATION" title="Select your tunnel location" id="flyer-location" class="locations">\n                                    <option>Hollywood, CA</option>\n                                    <option>Orlando, FL</option>\n                                    <option>Seattle, WA</option>\n                                    <option>San Francisco, CA</option>\n                                    <option>Find the closest location</option>\n                                    <option selected="selected">FLIGHT LOCATION</option>\n                                </select>', NULL, 1351104624, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(26, 1, 10, 'select-how-many-flyers', 'y', 'webpage', '									<select class="flyers" title="The Number of flyers for the reservation" name="# OF FLYERS" id="how-many-fliers">\n                                        <option>1</option>\n										<option>2</option>\n										<option>3</option>\n										<option>4</option>\n										<option>5</option>\n										<option>6</option>\n										<option>7</option>\n										<option>8</option>\n										<option>9</option>\n										<option>10</option>\n										<option>11</option>\n										<option>12</option>\n										<option value="13">13+</option>\n										<option selected="selected"># OF FLYERS</option>\n									</select>', NULL, 1351104624, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(27, 1, 10, 'select-flyer-type', 'y', 'webpage', '									<select name="TYPE OF FLYER" title="The type of flyer making the reservation" id="flyer-type">\n										<option>FIRST TIME FLYER</option>\n										<option>RETURN FLYER</option>\n										<option>EXPERIENCED FLYER</option>\n										<option>GROUP</option>\n										<option selected="selected">FLYER TYPE</option>\n									</select>', NULL, 1351104624, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(28, 1, 6, 'progress-indicator', 'y', 'webpage', '<div id="progress-indicator">\n		<ul>\n			<li id="step1">flyer guide</li>\n			<li id="step2">booking details</li>\n			<li id="step3">choose a package</li>\n			<li id="step4">choose a time</li>\n			<li id="step5">cart</li>\n			<li id="step6">checkout</li>\n			<li id="step7">confirmation</li>\n		</ul>\n	</div>', NULL, 1351105139, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(29, 1, 10, 'index', 'y', 'webpage', 'index.html', '', 1351107438, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(30, 1, 6, 'progress-indicator-gifting-redeem', 'y', 'webpage', '<div id="progress-indicator">\n		<ul>\n			<li id="step1">enter redeem code</li>\n			<li id="step2">booking details</li>\n			<li id="step3">choose a package</li>\n			<li id="step4">choose a time</li>\n			<li id="step6">checkout</li>\n			<li id="step7">confirmation</li>\n		</ul>\n	</div>', NULL, 1351106571, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(31, 1, 6, 'progress-indicator-gifting', 'y', 'webpage', '	<div id="progress-indicator" class="gifting">\n		<ul>\n			<li id="step1">begin</li>\n			<li id="step2">details</li>\n			<li id="step3">buy a gift card</li>\n			<li id="step4">checkout</li>\n			<li id="step5">confirmation</li>\n		</ul>\n	</div>', NULL, 1351106571, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(32, 1, 3, 'booking', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page booking one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<article>\n						{exp:channel:entries channel="booking" limit="1" disable="categories|category_fields|member_data|pagination"} \n							<h1>{title}</h1>\n							<h2 class="note">{sub_heading}</h2>\n							{page_copy}\n						{/exp:channel:entries}\n						\n					</article>\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', '', 1351106815, 1, 'n', 0, '', 'n', 'n', 'o', 3),
(33, 1, 9, 'template', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page booking one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<article>\n						{exp:channel:entries channel="booking" limit="1" disable="categories|category_fields|member_data|pagination"} \n							<h1>{title}</h1>\n							<h2 class="note">{sub_heading}</h2>\n							{page_copy}\n						{/exp:channel:entries}\n						\n					</article>\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351107370, 1, 'n', 0, '', 'n', 'n', 'o', 2),
(34, 1, 8, 'template-copy', 'y', 'webpage', '', '', 1351117871, 1, 'n', 0, '', 'n', 'n', 'o', 30),
(35, 1, 8, 'template-video', 'y', 'webpage', '', '', 1351117882, 1, 'n', 0, '', 'n', 'n', 'o', 34),
(36, 1, 8, 'template-image', 'y', 'webpage', '', '', 1351117876, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(37, 1, 3, 'one-column', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<article>\n\n						{exp:channel:entries channel="template_one" limit="1" disable="categories|category_fields|member_data|pagination"}	\n\n							{if info_image}\n							<aside>\n								<img src="{info_image}" alt="{title}">\n							</aside>	\n\n							{if:elseif info_video}\n							<aside>\n								<div class="video-wrapper">\n									{exp:channel:entries channel="template_one"}\n									    {info_video}\n									{/exp:channel:entries}							\n								</div>\n								<!-- /.video-wrapper -->\n							</aside>\n							{/if}\n							<h1>{title}</h1>\n							{info_copy}\n						{/exp:channel:entries}\n					</article>\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351118753, 1, 'n', 0, '', 'n', 'n', 'o', 4),
(38, 1, 6, 'nav', 'y', 'webpage', '{exp:navee:nav nav_title="global_nav_header"}', NULL, 1351122341, 1, 'n', 0, '', 'n', 'n', 'o', 43),
(39, 1, 8, 'ratings-and-reviews', 'y', 'webpage', '{embed="inc/head"}\n<body class="info-page one-col ratings">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					<h1 class="uppercase">Ratings &amp; Reviews</h1>\n					<div class="note">\n						<h2>Read what our customers have to say about their experience at iFLY,<br />or write a review yourself.</h2>\n					</div>\n					\n\n					<div id="ratings-dist">\n						<span class="star-rating star1">X-star review</span> <span class="average">4.7 out of 5</span>\n						<h2>Rating Distribution</h2>\n						<div class="progress-bar">\n							5 stars <span><em style="width : 80px"></em></span> 211\n						</div>\n						<div class="progress-bar">\n							4 stars <span><em style="width : 9px"></em></span> 39\n						</div>\n						<div class="progress-bar">\n							3 stars <span><em style="width : 3px"></em></span> 6\n						</div>\n						<div class="progress-bar">\n							2 stars <span><em style="width : 3px"></em></span> 4\n						</div>\n						<div class="progress-bar">\n							1 stars <span><em style="width : 3px"></em></span> 2\n						</div>\n						<p>265 out of 271 would recommend iFLY to a friend!</p>\n\n					</div>\n\n					<div id="sort-pagination" class="clearfix">\n						<div class="pagination">\n							<div>\n								<a href="#" class="prev ir" title="Previous Page">&lt;</a>\n							</div>\n							<div class="pages">\n								<span class="active">1</span> of <span class="total">4</span>\n							</div>\n							<div>\n								<a href="#" class="next ir" title="Next Page">&gt;</a>\n							</div>\n						</div>\n\n						<div class="sort">\n							<span>Sort by:</span>\n							<form class="uniform">\n								<select id="sort-recent">\n									<option>MOST RECENT</option>\n								</select>\n								<select id="sort-all">\n									<option>ALL</option>\n								</select>\n							</form>\n						</div><!-- /.sort -->\n\n					</div><!-- /#sort-pagination -->\n			\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<div class="review clearfix">\n\n						<div class="info">\n							<span class="star-rating star1">X-star review</span>\n							<span>Oct 14, 2012</span>\n							<em>by Lisa T.</em>\n							<span>Honolulu HI</span>\n						</div>\n\n						<div class="comment">\n							<h2>What a blast!</h2>\n							<p>So much fun! John was a great instructor! A lot of fun &amp; also able to give us clear direction. What a blast!</p>\n						</div>\n\n					</div><!-- /.review -->\n\n					<p><a href="#" class="btn"><em></em><span>MORE</span></a></p>\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351126197, 1, 'n', 0, '', 'n', 'n', 'o', 2),
(40, 1, 8, 'gift-cards', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					{exp:channel:entries channel="template_one" limit="1" disable="categories|category_fields|member_data|pagination"}	\n					\n					<article>\n						<h1>{title}</h1>\n						{info_copy}\n						{info_form}\n					</article>\n\n					{/exp:channel:entries}\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351292054, 1, 'n', 0, '', 'n', 'n', 'o', 0),
(41, 1, 11, 'gift-cards', 'y', 'webpage', '{embed="inc/head"}\n\n<body class="info-page one-col">\n	\n	{embed="inc/header"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<section class=" clearfix">\n\n					{exp:channel:entries channel="template_one" limit="1" disable="categories|category_fields|member_data|pagination"}	\n					\n					<article>\n						<h1>{title}</h1>\n						{info_copy}\n						{info_form}\n					</article>\n\n					{/exp:channel:entries}\n\n				</section>\n\n			</div><!-- /.\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351292269, 1, 'n', 0, '', 'n', 'n', 'o', 6),
(42, 1, 11, 'index', 'y', 'webpage', '{embed="inc/head"}\n<body>\n\n	<div id="skyline"></div>\n\n	\n	<div id="parallax-wrapper">\n		<div id="parallax">\n			<a href="http://www.adobe.com/go/getflash">\n            	<img src="http://www.adobe.com/img/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />\n        	</a>\n		</div>\n	</div>\n	<!-- /#parallax-wrapper-->\n\n	<div id="wrapper">\n\n		{embed="inc/header"}\n		\n		{embed="inc/hero"}\n\n	</div><!-- /#wrapper -->\n\n	<div id="main" role="main">\n\n		<div class="content-block box-section">\n\n			<div class="box-holder">\n\n				{embed="inc/flight-wizard"}\n\n				{embed="inc/reviews"}\n\n			</div><!-- /.box-holder -->\n\n		</div><!-- /.box-section -->\n\n		<div class="content-block metal-bg">\n\n			{embed="inc/tiles"}			\n\n			{embed="inc/gallery"}\n\n			<div class="btn-block">\n				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>\n			</div><!-- /.btn-block -->\n\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	{embed="inc/modals"}\n	\n</body>\n</html>', NULL, 1351292269, 1, 'n', 0, '', 'n', 'n', 'o', 2);
INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(43, 1, 11, 'flyer-guide', 'y', 'webpage', '{embed="inc/head"}\n<body class="info-page booking step1">\n	\n	{embed="inc/header"}\n\n	{embed="inc/progress-indicator"}\n\n	<div id="main" role="main">\n\n		<div class="content-block">\n\n			<div class="content-wrapper rounded-corners gradient-border clearfix">\n\n				<div class="continue">\n					<a href="#" class="btn"><em></em><span>Check gift card balance</span></a>\n				</div>\n\n				<h1 class="underline">Flyer Guide</h1>\n				<h2 class="note">Pick which type of flyer you are to get more info or to book a flight.</h2>\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>first time flyer</h2>\n							<p>Never flown at iFLY before…this is you. Choose from five different packages. Click on ''More info'' to see all the first-time flyer packages, or click on ''Book Now'' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Return Flyer</h2>\n							<p>If you''ve flown with us once before, congratulations! You''re a return flyer. Click on ''More info'' to see all the return flyer packages, or click on ''Book Now'' if you already know what you want.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Experienced Flyer</h2>\n							<p> You''re ''experienced'' if you’re an iFLY regular, a competitive skydiver, or a member of the military with skydiving training. Click ''More info'' to see them all, or ''Book Now'' of you know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Group</h2>\n							<p>If you''re looking to fly six or more people, click on ''More info'' to see all group flyer packages, or click on ''Book Now'' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->\n\n				<div class="divider"></div>\n\n				<div class="clearfix">\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Buy a Gift Card</h2>\n							<p>Can''t decide? Buy a gift card. You can choose a specific dollar amount or choose a package. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>SHOP NOW</span></a>\n						</div>\n						\n					</article>\n\n					<article class="flier-type">\n						<aside>\n							<img src="/images/temp-booking-step1.jpg" alt="alt">\n						</aside>\n						<div>\n							<h2>Redeem a Gift Card</h2>\n							<p>Someone gave you a gift card? Lucky you! Click ''Redeem Now'' to book your flight, or ''Check gift card balance'' to see how much is on your card.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>\n							<a href="#" class="btn green"><em></em><span>REDEEM NOW</span></a>\n						</div>\n						\n					</article>\n\n				</div><!-- /.clearfix -->	\n\n			</div><!-- /.content-wrapper -->\n\n		</div><!-- /.content-block -->\n\n	</div><!-- /#main -->\n\n	{embed="inc/footer"}\n	{embed="inc/scripts"}\n	\n</body>\n</html>', NULL, 1351294191, 1, 'n', 0, '', 'n', 'n', 'o', 9);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(2, 3, 'home', 1, 'y'),
(3, 1, 'live-look', 3, 'n'),
(6, 1, 'inc', 6, 'n'),
(7, 1, 'home', 4, 'y'),
(8, 1, 'what-is-ifly', 5, 'n'),
(9, 1, 'book-now', 6, 'n'),
(10, 1, 'form-elements', 7, 'n'),
(11, 1, 'flight-info', 8, 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_template_member_groups`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_template_no_access`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `exp_throttle`
--


-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_upload_no_access`
--

INSERT INTO `exp_upload_no_access` (`upload_id`, `upload_loc`, `member_group`) VALUES
(1, 'cp', 7),
(2, 'cp', 6),
(3, 'cp', 6),
(3, 'cp', 7);

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'General Uploads', '/Applications/XAMPP/xamppfiles/htdocs/coloringbook/ifly/dev/uploads/', 'http://ifly.dev/images/uploads/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(2, 3, 'General Uploads', 'C:\\inetpub\\wwwroot\\deviflyworld\\images\\uploads\\', 'http://dev.iflyworld.com/images/uploads/', 'img', '', '', '', '', '', '', '', '', '', '', NULL),
(3, 1, 'Gallery', '/Applications/XAMPP/xamppfiles/htdocs/coloringbook/ifly/dev/uploads/gallery/', 'http://ifly.dev/', 'img', '', '500', '840', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_videoplayer_accounts`
--

CREATE TABLE IF NOT EXISTS `exp_videoplayer_accounts` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) unsigned DEFAULT NULL,
  `order` int(3) unsigned DEFAULT NULL,
  `service` varchar(30) DEFAULT '',
  `api_key` varchar(200) DEFAULT '',
  `api_secret` varchar(200) DEFAULT '',
  `enabled` int(1) unsigned DEFAULT NULL,
  `is_authenticated` int(1) unsigned DEFAULT NULL,
  `authsub_session_token` varchar(50) DEFAULT '',
  `oauth_access_token` varchar(50) DEFAULT '',
  `oauth_access_token_secret` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_videoplayer_accounts`
--

INSERT INTO `exp_videoplayer_accounts` (`id`, `site_id`, `order`, `service`, `api_key`, `api_secret`, `enabled`, `is_authenticated`, `authsub_session_token`, `oauth_access_token`, `oauth_access_token_secret`) VALUES
(1, 1, 0, 'vimeo', '31cecf9c99f81e6de7556143cfc5016840a00126', '20db1dff367814a10dc801e1982ddc1536116c92', 1, 1, '', '9592f8154b39391cc75112c93c3912b3', '6b077acdf4dde1d7aeeb0c3510d5a903c756bdcc');
