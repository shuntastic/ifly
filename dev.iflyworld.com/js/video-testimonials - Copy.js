var pages = 1;
var vidID;
var videoID;
var VT_MAX_RESULTS_PER_PAGE = 25;

 IFLY.videotestimonials = {
	 thumbsPerPage: 25,
	 currentPage: 1,
	 currentCount: 0,
	 basePath: 'http://dev.iflyworld.com/php-lib/videos-by-tags.php',
	 // http://dev.iflyworld.com/php-lib/videos-by-tags.php?tunnel=aus&playlist=2&page=2&debug=1
	init: function() {
        //console.log('current host:' + host);
        var tunnel = null;
        var default_tunnel = 'ifo';
        
        var maxResults = VT_MAX_RESULTS_PER_PAGE;
        var resultCount = 0;
        var page = 1;
		$('#search_results').masonry({
			itemSelector : '.item'
		});

		var params = vt_getUrlVars();
		vidID = params['videoID'];
		tunnel = params['tunnel'];
		if (tunnel != undefined) {
			$("select.locations").val(tunnel).change();
		} else {
			tunnel = $("select.locations").val();
		}
		// if the user changes tunnels, stop videos and get results for that tunnel...
		$("select.locations").change(function(e) {
			tunnel = $("select.locations").val();
			// stop the video player if it was running
			player = document.getElementById("player");	
			if (player != undefined && isObject("player")) {
				player.stopVideo();
			}
			IFLY.videotestimonials.loadResultsPage(tunnel, IFLY.videotestimonials.currentPage);
		});
		IFLY.videotestimonials.loadResultsPage(tunnel, IFLY.videotestimonials.currentPage);
		if(vidID != undefined) {
			IFLY.videotestimonials.displayTestimonialsOverlay(vidID);
		}
		
	},
	loadResultsPage: function(tunnel, page) {
	 //   console.log('loadResultsPage called with maxResults:' + maxResults + ' and page:' + page);
		$('#search_results').after(IFLY.showRevLoader());
	
		$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: 1, tunnel: tunnel},  function(output) {
			// data contains a count and the array of results...
			var response = $.parseJSON(output);
			$('#search_results').html('');
			IFLY.videotestimonials.currentCount = response.count;
			console.log('loadResultsPage:',response);
			IFLY.videotestimonials.initPagination(response.media,'#search_results');
		});
	},	
	addResults: function(tunnel, page) {
		$('#search_results').after(IFLY.showRevLoader());
		if (IFLY.videotestimonials.currentPage*VT_MAX_RESULTS_PER_PAGE < IFLY.videotestimonials.currentCount) {
			$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: page,tunnel: tunnel},  function(output) {
				//console.log('addResults response:' + output);
				var response = $.parseJSON(output);
				IFLY.videotestimonials.displayThumbs('#search_results',response.media);
			});
		} else {
			IFLY.videotestimonials.currentPage = 1;
			 $('.footer-nav a.btn.more').css('display','none');
			IFLY.killLoader();
		}
	},	
	initPagination: function(data,tDiv) {
			IFLY.photovid.clearItems(tDiv);
			//console.log('initPagination',data);
				
			IFLY.photovid.totalThumbs = data.length;
			IFLY.photovid.lastThumb = 0;
			IFLY.videotestimonials.displayThumbs(tDiv,data);
			
			$('.footer-nav a.more').unbind('click').click(function(e) {
				addMore(tDiv);
				e.preventDefault();
			});

			if (IFLY.videotestimonials.currentPage*VT_MAX_RESULTS_PER_PAGE < IFLY.videotestimonials.currentCount) {
				$('.footer-nav a.more').css('display','inline-block');	
			} else {
				$('.footer-nav a.more').css('display','none'); 
			}

//			$(window).scroll(function() {
//			   if($(window).scrollTop() + $(window).height() == $(document).height()) {
//					addMore(tDiv);
//			   }
//			});
		
			function addMore(disDiv){
				IFLY.videotestimonials.currentPage +=1;
				IFLY.videotestimonials.addResults($("select.locations").val(), IFLY.videotestimonials.currentPage);
				//IFLY.videotestimonials.displayThumbs(disDiv,thumbSet);
				if (IFLY.videotestimonials.currentPage*VT_MAX_RESULTS_PER_PAGE < IFLY.videotestimonials.currentCount) {
					$('.footer-nav a.more').css('display','inline-block');	
				} else {
					$('.footer-nav a.more').css('display','none'); 
				}
			} 
				//addMore(tDiv);
				
	},
	displayThumbs: function(tDiv, data) {
		//$(tDiv).html('');
		var thumbs = '';
		//console.log('displayThumbs', data)
        $.each(data, function(key, value) {  
            if(typeof value.videoThumb != 'undefined'){
                videoID = value.videoID;
                vidID = videoID;
                title = value.videoTitle;  
                title = title.split('\'s Flight Video ');
				var date = title[1];
				var name = title[0].split(' ');
				name = name[0];
//                title = title.replace(/ Flight Video /,'');
//                title = title.replace(/\'s/,'').replace(/ January/,'<br />January').replace(/ February/,'<br />February').replace(/ March/,'<br />March').replace(/ April/,'<br />April').replace(/ May/,'<br />May').replace(/ June/,'<br />June').replace(/ July/,'<br />July').replace(/ August/,'<br />August').replace(/ September/,'<br />September').replace(/ October/,'<br />October').replace(/ November/,'<br />November').replace(/ December/,'<br />December');
				var textOverlay = name+'<br />'+date;
                url = value.videoURL; 
                description = value.videoDescription;
                thumb = value.videoThumb;
                if (title != "iFLY Austin August") {	
					thumbs+='<div class="item"><span>'+textOverlay+'</span><a data-vid="'+videoID+'" href="#"><img src="'+thumb+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';	
                }
            }
        });
        $(tDiv).fadeIn(500);
		$(tDiv).imagesLoaded(function(){
			var $thumbs = $(thumbs);
			$(tDiv).append($thumbs).masonry('appended', $thumbs).masonry( 'reload' );

			//PHOTO CART OVERLAY
			$('.item a').each(function(index,value){
				$(this).unbind('click').click(function(e) {
					e.preventDefault();
					var vid = $(this).data('vid');
					IFLY.videotestimonials.displayTestimonialsOverlay(vid);
				});
			});
			IFLY.killLoader();
		});
	},
	displayTestimonialsOverlay: function(id) {
		videoID = id;
		//console.log('clicked on setVideo for id:' + id);
	   var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="http://www.iflyworld.com/video-reviews/index.php?videoID='+id+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-width="90" data-show-faces="false"><iframe src="http://www.facebook.com/plugins/like.php?api_key=409878702400471&amp;locale=en_US&amp;sdk=joey&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D17%23cb%3Df39bc13f6085e52%26origin%3Dhttp%253A%252F%252Fwww.iflyworld.com%252Ff1a07571aee4992%26domain%3Dwww.iflyworld.com%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fwww.iflyworld.com%2Fvideo-reviews%2Findex.php%3FvideoID%3DOPYiZJ5bIc4&amp;node_type=link&amp;width=150&amp;layout=button_count&amp;colorscheme=light&amp;show_faces=false&amp;send=true&amp;extended_social_context=false" class="fb_ltr" title="Like this content on Facebook." name="f3cfbf57a6a4502" id="f18eeda2c1537d8" scrolling="no"></iframe></div><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a></div>';
	
		var overlayHTML = '<div id="like_screen"></div><div id="vHolder" class="gradient-border drop-shadow rounded-corners"><object id="ytplayer_ie"  width="840" height="500">\
							<param name="allowscriptaccess" value="always" />\
							<param name="src" value="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer_ie&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" />\
							<param name="allowfullscreen" value="true" />\
							<embed id="ytplayer" width="840" height="500" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer&hl=en_US&autoplay=1&autohide=1&rel=0&wmode=transparent&theme=light&color=red&hd=1&modestbranding=1" allowFullScreen="true" allowscriptaccess="always" allowfullscreen="true" /></object></div>'+shareString;
			//console.log('displayVideoCartOverlay overlayHTML:', overlayHTML);
		$.colorbox({
			width:900,
			height: 700,
			html:overlayHTML,
			onComplete:function() {
				//var getPlayer = document.getElementById('ytplayer');
				//ytplayer.addEventListener("onStateChange", "IFLY.videotestimonials.showEndFrameCTA");
				$('.fb-like').click(function() {
					IFLY.analytics.sendSocial('Facebook', 'Like', 'Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
				});
				$('a#vt_twitter').click(function(e) {
					e.preventDefault();
					sendTwitter(id);
					IFLY.analytics.sendSocial('Twitter','Share','Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
				});
			},
			onClose: function() {
				IFLY.youtube.destroy("ytplayer");
			}
		});
	},
	showEndFrameCTA: function(newState) {
		if(newState == 0) {
			 $('div#like_screen').fadeIn(500);	
		}
	}

}
  
function chooseDefaultTunnel(default_tunnel) {
    $("select.locations").val(default_tunnel).change();
    var selected_tunnel = $("select.locations").val();
    console.log('forced tunnel to:' + selected_tunnel);
    tunnel = selected_tunnel;
    
    return default_tunnel;
}  


function hideSocial() {
    //console.log('HIDESOCIAL CALLED');
    $('#vt_social').css('left','-3000px');
    //$('#vt_social').hide();
}  

function showSocial() {
    console.log('SHOWSOCIAL CALLED');
    $('#vt_social').css('left','0px');
    //$('#vt_social').show();
}
  
// default video (joe jennings) -- deprecated
function gup( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    // explode the URL
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "7G-QD2RO28c";
    else
        return results[1];
}
//IFLY.photovid.displayTestimonialsOverlay =  function(id) {
//	videoID = id;
//	//console.log('clicked on setVideo for id:' + id);
//   var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="http://www.iflyworld.com/video-reviews/index.php?videoID='+id+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-width="90" data-show-faces="false"><iframe src="http://www.facebook.com/plugins/like.php?api_key=409878702400471&amp;locale=en_US&amp;sdk=joey&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D17%23cb%3Df39bc13f6085e52%26origin%3Dhttp%253A%252F%252Fwww.iflyworld.com%252Ff1a07571aee4992%26domain%3Dwww.iflyworld.com%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fwww.iflyworld.com%2Fvideo-reviews%2Findex.php%3FvideoID%3DOPYiZJ5bIc4&amp;node_type=link&amp;width=150&amp;layout=button_count&amp;colorscheme=light&amp;show_faces=false&amp;send=true&amp;extended_social_context=false" class="fb_ltr" title="Like this content on Facebook." name="f3cfbf57a6a4502" id="f18eeda2c1537d8" scrolling="no"></iframe></div><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a></div>';
//
//	var overlayHTML = '<div id="like_screen"></div><div id="vHolder" class="gradient-border drop-shadow rounded-corners"><object id="ytplayer_ie"  width="840" height="500">\
//						<param name="allowscriptaccess" value="always" />\
//						<param name="src" value="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer_ie&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" />\
//						<param name="allowfullscreen" value="true" />\
//						<embed id="ytplayer" width="840" height="500" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer&hl=en_US&autoplay=1&autohide=1&rel=0&wmode=transparent&theme=light&color=red&hd=1&modestbranding=1" allowFullScreen="true" allowscriptaccess="always" allowfullscreen="true" /></object></div>'+shareString;
//		//console.log('displayVideoCartOverlay overlayHTML:', overlayHTML);
//	$.colorbox({
//		width:900,
//		height: 700,
//		html:overlayHTML,
//		onComplete:function() {
//			var getPlayer = document.getElementById('ytplayer');
//			ytplayer.addEventListener("onStateChange", "IFLY.photovid.showEndFrameCTA");
//			$('.fb-like').click(function() {
//				IFLY.analytics.sendSocial('Facebook', 'Like', 'Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
//			});
//			$('a#vt_twitter').click(function(e) {
//				e.preventDefault();
//				sendTwitter(id);
//				IFLY.analytics.sendSocial('Twitter','Share','Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
//			});
//		},
//		onClose: function() {
//			IFLY.youtube.destroy("ytplayer");
//		}
//	});
//}
//IFLY.photovid.showEndFrameCTA = function(newState) {
//	console.log('FIRED');
//			if(newState == 0) {
//				 $('div#like_screen').fadeIn(500);	
//			}
//}
	          
// load/que a video
function setVideo(id){
    console.log('clicked on setVideo for id:' + id);
    //$('#page_list').hide();
    $('#close_button_div').show();
    $('#page_list').hide();
    videoID = id;
    //Check for existing SWF
    if(isObject("player")){
        //replace object/element with a new div					 
        replaceSwfWithEmptyDiv("player");
    }

    $('#search_results_list_column').hide();				   
    $('#video_player_wrapper').show();				   
    showSocial();

    $('#like_screen').hide();

    var params = {
        allowScriptAccess: "always"
    };
    var atts = {
        id: "player"
    };
    swfobject.embedSWF("http://www.youtube.com/v/" + id + "?enablejsapi=1&playerapiid=ytplayer&version=3&hl=en_US&theme=light&color=red&hd=1&rel=0&showinfo=0&autoplay=1&autohide=1&loop=0&modestbranding=1",
        "player", "840", "500", "8", null, null, params, atts);
}

			

//function onYouTubePlayerReady(playerId) {
//    console.log('onYouTubePlayerReady  - with playerId:' + playerId);
////    $('#like_screen').hide();
// //   $('#close_button_div').show();
//    player = document.getElementById(playerId);	
//    player.addEventListener("onStateChange", "onytplayerStateChange");
//}
//			
//function onytplayerStateChange(newState) {			   			   	 
//    console.log('*** onytplayerStateChange-newstate:' + newState);
//    if (newState == 0){
//        $('div#like_screen').fadeIn(500);
////        $('div#like_screen').css('display','block');
//    }
//    if (newState == 1 || newState == 3){
//		//IFLY.analytics.sendEvent('Video', 'Plays', 'Video ID#: '+ videoID,null);
//        $('div#like_screen').css('display','none');
//    }
//    
//}
			
function playVideo(){
    //$('#vt_social').show();
    showSocial();
    player.playVideo();
}
			
//Support function: checks to see if target
//element is an object or embed element
function isObject(targetID){
    var isFound = false;
    var el = document.getElementById(targetID);
    if(el && (el.nodeName === "OBJECT" || el.nodeName === "EMBED")){
        isFound = true;
    }
    return isFound;
}
			 
//Support function: creates an empty
//element to replace embedded SWF object
function replaceSwfWithEmptyDiv(targetID){
    var el = document.getElementById(targetID);
    if(el){
        var div = document.createElement("div");
        el.parentNode.insertBefore(div, el);
			 
        //Remove the SWF
        swfobject.removeSWF(targetID);
			 
        //Give the new DIV the old element's ID
        div.setAttribute("id", targetID);
    }
}
		
function sendFacebok(){
    window.open('https://www.facebook.com/sharer/sharer.php?t=Check my video&u=' + window.location + '')
}


function sendTwitter(videoID){
    window.open('http://twitter.com/home?status=Check out my video from iFly: ' + window.location + '?videoID=' + videoID)
}
  
function vt_getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(document).ready(function() {
	if($('body.video-testimonials').length >0) {
		IFLY.videotestimonials.init(); 
	}
});
