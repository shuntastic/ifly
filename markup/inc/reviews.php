				<article class="box alt">
					<a href="#" class="photo"><img src="images/img04.jpg" width="184" height="158" alt="image description"></a>
					<div class="description">
						<h1>Flyer Feedback</h1>
						<blockquote id="home-review">
							<q>Best birthday ever! We were actually flying! I can’t wait...</q>
							<a href="#" class="more">READ MORE</a>
							<cite>-John Smith, 7/19/12</cite>
						</blockquote>
						<div class="rating">
							<span class="star-rating star1">X-star review</span>
						</div>
						<a href="#" class="btn red"><em></em><span>MORE REVIEWS</span></a>
					</div>
				</article><!-- /.box -->