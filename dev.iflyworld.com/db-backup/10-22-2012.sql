-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 22, 2012 at 02:05 PM
-- Server version: 5.1.57-community
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iflyworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `exp_accessories`
--

CREATE TABLE IF NOT EXISTS `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_accessories`
--

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`) VALUES
(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0'),
(2, 'Cp_analytics_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '2.0.7'),
(3, 'Mx_extended_content_menu_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3'),
(4, 'Template_variables_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.2.1'),
(5, 'Nsm_morphine_theme_acc', '1|5|6', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '2.0.3');

-- --------------------------------------------------------

--
-- Table structure for table `exp_actions`
--

CREATE TABLE IF NOT EXISTS `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `exp_actions`
--

INSERT INTO `exp_actions` (`action_id`, `class`, `method`) VALUES
(1, 'Comment', 'insert_new_comment'),
(2, 'Comment_mcp', 'delete_comment_notification'),
(3, 'Comment', 'comment_subscribe'),
(4, 'Comment', 'edit_comment'),
(5, 'Email', 'send_email'),
(6, 'Safecracker', 'submit_entry'),
(7, 'Safecracker', 'combo_loader'),
(8, 'Search', 'do_search'),
(9, 'Channel', 'insert_new_entry'),
(10, 'Channel', 'filemanager_endpoint'),
(11, 'Channel', 'smiley_pop'),
(12, 'Member', 'registration_form'),
(13, 'Member', 'register_member'),
(14, 'Member', 'activate_member'),
(15, 'Member', 'member_login'),
(16, 'Member', 'member_logout'),
(17, 'Member', 'retrieve_password'),
(18, 'Member', 'reset_password'),
(19, 'Member', 'send_member_email'),
(20, 'Member', 'update_un_pw'),
(21, 'Member', 'member_search'),
(22, 'Member', 'member_delete'),
(23, 'Rte', 'get_js'),
(24, 'Navee', 'add_navigation_handler'),
(25, 'Playa_mcp', 'filter_entries'),
(26, 'Videoplayer', 'ajax'),
(27, 'Nsm_pp_workflow_mcp', 'cron_review_entries'),
(28, 'Freeform', 'save_form');

-- --------------------------------------------------------

--
-- Table structure for table `exp_captcha`
--

CREATE TABLE IF NOT EXISTS `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_categories`
--

CREATE TABLE IF NOT EXISTS `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_categories`
--

INSERT INTO `exp_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`) VALUES
(1, 1, 1, 0, 'Image', 'image', '', '0', 1),
(2, 1, 1, 0, 'Video', 'video', '', '0', 2),
(3, 1, 1, 0, 'iFrame', 'iframe', '', '0', 3);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_fields`
--

CREATE TABLE IF NOT EXISTS `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_field_data`
--

CREATE TABLE IF NOT EXISTS `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_category_field_data`
--

INSERT INTO `exp_category_field_data` (`cat_id`, `site_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 1),
(3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_groups`
--

CREATE TABLE IF NOT EXISTS `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_category_groups`
--

INSERT INTO `exp_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `exclude_group`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`) VALUES
(1, 1, 'Media Type', 'c', 0, 'all', '6', '6');

-- --------------------------------------------------------

--
-- Table structure for table `exp_category_posts`
--

CREATE TABLE IF NOT EXISTS `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_category_posts`
--

INSERT INTO `exp_category_posts` (`entry_id`, `cat_id`) VALUES
(3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channels`
--

CREATE TABLE IF NOT EXISTS `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_channels`
--

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`) VALUES
(1, 1, 'tunnels', 'Tunnels', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 1, 1, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(2, 1, 'hero', 'Hero', 'http://dev.iflyworld.com/index.php', NULL, 'en', 0, 0, 0, 0, NULL, 1, 'open', 2, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(3, 1, 'tiles', 'Tiles', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 3, 13, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(4, 1, 'flier_type', 'Flier Type', 'http://dev.iflyworld.com/index.php', NULL, 'en', 0, 0, 0, 0, NULL, 1, 'open', 4, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(5, 1, 'gift_cards', 'Gift Cards', 'http://dev.iflyworld.com/index.php', NULL, 'en', 0, 0, 0, 0, '', 1, 'open', 4, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(6, 1, 'reviews', 'Flyer Feedback', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, NULL, 1, 'open', 5, 24, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(7, 1, 'template_one', 'Template - 1 Column', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, '1', 1, 'closed', 6, 28, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 2),
(8, 1, 'events', 'Events', 'http://dev.iflyworld.com/index.php', NULL, 'en', 1, 0, 1348268399, 0, NULL, 1, 'open', 7, NULL, NULL, 'y', 'y', NULL, 'all', 'y', 'n', 'n', NULL, NULL, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', NULL, 0, NULL, NULL, 'y', NULL, 'n', 10, '', '', 0),
(9, 1, 'call_to_action', 'Call to Action', 'http://dev.iflyworld.com/index.php', '', 'en', 1, 0, 1349910370, 0, NULL, 1, 'open', 8, 38, '', 'n', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'n', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0),
(10, 1, 'template_2', 'Template - 2 Column', 'http://dev.iflyworld.com/index.php', '', 'en', 0, 0, 0, 0, '1', 1, 'open', 6, 28, '', 'y', 'y', NULL, 'all', 'y', 'n', 'n', '', '', 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', '', 0, '', '', 'y', '', 'n', 10, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_data`
--

CREATE TABLE IF NOT EXISTS `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_37` text,
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_41` text,
  `field_ft_41` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_data`
--

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_1`, `field_ft_1`, `field_id_2`, `field_ft_2`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`, `field_id_19`, `field_ft_19`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_22`, `field_ft_22`, `field_id_23`, `field_ft_23`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_26`, `field_ft_26`, `field_id_28`, `field_ft_28`, `field_id_29`, `field_ft_29`, `field_id_30`, `field_ft_30`, `field_id_31`, `field_ft_31`, `field_id_32`, `field_ft_32`, `field_id_33`, `field_ft_33`, `field_id_34`, `field_ft_34`, `field_id_35`, `field_ft_35`, `field_id_37`, `field_ft_37`, `field_id_38`, `field_ft_38`, `field_id_39`, `field_ft_39`, `field_id_40`, `field_ft_40`, `field_id_41`, `field_ft_41`, `field_id_42`, `field_ft_42`) VALUES
(1, 1, 8, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '0', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '{"start_date":"2012-09-21","start_time":"17:30","end_time":"18:30","end_date":"2012-09-21","all_day":"n"}', 'xhtml', '<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>\n<p>We will be creating a media frenzy around women in the sport of skydiving and celebrating women in flight!</p>\n<p>$35 registration (See breakdown below.)</p>\n<p>$500/30 minutes (before taxes) including coaching/organizing (Melanie Curtis, Melissa Nelson, Kimberly Winslow, Brianne Thompson, Amy Chemelecki and Cat Adam</p>\n<p>Lots of load organizing and Huck Jams to promote the big sister little sister bonding!</p>\n<p>After flight activities for those who want to continue the bonding experience, will include: a sleep in, wine tasting,yoga and more...</p>\n<p>Registration cost break down:\n\nNOTE: Anyone wanting to make an additional donation and meet the President of the LFL, (who is a skydiver affected by this auto-immune disease), we will have a donation box ready and you can make out your donation directly to the LFL.</p><ul><li>$10- T-shirt\n\n</li><li>$5- Yoga Instructor\n</li><li>$5- Massage\n</li><li>$5- Meal\n</li><li>$9- Essential Items\n</li><li>$1- Charity Donation - Leap for LUPUS Foundation - A charity started by skydivers!&nbsp;</li></ul>', 'xhtml', '<p><span>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis arcu nibh. Morbi gravida eros vitae leo pulvinar in dictum dolor volutpat. Nunc auctor ante id mauris porttitor iaculis. Curabitur mattis aliquam purus at convallis. Mauris eget risus non nibh placerat aliquam quis sed est. Aliquam erat volutpat. Donec turpis urna, tempor vel bibendum sed, dignissim at sem.</span></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis arcu nibh. Morbi gravida eros vitae leo pulvinar in dictum dolor volutpat. Nunc auctor ante id mauris porttitor iaculis. Curabitur mattis aliquam purus at convallis. Mauris eget risus non nibh placerat aliquam quis sed est. Aliquam erat volutpat. Donec turpis urna, tempor vel bibendum sed, dignissim at sem.\n</p>', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none', '', 'none', '', 'xhtml'),
(2, 1, 9, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '<p>When do you want to fly? Find your flight packages and times.​</p>', 'xhtml', '', NULL, '', 'none', '', 'none', '', 'xhtml'),
(3, 1, 7, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', 'none', '', 'none', '<p>​Some default copy</p>', 'xhtml', '', 'xhtml', '', 'none', '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, '', NULL, 'Contact Form', 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_entries_autosave`
--

CREATE TABLE IF NOT EXISTS `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_fields`
--

CREATE TABLE IF NOT EXISTS `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `exp_channel_fields`
--

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`) VALUES
(1, 1, 1, 'tunnel_brief_description', 'Brief Description', 'Some instructions will live here.', 'rte', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(2, 1, 1, 'tunnel_thumbnail_image', 'Thumbnail Image', 'Image dimensions : 100px x 100px', 'file', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(3, 1, 1, 'tunnel_address', 'Address', 'Physical Address', 'textarea', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(4, 1, 1, 'tunnel_copyright', 'Copyright', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(5, 1, 1, 'tunnel_contact_email', 'Contact Email', 'Email address where email correspondence should be directed.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(6, 1, 1, 'tunnel_contact_phone', 'Contact Phone', 'Main phone number to handle enquiries.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(7, 1, 1, 'tunnel_facebook', 'Social Media - Facebook Page', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(8, 1, 1, 'tunnel_twitter', 'Social Media - Twitter Account', '', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(9, 1, 2, 'hero_tab_link', 'Tab Link', 'Maximum 16 characters. This is used to populate the navigation links.', 'text', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 16, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(10, 1, 2, 'hero_description', 'Main Description', 'This will appear above the image or video.', 'rte', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(11, 1, 2, 'hero_image', 'Image', 'Choose this field if no video is to be used.', 'file', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(12, 1, 2, 'hero_video', 'Video', '', 'videoplayer', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(13, 1, 3, 'tile_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(14, 1, 3, 'tile_button_text', 'Button Text', 'Maximum 20 characters.', 'text', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 20, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(15, 1, 3, 'tile_image', 'Image', 'Dimensions : 320px x 180px', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(16, 1, 3, 'tile_video', 'Video', 'YouTube video link.', 'videoplayer', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(17, 1, 3, 'tile_iframe_link', 'iFrame Link', 'Link to external content you want to include.\n\nFormat : http://example.com/page.html', 'text', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(18, 1, 4, 'flier_overview_image', 'Overview Page : Image', 'Dimensions : ', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(19, 1, 4, 'flier_overview_copy', 'Overview Page : Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(20, 1, 4, 'flier_detail_image', 'Detail Page : Image', 'Dimensions : ', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(21, 1, 4, 'flier_detail_copy', 'Detail Page : Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(22, 1, 4, 'flier_booking_image', 'Booking Flow : Pop-up Image', 'Dimensions : ', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(23, 1, 4, 'flier_booking_copy', 'Booking Flow : Pop-up Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 6, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(24, 1, 5, 'testimonial_copy', 'Testimonial', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(25, 1, 5, 'testimonial_image', 'Image', '', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(26, 1, 5, 'testimonial_video', 'Video', 'YouTube link.', 'videoplayer', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(28, 1, 6, 'info_hero', 'Hero', 'Leave blank and no hero space will be used.', 'playa', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YToxMjp7czo1OiJtdWx0aSI7czoxOiJ5IjtzOjc6ImV4cGlyZWQiO3M6MToibiI7czo2OiJmdXR1cmUiO3M6MToieSI7czo4OiJjaGFubmVscyI7YToxOntpOjA7czoxOiIyIjt9czo3OiJvcmRlcmJ5IjtzOjU6InRpdGxlIjtzOjQ6InNvcnQiO3M6MzoiQVNDIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(29, 1, 6, 'info_image', 'Image', 'If present this image will be floated to the right of the main content.', 'file', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE5OiJhbGxvd2VkX2RpcmVjdG9yaWVzIjtzOjM6ImFsbCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(30, 1, 6, 'info_copy', 'Copy', '', 'rte', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(31, 1, 6, 'info_video', 'Video', 'YouTube video link', 'videoplayer', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(32, 1, 6, 'info_iframe', 'iFrame', 'External page to display.\n\nFormat http://example.com/page.html', 'text', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(33, 1, 7, 'event_date', 'Event Date', '', 'low_events', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjI6IjMwIjtzOjE2OiJkZWZhdWx0X2R1cmF0aW9uIjtzOjI6IjYwIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(34, 1, 7, 'event_description', 'Event Description', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 2, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(35, 1, 7, 'event_lede', 'Event Lede', '', 'rte', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 3, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(37, 1, 1, 'tunnel_geo_location', 'Geo Location', '', 'mx_google_map', '', '0', 0, 0, 'channel', 8, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 9, 'any', 'YToxMjp7czo4OiJsYXRpdHVkZSI7czoxNzoiNDQuMDYxOTMyOTc4NjUzNDgiO3M6OToibG9uZ2l0dWRlIjtzOjE5OiItMTIxLjI3NTg0NDU3Mzk3NDYxIjtzOjQ6Inpvb20iO3M6MjoiMTMiO3M6MTA6Im1heF9wb2ludHMiO3M6MToiMSI7czo0OiJpY29uIjtzOjg6ImhvbWUucG5nIjtzOjk6InNsaWRlX2JhciI7czoxOiJ5IjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(38, 1, 8, 'cta_description', 'Description', '', 'rte', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 10, 128, 'y', 'ltr', 'n', 'n', 'xhtml', 'n', 1, 'any', 'YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
(39, 1, 1, 'tunnel_hours', 'Tunnel Hours', '', 'text', '', '0', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(40, 1, 2, 'hero_type', 'Hero Type', '', 'radio', 'Image\nVideo', 'n', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(41, 1, 5, 'testimonial_type', 'Media Type', '', 'radio', 'Image\nVideo', 'n', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
(42, 1, 6, 'info_form', 'Forms', '', 'select', 'Contact Form\nRegistration Form', 'n', 0, 0, 'channel', 9, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'xhtml', 'n', 6, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_channel_member_groups`
--

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`) VALUES
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9);

-- --------------------------------------------------------

--
-- Table structure for table `exp_channel_titles`
--

CREATE TABLE IF NOT EXISTS `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_channel_titles`
--

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`) VALUES
(1, 1, 8, 1, 0, NULL, '108.13.107.180', 'Test Event 1', 'test-event-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1348268399, 'n', '2012', '09', '21', 0, 0, 20120925214000, 0, 0),
(2, 1, 9, 1, 0, NULL, '98.149.249.30', 'Quick Book', 'quick-book', 'open', 'y', 0, 0, 0, 0, 'n', 'n', 1349910370, 'n', '2012', '10', '10', 0, 0, 20121010180610, 0, 0),
(3, 1, 7, 1, 0, NULL, '98.149.249.30', 'Test entry', 'test-entry', 'closed', 'y', 0, 0, 0, 0, 'y', 'n', 1349988505, 'n', '2012', '10', '11', 0, 0, 20121011154825, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_comments`
--

CREATE TABLE IF NOT EXISTS `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_comment_subscriptions`
--

CREATE TABLE IF NOT EXISTS `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_analytics`
--

CREATE TABLE IF NOT EXISTS `exp_cp_analytics` (
  `site_id` int(5) unsigned NOT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `profile` text,
  `settings` text,
  `hourly_cache` text,
  `daily_cache` text,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_log`
--

CREATE TABLE IF NOT EXISTS `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `exp_cp_log`
--

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`) VALUES
(1, 1, 1, 'admin', '86.164.24.131', 1346761990, 'Logged in'),
(2, 1, 1, 'admin', '86.164.24.131', 1346762054, 'Logged out'),
(3, 1, 1, 'admin', '86.164.24.131', 1346762088, 'Logged in'),
(4, 1, 1, 'admin', '86.164.24.131', 1346762416, 'Channel Created:&nbsp;&nbsp;Tunnel'),
(5, 1, 1, 'admin', '86.164.24.131', 1346762440, 'Field Group Created:&nbsp;Tunnel Info'),
(6, 1, 1, 'admin', '86.164.24.131', 1346763201, 'Field Group Created:&nbsp;Hero'),
(7, 1, 1, 'admin', '86.164.24.131', 1346763479, 'Member Group Created:&nbsp;&nbsp;Global Manager'),
(8, 1, 1, 'admin', '86.164.24.131', 1346763525, 'Category Group Created:&nbsp;&nbsp;Hero Type'),
(9, 1, 1, 'admin', '86.164.24.131', 1346764383, 'Channel Created:&nbsp;&nbsp;Hero'),
(10, 1, 1, 'admin', '86.164.24.131', 1346764557, 'Member profile created:&nbsp;&nbsp;manager'),
(11, 1, 1, 'admin', '86.164.24.131', 1346764610, 'Logged out'),
(12, 1, 2, 'manager', '86.164.24.131', 1346764654, 'Logged in'),
(13, 1, 2, 'manager', '86.164.24.131', 1346764705, 'Logged out'),
(14, 1, 1, 'admin', '86.164.24.131', 1346764721, 'Logged in'),
(15, 1, 1, 'admin', '86.164.24.131', 1346764759, 'Member Group Updated:&nbsp;&nbsp;Global Manager'),
(16, 1, 1, 'admin', '86.164.24.131', 1346764768, 'Logged out'),
(17, 1, 1, 'admin', '86.164.24.131', 1346766803, 'Logged in'),
(18, 1, 1, 'admin', '86.164.24.131', 1346766830, 'Field Group Created:&nbsp;Promotional Tiles'),
(19, 1, 1, 'admin', '86.164.24.131', 1346767253, 'Logged in'),
(20, 1, 1, 'admin', '86.164.24.131', 1346767592, 'Channel Created:&nbsp;&nbsp;Promotional Tile'),
(21, 1, 1, 'admin', '86.164.24.131', 1346767746, 'Channel Created:&nbsp;&nbsp;Flier Type'),
(22, 1, 1, 'admin', '86.164.24.131', 1346767764, 'Field Group Created:&nbsp;Flier Type'),
(23, 1, 1, 'admin', '86.164.24.131', 1346768263, 'Channel Created:&nbsp;&nbsp;Gift Cards'),
(24, 1, 1, 'admin', '86.164.24.131', 1346768308, 'Field Group Created:&nbsp;Testimonials'),
(25, 1, 1, 'admin', '86.164.24.131', 1346768548, 'Channel Created:&nbsp;&nbsp;Testimonials'),
(26, 1, 1, 'admin', '86.164.24.131', 1346768657, 'Field Group Created:&nbsp;Information Page'),
(27, 1, 1, 'admin', '86.164.24.131', 1346769043, 'Channel Created:&nbsp;&nbsp;Information page'),
(28, 1, 2, 'manager', '97.79.130.126', 1346786891, 'Logged in'),
(29, 1, 1, 'admin', '97.79.130.126', 1346789502, 'Logged in'),
(30, 1, 1, 'admin', '81.151.194.18', 1346790082, 'Logged in'),
(31, 1, 1, 'admin', '97.79.130.126', 1346799889, 'Logged in'),
(32, 1, 1, 'admin', '97.79.130.126', 1346799962, 'Logged out'),
(33, 1, 1, 'admin', '97.79.130.126', 1346948766, 'Logged in'),
(34, 1, 2, 'manager', '97.79.130.126', 1346948799, 'Logged in'),
(35, 1, 2, 'manager', '97.79.130.126', 1346953462, 'Logged in'),
(36, 1, 2, 'manager', '24.153.178.154', 1346964328, 'Logged in'),
(37, 1, 2, 'manager', '97.79.130.126', 1347046865, 'Logged in'),
(38, 1, 1, 'admin', '97.79.130.126', 1347982573, 'Logged in'),
(39, 1, 1, 'admin', '97.79.130.126', 1347993819, 'Logged in'),
(40, 1, 1, 'admin', '97.79.130.126', 1347994592, 'Logged in'),
(41, 1, 1, 'admin', '97.79.130.126', 1348006417, 'Logged in'),
(42, 1, 1, 'admin', '108.13.107.180', 1348008095, 'Logged in'),
(43, 1, 1, 'admin', '108.13.107.180', 1348017639, 'Logged in'),
(44, 1, 1, 'admin', '97.79.130.126', 1348068386, 'Logged in'),
(45, 1, 1, 'admin', '97.79.130.126', 1348068960, 'Member profile created:&nbsp;&nbsp;stuartw'),
(46, 1, 1, 'admin', '97.79.130.126', 1348069070, 'Member profile created:&nbsp;&nbsp;nayladp'),
(47, 1, 1, 'admin', '97.79.130.126', 1348069123, 'Member profile created:&nbsp;&nbsp;axelz'),
(48, 1, 1, 'admin', '97.79.130.126', 1348070946, 'Member profile created:&nbsp;&nbsp;chew'),
(49, 1, 1, 'admin', '97.79.130.126', 1348070997, 'Member profile created:&nbsp;&nbsp;craigb'),
(50, 1, 1, 'admin', '97.79.130.126', 1348077384, 'Logged in'),
(51, 1, 1, 'admin', '24.153.178.154', 1348084032, 'Logged in'),
(52, 1, 1, 'admin', '24.153.178.154', 1348085079, 'Logged out'),
(53, 1, 3, 'stuartw', '24.153.178.154', 1348085114, 'Logged in'),
(54, 1, 1, 'admin', '98.149.249.30', 1348268234, 'Logged in'),
(55, 1, 1, 'admin', '98.149.249.30', 1348268316, 'Channel Created:&nbsp;&nbsp;Events'),
(56, 1, 1, 'admin', '98.149.249.30', 1348268337, 'Field Group Created:&nbsp;Events'),
(57, 1, 1, 'admin', '108.13.107.180', 1348602988, 'Logged in'),
(58, 1, 1, 'admin', '108.13.107.180', 1348608590, 'Logged in'),
(59, 1, 1, 'admin', '108.13.107.180', 1348768409, 'Logged in'),
(60, 1, 1, 'admin', '108.13.107.180', 1348856240, 'Logged in'),
(61, 1, 1, 'admin', '97.79.130.126', 1348864730, 'Logged in'),
(62, 1, 1, 'admin', '97.79.130.126', 1348864980, 'Member profile created:&nbsp;&nbsp;royh'),
(63, 1, 1, 'admin', '97.79.130.126', 1348865151, 'Logged out'),
(64, 1, 3, 'stuartw', '97.79.130.126', 1349112303, 'Logged in'),
(65, 1, 5, 'axelz', '24.153.178.154', 1349123350, 'Logged in'),
(66, 1, 1, 'admin', '108.13.107.180', 1349123591, 'Logged in'),
(67, 1, 3, 'stuartw', '70.124.65.165', 1349150896, 'Logged in'),
(68, 1, 4, 'nayladp', '74.66.251.75', 1349189165, 'Logged in'),
(69, 1, 7, 'craigb', '97.79.130.126', 1349196638, 'Logged in'),
(70, 1, 1, 'admin', '108.13.107.180', 1349197344, 'Logged in'),
(71, 1, 1, 'admin', '108.13.107.180', 1349197390, 'Logged out'),
(72, 1, 7, 'craigb', '108.13.107.180', 1349197423, 'Logged in'),
(73, 1, 7, 'craigb', '108.13.107.180', 1349198883, 'Logged out'),
(74, 1, 1, 'admin', '108.13.107.180', 1349198900, 'Logged in'),
(75, 1, 1, 'admin', '108.13.107.180', 1349198989, 'Channel Created:&nbsp;&nbsp;Call to Action Copy'),
(76, 1, 1, 'admin', '108.13.107.180', 1349199017, 'Field Group Created:&nbsp;Call to Action Copy'),
(77, 1, 1, 'admin', '108.13.107.180', 1349199400, 'Member Group Updated:&nbsp;&nbsp;Global Manager'),
(78, 1, 7, 'craigb', '97.79.130.126', 1349201034, 'Logged out'),
(79, 1, 7, 'craigb', '97.79.130.126', 1349281049, 'Logged in'),
(80, 1, 7, 'craigb', '24.153.178.154', 1349293531, 'Logged in'),
(81, 1, 7, 'craigb', '24.153.178.154', 1349296166, 'Logged out'),
(82, 1, 7, 'craigb', '97.79.130.126', 1349457025, 'Logged in'),
(83, 1, 7, 'craigb', '97.79.130.126', 1349461004, 'Logged out'),
(84, 1, 7, 'craigb', '97.79.130.126', 1349461180, 'Logged in'),
(85, 1, 7, 'craigb', '97.79.130.126', 1349717081, 'Logged in'),
(86, 1, 1, 'admin', '98.149.249.30', 1349724189, 'Logged in'),
(87, 1, 7, 'craigb', '97.79.130.126', 1349799433, 'Logged in'),
(88, 1, 7, 'craigb', '97.79.130.126', 1349894200, 'Logged in'),
(89, 1, 7, 'craigb', '97.79.130.126', 1349907507, 'Logged in'),
(90, 1, 1, 'admin', '98.149.249.30', 1349907513, 'Logged in'),
(91, 1, 7, 'craigb', '97.79.130.126', 1349911931, 'Logged out'),
(92, 1, 1, 'admin', '98.149.249.30', 1349912163, 'Logged in'),
(93, 1, 1, 'admin', '98.149.249.30', 1349913768, 'Custom Field Deleted:&nbsp;iFrame'),
(94, 1, 1, 'admin', '98.149.249.30', 1349914143, 'Custom Field Deleted:&nbsp;iFrame'),
(95, 1, 1, 'admin', '98.149.249.30', 1349916650, 'Channel Created:&nbsp;&nbsp;Template - 2 Column'),
(96, 1, 8, 'royh', '24.153.178.154', 1349933752, 'Logged in'),
(97, 1, 8, 'royh', '24.153.178.154', 1349933868, 'Logged out'),
(98, 1, 8, 'royh', '24.153.178.154', 1349933869, 'Logged out'),
(99, 1, 1, 'admin', '173.198.122.173', 1349979906, 'Logged in'),
(100, 1, 1, 'admin', '173.198.122.173', 1349980057, 'Logged in'),
(101, 1, 7, 'craigb', '97.79.130.126', 1349985299, 'Logged in'),
(102, 1, 1, 'admin', '98.149.249.30', 1349995083, 'Logged out'),
(103, 1, 1, 'admin', '98.149.249.30', 1349998940, 'Logged in'),
(104, 1, 1, 'admin', '108.13.107.180', 1350598790, 'Logged in');

-- --------------------------------------------------------

--
-- Table structure for table `exp_cp_search_index`
--

CREATE TABLE IF NOT EXISTS `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_developer_log`
--

CREATE TABLE IF NOT EXISTS `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_developer_log`
--

INSERT INTO `exp_developer_log` (`log_id`, `timestamp`, `viewed`, `description`, `function`, `line`, `file`, `deprecated_since`, `use_instead`) VALUES
(1, 1349980638, 'n', 'videoplayer/lirabries/services/youtube.php "ping" method returned this error : Expected response code 200, got 403\n\n\nInvalid developer key\n\n\nInvalid developer key\nError 403\n\n\n', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_mg`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_cache_ml`
--

CREATE TABLE IF NOT EXISTS `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_console_cache`
--

CREATE TABLE IF NOT EXISTS `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_email_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_ping_status`
--

CREATE TABLE IF NOT EXISTS `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_entry_versioning`
--

CREATE TABLE IF NOT EXISTS `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_extensions`
--

CREATE TABLE IF NOT EXISTS `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `exp_extensions`
--

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`) VALUES
(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y'),
(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y'),
(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y'),
(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y'),
(5, 'Navee_ext', 'entry_submission_end', 'entry_submission_end', 's:0:"";', 10, '2.2.5', 'y'),
(6, 'Navee_ext', 'delete_entries_loop', 'delete_entries_loop', 's:0:"";', 10, '2.2.5', 'y'),
(7, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y'),
(8, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.4.3', 'y'),
(9, 'Mx_title_control_ext', 'cp_js_end', 'cp_js_end', 'a:38:{s:15:"title_default_9";s:8:"Headline";s:19:"url_title_default_9";s:0:"";s:15:"title_default_8";s:0:"";s:19:"url_title_default_8";s:0:"";s:15:"title_default_4";s:0:"";s:19:"url_title_default_4";s:0:"";s:15:"title_default_5";s:0:"";s:19:"url_title_default_5";s:0:"";s:15:"title_default_2";s:8:"Headline";s:19:"url_title_default_2";s:0:"";s:15:"title_default_7";s:0:"";s:19:"url_title_default_7";s:0:"";s:15:"title_default_3";s:8:"Headline";s:19:"url_title_default_3";s:0:"";s:15:"title_default_6";s:0:"";s:19:"url_title_default_6";s:0:"";s:15:"title_default_1";s:11:"Tunnel Name";s:19:"url_title_default_1";s:0:"";s:13:"multilanguage";s:1:"n";s:9:"max_title";s:3:"100";s:15:"title_pattern_9";s:0:"";s:19:"url_title_pattern_9";s:0:"";s:15:"title_pattern_8";s:0:"";s:19:"url_title_pattern_8";s:0:"";s:15:"title_pattern_4";s:0:"";s:19:"url_title_pattern_4";s:0:"";s:15:"title_pattern_5";s:0:"";s:19:"url_title_pattern_5";s:0:"";s:15:"title_pattern_2";s:0:"";s:19:"url_title_pattern_2";s:0:"";s:15:"title_pattern_7";s:0:"";s:19:"url_title_pattern_7";s:0:"";s:15:"title_pattern_3";s:0:"";s:19:"url_title_pattern_3";s:0:"";s:15:"title_pattern_6";s:0:"";s:19:"url_title_pattern_6";s:0:"";s:15:"title_pattern_1";s:0:"";s:19:"url_title_pattern_1";s:0:"";}', 1, '2.8.0', 'y'),
(10, 'Mx_title_control_ext', 'entry_submission_end', 'entry_submission_end', 'a:38:{s:15:"title_default_9";s:8:"Headline";s:19:"url_title_default_9";s:0:"";s:15:"title_default_8";s:0:"";s:19:"url_title_default_8";s:0:"";s:15:"title_default_4";s:0:"";s:19:"url_title_default_4";s:0:"";s:15:"title_default_5";s:0:"";s:19:"url_title_default_5";s:0:"";s:15:"title_default_2";s:8:"Headline";s:19:"url_title_default_2";s:0:"";s:15:"title_default_7";s:0:"";s:19:"url_title_default_7";s:0:"";s:15:"title_default_3";s:8:"Headline";s:19:"url_title_default_3";s:0:"";s:15:"title_default_6";s:0:"";s:19:"url_title_default_6";s:0:"";s:15:"title_default_1";s:11:"Tunnel Name";s:19:"url_title_default_1";s:0:"";s:13:"multilanguage";s:1:"n";s:9:"max_title";s:3:"100";s:15:"title_pattern_9";s:0:"";s:19:"url_title_pattern_9";s:0:"";s:15:"title_pattern_8";s:0:"";s:19:"url_title_pattern_8";s:0:"";s:15:"title_pattern_4";s:0:"";s:19:"url_title_pattern_4";s:0:"";s:15:"title_pattern_5";s:0:"";s:19:"url_title_pattern_5";s:0:"";s:15:"title_pattern_2";s:0:"";s:19:"url_title_pattern_2";s:0:"";s:15:"title_pattern_7";s:0:"";s:19:"url_title_pattern_7";s:0:"";s:15:"title_pattern_3";s:0:"";s:19:"url_title_pattern_3";s:0:"";s:15:"title_pattern_6";s:0:"";s:19:"url_title_pattern_6";s:0:"";s:15:"title_pattern_1";s:0:"";s:19:"url_title_pattern_1";s:0:"";}', 1, '2.8.0', 'y'),
(11, 'Nsm_pp_workflow_ext', 'dummy_hook_function', 'dummy_hook_function', 'a:0:{}', 10, '0.10.2', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `exp_fieldtypes`
--

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`) VALUES
(1, 'select', '1.0', 'YTowOnt9', 'n'),
(2, 'text', '1.0', 'YTowOnt9', 'n'),
(3, 'textarea', '1.0', 'YTowOnt9', 'n'),
(4, 'date', '1.0', 'YTowOnt9', 'n'),
(5, 'file', '1.0', 'YTowOnt9', 'n'),
(6, 'multi_select', '1.0', 'YTowOnt9', 'n'),
(7, 'checkboxes', '1.0', 'YTowOnt9', 'n'),
(8, 'radio', '1.0', 'YTowOnt9', 'n'),
(9, 'rel', '1.0', 'YTowOnt9', 'n'),
(10, 'rte', '1.0', 'YTowOnt9', 'n'),
(11, 'mx_google_map', '1.4', 'YTo4OntzOjg6ImxhdGl0dWRlIjtzOjE3OiI0NC4wNjE5MzI5Nzg2NTM0OCI7czo5OiJsb25naXR1ZGUiO3M6MTk6Ii0xMjEuMjc1ODQ0NTczOTc0NjEiO3M6NDoiem9vbSI7aToxMztzOjEwOiJtYXhfcG9pbnRzIjtzOjE6IjMiO3M6NDoiaWNvbiI7czowOiIiO3M6OToic2xpZGVfYmFyIjtzOjE6InkiO3M6MTg6InBhdGhfbWFya2Vyc19pY29ucyI7czo3NzoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFx0aGVtZXNcL3RoaXJkX3BhcnR5L214X2dvb2dsZV9tYXAvbWFwcy1pY29ucy8iO3M6MTc6InVybF9tYXJrZXJzX2ljb25zIjtzOjY5OiJodHRwOi8vZGV2LmlmbHl3b3JsZC5jb20vdGhlbWVzL3RoaXJkX3BhcnR5L214X2dvb2dsZV9tYXAvbWFwcy1pY29ucy8iO30=', 'y'),
(12, 'navee', '2.2.5', 'YTowOnt9', 'n'),
(13, 'playa', '4.3.3', 'YTowOnt9', 'y'),
(14, 'videoplayer', '3.1.1', 'YToxOntzOjk6InZpZGVvX3VybCI7czowOiIiO30=', 'n'),
(15, 'matrix', '2.4.3', 'YTowOnt9', 'y'),
(16, 'low_events', '1.0.4', 'YTowOnt9', 'n'),
(17, 'nsm_pp_workflow', '0.10.2', 'YTowOnt9', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_formatting`
--

CREATE TABLE IF NOT EXISTS `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=127 ;

--
-- Dumping data for table `exp_field_formatting`
--

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`) VALUES
(1, 1, 'none'),
(2, 1, 'br'),
(3, 1, 'xhtml'),
(4, 2, 'none'),
(5, 2, 'br'),
(6, 2, 'xhtml'),
(7, 3, 'none'),
(8, 3, 'br'),
(9, 3, 'xhtml'),
(10, 4, 'none'),
(11, 4, 'br'),
(12, 4, 'xhtml'),
(13, 5, 'none'),
(14, 5, 'br'),
(15, 5, 'xhtml'),
(16, 6, 'none'),
(17, 6, 'br'),
(18, 6, 'xhtml'),
(19, 7, 'none'),
(20, 7, 'br'),
(21, 7, 'xhtml'),
(22, 8, 'none'),
(23, 8, 'br'),
(24, 8, 'xhtml'),
(25, 9, 'none'),
(26, 9, 'br'),
(27, 9, 'xhtml'),
(28, 10, 'none'),
(29, 10, 'br'),
(30, 10, 'xhtml'),
(31, 11, 'none'),
(32, 11, 'br'),
(33, 11, 'xhtml'),
(34, 12, 'none'),
(35, 12, 'br'),
(36, 12, 'xhtml'),
(37, 13, 'none'),
(38, 13, 'br'),
(39, 13, 'xhtml'),
(40, 14, 'none'),
(41, 14, 'br'),
(42, 14, 'xhtml'),
(43, 15, 'none'),
(44, 15, 'br'),
(45, 15, 'xhtml'),
(46, 16, 'none'),
(47, 16, 'br'),
(48, 16, 'xhtml'),
(49, 17, 'none'),
(50, 17, 'br'),
(51, 17, 'xhtml'),
(52, 18, 'none'),
(53, 18, 'br'),
(54, 18, 'xhtml'),
(55, 19, 'none'),
(56, 19, 'br'),
(57, 19, 'xhtml'),
(58, 20, 'none'),
(59, 20, 'br'),
(60, 20, 'xhtml'),
(61, 21, 'none'),
(62, 21, 'br'),
(63, 21, 'xhtml'),
(64, 22, 'none'),
(65, 22, 'br'),
(66, 22, 'xhtml'),
(67, 23, 'none'),
(68, 23, 'br'),
(69, 23, 'xhtml'),
(70, 24, 'none'),
(71, 24, 'br'),
(72, 24, 'xhtml'),
(73, 25, 'none'),
(74, 25, 'br'),
(75, 25, 'xhtml'),
(76, 26, 'none'),
(77, 26, 'br'),
(78, 26, 'xhtml'),
(82, 28, 'none'),
(83, 28, 'br'),
(84, 28, 'xhtml'),
(85, 29, 'none'),
(86, 29, 'br'),
(87, 29, 'xhtml'),
(88, 30, 'none'),
(89, 30, 'br'),
(90, 30, 'xhtml'),
(91, 31, 'none'),
(92, 31, 'br'),
(93, 31, 'xhtml'),
(94, 32, 'none'),
(95, 32, 'br'),
(96, 32, 'xhtml'),
(97, 33, 'none'),
(98, 33, 'br'),
(99, 33, 'xhtml'),
(100, 34, 'none'),
(101, 34, 'br'),
(102, 34, 'xhtml'),
(103, 35, 'none'),
(104, 35, 'br'),
(105, 35, 'xhtml'),
(109, 37, 'none'),
(110, 37, 'br'),
(111, 37, 'xhtml'),
(112, 38, 'none'),
(113, 38, 'br'),
(114, 38, 'xhtml'),
(115, 39, 'none'),
(116, 39, 'br'),
(117, 39, 'xhtml'),
(118, 40, 'none'),
(119, 40, 'br'),
(120, 40, 'xhtml'),
(121, 41, 'none'),
(122, 41, 'br'),
(123, 41, 'xhtml'),
(124, 42, 'none'),
(125, 42, 'br'),
(126, 42, 'xhtml');

-- --------------------------------------------------------

--
-- Table structure for table `exp_field_groups`
--

CREATE TABLE IF NOT EXISTS `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_field_groups`
--

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Tunnel Info'),
(2, 1, 'Hero'),
(3, 1, 'Tiles'),
(4, 1, 'Flier Type'),
(5, 1, 'Flyer Feedback'),
(6, 1, 'Information Page'),
(7, 1, 'Events'),
(8, 1, 'Call to Action Copy');

-- --------------------------------------------------------

--
-- Table structure for table `exp_files`
--

CREATE TABLE IF NOT EXISTS `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_files`
--

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`) VALUES
(1, 1, '1-col.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/1-col.jpg', 'image/jpeg', '1-col.jpg', 623790, NULL, NULL, NULL, 1, 1348858260, 1, 1348858260, '2368 1369'),
(2, 1, 'img01.png', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img01.png', 'image/png', 'img01.png', 191024, NULL, NULL, NULL, 1, 1349724484, 1, 1349987779, '368 764'),
(3, 1, 'calendar_sprite.png', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/calendar_sprite.png', 'image/png', 'calendar_sprite.png', 3543, '', '', '', 1, 1349987904, 1, 1349987968, '19 57'),
(4, 1, 'img07-right.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img07.jpg', 'image/jpeg', 'img07.jpg', 21921, '', '', '', 1, 1349998974, 1, 1349999032, '313 186'),
(5, 1, 'right.jpg', 1, 'C:/inetpub/wwwroot/deviflyworld/images/uploads/img05.jpg', 'image/jpeg', 'img05.jpg', 17792, '', '', '', 1, 1349999077, 1, 1349999182, '313 186');

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_categories`
--

CREATE TABLE IF NOT EXISTS `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_dimensions`
--

CREATE TABLE IF NOT EXISTS `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_file_watermarks`
--

CREATE TABLE IF NOT EXISTS `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_layouts`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_composer_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fields`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `exp_freeform_fields`
--

INSERT INTO `exp_freeform_fields` (`field_id`, `site_id`, `field_name`, `field_label`, `field_type`, `settings`, `author_id`, `entry_date`, `edit_date`, `required`, `submissions_page`, `moderation_page`, `composer_use`, `field_description`) VALUES
(1, 1, 'first_name', 'First Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s first name.'),
(2, 1, 'last_name', 'Last Name', 'text', '{"field_length":150,"field_content_type":"any"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s last name.'),
(3, 1, 'email', 'Email', 'text', '{"field_length":150,"field_content_type":"email"}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.'),
(4, 1, 'user_message', 'Message', 'textarea', '{"field_ta_rows":6}', 1, 1349915768, 0, 'n', 'y', 'y', 'y', 'This field contains the user''s message.'),
(5, 1, 'home_drop_zone', 'Home Drop Zone', 'text', '{"field_length":"150","field_content_type":"any","disallow_html_rendering":"y"}', 1, 1349915979, 0, 'n', 'y', 'y', 'y', ''),
(6, 1, 'number_of_times_you_have_jumped', 'Number of times you have jumped', 'text', '{"field_length":"150","field_content_type":"any","disallow_html_rendering":"y"}', 1, 1349916019, 0, 'n', 'y', 'y', 'y', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_fieldtypes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `exp_freeform_fieldtypes`
--

INSERT INTO `exp_freeform_fieldtypes` (`fieldtype_id`, `fieldtype_name`, `settings`, `default_field`, `version`) VALUES
(1, 'file_upload', '[]', 'n', '4.0.7'),
(2, 'mailinglist', '[]', 'n', '4.0.7'),
(3, 'text', '[]', 'n', '4.0.7'),
(4, 'textarea', '[]', 'n', '4.0.7');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_file_uploads`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_forms`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_freeform_forms`
--

INSERT INTO `exp_freeform_forms` (`form_id`, `site_id`, `form_name`, `form_label`, `default_status`, `notify_user`, `notify_admin`, `user_email_field`, `user_notification_id`, `admin_notification_id`, `admin_notification_email`, `form_description`, `field_ids`, `field_order`, `template_id`, `composer_id`, `author_id`, `entry_date`, `edit_date`, `settings`) VALUES
(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'alex@e-digitalgroup.com', 'This is a basic contact form.', '1|2|3|4', '1|2|4|3', 0, 0, 1, 1349915768, 1349987408, NULL),
(2, 1, 'event_registration', 'Event Registration', 'pending', 'y', 'y', '3', 0, 0, 'alex@e-digitalgroup.com', '', '1|2|3|5|6', '3|1|2|5|6', 0, 0, 1, 1349915910, 1349916076, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_1`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_1` text,
  `form_field_2` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_form_entries_1`
--

INSERT INTO `exp_freeform_form_entries_1` (`entry_id`, `site_id`, `author_id`, `complete`, `ip_address`, `entry_date`, `edit_date`, `status`, `form_field_1`, `form_field_2`, `form_field_3`, `form_field_4`) VALUES
(1, 1, 0, 'y', '127.0.0.1', 1349915768, 0, 'pending', 'Jake', 'Solspace', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.');

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_form_entries_2`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_1` text,
  `form_field_2` text,
  `form_field_3` text,
  `form_field_5` text,
  `form_field_6` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_multipage_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_notification_templates`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_params`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_preferences`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_freeform_preferences`
--

INSERT INTO `exp_freeform_preferences` (`preference_id`, `preference_name`, `preference_value`, `site_id`) VALUES
(1, 'ffp', 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_freeform_user_email`
--

CREATE TABLE IF NOT EXISTS `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_global_variables`
--

CREATE TABLE IF NOT EXISTS `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_html_buttons`
--

CREATE TABLE IF NOT EXISTS `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exp_html_buttons`
--

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`) VALUES
(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b'),
(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i'),
(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote'),
(4, 1, 0, 'a', '<a href="[![Link:!:http://]!]"(!( title="[![Title]!]")!)>', '</a>', 'a', 4, '1', 'btn_a'),
(5, 1, 0, 'img', '<img src="[![Link:!:http://]!]" alt="[![Alternative text]!]" />', '', '', 5, '1', 'btn_img');

-- --------------------------------------------------------

--
-- Table structure for table `exp_layout_publish`
--

CREATE TABLE IF NOT EXISTS `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `exp_layout_publish`
--

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`) VALUES
(5, 1, 1, 7, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:28;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:31;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:32;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(6, 1, 6, 7, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:28;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:31;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:32;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(15, 1, 1, 4, 'a:4:{s:7:"publish";a:16:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:19;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:21;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:11:"ee_required";a:3:{s:10:"_tab_label";s:11:"EE Required";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(16, 1, 6, 4, 'a:4:{s:7:"publish";a:16:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:18;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:19;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:20;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:21;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:22;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:23;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:11:"new_channel";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:0;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:11:"ee_required";a:3:{s:10:"_tab_label";s:11:"EE Required";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(17, 1, 1, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:33;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:35;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:34;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(18, 1, 6, 8, 'a:6:{s:7:"publish";a:6:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:33;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:35;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:34;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:10:"categories";a:2:{s:10:"_tab_label";s:10:"Categories";s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"SEO Lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(21, 1, 1, 3, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(22, 1, 6, 3, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:13;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:14;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:15;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:16;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:17;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(23, 1, 1, 6, 'a:5:{s:7:"publish";a:8:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:24;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:41;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(24, 1, 6, 6, 'a:5:{s:7:"publish";a:8:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:24;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:25;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:26;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:41;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(25, 1, 1, 2, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:40;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:2:{s:10:"_tab_label";s:15:"nsm_pp_workflow";s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(26, 1, 6, 2, 'a:5:{s:7:"publish";a:9:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}i:9;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:40;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:10;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:11;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:12;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:4:{s:10:"_tab_label";s:8:"seo_lite";s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:2:{s:10:"_tab_label";s:15:"nsm_pp_workflow";s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";b:1;s:8:"collapse";b:1;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}}'),
(27, 1, 1, 10, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:28;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:31;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:32;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}'),
(28, 1, 6, 10, 'a:5:{s:7:"publish";a:10:{s:10:"_tab_label";s:7:"Publish";s:5:"title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:9:"url_title";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:3:"50%";}s:8:"category";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:28;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:29;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:30;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:31;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}i:32;a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}i:42;a:4:{s:7:"visible";s:4:"true";s:8:"collapse";b:0;s:11:"htmlbuttons";b:0;s:5:"width";s:4:"100%";}}s:4:"date";a:4:{s:10:"_tab_label";s:4:"Date";s:10:"entry_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:15:"expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:23:"comment_expiration_date";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:7:"options";a:6:{s:10:"_tab_label";s:7:"Options";s:11:"new_channel";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"status";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:6:"author";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:7:"options";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}s:4:"ping";a:4:{s:7:"visible";b:1;s:8:"collapse";b:0;s:11:"htmlbuttons";b:1;s:5:"width";s:4:"100%";}}s:8:"seo_lite";a:3:{s:24:"seo_lite__seo_lite_title";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:27:"seo_lite__seo_lite_keywords";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}s:30:"seo_lite__seo_lite_description";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}s:15:"nsm_pp_workflow";a:1:{s:32:"nsm_pp_workflow__nsm_pp_workflow";a:4:{s:7:"visible";s:4:"true";s:8:"collapse";s:5:"false";s:11:"htmlbuttons";s:5:"false";s:5:"width";s:4:"100%";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_low_events`
--

CREATE TABLE IF NOT EXISTS `exp_low_events` (
  `event_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `entry_id` int(10) unsigned NOT NULL,
  `field_id` int(6) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `all_day` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`event_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_low_events`
--

INSERT INTO `exp_low_events` (`event_id`, `site_id`, `entry_id`, `field_id`, `start_date`, `start_time`, `end_date`, `end_time`, `all_day`) VALUES
(1, 1, 1, 33, '2012-09-21', '17:30:00', '2012-09-21', '18:30:00', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_cols`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_matrix_data`
--

CREATE TABLE IF NOT EXISTS `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `row_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_members`
--

CREATE TABLE IF NOT EXISTS `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `exp_members`
--

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`) VALUES
(1, 1, 'admin', 'Administrator', '64e320695e63747956853c5d68c8bb7f392281f3ca1d58d6c134460b55eb862f7b2bc80d33135f33e8a2d651c951cef213592acbd61aaa8356c68952bae62294', 'e`V3h?d*c|_y)7ihu;}ju;Hq(,mP[X0Vl7h?>~/,)=gsi9RaE%4c+-e?t%B[L6`0UP6e"^5@G^uNSd3{psJD0v;N}o0~FhNfl+Yn3,Fz[Ly%ZKl!@Ye''yat<G$T{K@W]', '8a5ca7a4428eacc18fc10ed403e10635a379b2bf', '6d21c7eb6207056521a9d5ac83d4ad7475498a46', NULL, 'alex@e-digitalgroup.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '80.239.243.112', 1345582335, 1350002779, 1350600177, 3, 0, 0, 0, 1349988506, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', '', 'NavEE|index.php?S=f8802fafbb0b7ed45cb030dbf0f9e9f4b492b928&amp;D=cp&amp;C=addons_modules&M=show_module_cp&module=navee|1\nView Members|index.php?S=1936234eb3b463d3903a66654e1b2f2dc9ed88bb&amp;D=cp&amp;C=members&M=view_all_members|2\nNew Entry: Hero|index.php?S=3361dd1d9f3426d74a38c492e6b99d4fc8e1a96f&amp;D=cp&amp;C=content_publish&M=entry_form&channel_id=2|3', 'y', 0, 'y', 0),
(2, 6, 'manager', 'General Manager', 'bc2a6799509e6d9319fbe2a9bfbc6644d0e479c35c7a01f4e2c8a1f88ccfed77be2e4bd577d08b672fe3a410c05f60428c58387a15d59e83f250a95c6ffb0d0b', 'q~hbzN`jv%~s5E:mYOJ''kPdWiHkV6''.PEg=D+0O3w@!-<jL^p<mqgj,GSH,\\~;!hz%M''I?#*MDwhMir&h4lRCPkVbG.9"Q+^y`^+]gbS]K18N/;M2jwC06U/d4XTS}n_', '0ab575063776fa9a7c98f994b1a852849f1a64d8', '89170e40425a95bfb986a60e94c7864730a1a51a', NULL, 'manager@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '86.164.24.131', 1346764557, 1346964329, 1347046866, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'y', 0, 'y', 0),
(3, 6, 'stuartw', 'Stuart Wallock', 'a94acdcaafd7aba83c1515922d0996ffba8f42d5b8edccd07212702b96c0b7298737eaf536d6f48ff10a2718d5de491b486a7bf208197e18f4c605e1837ce535', 'Ww5Xxvw2NJBG3m?N~$v;VzD@0Uq4&"sovkHXWk^@[)<x5\\JZW%et6~&h~c6D)}sO6T!Rd`qm2<@irWPOlliZ#b7NqEBjC*R._U[;Vh4wq2#^''EP1I4Zebk/1}I,[>[lb', '95cc8921861795e780c2c77a48d66ef2f50c8cf4', 'a9d1a2e0a5acfb3f8897cb1107764b1bae202cb8', NULL, 'stuart@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348068960, 1349114597, 1349150899, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(4, 6, 'nayladp', 'Nayla Della Penna', '412e3675a4c1030f4f00ca40dba9d656837c97d419fa567f21e884f77068c3462c97ecd3e56926decbf981a7ae0538a1bb0c71cf8be2f8ca6a550e9d6b0bdd46', 'z`VpN>R&SiE0a1]F4_rO)]u=qpp[GlMrk6#GxGHH#Z-?Vi?cJw[(7S{SFJX\\XTehu*`geRQezBk}2{x\\uIkEYpNL9kiBBY2z7&hV?cN1If0=&{r!E*gY5BM+?weXV"Sy', '9c79715ae643ed0ffa9271f2d97737c9afb8fed9', '6c65dc13c9878f0ad27bb10d6d2f041e76bcb2be', NULL, 'ndelpen@nyc.rr.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348069070, 1349189166, 1349190903, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(5, 6, 'axelz', 'Axel Zohman', 'b806298a50106195d39db32949cee77fd4a5eb925d9330d8f4a7e1be1780cf3c74a8126ed2561ec3b5dadf51683abc2051b8d9b67f88231ae4f2acafdb935678', 'sM}>^E.jFrafi/e$LRCE2BpW''c@''}w]YW&]QmOT@^s,az9prOv-q)8xgnEmeesxkL?]@_C~`#(lyIn+7U82B2:^5RW/rta"{`=izh@jg\\hL-Kp0Ac%2~Df5(z,szy9HM', '177a1c0db85cd4da44ce026a6b9bf45db376bd64', 'ba3ac3eed2d62b8eda7e759a3ab9674693ab97ec', NULL, 'axel@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348069123, 1349123352, 1349123352, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(6, 6, 'chew', 'Che Wilcox', '525c82b34012818639c2e869a4d884df1a3115c2', '', 'd5b4155b8c8104fc84fac103b327fc9d12d4c61e', NULL, NULL, 'che@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348070946, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0),
(7, 6, 'craigb', 'Craig Buxton', 'ab3a5066d166a14321acecedfd8206f37d87981ead9358eda9aa859eef4f8c3ea1f2c3b032d4e24071d2fa3e05cc29127711dc3d9dacfc61b6b47a065253a9c9', 'K:~o"/!Q=3:4:ST!jqr(qHTRm9UidhdHtrR8~i,=7O/K$>H`FZAS??44CSZ&VsJtItEg;9m-T+Y1\\l+(''GckC2iqiyBzLD8!Jvq2!S%7G.<T}]-+}o#F!o`0:3?|St{#', '645282baad77c5639fcffaece6155e4dcc0de75e', '6e6189aa4031d3411d14338955424df5af1bf7fa', NULL, 'Craig@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348070997, 1349911931, 1349985684, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'y', 0, 'y', 0),
(8, 6, 'royh', 'Roy Hughes', '77ab6e29a92db518e0182addee4ec2e610bae64de686483e589720f2ae21595e56b37ffcbc569c7b5d50e197f33765272d9cc6a2576a9b7c445a93c5b66953fb', 'fNrVA)\\Xf`82GS$pSz]P*>CLED9(VELcHff.1GOe{Pkj)988?"!R4&Bw_O''N/&#VK#72d8ftmg\\1]9ee=pphu4Em0]3^saWTX,]''WGsRbRK7&TzAO*zaJ4yMGgT@h7tp', '6d99e8944426f8139e27763e9c96a9e7b3d8c9cd', '5b31ea61a57a40fe9d9f2c638560bdc1f0648a38', NULL, 'roy@skyventure.com', '', '', '', '', NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'y', 0, 0, '97.79.130.126', 1348864980, 1349933753, 1349933753, 0, 0, 0, 0, 0, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UM6', 'y', 'n', 'us', NULL, NULL, NULL, NULL, '20', NULL, '18', NULL, NULL, 'n', 0, 'y', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_bulletin_board`
--

CREATE TABLE IF NOT EXISTS `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_data`
--

CREATE TABLE IF NOT EXISTS `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_data`
--

INSERT INTO `exp_member_data` (`member_id`) VALUES
(1),
(2),
(3),
(4),
(5),
(6),
(7),
(8);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_fields`
--

CREATE TABLE IF NOT EXISTS `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_groups`
--

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`) VALUES
(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y'),
(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n'),
(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y'),
(6, 1, 'Global Manager', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'y', 'y', 'n', 'n', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_homepage`
--

CREATE TABLE IF NOT EXISTS `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_member_homepage`
--

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`) VALUES
(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0),
(2, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(3, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(4, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(5, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(6, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(7, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0),
(8, 'l', 0, 'l', 0, 'n', 0, 'r', 0, 'n', 0, 'r', 0, 'r', 0, 'n', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exp_member_search`
--

CREATE TABLE IF NOT EXISTS `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_attachments`
--

CREATE TABLE IF NOT EXISTS `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_copies`
--

CREATE TABLE IF NOT EXISTS `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_data`
--

CREATE TABLE IF NOT EXISTS `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_folders`
--

CREATE TABLE IF NOT EXISTS `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_message_folders`
--

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`) VALUES
(1, 'InBox', 'Sent', '', '', '', '', '', '', '', ''),
(7, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_message_listed`
--

CREATE TABLE IF NOT EXISTS `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_modules`
--

CREATE TABLE IF NOT EXISTS `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `exp_modules`
--

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`) VALUES
(1, 'Comment', '2.3', 'y', 'n'),
(2, 'Email', '2.0', 'n', 'n'),
(3, 'Emoticon', '2.0', 'n', 'n'),
(4, 'Jquery', '1.0', 'n', 'n'),
(5, 'Rss', '2.0', 'n', 'n'),
(6, 'Safecracker', '2.1', 'y', 'n'),
(7, 'Search', '2.2', 'n', 'n'),
(8, 'Channel', '2.0.1', 'n', 'n'),
(9, 'Member', '2.1', 'n', 'n'),
(10, 'Stats', '2.0', 'n', 'n'),
(11, 'Rte', '1.0', 'y', 'n'),
(12, 'Mx_google_map', '1.4', 'y', 'n'),
(13, 'Navee', '2.2.5', 'y', 'n'),
(14, 'Playa', '4.3.3', 'n', 'n'),
(15, 'Seo_lite', '1.3.6', 'y', 'y'),
(16, 'Videoplayer', '3.1.1', 'y', 'n'),
(17, 'Cp_analytics', '2.0.7', 'y', 'n'),
(18, 'Low_events', '1.0.4', 'n', 'n'),
(19, 'Nsm_pp_workflow', '0.10.2', 'y', 'y'),
(20, 'Freeform', '4.0.7', 'y', 'n');

-- --------------------------------------------------------

--
-- Table structure for table `exp_module_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_module_member_groups`
--

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`) VALUES
(6, 1),
(6, 6),
(6, 11);

-- --------------------------------------------------------

--
-- Table structure for table `exp_mx_google_map`
--

CREATE TABLE IF NOT EXISTS `exp_mx_google_map` (
  `point_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` varchar(10) NOT NULL DEFAULT '',
  `latitude` varchar(50) NOT NULL DEFAULT '',
  `longitude` varchar(50) NOT NULL DEFAULT '',
  `address` varchar(50) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  `zipcode` varchar(50) NOT NULL DEFAULT '',
  `state` varchar(50) NOT NULL DEFAULT '',
  `field_id` varchar(10) NOT NULL DEFAULT '',
  `icon` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`point_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_mx_google_map_fields`
--

CREATE TABLE IF NOT EXISTS `exp_mx_google_map_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL,
  `field_name` varchar(128) NOT NULL DEFAULT '',
  `field_label` varchar(128) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_maxl` varchar(50) NOT NULL DEFAULT '',
  `field_pattern` varchar(256) NOT NULL DEFAULT '',
  `field_order` int(10) DEFAULT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee`
--

CREATE TABLE IF NOT EXISTS `exp_navee` (
  `navee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `navigation_id` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `template` int(10) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `id` varchar(255) DEFAULT NULL,
  `sort` int(10) DEFAULT NULL,
  `include` tinyint(4) DEFAULT NULL,
  `passive` tinyint(4) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  `rel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `regex` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `custom_kids` varchar(255) DEFAULT NULL,
  `access_key` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`navee_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `exp_navee`
--

INSERT INTO `exp_navee` (`navee_id`, `navigation_id`, `site_id`, `entry_id`, `channel_id`, `template`, `type`, `parent`, `text`, `link`, `class`, `id`, `sort`, `include`, `passive`, `datecreated`, `dateupdated`, `ip_address`, `member_id`, `rel`, `name`, `target`, `title`, `regex`, `custom`, `custom_kids`, `access_key`) VALUES
(1, 1, 1, 0, 0, 1, 'manual', 0, 'What is iFly?', '/what-is-ifly/', '', '', 1, 1, 0, '2012-10-11 12:32:04', '2012-10-11 12:32:04', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(2, 1, 1, 0, 0, 1, 'manual', 1, 'How it Works', '/what-is-ifly/how-it-works', '', '', 1, 1, 0, '2012-10-11 12:34:37', '2012-10-11 12:34:37', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(3, 1, 1, 0, 0, 1, 'manual', 1, 'About iFly', '/what-is-ifly/about-ifly', '', '', 2, 1, 0, '2012-10-11 12:36:06', '2012-10-11 12:36:06', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(4, 1, 1, 0, 0, 1, 'manual', 1, 'Video Testimonials', '/what-is-ifly/video-testimonials', '', '', 3, 1, 0, '2012-10-11 12:36:30', '2012-10-11 12:36:30', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(5, 1, 1, 0, 0, 1, 'manual', 1, 'Ratings & Reviews', '/what-is-ifly/ratings-and-reviews', '', '', 4, 1, 0, '2012-10-11 12:36:56', '2012-10-11 12:36:56', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(6, 1, 1, 0, 0, 1, 'manual', 0, 'Shop Now', '/shop-now/', '', '', 2, 1, 0, '2012-10-11 12:40:17', '2012-10-11 12:40:17', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(7, 1, 1, 0, 0, 1, 'manual', 0, 'Pics & Videos', '/pics-and-videos/', '', '', 3, 1, 0, '2012-10-11 12:40:40', '2012-10-11 12:40:40', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(8, 1, 1, 0, 0, 1, 'manual', 7, 'Find your photos & videos', '/pics-and-videos/find-your-photos-and-videos', '', '', 1, 1, 0, '2012-10-11 12:41:12', '2012-10-11 12:41:12', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(9, 1, 1, 0, 0, 1, 'manual', 7, 'iFly Photos & Videos', '/pics-and-videos/ifly-photos-and-videos', '', '', 2, 1, 0, '2012-10-11 12:41:42', '2012-10-11 12:41:42', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(10, 1, 1, 0, 0, 1, 'manual', 0, 'Find a Tunnel', '/find-a-tunnel/', '', '', 4, 1, 0, '2012-10-11 12:42:05', '2012-10-11 12:42:05', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(11, 2, 1, 0, 0, 1, 'manual', 0, 'What is iFly?', '/what-is-ifly/', '', '', 1, 1, 0, '2012-10-11 12:43:17', '2012-10-11 12:43:17', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(12, 2, 1, 0, 0, 1, 'manual', 11, 'About iFly', '/what-is-ifly/about-ifly', '', '', 2, 1, 0, '2012-10-11 12:43:34', '2012-10-11 12:43:34', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(13, 2, 1, 0, 0, 1, 'manual', 11, 'How it Works', '/what-is-ifly/how-it-works', '', '', 1, 1, 0, '2012-10-11 12:43:54', '2012-10-11 12:43:54', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(14, 2, 1, 0, 0, 1, 'manual', 11, 'Video Testimonials', '/what-is-ifly/video-testimonials', '', '', 3, 1, 0, '2012-10-11 12:44:20', '2012-10-11 12:44:20', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(15, 2, 1, 0, 0, 1, 'manual', 11, 'Ratings & Reviews', '/what-is-ifly/ratings-and-reviews', '', '', 4, 1, 0, '2012-10-11 12:44:39', '2012-10-11 12:44:39', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(16, 2, 1, 0, 0, 1, 'manual', 0, 'Shop Now', '/shop-now/', '', '', 2, 1, 0, '2012-10-11 12:44:55', '2012-10-11 12:44:55', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(17, 2, 1, 0, 0, 1, 'manual', 16, 'Overview', '/shop-now/overview', '', '', 1, 1, 0, '2012-10-11 12:45:14', '2012-10-11 12:45:14', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(18, 2, 1, 0, 0, 1, 'manual', 16, 'First Time Flyers', '/shop-now/first-time-flyers', '', '', 2, 1, 0, '2012-10-11 12:45:34', '2012-10-11 12:45:34', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(19, 2, 1, 0, 0, 1, 'manual', 16, 'Return Flyers', '/shop-now/return-flyers', '', '', 3, 1, 0, '2012-10-11 12:45:57', '2012-10-11 12:45:57', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(20, 2, 1, 0, 0, 1, 'manual', 16, 'Experienced Flyers', '/shop-now/experienced-flyers', '', '', 4, 1, 0, '2012-10-11 12:46:25', '2012-10-11 12:46:25', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(21, 2, 1, 0, 0, 1, 'manual', 16, 'Group or Party', '/shop-now/group-or-party', '', '', 5, 1, 0, '2012-10-11 12:46:52', '2012-10-11 12:46:52', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(22, 2, 1, 0, 0, 1, 'manual', 16, 'Buy a Gift Card', '/shop-now/buy-a-gift-card', '', '', 6, 1, 0, '2012-10-11 12:47:19', '2012-10-11 12:47:19', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(23, 2, 1, 0, 0, 1, 'manual', 16, 'Redeem a Gift Card', '/shop-now/redeem-a-gift-card', '', '', 7, 1, 0, '2012-10-11 12:47:48', '2012-10-11 12:47:48', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(24, 2, 1, 0, 0, 1, 'manual', 0, 'Pics & Videos', '/pics-and-videos/', '', '', 3, 1, 0, '2012-10-11 12:48:01', '2012-10-11 12:48:01', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(25, 2, 1, 0, 0, 1, 'manual', 24, 'Find your photos & videos', '/pics-and-videos/find-your-photos-and-videos', '', '', 1, 1, 0, '2012-10-11 12:48:21', '2012-10-11 12:48:21', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(26, 2, 1, 0, 0, 1, 'manual', 24, 'iFly Photos & Videos', '/pics-and-videos/ifly-photos-and-videos', '', '', 2, 1, 0, '2012-10-11 12:48:38', '2012-10-11 12:48:38', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(27, 2, 1, 0, 0, 1, 'manual', 0, 'My iFly', '/my-ifly/', '', '', 4, 1, 0, '2012-10-11 12:49:36', '2012-10-11 12:49:36', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(28, 2, 1, 0, 0, 1, 'manual', 27, 'My iFly Page', '/my-ifly/my-ifly-page', '', '', 1, 1, 0, '2012-10-11 12:50:01', '2012-10-11 12:50:01', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(29, 2, 1, 0, 0, 1, 'manual', 27, 'My Media', '/my-ifly/my-media', '', '', 2, 1, 0, '2012-10-11 12:50:30', '2012-10-11 12:50:30', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(30, 2, 1, 0, 0, 1, 'manual', 27, 'My Profile', '/my-ifly/my-profile', '', '', 3, 1, 0, '2012-10-11 12:50:52', '2012-10-11 12:50:52', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(31, 2, 1, 0, 0, 1, 'manual', 0, 'I.B.A', 'http://iba.com', '', '', 5, 1, 0, '2012-10-11 12:52:14', '2012-10-11 12:52:14', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(32, 2, 1, 0, 0, 1, 'manual', 31, 'About the Sport', 'http://iba.com/about', '', '', 1, 1, 0, '2012-10-11 12:52:35', '2012-10-11 12:52:35', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(33, 2, 1, 0, 0, 1, 'manual', 27, 'About the I.B.A', 'http://iba.com/about', '', '', 4, 1, 0, '2012-10-11 12:53:00', '2012-10-11 12:53:00', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(34, 2, 1, 0, 0, 1, 'manual', 0, 'Legal', '/legal/', '', '', 6, 1, 0, '2012-10-11 12:53:11', '2012-10-11 12:53:11', '173.198.122.173', 1, '', '', '', '', '', '', '', ''),
(35, 2, 1, 0, 0, 1, 'manual', 34, 'Waiver', '/legal/waiver', '', '', 1, 1, 0, '2012-10-11 12:53:33', '2012-10-11 12:53:33', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(36, 2, 1, 0, 0, 1, 'manual', 34, 'Terms', '/legal/terms', '', '', 2, 1, 0, '2012-10-11 12:53:54', '2012-10-11 12:53:54', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, ''),
(37, 2, 1, 0, 0, 1, 'manual', 34, 'Privacy', '/legal/privacy', '', '', 3, 1, 0, '2012-10-11 12:54:19', '2012-10-11 12:54:19', '173.198.122.173', 1, '', '', '', '', NULL, '', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_cache`
--

CREATE TABLE IF NOT EXISTS `exp_navee_cache` (
  `navee_cache_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navigation_id` int(10) DEFAULT NULL,
  `group_id` smallint(4) DEFAULT NULL,
  `parent` int(10) DEFAULT NULL,
  `recursive` tinyint(4) DEFAULT NULL,
  `ignore_include` tinyint(4) DEFAULT NULL,
  `start_from_parent` tinyint(4) DEFAULT NULL,
  `start_from_kid` tinyint(4) DEFAULT NULL,
  `single_parent` tinyint(4) DEFAULT NULL,
  `cache` longtext,
  PRIMARY KEY (`navee_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_config`
--

CREATE TABLE IF NOT EXISTS `exp_navee_config` (
  `navee_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `k` varchar(255) DEFAULT NULL,
  `v` text,
  PRIMARY KEY (`navee_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_members`
--

CREATE TABLE IF NOT EXISTS `exp_navee_members` (
  `navee_mem_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `navee_id` int(10) DEFAULT NULL,
  `members` text,
  PRIMARY KEY (`navee_mem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_navee_navs`
--

CREATE TABLE IF NOT EXISTS `exp_navee_navs` (
  `navigation_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) DEFAULT NULL,
  `nav_title` varchar(255) DEFAULT NULL,
  `nav_name` varchar(255) DEFAULT NULL,
  `nav_description` varchar(255) DEFAULT NULL,
  `datecreated` datetime DEFAULT NULL,
  `dateupdated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `member_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`navigation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_navee_navs`
--

INSERT INTO `exp_navee_navs` (`navigation_id`, `site_id`, `nav_title`, `nav_name`, `nav_description`, `datecreated`, `dateupdated`, `ip_address`, `member_id`) VALUES
(1, 1, 'global_nav_header', 'Global Header Navigation', 'Navigation for global site', '2012-10-11 12:29:18', '2012-10-11 12:55:53', '173.198.122.173', 1),
(2, 1, 'global_nav_footer', 'Global Footer Navigation', 'Global Footer Navigation', '2012-10-11 12:42:49', '2012-10-11 12:42:49', '173.198.122.173', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_nsm_addon_settings`
--

CREATE TABLE IF NOT EXISTS `exp_nsm_addon_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) unsigned NOT NULL DEFAULT '1',
  `addon_id` varchar(255) NOT NULL,
  `settings` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_nsm_addon_settings`
--

INSERT INTO `exp_nsm_addon_settings` (`id`, `site_id`, `addon_id`, `settings`) VALUES
(1, 1, 'nsm_pp_workflow', '{"enabled":"1","channels":{"9":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"8":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"4":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"5":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"2":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"7":{"enabled":"1","next_review":"60","email_author":"1","recipients":"","state":"pending"},"3":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"6":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"},"1":{"enabled":"0","next_review":"60","email_author":"0","recipients":"","state":"pending"}},"notifications":{"from_name":"","from_email":"","subject":"","message":""}}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_nsm_pp_workflow_entries`
--

CREATE TABLE IF NOT EXISTS `exp_nsm_pp_workflow_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) DEFAULT NULL,
  `channel_id` int(10) DEFAULT NULL,
  `entry_state` varchar(10) DEFAULT NULL,
  `last_review_date` int(10) DEFAULT NULL,
  `next_review_date` int(10) DEFAULT NULL,
  `site_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_nsm_pp_workflow_entries`
--

INSERT INTO `exp_nsm_pp_workflow_entries` (`id`, `entry_id`, `channel_id`, `entry_state`, `last_review_date`, `next_review_date`, `site_id`) VALUES
(1, 3, 7, 'pending', 0, 1355172482, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exp_online_users`
--

CREATE TABLE IF NOT EXISTS `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `exp_online_users`
--

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`) VALUES
(40, 1, 0, 'n', '', '150.70.75.32', 1349462890, ''),
(51, 1, 0, 'n', '', '150.70.75.32', 1349462890, ''),
(52, 1, 0, 'n', '', '70.123.138.247', 1349576353, ''),
(53, 1, 0, 'n', '', '70.123.138.247', 1349653843, ''),
(54, 1, 0, 'n', '', '70.123.138.247', 1349721979, ''),
(55, 1, 0, 'n', '', '209.163.205.90', 1349788971, ''),
(56, 1, 0, 'n', '', '209.163.205.90', 1349872363, ''),
(57, 1, 0, 'n', '', '209.163.205.90', 1349885796, ''),
(58, 1, 0, 'n', '', '98.149.249.30', 1349900211, ''),
(59, 1, 0, 'n', '', '98.149.249.30', 1349907242, ''),
(61, 1, 0, 'n', '', '98.149.249.30', 1349912383, ''),
(62, 1, 0, 'n', '', '80.236.63.238', 1349950353, ''),
(63, 1, 0, 'n', '', '209.163.205.90', 1349959967, ''),
(64, 1, 0, 'n', '', '173.198.122.173', 1349979902, ''),
(65, 1, 0, 'n', '', '50.84.124.94', 1349982091, ''),
(66, 1, 0, 'n', '', '173.198.122.173', 1349983620, ''),
(67, 1, 0, 'n', '', '98.149.249.30', 1349987991, ''),
(69, 1, 0, 'n', '', '98.149.249.30', 1349992253, ''),
(70, 1, 0, 'n', '', '98.149.249.30', 1349998397, ''),
(71, 1, 0, 'n', '', '70.123.138.247', 1350046515, ''),
(72, 1, 0, 'n', '', '98.149.249.30', 1350062317, ''),
(73, 1, 0, 'n', '', '76.79.113.21', 1350254498, ''),
(74, 1, 0, 'n', '', '97.79.130.126', 1350414393, ''),
(75, 1, 0, 'n', '', '97.79.130.126', 1350425615, ''),
(76, 1, 0, 'n', '', '76.253.76.206', 1350446767, ''),
(77, 1, 0, 'n', '', '108.13.107.180', 1350448452, ''),
(78, 1, 0, 'n', '', '76.253.76.206', 1350456723, ''),
(79, 1, 0, 'n', '', '97.79.130.126', 1350482932, ''),
(80, 1, 0, 'n', '', '97.79.130.126', 1350491427, '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_password_lockout`
--

CREATE TABLE IF NOT EXISTS `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `exp_password_lockout`
--

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`) VALUES
(12, 1349799423, '97.79.130.126', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.53.11 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10', 'craigb'),
(13, 1349933729, '24.153.178.154', 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)', 'royh'),
(14, 1349950422, '80.236.63.238', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4', 'iflyworlddev');

-- --------------------------------------------------------

--
-- Table structure for table `exp_ping_servers`
--

CREATE TABLE IF NOT EXISTS `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_playa_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_relationships`
--

CREATE TABLE IF NOT EXISTS `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_remember_me`
--

CREATE TABLE IF NOT EXISTS `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_reset_password`
--

CREATE TABLE IF NOT EXISTS `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_reset_password`
--

INSERT INTO `exp_reset_password` (`reset_id`, `member_id`, `resetcode`, `date`) VALUES
(2, 1, 'Z4U5jiZy', 1346761428);

-- --------------------------------------------------------

--
-- Table structure for table `exp_revision_tracker`
--

CREATE TABLE IF NOT EXISTS `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_tools`
--

CREATE TABLE IF NOT EXISTS `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exp_rte_tools`
--

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`) VALUES
(1, 'Blockquote', 'Blockquote_rte', 'y'),
(2, 'Bold', 'Bold_rte', 'y'),
(3, 'Headings', 'Headings_rte', 'y'),
(4, 'Image', 'Image_rte', 'y'),
(5, 'Italic', 'Italic_rte', 'y'),
(6, 'Link', 'Link_rte', 'y'),
(7, 'Ordered List', 'Ordered_list_rte', 'y'),
(8, 'Underline', 'Underline_rte', 'y'),
(9, 'Unordered List', 'Unordered_list_rte', 'y'),
(10, 'View Source', 'View_source_rte', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_rte_toolsets`
--

CREATE TABLE IF NOT EXISTS `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_rte_toolsets`
--

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`) VALUES
(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_search`
--

CREATE TABLE IF NOT EXISTS `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_search_log`
--

CREATE TABLE IF NOT EXISTS `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_security_hashes`
--

CREATE TABLE IF NOT EXISTS `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2125 ;

--
-- Dumping data for table `exp_security_hashes`
--

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `ip_address`, `hash`) VALUES
(1786, 1349933714, '24.153.178.154', 'a2db3ab1e2cfd6031c5fc42e47d70262eae5ca0f'),
(1787, 1349933728, '24.153.178.154', 'ee7a370228610247c1179f4c46d46e5755541d8f'),
(1788, 1349933730, '24.153.178.154', 'd7d43d75051ae91d6499558e0ade91d32bc13a3a'),
(1789, 1349933752, '24.153.178.154', 'b125b0978e3b8cddfa587b7ffb8ed4adbf295476'),
(1790, 1349933753, '24.153.178.154', '8c7d1f79d7a95579457bcf1ddc2a126446af3f55'),
(1791, 1349933814, '24.153.178.154', 'd12bf3aba78a95afabcfe4da02d35a88eccccf37'),
(1792, 1349933816, '24.153.178.154', 'f074858d42640c2037313a7d1e570b1bd166181a'),
(1793, 1349933817, '24.153.178.154', 'f1f0b9c95a546302305345fcce715ab0079503e3'),
(1794, 1349933817, '24.153.178.154', 'abfadd167ce7cecfa170279abdc0cc411a73bc45'),
(1795, 1349933830, '24.153.178.154', '67e8e47c60b21e0e6aa3457c837fc36717723c19'),
(1796, 1349933847, '24.153.178.154', '48bd5fe00fcb5c81617bae1047103a99061a525f'),
(1797, 1349933850, '24.153.178.154', '2a4fbab820583a2453d9ba4b4615731354d22e66'),
(1798, 1349933869, '24.153.178.154', '059e4009915149b4bb66cf31371778ec33c05ed8'),
(1799, 1349933869, '24.153.178.154', '29fe6bea4475b545d827fee7f12f029c2a7007e2'),
(1800, 1349933872, '24.153.178.154', '0083a3d3e3c82895599c0b8492e2e76cb71ce987'),
(1801, 1349950379, '80.236.63.238', '187ad5888eda773f357a7c63efd8ab373a32450d'),
(1802, 1349950422, '80.236.63.238', '9792228b00d59d95dcf7742c97778483ec8c967e'),
(1803, 1349950423, '80.236.63.238', '894346cce45396b308e9b1f882738b3d02dd7b93'),
(1804, 1349979897, '173.198.122.173', '07543a9351bea0fd49b7269edb5accd3798d143b'),
(1805, 1349979906, '173.198.122.173', 'c47e34110553d264879449481be810d1b36ff65c'),
(1806, 1349979907, '173.198.122.173', 'a6be10464cae99bad822772be1a28d32dedf0cf0'),
(1807, 1349979944, '173.198.122.173', 'a35a6d7803531d0509bf48dd9dcbf8dfe93d7265'),
(1808, 1349980045, '173.198.122.173', '69187547b9d1dae3e050d9a3455c312a429d614d'),
(1809, 1349980057, '173.198.122.173', 'd279ddb59f69585fc636127d823ce8e3dc15699b'),
(1810, 1349980058, '173.198.122.173', '61af6db0408765ed9a5756fc836e9fc7fd9995c4'),
(1811, 1349980078, '173.198.122.173', '7900272e6ea58d559b45ffdad2d7104b80ad99ce'),
(1812, 1349980083, '173.198.122.173', '450d76e68ed9eedf29d1a1acfcd9ed5c594878a1'),
(1813, 1349980123, '173.198.122.173', 'c344c6c78bc0918ed15a38de3bd7630f754dc1c2'),
(1814, 1349980125, '173.198.122.173', 'e047da27dc5d40e927defbd853ec3468177f11c4'),
(1815, 1349980158, '173.198.122.173', '0edd48a17e82c3e8141789da0a74ad64ce204f63'),
(1816, 1349980160, '173.198.122.173', '98c831ab09c302a8f1de1bc355a2f800860cbfc0'),
(1817, 1349980324, '173.198.122.173', 'cdc93c5d28f9fc00fc57e07a5527e4655017bf72'),
(1818, 1349980325, '173.198.122.173', '93cddb59221d621d95579029b674dc0db8a2d5e4'),
(1819, 1349980477, '173.198.122.173', '6b84d728d7dd665efcb9bb96f53552cfa118afaf'),
(1820, 1349980478, '173.198.122.173', '4076954108754ed374c3681a9492c36744d45e74'),
(1821, 1349980566, '173.198.122.173', 'acb9e28cdc61de503158993beefd035674755a11'),
(1822, 1349980567, '173.198.122.173', '850b2999dbad7e24daf42bd584cd250c7e1a2978'),
(1823, 1349980590, '173.198.122.173', '36151567d51db4256563bc1c57c3e64f67b66338'),
(1824, 1349980592, '173.198.122.173', '284748b0474f7adecaa624494eb14f8a48739970'),
(1825, 1349980616, '173.198.122.173', 'f704231274aefffd217b42a7c8b0fc891a1a987b'),
(1826, 1349980617, '173.198.122.173', '1d76b96a0153b8791c220012c7f2f99270577f70'),
(1827, 1349980626, '173.198.122.173', 'c219d4da869db634ea942fa09d5755974c7b79b6'),
(1828, 1349980633, '173.198.122.173', '776781dc49467773ace4b79f8fdffa697225e456'),
(1829, 1349980638, '173.198.122.173', 'e9f3087d71a81a2c99551bf97eea660b1f497ab6'),
(1830, 1349980641, '173.198.122.173', '693a4060a57628e4efe53665d77d656b760f97d6'),
(1831, 1349980666, '173.198.122.173', '53c7615c340302d0cf2cdb1d181b56cd221fadc7'),
(1832, 1349980791, '173.198.122.173', 'be2841526d4c967762a46e1001952bc088da3741'),
(1833, 1349980795, '173.198.122.173', 'eefb64ed9482206f79be740dea3e1f7fdf36724b'),
(1834, 1349980799, '173.198.122.173', 'c6481991ca70fddd56ad6aaefd5d5bfe54317e68'),
(1835, 1349980817, '173.198.122.173', 'ba3abbb3f68ca6b1a665d8e0994d2265e01600cf'),
(1836, 1349980818, '173.198.122.173', '0b2d1c14c085f6cc41d1897cb576219e117811d6'),
(1837, 1349980840, '173.198.122.173', 'ceb3cea5523fdb7a3b0763afecd86d2a807c4b60'),
(1838, 1349980841, '173.198.122.173', '6a9130f572d757969da06ab8fcd9157bb79f5be1'),
(1839, 1349980872, '173.198.122.173', 'e8830e2931d766525e36cc58c8ce912de310563c'),
(1840, 1349980874, '173.198.122.173', '0fe1c030b660180232c6e5894ff85756830ad682'),
(1841, 1349980902, '173.198.122.173', '58e3bf2e7f50d5c69c15252c017f5d4a8495b881'),
(1842, 1349980903, '173.198.122.173', '50fb0a952d885b34210bd5beec4b1fc36ed1870b'),
(1843, 1349980925, '173.198.122.173', 'c2b200f6aaaf9ea766677ae68c1e229d97313511'),
(1844, 1349980926, '173.198.122.173', 'ce336e18fa020ae7a3dae4baf368e146a132fce1'),
(1845, 1349980936, '173.198.122.173', 'c5e973bd5ac9f4378b619c22357eae040d46cf13'),
(1846, 1349980969, '173.198.122.173', '534193ed578e3d74627b246c62ec4de8ce7f45c4'),
(1847, 1349980971, '173.198.122.173', 'b3056fe70b339dcd3537241838a9fbf27340ee11'),
(1848, 1349980997, '173.198.122.173', '9996f63fff75d373a7fa792938f1e4085b635fb6'),
(1849, 1349980999, '173.198.122.173', '18d39c7411bd15efcaa95418cf26b568389c19cc'),
(1850, 1349981014, '173.198.122.173', 'c84fb0baa4152aebb6511f308a18f5bc26c0b802'),
(1851, 1349981015, '173.198.122.173', '283909755bfda5c4b45c5b7c11a4ff5e8e440b71'),
(1852, 1349981034, '173.198.122.173', '269c9a7693a6a42c74ef6074c3dc0084524a9bc4'),
(1853, 1349981036, '173.198.122.173', '85c521efae9dfac8ba734beccb4dbcf16b9a375c'),
(1854, 1349981043, '173.198.122.173', '8ac71aced112ae687eb6921f1db5bf76b195b83a'),
(1855, 1349981060, '173.198.122.173', '955fc6f84fd25c0b39b0d087d3a626df9d256dce'),
(1856, 1349981061, '173.198.122.173', '3c59811d7074367c7dacb93fe8681a9ae9bc0742'),
(1857, 1349981079, '173.198.122.173', '7b5401de9e4dbe7563571fae87fcefb4f2ccbfad'),
(1858, 1349981081, '173.198.122.173', '31b7f013eba0b20581efd79b93069fefaa953875'),
(1859, 1349981095, '173.198.122.173', '91b0ac5ceabfbd9e1d18e7f371650c44d0436fa5'),
(1860, 1349981096, '173.198.122.173', '1040593055738dd004ca9f4734bde2cec94e7ecd'),
(1861, 1349981114, '173.198.122.173', 'ad2b6d805abb1c2a262924b172d28a67ba6356ee'),
(1862, 1349981116, '173.198.122.173', '5f206c05e3fda8a983ab467f40b0f2df3bb15ed2'),
(1863, 1349981134, '173.198.122.173', 'b02d21af90186f55d97dcb794ac26d8208729dc6'),
(1864, 1349981135, '173.198.122.173', '9cb6889b0cd4512f15817a6cebdada8c716529d8'),
(1865, 1349981157, '173.198.122.173', '04e76c2bd9351c5dd8c7558dd37f388fa7f84caa'),
(1866, 1349981159, '173.198.122.173', '9512359ca87808fd7f2026df0a38f8a62b9ced66'),
(1867, 1349981185, '173.198.122.173', '8ce627fd2491be1552f9ceac80f73b85edaf008d'),
(1868, 1349981187, '173.198.122.173', '373107c66945e82a6cb30a76464674fce9e60ec2'),
(1869, 1349981212, '173.198.122.173', '913fca461dc3a784f9895bdb2a5729d2c4d95752'),
(1870, 1349981214, '173.198.122.173', '8c5160d594b5c806ebe2ca2da800a070d5d747b4'),
(1871, 1349981239, '173.198.122.173', 'd2b3c2a5f4322d2e03e38c7baf0fbbe5dddac54a'),
(1872, 1349981241, '173.198.122.173', 'ecaf1cd320d57b4d8ca7c16a178de53309d5d4cb'),
(1873, 1349981268, '173.198.122.173', 'b09e1790899f53da48b453f77ad6f88235450507'),
(1874, 1349981269, '173.198.122.173', 'deb969130760e36682d783ede27cafb5fb79d858'),
(1875, 1349981281, '173.198.122.173', '737b10a781ce2425d9c03451bc9638878a20c0b7'),
(1876, 1349981282, '173.198.122.173', '50b0c1df438101e50a02758bff38a5efb3aa9dd2'),
(1877, 1349981301, '173.198.122.173', '13778ea63b54a5e3321d9c377d740a4cb6b88232'),
(1878, 1349981303, '173.198.122.173', '047465e1e591aa56fe9317894c7ee0bb59622fd4'),
(1879, 1349981318, '173.198.122.173', '7d5cad4f682e4f107e36563084eb490f26442617'),
(1880, 1349981320, '173.198.122.173', 'ef400bfd56095ca377d54c7880b796357b121a3c'),
(1881, 1349981375, '173.198.122.173', '0900a20a22636310830c79a3b6191e80bcf319fa'),
(1882, 1349981377, '173.198.122.173', '4aaf0ddb656b74d29a48de8fb88f4e9fcbdb5992'),
(1883, 1349981401, '173.198.122.173', '002059f958595fcd64be817acde8db0b0c3a3252'),
(1884, 1349981402, '173.198.122.173', 'ffac987b12dc4fdf8dce94d804475b1659d3bb8c'),
(1885, 1349981430, '173.198.122.173', '9eb2719a1e5d937dda372652b49420b51dda58c2'),
(1886, 1349981431, '173.198.122.173', 'df09794045c8c8c589e33eebf7fff80d2a83cd10'),
(1887, 1349981452, '173.198.122.173', '3ee205523448867c71cdf49761b27a1598081ffa'),
(1888, 1349981453, '173.198.122.173', '77b520a5c57917cd0946c6fb2f1b75606dc227d3'),
(1889, 1349981534, '173.198.122.173', '1c0d46580a832803daf93f907ae6124390283db1'),
(1890, 1349981535, '173.198.122.173', 'df4629a2db5dba603742a67b6b93208de1ae9dab'),
(1891, 1349981555, '173.198.122.173', 'bad6ff594934ce1804a3e7358ce9c97595b63e8d'),
(1892, 1349981557, '173.198.122.173', 'ea61d3076e3c21255da71528fe7fc7e85351edd7'),
(1893, 1349981580, '173.198.122.173', '55a31cb88217cf6bede7b0878d219bcc969a47b1'),
(1894, 1349981581, '173.198.122.173', '2382fb0c3a1d930ef78709daee98b1dfab1d1c17'),
(1895, 1349981591, '173.198.122.173', 'a708cf399fed4df56ab487e607663e7f520ac22b'),
(1896, 1349981592, '173.198.122.173', '006c4c17ca843119090cd3a0e1941d6b66672bfb'),
(1897, 1349981613, '173.198.122.173', 'e6dd394628246748027018b51eaf54e5f630fd16'),
(1898, 1349981614, '173.198.122.173', '25e63f660485bd3cea23aebd31894577e3d97f0e'),
(1899, 1349981634, '173.198.122.173', 'd447c2801b8de24119430233abaa67fb931f9626'),
(1900, 1349981635, '173.198.122.173', '4bb24b0f68d59ec4296afc057b19011dd4253ef8'),
(1901, 1349981659, '173.198.122.173', '668c7e42bbb19a431565c98d1f007487e2245719'),
(1902, 1349981661, '173.198.122.173', 'd455ab29b8bb0146f506da7e95e21c80c4dd0d7a'),
(1903, 1349981670, '173.198.122.173', '26425212c98c716c3c3f1a0c14d3bface0bd556b'),
(1904, 1349981685, '173.198.122.173', 'ac31c895b5a21443379e31924f245228d259e7b7'),
(1905, 1349981695, '173.198.122.173', '6e3901e23db782459bacd1ee98310403d9ca2f01'),
(1906, 1349981709, '173.198.122.173', '749a087743a17d3315cb6fd0907ee39a2d859e55'),
(1907, 1349981722, '173.198.122.173', '16bcc9ba5e885299106372c9a0584d18557f0d44'),
(1908, 1349981740, '173.198.122.173', 'a916783b81334323dfe7523e01061044c3d1411e'),
(1909, 1349981752, '173.198.122.173', '0a958a313136e4d69960e34e1e559fc56f1c846f'),
(1910, 1349981755, '173.198.122.173', '5f175fdd9e6c9dd7eef7fba57a30da25e05d8cc9'),
(1911, 1349982246, '173.198.122.173', '3668d6713c55b1ecd3a62d04a6772f41835b735f'),
(1912, 1349982253, '173.198.122.173', 'f291e284268c949b4fdb7f89e1e60892f51d26a6'),
(1913, 1349982588, '173.198.122.173', 'dcca37a44ae62989b9ba0943e98640b01b289f12'),
(1914, 1349982638, '173.198.122.173', '9744cf4fd3e68431715a407681403462ccaa16d8'),
(1915, 1349982740, '173.198.122.173', 'cea05160623b571427dc9007f67d878e78908d4b'),
(1916, 1349982744, '173.198.122.173', 'fd40eb9292fa8f8f0c99f0b8684d4afcf0b3d0f3'),
(1917, 1349982746, '173.198.122.173', '67f2bb8b694b1935c78705e7b0452aafb89c18a2'),
(1918, 1349982747, '173.198.122.173', '0ef494a63104d7c42361fde0e949059d6032d412'),
(1919, 1349982764, '173.198.122.173', '935bd8c9a9781f7e430363d51fef1d4f9f42f1e9'),
(1920, 1349982765, '173.198.122.173', '7a7858d90f1127daf20e8d3e38d1e3aa7b33775f'),
(1921, 1349982767, '173.198.122.173', 'deac6f0dd2b9c22fee8dcc5cdda01fb6d0754ebe'),
(1922, 1349982769, '173.198.122.173', '5aea4f9d4450709994f6726c38ab54b5193d3d99'),
(1923, 1349982776, '173.198.122.173', '125e527580b2c9fb2b2cc73cac5ec8c0353889fc'),
(1924, 1349982778, '173.198.122.173', 'f29494778bb2958b30e8c968a0c687f122b8e847'),
(1925, 1349982779, '173.198.122.173', '9daef0b5e17eb70dd5c2874ec72c79ccb0c7c9b6'),
(1926, 1349982781, '173.198.122.173', '0fbbd42caa822eb89e243f1259be6fd35959dbd5'),
(1927, 1349982791, '173.198.122.173', '5881bcaf7aab19e91995e5681ecb864e9855e9af'),
(1928, 1349982793, '173.198.122.173', 'f683f6571f2d2e66ffc93e327eaed38184e81453'),
(1929, 1349982794, '173.198.122.173', '4dcd5c92e96aaa5edca04195b9542de842037eaf'),
(1930, 1349982796, '173.198.122.173', '99e6c48447386aca87c2180cbe1346cc1bb7df3d'),
(1931, 1349982796, '173.198.122.173', 'a06ccfaecf13350c7c679ef3b154b61c1728ef58'),
(1932, 1349982831, '173.198.122.173', '4ad87209b4998bb51ad4e5a6a4b653b17c12b449'),
(1933, 1349982888, '173.198.122.173', 'c7a096385fee03a85d594e3d2d4a3329c5bbd5a1'),
(1934, 1349982890, '173.198.122.173', '2216d367e16a4ba060008f26ee0eaa29d262fc73'),
(1935, 1349982891, '173.198.122.173', '0c67c7635f129091970969038e4afb18e9c9e95f'),
(1936, 1349982892, '173.198.122.173', '16326215a80641284f0a992372b849de1d277d8d'),
(1937, 1349982905, '173.198.122.173', '9fae92c2ccb8bfabd41fd544b342b978220f2440'),
(1938, 1349982914, '173.198.122.173', '7a7e9c0abe474726ed62f0c9c1648d3b0ea338d1'),
(1939, 1349982919, '173.198.122.173', 'db76219853e9b4241d2f1b397c21d3cadf7046bf'),
(1940, 1349982953, '173.198.122.173', '573efa9edda8738fcfa94ed1ebe6fb79cf03eaab'),
(1941, 1349982955, '173.198.122.173', 'c176a94e628eda685c8b2ecb4237a1ee05c3df82'),
(1942, 1349982960, '173.198.122.173', 'd10eca9daaa0035cecea3abde06099ea5f42efd3'),
(1943, 1349982962, '173.198.122.173', '5f34df06f898397f5d2ffdb31ad821073b2dc0ce'),
(1944, 1349982964, '173.198.122.173', '5cf0ea459b90a736cf99c782110d879e7fead826'),
(1945, 1349982965, '173.198.122.173', 'bfcc75bf5642535e52d5592eda484f323b650656'),
(1946, 1349982972, '173.198.122.173', '10b923683c08a74d7dd441f3165d4ba6caa16c54'),
(1947, 1349983006, '173.198.122.173', 'a2865f05fe55a19c68b728d53a4a3ebd004d154b'),
(1948, 1349983032, '173.198.122.173', '09f2deb00b8a030cafa127451d75ea13c259c39b'),
(1949, 1349983057, '173.198.122.173', '464a6145fe4ace4b0c3ea31166797de659118e5e'),
(1950, 1349983059, '173.198.122.173', 'f8e64d1b38c6a22fa724c28124e16ba954690e90'),
(1951, 1349985282, '97.79.130.126', '965078f090e76ced12e11c4f7521a7872013bc17'),
(1952, 1349985299, '97.79.130.126', 'dcbb38cff6dfd5e3f36d4420c2cae7c5a630254a'),
(1953, 1349985301, '97.79.130.126', '87caef806606b0141d73898d364094492adc6259'),
(1954, 1349985684, '97.79.130.126', 'a4d57f189037f81cf1e9e7a7b226a0a04e81ceda'),
(1955, 1349985688, '97.79.130.126', '0b30b05b00c5207d6c5dada3f43fbe6541013075'),
(1956, 1349985691, '97.79.130.126', 'e34d752bdb4cb718a4ddbb855da0f6722fa1e684'),
(1957, 1349985852, '98.149.249.30', '5eb6bb5576ef08d39df292135c48c9f5ed16abe4'),
(1958, 1349986389, '98.149.249.30', '3f05238a63c4743664e7d6ae15f5d303dac61b1a'),
(1959, 1349986390, '98.149.249.30', 'bde222303ecd86143bbb11cb1f1cb8a85393b31d'),
(1960, 1349986535, '98.149.249.30', 'b714e236b6317946ada041dc202c7d0629fd9c2b'),
(1961, 1349986676, '98.149.249.30', '5e83f2752857069cdcfc2e47a0b4d09ae8344a87'),
(1962, 1349986679, '98.149.249.30', 'd02f4877313d56a68511e34d58a4a1233b99821c'),
(1963, 1349986681, '98.149.249.30', '027664944acc1d1cee69b5b14d5bde892690ee09'),
(1964, 1349986682, '98.149.249.30', '6450dd4fbb3347a76f8c45fbff1b04c7f37a67ae'),
(1965, 1349986688, '98.149.249.30', '52e488d15166c5ac617efb32f258df73c9554533'),
(1966, 1349986691, '98.149.249.30', '130422610f6dc8b6f3fe6a1edf29a4ae789fa389'),
(1967, 1349986692, '98.149.249.30', '56d0bfbc7b815638199ef41ebcc7fe271a9df7ee'),
(1968, 1349986694, '98.149.249.30', '781515b13c9fab23d29ea7bf39944952eba25f66'),
(1969, 1349986802, '98.149.249.30', 'c08640e6aeba6bd7c6fad2f627b9c06aaf09e601'),
(1970, 1349987105, '98.149.249.30', 'c12fe5a666f7aea1e50e49f88a2e41710ffb968a'),
(1971, 1349987110, '98.149.249.30', 'dc15a680ef4f9d434f4ce46bfa8123fec21ad8d6'),
(1972, 1349987122, '98.149.249.30', '2af057f2145af15351a4bd4fa4d2a4fca70f5a54'),
(1973, 1349987131, '98.149.249.30', 'bfd3933cdccf18675e039de329e475a3e9e41119'),
(1974, 1349987139, '98.149.249.30', 'a6e7cbba02a7bbf4432c59fcde870c64b454f0c6'),
(1975, 1349987182, '98.149.249.30', 'd2a54a581e32650e797f7aecd9a4e84e7b4b93ca'),
(1976, 1349987184, '98.149.249.30', '1b0d6252691b1e60b0483f3464443d3786828b9f'),
(1977, 1349987185, '98.149.249.30', 'f724d8c114376319f5114a2af49b4d105c607ea9'),
(1978, 1349987186, '98.149.249.30', '1c4f409e5792c29d9e78a9e880e37fb23c739aed'),
(1979, 1349987261, '98.149.249.30', 'ff86bafd5ca214866868c930f4324ecb15f5c5b3'),
(1980, 1349987330, '98.149.249.30', 'e2cb081ac471fdb1ae3382646778aada05462309'),
(1981, 1349987333, '98.149.249.30', '9c2b13ee1145f1bb6b31a34608d0d1acecfe43ba'),
(1982, 1349987337, '98.149.249.30', '3b0f1b6d397e717cd1b6f5ac10d5e4cbae5e246e'),
(1983, 1349987352, '98.149.249.30', '4941987c799de5d55531f0e2777c8fbf89265849'),
(1984, 1349987364, '98.149.249.30', '1c7c1c8c9625ed81c5c55a00d5783dac08fbe19f'),
(1985, 1349987369, '98.149.249.30', '9cee782415178a63921eaf11313da2767f739cd6'),
(1986, 1349987373, '98.149.249.30', 'd8bee8fd1fb4a6581da585fe4d6b555edc80682f'),
(1987, 1349987407, '98.149.249.30', '49ed95e72187e50c1422dacdf31373a2c15afc3f'),
(1988, 1349987408, '98.149.249.30', '22c89fe02cbf06150a82441ca8961c209457ee15'),
(1989, 1349987410, '98.149.249.30', 'b0070c1887770bd9849133aadcf2a682a95c2c4d'),
(1990, 1349987414, '98.149.249.30', 'dd8845627c8d5c79ca92e1df82c8cceffb74012a'),
(1991, 1349987422, '98.149.249.30', 'b3367f807181c8c9c44e6995d6f4d674586fbe76'),
(1992, 1349987430, '98.149.249.30', '54129b2d119b61dbb8fa010b05c78e040e7e1793'),
(1993, 1349987435, '98.149.249.30', '5d379222d9397bc6b2e95902fcd8cc1e643c6896'),
(1994, 1349987439, '98.149.249.30', '5ab63f79b94b78860957518a04058ddd12593345'),
(1995, 1349987490, '98.149.249.30', '9dd8d7dd03260d2878c712630564918def84dafb'),
(1996, 1349987576, '98.149.249.30', '359bbcea64242fc30a634f554510576a5abe6fec'),
(1997, 1349987579, '98.149.249.30', '72fba1440e6543e5867ddc1cdd4fc7894ebd2f37'),
(1998, 1349987580, '98.149.249.30', 'bddf8f24ae8f4f531ebee40a185dea3e38167dba'),
(1999, 1349987581, '98.149.249.30', '3f8279654e19c13e5059ae03196a180cdf7da8b7'),
(2000, 1349987650, '98.149.249.30', '52acfc698f4ad3fe508313b01ffe4b1282b680d3'),
(2001, 1349987654, '98.149.249.30', '9dfd3d5d3abe765a44dffbdfee740ba1515bbbe1'),
(2002, 1349987655, '98.149.249.30', '1d7c332f95d398e0421f27ece6e10ba109ce14bc'),
(2003, 1349987678, '98.149.249.30', '6420db41d4a43084abe8be51e9baafadc0d8a731'),
(2004, 1349987716, '98.149.249.30', 'c092be27c68b958a0fe506d1c93a545531aa39a8'),
(2005, 1349987720, '98.149.249.30', '72df4d7474c23e5326d490dce393c84b323681f4'),
(2006, 1349987723, '98.149.249.30', '5d662a6568dba5c09bb326b45bc6670564ba16b7'),
(2007, 1349987724, '98.149.249.30', '346091f52ad0422c947b28e2b4bca83a9b937bb2'),
(2008, 1349987754, '98.149.249.30', 'c9fa781758606cc1209524a8f37984f488fe5402'),
(2009, 1349987765, '98.149.249.30', 'cf84619bc4659281e6e798f1666194146523f84b'),
(2010, 1349987767, '98.149.249.30', '8ae7b93177a68fc98c9ee5442bb2161993842327'),
(2011, 1349987779, '98.149.249.30', 'ac0d849bf3f58cfbb1fd98170229e6cb4ffc4304'),
(2012, 1349987781, '98.149.249.30', '26bf02456bafe280a129befd54b0c6d66f3c3b0b'),
(2013, 1349987783, '98.149.249.30', 'cfb4ab1d37ddb6320fe63be59cf57b2995786477'),
(2014, 1349987786, '98.149.249.30', 'b28007f27302df121eeb32832621c4bb3e3f0d5d'),
(2015, 1349987788, '98.149.249.30', '4e7caf8d5968a35cb22bcbb8172e399f194ad7e4'),
(2016, 1349987806, '98.149.249.30', '541c7c789d73f982acca99b8663b85310676a457'),
(2017, 1349987811, '98.149.249.30', 'cbf264d22a7890cd80195920a4a59fa5600b6499'),
(2018, 1349987847, '98.149.249.30', 'd4a3154113a7429b831e42a2d44b842b3bc4eea1'),
(2019, 1349987850, '98.149.249.30', '1ec817a463ca0e9a297969839e5da85821710f8c'),
(2020, 1349987852, '98.149.249.30', 'd6f2079282fd6a4d8dac021cb709543c0d70fc77'),
(2021, 1349987904, '98.149.249.30', 'e0a1d752c12533398cbc5b4b9d837f5cec05730d'),
(2022, 1349987957, '98.149.249.30', 'c1cc12ccdf5510f39a0132cb3cf80ca077494bc2'),
(2023, 1349987968, '98.149.249.30', 'fe4b554c2eeb2b82699526ec39b4c7b3a8b81965'),
(2024, 1349987970, '98.149.249.30', '8059359241da21c3f401a9e1bb67b6f542ed4144'),
(2025, 1349987973, '98.149.249.30', '6fed0dd0393fd47ff098fc0d10f03e47c23b0929'),
(2026, 1349987974, '98.149.249.30', '06ce8c10cb999ca450967122ca76ad5d23722fa6'),
(2027, 1349988208, '98.149.249.30', 'eabca491c003a566ff483689fb5ffe6502ae761a'),
(2028, 1349988210, '98.149.249.30', '993461422fc2b1abbcc9b6c916db7420ca0f7ebd'),
(2029, 1349988216, '98.149.249.30', '617d6b133628458f118c3b493af868ada43e6fc0'),
(2030, 1349988217, '98.149.249.30', 'de5e2975eecce52ed8b71cb4723c6534cacac1da'),
(2031, 1349988227, '98.149.249.30', 'c699bd4c6f6efbab0f6d80f35ffb6e622f3d6722'),
(2032, 1349988230, '98.149.249.30', '8d33c9fc4d74fb26d7e1425a4eef43095277112e'),
(2033, 1349988231, '98.149.249.30', '4156c81b71bff3ea2f679489bd135c538e1112a6'),
(2034, 1349988232, '98.149.249.30', 'f3dd2b43be332d64a0d3af4ac720c8bef4e8fde8'),
(2035, 1349988250, '98.149.249.30', '653e1e786d962070ae466e1cec40e5b04abe73e5'),
(2036, 1349988258, '98.149.249.30', '0669389fc173c7d711606debca3d2d8a5c10a036'),
(2037, 1349988382, '98.149.249.30', 'cd98ec5e152898a74e276bcb68f32cefb0c37d25'),
(2038, 1349988401, '98.149.249.30', '635abfa015a67c3fe2deabff436e04150ea31cf0'),
(2039, 1349988472, '98.149.249.30', '9a8f3a0a89103cf702ea9c3c854770d60d910c18'),
(2040, 1349988474, '98.149.249.30', 'b84b46a2a30743c92deff10545578fe4ac643e78'),
(2041, 1349988482, '98.149.249.30', '5eedd6872805a5714028534d4818e6e95e95b82f'),
(2042, 1349988484, '98.149.249.30', '135149907d77046588b084bf019fd9b1dd4b4199'),
(2043, 1349988486, '98.149.249.30', '2c20135cd90e7f7a31153a6a1ced2c9403e157df'),
(2044, 1349988487, '98.149.249.30', '7c1c7c6e3ca335845c364cae5d71cbb8585d854c'),
(2045, 1349988506, '98.149.249.30', '25be1bc779b65cb8d682a63ba836a51095398f77'),
(2046, 1349988508, '98.149.249.30', '74901651ce03388437e995ac8ac61174aa553b10'),
(2047, 1349988520, '98.149.249.30', 'b5201360e4c07b0b85dea4db083cfda17b03b60b'),
(2048, 1349988529, '98.149.249.30', 'a8a244d378727f5f1a3035902d83c520b01f9434'),
(2049, 1349988546, '98.149.249.30', '8b5c009b385e0198ee046856fa7e0fdd309d33cf'),
(2050, 1349988549, '98.149.249.30', 'd16c544a7eb335261dcf92fcd9107ff009511aa4'),
(2051, 1349988551, '98.149.249.30', '3ce638990315d4934c670052d614b995780a976f'),
(2052, 1349988552, '98.149.249.30', '5a674debef221494b94b302a65f557b3d5430024'),
(2053, 1349988619, '98.149.249.30', 'fd215290d96b78c46630c99addc18675f19a7333'),
(2054, 1349988623, '98.149.249.30', '3a1c2580cc2f4228bf475a580c107282fec53e8b'),
(2055, 1349988633, '98.149.249.30', 'e4ddd6f810bbeb8f3f2887189faaf5fe920bfa69'),
(2056, 1349988812, '98.149.249.30', 'c21b21e3c4b53a2893af187d778f03664066edde'),
(2057, 1349988814, '98.149.249.30', 'b167be0839d8c13749eb3ddc9967bbd639453a9f'),
(2058, 1349989719, '98.149.249.30', 'ceaf2d29f8f603906757d22289c91346a8fbae0b'),
(2059, 1349992240, '98.149.249.30', '327b45034d87e49f1a1ef92620db8a069d3d4b6b'),
(2060, 1349992241, '98.149.249.30', 'aeb253a94fafb229b7c3d686dd66c16a8782574e'),
(2061, 1349992801, '97.79.130.126', '12405e0b86434ce4a26cde5844828c89baa86b25'),
(2062, 1349992805, '97.79.130.126', '009036ad842cab9f9a7cb70be2fc2e7210ba24f7'),
(2063, 1349995083, '98.149.249.30', 'b65fe14c09351a451a4aabd22cb9547119be5057'),
(2064, 1349995085, '98.149.249.30', '40b07bfca83383f31dbf7cfa547e59091fea5bde'),
(2065, 1349996832, '98.149.249.30', 'fb3b08caac8e8256abcc8f5ca233368123f19d67'),
(2066, 1349996836, '98.149.249.30', '4804c34b3f61624dd9872e31025c22cd44042a1d'),
(2067, 1349998388, '98.149.249.30', 'b41aca58eae87514979e1933187c2bbcf837854c'),
(2068, 1349998930, '98.149.249.30', 'e234899be214cc79c5d94c3b78e18ab5ec1cfcbe'),
(2069, 1349998940, '98.149.249.30', '1c0446ee6c0c7aadafbec2172b01e6935ef402a9'),
(2070, 1349998941, '98.149.249.30', '311c710dbbdd3f4550d3a6b4ff6863b7ab824c77'),
(2071, 1349998951, '98.149.249.30', '57278784aef8b4fd9f66c54babe712f8572a4d3b'),
(2072, 1349998956, '98.149.249.30', '69022fb9e750fe40cbc12e429296d04591cf47a4'),
(2073, 1349998957, '98.149.249.30', '976d9c40833260cecfee9c4c61ed54ed09b4e14b'),
(2074, 1349998958, '98.149.249.30', 'b19c15fd688466250ad38413f84e1b1ba634723b'),
(2075, 1349998958, '98.149.249.30', 'b6a04ffde768442abe00be50fda0ec1766102dd1'),
(2076, 1349998962, '98.149.249.30', '7a03d06ddcca3c75bf7ca953e3736cc5a8f76df9'),
(2077, 1349998974, '98.149.249.30', '1e2514171b5851a80f206c1c929e51bff6ff5aa5'),
(2078, 1349998982, '98.149.249.30', 'c6a865ca51b8f938be71e2e4beb4d1b1b9f58573'),
(2079, 1349999010, '98.149.249.30', 'aa689d80c5e88c65fd5001343e67e0f558e5a782'),
(2080, 1349999032, '98.149.249.30', '2cf234d959bb8c88b589abd136207795b15e95da'),
(2081, 1349999038, '98.149.249.30', 'fab2333e8fd1d702cbfad42cad45e984c7e56978'),
(2082, 1349999038, '98.149.249.30', 'caf76f4cb16337c8723d4d569a4e9198969cefe6'),
(2083, 1349999055, '98.149.249.30', '56f4993fbb1ccaec352ba9640b600db099116eca'),
(2084, 1349999077, '98.149.249.30', '2c44a36640fc7096b2aa2c54835bac7b0ee519d7'),
(2085, 1349999082, '98.149.249.30', 'c844820b839369cc605af9d4f0c21d0158edb4de'),
(2086, 1349999182, '98.149.249.30', '513935d568084405c6f737e53fc70f17437b82b0'),
(2087, 1349999187, '98.149.249.30', 'd78303da60f84ae9f3635b699a833b3830859b83'),
(2088, 1349999187, '98.149.249.30', 'c6df43cb6e6e0ee0f79eb905259d9db48f2827d4'),
(2089, 1349999205, '98.149.249.30', '240e85d3bdc1addd63617d34b8a231d51e229513'),
(2090, 1349999343, '98.149.249.30', '9bd9f8df3b9aab4c401d543a5f9ec8cac0bbaf54'),
(2091, 1349999347, '98.149.249.30', 'a11308e57680cc1986726a73e2628f8dd49efd0c'),
(2092, 1349999348, '98.149.249.30', '320c9e01dd3d8cf68cf1a72443fa1ade0d0c2440'),
(2093, 1349999348, '98.149.249.30', '703c22194f5849525462319c3135c5e79ea5b663'),
(2094, 1349999357, '98.149.249.30', '9a673b630c1ea83d42a8b06309b086a0c51b9dc8'),
(2095, 1349999359, '98.149.249.30', 'bd4b581775f5e31160331e33f01775efdfa888e8'),
(2096, 1349999360, '98.149.249.30', 'a33ba7ebb52f373e96fcdcfd7bc6cdd8d7e730a5'),
(2097, 1349999364, '98.149.249.30', '3ba43c94757b0dbe39bdc6029bf87c8c09d7aa5f'),
(2098, 1349999483, '98.149.249.30', 'b0fce5ee35ec608d8a09ad516fa8a7663d2ab381'),
(2099, 1349999487, '98.149.249.30', '670a51908e7b2ef685c19228b44fadf605d5da0d'),
(2100, 1350002779, '98.149.249.30', '0563c44981ab65f3abee1a7559b3e1f7102508ab'),
(2101, 1350002781, '98.149.249.30', '2f32eae13c748d388e227866e0b817236416bc0c'),
(2102, 1350059280, '97.79.130.126', '6e7959a563b302b8ae966d6d93e94172a763655a'),
(2103, 1350062430, '98.149.249.30', '8a8a04ba03e9c8377a56cf0c784270808a70eab0'),
(2105, 1350250126, '76.79.113.21', '27f0d43367ce6e4ec7389156afd26647fa7a5787'),
(2107, 1350340136, '76.79.113.21', '69953bed0def58b214d623094d22c2d371f02df5'),
(2108, 1350343755, '76.79.113.21', 'd7788c894bfc1e5258246c2df6bd3a91d970311b'),
(2109, 1350405992, '76.90.248.171', 'c256ef19f107a5c302803b962a274f388382a738'),
(2111, 1350508531, '76.79.113.21', '6e5fe97c99f6d2d3ec6dc7910083991eed43d2a3'),
(2113, 1350591843, '108.13.107.180', '61c7c8ad6a1c46fa5df33991b0a799bb75c6310f'),
(2114, 1350598781, '108.13.107.180', '9ef697421bbc7d431aeb996ca3a96e3c97628d4d'),
(2115, 1350598790, '108.13.107.180', 'af9b8ae86bf672eb6bc8f6c192f973dabbbbd061'),
(2116, 1350598792, '108.13.107.180', '5e1b686ac4855f3e008e0b9098059ab99de46c3c'),
(2117, 1350600177, '108.13.107.180', '0a035395746751a15d3746b2e54e3baee46e75f4'),
(2118, 1350600178, '108.13.107.180', '187699b5acf732895fa058bb76d3665d94aa1dd2'),
(2119, 1350605906, '108.13.107.180', '0e2c727c3e1c6adb01f8959d934b671aa1ebb8f4'),
(2120, 1350605910, '108.13.107.180', '9659e317ca1f7040f22a90370bb58a9cab4f69ac'),
(2121, 1350607393, '108.13.107.180', 'fdafd307b2e9b68c18f898a060739dc910b00f15'),
(2122, 1350617376, '108.13.107.180', '48096e3f913708fe05adcfcf5f43a47939a627b3'),
(2123, 1350930179, '98.149.249.30', '2caa18427743be31ef86927ad7430938f68e6da8'),
(2124, 1350934215, '98.149.249.30', '27a1ca7ee93110bd685d1f8937f83ae9f7dfb50e');

-- --------------------------------------------------------

--
-- Table structure for table `exp_seolite_config`
--

CREATE TABLE IF NOT EXISTS `exp_seolite_config` (
  `seolite_config_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned DEFAULT NULL,
  `template` text,
  `default_keywords` varchar(1024) NOT NULL,
  `default_description` varchar(1024) NOT NULL,
  `default_title_postfix` varchar(60) NOT NULL,
  PRIMARY KEY (`seolite_config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_seolite_config`
--

INSERT INTO `exp_seolite_config` (`seolite_config_id`, `site_id`, `template`, `default_keywords`, `default_description`, `default_title_postfix`) VALUES
(1, 1, '<title>{title}{site_name}</title>\n<meta name=''keywords'' content=''{meta_keywords}'' />\n<meta name=''description'' content=''{meta_description}'' />\n<link rel=''canonical'' href=''{canonical_url}'' />\n<!-- generated by seo_lite -->', 'your, default, keywords, here', 'Your default description here', ' |&nbsp;');

-- --------------------------------------------------------

--
-- Table structure for table `exp_seolite_content`
--

CREATE TABLE IF NOT EXISTS `exp_seolite_content` (
  `seolite_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) NOT NULL,
  `entry_id` int(10) NOT NULL,
  `title` varchar(1024) DEFAULT NULL,
  `keywords` varchar(1024) NOT NULL,
  `description` text,
  PRIMARY KEY (`seolite_content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `exp_seolite_content`
--

INSERT INTO `exp_seolite_content` (`seolite_content_id`, `site_id`, `entry_id`, `title`, `keywords`, `description`) VALUES
(1, 1, 1, '', '', ''),
(2, 1, 2, '', '', ''),
(3, 1, 3, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `exp_sessions`
--

CREATE TABLE IF NOT EXISTS `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_sites`
--

CREATE TABLE IF NOT EXISTS `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` text NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_sites`
--

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`) VALUES
(1, 'iFlyWorld', 'default_site', NULL, 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czo5OiJpbmRleC5waHAiO3M6ODoic2l0ZV91cmwiO3M6MjU6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS8iO3M6MTY6InRoZW1lX2ZvbGRlcl91cmwiO3M6MzI6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MjM6ImFsZXhAZS1kaWdpdGFsZ3JvdXAuY29tIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czo0MToiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfcGF0aCI7czo0ODoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFxpbWFnZXNcY2FwdGNoYXNcIjtzOjEyOiJjYXB0Y2hhX2ZvbnQiO3M6MToieSI7czoxMjoiY2FwdGNoYV9yYW5kIjtzOjE6InkiO3M6MjM6ImNhcHRjaGFfcmVxdWlyZV9tZW1iZXJzIjtzOjE6Im4iO3M6MTc6ImVuYWJsZV9kYl9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImVuYWJsZV9zcWxfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJmb3JjZV9xdWVyeV9zdHJpbmciO3M6MToibiI7czoxMzoic2hvd19wcm9maWxlciI7czoxOiJuIjtzOjE4OiJ0ZW1wbGF0ZV9kZWJ1Z2dpbmciO3M6MToibiI7czoxNToiaW5jbHVkZV9zZWNvbmRzIjtzOjE6Im4iO3M6MTM6ImNvb2tpZV9kb21haW4iO3M6MDoiIjtzOjExOiJjb29raWVfcGF0aCI7czowOiIiO3M6MTc6InVzZXJfc2Vzc2lvbl90eXBlIjtzOjE6ImMiO3M6MTg6ImFkbWluX3Nlc3Npb25fdHlwZSI7czoyOiJjcyI7czoyMToiYWxsb3dfdXNlcm5hbWVfY2hhbmdlIjtzOjE6InkiO3M6MTg6ImFsbG93X211bHRpX2xvZ2lucyI7czoxOiJ5IjtzOjE2OiJwYXNzd29yZF9sb2Nrb3V0IjtzOjE6InkiO3M6MjU6InBhc3N3b3JkX2xvY2tvdXRfaW50ZXJ2YWwiO3M6MToiMSI7czoyMDoicmVxdWlyZV9pcF9mb3JfbG9naW4iO3M6MToieSI7czoyMjoicmVxdWlyZV9pcF9mb3JfcG9zdGluZyI7czoxOiJ5IjtzOjI0OiJyZXF1aXJlX3NlY3VyZV9wYXNzd29yZHMiO3M6MToibiI7czoxOToiYWxsb3dfZGljdGlvbmFyeV9wdyI7czoxOiJ5IjtzOjIzOiJuYW1lX29mX2RpY3Rpb25hcnlfZmlsZSI7czowOiIiO3M6MTc6Inhzc19jbGVhbl91cGxvYWRzIjtzOjE6InkiO3M6MTU6InJlZGlyZWN0X21ldGhvZCI7czo3OiJyZWZyZXNoIjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVTTYiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czozOiJVTTYiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MToieSI7czoxNToiaG9ub3JfZW50cnlfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6NDA6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS9pbWFnZXMvc21pbGV5cy8iO3M6MTk6InJlY291bnRfYmF0Y2hfdG90YWwiO3M6NDoiMTAwMCI7czoxNzoibmV3X3ZlcnNpb25fY2hlY2siO3M6MToieSI7czoxNzoiZW5hYmxlX3Rocm90dGxpbmciO3M6MToibiI7czoxNzoiYmFuaXNoX21hc2tlZF9pcHMiO3M6MToieSI7czoxNDoibWF4X3BhZ2VfbG9hZHMiO3M6MjoiMTAiO3M6MTM6InRpbWVfaW50ZXJ2YWwiO3M6MToiOCI7czoxMjoibG9ja291dF90aW1lIjtzOjI6IjMwIjtzOjE1OiJiYW5pc2htZW50X3R5cGUiO3M6NzoibWVzc2FnZSI7czoxNDoiYmFuaXNobWVudF91cmwiO3M6MDoiIjtzOjE4OiJiYW5pc2htZW50X21lc3NhZ2UiO3M6NTA6IllvdSBoYXZlIGV4Y2VlZGVkIHRoZSBhbGxvd2VkIHBhZ2UgbG9hZCBmcmVxdWVuY3kuIjtzOjE3OiJlbmFibGVfc2VhcmNoX2xvZyI7czoxOiJ5IjtzOjE5OiJtYXhfbG9nZ2VkX3NlYXJjaGVzIjtzOjM6IjUwMCI7czoxNzoidGhlbWVfZm9sZGVyX3BhdGgiO3M6Mzk6IkM6XGluZXRwdWJcd3d3cm9vdFxkZXZpZmx5d29ybGRcdGhlbWVzXCI7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDA6Imh0dHA6Ly9kZXYuaWZseXdvcmxkLmNvbS9pbWFnZXMvYXZhdGFycy8iO3M6MTE6ImF2YXRhcl9wYXRoIjtzOjQ3OiJDOlxpbmV0cHViXHd3d3Jvb3RcZGV2aWZseXdvcmxkXGltYWdlc1xhdmF0YXJzXCI7czoxNjoiYXZhdGFyX21heF93aWR0aCI7czozOiIxMDAiO3M6MTc6ImF2YXRhcl9tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMzoiYXZhdGFyX21heF9rYiI7czoyOiI1MCI7czoxMzoiZW5hYmxlX3Bob3RvcyI7czoxOiJuIjtzOjk6InBob3RvX3VybCI7czo0NjoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxMDoicGhvdG9fcGF0aCI7czo1MzoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFxpbWFnZXNcbWVtYmVyX3Bob3Rvc1wiO3M6MTU6InBob3RvX21heF93aWR0aCI7czozOiIxMDAiO3M6MTY6InBob3RvX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEyOiJwaG90b19tYXhfa2IiO3M6MjoiNTAiO3M6MTY6ImFsbG93X3NpZ25hdHVyZXMiO3M6MToieSI7czoxMzoic2lnX21heGxlbmd0aCI7czozOiI1MDAiO3M6MjE6InNpZ19hbGxvd19pbWdfaG90bGluayI7czoxOiJuIjtzOjIwOiJzaWdfYWxsb3dfaW1nX3VwbG9hZCI7czoxOiJuIjtzOjExOiJzaWdfaW1nX3VybCI7czo1NDoiaHR0cDovL2Rldi5pZmx5d29ybGQuY29tL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6NjE6IkM6XGluZXRwdWJcd3d3cm9vdFxkZXZpZmx5d29ybGRcaW1hZ2VzXHNpZ25hdHVyZV9hdHRhY2htZW50c1wiO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo1NDoiQzpcaW5ldHB1Ylx3d3dyb290XGRldmlmbHl3b3JsZFxpbWFnZXNccG1fYXR0YWNobWVudHNcIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czoxMToiL3RlbXBsYXRlcy8iO30=', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjQxOiJDOlxpbmV0cHViXHd3d3Jvb3RcZGV2aWZseXdvcmxkXGluZGV4LnBocCI7czozMjoiYTJlYmE4YWQwMjc4NzAwZGQ0ZTljNjQ2MTNlMjJlMmYiO3M6NDk6IkM6XGluZXRwdWJcd3d3cm9vdFxhdXN0aW4uaWZseXdvcmxkLmNvbVxpbmRleC5waHAiO3M6MzI6ImQzNDAyZjA0NzE1ZjFiMThkN2E3OTc0YzE5NTI0MWQ0Ijt9');

-- --------------------------------------------------------

--
-- Table structure for table `exp_snippets`
--

CREATE TABLE IF NOT EXISTS `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_specialty_templates`
--

CREATE TABLE IF NOT EXISTS `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `exp_specialty_templates`
--

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`) VALUES
(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=''content-type'' content=''text/html; charset={charset}'' />\n\n{meta_refresh}\n\n<style type="text/css">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id="content">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe''re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the "{mailing_list}" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled ‘{message_subject}’.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

-- --------------------------------------------------------

--
-- Table structure for table `exp_stats`
--

CREATE TABLE IF NOT EXISTS `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_stats`
--

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`) VALUES
(1, 1, 8, 8, 'Roy Hughes', 2, 0, 0, 0, 1349910370, 0, 0, 1350491427, 5, 1346789616, 1350504964);

-- --------------------------------------------------------

--
-- Table structure for table `exp_statuses`
--

CREATE TABLE IF NOT EXISTS `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_statuses`
--

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`) VALUES
(1, 1, 1, 'open', 1, '009933'),
(2, 1, 1, 'closed', 2, '990000');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_groups`
--

CREATE TABLE IF NOT EXISTS `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_status_groups`
--

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`) VALUES
(1, 1, 'Statuses');

-- --------------------------------------------------------

--
-- Table structure for table `exp_status_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_templates`
--

CREATE TABLE IF NOT EXISTS `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `exp_templates`
--

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`) VALUES
(1, 1, 1, 'index', 'y', 'webpage', '	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">\n	<head>\n	<title>calendar</title>\n	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n	<link media="all" rel="stylesheet" type="text/css" href="/css/all.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/anythingslider.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/calendar.css">\n	<link rel="stylesheet" href="/css/calendar.css" media="screen" />\n        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>\n        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>\n	</head>\n	<body>\n<div class="content-block">\n\n			<section class="block-section">\n				<article class="block">\n					<div class="block-holder">\n                        			<div class="event-overlay"></div>\n						{exp:low_events:calendar channel="events" date="{segment_2}"}\n  <div id="calendar">\n    <div id="title">\n      <a class="prev" href="{path="events/{prev_month_url}"}" title="{prev_month format=''%F %Y''}">&larr;</a>\n      <strong>{this_month format="%F %Y"}</strong>\n      <a class="next" href="{path="events/{next_month_url}"}" title="{next_month format=''%F %Y''}">&rarr;</a>\n    </div>\n<table> \n    <thead>\n      <tr>\n        {weekdays}<th scope="col">{weekday_1}</th>{/weekdays}\n      </tr>\n    </thead>\n    <tbody>\n      {weeks}\n        <tr{if is_given_week} class="given-week"{/if}>\n          {days}\n            <td class="{if is_current}current{/if}{if is_given} given{/if}{if is_today} today{/if} {if events_on_day}highlight{/if}">\n              {if events_on_day}\n                <a href="{path="events/{day_url}"}">{day_number}</a>\n              {if:else}\n                <span>{day_number}</span>\n              {/if}\n            </td>\n          {/days}\n        </tr>\n      {/weeks}\n    </tbody>\n  </table>\n</div><!-- /#calendar -->\n{/exp:low_events:calendar}\n					</div>\n				</article>\n                <article class="block event-tile">\n                    <div class="block-holder">\n                        <div class="event-overlay"></div>\n                        <div id="calendar-widget"></div>\n                        <p>Choose a date above.</p>\n                        <h2>iFLY EVENTS</h2>\n                        <!--<a href="#" class="btn red"><em></em><span>See all Events</span></a>-->\n                    </div>\n                </article>\n				<article class="block">\n					<div class="block-holder">\n						<img src="images/img07.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>LOREM IPSUM DOLOR SIT AMIT.</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>LEARN MORE</span></a>\n					</div>\n				</article>\n			</section><!-- /.block-section -->\n\n			<section class="gallery-block">\n				<h1>OUR FAVORITE iFLY PHOTOS &amp; VIDEOS</h1>\n				<div class="gallery">\n					<div class="gholder">\n					</div>\n				</div>\n			</section><!-- /.gallery-block -->\n\n			<div class="btn-block">\n				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>\n			</div><!-- /.btn-block -->\n\n		</div>	\n<script src="/js/calendar.js"></script>\n	</body>\n</html>', '', 1348607936, 1, 'n', 0, '', 'n', 'n', 'o', 160),
(2, 1, 1, 'events', 'n', 'webpage', '	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">\n<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">\n	<head>\n	<title>calendar</title>\n	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">\n	<link media="all" rel="stylesheet" type="text/css" href="/css/all.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/anythingslider.css">\n        <link media="all" rel="stylesheet" type="text/css" href="/css/calendar.css">\n	<link rel="stylesheet" href="/css/calendar.css" media="screen" />\n        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>\n        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>\n	</head>\n	<body>\n<div class="content-block">\n\n			<section class="block-section">\n				<article class="block">\n					<div class="block-holder">\n                        			<div class="event-overlay"></div>\n						{exp:low_events:calendar channel="events" date="{segment_2}"}\n  <div id="calendar">\n    <div id="title">\n      <a class="prev" href="{path="events/{prev_month_url}"}" title="{prev_month format=''%F %Y''}">&larr;</a>\n      <strong>{this_month format="%F %Y"}</strong>\n      <a class="next" href="{path="events/{next_month_url}"}" title="{next_month format=''%F %Y''}">&rarr;</a>\n    </div>\n<table> \n    <thead>\n      <tr>\n        {weekdays}<th scope="col">{weekday_1}</th>{/weekdays}\n      </tr>\n    </thead>\n    <tbody>\n      {weeks}\n        <tr{if is_given_week} class="given-week"{/if}>\n          {days}\n            <td class="{if is_current}current{/if}{if is_given} given{/if}{if is_today} today{/if} {if events_on_day}highlight{/if}">\n              {if events_on_day}\n                <a href="{path="events/{day_url}"}">{day_number}</a>\n              {if:else}\n                <span>{day_number}</span>\n              {/if}\n            </td>\n          {/days}\n        </tr>\n      {/weeks}\n    </tbody>\n  </table>\n</div><!-- /#calendar -->\n{/exp:low_events:calendar}\n					</div>\n				</article>\n                <article class="block event-tile">\n                    <div class="block-holder">\n                        <div class="event-overlay"></div>\n                        <div id="calendar-widget"></div>\n                        <p>Choose a date above.</p>\n                        <h2>iFLY EVENTS</h2>\n                        <!--<a href="#" class="btn red"><em></em><span>See all Events</span></a>-->\n                    </div>\n                </article>\n				<article class="block">\n					<div class="block-holder">\n						<img src="images/img07.jpg" width="313" height="186" alt="image description" class="image">\n						<div class="text-block">\n							<h2>LOREM IPSUM DOLOR SIT AMIT.</h2>\n							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>\n						</div>\n						<a href="#" class="btn red"><em></em><span>LEARN MORE</span></a>\n					</div>\n				</article>\n			</section><!-- /.block-section -->\n\n			<section class="gallery-block">\n				<h1>OUR FAVORITE iFLY PHOTOS &amp; VIDEOS</h1>\n				<div class="gallery">\n					<div class="gholder">\n					</div>\n				</div>\n			</section><!-- /.gallery-block -->\n\n			<div class="btn-block">\n				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>\n			</div><!-- /.btn-block -->\n\n		</div>	\n<script src="/js/calendar.js"></script>\n	</body>\n</html>', '', 1348609318, 0, 'n', 0, '', 'n', 'n', 'o', 38);

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_template_groups`
--

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`) VALUES
(1, 1, 'home', 1, 'y');

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_member_groups`
--

CREATE TABLE IF NOT EXISTS `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_template_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exp_throttle`
--

CREATE TABLE IF NOT EXISTS `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_no_access`
--

CREATE TABLE IF NOT EXISTS `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exp_upload_no_access`
--

INSERT INTO `exp_upload_no_access` (`upload_id`, `upload_loc`, `member_group`) VALUES
(1, 'cp', 6);

-- --------------------------------------------------------

--
-- Table structure for table `exp_upload_prefs`
--

CREATE TABLE IF NOT EXISTS `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `exp_upload_prefs`
--

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`) VALUES
(1, 1, 'General Uploads', 'C:\\inetpub\\wwwroot\\deviflyworld\\images\\uploads\\', 'http://dev.iflyworld.com/images/uploads/', 'img', '', '', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exp_videoplayer_accounts`
--

CREATE TABLE IF NOT EXISTS `exp_videoplayer_accounts` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(5) unsigned DEFAULT NULL,
  `order` int(3) unsigned DEFAULT NULL,
  `service` varchar(30) DEFAULT '',
  `api_key` varchar(200) DEFAULT '',
  `api_secret` varchar(200) DEFAULT '',
  `enabled` int(1) unsigned DEFAULT NULL,
  `is_authenticated` int(1) unsigned DEFAULT NULL,
  `authsub_session_token` varchar(50) DEFAULT '',
  `oauth_access_token` varchar(50) DEFAULT '',
  `oauth_access_token_secret` varchar(50) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
