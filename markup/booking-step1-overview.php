<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step1 overview">
	
	<?php include("inc/header.php"); ?>

	<div id="main" class="step1" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<aside>
						<div class="video-wrapper">
							<a class="btn-prev" href="#">&lt;</a>
							<a class="btn-next" href="#">&gt;</a>
							<div class="video-player">
								<div id="yt-player" data-ytid="yP9GXL68w2w"></div>
							</div>
						</div><!-- /.video-wrapper -->
					</aside>
					<article>
						<h1>First Time Flyer Experience</h1>
						<p>No special skills are required, no experience is necessary, and all ages can fly—from 3 to 93.</p>
						<p>iFLY’s first-time flyer experience will have you grinning from ear to ear the moment you step into the flight chamber and lift off. It’s an incredibly safe, realistic and unique experience, fully supervised by instructors who are trained and certified by the International Bodyflight Association.</p>
						<p>All First-Time Flyer packages include a comprehensive training session, equipment rental, and one-on-one assistance throughout your flight from one of our instructors.</p>
						<p>Pick from one of the packages below, or if you need help deciding, click on our <a href="#" title="Flight Wizard">Flight Wizard</a> to get our personalized recommendations just for you.</p>
						<br />
    					<h2 class="main">Packages</h2>
					</article>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var defaultTunnelLocation = 'ifo';
			var defaultFlyerType = '1ST_TIME';
			var today = new Date();
			IFLY.booking.getFlightOverviewPackages(defaultTunnelLocation, defaultFlyerType, today);
		});

	</script>
</body>
</html>