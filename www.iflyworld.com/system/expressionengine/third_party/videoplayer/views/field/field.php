<?php
if($any_account_exists)
{
	?>
	<div class="videoplayer-field">
		<?php
		echo $hidden_input;
		?>
		<div class="preview"></div>

		<p class="controls">
			<a class="add"><span></span><?php echo $this->lang->line('add_video'); ?></a>
			<a class="change"><span></span><?php echo $this->lang->line('change_video'); ?></a>
			<a class="remove"><span></span><?php echo $this->lang->line('remove_video'); ?></a>
		</p>
	</div>
	<?php
}
else
{
	echo '<p>'.$this->lang->line('videoplayer_disabled').'</p>';
}
?>
