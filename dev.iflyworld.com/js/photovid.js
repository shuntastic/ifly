
IFLY.vidcontrols = {
	videoPlayer: null,
	scrubber: null,
	init: function(vidSrc,tVid,tPlayBtn,tPauseBtn,tSeek) {
//				projekktor('#'+tVid);
				IFLY.vidcontrols.videoPlayer = _V_(tVid);

//				IFLY.vidcontrols.videoPlayer = projekktor('#'+tVid);
 				IFLY.vidcontrols.videoPlayer.src(vidSrc);
				IFLY.vidcontrols.videoPlayer.ready(function() {
					IFLY.vidcontrols.videoPlayer.play();
					IFLY.vidcontrols.initSeek(tSeek);
					IFLY.vidcontrols.initPlayPause(tPlayBtn,tPauseBtn);
					IFLY.vidcontrols.initAutoHide(tVid);
					//if (autoFlag == "true")
					//	this.play();
					
					IFLY.vidcontrols.videoPlayer.addEvent("ended", function() {
						//_gaq.push(['trackEvent', SecondaryPage.googleCampaignName, 'videoEnd', SecondaryPage.slug]);
					}, false);
				});	
	},
	initSeek: function(tSeek) {
		//build slider
		var slide_handler = function(e, ui) {
			var newPos = IFLY.vidcontrols.scrubber.slider('value')/100;
			var newTime = IFLY.vidcontrols.videoPlayer.duration()*newPos;
			IFLY.vidcontrols.videoPlayer.currentTime(newTime);
		};
		
		IFLY.vidcontrols.scrubber = $(tSeek).slider({
			orientation: "horizontal",
			animate:true,
			max:100,
			min:1,
			step: 1,
			value: 1, // Sets the value to the top
			slide: slide_handler//,
		 // change: slide_handler
		});
		
		
		IFLY.vidcontrols.videoPlayer.addEvent("play", function(e) {
			var current = IFLY.vidcontrols.videoPlayer.currentTime();
			var total = IFLY.vidcontrols.videoPlayer.duration();
			var pos = (current/total)*100;
			IFLY.vidcontrols.scrubber.slider('value', pos);
		}, false);
		
	},
	initPlayPause: function(tPlayBtn,tPauseBtn) {
		$(tPlayBtn).unbind('click').click(function(e) {
			IFLY.vidcontrols.videoPlayer.play();
			$(this).addClass('inactive');
			$(tPlayBtn).removeClass('inactive');
		});
		$(tPauseBtn).unbind('click').click(function(e) {
			IFLY.vidcontrols.videoPlayer.pause();
			$(this).addClass('inactive');
			$(tPauseBtn).removeClass('inactive');
		});
		
		
		IFLY.vidcontrols.videoPlayer.addEvent("play", function(e) {
			$(tPlayBtn).addClass('inactive');
			$(tPauseBtn).removeClass('inactive');
		}, false);
		IFLY.vidcontrols.videoPlayer.addEvent("pause", function(e) {
			$(tPauseBtn).addClass('inactive');
			$(tPlayBtn).removeClass('inactive');
		}, false);
	},
	hideControls: function() {
		$('div.video-share a.btn').stop().fadeTo(300,0);
	},
	showControls: function() {
		$('div.video-share a.btn').stop().fadeTo(300,1);
	},
	initAutoHide: function(targetDiv) {
		$('#'+targetDiv).mouseover(IFLY.vidcontrols.showControls).mouseout(IFLY.vidcontrols.hideControls);
		//IFLY.vidcontrols.hideControls();
	}
};

IFLY.photovid = {
	basePath: 'http://skyventure.s3.amazonaws.com/',
	baseDate: null,
	price:0,
	session:[],
	sessionIndex:0,
	//FOR ANALYTICS TRACKING OF CURATED MEDIA
	currentOverlayType:'Photo',
	photoDCI:'',
	callApi: function(callStr,callbackfunc,onError,onComplete) {
		try {
			$.ajax({
				url: '/api_curl.php',
				type: 'post',
				dataType: 'json',
				data: callStr,
				success: callbackfunc,
				error: (onError != null) ? onError : handler.onError,
				complete: (onComplete != null) ? onComplete : handler.onComplete
			});

		  } catch(err) {
			  console.log('CALLBACK ERROR', err);
		  //Handle errors here
		  }	
  },
  init: function() {
	
	if($('body.photo-video').length>0) {
	
		if($('body.photo-video').hasClass('curated')) {
			$('#checkout').css('display','none');
		$('a.btn.photo-toggle, a.btn.video-toggle').unbind('click').bind('click', function() {IFLY.photovid.toggleCuratedMedia();});
			$('#grid-photo').imagesLoaded(function(){
				$('#grid-photo').masonry({
					itemSelector : '.item'//,columnWidth : 192
				})
			});
			$('#grid-photo').css('display','block');
			$('#grid-photo').delegate('.item a', 'click', function (e) {
				e.preventDefault();
				$(this).colorbox();
			});
				
		} else {
			$('a.btn.photo-toggle, a.btn.video-toggle').unbind('click').bind('click', function() {IFLY.photovid.toggleMedia();});
			$('#checkout').unbind('click').click(function(e){
				e.preventDefault();
				if(!$(this).hasClass('inactive')) {
					var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
					var localSite = 'https://'+selTunnelInfo.subdomain+'.iflyworld.com';
					// var full = 'http://'+window.location.host;
					var dURL = localSite + '/book-now/booking-step5';
					window.location = dURL;
				}
			});
			//IFLY.current_tunnel = (IFLY.current_tunnel!='' && IFLY.current_tunnel!=null) ? IFLY.current_tunnel : (cookieHandler.read('ifly_tunnel') != '' && cookieHandler.read('ifly_tunnel') != null) ? cookieHandler.read('ifly_tunnel') : 'ifo';
			//$('select[id$="flyer-location"] option:selected').removeAttr('selected');
			//$('select[id$="flyer-location"] option[value="'+IFLY.current_tunnel+'"]').attr("selected","selected");
			//$.uniform.update();
			//console.log('IFLY.current_tunnel',IFLY.current_tunnel);
			
			var today = new Date();
			today.setDate(today.getDate() );  // today.setDate(today.getDate() - 1); to set yesterday
			IFLY.photovid.baseDate = new Date(today);
//			IFLY.photovid.initPhotos();
			IFLY.calendarWidget.initPhotoCalendar();

			$('select[id$="flyer-location"]').change(function() {
				IFLY.current_tunnel = $('select[id$="flyer-location"] option:selected').val();
				if($('a.btn.photo-toggle').hasClass('red')) {
					IFLY.photovid.getSessions(IFLY.photovid.baseDate,IFLY.photovid.displayLatestPhotoThumbs);
				} else {
					IFLY.photovid.getSessions(IFLY.photovid.baseDate,IFLY.photovid.displayLatestVideoThumbs);
				}
			});
			
			  getParams();
			  if(urlParams.d && urlParams.f && urlParams.l) {
				  function gotPhoto(response, textStatus, jqXHR) {
		//			  IFLY.killLoader();
						console.log('response.data',response);
						var ses = response.data;
						ses.file_name = urlParams.f;

						$('select[id$="flyer-location"] option:selected').removeAttr('selected');
						$('select[id$="flyer-location"] option[value="'+urlParams.l+'"]').attr("selected","selected");
						$.uniform.update();
						IFLY.photovid.session.push(response.data)
						var ovObj = IFLY.photovid.session[0];
						var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
						var img = IFLY.photovid.basePath+overlayPath;
						console.log('overlayPath',overlayPath);
						var trackPath = (overlayPath).split('/');
						var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(urlParams.l);
						IFLY.analytics.sendEvent('Photo', 'Views', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);
						IFLY.photovid.price = parseFloat(response.price);
						var thumbPath = IFLY.photovid.basePath+ovObj.lores_url;
						
						IFLY.photovid.displayCartOverlay(0,img, thumbPath);
						$(document).bind('cbox_closed', function(){
							IFLY.photovid.initPhotos();
						});
						IFLY.photovid.sessionIndex = 0;
				  }
				  var date = urlParams.d.split('-');
				  var fullDate = date[1]+'/'+date[2]+'/'+date[0];
				  var fullFile = urlParams.f+'.JPG';
					var call='method=get_photo&controller=sessionData&format=json&tunnel='+urlParams.l+'&photoName='+fullFile+'&theDate='+fullDate;
					console.log('call',call);
					IFLY.photovid.callApi(call, gotPhoto,null,null);
			  } else {
				IFLY.photovid.initPhotos();
			  }
			
			
			//	
		}
	}
  },
	initPhotos: function() {
		$('select#photovid-flight-time').attr("disabled",true);
		$('#uniform-photovid-flight-time').addClass('disabled');
			//$('a.btn.photo-toggle, a.btn.video-toggle').unbind('click').bind('click', function() {IFLY.photovid.toggleMedia();});
//			$('#book-date span').html(today);
//			$.uniform.update();
//			$('#grid-photo').imagesLoaded(function(){
//				$('#grid-photo').masonry({
//					itemSelector : '.item'
//					/*,columnWidth : 192*/
//				});
//			});

			$('#grid-photo').masonry({
				itemSelector : '.item'
			});
			IFLY.photovid.getSessions(IFLY.photovid.baseDate,IFLY.photovid.displayLatestPhotoThumbs);
	},
	initVideos: function() {
		$('select#photovid-flight-time').attr("disabled",true);
			$('#uniform-photovid-flight-time').addClass('disabled');
			//$('a.btn.photo-toggle, a.btn.video-toggle').unbind('click').bind('click', function() {IFLY.photovid.toggleMedia();});
//			var today = new Date();
//			today.setDate(today.getDate() - 1);
			$('#grid-video').masonry({
				itemSelector : '.item'
			});
			
			IFLY.photovid.getSessions(IFLY.photovid.baseDate,IFLY.photovid.displayLatestVideoThumbs);
	},
	clearItems: function(tDiv) {
		var items = tDiv+' .item';
		var $items = $(items);
		//console.log('clearItems',items,$items.length);
		$(tDiv).html('');
		if($items.length > 0) {
			//$(tDiv).masonry('reload');
			$.each($items, function() {
				var obj = $(this);
//				obj.empty().remove();
				$(tDiv).masonry('remove', obj);
			});
			$(tDiv).masonry('reload');
		}
	},
	displayLatestPhotoThumbs: function(response, textStatus, jqXHR){
					//IFLY.photovid.showSessionTimes(response, textStatus, jqXHR);
					IFLY.photovid.showSessionTimes(response, textStatus, jqXHR);
					var data = response.data;
					tSession = data[data.length - 1];
					IFLY.photovid.loadPhotos(tSession);
	//				IFLY.photovid.loadPhotos({"session_start":1350664349,"session_stop":1350665982});
//					IFLY.photovid.displayThumbs(data, false);
	},
	displayLatestVideoThumbs: function(response, textStatus, jqXHR){
					IFLY.photovid.showSessionTimes(response, textStatus, jqXHR);
					var data = response.data;
				//	console.log('displayLatestVideoThumbs',data);
					tSession = data[data.length - 1];
					IFLY.photovid.loadVideos(tSession);
	//				IFLY.photovid.loadPhotos({"session_start":1350664349,"session_stop":1350665982});
//					IFLY.photovid.displayThumbs(data, false);
	},
	getSessions: function(date, callback) {
		IFLY.killLoader();
		$('form.find-photovid fieldset div.row').append('<span style="position: absolute;margin-left: 30px;">'+IFLY.showRevLoader()+'</span>');

		//var tunnel = ($('select[id$="flyer-location"] option:selected').val() !='' && $('select[id$="flyer-location"] option:selected').val() != 'FLIGHT LOCATION') ? $('select[id$="flyer-location"] option:selected').val() : IFLY.current_tunnel;
		var call = 'method=get_session_events&controller=sessionData&format=json&tunnel='+$('select[id$="flyer-location"] option:selected').val()+'&date='+date.yyyymmdd();
	//	console.log('call',call);
			$.ajax({
				url: '/api_curl.php',
				type: 'post',
				dataType: 'json',
				data: call,
				success: callback,
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	showSessionTimes: function(response, textStatus, jqXHR){
			var data = response.data;
	//		console.log('showSessionTimes', response);
			//alert(data.toSource());
			var newValues = '<option selected="selected">TIME</option>';
			$.each(data, function(index,value) {
				var newTime = new Date(this.session_start*1000);
				newValues+='<option value="'+this.session_start + ',' + this.session_stop + '">'+this.time+'</option>';
			});
			$('select#photovid-flight-time').html(newValues);
			$('select#photovid-flight-time').attr("disabled",false);
			$('#uniform-photovid-flight-time').removeClass('disabled');
			
			$('select#photovid-flight-time').unbind('change').bind('change',function() {
				var times = $(this).val();
				times = times.split(",");
				if($('a.btn.photo-toggle').hasClass('red')) {
					IFLY.photovid.loadPhotos({"session_start":times[0],"session_stop":times[1]});
				} else {
					IFLY.photovid.loadVideos({"session_start":times[0],"session_stop":times[1]});
				}
				$.uniform.update();
			});
			IFLY.killLoader();
	},
	loadPhotos: function(sessionInfo) {
		IFLY.killLoader();
		$('a.btn.video-toggle').after(IFLY.showRevLoader());
		var call='method=get_photos&controller=sessionData&format=json&tunnel='+$('select[id$="flyer-location"] option:selected').val()+'&session_start='+sessionInfo.session_start+'&session_stop='+sessionInfo.session_stop+'&status=';
		//var d = Math.round(new Date().getTime() / 1000);
		//console.log('call',call);
				$.ajax({
					url: '/api_curl.php',
					type: 'post',
					dataType: 'json',
					data: call,
					success: function(response, textStatus, jqXHR){
						IFLY.killLoader();
						console.log("loadPhotos response:", response,'textStatus',textStatus);
						if(response.status =="OK") {
							IFLY.photovid.price = parseFloat(response.price);
							IFLY.photovid.session = [];
							$.each(response.data,function(index,value){
								var imgCheck = (this.lores_url != null) ? true : (this.medres_url != null) ? true : false;
								if(imgCheck) IFLY.photovid.session.push(value);
							});
							//IFLY.photovid.clearItems('#grid-photo');
							IFLY.photovid.initPagination(IFLY.photovid.session,'#grid-photo');
							//IFLY.photovid.displayThumbs('#grid-photo',data);
						} else {
							$('#grid-photo').html('<h2>No images were returned for this session.<br />Please enter a new location, a new date and time.</h2>').css('display','block');
						}
					},
					error: handler.onError,
					complete: handler.onComplete
				});
	},
	loadVideos: function(sessionInfo) {
		$('a.btn.video-toggle').after(IFLY.showRevLoader());
		var call='method=get_videos&controller=sessionData&format=json&tunnel='+$('select[id$="flyer-location"] option:selected').val()+'&session_start='+sessionInfo.session_start+'&session_stop='+sessionInfo.session_stop+'&status=';
		//var d = Math.round(new Date().getTime() / 1000);
		//console.log('call',call);
				$.ajax({
					url: '/api_curl.php',
					type: 'post',
					dataType: 'json',
					data: call,
					success: function(response, textStatus, jqXHR){
						IFLY.killLoader();
						console.log("loadVideos response:", response,'textStatus',textStatus);
						if(response.status =="OK") {
							IFLY.photovid.price = parseFloat(response.price);
							IFLY.photovid.session = [];
						//	$.each(response.data,function(index,value){IFLY.photovid.session.push(value)});
							$.each(response.data,function(index,value){
								var imgCheck = (this.lores_url != null) ? true : (this.medres_url != null) ? true : false;
								if(imgCheck) IFLY.photovid.session.push(value);
							});
							
							IFLY.photovid.initPagination(IFLY.photovid.session,'#grid-video');
							//IFLY.photovid.displayThumbs('#grid-photo',data);
						} else {
							$('#grid-video').html('<h2>No videos were returned for this session.<br />Please enter a new location, a new date and time.</h2>').css('display','block');
						}
					},
					error: handler.onError,
					complete: handler.onComplete
				});
	},
	currentPage:0,
	totalThumbs:0,
	lastThumb:0,
	totalPages:0,
	thumbsPerPage:25,
	
	//SHOW THUMBNAILS
	displayThumbs: function(tDiv,data) {
//				IFLY.killLoader();
				var div = $(tDiv);
				div.css('display','block');
				var thumbs = '';
				//var thumbCount=0;
				//var emptyFlag = $(tDiv + ' .item').length > 0;
				$.each(data,function(index,value){
					if (this.event == 'video clipped' || this.status == 'FLV' || this.status == 'MPG') {
						
						var thumbPath = (this.lores_url) ? this.lores_url : (this.medres_url) ? this.medres_url : null;
						thumbPath = thumbPath.replace('.flv','.png');
						var overlayPath = (this.medres_url) ? this.medres_url : this.lores_url;
						var flyerNum = (this.person_ct) ? 'Flyer #'+parseInt(this.person_ct): '';
						if (thumbPath != null) {
							thumbs+='<div class="item video"><span style="left:3px;right:initial;">'+this.id+'</span><span>'+flyerNum+'</span><a href="'+IFLY.photovid.basePath+overlayPath+'"><img src="'+IFLY.photovid.basePath+thumbPath+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';
						//	thumbCount++;
						}
					} else {
						var thumbPath = (this.lores_url) ? this.lores_url : (this.medres_url) ? this.medres_url : this.hires_url;
						var overlayPath = (this.medres_url) ? this.medres_url : this.lores_url;
						var flyerNum = (this.person_ct) ? 'Flyer #'+parseInt(this.person_ct): '';
						if (thumbPath != null) {
							var d1 = new Date(this.date_time);
							
							thumbs+='<div class="item image"><span style="left:3px;right:initial;">'+this.time + "<br/>" + this.file_name.replace("_","").replace(".JPG","")+'</span><span>'+flyerNum+'</span><a href="'+IFLY.photovid.basePath+overlayPath+'"><img src="'+IFLY.photovid.basePath+thumbPath+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';
						//	thumbCount++;
						}
					}
				});
				div.imagesLoaded(function(){
					var $thumbs = $(thumbs);
					div.append($thumbs).masonry('appended', $thumbs).masonry( 'reload' );

					//PHOTO CART OVERLAY
					$('.item.image a').each(function(index,value){
						$(this).unbind('click').click(function(e) {
							e.preventDefault();
							var ovObj = IFLY.photovid.session[index];
							var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
							var thumbPath = ovObj.lores_url;
							var img = IFLY.photovid.basePath+overlayPath;
							
							var trackPath = overlayPath.split('/');
							var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
							IFLY.analytics.sendEvent('Photo', 'Views', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);

//							var subCheck = full.split('.');
//							var trackPath = overlayPath.split('/');
//							IFLY.analytics.sendEvent('Photo', 'Views', subCheck[0]+trackPath[trackPath.length-1], null);
							
							IFLY.photovid.displayCartOverlay(index,img, IFLY.photovid.basePath+thumbPath);
							IFLY.photovid.sessionIndex = index;
							IFLY.photovid.initOverlayArrows();
						});
					});
					$('.item.video a').each(function(index,value){
						$(this).unbind('click').click(function(e) {
							e.preventDefault();
							var ovObj = IFLY.photovid.session[index];
							var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
							var thumbPath = ovObj.lores_url;
							var img = IFLY.photovid.basePath+overlayPath;
							
							var trackPath = overlayPath.split('/');
							var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
							IFLY.analytics.sendEvent('Video', 'Plays', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);

							IFLY.photovid.displayVideoCartOverlay(index,img, IFLY.photovid.basePath+thumbPath);
							IFLY.photovid.sessionIndex = index;
							IFLY.photovid.initOverlayArrows();
						});
					});
					IFLY.killLoader();
//					div.css('display','block');

				});
				
	},
	
	//CMS MANAGED THUMBS
	displayCuratedThumbs: function(tDiv,data) {
				var div = $(tDiv);
				div.css('display','none');
				var thumbs = '';
				var thumbCount=0;
				
				
				$.each(data,function(index,value){
					var thumbPath = (this.lores_url) ? this.lores_url : (this.medres_url) ? this.medres_url : this.hires_url;
					var overlayPath = (this.lores_url) ? this.lores_url : this.medres_url;
					if (thumbPath != null) {
					thumbs+='<div class="item image"><a href="'+IFLY.photovid.basePath+overlayPath+'"><img src="'+IFLY.photovid.basePath+thumbPath+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';
					thumbCount++;
					}
				});
				div.html(thumbs);
				div.imagesLoaded(function(){
					div.masonry({
						itemSelector : '.item'/*,
						columnWidth : 192*/
					})
				});
				div.delegate('.item a', 'click', function (e) {
					e.preventDefault();
					$(this).colorbox();
					IFLY.analytics.sendEvent(IFLY.photovid.currentOverlayType, 'Views', $(this).href(), null);

				});
				div.css('display','block');
	},
	initPagination: function(data,tDiv) {
				IFLY.photovid.clearItems(tDiv);
				if (data.length > IFLY.photovid.thumbsPerPage) {
					$('.footer-nav a.more').css('display','inline-block');	
					IFLY.photovid.totalThumbs = data.length;
					IFLY.photovid.lastThumb = 0;
					
					$('.footer-nav a.more').unbind('click').click(function(e) {
						addMore(tDiv);
						e.preventDefault();
					});

					$(window).scroll(function() {
					   if($(window).scrollTop() + $(window).height() == $(document).height()) {
							addMore(tDiv);
					   }
					});
				
					function addMore(disDiv){
						var nextSet = IFLY.photovid.lastThumb + IFLY.photovid.thumbsPerPage;
						var maxNum = (nextSet <= IFLY.photovid.totalThumbs) ? nextSet : IFLY.photovid.totalThumbs;
						var thumbSet = [];
						for(i=IFLY.photovid.lastThumb; i<maxNum; i++) {
							thumbSet.push(IFLY.photovid.session[i]);
						}
						//console.log('IFLY.photovid.lastThumb', IFLY.photovid.lastThumb,'maxNum',maxNum,'thumbSet',thumbSet.length);
						IFLY.photovid.lastThumb = maxNum;
						IFLY.photovid.displayThumbs(disDiv,thumbSet);
						if(IFLY.photovid.lastThumb == IFLY.photovid.totalThumbs) $('.footer-nav a.more').css('display','none');
					} 
					addMore(tDiv);
					
					// return thumbSet;
				} else {
					$('.footer-nav a.more').css('display','none');	
						IFLY.photovid.displayThumbs(tDiv,data);
					// return data;
				}

	},
	initVideoTestimonial: function() {
		//
	},
	displayCartOverlay: function(index,imgPath,thumbPath) {
		console.log('displayCartOverlay', IFLY.photovid.session[index]);
		var currentSession = IFLY.photovid.session[index];
		var flyerNum = (currentSession.person_ct) ? 'Flyer #'+parseInt(currentSession.person_ct): '';
		var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
		var localSite = 'https://'+selTunnelInfo.subdomain+'.iflyworld.com/book-now/booking-step5';
		
		var mediaTitle = (currentSession.file_name) ? currentSession.file_name.replace("_","").replace(".JPG","") : currentSession.id;
		
		var overlayHTML = '<a class="btn-prev" href="#">&lt;</a><a class="btn-next" href="#">&gt;</a>\
						   <div class="photo-share"><a class="btn" href="javascript:void(0);">SHARE</a></div>\
						   <div class="overlay-info"><p>'+ mediaTitle +'</p><p>'+flyerNum+'</p></div>\
						   <img class="gradient-border drop-shadow rounded-corners" id="overlay-img" width="600"  src="'+imgPath+'" />\
						   <div id="photo-cart" class="row"><p>$'+IFLY.photovid.price+'</p><a class="photo-add" title="Add this to your cart">Add to cart</a></div><div class="row checkout disabled"><a href="'+localSite+'" class="btn green"><em></em><span>CHECKOUT</span></a></div>';
		$.colorbox({width:700, height: 600, html:overlayHTML, onComplete:function() {
			
			$('div.photo-share a.btn').unbind('click').click(function(e) {
					e.preventDefault();
					IFLY.photovid.callFace(imgPath,index);
			});
			$('a.photo-add').click(function(e) {
				e.preventDefault();
				var x = IFLY.photovid.session[index].file_name;
				var imageID = IFLY.photovid.session[index].id;
			//	var imageID = x.replace('_','').replace('.JPG','');
				var imageFile = IFLY.photovid.session[index].file_name;
				$(this).addClass('disabled').after('<br />'+IFLY.showLoader());
				var x = (cookieHandler.read('wwsale_id')) ? cookieHandler.read('wwsale_id'): null;
					//console.log('ADD CLICKED', x);
				if(x == null) {
					var call='method=create_wwsale_id&controller=siriusware&format=json&tunnel='+$('select[id$="flyer-location"] option:selected').val();
					console.log('call',call);
					function success(response, textStatus, jqXHR){
							console.log('success',response, textStatus, jqXHR);
							urlParams.wwsale_id = response.data;
							cookieHandler.create('wwsale_id',urlParams.wwsale_id);
							cookieHandler.create('ifly_tunnel',$('select[id$="flyer-location"] option:selected').val());

							IFLY.photovid.addToCart(imageID,imageFile,'photos', thumbPath);
					}
					IFLY.photovid.callApi(call, success,null,null);
			
					} else{
						urlParams.wwsale_id = x;
						IFLY.photovid.addToCart(imageID,imageFile,'photos', thumbPath);
					}
			});
		}});
		
	},
	displayVideoCartOverlay: function(index, vidPath,thumbPath) {
		console.log('displayVideoCartOverlay', IFLY.photovid.session[index]);
		var currentSession = IFLY.photovid.session[index];
		var flyerNum = (currentSession.person_ct) ? 'Flyer #'+parseInt(currentSession.person_ct): '';
		var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
		var localSite = 'https://'+selTunnelInfo.subdomain+'.iflyworld.com/book-now/booking-step5';

		var sourceImg = IFLY.photovid.basePath + currentSession.lores_url;
		sourceImg = sourceImg.replace('.flv','.png');
		var overlayHTML = '<a class="btn-prev" href="#">&lt;</a><a class="btn-next" href="#">&gt;</a><div id="vHolder"><video id="videoHolder" class="video-js vjs-ifly-skin gradient-border drop-shadow rounded-corners" controls preload="auto" width="600" height="340" poster="'+sourceImg+'">' + '<source src="'+vidPath+'" type=\'video/x-flv\' /></video><div class="video-share"><a class="btn" href="javascript:void(0);">SHARE</a></div></div><div class="overlay-info vid"><p>'+currentSession.id+'</p><p>'+flyerNum+'</p></div><div id="photo-cart" class="row"><p>$'+IFLY.photovid.price+'</p><a class="photo-add" title="Add this to your cart">Add to cart</a></div><div class="row checkout disabled"><a href="'+localSite+'" class="btn green"><em></em><span>CHECKOUT</span></a></div>';
//		console.log('displayVideoCartOverlay', index, vidPath, sourceImg);
		$.colorbox({
			width:700,
			height: 600,
			html:overlayHTML,
			onComplete:function() {
//				$('#photo-cart p').html(IFLY.photovid.price);
				if(!swfobject.hasFlashPlayerVersion("9.0.115")) {
					IFLY.vidcontrols.init(vidPath,'videoHolder','.video-controls a.play.btn','.video-controls a.pause.btn','.video-controls div.scrubber');
				} else {
					
					$('#vHolder').css('background-image','url('+sourceImg+')');
					var flashvars = {
							vidPath: vidPath,
							posterPath: sourceImg
						};
					var params = {/*bgcolor: "#222222",*/ wmode: "transparent", quality:"high",allowscriptaccess:"always",allownetworking:"all"};
					var attributes = {swliveconnect:"true"};
					swfobject.switchOffAutoHideShow();
					swfobject.embedSWF( 'http://www.iflyworld.com/swf/ifly-videoplayer.swf', "vHolder", "600", "340", "9.0.0","expressInstall.swf", flashvars, params, attributes);
					
//					$('#vHolder').flash({
//						swf: '/swf/ifly-videoplayer.swf',
//						width: 600,
//						height: 340,
//						allowFullScreen: true,
//						wmode: 'transparent',
//						allowscriptaccess: 'always',
//						quality:'high',
//						swLiveConnect: true,
//						encodeParams: false,
//						flashvars: {
//							vidPath: vidPath,
//							posterPath: sourceImg
//	//						autoPlayFlag: autoFlag,
//	//						campaignName: SecondaryPage.googleCampaignName,
//	//						campaignEvent: SecondaryPage.slug,
//	//						embedCode: embed
//						}
//					});	
					$('#vHolder').addClass('gradient-border').addClass('drop-shadow').addClass('rounded-corners');
				
				}
				$('div.video-share a.btn').unbind('click').click(function(e) {
						IFLY.photovid.callFace(sourceImg, index);
						e.preventDefault();
				});
				$('a.photo-add').click(function() {
					var imageID = IFLY.photovid.session[index].id;
					var imageFile = IFLY.photovid.session[index].data;
					
					var x = (cookieHandler.read('wwsale_id')) ? cookieHandler.read('wwsale_id'): null;
	
					$(this).addClass('disabled').after('<br />'+IFLY.showLoader());
	
					if(x == null) {
						var call='method=create_wwsale_id&controller=siriusware&format=json&tunnel='+$('select[id$="flyer-location"] option:selected').val();
						console.log('call',call);
						function success(response, textStatus, jqXHR){
								console.log('success',response, textStatus, jqXHR);
								urlParams.wwsale_id = response.data;
								cookieHandler.create('wwsale_id',urlParams.wwsale_id);
								cookieHandler.create('ifly_tunnel',$('select[id$="flyer-location"] option:selected').val());

								IFLY.photovid.addToCart(imageID,imageFile,'videos',thumbPath);
						}
						IFLY.photovid.callApi(call, success,null,null);
				
					} else{
						urlParams.wwsale_id = x;
						IFLY.photovid.addToCart(imageID,imageFile,'videos',thumbPath);
					}
				});
				
				
		}});
	},
	addToCart: function(assetID,fName,fType, thumbPath) {
		console.log('fName',fName);
		var fileName = fName.split('.');
		var call='method=get_all_flights&controller=siriusware&format=json&flyer_type='+fType+'&item_type=item&tunnel='+$('select[id$="flyer-location"] option:selected').val();
	//	var call='method=get_DCI&controller=siriusware&format=json&flyer_type=MEDIA&item_type=flight&tunnel='+IFLY.current_tunnel;
		
		console.log('get_all_flights call: ',call);
		function success(response, textStatus, jqXHR){
			console.log('success response: ',response);
			IFLY.photovid.photoDCI = urlParams.padded_DCI = response.data[0].padded_DCI;
			console.log('get_all_flights response IFLY.photovid.photoDCI: ',IFLY.photovid.photoDCI)
			var call2='method=add_to_cart&controller=siriusware&format=json&voucher_number=&tunnel='+$('select[id$="flyer-location"] option:selected').val()+'&flyer_num=&qty=1&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+IFLY.photovid.photoDCI+'&session_time=&flight_date=&gc_amount=&token=&asset_id='+assetID+'&asset_info='+fileName[0]+'&asset_url='+thumbPath;
			//'method=add_to_cart&controller=siriusware&format=json&paddedDCI='+IFLY.photovid.photoDCI+'&guest_id=&asset_id='+assetID+'&processed=0&tunnel='+$('select[id$="flyer-location"] option:selected').val();
			console.log('call',call2);
			IFLY.booking.callApi(call2,addSuccess,addError,null);
			
			//callAdd('method=add_to_cart&controller=siriusware&format=json&voucher_number=&tunnel='+urlParams.tunnel+'&flyer_num='+urlParams.flyer_num+'&qty='+urlParams.qty+'&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+urlParams.padded_DCI+'&session_time='+urlParams.session_time+'&flight_date='+urlParams.flyer_date+'&gc_amount='+urlParams.gc_amount+'&token='+urlParams.token);
	
		function addSuccess(response, textStatus, jqXHR){
			console.log('addSuccess:',response);
	
			IFLY.killLoader();
			$('#photo-cart a.photo-add').addClass('added');

			var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode($('select[id$="flyer-location"] option:selected').val());
			var localSite = 'https://'+selTunnelInfo.subdomain+'.iflyworld.com/book-now/booking-step5?wwsale_id='+urlParams.wwsale_id+'&tunnel='+$('select[id$="flyer-location"] option:selected').val();
			$('div.row.checkout a, a#checkout').attr('href', localSite);
			console.log('localSite',localSite);
			$('div.row.checkout').removeClass('disabled');
			
			//$('div#photo-cart').after('<div class="row checkout disabled"><a href="/book-now/booking-step5" class="btn"><em></em><span>CHECKOUT</span></a></div>')
			IFLY.booking.checkCartStatus();
			if(response.status =="ERR") {
				cookieHandler.remove('wwsale_id');
				IFLY.booking.showApiErrorMessage(response.message, null)
			} else {

					try {
						delete urlParams.qty;
						delete urlParams.padded_DCI;
						delete urlParams.session_time;
						delete urlParams.session_minutes;
						delete urlParams.flyer_date;
						delete urlParams.gc_amount;
						delete urlParams.token;
					} catch(error) {
						console.log(error);	
					}
			}
		}
		function addError(response, textStatus, jqXHR){
			var data = $(response.responseText)
			var locRef = $.inArray('data', data)
			console.log('error response: ',response);
			if(locRef >-1) {
					var dataCap = $(data[locRef]);
					//console.log('dataCap',dataCap);
			}
			
		}
			//	cookieHandler.create('wwsale_id',urlParams.wwsale_id);
			//	IFLY.booking.addToCart(true,true);
		}
		IFLY.photovid.callApi(call, success,function(response, textStatus, jqXHR){console.log('get_all_flights response error: ',response, textStatus, jqXHR);},null);
	},
	initOverlayButtons: function() {
		
	},
	initOverlayArrows: function() {
			if(IFLY.photovid.sessionIndex < IFLY.photovid.session.length) {
				$('a.btn-next').css('display','block');
			} else {
				$('a.btn-next').css('display','none');
			}
			if(IFLY.photovid.sessionIndex >0){
				$('a.btn-prev').css('display','block');
			} else {
				$('a.btn-prev').css('display','none');
			}
		$('a.btn-next').click(function (e) {
			if(IFLY.photovid.sessionIndex < IFLY.photovid.session.length) {
				IFLY.photovid.sessionIndex ++;
				$('div#photo-cart').after(IFLY.showLoader());
				changeOverlay();
			}
			e.preventDefault();
		});
		$('a.btn-prev').click(function (e) {
			if(IFLY.photovid.sessionIndex >0){
				IFLY.photovid.sessionIndex --;
				$('div#photo-cart').after(IFLY.showLoader());
				changeOverlay();
			}
			e.preventDefault();
		});
		function changeOverlay() {
			$('#photo-cart a.photo-add').removeClass('added').removeClass('disabled');
			$('div.row.checkout').addClass('disabled');

			var currentSession = IFLY.photovid.session[IFLY.photovid.sessionIndex];
			
			var flyerNum = (currentSession.person_ct) ? 'Flyer #'+parseInt(currentSession.person_ct): '';
			
			var mediaTitle = (currentSession.file_name) ? currentSession.file_name.replace("_","").replace(".JPG","") : currentSession.id;

			$('.overlay-info').html('<p>'+ mediaTitle +'</p><p>'+flyerNum+'</p>');


			//IMAGE OVERLAY
			if($('img#overlay-img').length>0){
				var overlayPath = (currentSession.medres_url) ? currentSession.medres_url : currentSession.lores_url;
				
				$('img#overlay-img').attr('src',IFLY.photovid.basePath+overlayPath).load(function() {
					IFLY.killLoader();
				});
				$('div.photo-share a.btn').unbind('click').click(function(e) {
						e.preventDefault();
						IFLY.photovid.callFace(overlayPath,IFLY.photovid.sessionIndex);
				});
			} else {
			//VIDEO OVERLAY	
			
				var overlayPath = (currentSession.medres_url) ? currentSession.medres_url : currentSession.lores_url;
				IFLY.killLoader();
				var sourceImg = IFLY.photovid.basePath + currentSession.lores_url;
				sourceImg = sourceImg.replace('.flv','.png');
				var vidPath = (currentSession.medres_url) ? currentSession.medres_url : currentSession.lores_url;
				if(!swfobject.hasFlashPlayerVersion("9.0.115")) {
					$('video#videoHolder').attr('poster',sourceImg).load(function() {
						IFLY.killLoader();
					});
					IFLY.vidcontrols.videoPlayer.src = IFLY.photovid.basePath+overlayPath;
				} else {
					IFLY.vidcontrols.videoPlayer = swfobject.getObjectById("vHolder");
					IFLY.vidcontrols.videoPlayer.loadMedia(IFLY.photovid.basePath+vidPath,true);

				}
				$('div.photo-share a.btn').unbind('click').click(function(e) {
						e.preventDefault();
						IFLY.photovid.callFace(vidPath,IFLY.photovid.sessionIndex);
				});
	//			$('video#videoHolder source').attr('src',IFLY.photovid.basePath+overlayPath);
			}
			
			if(IFLY.photovid.sessionIndex < IFLY.photovid.session.length) {
				$('a.btn-next').css('display','block');
			} else {
				$('a.btn-next').css('display','none');
			}
			if(IFLY.photovid.sessionIndex >0){
				$('a.btn-prev').css('display','block');
			} else {
				$('a.btn-prev').css('display','none');
			}
			
		}

	},
	toggleCuratedMedia: function() {
		if($('a.btn.photo-toggle').hasClass('red')) {
			$('a.btn.photo-toggle').removeClass('red');
			$('a.btn.video-toggle').addClass('red');
			$('#grid-photo').css('display','none');
			$('#grid-video').css('display','block');
			IFLY.photovid.currentOverlayType = 'Video';
		} else {
			$('a.btn.video-toggle').removeClass('red');
			$('a.btn.photo-toggle').addClass('red');
			$('#grid-photo').css('display','block');
			$('#grid-video').css('display','none');
			IFLY.photovid.currentOverlayType = 'Photo';
		}
	},
	toggleMedia: function() {
		if($('a.btn.photo-toggle').hasClass('red')) {
			$('a.btn.photo-toggle').removeClass('red').addClass('inactive');
			$('a.btn.video-toggle').addClass('red').removeClass('inactive');
			$('#grid-photo').css('display','none');
			$('#grid-video').css('display','block');
			IFLY.photovid.initVideos();
		} else {
			$('a.btn.video-toggle').removeClass('red').addClass('inactive');
			$('a.btn.photo-toggle').addClass('red').removeClass('inactive');
			$('#grid-photo').css('display','block');
			$('#grid-video').css('display','none');
			IFLY.photovid.initPhotos();
		}
	},
	callFace: function(fIndex,refNum) {
//		javascript:callFace('How%20is%20Our%20Tax%20$%20Spent%20on%20Seed?','Over%20a%20three-year%20period,%20plant%20biotechnology%20research%20received%20$54%20million%20in%20public%20funding%20while%20only%20$775,000%20went%20to%20organic%20seed%20research%20–%20a%20disparity%20of%2070%20to%201.',2);
			var currentSession = IFLY.photovid.session[refNum];
			var f = currentSession.file_name.split('.');
			var d = currentSession.date_time.split(' ');
			
			var newURL = window.location.protocol + "//" + window.location.host + "/" + window.location.pathname+'?f='+f[0]+'&l='+IFLY.current_tunnel+'&d='+d[0];
			console.log('newURL',newURL);
			//var pathArray = window.location.pathname.split( '/' );
//			var host = 'http://'+pathArray[2];
//			console.log('host',host);
//			var obj = {
//				  method: 'feed',
//				  link: host,
//				  picture: fIndex,
//				  name: 'iFLY Indoor Skydiving',
//				  caption: 'Check me out, I\'m flying at iFLY Indoor Skydiving!'
////				  description: fText
//			};
				
			function callback(response) {
				console.log('callback '+response);
			  //document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
			}

//			console.log('callFace'+' '+obj.picture);
			//FB.ui(obj, callback);
			FB.ui({
					method: 'feed',
					name: 'iFLY Indoor Skydiving',
					link: newURL,
					picture: fIndex,
					caption: 'Check me out!',
					description: 'I\'m flying at iFLY Indoor Skydiving!'
			  }, callback);
	}
};		

IFLY.calendarWidget.initPhotoCalendar = function(date) {
	if($('body.photo-video #calendar-quickbook').length > 0)
	$('#calendar-quickbook').datepicker({
		dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
		buttonImageOnly:true,
		maxDate:0,
		onSelect: function(e) {
			//console.log('e: '+e);
			$('#book-date span').html(e);

			$('select#photovid-flight-time').html('<option selected="selected">TIME</option>');
			$('select#photovid-flight-time').attr("disabled",true);
			$('#uniform-photovid-flight-time').addClass('disabled');
			$.uniform.update();
			IFLY.photovid.baseDate = new Date(e);
			IFLY.photovid.getSessions(IFLY.photovid.baseDate,IFLY.photovid.showSessionTimes);
			//$('#calendar-quickbook').css('display','none');	
			$('#calendar-quickbook').hide();
		}
	});
	$(document).bind("click", function(e){
		var _target = $(e.target),
		_selector = "calendar-quickbook";
		
		if( _target.attr('id') !== "_selector" && _target.parents("#" + _selector).length <= 0 && !_target.hasClass('ui-datepicker-prev') && !_target.hasClass('ui-datepicker-next')) {
			$("#" + _selector).hide();	
		}
	});

	$('#calendar-quickbook').hide();
	$('#book-date').click(function(e) {
			$('#calendar-quickbook').show();
			e.preventDefault(); 
			e.stopPropagation();
		//	$('#calendar-quickbook').css('display','block');
	});
}

//$(document).bind("cbox_complete", function(){
//    var href = $.colorbox.element().attr("href");
//    if (href) {
//        _gaq.push(["_trackPageview", href]);
//    }
//});
//
//// traditional tracker
//$(document).bind("cbox_complete", function(){
//    var href = $.colorbox.element().attr("href");
//    if (href) {
//        pageTracker._trackPageview(href);
//    }
//});
$(document).ready(function() {
	IFLY.photovid.init();	
});
