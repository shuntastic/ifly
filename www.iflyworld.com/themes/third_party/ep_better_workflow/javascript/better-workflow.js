(function () {
'use strict';

  this.Bwf = {};
  
  Bwf.logEvents;
  
  Bwf.callbacks = {
    ajaxSave: {},
    previewClose: {}
  };

  /**
  * Bind
  */
  Bwf.bind = function(fieldtype, event, callback){
    if (typeof Bwf.callbacks[event] == 'undefined') return;
    Bwf.callbacks[event][fieldtype] = callback;
  };

  /**
  * Unbind
  */
  Bwf.unbind = function(fieldtype, event){
    if (typeof Bwf.callbacks[event] == 'undefined') return;
    if (typeof Bwf.callbacks[event][fieldtype] == 'undefined') return;
    delete Bwf.callbacks[event][fieldtype];
  };

  /**
  * Console.log which won't break older browsers
  */
  Bwf.debug = function(msg){
    if(this.logEvents){
      try {
        console.log(msg);
      }
      catch (e) {}
    }     
  };

}).call(window);
