<?php include("inc/head.php"); ?>
<body class="photo-video info-page">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">
			<div class="content-wrapper clearfix">
				<section class=" clearfix">
			     <h1 class="uppercase">Find Your Photos and Videos</h1>
						<form action="#" class="uniform find-photovid">
							<fieldset>
                                <div class="row">
                                 <?php include ('inc/form-elements/select-tunnel-locations.php'); ?>
                                </select><span class="vert-divider"></span>
									<input type="text" name="name" placeholder="Enter Reservation #" class="text"><span class="formtext">OR</span>
									<a href="#" class="btn date" id="book-date" title="Find the day that you flew" ><em></em><span>DATE</span></a>
                                    <div id="calendar-quickbook"></div>
                                <select name="FLIGHT TIME" title="Find the time that you flew" id="photovid-flight-time" class="">
                                    <option selected="selected" value="">TIME</option>
                                    <option>9:00 AM</option>
                                    <option>10:00 AM</option>
                                    <option>11:00 AM</option>
                                    <option>12:00 PM</option>
                                    <option>1:00 PM</option>
                                    <option>2:00 PM</option>
                                    <option>3:00 PM</option>
                                    <option>4:00 PM</option>
                                    <option>5:00 PM</option>
                                    <option>6:00 PM</option>
                                    <option>7:00 PM</option>
                                    <option>8:00 PM</option>
                                    <option>9:00 PM</option>
                                    <option>10:00 PM</option>
                                    <option>11:00 PM</option>
                                    <option>12:00 AM</option>
                                </select>
								</div>
							</fieldset>
						</form>
                        <div class="row media-toggle"><a href="javascript:void(0);" class="btn photo-toggle red"><em></em><span>PHOTOS</span></a><a href="javascript:void(0);" class="btn video-toggle"><em></em><span>VIDEOS</span></a> <div class="paginate"><a class="previous" href="javascript:void(0);">previous</a><span>1 of 4</span><a class="next" href="javascript:void(0);">next</a></div></div>

			<!--<img src="images/404.jpg" alt="404 error"  class="rounded-corners gradient-border">-->
			</section>
            <div class="grid-holder">
            <div id="grid-photo"></div>
            <div id="grid-video"></div>
            </div>
            <div class="footer-nav"><div class="paginate"><a class="previous" href="javascript:void(0);">previous</a><span>1 of 4</span><a class="next" href="javascript:void(0);">next</a></div></div>
            </div><!-- /.content-wrapper -->
            
			<div class="btn-block">
				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>
			</div><!-- /.btn-block -->
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
    <?php include("inc/scripts.php"); ?>
    <?php include("inc/modals.php"); ?>
	<script type="text/javascript" src="js/photovid.js"></script>
	
</body>
</html>