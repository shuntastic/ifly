<?php include("inc/head.php"); ?>
<body style="width : 960px; margin : 20px auto 0 auto;">

	<dl>
		<dt>one-off pages</dt>
		<dd><a href="/home.php">Home</a></dd>
		<dd><a href="/home-local.php">Home Local</a></dd>
		<dd><a href="/template-404.php">404</a></dd>
		<dd><a href="/tunnel-locator.php">Tunnel Locator</a></dd>
		<dd><a href="/template-photos-videos.php">Photo/Video Manager</a></dd>
		<dd><a href="/calendar-view.php">Calendar View</a></dd>
		<dd><a href="/ratings-and-reviews.php">Ratings &amp; Reviews</a></dd>
	</dl>
	<dl>
		<dt>template pages</dt>
		<dd><a href="/template-one-col.php">1 Column Template</a></dd>
		<dd><a href="/template-one-col-form.php">1 Column Template Form</a></dd>
		<dd><a href="/template-two-col.php">2 Column Template</a></dd>
	</dl>

	<dl>
		<dt>booking flow</dt>
		<dd><a href="/booking-step1.php">step 1</a></dd>
		<dd><a href="/booking-step1-overview.php">step 1 overview</a></dd>
		<dd><a href="/booking-step2.php">step 2</a></dd>
		<dd><a href="/booking-step3.php">step 3</a></dd>
		<dd><a href="/booking-step4.php">step 4</a></dd>
		<dd><a href="/booking-step5.php">step 5</a></dd>
		<dd><a href="/booking-step6.php">step 6</a></dd>
		<dd><a href="/booking-step7.php">step 7</a></dd>
	</dl>

	<dl>
		<dt>gifting flow</dt>
		<dd><a href="/gifting-step1.php">step 1</a></dd>
		<dd><a href="/gifting-step2.php">step 2</a></dd>
		<dd><a href="/gifting-step3.php">step 3</a></dd>
		
	</dl>
	
</body>
</html>