<?php if (! defined('APP_VER')) exit('No direct script access allowed');


/*
_|                                                      _|
_|_|_|     _|_|     _|_|   _|    _|   _|_|_| _|_|_|   _|_|_|_|
_|    _| _|    _| _|    _| _|    _| _|    _| _|    _|   _|
_|    _| _|    _| _|    _| _|    _| _|    _| _|    _|   _|
_|_|_|     _|_|     _|_|     _|_|_|   _|_|_| _|    _|     _|_|
                                 _|
                             _|_|

Description:		NavEE Extensions for Pixel & Tonic WYGWAM
Developer:			Booyant, Inc.
Website:			www.booyant.com/navee
Location:			./system/expressionengine/third_party/wygwam_navee/ext.wygwam_navee.php
Contact:			navee@booyant.com  / 978.OKAY.BOB
Version:			2.0.0
*/

class Wygwam_navee_ext {

	var $name           			= 'Wygwam / NavEE Links';
	var $version       				= '2.0.0';
	var $description    			= 'Adds a NavEE Type to Wygwams Link dialog';
	var $settings_exist 			= 'n';
	var $docs_url       			= 'http://booyant.com/navee/wygwam-extension';
	var $navee						= "NavEE";
	var $site_id					= "1";
	var $navee_array				= array();
	var $ee_install_directory		= "";
    var $include_index				= false;

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	C O N S T R U C T O R
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function __construct(){
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
		$this->site_id = $this->EE->config->item('site_id');
		
		// Determine if EE is installed in a subdirectory
		$this->EE->db->select("k,v");
		$this->EE->db->where("site_id", $this->site_id);
		$keys = array("include_index", "install_directory");
		$this->EE->db->where_in("k", $keys);
		$q = $this->EE->db->get("navee_config");
			
			if ($q->num_rows() > 0){
				foreach ($q->result() as $r){
					switch ($r->k) {
						case "install_directory":
							$this->ee_install_directory 	= $this->_formatInstallDirectory($r->v);
							break;
						case "include_index":
							$this->include_index			= $r->v;
							break;
					}
				}
			}
		$q->free_result();	
	
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	A C T I V A T E
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function activate_extension(){
		// add the row to exp_extensions
		$this->EE->db->insert('extensions', array(
			'class'    => get_class($this),
			'method'   => 'wygwam_config',
			'hook'     => 'wygwam_config',
			'settings' => '',
			'priority' => 10,
			'version'  => $this->version,
			'enabled'  => 'y'
		));
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
 	//	U P D A T E
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>	
	
	function update_extension($current = ''){
		return FALSE;
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	D I S A B L E
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function disable_extension(){
		// Remove all Wygwam_navee_ext rows from exp_extensions
		$this->EE->db->where('class', get_class($this))
		             ->delete('extensions');
	}


	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	C O N F I G   H O O K
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function wygwam_config($config, $settings){
		
		// If another extension shares the same hook,
		// we need to get the latest and greatest config
		if ($this->EE->extensions->last_call !== FALSE){
			$config = $this->EE->extensions->last_call;
		}

		$site_id  = $this->EE->config->item('site_id');
		$site_url = $this->EE->config->item('site_url');
		
		$this->EE->db->select("*");
		$this->EE->db->order_by("nav_name", "ASC");
		$this->EE->db->where("site_id", $this->site_id);
		$q = $this->EE->db->get("navee_navs");
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
		
				$nav = $this->_getNav($r->navigation_id);
				
				if (sizeof($nav)>0){
					// Let's create a header for each nav, if it contains any items	
						
					if (sizeof($this->navee_array)>0){
						array_push($this->navee_array, array(
							'label' => '----------',
							'url'   => ''
						));
					}
					
					array_push($this->navee_array, array(
						'label' => $r->nav_name,
						'url'   => ''
					));
					
					
					$this->_buildNav($nav);

				}
			}
		}
		
		$q->free_result();
		
		$config['link_types'][$this->navee] = $this->navee_array;
		return $config;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	B U I L D   N A V
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _buildNav($nav, $level=1){
		
		foreach ($nav as $k=>$v){
		
			array_push($this->navee_array, array(
				'label_depth' => $level,
				'label'       => $v["text"],
				'url'         => $v["link"]
			));
			
			if (sizeof($v["kids"])>0){
					$this->_buildNav($v["kids"], $level+1);
			}
				
		}
				
	
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   N A V
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getNav($navId, $parent=0, $recursive=true, $ignoreInclude=false, $skipOwnKids=false, $skipID=0){

		$nav = array();
		$pages 		= $this->EE->config->item('site_pages');
		$pages 		= $pages[$this->site_id]["uris"];

		$this->EE->db->select("n.navee_id,
								n.parent,
								n.text,
								n.link,
								n.class,
								n.id,
								n.sort,
								n.include,
								n.passive,
								n.rel,
								n.name,
								n.target,
								n.regex,
								n.entry_id,
								n.channel_id,
								n.template,
								n.type,
								n.custom,
								n.custom_kids,
								t.template_name,
								tg.group_name,
								ct.url_title,
								nn.nav_title");
		$this->EE->db->from("navee AS n");
		$this->EE->db->join("templates AS t", "n.template=t.template_id", "LEFT OUTER");
		$this->EE->db->join("template_groups AS tg", "t.group_id=tg.group_id", "LEFT OUTER");
		$this->EE->db->join("channel_titles AS ct", "n.entry_id=ct.entry_id", "LEFT OUTER");
		$this->EE->db->join("navee_navs AS nn", "n.navigation_id=nn.navigation_id");
		$this->EE->db->where("n.navigation_id", $navId);
		$this->EE->db->where("n.parent", $parent);
		$this->EE->db->where("n.site_id", $this->site_id);
		$this->EE->db->order_by("n.sort", "asc");
		
		
		if (!$ignoreInclude){
			$this->EE->db->where("n.include", 1);
		}
		$q = $this->EE->db->get();
		if ($q->num_rows() > 0){
			$count = 0;
			foreach ($q->result() as $r){
			
				// Build link based on which type it is
					switch ($r->type) {
						case "guided":
							$nav[$count]["link"] = "";
							
							// add install directory if necessary
							if (strlen($this->ee_install_directory)>0){
								$nav[$count]["link"] .= "/".$this->ee_install_directory;
							}
							
							// add index if necessary
							if ($this->include_index == "true"){
								$nav[$count]["link"] .= "/".$this->EE->config->item('index_page');
							}
						
							// template group
							$nav[$count]["link"] 		.= "/".$r->group_name;
							
							// template
							if ($r->template_name !== "index"){
								$nav[$count]["link"] 	.= "/".$r->template_name;
							}
							
							// url_title
							if (strlen($r->url_title)>0){
								$nav[$count]["link"] 	.= "/".$r->url_title;
							}
							
							break;
						case "pages":
							$nav[$count]["link"] = "";
							
							// add install directory if necessary
							if (strlen($this->ee_install_directory)>0){
								$nav[$count]["link"] .= "/".$this->ee_install_directory;
							}
							
							// add index if necessary
							if ($this->include_index == "true"){
								$nav[$count]["link"] .= "/".$this->EE->config->item('index_page');
							}
							
							// pages content
							$nav[$count]["link"] .= $pages[$r->entry_id];
							
							break;
						default:
							$nav[$count]["link"] 		= $r->link;
							break;
					}
			
				$nav[$count]["navee_id"] 	= $r->navee_id;
				$nav[$count]["parent"] 		= $r->parent;
				$nav[$count]["text"] 		= $r->text;
				$nav[$count]["class"] 		= $r->class;
				$nav[$count]["id"] 			= $r->id;
				$nav[$count]["sort"] 		= $r->sort;
				$nav[$count]["include"] 	= $r->include;
				$nav[$count]["rel"]			= $r->rel;
				$nav[$count]["name"]		= $r->name;
				$nav[$count]["target"]		= $r->target;
				$nav[$count]["regex"]		= $r->regex;
				$nav[$count]["nav_title"]	= $r->nav_title;

				if ($recursive){
					if ($skipOwnKids){
						if ($skipID == $r->navee_id){
							$nav[$count]["kids"] = array();
						} else {
							$nav[$count]["kids"] = $this->_getNav($navId, $r->navee_id, $recursive, $ignoreInclude, $skipOwnKids, $skipID);
						}
					} else {
						$nav[$count]["kids"] = $this->_getNav($navId, $r->navee_id, $recursive, $ignoreInclude, $skipOwnKids, $skipID);
					}
					
				} else {
					$nav[$count]["kids"] = array();
				}
				$count++;
			}
		}
		$q->free_result();

		return $nav;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	F O R M A T   I N S T A L L   D I R E C T O R Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _formatInstallDirectory($dir){
		$dir = preg_replace('/^\//i', '', $dir);
		$dir = preg_replace('/\/$/i', '', $dir);
		return $dir;	
	}
	
}