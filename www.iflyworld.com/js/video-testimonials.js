var pages = 1;
var vidID;
var videoID;
var VT_MAX_RESULTS_PER_PAGE = 25;
 
 IFLY.videotestimonials = {
	thumbsPerPage: 25,
	playlists: [],
	thumbIndex: 1,
	currentPage: 1,
	currentPlaylist: 1,
	basePath: 'http://www.iflyworld.com/php-lib/videos-by-tags.php',
	callApi: function(callStr,callbackfunc,onError,onComplete) {
		try {
			$.ajax({
				url: 'http://www.iflyworld.com/php-lib/videos-by-tags.php',
				type: 'post',
				data: callStr,
				success: callbackfunc,
				error: (onError != null) ? onError : handler.onError,
				complete: (onComplete != null) ? onComplete : handler.onComplete
			});
	
		  } catch(err) {
			  console.log('CALLBACK ERROR', err);
		  //Handle errors here
		  }	
	},

	 // http://dev.iflyworld.com/php-lib/videos-by-tags.php?tunnel=aus&playlist=2&page=2&debug=1
	init: function() {
        //console.log('current host:' + host);
        var tunnel = null;
        var default_tunnel = 'ifo';
        
//        var maxResults = VT_MAX_RESULTS_PER_PAGE;
//        var resultCount = 0;
//        var page = 1;

		$('#search_results').masonry({
			itemSelector : '.item'
		});

		var params = vt_getUrlVars();
		vidID = (params['videoID'] != undefined) ? params['videoID'] : (params['fb_ref']) ? params['fb_ref'] : undefined;
		
		tunnel = params['tunnel'];
		if (tunnel != undefined) {
			$("select.locations").val(tunnel).change();
		} else {
			tunnel = $("select.locations").val();
		}
		// if the user changes tunnels, stop videos and get results for that tunnel...
		$("select.locations").change(function(e) {
			tunnel = $("select.locations").val();
			// stop the video player if it was running
			player = document.getElementById("player");	
			if (player != undefined && isObject("player")) {
				player.stopVideo();
			}
			IFLY.videotestimonials.loadResultsPage(tunnel, IFLY.videotestimonials.currentPage);
		});
		IFLY.videotestimonials.loadResultsPage(tunnel, IFLY.videotestimonials.currentPage);
		if(vidID != undefined) {
			IFLY.videotestimonials.displayTestimonialsOverlay(vidID);
		}
		
	},
	loadResultsPage: function(tunnel, page) {
	 //   console.log('loadResultsPage called with maxResults:' + maxResults + ' and page:' + page);
	 //playlist=2&page=2&debug=1&month=january
	IFLY.videotestimonials.currentPlaylist = 1;
	IFLY.videotestimonials.currentPage = 1;
	IFLY.videotestimonials.playlists = [];
	IFLY.killLoader();
	$('#search_results').after(IFLY.showRevLoader());
		var call = 'queryType=all&page='+IFLY.videotestimonials.currentPage+'&debug=1&tunnel='+tunnel+'&playlist='+IFLY.videotestimonials.currentPlaylist.toString();
		console.log('call',call);
		IFLY.videotestimonials.callApi(call,success,null,null);
		function success(output) {
			// data contains a count and the array of results...
			console.log('output:',output);
			var response = $.parseJSON(output);
			console.log('loadResultsPage:',response);
			if(response.total_videos ==0) {
				IFLY.killLoader();
				$('.footer-nav a.btn.more').css('display','none');
				$('#search_results').html('<h2>No images were returned for this session.<br />Please select a new location from the dropdown above.</h2>').css('display','block');
			} else {
				
				$.each(response.playlists, function(index,value) {
					IFLY.videotestimonials.playlists.push(parseInt(value));
				});
				$('#search_results').html('');
				IFLY.videotestimonials.currentPlaylist = parseInt(response.current_playlist);
				IFLY.videotestimonials.currentCount = IFLY.videotestimonials.playlists[0];
	
				IFLY.videotestimonials.initPagination(response.media,'#search_results');
			}
		}
		
//		$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: IFLY.videotestimonials.currentPage, tunnel: tunnel, playlist:IFLY.videotestimonials.currentPlaylist.toString()},  function(output) {
//			// data contains a count and the array of results...
//			var response = $.parseJSON(output);
//			console.log('loadResultsPage:',response);
//			$.each(response.playlists, function(index,value) {
//				IFLY.videotestimonials.playlists.push(parseInt(value));
//			});
//			$('#search_results').html('');
//			IFLY.videotestimonials.currentPlaylist = parseInt(response.current_playlist);
//			IFLY.videotestimonials.currentCount = parseInt(response.current_playlist_count);
//
//			IFLY.videotestimonials.initPagination(response.media,'#search_results');
//		});

	},	
	addResults: function() {
		IFLY.killLoader();
		$('#search_results').after(IFLY.showRevLoader());
		var queryObj = {};
		
		IFLY.videotestimonials.currentPage+=1;
		
		if ((IFLY.videotestimonials.currentPage*IFLY.videotestimonials.thumbsPerPage-IFLY.videotestimonials.currentCount) > IFLY.videotestimonials.thumbsPerPage){
	//	if (IFLY.videotestimonials.currentPage*IFLY.videotestimonials.thumbsPerPage > IFLY.videotestimonials.currentCount) {
			IFLY.videotestimonials.currentPlaylist+=1;
			IFLY.videotestimonials.currentPage = 1;
			
			if(IFLY.videotestimonials.currentPlaylist <= IFLY.videotestimonials.playlists.length) {

				function addSuccess(output) {
					var response = $.parseJSON(output);
					console.log('addResults:',response);
					IFLY.videotestimonials.currentCount = IFLY.videotestimonials.playlists[IFLY.videotestimonials.currentPlaylist-1];
					IFLY.videotestimonials.displayThumbs('#search_results',response.media);
//					IFLY.killLoader();
				}


				var call = 'queryType=all&page='+IFLY.videotestimonials.currentPage+'&debug=1&tunnel='+$("select.locations").val()+'&playlist='+IFLY.videotestimonials.currentPlaylist.toString();
				console.log('call',call);
				IFLY.videotestimonials.callApi(call,addSuccess,null,null);
//				$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: IFLY.videotestimonials.currentPage, tunnel: $("select.locations").val(), playlist:IFLY.videotestimonials.currentPlaylist.toString()},  function(output) {
//					var response = $.parseJSON(output);
//					console.log('addResults:',response);
//					IFLY.videotestimonials.currentCount = parseInt(IFLY.videotestimonials.playlists[IFLY.videotestimonials.currentPlaylists]);
//					IFLY.videotestimonials.displayThumbs('#search_results',response.media);
//					IFLY.killLoader();
//				});
			} else {
				//IFLY.videotestimonials.currentPage = 1;
				 $('.footer-nav a.btn.more').css('display','none');
				IFLY.killLoader();
			}
		} else {
				function addResultsSuccess(output) {
					var response = $.parseJSON(output);
					console.log('addResults:',response);
					IFLY.videotestimonials.displayThumbs('#search_results',response.media);
//					IFLY.killLoader();
				}
				var call = 'queryType=all&page='+IFLY.videotestimonials.currentPage+'&debug=1&tunnel='+$("select.locations").val()+'&playlist='+IFLY.videotestimonials.currentPlaylist.toString();
				console.log('call',call);
				IFLY.videotestimonials.callApi(call,addResultsSuccess,null,null);
//			$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: IFLY.videotestimonials.currentPage, tunnel: $("select.locations").val()},  function(output) {
//				var response = $.parseJSON(output);
//				console.log('addResults:',response);
//				IFLY.videotestimonials.displayThumbs('#search_results',response.media);
//				IFLY.killLoader();
//			});
		}
	},	
	initPagination: function(data,tDiv) {
			$('.footer-nav a.btn.more').css('display','inline-block');
			IFLY.photovid.clearItems(tDiv);
			//console.log('initPagination',data);
				
			//IFLY.videotestimonials.currentCount = data.length;
			//IFLY.photovid.thumbIndex = 1;
			IFLY.videotestimonials.displayThumbs(tDiv,data);
			
			$('.footer-nav a.more').unbind('click').click(function(e) {
				addMore();
				e.preventDefault();
			});

//			if (IFLY.videotestimonials.currentPage*VT_MAX_RESULTS_PER_PAGE < IFLY.videotestimonials.currentCount) {
//				$('.footer-nav a.more').css('display','inline-block');	
//			} else {
//				$('.footer-nav a.more').css('display','none'); 
//			}

			$(window).unbind('scroll').scroll(function() {
			   if($(window).scrollTop() + $(window).height() == $(document).height()) {
					addMore();
			   }
			});
		
			function addMore(){
			//	IFLY.videotestimonials.thumbIndex +=1;
				IFLY.videotestimonials.addResults();
				//IFLY.videotestimonials.displayThumbs(disDiv,thumbSet);
//				if (IFLY.videotestimonials.thumbIndex*IFLY.videotestimonials.thumbsPerPage < IFLY.videotestimonials.currentCount) {
//					$('.footer-nav a.more').css('display','inline-block');	
//				} else {
//					$('.footer-nav a.more').css('display','none'); 
//				}
			} 
				//addMore(tDiv);
				
	},
	displayThumbs: function(tDiv, data) {
		//$(tDiv).html('');
		var thumbs = '';
		//console.log('displayThumbs', data)
        $.each(data, function(key, value) {  
            if(typeof value.videoThumb != 'undefined'){
                videoID = value.videoID;
                vidID = videoID;
                title = value.videoTitle;  
                title = title.split('\'s Flight Video ');
				var date = title[1];
				var name = title[0].split(' ');
				name = name[0];
//                title = title.replace(/ Flight Video /,'');
//                title = title.replace(/\'s/,'').replace(/ January/,'<br />January').replace(/ February/,'<br />February').replace(/ March/,'<br />March').replace(/ April/,'<br />April').replace(/ May/,'<br />May').replace(/ June/,'<br />June').replace(/ July/,'<br />July').replace(/ August/,'<br />August').replace(/ September/,'<br />September').replace(/ October/,'<br />October').replace(/ November/,'<br />November').replace(/ December/,'<br />December');
				var textOverlay = name+'<br />'+date;
                url = value.videoURL; 
                description = value.videoDescription;
                thumb = value.videoThumb;
                if (title != "iFLY Austin August") {	
					thumbs+='<div class="item"><span>'+textOverlay+'</span><a data-vid="'+videoID+'" href="#"><img src="'+thumb+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';	
                }
            }
        });
        $(tDiv).fadeIn(500);
		$(tDiv).imagesLoaded(function(){
			var $thumbs = $(thumbs);
			$(tDiv).append($thumbs).masonry('appended', $thumbs).masonry( 'reload' );

			//PHOTO CART OVERLAY
			$('.item a').each(function(index,value){
				$(this).unbind('click').click(function(e) {
					e.preventDefault();
					var vid = $(this).data('vid');
					IFLY.videotestimonials.displayTestimonialsOverlay(vid);
				});
			});
			IFLY.killLoader();
		});
	},
	displayTestimonialsOverlay: function(id) {
		videoID = id;
		//console.log('clicked on setVideo for id:' + id);
		
		var nURL = 'http://'+window.location.hostname+'/what-is-ifly/video-testimonials?videoID='+id;
		//	var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="'+nURL+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-show-faces="false" /><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a>';
		var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="'+nURL+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-show-faces="false" ref="'+id+'" /><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a>';
	  // var shareString = '<div class="video-share"><div class="fb-like fb_edge_widget_with_comment fb_iframe_widget"><iframe width="450" height="205" src="http://www.facebook.com/plugins/like.php?api_key=409878702400471&amp;locale=en_US&amp;sdk=joey&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D17%23cb%3Df39bc13f6085e52%26origin%3Dhttp%253A%252F%252F'+window.location.hostname+'%252Ff1a07571aee4992%26domain%3D'+window.location.hostname+'%26relation%3Dparent.parent&amp;href='+nURL+'&amp;node_type=link&amp;layout=button_count&amp;colorscheme=light&amp;show_faces=false&amp;send=true&amp;link='+nURL+'&amp;extended_social_context=false" class="fb_ltr" title="Like this content on Facebook." name="f3cfbf57a6a4502" id="f18eeda2c1537d8" scrolling="no"></iframe></div><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a></div>';
	//   var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="'+nURL+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-show-faces="false"><iframe src="http://www.facebook.com/plugins/like.php?api_key=409878702400471&amp;locale=en_US&amp;sdk=joey&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D17%23cb%3Df39bc13f6085e52%26origin%3Dhttp%253A%252F%252F'+window.location.hostname+'%252Ff1a07571aee4992%26domain%3D'+window.location.hostname+'%26relation%3Dparent.parent&amp;href='+nURL+'&amp;node_type=link&amp;width=150&amp;layout=button_count&amp;colorscheme=light&amp;show_faces=false&amp;send=true&amp;extended_social_context=false" class="fb_ltr" title="Like this content on Facebook." name="f3cfbf57a6a4502" id="f18eeda2c1537d8" scrolling="no"></iframe></div><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a></div>';
		var vWidth = (IFLY.mobileOn()) ? '400' : '840';
		var vHeight = (IFLY.mobileOn()) ? '238' : '500';
		
		var overlayHTML = '<div id="like_screen"></div><div id="vHolder" class="gradient-border drop-shadow rounded-corners"><object id="ytplayer_ie"  width="'+vWidth+'" height="'+vHeight+'">\
							<param name="allowscriptaccess" value="always" />\
							<param name="src" value="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer_ie&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" />\
							<param name="allowfullscreen" value="true" />\
							<embed id="ytplayer" width="'+vWidth+'" height="'+vHeight+'" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer&hl=en_US&autoplay=1&autohide=1&rel=0&wmode=transparent&theme=light&color=red&hd=1&modestbranding=1" allowFullScreen="true" allowscriptaccess="always" allowfullscreen="true" /></object></div>'+shareString;
			//console.log('displayVideoCartOverlay overlayHTML:', overlayHTML);
		$.colorbox({
			width:(IFLY.mobileOn()) ? 453 : 900,
			height: (IFLY.mobileOn()) ? 300 : 700,
			html:overlayHTML,
			onComplete:function() {
				FB.XFBML.parse(document.getElementById('colorbox'));
				//var getPlayer = document.getElementById('ytplayer');
				//ytplayer.addEventListener("onStateChange", "IFLY.videotestimonials.showEndFrameCTA");
				$('.fb-like').click(function() {
					IFLY.analytics.sendSocial('Facebook', 'Like', 'Video Testimonial ID#: '+id, 'http://'+window.location.hostname+'/video-reviews/index.php?videoID='+id);
				});
				$('a#vt_twitter').click(function(e) {
					e.preventDefault();
					sendTwitter(id);
					IFLY.analytics.sendSocial('Twitter','Share','Video Testimonial ID#: '+id, 'http://'+window.location.hostname+'/video-reviews/index.php?videoID='+id);
				});
				try{
					function yt_callback(category,action,label,value){
						_gaq.push(['_trackEvent',category,action,label,value]);
					}
					var ytTracker = new YoutubeTracker(false, yt_callback, true);
				}catch(e){}

			},
			onClose: function() {
				IFLY.youtube.destroy("ytplayer");
			}
		});
	},
	showEndFrameCTA: function(newState) {
		if(newState == 0) {
			 $('div#like_screen').fadeIn(500);	
		}
	}

}
  
function chooseDefaultTunnel(default_tunnel) {
    $("select.locations").val(default_tunnel).change();
    var selected_tunnel = $("select.locations").val();
    console.log('forced tunnel to:' + selected_tunnel);
    tunnel = selected_tunnel;
    
    return default_tunnel;
}  


function hideSocial() {
    //console.log('HIDESOCIAL CALLED');
    $('#vt_social').css('left','-3000px');
    //$('#vt_social').hide();
}  

function showSocial() {
    console.log('SHOWSOCIAL CALLED');
    $('#vt_social').css('left','0px');
    //$('#vt_social').show();
}
  
// default video (joe jennings) -- deprecated
function gup( name ) {
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    // explode the URL
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "7G-QD2RO28c";
    else
        return results[1];
}
//IFLY.photovid.displayTestimonialsOverlay =  function(id) {
//	videoID = id;
//	//console.log('clicked on setVideo for id:' + id);
//   var shareString = '<div class="video-share"><div fb-xfbml-state="rendered" class="fb-like fb_edge_widget_with_comment fb_iframe_widget" data-href="http://www.iflyworld.com/video-reviews/index.php?videoID='+id+'" data-colorscheme="light" data-send="true" data-layout="button_count" data-width="90" data-show-faces="false"><iframe src="http://www.facebook.com/plugins/like.php?api_key=409878702400471&amp;locale=en_US&amp;sdk=joey&amp;channel_url=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D17%23cb%3Df39bc13f6085e52%26origin%3Dhttp%253A%252F%252Fwww.iflyworld.com%252Ff1a07571aee4992%26domain%3Dwww.iflyworld.com%26relation%3Dparent.parent&amp;href=http%3A%2F%2Fwww.iflyworld.com%2Fvideo-reviews%2Findex.php%3FvideoID%3DOPYiZJ5bIc4&amp;node_type=link&amp;width=150&amp;layout=button_count&amp;colorscheme=light&amp;show_faces=false&amp;send=true&amp;extended_social_context=false" class="fb_ltr" title="Like this content on Facebook." name="f3cfbf57a6a4502" id="f18eeda2c1537d8" scrolling="no"></iframe></div><a id="vt_twitter" title="share on Twitter" href="#">Twitter</a></div>';
//
//	var overlayHTML = '<div id="like_screen"></div><div id="vHolder" class="gradient-border drop-shadow rounded-corners"><object id="ytplayer_ie"  width="840" height="500">\
//						<param name="allowscriptaccess" value="always" />\
//						<param name="src" value="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer_ie&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" />\
//						<param name="allowfullscreen" value="true" />\
//						<embed id="ytplayer" width="840" height="500" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'+id+'?enablejsapi=1&version=3&playerapiid=ytplayer&hl=en_US&autoplay=1&autohide=1&rel=0&wmode=transparent&theme=light&color=red&hd=1&modestbranding=1" allowFullScreen="true" allowscriptaccess="always" allowfullscreen="true" /></object></div>'+shareString;
//		//console.log('displayVideoCartOverlay overlayHTML:', overlayHTML);
//	$.colorbox({
//		width:900,
//		height: 700,
//		html:overlayHTML,
//		onComplete:function() {
//			var getPlayer = document.getElementById('ytplayer');
//			ytplayer.addEventListener("onStateChange", "IFLY.photovid.showEndFrameCTA");
//			$('.fb-like').click(function() {
//				IFLY.analytics.sendSocial('Facebook', 'Like', 'Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
//			});
//			$('a#vt_twitter').click(function(e) {
//				e.preventDefault();
//				sendTwitter(id);
//				IFLY.analytics.sendSocial('Twitter','Share','Video Testimonial ID#: '+id, 'http://www.iflyworld.com/video-reviews/index.php?videoID='+id);
//			});
//		},
//		onClose: function() {
//			IFLY.youtube.destroy("ytplayer");
//		}
//	});
//}
//IFLY.photovid.showEndFrameCTA = function(newState) {
//	console.log('FIRED');
//			if(newState == 0) {
//				 $('div#like_screen').fadeIn(500);	
//			}
//}
	          
// load/que a video
function setVideo(id){
    console.log('clicked on setVideo for id:' + id);
    //$('#page_list').hide();
    $('#close_button_div').show();
    $('#page_list').hide();
    videoID = id;
    //Check for existing SWF
    if(isObject("player")){
        //replace object/element with a new div					 
        replaceSwfWithEmptyDiv("player");
    }

    $('#search_results_list_column').hide();				   
    $('#video_player_wrapper').show();				   
    showSocial();

    $('#like_screen').hide();

    var params = {
        allowScriptAccess: "always"
    };
    var atts = {
        id: "player"
    };
    swfobject.embedSWF("http://www.youtube.com/v/" + id + "?enablejsapi=1&playerapiid=ytplayer&version=3&hl=en_US&theme=light&color=red&hd=1&rel=0&showinfo=0&autoplay=1&autohide=1&loop=0&modestbranding=1",
        "player", "840", "500", "8", null, null, params, atts);
}

			

//function onYouTubePlayerReady(playerId) {
//    console.log('onYouTubePlayerReady  - with playerId:' + playerId);
////    $('#like_screen').hide();
// //   $('#close_button_div').show();
//    player = document.getElementById(playerId);	
//    player.addEventListener("onStateChange", "onytplayerStateChange");
//}
//			
//function onytplayerStateChange(newState) {			   			   	 
//    console.log('*** onytplayerStateChange-newstate:' + newState);
//    if (newState == 0){
//        $('div#like_screen').fadeIn(500);
////        $('div#like_screen').css('display','block');
//    }
//    if (newState == 1 || newState == 3){
//		//IFLY.analytics.sendEvent('Video', 'Plays', 'Video ID#: '+ videoID,null);
//        $('div#like_screen').css('display','none');
//    }
//    
//}
			
function playVideo(){
    //$('#vt_social').show();
    showSocial();
    player.playVideo();
}
			
//Support function: checks to see if target
//element is an object or embed element
function isObject(targetID){
    var isFound = false;
    var el = document.getElementById(targetID);
    if(el && (el.nodeName === "OBJECT" || el.nodeName === "EMBED")){
        isFound = true;
    }
    return isFound;
}
			 
//Support function: creates an empty
//element to replace embedded SWF object
function replaceSwfWithEmptyDiv(targetID){
    var el = document.getElementById(targetID);
    if(el){
        var div = document.createElement("div");
        el.parentNode.insertBefore(div, el);
			 
        //Remove the SWF
        swfobject.removeSWF(targetID);
			 
        //Give the new DIV the old element's ID
        div.setAttribute("id", targetID);
    }
}
		
function sendFacebok(){
    window.open('https://www.facebook.com/sharer/sharer.php?t=Check my video&u=' + window.location + '')
}


function sendTwitter(videoID){
    window.open('http://twitter.com/home?status=Check out my video from iFly: ' + window.location + '?videoID=' + videoID)
}
  
function vt_getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(document).ready(function() {
	if($('body.video-testimonials').length >0) {
		IFLY.videotestimonials.init(); 
	}
});
