<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step7">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step7" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<div class="continue">
					<a href="#" class="btn"><em></em><span>PRINT NOW</span></a>
				</div>

				<h1>CONFIRMATION</h1>
				
				<div class="confirmation-wrapper">
					<h2 class="confirmation">Thank you for your purchase!
							<br> You have a reservation for 2 at iFLY Utah on July 7, 2012 at 11:00AM. 
							<br>Your reservation number is: <span>R1345YD</span>
					</h2>
					<div class="actions">
						<a href="#" class="btn facebook"><em></em><span>Share on Facebook</span><div></div></a>
						<br>
						<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
					</div>
					
				</div><!-- /.confirmation-waiver -->
				
				<div class="confirmation-wrapper">

					<h2 class="confirmation">Complete your waiver online<br />to save time.</h2>

					<div class="actions">
						<a href="#" class="btn green"><em></em><span>WAIVER</span></a>
					</div>
					<br />
					<h2 class="confirmation">Email the waiver to other flyers.</h2>
					<form>
						<input type="text" class="text" value="ENTER EMAIL ADDRESS">
						<a href="#" class="btn"><em></em><span>ADD ANOTHER</span></a>
					</form>

					<div class="actions">
						<input type="submit" value="SEND" class="send">
					</div>

				</div><!-- /.confirmation-waiver -->

				<div class="confirmation-wrapper last">
					<h1>Pre-flight Checklist</h1>

					<h2>Wear the Right Clothes</h2>
					<p>Please wear well-fitting, lace-up sneakers, or running shoes. Other shoes are not suitable. Wear casual clothes, preferably pants and a shirt with no collar.</p>

					<h2>Arrive on Time</h2>
					<p>To be sure you are able to fly at the time you have reserved we need you to arrive 60 minutes before your flight time. Of course you need to allow time for traffic and on site parking issues.</p>

					<h2>Check-In</h2>
					<p>After you park, head to our ticket office to check-in and get everyone in your party registered. You can conveniently check-in at one of our self-entry kiosk.</p>

					<h2>Take The Class</h2>
					<p>In the classroom you will see a brief video and you will learn the hand signals needed to communicate in the tunnel. You will also get an opportunity to practice your skydiving position.</p>

					<h2>Put on Your Gear</h2>
					<p>You will receive kneepads, elbow pads, earplugs, a flight suit, a helmet and goggles for your flight.</p>

					<h2>Enjoy Your Flight</h2>
					<p>When it is your turn your instructor will assist you into the flight chamber, remember to have fun, relax and enjoy this amazing experience.</p>

				</div><!-- /.confirmation-waiver -->

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	
</body>
</html>