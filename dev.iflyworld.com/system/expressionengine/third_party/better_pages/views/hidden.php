<ul class="bp-hidden-fields <?php if ($show_fields == true){ ?>bp-visible<?php } else { ?>bp-hidden<?php } ?>">
<?php
	if ($groupFields->num_rows() > 0){
			foreach ($groupFields->result() as $r){ 
				$checked = "";
				
				if (array_key_exists($template_id, $selectedFields)){
					if (in_array($r->field_id, $selectedFields[$template_id])){
						$checked = " checked='checked'";
					}
				}
			?>
			<li><input type="checkbox" name="bpHidden_<?= $group_id ?>_<?= $template_id ?>_<?= $r->field_id ?>" value="<?= $r->field_id ?>_<?= $template_id ?>"<?= $checked ?> /> <?= $r->field_label ?></li>
<?php } } ?>
</ul>