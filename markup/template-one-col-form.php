<?php include("inc/head.php"); ?>
<body class="info-page one-col form">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<aside>
						<img src="/images/tunnel-retreat.jpg" alt="alt text"  class="rounded-corners gradient-border drop-shadow">
					</aside>

					<article>
						<h1 class="uppercase">Sisters in Skydiving - Tunnel Retreat</h1>
						<h2>Mark Your Calendar - October 12-14th, 2012 !</h2>
						<p class="uppercase">REGISTRATION IS REQUIRED:</p>
						<p>Sign up today via our online form below</p>
						<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
						<p>We will be creating a media frenzy around women in the sport of skydiving and celebrating women in flight!</p>
						<p>$35 registration (See breakdown below.)</p>
						<p>$500/30 minutes (before taxes) including coaching/organizing (Melanie Curtis, Melissa Nelson, Kimberly Winslow, Brianne Thompson, Amy Chemelecki and Cat Adam</p>
						<p class="second">Lots of load organizing and Huck Jams to promote the big sister little sister bonding!</p>
						<p>After flight activities for those who want to continue the bonding experience, will include: a sleep in, wine tasting,yoga and more...</p>
						<dl>
							<dt>Registration Cost Breakdown</dt>
							<dd>$10- T-shirt</dd>
							<dd>$5- Yoga Instructor</dd>
							<dd>$5- Massage</dd>
							<dd>$5- Meal</dd>
							<dd>$9- Essential Items</dd>
							<dd></dd>
						</dl>

						<form class="validate placeholder">
							<fieldset>
								<div class="row">
									<label for="name">Enter your name:</label>
									<div class="glow"><input type="text" name="name" value="Enter Text" class="text required"></div>
									<label for="name" generated="true" class="error"></label>
								</div>
								<div class="row">
									<label for="email">Enter your email address:</label>
									<div class="glow"><input type="text" name="email" value="Enter Text"  class="text required email"></div>
									<label for="email" generated="true" class="error"></label>
								</div>
								<div class="row">
									<label for="dropzone">Enter your home drop zone:</label>
									<div class="glow"><input type="text" name="dropzone" value="Enter Text"  class="text required"></div>
									<label for="dropzone" generated="true" class="error"></label>
								</div>
								<div class="row">
									<label for="timesjumped">Enter the number of times you have jumped:</label>
									<div class="glow"><input type="text" name="timesjumped" value="Enter Text"  class="text required numeric"></div>
									<label for="timesjumped" generated="true" class="error"></label>
								</div>
								<input type="submit" value="SUBMIT" class="btn red">
							</fieldset>
						</form>

					</article>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>