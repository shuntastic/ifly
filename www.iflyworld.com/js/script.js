/* 
	Authors: 
        Alex Hobday (alex@e-digitalgroup.com)
        Shun Smith (shun@coloringbookstudio.com)
*/
IFLY = {
	
	mobileInit: function() {
		getParams();
	//	var showMobile = (urlParams.showMobile && urlParams.showMobile == 'true') ? true : (cookieHandler.read('showMobile') && cookieHandler.read('showMobile') != null && cookieHandler.read('showMobile') == 'true') ? true : false;
	//	if( /Android|webOS|iPhone|iPod|BlackBerry|iemobile/i.test(navigator.userAgent) && !showMobile) {
			console.log('mobileInit: ',navigator.userAgent);
		if( /Android|webOS|iPhone|iPod|BlackBerry|iemobile/i.test(navigator.userAgent)) {
	//		use line below to include iPads
	//     if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|iemobile/i.test(navigator.userAgent) || urlParams.showMobile == 'true') {
			console.log('site size: ',$(window).width() +'x' + $(window).height());
			$(window).bind('orientationchange', function(event) {
				console.log('change: ',$(window).width() +'x' + $(window).height(),event);
			});


//			$.colorbox({
//				width:400,
////				height:320,
//				html:'<div class="mobile-check"><div class="row"><h2>Please select which version of the site you would like to view:</h2></div><div class="row cont-shopping"><a href="javascript:void(0);" class="btn green adddesk"><em></em><span>DESKTOP</span></a><br />OR<br /><a href="javascript:void(0);" class="btn green addmobile"><em></em><span>MOBILE</span></a></div></div>',
//				onComplete:function() {
//					$('#colorbox a.btn.addmobile').unbind('click').click(function() {
//						$('body').addClass('mobile');
//						cookieHandler.create('showMobile','true',14);
//						IFLY.init();
//						 $.fn.colorbox.close();
//					});
//					$('#colorbox a.btn.adddesk').unbind('click').click(function() {
//						$('body').addClass('desktop');
//						cookieHandler.create('showMobile','false',14);
//						IFLY.init();
//						 $.fn.colorbox.close();
//					});
//				}
//			});  
//		 
//		}	else if (showMobile) {
	
			$('nav.add-nav li a').each(function(index, element) {
				if($(this).attr('href') == '/legal/waiver') $(this).attr('href','http://access.iflyworld.com/waiver/web/aus/res/guest/welcome');
            });
			
	
			$('body').addClass('mobile');
			IFLY.init();
		} else {
			$('body').addClass('desktop');
			IFLY.init();
		}
		//MOBILE TESTING ONLY
		//ADD PARAM: ?showMobile=true
//		if(urlParams.showMobile == 'true') {
//		 	$('body').addClass('mobile');
//			$('body').removeClass('desktop');
//		}
		
	},
    init : function () {
	
    	var self = this;

    	self.hasFlash();

 		IFLY.current_tunnel = IFLY.locationHandler.getCurrentTunnel();
		
        if($('form.uniform, .ff_composer').length > 0){
            self.initUniform();
        }      

        if($('#calendar-widget').length > 0) {
             IFLY.calendarWidget.init();            
        }

        if($('#slider').length > 0) {
            IFLY.initSlider();            
        }
         
        if($('#event-widget').length > 0) {
            IFLY.calendarWidget.initEvents();            
        }
         
        if($('#yt-player').length > 0){
            IFLY.youtube.getID("yt-player");            
        } 

        if($('form.validate').length > 0){
            $('form.validate').validate();
        } 

        if($('div.validate form').length > 0){
            $('div.validate form').validate();
        } 

        if($('#uniform-how-many-fliers').length > 0){
            self.initNumFlyers();
        }   
		
		if($('#home-review').length>0){
			IFLY.getHomepageReview();
		}

        if($('a.tooltip').length > 0){
            self.initToolTip();
        }   

        if($('a.legal').length > 0){
            self.initLegal();
        } 

        if($('.iframe').length > 0){
			if(IFLY.mobileOn()) {
//				var pWidth = (document.body.offsetWidth).toString()+'px';
//				var pHeight = (document.body.offsetWidth*.75).toString()+'px';

				$('.iframe').each(function(index,value) {
					if($(this).hasClass('map-locator')) {
						$(this).attr('target','_blank');
					} else {
						var pWidth = ($(window).width()-20).toString()+'px';
						$(".iframe").colorbox({iframe:true, width:pWidth});
					}
				});
	//			$(".iframe").colorbox({iframe:true, width:pWidth, height:pHeight});
			} else {
				$(".iframe").colorbox({iframe:true, width:"600px", height:"450px"});
			}

        }

        if($('a.inline').length > 0){
			if(IFLY.mobileOn()) {
				var pWidth = ($(window).width()-20).toString()+'px';
				$("a.inline").colorbox({iframe:true, width:pWidth});
			} else {
				$("a.inline").colorbox({inline:true, width:"500px"});
			}
        }

        if($('li.inline').length > 0){
			if(IFLY.mobileOn()) {
				var pWidth = ($(window).width()-20).toString()+'px';
				$("li.inline a").colorbox({inline:true, width:pWidth, onComplete:function(){$.uniform.update();}});
			} else {
				$("li.inline a").colorbox({inline:true, width:"500px", onComplete:function(){$.uniform.update();}});
			}
        } 

        if($('.youtube').length > 0){
            $(".youtube").colorbox({iframe:true, innerWidth:425, innerHeight:344});    
        }

        if ( $(".modal").length > 0 ) {
            self.initModalFormValidation();
        }   


		if(($('.box-holder #flyer-location, body.tunnel-locator, body.booking.step2, body.gifting.step2, body.video-testimonials, body.ratings, body.photo-video').length > 0)) {
			IFLY.locationHandler.setTunnelDropdown();
		}
		
		IFLY.profile.checkLoginStatus();
	
		if($('article.box.video-testimonial').length > 0) {
			IFLY.getHomepageTestimonial();	
		}

		if($('form.quickbook').length>0) {
        	IFLY.calendarWidget.initQuickBook();
		}

        if($('#parallax').length > 0){
            IFLY.parallax.init();
        }
		
		
		try{
			function yt_callback(category,action,label,value){
				IFLY.analytics.sendEvent(category,action,label,value);
				//_gaq.push(['_trackEvent',category,action,label,value]);
			}
			var ytTracker = new YoutubeTracker(false, yt_callback, true);
			//console.log('TRACKER');
		}catch(e){}

    },
	//SET DROP DOWNS AND 1ST TUNNEL BY IP
	initTimePicker : function () {
		$("#time-picker").delegate("label", "click", function () {
		var c = $(this).attr('class');
			if ( c == undefined) {
				var e = $("#time-picker").find('label.your-time');
				var s = e.siblings('input');
				e.removeAttr('class');
				s.removeAttr('checked');
				$(this).addClass('your-time');
				$(this).siblings('input').attr('checked',true);
				if($('#time-picker input.book-now').length >0) {
					$('#time-picker input.book-now').removeClass('disabled');
				}
				
			} 
		});
	},
    initHome : function () {

    	var self = this;

    	self.initCarousel();
    	self.initHomeSlider();
	
        $(".gmask").delegate("a", "click", function (e) {
            e.preventDefault();
            $(this).colorbox();           
        });
		
 		IFLY.current_tunnel = IFLY.locationHandler.getCurrentTunnel();
		if(($('.box-holder #flyer-location').length > 0)) {
			IFLY.locationHandler.setTunnelDropdown();
		}

		if($('form.quickbook').length>0) {
        	IFLY.calendarWidget.initQuickBook();
		}
    	
    },

    initCarousel : function () {
		if($('.gmask ul li').length > 0) {
			$(".gmask").jCarouselLite({
				btnNext : ".gholder .btn-next",
				btnPrev : ".gholder .btn-prev",
				visible : 5,
				scroll  : 1
			});	
		}
    },

    initHomeSlider : function () {
        var ytID;
        var current_slide;
        var video_modal;
        var self = this;

    	$('.menu').delegate("a", "click", function(e) {
    		var slide = $(this).attr('href').substring(1);
			
			
			IFLY.analytics.sendEvent('Slider', 'Clicks', $(this).html(),null);
			//console.log('menu click', $(this).html());
    		
			
			$('#slider').anythingSlider(slide);
    		e.preventDefault();
    		var parent = $(this).parent();
    		parent.siblings().removeClass("active");
    		parent.addClass('active');
            
            if($('#parallax').length > 0){
                IFLY.parallax.lock();
            }

            if ( $("#video-modal").is(":visible") ) {
                self.current_slide.toggle();
                self.video_modal.toggle();
                IFLY.youtube.destroy("yt-player");
            }
            
  		});

        $("#slider").delegate("div.video a", "click", function(e) {
            e.preventDefault();
			
            $('#slider').data('AnythingSlider').startStop(false);
			$('#slider').unbind('slide_complete');
			
            self.current_slide = $(this).parents(".video-block");

            self.current_slide.toggle();

            self.video_modal = $("div#video-modal");
            self.video_modal.toggle();
			
            if($('#parallax').length > 0){
                IFLY.parallax.pause();
            }
			
            self.ytID   = $(this).attr("href").replace('#','');
            IFLY.youtube.init("yt-player", self.ytID);

        });

        $("#video-modal").delegate("a.btn, span.arrow a", "click", function(e) {
            e.preventDefault();
			//IFLY.parallax.trackMouse(true);            
            self.current_slide.toggle();
            self.video_modal.toggle();
            IFLY.youtube.destroy("yt-player");
			
            if($('#parallax').length > 0){
                IFLY.parallax.hideOverlay();
            }
        });

        $("#video-modal").delegate("span.arrow.back a", "click", function(e) {
            var a = $(".menu li.active")
            var i = $('.menu li').index(a);
            var l = $('.menu li').length;
            
            a.removeClass('active');
            
            if(a == 0) {
                $(".menu li:last-child").addClass('active');
            }
            else {
                $(".menu li").eq(--i).addClass('active');   
            }
        });

        $("#video-modal").delegate("span.arrow.forward a", "click", function(e) {

            var a = $(".menu li.active")
            var i = $('.menu li').index(a);
            var l = $('.menu li').length;

            a.removeClass('active');
        
            if( a == (--l) ) {
                $(".menu li:first-child").addClass('active');
            }
            else {
                $(".menu li").eq(++i).addClass('active');   
            }
        });

    },

    initSlider : function () {

        $('#slider').anythingSlider({
            theme           : "default",
            mode            : "horiz", 
            buildArrows     : true,
            buildNavigation : false,
            enableArrows    : true,
            appendForwardTo : $(".video-wrapper"),
            appendBackTo    : $(".video-wrapper"),
            buildStartStop  : false,
            hashTags        : false,

            //amimation
            autoPlay            : true,
			autoPlayDelayed		:true,
            pauseOnHover        : true,
            stopAtEnd           : false,
            delay               : 6500,
            resumeDelay         : 15000,
            animationTime       : 600, 
            delayBeforeAnimate  : 0,
			onSlideComplete: updateNav,

            addWmodeToObject: 'transparent'
        });
		function updateNav(e){
	        var whichPage = $('#slider').data('AnythingSlider').currentPage;
			$('.menu li').each(function() {$(this).removeClass('active');});
			$('.menu li:nth-child('+(whichPage)+')').addClass('active');
			//console.log('slideChanged', whichPage);
	}

    },

    initVideoPlayer : function () {
        $("#slider").delegate("div.video a", "click", function(e) {
            e.preventDefault();
            
            self.current_slide = $(this).parents(".video-block");

            self.current_slide.toggle();

            self.video_modal = $("div#video-modal");
            self.video_modal.toggle();
            

            if($('#parallax').length > 0){
                IFLY.parallax.pause();
            }
            
            self.ytID   = $(this).attr("href").replace('#','');
            IFLY.youtube.init("yt-player", self.ytID);

        });
    },
	mobileOn : function() {
		var flag = ($('body').hasClass('mobile')) ? true: false;
		return 	flag;
	},
    hasFlash : function (){

		if(swfobject.hasFlashPlayerVersion("1")) {
			$('html').addClass('hasflash');
		}else{
			$('html').addClass('noflash');
		}

	},

	initUniform : function () {
	//	$("select, input:radio, input:file, input:checkbox").not('#cboxOverlay > select, select#freeform_areas_of_interest, select#freeform_tunnel_location').uniform();
		if(IFLY.mobileOn()) {
			$("select, input:radio, input:file, input:checkbox").not('.modal select, .modal input, #cboxOverlay > select, select#freeform_areas_of_interest, select#freeform_tunnel_location, .ff_composer select, #register-ifly-acct select[id$="flyer-location"], form.reset-password select[id$="flyer-location"], #lgn select[id$="flyer-location"], .logloc select[id$="flyer-location"]').uniform();
		} else {
			$("select, input:radio, input:file, input:checkbox").not('#cboxOverlay > select, select#freeform_areas_of_interest, select#freeform_tunnel_location, .ff_composer select, #register-ifly-acct select[id$="flyer-location"], form.reset-password select[id$="flyer-location"], #lgn select[id$="flyer-location"], .logloc select[id$="flyer-location"]').uniform();
		}
	
	//	$("select, input:radio, input:file, input:checkbox").not('#cboxOverlay > select, select#freeform_areas_of_interest, select#freeform_tunnel_location, .ff_composer select, #register-ifly-acct select[id$="flyer-location"], form.reset-password select[id$="flyer-location"], #lgn select[id$="flyer-location"], .logloc select[id$="flyer-location"]').uniform();
		$('input[type="text"]').inputToggle();
	},

    initNumFlyers : function () {
        $("#uniform-how-many-fliers").on("change", function () {
            if( $('#uniform-how-many-fliers option:selected').val() == "13" ) {
                $.colorbox({ 
                            html : $("#large-parties").html(),
                            width:"500px" 
                            });
            } 
        });
    },
	
	loggedIn: function() {
		var x = cookieHandler.read('guest_no');
		var y = cookieHandler.read('user_tunnel');
		
		var status = (y && y!=null && x && x != null || (urlParams.guest_no != null) ) ? true : false
		
		return status;
	//	return false;
	},
    initLogin : function () {
		
		if($('input.checkout').length > 0 && !IFLY.loggedIn()) {
			$('input.checkout').click(function() {
                $.colorbox({
				//	width:(IFLY.mobileOn()) ? 500 : 800,
					html : $(".modal.login").html(),
					onComplete: function() {
						//$('#lgn
						
					}
				});
				e.preventDefault();
			});
			
		}
		
        $("#uniform-how-many-fliers").on("change", function () {
            if( $('#uniform-how-many-fliers option:selected').val() == "13" ) {
                $.colorbox({ html : $(".modal.lp").html() });
            } 
        });
    },

    initLegal : function () {

        $("a.legal").on("click", function () {
            $.colorbox({ html : $(".modal.legal").html() });
        });
    },
	toolTimer: {},
    initToolTip : function () {
		$('.tooltip').betterTooltip({speed:200,delay:200});

    },
	showLoader: function() {
		return '<img class="loader" width="45px" height="45px" src="https://iflyworld.com/images/iFLY_loader.gif" />';
	},
	showRevLoader: function() {
		return '<img class="loader" width="45px" height="45px" src="https://iflyworld.com/images/iFLY_loader_white.gif" />';
	},
	killLoader: function() {
		$('img.loader').remove();
		//return '<img class="loader" width="45px" height="45px" src="https://iflyworld.com/images/iFLY_loader.gif" />';
	},
	callApi: function(callStr,callbackfunc,onError,onComplete) {
		try {
			$.ajax({
				url: '/api_curl.php',
				type: 'post',
				dataType: 'json',
				data: callStr,
				success: callbackfunc,
				error: (onError != null) ? onError : handler.onError,
				complete: (onComplete != null) ? onComplete : handler.onComplete
			});

		  } catch(err) {
			  console.log('CALLBACK ERROR', err);
		  //Handle errors here
		  }	
    },

    initModalFormValidation : function () {
    
        $(".modal form").each(function(index){
    
            var req = $(this).find("label span");
        /*
            $.each(req, function(){

                if ( $(this).hasClass("required_item") ) {

                    var name = $(this).parent("label").attr("for");
                    $("#" + name).addClass('required');    
                
                }
                
            })

            form.validate();
            */

            if ( req.length > 0 ) {

                $(this).attr("id", "form"+index);
                var form    = $("#form"+index);
                var errors  = form.siblings(".errors");
                var success = form.siblings(".success");
                var fail    = form.siblings(".fail");

                form.on('submit', function (e) {

                    e.preventDefault();
                    
                    $.ajax({
                            type: "POST",
                            url: form.attr( 'action' ),
                            data: form.serialize(),
                            timeout : 5000,

                            success: function( response ) {
	
																console.log(response)

                                if (response.success == true) 
                                {
                                    $(form).fadeToggle(1000, function () {
                                        $(success).fadeToggle(1000);
                                    });
                                    
                                }

                                else
                                {

                                    $(errors).find('p, h4').remove();
                                    
                                    var string = '<h4>We found some errors</h4>';

                                    $.each(response.errors, function(key, value) { 
                                        //var k = key.replace( /_/g, " " );
                                        //string += '<p><span class="field_name capitalize">' + k + '</span>: ' + value + '</p>'; 
																				string += '<p><span class="field_name capitalize">' + key + '</span>: ' + value + '</p>'; 
                                    });

                                    $(string).prependTo(errors);

                                    $(form).fadeToggle();
                                    $(errors).fadeToggle();

                                    $(errors).on("click", ".correct-errors a", function(e) {
                                      e.preventDefault();
                                        $(errors).fadeToggle(1000, function () {
                                            $(form).fadeToggle();    
                                        });
                                    });
                                    
                                }//end else
                                
                            },//end success
                            error : function ( response) {
                                $(form).fadeToggle();
                                $(fail).fadeToggle();
                            }//end error
                                   
                    }); //end AJAX

                })//end for submit handler
            
            }//end if

        });//end modal form
    
    },//end function 

    updateWaiverLocation : function () {

        var url1 = 'http://access.iflyworld.com/waiver/web/'; 
        var url2 = '/res/guest/welcome';

        if ( $("#waiver-location").length > 0 ){

            $("#waiver-location").on("change", function (){
                var code = $(this).val();
                loadIframe(url1, url2, code);
            });

        }
        else if ( $("#waiver-iframe").length > 0 ){


            var full = window.location.host;
            var parts = full.split('.');
            var sub = parts[0];

            for (var i = 0; i < IFLY.tunnels.length; i++) {
                $.each(IFLY.tunnels[i], function(key, value) { 
                    //console.log(key + ': ' + value); 
                    console.log(url1, url2, sub, key, value)
                    if ( key == "subdomain" && value == sub){

                        loadIframe(url1, url2, IFLY.tunnels[i]['code']);
                    }
                })
            }
            
        }

        function loadIframe(url1, url2, code) {
				if($('.content-wrapper iframe').length > 0) {
					$('.content-wrapper iframe').remove();
				}
                $('<iframe />', {
                                    "name"  : "waiver",
                                    "id"    : "waiver",
                                    "src"   : url1 + code + url2,
                                    "width" : "925",
                                    "height": "910"
                                }).appendTo('.content-wrapper');
            }
    }

};

IFLY.parallax = {
    init: function() {
		if($('html').hasClass('hasflash')){
			$('#skyline').css({background:'#020a3d',display:'none'});

				if(jQuery.browser.msie){
					var flashVideoContent = '<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" width="1370" height="550" id="parallax" align="middle"><param name="movie" value="swf/parallax.swf" /><param name="quality" value="high" /><param name="bgcolor" value="#000000" /><param name="play" value="true" /><param name="loop" value="true" /><param name="wmode" value="transparent" /><param name="scale" value="showall" /><param name="menu" value="true" /><param name="devicefont" value="false" /><param name="salign" value="" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><a href="http://www.adobe.com/go/getflash"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></object>';
					$("#parallax-wrapper").html(flashVideoContent);
	//				#parallax-wrapper$("#parallax").html(flashVideoContent);
				} else {

					var flashvars = {};
					var params = {wmode: "transparent",quality:"high",allowscriptaccess:"always",allownetworking:"all"};
					var attributes = {swliveconnect:"true"};
					swfobject.embedSWF("swf/parallax.swf", "parallax", "1370", "550", "10.0.0","expressInstall.swf", flashvars, params, attributes);
				}
		}
    },
	hideOverlay: function() {
         $("#skyline").css({display:'none'}).css({opacity : 0});    
	},
    trackMouse: function(pass) {
        if (pass){
            $("#skyline").css({display:'none'}).css({opacity : 0});    
        }
        if (!jQuery.browser.msie) {
			//console.log('MOUSE SHOULD');
			$('#wrapper').mousemove(function(evt) {
				var offset = $(this).offset();
				var xPos = evt.pageX - offset.left;
				var paraObj = swfobject.getObjectById("parallax");
				paraObj.onHMove(xPos);
			});
		} else {
			console.log('MOUSE SHOULDNOT');
			var paraObj = swfobject.getObjectById("parallax");
			paraObj.onHMove(800);
		}
    },
	lock: function() {
		$('#wrapper').unbind('mousemove');
		var paraObj = swfobject.getObjectById("parallax");
		paraObj.onHMove(1145);
		//paraObj.fadeBack();
	},
	pause: function() {
		$("#skyline").css({display:'block'}).animate({opacity :.8},200);    
		$('#wrapper').unbind('mousemove');
	}
};

IFLY.youtube = {

    init : function (id, ytid) {
		var pWidth = (IFLY.mobileOn()) ? (document.body.offsetWidth).toString() : '600';
		var pHeight = (IFLY.mobileOn()) ? (document.body.offsetWidth*.583).toString() : '350';
		
		var tubeHTML = '<object id="ytplayer_ie"  width="'+pWidth+'" height="'+pHeight+'">\
						<param name="allowscriptaccess" value="always" />\
						<param name="src" value="http://www.youtube.com/v/'+ytid+'?enablejsapi=1&version=3&playerapiid=ytplayer_ie&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" />\
						<param name="allowfullscreen" value="true" />\
						<embed id="ytplayer" width="'+pWidth+'" height="'+pHeight+'" type="application/x-shockwave-flash" src="http://www.youtube.com/v/'+ytid+'?enablejsapi=1&version=3&playerapiid=ytplayer&hl=en_US&autoplay=0&autohide=1&rel=0&wmode=opaque" allowFullScreen="true" allowscriptaccess="always" allowfullscreen="true" /></object>'

		$("#"+id).html(tubeHTML);
		
//        $("#"+id).tubeplayer({
//            width   : 600, // the width of the player
//            height  : 350, // the height of the player
//            iframed : true,
//			showRelated: 0,
//            allowFullScreen : "true", // true by default, allow user to go full screen
//            initialVideo    : ytid, // the video that is loaded into the player
//            preferredQuality: "default",// preferred quality: default, small, medium, large, hd720
//            onPlay: function(id){}, // after the play method is called
//            onPause: function(){}, // after the pause method is called
//            onStop: function(){}, // after the player is stopped
//            onSeek: function(time){}, // after the video has been seeked to a defined point
//            onMute: function(){}, // after the player is muted
//            onUnMute: function(){} // after the player is unmuted
//        });       
		try{
			//console.log('youtube tracker');
			function yt_callback(category,action,label,value){
				_gaq.push(['_trackEvent',category,action,label,value]);
			}
			var ytTracker = new YoutubeTracker(false, yt_callback, true);
		}catch(e){}
    },

    stop : function (id) {
        $("#"+id).tubeplayer("stop");
    },

    destroy : function (id) {
        $("#"+id).tubeplayer("destroy");
    },

    getID : function (elem) {
        var data = $("#"+elem).data("ytid");
        if ( data != undefined ) {
            IFLY.youtube.init(elem, $("#"+elem).data("ytid"));
        }
    }

};
IFLY.calendarWidget = {
	calInitDate: 0,
    init: function() {
  		var today = new Date();
      $('#calendar-widget').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
			minDate: today,
            onSelect: this.callDaySelect,
            beforeShowDay:this.beforeShowDay
        });
//		$(document).bind("click", function(e){
//			var _target = $(e.target),
//			_selector = "calendar-widget";
//			
//			if( _target.attr('id') !== "_selector" && _target.parents("#" + _selector).length <= 0 && !_target.hasClass('ui-datepicker-prev') && !_target.hasClass('ui-datepicker-next')) {
//				$("#" + _selector).hide();	
//			}
//		});
		
    },               
    initEvents: function() {
        $('#event-widget').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
            onSelect: this.callEventsSelect,
            beforeShowDay:this.beforeEventsDay
        });
    },     
	setCalendarDay: function(tCal) {
	//	IFLY.calendarWidget.calInitDate = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : new Date(0);
		IFLY.calendarWidget.calInitDate = new Date(0);
		$(tCal).datepicker("option", "minDate", IFLY.calendarWidget.calInitDate);
		$(tCal).datepicker("option", "setDate", IFLY.calendarWidget.calInitDate);
		
	},
//INITIALIZES FLIGHT WIZARD ON HOMEPAGE	          
    initQuickBook: function() {
		var bookingURL = '';
		
		//SETS CLICK THRU SUBDOMAIN FOR FLIGHT WIZARD
		function setButtons () {
			if (IFLY.tunnels) {
				var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
			//	console.log('setButtons selTunnelInfo ',selTunnelInfo);
				//if(selTunnelInfo.length > 0) {
					bookingURL = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';
					var infoURL = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com/flight-info/flyer-guide';
					$('form.quickbook a.btn.flyer-guide').attr('href',infoURL);
				//}
			}
		}
		
		setButtons();
		
//		$('select[id$="flyer-location"] option:selected').removeAttr('selected');
//		$('select[id$="flyer-location"] option[value="'+IFLY.current_tunnel+'"]').attr("selected","selected");
		$('select[id$="flyer-location"]').change(function() {
			if($(this).val() != 'FLIGHT LOCATION') {
				IFLY.current_tunnel = urlParams.tunnel = $(this).val();
				
				setButtons();
				if($('#calendar-quickbook').length >0)  {
					IFLY.calendarWidget.setCalendarDay('#calendar-quickbook');
//					IFLY.calendarWidget.calInitDate = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : 0;
//					$("#calendar-quickbook").datepicker("option", "minDate", IFLY.calendarWidget.calInitDate);
//					$("#calendar-quickbook").datepicker("option", "setDate", IFLY.calendarWidget.calInitDate);
					$('#book-date span').html('FLIGHT DATE');
					urlParams.flyer_date = null; 
				}
			}
			checkQuickBookStatus();
		});
		
		$('select[id$="how-many-fliers"]').change(function() {
			checkQuickBookStatus();
		});
	//	IFLY.calendarWidget.getClosedDates();
	//	var today = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : 0;
		var today = 0;
		if($('.box-holder #calendar-quickbook').length > 0) {
			$('#calendar-quickbook').datepicker({
				dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
				buttonImageOnly:true,
		//		beforeShowDay: IFLY.calendarWidget.unavailable,
				minDate: today,
				onSelect: function(e) {
					//console.log('e: '+e);
					//$('#book-date span').html(e);
						$('a.date span').html(e)
						$(this).hide();
					urlParams.flyer_date = e;
					checkQuickBookStatus();
				}
			});
				$('#calendar-quickbook').hide();
//				$(document).bind("click", function(e){
//					var _target = $(e.target),
//					_selector = "calendar-quickbook";
//					
//					if( _target.attr('id') !== "_selector" && _target.parents("#" + _selector).length <= 0 && !_target.hasClass('ui-datepicker-prev') && !_target.hasClass('ui-datepicker-next')) {
//						$("#" + _selector).hide();	
//					}
//				});

		}
		
		$('#book-date').click(function(e) {
				$('#calendar-quickbook').show();
                e.preventDefault(); 
		});
		$('form.quickbook').data('validator', null).validate({
		   rules: {
			 tunnel: "required",
			 flyer_num: "required",
			 code:"required"
		   },
		   errorPlacement: function(error, element) { }
		});
		
		$('select#flyer-location, select#flyer-type, select#flyer_num').change(checkQuickBookStatus);
		function checkQuickBookStatus(){
			if($('form.quickbook').valid() && urlParams.flyer_date != null && $('select[id$="flyer-type"]').val() != 'FLYER TYPE' && $('select[id$="flyer-location"]').val() != 'FLIGHT LOCATION' && $('select[id$="how-many-fliers"]').val() != '# OF FLYERS') {
				$('input.find-flights').removeClass('disabled');
			} else {
				$('input.find-flights').addClass('disabled');
			}
		}

		$('form.quickbook').submit(function(e) {
			if($(this).valid() && !$('input.find-flights').hasClass('disabled')) {
				var $form = $(this)
				$inputs = $form.find("input");
				serializedData = $form.serialize();
				//var dURL = '/book-now/booking-step3/?'+serializedData+'&flyer_date='+urlParams.flyer_date+'&tunnel_name='+$('select[id$="flyer-location"] option:selected').html()+'&flyer_type_name='+$('select[id$="flyer-type"] option:selected').html();
				var dURL = bookingURL + '/book-now/booking-step3?'+serializedData+'&flyer_date='+urlParams.flyer_date+'&tunnel_name='+$('select[id$="flyer-location"] option:selected').html()+'&flyer_type_name='+$('select[id$="flyer-type"] option:selected').html();
				window.location = dURL;
			}
			return false;
		});
    },               

    callDaySelect: function(dateText) {
        var date,
            selectedDate = new Date(dateText),
            i = 0,
            calevent = [];
    
        /* Determine if the user clicked an event:  && !calevent*/
        while (i < IFLY.calendarWidget.calData.length) {
            date = IFLY.calendarWidget.calData[i].date;
    
            if (selectedDate.valueOf() === date.valueOf()) {
                calevent.push(IFLY.calendarWidget.calData[i]);
				console.log('found', IFLY.calendarWidget.calData[i]);
            }
            i++;
        }
        if (calevent.length > 0) {
			var eventContent ='<a class="closeButton" href="#">close</a><h2>'+IFLY.calendarWidget.fullDate(calevent[0].date)+'</h2>';
			$.each(calevent, function(index, value) {
				eventContent += '<h3>'+ this.title+'</h3><p>'+IFLY.calendarWidget.truncate(this.description,50,'...')+'<br /><a href="'+this.url+'">MORE INFO</a></p>';
			});
            $('div.event-overlay').html(eventContent).focus();
                $('div.event-overlay').fadeIn(300, function(){               
                    $('div.event-overlay a.closeButton').click(function(e){
                        e.preventDefault();
                        $('div.event-overlay').fadeOut(100, function() {$(this).html('')});
                    }); 
            });
        }
    },
    callEventsSelect: function(dateText) {
        var date,
            selectedDate = new Date(dateText),
            i = 0,
            event = null;
    
        /* Determine if the user clicked an event: */
        while (i < calData.length && !event) {
            date = this.calData[i].date;
    
            if (selectedDate.valueOf() === date.valueOf()) {
                event = IFLY.calendarWidget.calData[i];
            }
            i++;
        }
        if (event) {
			
//            var eventContent = '<a class="closeButton" href="#">close</a><h2>'+IFLY.calendarWidget.fullDate(event.date)+'<br />'+event.title+'</h2>'+IFLY.calendarWidget.truncate(event.description,50,'...<p><a href="'+event.url+'">MORE INFO</a></p>');
//            $('div.event-overlay').html(eventContent).focus();
//                $('div.event-overlay').fadeIn(300, function(){               
//                    $('div.event-overlay a.closeButton').click(function(){
//                        $('div.event-overlay').fadeOut(100, function() {$(this).html('')});
//                    }); 
//            });
        }
    },
    beforeEventsDay: function(date) {
        var result = [true, '', null];
        var matching = $.grep(IFLY.calendarWidget.calData, function(event) {
            return event.date.valueOf() === date.valueOf();
        });
    
        if (matching.length) {
            result = [true, 'highlight', null];
        }

        return result;
    },  
    beforeShowDay: function(date) {
        var result = [true, '', null];
        var matching = $.grep(IFLY.calendarWidget.calData, function(event) {
            return event.date.valueOf() === date.valueOf();
        });
    
        if (matching.length) {
            result = [true, 'highlight', null];
        }

        return result;
    },  

    fullDate: function(dateObj) {
        calendar = dateObj;
        day = calendar.getDay();
        month = calendar.getMonth();
        date = calendar.getDate();
        year = calendar.getYear();
        if (year < 1000)
        year+=1900
        cent = parseInt(year/100);
        g = year % 19;
        k = parseInt((cent - 17)/25);
        i = (cent - parseInt(cent/4) - parseInt((cent - k)/3) + 19*g + 15) % 30;
        i = i - parseInt(i/28)*(1 - parseInt(i/28)*parseInt(29/(i+1))*parseInt((21-g)/11));
        j = (year + parseInt(year/4) + i + 2 - cent + parseInt(cent/4)) % 7;
        l = i - j;
        emonth = 3 + parseInt((l + 40)/44);
        edate = l + 28 - 31*parseInt((emonth/4));
        emonth--;
        var dayname = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        var monthname = new Array ("January","February","March","April","May","June","July","August","September","October","November","December" );
//      var fullD = dayname[day] + ", "+monthname[month] + " ";
        var fullD = monthname[month] + " ";
        if (date< 10)
            fullD+="0" + date + ", "
        else fullD+=date + ", ";
        fullD+=year
        
        return fullD;
    },

    truncate : function (text, limit, append) {
        if (typeof text !== 'string')
            return '';
        if (typeof append == 'undefined')
            append = '...';
        var parts = text.split(' ');
        if (parts.length > limit) {
            // loop backward through the string
            for (var i = parts.length - 1; i > -1; --i) {
                // if i is over limit, drop this word from the array
                if (i+1 > limit) {
                    parts.length = i;
                }
            }
            // add the truncate append text
            parts.push(append);
        }
        // join the array back into a string
        return parts.join(' ');
    },
	unavailableDates:[],

	unavailableDays: function(date) {
	  var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
	  //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
	  for (i = 0; i < IFLY.calendarWidget.unavailableDates.length; i++) {
		if($.inArray((m+1) + '-' + d + '-' + y,IFLY.calendarWidget.unavailableDates) != -1 || new Date() > date) {
//		  console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ');
		  return [false];
		}
	  }
//	  console.log('good:  ' + (m+1) + '-' + d + '-' + y);
	  return [true];
	},

	unavailable: function (date) {
//	  var noWeekend = jQuery.datepicker.noWeekends(date);
//	  return noWeekend[0] ? nationalDays(date) : noWeekend;
	  return IFLY.calendarWidget.unavailableDays(date) ? IFLY.calendarWidget.unavailableDays(date) : [];
	},
	getClosedDates: function() {
				//Get closed dates
			var call='method=get_closed_dates&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel;
			IFLY.callApi(call, success,null,null);
			function success(response, textStatus, jqXHR){
					var data = response.data;
//					console.log('getClosedDates',data);
					refDates =[];
					$.each(data, function(index,value) {
						var dParse = value.split('-');
						var newDate  = new Date(new Date(parseInt(dParse[0]),parseInt(dParse[1]),parseInt(dParse[2])))
						$refdate = $.datepicker.formatDate('yy-mm-dd', newDate);
						refDates.push($refdate);
					});
					IFLY.calendarWidget.unavailableDates = refDates;
			}
	}

};
IFLY.getHomepageReview = function() {
			$('#home-review').html(IFLY.showLoader());
			
			//var tunnel = IFLY.locationHandler.getCurrentTunnel();
			
			var today = new Date();
			var call='method=get_review_top&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel;
			IFLY.callApi(call,displayReview, notFound, null);
			function displayReview(response, textStatus, jqXHR){
				var data = response.data;
				var reviewDate = new Date(data.datesubmitted).mmddyyyy();
				//console.log('data.datesubmitted', data.datesubmitted, reviewDate)
				$('#home-review').html('<q>'+(data.reviewText).trunc(56,true)+'</q>\
										<br /><a href="#" class="more">READ MORE</a>\
										<cite>-'+data.name+', '+reviewDate+'</cite>');
				$('div.rating').html('<span class="star-rating star5">5 stars</span>');
				$('#home-review a.more').click(function(e) {
					$('#home-review q').html(data.reviewText);
					$(this).remove();
					e.preventDefault();
				})
			}
			
			function notFound() {
				//no review at default location available
				call='method=get_review_top&controller=testimonials&format=json&tunnel=ifo';
//				console.log('call',call);
				IFLY.callApi(call,displayReview, null, null);	
			}
				
	}
IFLY.getHomepageTestimonial = function() {
			$('article.video-testimonial div.thumb').html(IFLY.showLoader());
			//console.log('getHomepageTestimonial: ',IFLY.current_tunnel);
			$.post(IFLY.videotestimonials.basePath, {queryType: "all",page: 1,tunnel: IFLY.current_tunnel},  function(output) {
				// data contains a count and the array of results...
				var response = $.parseJSON(output);
				//$('#search_results').html('');
				IFLY.videotestimonials.currentCount = response.count;

				var value = response.media;

				if(typeof value[0].videoThumb != 'undefined'){
					videoID = value[0].videoID;
					vidID = videoID;
					title = value[0].videoTitle;  
					title = title.split('\'s Flight Video ');
					var date = title[1];
					var name = title[0].split(' ');
					name = name[0];
					var textOverlay = name+'<br />'+date;
					url = value[0].videoURL; 
					description = value[0].videoDescription;
					thumb = value[0].videoThumb;
					//
					//<span>'+textOverlay+'</span>
				//	$('article.box.video-testimonial .thumb').html('<div class="item gradient-border drop-shadow rounded-corners" width="184" height="158" style="background:transparent url('+thumb+') top center no-repeat;"><a data-vid="'+videoID+'" href="#"><span class="play"><em></em><strong></strong></span></a></div>');	
					$('article.box.video-testimonial .thumb').html('<div class="item"><a data-vid="'+videoID+'" href="#"><span class="play"><em></em><strong></strong></span><img src="'+thumb+'" class="gradient-border drop-shadow rounded-corners" width="184" height="122" /></a></div>');	

					$('.thumb .item a').each(function(index,value){
						$(this).unbind('click').click(function(e) {
							e.preventDefault();
							var vid = $(this).data('vid');
							IFLY.videotestimonials.displayTestimonialsOverlay(vid);
						});
					});

				}
				IFLY.killLoader();
			});
			
				
	}	
//GEO-LOCATION  
IFLY.locationHandler = {
	rad: function (x) {return x*Math.PI/180;},
	getCurrentTunnel: function() {
		if(IFLY.tunnels) {
			var locRef = jQuery.map(IFLY.tunnels, function(obj, index) {
				if(obj.current === 'yes')
				return obj; // or return obj.name, whatever.
			});
			var x;
			if(locRef.length > 0) {
				x = locRef[0].code;
			}
			var ck = cookieHandler.read('ifly_tunnel');//Load cookie value
			var y = (x && x != null) ? x : (ck != '' && ck != null) ? ck : null;
			//console.log('getCurrentTunnel',IFLY.current_tunnel);
			return y;
		}
		return null
			//urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
	},
	isLocalSite: function() {
		    var full = window.location.host;
            var parts = full.split('.');
            var ref = parts[0];
			//console.log('islocalsite',ref);
			var returnKey = -1;
			$.each(IFLY.tunnels, function(key, info) {
				if (info.subdomain == ref) {
				   returnKey = key;
				   return false; 
				};   
			});
			var x = (returnKey == -1) ? false : true;
			return x;    

	},
	getLocationInfoByCode: function(tunnelCode) {
		//jQuery.inArray( value, array [, fromIndex] )
			if(IFLY.tunnels) {
				//console.log('getLocationInfoByCode ',IFLY.tunnels);
				var locRef = jQuery.map(IFLY.tunnels, function(obj) {
					if(obj.code === tunnelCode)
						 return obj; // or return obj.name, whatever.
				});
				if(locRef.length >0) {
						return locRef[0]
				}
				return null
			}
		//	return null
			//urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
	},
	getLocationInfoByTitle: function(tunnelCode) {
		//jQuery.inArray( value, array [, fromIndex] )
		//		console.log('getLocationInfoByTitle',tunnelCode);
			if(IFLY.tunnels) {
				var locRef = jQuery.map(IFLY.tunnels, function(obj) {
					if(obj.title === tunnelCode)
						 return obj; // or return obj.name, whatever.
				});
				if(locRef.length >0) {
						return locRef[0]
				}
				return null
			}
			//urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
	},

	setTunnelDropdown: function() {
	
		//hadToUseADefaultLocation = false;
	
		if(IFLY.current_tunnel == null) {
			//hadToUseADefaultLocation = true;
			IFLY.current_tunnel = ($('select[id$="flyer-location"] option:first-child').val() != null && $('select[id$="flyer-location"] option:first-child').val() != undefined) ? $('select[id$="flyer-location"] option:first-child').val() : $('select[id$="flyer-location"] option:nth-child(1)').val();
		}
		
	//	if($('#flyer-location').length > 0 && !hadToUseADefaultLocation) {	
		if($('#flyer-location').length > 0 && (IFLY.locationHandler.isLocalSite() || $('body.photo-video').length > 0)) {	
			$('select[id$="flyer-location"] option:selected').removeAttr('selected');
			$('select[id$="flyer-location"] option[value="'+IFLY.current_tunnel+'"]').attr("selected","selected");
			$.uniform.update();
		}
//		if($('body.booking.step2, body.gifting.step2').length > 0) {
//			var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
//       		console.log('setTunnelDropdown selTunnelInfo',selTunnelInfo);
//			$('body.booking.step2 .tunnel-info h2, body.booking.step2 .tunnel-info h2').html(selTunnelInfo.title);
//			IFLY.locationHandler.updateAddress(IFLY.current_tunnel);
//			$.uniform.update();
//		}
	},

	sortTunnels: function(tLatLng,locations) {
		var lat = new Number(tLatLng.latitude);
		var lng = new Number(tLatLng.longitude);
		var R = 6371; // radius of earth in km
		
	
		var distances = [];
		var closest = -1;
		for( i=0;i<locations.length; i++ ) {
			var mlat = new Number(locations[i].lat);
			var mlng = new Number(locations[i].lon);
			
// NEW FUNCTION
//			var d = Math.acos(Math.sin(mlat)*Math.sin(lat) +  Math.cos(mlat)*Math.cos(lat) * Math.cos(lng-mlng)) * R;
			
	// CURRENT FUNCTION
			var dLat  = IFLY.locationHandler.rad(mlat - lat);
			var dLong = IFLY.locationHandler.rad(mlng - lng);
			var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
			Math.cos(IFLY.locationHandler.rad(lat)) * Math.cos(IFLY.locationHandler.rad(lat))*Math.sin(dLong/2) * Math.sin(dLong/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;
	
	//V3 function		
//			var d = Math.acos(Math.sin(lat) * Math.sin(mlat) + Math.cos(lat) * Math.cos(mlat) * Math.cos(mlng - lng)) * R

			
			distances[i] = d;
			locations[i].distance = d;
			if ( closest == -1 || d < distances[closest] ) {
				closest = i;
			}
		}
		function compare(a,b) {
		  if (a.distance < b.distance)
			 return -1;
		  if (a.distance > b.distance)
			return 1;
		  return 0;
		}
		locations.sort(compare);
		return locations;
	},
	onError: function(e, ip) {
		
		IFLY.current_tunnel = 'aus';
		return IFLY.current_tunnel;
//		switch(e.status) {
//		  case 400:
//		  case 404:
//			$("#info").html(no_results(ip));
//			break;
//		  case 403:
//			$("#info").html(e);
//			break;
//		  default:
//			$("#info").html(e);
//		}
	},
	updateAddress: function(tun){
		if(IFLY.tunnels) {
			//console.log('locRef',locRef);
			var locRef = jQuery.map(IFLY.tunnels, function(obj) {
				if(obj.code === tun)
					 return obj; // or return obj.name, whatever.
			});
			
			if(locRef.length > 0) {
				$('body.booking.step2 .tunnel-info h2').html(locRef[0].title);
				$('body.booking.step2 .tunnel-info img').attr('src',locRef[0].thumb);
				$('body.booking.step2 .tunnel-info img').attr('alt',locRef[0].title);
				//$('body.booking.step2 .tunnel-info img').attr('src',IFLY.tunnels[locRef].image);
				var addressString = '<li>'+locRef[0].add1+', '+locRef[0].add2+'</li><li>'+locRef[0].city+', '+locRef[0].state+' '+locRef[0].zip+'</li><li>'+locRef[0].phone+'</li>'
				$('body.booking.step2 .tunnel-info ul').html(addressString);
				var mapURL = locRef[0].map.replace(/&amp;/g, "&");
	
				$('body.booking.step2 .tunnel-info a.map-locator').attr('href',mapURL);
	
			}
		} else {
			//IFLY.locationHandler.updateAddress(tun);	
		}
	}
};
IFLY.profile = {
	checkLoginStatus: function() {
		//console.log('checkLoginStatus');
		//		IFLY.profile.checkLoginStatus();

		if(IFLY.loggedIn()) {
			var x = cookieHandler.read('guest_no');
			var y = cookieHandler.read('user_tunnel');
			console.log('checkLoginStatus logged in', x);
					
			function success(response, textStatus, jqXHR) {
				console.log('checkLoginStatus response',response);
				var guestInfo = response.data[0];
				var user = (guestInfo.web_user).split('@');
				var last = (guestInfo.last_name != '') ? (guestInfo.last_name).substr(0,1)+'.' : '';
				var fName = (guestInfo.first_name != '') ? ((guestInfo.first_name) + ' ' + last) : user[0];
				$('li#log-in').html('<a href="/book-now/ifly-profile">Hi, '+fName+'</a>');
				IFLY.profile.init(response);
			}
	
			IFLY.callApi('method=get_guest&controller=siriusware&format=json&tunnel='+y+'&guest_no='+x, success,null,null);	
		
		} else {
//			if (FB) {
//				FB.getLoginStatus(function(response) {
//					//  {
//					//     status: 'connected',
//					//     authResponse: {
//					//        accessToken: '...',
//					//        expiresIn:'...',
//					//        signedRequest:'...',
//					//        userID:'...'
//					//     }
//					//  }
//					
//					//alert('getLoginStatus: ' + response.status);
//					//console.log('FB.getLoginStatus', response);
//					//IFLY.profile.checkLoginStatus();
//					if (response.status=='connected') {
//						FB.api('/me',function(response){
//							fbObj = response;
//							//console.log('FB connected: ',response);
//							IFLY.profile.facebookInit(fbObj,false);
//						//	IFLY.profile.loginFacebook(false);
//							//alert('Welcome back ' + response.name);
//						});
//					}
//				});
//
//			}
			if($('body.flyer').length>0) {
	//		if($('body.flyer, body.lostpassword').length>0) {
				IFLY.booking.checkSession();
				getParams();
				IFLY.profile.init(null);
			}

			$('li#log-in a').unbind('click').click(function(e) {
				e.preventDefault();
				$.colorbox({html:$(".modal.login").html(),
					height:625,
					//onComplete:IFLY.booking.init.login
					onComplete:IFLY.profile.loginInit,
					onClosed: function() {
								$('form#register-ifly-acct').fadeOut(0);
								$('form.reset-password').fadeOut(0);
								$('div.return-user').css('display','inline-block');
								$('div.logloc').css('display','block');
								$('div.new-user').css('display','inline-block');
							//	$('form#lgn').fadeTo(0, 1);
								$('div#login.overlay div.row h1').html('Login');
							}
				});
			});
		}
	},
	init: function(data) {
		var checkPath = window.location.pathname.split('/');
	//	console.log('checkPath[2]:',checkPath[2])
		switch (checkPath[2]) {
			case 'ifly-profile':
				IFLY.booking.checkSession();
				if(IFLY.loggedIn) {
					console.log('REACHED');
					IFLY.callApi('method=get_guest&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&guest_no='+urlParams.guest_no, IFLY.profile.showProfilePage,null,null);			
				}
			break;
			case 'password_reset':
				function resetConfirmed(response, textStatus, jqXHR) {
					console.log('password_reset',response);
					$.colorbox({
						width:400,
						height:400,
						html:'<div class="row">Your password is now reset.</div><div class="row cont-shopping"><a href="javascript:void(0);" class="btn close"><em></em><span>CLOSE</span></a></div>',
						onComplete:function() {
							$('#colorbox a.btn.close').unbind('click').click(function() {
								 $.fn.colorbox.close();
							});
						}
					});  
				}
				
				getParams();
				
				if(urlParams.token && urlParams.token !=null) {
					jQuery.validator.addMethod(
						'ContainsAtLeastOneDigit',
						function (value) { 
							return /[0-9]/.test(value); 
						},  
						'Your password must contain at least one digit.'
					);  
				 
					jQuery.validator.addMethod(
						'ContainsAtLeastOneCapitalLetter',
						function (value) { 
							return /[A-Z]/.test(value); 
						},  
						'Your password must contain at least one capital letter.'
					);
					$('#lost-password').data('validator', null).validate({
						   rules: {
								password: {
										required: true,
										rangelength: [8, 12],
										ContainsAtLeastOneDigit: true,
										ContainsAtLeastOneCapitalLetter: true
								},
								confirm_password: {
									required: true,
									equalTo: "#password"
								}
						   },
						   messages: {
								password: {
									required: "Please provide your password",
									rangelength:"passwords must be 8 to 12 characters long"
												//passwords must contain a letter and a number
								},
								confirm_password: {
									required: "Please re-enter your password",
									equalTo: "Passwords do not match"
								}
						   }
						});
				
					$('a.btn.reset').unbind('click').click(function() {
						if($('#lost-password').valid()) {
							//FOR DEV////////////////////////////
							var tunnel = (IFLY.current_tunnel != null) ? IFLY.current_tunnel : 'ifo';
////////////////////////////REMOVE ABOVE ///////////////							
							
							
							$form = $('form#lost-password');
							$inputs = $form.find("input");
							serializedData = $form.serialize();
							
							var call='method=set_password&controller=siriusware&format=json&tunnel='+tunnel+'&token='+urlParams.token+'&'+serializedData;
							
	//						var call='method=set_password&controller=siriusware&format=json&tunnel='+tunnel+'&token='+urlParams.token+'&'+serializedData;
							console.log('call',call);
							IFLY.callApi(call,resetConfirmed,null,null);		
						}
					});
				}

			break;
			default:
				function continueToProfile(response,textStatus,jqXHR) {
					var guestID = response.data;
					IFLY.callApi('method=get_guest&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&guest_no='+guestID, IFLY.profile.showProfilePage,null,null);		
					var call = 'method=get_guest_assets&controller=sessionData&format=json&processed=&date=&tunnel='+IFLY.current_tunnel+'&guest_id='+guestID;
					console.log('call',call);
					IFLY.callApi(call, IFLY.profile.showFlyerPageThumbs,null,null);			
				}
				
				if (checkPath[1] == 'flyer') {
					IFLY.booking.checkSession();
					getParams();

					console.log('flyer: ',checkPath[2]);
				//	IFLY.booking.checkSession();
				//	if(IFLY.loggedIn) {
				//	}
					if(checkPath[2] && checkPath[2] !='') {
						function successID(response, textStatus, jqXHR) {
							console.log('successID', response);
							var gID = response.data;
///////////////////////////////////
							if(!IFLY.locationHandler.isLocalSite()) {
								$.post('/php-lib/user_session.php', {},  function(response) {
									var data = $.parseJSON(response);
									console.log('response: ',data);
									IFLY.current_tunnel = (data.user_tunnel) ? data.user_tunnel : 'ifo';
////////////////////////////REMOVE ABOVE ///////////////							
									var call = 'method=get_guest_assets&controller=sessionData&format=json&processed=&date=&tunnel='+IFLY.current_tunnel+'&guest_id='+gID;
									console.log('call',call);
									IFLY.callApi(call, IFLY.profile.showFlyerPageThumbs,null,null);	
								});
							} else {
								var call = 'method=get_guest_assets&controller=sessionData&format=json&processed=&date=&tunnel='+IFLY.current_tunnel+'&guest_id='+gID;
								console.log('call',call);
								IFLY.callApi(call, IFLY.profile.showFlyerPageThumbs,null,null);	
							
							}
							$('section.clearfix').append('<div class="thumbHolder"></div>'+IFLY.showLoader());
						}
						//get_guest_id_from_lookup_code
						var call = 'method=get_guest_id_from_lookup_code&controller=siriusware&format=json&lookup_code='+checkPath[2];
						console.log('call',call);
						IFLY.callApi(call, successID,null,null);	
								
					} else if(data != null && data.guest_no) {
						IFLY.profile.showProfilePage(data,null,null);
						var call = 'method=get_guest_assets&controller=sessionData&format=json&processed=&date=&tunnel='+IFLY.current_tunnel+'&guest_id='+data.guest_no;
						console.log('call',call);
						$('section.clearfix').append('<div class="thumbHolder"></div>'+IFLY.showLoader());
						IFLY.callApi(call, IFLY.profile.showFlyerPageThumbs,null,null);			
					} else {
						
						if(IFLY.loggedIn) {
							IFLY.callApi('method=get_guest&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&guest_no='+urlParams.guest_no, IFLY.profile.showProfilePage,null,null);			
							var call = 'method=get_guest_assets&controller=sessionData&format=json&processed=&date=&tunnel='+IFLY.current_tunnel+'&guest_id='+urlParams.guest_no;
							$('section.clearfix').append('<div class="thumbHolder"></div>'+IFLY.showLoader());
							console.log('call',call);
							IFLY.callApi(call, IFLY.profile.showFlyerPageThumbs,null,null);			
						} else {
//							IFLY.callApi('method=get_guest_id_from_lookup_code&controller=siriusware&format=json&lookup_code ='+checkPath[2], continueToProfile,null,null);			
						}
					}

				}
			
			break;
			
		
			
		}
//		console.log('checkPath',checkPath);
	},
	setSession: function(data) {
		if(data.guest_no)
			cookieHandler.create('guest_no',data.guest_no,14);
		if(data.token)
			cookieHandler.create('token',data.token,14);
		if(IFLY.current_tunnel)
			cookieHandler.create('user_tunnel',IFLY.current_tunnel,14);
		if(data.balance)
			cookieHandler.create('user_balance',data.balance,14);
		
		//var call = '/php-lib/user_session.php?guest_no='+data.guest_no+'&token='+data.token+'&user_tunnel='+IFLY.current_tunnel+'&user_balance='+data.balance;
		console.log('setSession');
		$.post('/php-lib/user_session.php', {guest_no:data.guest_no,token:data.token, user_tunnel:IFLY.current_tunnel,user_balance:data.balance},  function(response) {
			// data contains a count and the array of results...
			var data = $.parseJSON(response);
			console.log('setSession response: ',data);
			IFLY.profile.checkLoginStatus();
			//IFLY.profile.facebookInit(data,true);
		});
		
	},
	getSession:function() {
		$.post('/php-lib/user_session.php', {},  function(response) {
			var data = $.parseJSON(response);
				console.log('getSession response: ',data);
			return data;
		});
	},
	logoutInit: function() {
		function logoutConfirmed(response, textStatus, jqXHR) {
			$.colorbox({
				width:400,
				height:400,
				html:'<div class="row">You have successfully logged out.</div><div class="row cont-shopping"><a href="javascript:void(0);" class="btn close"><em></em><span>CLOSE</span></a></div>',
				onComplete:function() {
					cookieHandler.remove('guest_no');
					cookieHandler.remove('token');

					$('#colorbox a.btn.close').unbind('click').click(function() {
						 $.fn.colorbox.close();
					});
				}
			});  
		}
		if(IFLY.loggedIn()) {
			var x = cookieHandler.read('guest_no');
			var call='method=log_out&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&token'+serializedData;
			console.log('call',call);
			IFLY.callApi(call,logoutConfirmed,badLogin,null);
		}
		
		
	},
	loginInit: function() {

		$('#login div#guestcheckout').css('display','none');
		if(IFLY.locationHandler.isLocalSite()) {
			$('#lgn div.location').css('display','none');
		} else {
			$('#colorbox select[id$="flyer-location"]').change(function() {
				IFLY.current_tunnel = $(this).val();
				console.log('changed',IFLY.current_tunnel);
			});
		}
		
		//$.uniform.update('form#lgn, form#register-ifly-acct');
		$.uniform.update();
		$('form#lgn').data('validator', null).validate({
		   rules: {
			 web_user: {
						required: true,
						email: true
			 },
			 password: "required"
		   },
		   messages: {
			 web_user: "Please enter a valid email address for your username",
			 password: "Passwords must be 8-12 characters"
		   }
		});
			
		$('#lgn a.login').unbind('click').click(function(e) {
			e.preventDefault();
			
			//USER LOGIN SUCCESSFUL
			function loginConfirmed(response, textStatus, jqXHR) {
				IFLY.killLoader();
				console.log('loginConfirmed',response);
				if(response.status=="OK") {
					var data  = response.data;
					IFLY.profile.setSession(data);

					$('#cboxLoadedContent div.status p').html('Login complete!');
					
					$('#cboxLoadedContent div.status').append('<div><br /><a href="javascript:void(0);" class="btn close"><em></em><span>CLOSE</span></a></div>')
					$('#colorbox a.btn.close').unbind('click').click(function() {
						$.fn.colorbox.close();
						//location.reload(true);
					});
				} else {
					$('#cboxLoadedContent div.status p').html(response.message);
				}
	//			$.colorbox.close();
			}
			
			//IF USER DIDN'T LOGIN
			function badLogin(response, textStatus, jqXHR) {
				console.log('badLogin',response);
				$('#cboxLoadedContent div.status p').html(response.message);
			}

			if(!IFLY.locationHandler.isLocalSite() && $('#colorbox select[id$="flyer-location"]').val() !='FLIGHT LOCATION') {
				if($('#colorbox #lgn').valid()) {
					$form = $('form#lgn');
					$inputs = $form.find("input");
		//			$inputs = $form.find("input").not('div.location');
					serializedData = $form.serialize();
					var call='method=log_in&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&'+serializedData;
					console.log('call',call);
					//call method=log_in&controller=siriusware&format=json&tunnel=undefined&web_user=&password=&web_user=shun%40coloringbookstudio.com&password=shun1234
					IFLY.booking.callApi(call,loginConfirmed,badLogin,null);
					$('#cboxLoadedContent div.status p').after(IFLY.showRevLoader());
				}
			} else {
				$('#cboxLoadedContent div.status p').html('<span style="color:#FF0000">Please select a Tunnel location.</span>');
			}


		});
		
		//RESET PASSWORD
		$('a.lost-password').unbind('click').click(function(e) {
			e.preventDefault();
			$('div#login.overlay div.row h1').html('Forgot Password');
		
			if(IFLY.locationHandler.isLocalSite()) {
				$('form.reset-password div.location').css('display','none');
			} else {
				$('form.reset-password select[id$="flyer-location"]').change(function() {
					IFLY.current_tunnel = $(this).val();
					//$.uniform.update();
				});
			}
			
			$('div.return-user, div.logloc').css('display','none');
			$('div.new-user').css('display','none');
			$('form.reset-password').fadeIn(300);
			$('form.reset-password').data('validator', null).validate({
				rules: {
					web_user: {
						required: true,
						email:true,
					}
				},
				messages: {
					web_user: "Please enter a valid email address"
				}
			});

			
			$('a.btn.send_email').unbind('click').click(function(e) {
				e.preventDefault();
				if($('form.reset-password').valid()) {
					$form = $('form.reset-password');
					$inputs = $form.find("input");
					serializedData = $form.serialize();
					var call = 'method=reset_ifly_password&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&'+serializedData;
					IFLY.booking.callApi(call,success,null,null);
					$('#cboxLoadedContent div.status p').after(IFLY.showRevLoader());
				}
			});
			
			function success(response, textStatus, jqXHR) {
				console.log('response',response);
				$('#cboxLoadedContent div.status p').html(response.message);
				$('#cboxLoadedContent div.status').after('<div class="row status"><a href="javascript:void(0);" class="btn close"><em></em><span>CLOSE</span></a></div>')
				$('#colorbox a.btn.close').unbind('click').click(function(e) {
					e.preventDefault();
					$.fn.colorbox.close();
				});
			}
		});
		
		
		//INIT FACEBOOK BUTTON
		$('a.btn.facebook').unbind('click').click(function(e) {
			e.preventDefault();
			IFLY.profile.loginFacebook(true);
		});
		
		$('a.btn.myifly').click(function(e) {
			//$('form#register-ifly-acct').css('display','block');
			$('div#login.overlay div.row h1').html('Register');
			 $('#register-ifly-acct p.register-status').html('* Required fields');
			 
			urlParams.tunnel = IFLY.current_tunnel;
			if(IFLY.locationHandler.isLocalSite()) {
				$('#register-ifly-acct div.location').css('display','none');
			} else {
				$('#register-ifly-acct select[id$="flyer-location"]').change(function() {
					IFLY.current_tunnel = $(this).val();
					//$.uniform.update();
				});
			}
			
			$('div.return-user, div.logloc').css('display','none');
			$('div.new-user').css('display','none');
			$('form#register-ifly-acct').fadeIn(300);

			//$('form#lgn').fadeIn(300);
			
			jQuery.validator.addMethod(
				'ContainsAtLeastOneDigit',
				function (value) { 
					return /[0-9]/.test(value); 
				},  
				'Your password must contain at least one digit.'
			);  
		 
			jQuery.validator.addMethod(
				'ContainsAtLeastOneCapitalLetter',
				function (value) { 
					return /[A-Z]/.test(value); 
				},  
				'Your password must contain at least one capital letter.'
			);
		
			$('#colorbox #register-ifly-acct').data('validator', null).validate({
				rules: {
					first_name: "required",
					last_name: "required",
					web_user: {
							required: true,
							email:true,
						},
					password: {
							required: true,
							rangelength: [8, 12],
        			        ContainsAtLeastOneDigit: true,
		            	    ContainsAtLeastOneCapitalLetter: true
					},
					confirm_password: {
						required: true,
						equalTo: "#colorbox #password"
					}
				},
				messages: {
					first_name: "First name is required",
					last_name: "Last name is required",
					web_user: "Please enter a valid email address",
					password: {
						required: "Please provide your password",
						rangelength:"passwords must be 8 to 12 characters long"
									//passwords must contain a letter and a number
					},
					confirm_password: {
						required: "Please re-enter your password",
						equalTo: "Passwords do not match"
					}
				}
			});
	
			$.uniform.update();
			
			function continueTo(response,textStatus, jqXHR) {
					IFLY.killLoader();
					console.log('registration response:',response);
				if (response.data ==0 || response.status =="ERR") {
					
					$('#colorbox #register-ifly-acct p.register-status').html(response.message);
					$('#colorbox #register-ifly-acct a.btn.register').html('<em></em><span>CONTINUE</span>');
					$('#colorbox #register-ifly-acct a.btn.register').unbind('click').click(function() {
						 $.fn.colorbox.close();
					});
				} else {
					cookieHandler.create('guest_no',response.guest_no);
					$('#register-ifly-acct p.register-status').html('Registration complete!');
					//setTimeout(continueClose,2000);
				}
				function continueClose() {
					$.fn.colorbox.close();
					//var userData = $.param(response.data);
					//var dURL = IFLY.booking.init.step6path+'?&guest_no='+response.data;
					//window.location = dURL;
				}
			}
			
			function registrationError(response, textStatus, jqXHR) {
				IFLY.killLoader();
				$('#cboxLoadedContent div.status p').html(response.message);
				console.log('registrationError',response);
			}
			$('#register-ifly-acct a.register').unbind('click').click(function(e) {
				if($('#colorbox form#register-ifly-acct').valid()) {
					$form = $('form#register-ifly-acct');
					$inputs = $form.find('input');
					$email = $form.find('input[name="web_user"]');
					serializedData = $inputs.serialize();
					var call='method=create_ifly_credentials&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&e_mail='+$email+'&'+serializedData;
					console.log('call',call);
					IFLY.callApi(call,continueTo,registrationError,null);
					$('#cboxLoadedContent div.status p').after(IFLY.showRevLoader());
				}
				e.preventDefault();
			});
			
			e.preventDefault();
		});
	},
	loginCheckoutInit: function() {
		$.uniform.update('form#lgn, form#register-ifly-acct');
		$('#login div#guestcheckout').css('display','block');
		$('#lgn div.location').css('display','none');
		
		$('#colorbox #lgn').data('validator', null).validate({
		   rules: {
			 web_user: {
						required: true,
						email: true
			 },
			 password: "required"
		   },
		   messages: {
			 web_user: "Please enter a valid email address for your username",
			 password: "Passwords must be 8-12 characters"
		   }
		});
			
		$('#lgn a.login').unbind('click').click(function(e) {
			
			//USER LOGIN SUCCESSFUL
			function loginConfirmed(response, textStatus, jqXHR) {
				console.log('loginConfirmed',response);
				IFLY.killLoader();
				function continueCheckout() {
					var data  = response.data;
					IFLY.profile.setSession(data);
					//cookieHandler.create('guest_no',data.guest_no,14);
					//cookieHandler.create('token',data.token,14);
					
					var userData = $.param(data);
					var dURL = IFLY.booking.init.step6path+'?'+userData;
					window.location = dURL;
				}
				if(response.status=="OK") {
					$('#cboxLoadedContent div.status p').html('Login complete! Continuing to checkout.');
					setTimeout(continueCheckout,2000);
				} else {
					$('#cboxLoadedContent div.status p').html(response.message);
				}
	//			$.colorbox.close();
			}
			
			//IF USER DIDN'T LOGIN
			function badLogin(response, textStatus, jqXHR) {
				console.log('badLogin',response);
				IFLY.killLoader();
				$('#cboxLoadedContent div.status p').html(response.message);
			}

			if($('#colorbox #lgn').valid()) {
				$form = $('form#lgn');
				$inputs = $form.find("input");
				serializedData = $form.serialize();
				var call='method=log_in&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&'+serializedData;
				console.log('call',call);
				IFLY.booking.callApi(call,loginConfirmed,badLogin,null);
				$('#cboxLoadedContent div.status p').after(IFLY.showRevLoader());

			}
			e.preventDefault();
		});
		$('a.btn.facebook').unbind('click').click(function(e) {
			e.preventDefault();
			IFLY.profile.loginFacebook(true);
		});

		//CHECKOUT AS A GUEST
//		$('#login.overlay a.guest').click(function(e) {
//			//var serializedData = $.param(urlParams.cartData.id);
//			var dURL = IFLY.booking.init.step6path; //+'?'+serializedData;
//			console.log('dURL',dURL);
//			window.location = dURL;
//			e.preventDefault();
//		});
		$('a.btn.myifly').click(function(e) {
			//$('form#register-ifly-acct').css('display','block');
			$('div#login.overlay div.row h1').html('Register');
			 $('#register-ifly-acct p.register-status').html('* Required fields');
			
			$('div.return-user, div.logloc').css('display','none');
			$('div.new-user').css('display','none');

			$('form#register-ifly-acct').fadeIn(300);
//			$('form#lgn').fadeTo(300, 0);
			
			jQuery.validator.addMethod(
				'ContainsAtLeastOneDigit',
				function (value) { 
					return /[0-9]/.test(value); 
				},  
				'Your password must contain at least one digit.'
			);  
		 
			jQuery.validator.addMethod(
				'ContainsAtLeastOneCapitalLetter',
				function (value) { 
					return /[A-Z]/.test(value); 
				},  
				'Your password must contain at least one capital letter.'
			);
		
			$('#colorbox #register-ifly-acct').data('validator', null).validate({
				rules: {
					first_name: "required",
					last_name: "required",
					web_user: {
							required: true,
							email:true,
						},
					password: {
							required: true,
							rangelength: [8, 12],
        			        ContainsAtLeastOneDigit: true,
		            	    ContainsAtLeastOneCapitalLetter: true
					},
					confirm_password: {
						required: true,
						equalTo: "#colorbox #password"
					}
				},
				messages: {
					first_name: "First name is required",
					last_name: "Last name is required",
					web_user: "Please enter a valid email address",
					password: {
						required: "Please provide your password",
						rangelength:"passwords must be 8 to 12 characters long"
									//passwords must contain a letter and a number
					},
					confirm_password: {
						required: "Please re-enter your password",
						equalTo: "Passwords do not match"
					}
				}
			});
	
			$.uniform.update();
			
			function continuetoCheckout(response,textStatus, jqXHR) {
				//console.log('continuetoCheckout response:',response);
			//	cookieHandler.create('guest_id',response.data);
				if (response.data ==0 || response.status =="ERR") {
					
					$('#colorbox #register-ifly-acct p.register-status').html(response.message);
					$('#colorbox #register-ifly-acct a.btn.register').html('<em></em><span>CONTINUE</span>');
					$('#colorbox #register-ifly-acct a.btn.register').unbind('click').click(function() {
						 $.fn.colorbox.close();
					});
				} else {
					cookieHandler.create('guest_no',response.guest_no);
					$('#register-ifly-acct p.register-status').html('Registration complete! Redirecting to checkout...');
					setTimeout(continueCheckout,2000);
				}
				function continueCheckout() {
					//var userData = $.param(response.data);
					var dURL = IFLY.booking.init.step6path+'?&guest_no='+response.data;
					window.location = dURL;
				}
			}
			
			function registrationError(response, textStatus, jqXHR) {
				$('#cboxLoadedContent div.status p').html(response.message);
				console.log('registrationError',response);
			}
			$('#register-ifly-acct a.register').unbind('click').click(function(e) {
				if($('#colorbox form#register-ifly-acct').valid()) {
					$form = $('form#register-ifly-acct');
					$inputs = $form.find('input');
					$email = $form.find('input[name="web_user"]');
					serializedData = $inputs.serialize();
					var call='method=create_ifly_credentials&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&e_mail='+$email+'&'+serializedData;
					IFLY.callApi(call,continuetoCheckout,registrationError,null);
				}
				e.preventDefault();
			});
			
			e.preventDefault();
		});

	},
	loginFacebook:function(redir) {
		if(fbObj == null) {
			FB.login(function(response){/**/},{scope: 'email, publish_actions'});
		} else {
			IFLY.profile.facebookInit(fbObj,redir);
		}
	},
	facebookInit: function(obj,redir) {
        // data contains a count and the array of results...
		if(jQuery("#colorbox").css("display") != "none"){
			$('#cboxLoadedContent div.status p').after(IFLY.showLoader());
		}
		var lookupLoc = (cookieHandler.read('user_tunnel') != null) ? cookieHandler.read('user_tunnel') : IFLY.current_tunnel;
		var call='method=log_in_facebook&controller=siriusware&format=json&tunnel='+ lookupLoc +'&e_mail='+obj.email+'&first_name='+obj.first_name+'&offset=&last_name='+obj.last_name;
		
		console.log('facebookInit call',call);
		IFLY.callApi(call,facebookLoginConfirmed,facebookLoginError,null);

		function facebookLoginConfirmed(response, textStatus, jqXHR) {
			console.log('facebookLoginConfirmed response',response);
			IFLY.killLoader();
				var data  = response.data;
				if(response.status == 'ERR') {
						facebookLoginError(response, textStatus, jqXHR);
				} else {
//					IFLY.profile.checkLoginStatus();
					IFLY.profile.setSession(data);
					
					if(redir==true) {
						if(jQuery("#colorbox").css("display") != "none") { 
							$('#cboxLoadedContent div.status p').html('Login complete! Continuing to checkout.');
							setTimeout(continueCheckout,2000);
						} else { 
						  // code when colorbox not open 
						  continueCheckout();
						}	
					} else {
						if(jQuery("#colorbox").css("display") != "none") { 
							$('#cboxLoadedContent div.status p').html('Login complete!');
						}
					}
				}
				console.log('facebookLoginConfirmed',response);
				function continueCheckout() {
					var userData = $.param(data);
					var dURL = IFLY.booking.init.step6path+'?'+userData;
					window.location = dURL;
				}
		}


		function facebookLoginError(response, textStatus, jqXHR) {
			console.log('ERROR', response);
			if(jQuery("#colorbox").css("display") != "none") { 
				$('#cboxLoadedContent div.status p').html(response.message);
			} else { 
			
//				$.colorbox({
//					width:400,
//					//height:400,
//					html:'<div class="row"><p style="padding-top:20px;">'+response.message+'</p></div><div class="row cont-shopping"><a href="javascript:void(0);" class="btn close"><em></em><span>CLOSE</span></a></div>',
//					onComplete:function() {
//						$('#colorbox a.btn.close').unbind('click').click(function() {
//							$.fn.colorbox.close();
//						});
//					}
//				});  
			}
		}
	},
	checkFacebook: function(redir) {
		$.post('/php-lib/fbHandler.php', {},  function(response) {
			// data contains a count and the array of results...
			var data = $.parseJSON(response);
			console.log('FACE: ',data);
			IFLY.profile.facebookInit(data,true);
		});
		
	},
	populateUserForm: function(response, textStatus, jqXHR) {
			var userInfo = response.data[0];
			console.log('guestFound: ',userInfo);
			$('form#process_sale input[name="first_name"]').attr('value',userInfo.first_name);
			$('form#process_sale input[name="last_name"]').attr('value',userInfo.last_name);
			$('form#process_sale input[name="e_mail"]').attr('value',userInfo.e_mail);
			$('form#process_sale input[name="address"]').attr('value',userInfo.address);
			$('form#process_sale input[name="zip"]').attr('value',userInfo.zip);
			var area = userInfo.area_code;
			$('form#process_sale input[name="cell_phone1"]').attr('value',area.replace(/ /,''));
			var parsePhone = userInfo.phone.replace(/-;/g, "");
			
			$('form#process_sale input[name="cell_phone2"]').attr('value',parsePhone.substr(0, 3));
			$('form#process_sale input[name="cell_phone3"]').attr('value',parsePhone.substr(3, 4));
		},
	showProfilePage: function(response, textStatus, jqXHR) {
			var userInfo = response.data[0];
			console.log('showProfilePage guestFound: ',userInfo);
			var profileOutput = '<div class="profile-info"><h3>ACCOUNT INFO</h3>';
			profileOutput +='<p>Username: ' +userInfo.web_user+'</p>';
			profileOutput +='<p>Password: *********</p>';
			profileOutput +='<p><a href="#" class="get-pass">Change Password</a></p>';
			profileOutput +='<p>Name: '+userInfo.first_name+' '+userInfo.last_name+'</p>';
			profileOutput +='<p>Address: ' +userInfo.address+'</p>';
			profileOutput +='<p>Zip Code: ' +userInfo.zip+'</p>';
			profileOutput +='<p>Phone: ' +userInfo.area_code+userInfo.phone+'</p>';
			profileOutput += '</div><div class="row"><br /><br /><a href="javascript:void(0);" class="btn logout"><em></em><span>LOG OUT</span></a></div>';
			//profileOutput +='<h3>DOWNLOADS/h3>';
			$('div.content-wrapper section').append(profileOutput);
			
			
			///////////////////////////////////
			//LOGOUT BUTTON
			///////////////////////////////////
			$('a.get-pass').unbind('click').click(function(e) {
				
				e.preventDefault();
			});
			
			$('a.btn.logout').unbind('click').click(function(e) {
				//USER LOGIN SUCCESSFUL
				function logoutConfirmed(response, textStatus, jqXHR) {
					console.log('loginConfirmed',response);
	
					function continueCheckout() {
						var data  = response.data;
						cookieHandler.remove('guest_no');
						cookieHandler.remove('user_tunnel');
						cookieHandler.remove('token');
						
						var userData = $.param(data);
						var dURL = '/';
						window.location = dURL;
					}
					if(response.status=="OK") {
						$('a.btn.logout').after('<p>You\'ve successfully logged out.</p>');
						setTimeout(continueCheckout,2000);
					} else {
						$('a.btn.logout').after('<p>'+response.message+'</p>');
					}
		//			$.colorbox.close();
				}
				
				var x = (cookieHandler.read('token')) ? cookieHandler.read('token') : null;
//				var y = (cookieHandler.read('guest_no')) ? cookieHandler.read('token') : null;
	
				if(x != null) {
					var call='method=log_out&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&token='+x;
					console.log('call',call);
					IFLY.booking.callApi(call,logoutConfirmed,null,null);
				}
				e.preventDefault();
			});
			

//cookieHandler.read('token');
		},
	showFlyerPageThumbs: function(response, textStatus, jqXHR) {
		console.log('showFlyerPageThumbs', response);
		var data = response.data;
		IFLY.photovid.session = [];
		var div = $('.thumbHolder');
			div.imagesLoaded(function(){
				div.masonry({
					itemSelector : '.item'//,columnWidth : 192
				})
			});
				//div.css('display','block');
				var thumbs = '';
				//var thumbCount=0;
				//var emptyFlag = $(tDiv + ' .item').length > 0;
				$.each(data,function(index,value){
					IFLY.photovid.session.push(value)
	//				if (this.status == 'FLV' || this.status == 'MPG') {
					if (this.event == 'video clipped') {
						var thumbPath = (this.lores_url) ? this.lores_url : (this.medres_url) ? this.medres_url : null;
						thumbPath = thumbPath.replace('.flv','.png');
						var overlayPath = (this.medres_url) ? this.medres_url : this.lores_url;
						var flyerNum = (this.person_ct) ? 'Flyer #'+parseInt(this.person_ct): '';
						if (thumbPath != null) {
							thumbs+='<div class="item video"><span style="left:3px;right:initial;">'+this.event_id+'</span><a href="'+IFLY.photovid.basePath+overlayPath+'"><img src="'+IFLY.photovid.basePath+thumbPath+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';
						//	thumbCount++;
						}
					} else {
						var thumbPath = (this.lores_url) ? this.lores_url : (this.medres_url) ? this.medres_url : this.hires_url;
						var overlayPath = (this.hires_url && this.hires_url != null) ? this.hires_url : (this.medres_url) ? this.medres_url : this.medres_url;
						var flyerNum = (this.person_ct) ? 'Flyer #'+parseInt(this.person_ct): '';
						if (thumbPath != null) {
							thumbs+='<div class="item image"><span style="left:3px;right:initial;">'+this.event_id+'</span><a href="'+IFLY.photovid.basePath+overlayPath+'"><img src="'+IFLY.photovid.basePath+thumbPath+'" class="gradient-border drop-shadow rounded-corners" width="175" height="116" /></a></div>';
						//	thumbCount++;
						}
					}
				});
				div.imagesLoaded(function(){
					var $thumbs = $(thumbs);
					div.append($thumbs).masonry('appended', $thumbs).masonry( 'reload' );
					
					$('.item a').each(function(index,value){
						if($(this).parent().hasClass('image')) {
							$(this).unbind('click').click(function(e) {
								e.preventDefault();
								var ovObj = IFLY.photovid.session[index];
								var overlayPath = (ovObj.hires_url && ovObj.hires_url != null) ? ovObj.hires_url : (ovObj.medres_url) ? ovObj.medres_url : ovObj.medres_url; 
											//= (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
								var img = IFLY.photovid.basePath+overlayPath;
								
								var trackPath = overlayPath.split('/');
								var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
								IFLY.analytics.sendEvent('Photo', 'Views', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);
	
	//							var subCheck = full.split('.');
	//							var trackPath = overlayPath.split('/');
	//							IFLY.analytics.sendEvent('Photo', 'Views', subCheck[0]+trackPath[trackPath.length-1], null);
								
								IFLY.profile.displayImageOverlay(index,img);
								IFLY.photovid.sessionIndex = index;
								IFLY.photovid.initOverlayArrows();
							});
						} else {
							$(this).unbind('click').click(function(e) {
								e.preventDefault();
								var ovObj = IFLY.photovid.session[index];
								var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
								var img = IFLY.photovid.basePath+overlayPath;
								
								var trackPath = overlayPath.split('/');
								var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
								IFLY.analytics.sendEvent('Video', 'Plays', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);
	
								IFLY.profile.displayVideoOverlay(index,img);
								IFLY.photovid.sessionIndex = index;
								IFLY.photovid.initOverlayArrows();
							});
						}
					});

					//PHOTO CART OVERLAY
//					$('.item.image a').each(function(index,value){
//						$(this).unbind('click').click(function(e) {
//							e.preventDefault();
//							var ovObj = IFLY.photovid.session[index];
//							var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
//							var img = IFLY.photovid.basePath+overlayPath;
//							
//							var trackPath = overlayPath.split('/');
//							var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
//							IFLY.analytics.sendEvent('Photo', 'Views', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);
//
////							var subCheck = full.split('.');
////							var trackPath = overlayPath.split('/');
////							IFLY.analytics.sendEvent('Photo', 'Views', subCheck[0]+trackPath[trackPath.length-1], null);
//							
//							IFLY.profile.displayImageOverlay(index,img);
//							IFLY.photovid.sessionIndex = index;
//							IFLY.photovid.initOverlayArrows();
//						});
//					});
//					$('.item.video a').each(function(index,value){
//						$(this).unbind('click').click(function(e) {
//							e.preventDefault();
//							var ovObj = IFLY.photovid.session[index];
//							var overlayPath = (ovObj.medres_url != null) ? ovObj.medres_url : ovObj.lores_url;
//							var img = IFLY.photovid.basePath+overlayPath;
//							
//							var trackPath = overlayPath.split('/');
//							var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
//							IFLY.analytics.sendEvent('Video', 'Plays', selTunnelInfo.subdomain+trackPath[trackPath.length-1], null);
//
//							IFLY.profile.displayVideoOverlay(index,img);
//							IFLY.photovid.sessionIndex = index;
//							IFLY.photovid.initOverlayArrows();
//						});
//					});
					IFLY.killLoader();
				});
		},
	displayImageOverlay: function(index,imgPath) {
		console.log('displayImageOverlay', IFLY.photovid.session[index]);
		var currentSession = IFLY.photovid.session[index];
		var flyerNum = (currentSession.person_ct) ? 'Flyer #'+parseInt(currentSession.person_ct): '';
		
		var overlayHTML = '<a class="btn-prev" href="#">&lt;</a><a class="btn-next" href="#">&gt;</a>\
						   <div class="photo-share"><a class="btn" href="javascript:void(0);">SHARE</a></div>\
						   <div class="overlay-info"><p>'+currentSession.event_id+'</p><p>'+flyerNum+'</p></div>\
						   <img class="gradient-border drop-shadow rounded-corners" id="overlay-img" width="600" height="auto" src="'+imgPath+'" />';
		$.colorbox({width:700, height: 600, html:overlayHTML, onComplete:function() {
			$('div.photo-share a.btn').unbind('click').click(function(e) {
					e.preventDefault();
					IFLY.photovid.callFace(imgPath,index);
			});
		}});
	},
	displayVideoOverlay: function(index, vidPath) {
		console.log('displayVideoOverlay', IFLY.photovid.session[index]);
		var currentSession = IFLY.photovid.session[index];
		var flyerNum = (currentSession.person_ct) ? 'Flyer #'+parseInt(currentSession.person_ct): '';

		var sourceImg = IFLY.photovid.basePath + currentSession.lores_url;
		sourceImg = sourceImg.replace('.flv','.png');
		var overlayHTML = '<a class="btn-prev" href="#">&lt;</a><a class="btn-next" href="#">&gt;</a><div id="vHolder"><video id="videoHolder" class="video-js vjs-ifly-skin gradient-border drop-shadow rounded-corners" controls preload="auto" width="600" height="340" poster="'+sourceImg+'">' + '<source src="'+vidPath+'" type=\'video/mp4\' /></video><div class="video-share"><a class="btn" href="javascript:void(0);">SHARE</a></div></div><div class="vid overlay-info"><p>'+currentSession.id+'</p><p>'+flyerNum+'</p></div>';
//		console.log('displayVideoCartOverlay', index, vidPath, sourceImg);
		$.colorbox({
			width:700,
			height: 500,
			html:overlayHTML,
			onComplete:function() {
//				$('#photo-cart p').html(IFLY.photovid.price);
				if(!swfobject.hasFlashPlayerVersion("9.0.115")) {
					IFLY.vidcontrols.init(vidPath,'videoHolder','.video-controls a.play.btn','.video-controls a.pause.btn','.video-controls div.scrubber');
				} else {
					
					$('#vHolder').css('background-image','url('+sourceImg+')');
					var flashvars = {
							vidPath: vidPath,
							posterPath: sourceImg
						};
					var params = {/*bgcolor: "#222222",*/ wmode: "transparent", quality:"high",allowscriptaccess:"always",allownetworking:"all"};
					var attributes = {swliveconnect:"true"};
					swfobject.switchOffAutoHideShow();
					swfobject.embedSWF( 'http://www.iflyworld.com/swf/ifly-videoplayer.swf', "vHolder", "600", "340", "9.0.0","expressInstall.swf", flashvars, params, attributes);
					$('#vHolder').addClass('gradient-border').addClass('drop-shadow').addClass('rounded-corners');
				}
				$('div.video-share a.btn').unbind('click').click(function(e) {
						IFLY.photovid.callFace(sourceImg, index);
						e.preventDefault();
				});
 		}});
	},
	saveProfileInfo: function(guest_no, userData) {
		function success(response, textStatus, jqXHR) {
			console.log('saveProfileInfo', response);
		}
		
		var call = 'method=modify_guest&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&guest_no='+guest_no+'&first_name='+userData.first_name+'&last_name='+userData.last_name+'&e_mail='+userData.e_mail+'&address='+userData.address+'&zip='+userData.zip+'&area_code='+userData.cell_phone1+'&phone='+userData.cell_phone2+userData.cell_phone3;
		console.log('saveProfileInfo call', call)
	
		IFLY.callApi(call, success,null,null);			
		
	}
	
}

$(document).ready(function() {
	IFLY.mobileInit();
	//IFLY.init(); 
});
