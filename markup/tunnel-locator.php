<?php include("inc/head.php"); ?>
<body class="info-page tunnel-locator">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<h1 class="underline">Where to you want to fly?</h1>
				<h2 class="note">We have locations across the United States, Canada, the UK and Asia. </h2>

				<div class="sort">
					 <!--<a href="#" class="btn red" title="Finds the facility closest to your current location"><em></em><span>Closest to you</span></a>
					<a href="#" class="btn" title="Alphabetical"><em></em><span>Alphabetical</span></a>
                   -->
                    <select name="SORT TUNNELS" title="Sort the order of the locations" id="tunnel-order">
                        <option selected="selected">CLOSEST TO YOU</option>
                        <option>ALPHABETICAL</option>
                    </select>
                	
				</div>
				

				<section class=" clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<div class="buttons">
							<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
							<a href="#" class="btn"><em></em><span>More Info</span></a>
						</div>
						<form action="#" class="uniform tunnel-locator">
							<fieldset>
								<div class="row">
									<a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
									<input type="submit" value="Find Flights" class="find-flights">
								</div>
							</fieldset>
						</form>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>
				
				<section class=" clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<div class="buttons">
							<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
							<a href="#" class="btn"><em></em><span>More Info</span></a>
						</div>
						<form action="#" class="uniform tunnel-locator">
							<fieldset>
								<div class="row">
									<a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
									<input type="submit" value="Find Flights" class="find-flights">
								</div>
							</fieldset>
						</form>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>

								<section class=" clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<div class="buttons">
							<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
							<a href="#" class="btn"><em></em><span>More Info</span></a>
						</div>
						<form action="#" class="uniform tunnel-locator">
							<fieldset>
								<div class="row">
									<a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
									<input type="submit" value="Find Flights" class="find-flights">
								</div>
							</fieldset>
						</form>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>


				<section class=" clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<div class="buttons">
							<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
							<a href="#" class="btn"><em></em><span>More Info</span></a>
						</div>
						<form action="#" class="uniform tunnel-locator">
							<fieldset>
								<div class="row">
									<a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
									<input type="submit" value="Find Flights" class="find-flights">
								</div>
							</fieldset>
						</form>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>


				<section class="clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<div class="buttons">
							<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
							<a href="#" class="btn"><em></em><span>More Info</span></a>
						</div>
						<form action="#" class="uniform tunnel-locator">
							<fieldset>
								<div class="row">
									<a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
									<?php include ('inc/form-elements/select-flyer-type.php'); ?>
									<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
									<input type="submit" value="Find Flights" class="find-flights">
								</div>
							</fieldset>
						</form>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>

				<section class=" extra clearfix">

					<article>
						<h2>iFLY Hollywood</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<p><a href="#" title="More Info" class="more-info">MORE INFO</a></p>
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>

				<section class="last clearfix">

					<article>
						<h2>iFLY Extra</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						<p>Hours: M-F, 10 AM - 7 PM; Sat &amp; Sun, 10 AM - 10 PM</p>
						<p>Phone: 555-555-5555</p>
						<p>Location: 555 iFLY St., Holywood, CA 55555</p>
						<p><a href="#" title="More Info" class="more-info">MORE INFO</a></p>
						
					</article>
					
					<aside>
						<img src="images/temp-tunnel-locator.png" alt="Some Text">
					</aside>

				</section>
			

			</div>
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>