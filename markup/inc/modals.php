<div class="modal lp">
	<div id="large-party" class="overlay" >
		<h2>Information for large groups</h2>
		<p>This is some information informing customer that currently system can only handle up to 12 people.</p>
		<form id="lp" name="" method="get" class="validate">
			<fieldset>
				<div class="clearfix">
					<label for="name">Name</label>
					<input type="text" name="name" class="text required">
				</div>
				<div class="clearfix">
					<label for="email">Email</label>
					<input type="text" name="email" class="text required email">
				</div>
				<div class="clearfix">
					<label for="phone">Phone</label>
					<input type="text" name="phone" class="text required number">
				</div>
				<div class="clearfix">
					<label for="message">Message</label>
					<textarea class="text"></textarea>
				</div>
				<div class="clearfix">
					<input type="submit" value="submit" class="submit">
				</div>
			</fieldset>
		</form>
	</div>
</div>
	<div class="modal login">
    <div id="login" class="overlay" >
		<h1>Checkout as a Guest</h1>
        <div class="row"><a href="#" class="btn green"><em></em><span>CONTINUE</span></a></div>
        <div class="row"><h1>Login</h1>
        <div class="return-user">
		<form id="lgn" name="" method="get" class="validate">
			<fieldset>
				<div class="clearfix">
					<input type="text" name="username" class="text required username" value="USERNAME">
				</div>
				<div class="clearfix">
					<input type="text" name="password" class="text required password" value="PASSWORD">
				</div>
				<div class="clearfix">
					<a href="#" class="btn login"><em></em><span>LOGIN</span></a>
				</div>
			</fieldset>
		</form>
        </div>
        <div class="new-user">
           <div class="row"><a href="#" class="btn myifly"><em></em><span>CREATE A MYiFLY ACCOUNT</span></a></div>
           <div class="row"><a href="#" class="btn facebook"><em></em><span>SIGN IN WITH FACEBOOK<fb:login-button></fb:login-button></span></a></div>
           <div class="row"><a href="#" class="btn twitter"><em></em><span>SIGN IN WITH TWITTER</span></a></div>
           <div id="fb-root"></div>
        
        <script>
          window.fbAsyncInit = function() {
            FB.init({
              appId      : '409878702400471', // App ID
              channelUrl : 'www.iflyworld.com/channel.php', // Channel File
              status     : true, // check login status
              cookie     : true, // enable cookies to allow the server to access the session
              xfbml      : true  // parse XFBML
            });
        FB.Event.subscribe('auth.login', function(response) {
          window.location.reload();
        });
          };
          // Load the SDK Asynchronously
          (function(d){
             var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement('script'); js.id = id; js.async = true;
             js.src = "//connect.facebook.net/en_US/all.js";
             ref.parentNode.insertBefore(js, ref);
           }(document));
        </script>
		</div>

        </div>
        </div>
	</div>
</div>
</div>
	<div class="modal balance">
    <div id="gc-balance" class="overlay" >
		<h2>Get Gift Card Balance</h2>
		<form name="" method="get" class="validate">
			<fieldset>
				<div class="clearfix">
					<label for="name">Enter Gift Card #:</label>
					<input type="text" name="gc_num" class="text required">
				</div>
				<div class="clearfix">
					<input type="submit" value="submit" class="submit">
				</div>
			</fieldset>
		</form>
        <div class="status"></div>
	</div>
</div>
</div>

<div class="modal legal">
	<div class="overlay" >
		<h2>Legal Discalaimer</h2>
		<p>Lorem Ipsum</p>
		
	</div>
</div>

<div class="tooltip">
	<div id="tooltip" class="overlay event">
		<a class="closeButton" href="#">close</a>
		<h2></h2>
		<p></p>
	</div>
</div>