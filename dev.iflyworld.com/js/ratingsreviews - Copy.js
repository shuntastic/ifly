// Booking functions
IFLY.ratingsreviews = {
	total_reviews:0,
	total_pages:0,
	current_page:1,
	page_number:1,
	star_count:[],
	init: function() {
		
			IFLY.current_tunnel = (IFLY.current_tunnel == null) ? IFLY.locationHandler.getCurrentTunnel() :  IFLY.current_tunnel;
			
			IFLY.ratingsreviews.getSummary();
			if($('a.btn.more').length > 0) {
				$('a.btn.more').click(function(e) {
					IFLY.ratingsreviews.getMoreReviews();
					e.preventDefault();
				});
			}
			if($('select#sort-all').length > 0) {
				$('select#sort-all').change(function(){
						if($(this).val() != 'all') {
							IFLY.ratingsreviews.getReviewsByRating($(this).val());
						} else {
							IFLY.ratingsreviews.getReviews();
						}
					});
			}
			if($('select#sort-recent').length > 0) {
				$('select#sort-recent').change(function(){
						IFLY.ratingsreviews.getReviews();
					});
			}
			if($('.pagination a.next').length > 0) {
				$('.pagination a.next').click(function(e) {
					if (IFLY.ratingsreviews.current_page != IFLY.ratingsreviews.total_pages) {
						IFLY.ratingsreviews.current_page -=1;
						IFLY.ratingsreviews.getReviews();
					}
					e.preventDefault();
				});
			}
			if($('.pagination a.prev').length > 0) {
				$('.pagination a.prev').click(function(e) {
					if (IFLY.ratingsreviews.current_page != 1) {
						IFLY.ratingsreviews.current_page +=1;
						IFLY.ratingsreviews.getReviews();
					}
					e.preventDefault();
				});
			}
	},
	getSummary: function() {
			if($('#ratings-dist').length>0) {
					$('#reviews').html(IFLY.showLoader());
					var call='method=get_review_summary&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel;
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){

/*{"data":{"stars":{"star_count":461,"star_count0":"0","star_percent0":"0","star_count1":"1","star_percent1":"0.2","star_count2":"4","star_percent2":"0.9","star_count3":"7","star_percent3":"1.5","star_count4":"60","star_percent4":"12.9","star_count5":"389","star_percent5":"83.8"},"recommend":{"Yes":"471","total":"479","No":"8"}},"status":"OK","message":"get_review_summary data returned"}*/							
							var data = IFLY.ratingsreviews.rating_summary = response.data;
							console.log('rating summary data',response);
							IFLY.ratingsreviews.total_reviews = parseInt(data.recommend.total);
							IFLY.ratingsreviews.total_pages = Math.ceil(IFLY.ratingsreviews.current_page = IFLY.ratingsreviews.total_reviews/5);
	//						IFLY.ratingsreviews.total_pages = IFLY.ratingsreviews.current_page = data.pages;
						
							var avg = ((( parseInt(data.stars.star_count5) * 100 ) + (parseInt(data.stars.star_count4) * 80 ) + (parseInt(data.stars.star_count3) * 60 ) + (parseInt(data.stars.star_count2) * 40 ) + (parseInt(data.stars.star_count1) * 20 ) )  / (parseInt(data.stars.star_count5) + parseInt(data.stars.star_count4) + parseInt(data.stars.star_count3) + parseInt(data.stars.star_count2) + parseInt(data.stars.star_count1)) );
                        
							if (!isNaN(Math.round(avg))) $('span.average').html(Math.round(avg / 20 * 10) / 10 + " out of 5 stars");
							$('span.star-rating').addClass('star4');
							$('.progress-bar.prog1 em').animate({width:(parseFloat(data.stars.star_percent1))},1000);
							$('.progress-bar.prog2 em').animate({width:(parseFloat(data.stars.star_percent2))},1000);
							$('.progress-bar.prog3 em').animate({width:(parseFloat(data.stars.star_percent3))},1000);
							$('.progress-bar.prog4 em').animate({width:(parseFloat(data.stars.star_percent4))},1000);
							$('.progress-bar.prog5 em').animate({width:(parseFloat(data.stars.star_percent5))},1000);
							$('.progress-bar.prog1').append('&nbsp;'+data.stars.star_count1);
							$('.progress-bar.prog2').append('&nbsp;'+data.stars.star_count2);
							$('.progress-bar.prog3').append('&nbsp;'+data.stars.star_count3);
							$('.progress-bar.prog4').append('&nbsp;'+data.stars.star_count4);
							$('.progress-bar.prog5').append('&nbsp;'+data.stars.star_count5);
							IFLY.ratingsreviews.star_count.push(data.stars.star_count1)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count2)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count3)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count4)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count5)
							
							$('#ratings-dist p').html(data.recommend.Yes+' out of '+data.recommend.total+' would recommend iFLY to a friend!');
							IFLY.ratingsreviews.getReviews();
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
},
	getReviews: function() {
				if(IFLY.ratingsreviews.total_pages>0) {
					IFLY.ratingsreviews.updatePagination(IFLY.ratingsreviews.current_page);
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+IFLY.ratingsreviews.current_page+'&reviewsort=desc&starfilter=&search=';
						console.log('call',call);
				$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
//							console.log('response',response);
							var data = response.data;
							IFLY.ratingsreviews.displayReviews(data.data, false);

						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
	},
	getMoreReviews: function() {
			if(IFLY.ratingsreviews.current_page>0) {
				IFLY.ratingsreviews.current_page -= 1;
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+parseInt(IFLY.ratingsreviews.current_page)+'&reviewsort=desc&starfilter=&search=';
					console.log('call',call);
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
							var data = response.data;
							IFLY.ratingsreviews.displayReviews(data.data, true);
							IFLY.ratingsreviews.updatePagination(data.page,data.total,data.pages);
							
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
	},
	getReviewsByRating: function(rating) {
			if(IFLY.ratingsreviews.current_page>0) {
				 var pageOffset = Math.floor(parseInt(IFLY.ratingsreviews.star_count[parseInt(rating)-1])/5);
				 pageOffset = (pageOffset == 0)? 1 : pageOffset;
				 console.log('pageOffset',pageOffset);
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+pageOffset+'&starfilter='+rating+'&reviewsort=&search=';
					console.log('call',call);
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
//							console.log('response',response);
							var data = response.data;
							IFLY.ratingsreviews.displayReviews(data.data, false);
							IFLY.ratingsreviews.updatePagination(data.page,data.total,data.pages);
							
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
	},
	
	displayReviews: function(data,append) {
						var sortedData = ($('select#sort-recent').val() == 'asc') ? data : data.reverse();
	//					var sortedData = ($('select#sort-recent').val() == 'asc') ? data : data.reverse();
						var reviews ='';
						$.each(sortedData, function(index,value) {
							var review = '<div class="review clearfix"><div class="info"><span class="star-rating star'+this.stars+'">'+this.stars+'-star review</span><span>'+this.datesubmitted+'</span><em>by '+this.name+'</em><span>'+this.city+' '+this.state+'</span></div><div class="comment"><h2>'+this.reviewTitle+'</h2><p>'+this.reviewText+'</p></div></div>';
							reviews += review;
							
						});
						if(append) {
							$('#reviews').append(reviews);
						} else {
							$('#reviews').html(reviews);
						}
	},
	updatePagination: function(active) {
		//pages = parseInt(pages);
//		active = parseInt(active);
//		IFLY.ratingsreviews.current_page = pages - active;
//		IFLY.ratingsreviews.total_pages = parseInt(total)/5;
		$('.pages span.active').html(Math.ceil(IFLY.ratingsreviews.total_pages - parseInt(active)));
		$('.pages span.total').html(Math.floor(IFLY.ratingsreviews.total_pages));
					

	}
}

var urlParams = {};
var packageData, sessionStatus;
var api = '/api_curl.php';
var dataType = 'html json';

$(document).ready(function() {
	if($('body.ratings').length>0)
	IFLY.ratingsreviews.init();
	//getParams();
});
