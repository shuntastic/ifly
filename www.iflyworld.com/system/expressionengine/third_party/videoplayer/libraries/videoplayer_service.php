<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_service {

	var $api_key_title = "API KEY";
	var $api_secret_title = "API SECRET";
	var $service_key  = false;
	var $service_name 	= false;

	var $video = array(
		'url' 					=> false,
		'id' 					=> false,
		'service_key' 			=> false,
		'service_name' 			=> false,
		'embed' 				=> false,
		'width' 				=> 500,
		'height' 				=> 281,
		'thumbnail' 			=> false,
		'thumbnail_large' 			=> false,

		'title'		 			=> false,
		'description'			=> false,
		'username'	 			=> false,
		'author'	 			=> false,
		'date'		 			=> false,
		'plays'		 			=> false,
		'duration'	 			=> false,
		'duration_seconds'		=> false,
	);

	// --------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{
		$this->EE =& get_instance();

		// Video Player cache path

		$this->_cache_path = APPPATH."cache/videoplayer/";

		// load helper

		if (! class_exists('Videoplayer_helper'))
		{
			require_once PATH_THIRD.'videoplayer/helper.php';
		}

		$this->helper = new Videoplayer_helper;


		// create cache folder if it doesn't exist

		if(!file_exists($this->_cache_path))
		{
			@mkdir($this->_cache_path, 0777);
		}
	}
	
	// --------------------------------------------------------------------

	/**
	 * Get video embed
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function load_embed($video)
	{
		// video id
		
		$id = $video['id'];
	
		
		// specific processing for width & height because they are not needed to build the http query
		
		$width = "";
		$height = "";
		
		if(isset($video['width']))
		{		
			$width = $video['width'];	
		}
		
		if(isset($video['height']))
		{	
			$height = $video['height'];
		}
		
		
		// default options
		
		$opts = array();
		$opts['autoplay'] = false;
		
		
		// youtube specific defaults
		
		if($video['service_key'] == "youtube")
		{		
			$opts['rel'] = 0;
			$opts['autohide'] = true;
			$opts['controls'] = true;
			$opts['showinfo'] = false;	
		}
			
	
		// parameters used for building http query
		
		$parameters = array(
		
			// common
			
			'autoplay',
			'loop',
			
			
			// youtube
			
			'autohide',
			'rel',
			'related_videos',
			
			'youtube_autohide',
			'youtube_cc_load_policy',
			'youtube_color',
			'youtube_controls',
			'youtube_disablekb',
			'youtube_enablejsapi',
			'youtube_end',
			'youtube_fs',
			'youtube_iv_load_policy',
			'youtube_modestbranding',
			'youtube_playerapiid',
			'youtube_rel',
			'youtube_showinfo',
			'youtube_start',
			'youtube_theme',
			
			
			//vimeo
			
			'vimeo_color',
			'vimeo_portrait',
			'vimeo_title',
			'vimeo_byline'
		);
		
		
		// process parameters
		
		foreach($parameters as $parameter)
		{
			if(isset($video[$parameter]))
			{
				$opt_key = $parameter;
			
				if(strpos($opt_key, $video['service_key']) === 0)
				{
					$opt_key = substr($opt_key, strlen($video['service_key']) + 1);
				}
				
				switch($parameter)
				{
					// aliases for 1/0 - yes/no - true/false
					
					case "autoplay":
					case "loop":
					
					case "youtube_autohide":
					case "youtube_cc_load_policy":
					case "youtube_controls":
					case "youtube_disablekb":
					case "youtube_enablejsapi":
					case "youtube_fs":
					case "youtube_modestbranding":
					case "youtube_rel":
					case "youtube_related_videos":
					case "youtube_showinfo":
					
					case "vimeo_portrait":
					case "vimeo_title":
					case "vimeo_byline":
					
					
					if($video[$parameter] === "yes" || $video[$parameter] === true || $video[$parameter] === 1 || $video[$parameter] === "1")
					{
						$opts[$opt_key] = true;
					}
					elseif($video[$parameter] === "no" || $video[$parameter] === false || $video[$parameter] === 0 || $video[$parameter] === "0")
					{
						$opts[$opt_key] = false;
					}

					break;

					
					// default
					
					default:
					
					$opts[$opt_key] = $video[$parameter];
				}
			}
		}
		
		
		// related videos alias
		
		if(isset($opts['related_videos']))
		{
			$opts['rel'] = $opts['related_videos'];
		}


		// build embed
		
		$opts_query = http_build_query($opts);

		if(!preg_match('/\?/', $this->universal_url, $matches, PREG_OFFSET_CAPTURE))
		{
			$opts_query = '?'.$opts_query;
		}
		else
		{
			$opts_query = '&'.$opts_query;
		}

		$format = '<iframe src="'.$this->universal_url.$opts_query.'" width="%s" height="%s" frameborder="0" allowfullscreen="true" allowscriptaccess="true"></iframe>';
		
		$embed = sprintf($format, $id, $width, $height);
		
		$video['embed'] = $embed;


		return $video;
	}

	// --------------------------------------------------------------------

	/**
	 * Get content (with cache layer)
	 *
	 * @access	public
	 * @param	string
	 * @return	string
	 */
	function get_content($url, $expire = 600)
	{
		// api url hash (for the cache)

		$url_hash = md5($url);


		// create cache if it doesn't exist

		if(!$data = $this->cache_get($url_hash))
		{
			$data = @file_get_contents($url);
			
			if(strpos($http_response_header[0], "200") === false)
			{
				return false;
			}

			$this->cache_save($url_hash, $data, $expire);
		}

		if(!$data)
		{
			return false;
		}

		return $data;
	}

	// --------------------------------------------------------------------

	/**
	 * Post process video
	 *
	 * @access	public
	 * @param	array
	 * @return	array
	 */
	function post_process_video($video)
	{

		// shorten long titles

		if(strlen($video['title']) > 80)
		{
			$video['title'] = trim(substr($video['title'], 0, 80))."&hellip;";
		}

		return $video;
	}

	// --------------------------------------------------------------------

	/**
	 * Post process multiple videos
	 *
	 * @access	public
	 * @param	array
	 * @return	array
	 */
	function post_process_videos($videos)
	{
		if(is_array($videos))
		{
			foreach($videos as $k => $video)
			{
				$videos[$k] = $this->post_process_video($video);
			}
		}

		return $videos;
	}

	// --------------------------------------------------------------------

	/**
	 * Get cache file
	 *
	 * @access	private
	 * @param	string
	 * @return	string
	 */
	function cache_get($id)
	{
		$this->EE->load->helper('file');

		if ( ! file_exists($this->cache_path().$id))
		{
			return FALSE;
		}

		$data = read_file($this->cache_path().$id);

		$data = unserialize($data);

		if (time() >  $data['time'] + $data['ttl'])
		{
			unlink($this->cache_path().$id);
			return FALSE;
		}

		return $data['data'];
	}

	// --------------------------------------------------------------------

	/**
	 * Save cache file with time to live
	 *
	 * @access	private
	 * @param	string
	 * @param	string
	 * @param	integer
	 * @return	bool
	 */
	function cache_save($id, $data, $ttl = 60)
	{

		$this->EE->load->helper('file');

		$contents = array(
				'time'		=> time(),
				'ttl'		=> $ttl,
				'data'		=> $data
			);

		if (write_file($this->cache_path().$id, serialize($contents)))
		{
			@chmod($this->cache_path().$id, 0777);
			return TRUE;
		}

		return FALSE;

	}

	// --------------------------------------------------------------------

	/**
	 * Get cache path with service slug
	 *
	 * @access	private
	 * @return	string
	 */
	function cache_path(){
		
		$cache_path = APPPATH."cache/videoplayer/";

		if($this->service_key)
		{
			$cache_path .= $this->service_key."/";
			
			// create cache folder if it doesn't exist
	
			if(!file_exists($cache_path))
			{
				@mkdir($cache_path, 0777);
			}
		}
		
		return $cache_path;
	}
}

// END Videoplayer_service class

/* End of file Videoplayer_service.php */
/* Location: ./system/expressionengine/third_party/videoplayer/libraries/Videoplayer_service.php */