<?php include("inc/head.php"); ?>
<body>

	<div id="skyline"></div>

	
	<div id="parallax-wrapper">
		<div id="parallax">
			<a href="http://www.adobe.com/go/getflash">
            	<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
        	</a>
		</div>
	</div>
	<!---->

	<div id="wrapper">

		<?php include("inc/header.php"); ?>
		
		<div class="visual-block">
			<div class="visual-holder">
				<ul id="slider">
					<li>
						<article class="video-block">
							<h2>What Is It?</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
							<div class="video"><a href="#yP9GXL68w2w"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
						</article>
					</li>
					<li>
						<article class="video-block">
							<h2>First Time Flyers</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
							<div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
						</article>
					</li>
					<li>
						<article class="video-block">
							<h2>Who Can Fly</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
							<div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
						</article>
					</li>
					<li>
						<article class="video-block">
							<h2>Instructors</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
							<div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
						</article>
					</li>
					<li>
						<article class="video-block">
							<h2>I.B.A. Moves</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam neque turpis, sodales sit amet tempor at, vulputate et urna. Duis luctus, lectus a vehicula pharetra.</p>
							<div class="video"><a href="#"><img src="images/img02.jpg" width="174" height="104" alt="image description"><span class="play"><em></em><strong></strong></span></a></div>
						</article>
					</li>
				</ul><!-- /#slider -->

				<div id="video-modal">
					<div class="video-wrapper">
						<a href="#" class="btn red" title="close" >
						<em></em>
						<span>close</span>
						</a>
						<a class="btn-prev" href="#">&lt;</a>
						<a class="btn-next" href="#">&gt;</a>
						<div class="video-player">
							<div id="yt-player"></div>
						</div>
					</div><!-- /.video-wrapper -->
				</div><!-- /#video-modal -->

				<div class="menu-block">
					<nav class="menu">
						<ul>
							<li class="active"><a href="#1">What Is It?</a></li>
							<li><a href="#2">First Time Flyers</a></li>
							<li><a href="#3">Who Can Fly</a></li>
							<li><a href="#4">Instructors</a></li>
							<li><a href="#5">I.B.A. Moves</a></li>
						</ul>
					</nav>
				</div>
			</div>
		
		</div><!-- /.visual-block -->

	</div><!-- /#wrapper -->

	<div id="main" role="main">

		<div class="content-block box-section">

			<div class="box-holder">

				<article class="box">

					<a href="#" class="photo"><img src="images/img03.jpg" width="185" height="158" alt="image description"></a>
					<div class="description popup-block">
						<p>When do you want to fly? Find your flight packages and times.</p>
						<form action="#" class="tunnel-locator quickbook">
							<fieldset>
                                <div class="row">
                                <select name="TUNNEL LOCATION" title="Select your tunnel location" id="flyer-location" class="locations">
                                    <option>iFLY Hollywood</option>
                                    <option>Hollywood, CA</option>
                                    <option>Orlando, FL</option>
                                    <option>Seattle, WA</option>
                                    <option>Ogden, UT</option>
                                    <option>San Francisco, CA</option>
                                    <option>Find the closest location</option>
                                    <option selected="selected">FLIGHT LOCATION</option>
                                </select>
                                </div>
								<div class="row">
									<a href="#" class="btn date" id="book-date" title="What day for the reservation" ><em></em><span>FLIGHT DATE</span></a>
                                    <div id="calendar-quickbook"></div>
									<select class="flyers" title="The Number of flyers for the reservation" name="# OF FLYERS" id="how-many-fliers">
                                        <option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
										<option>6</option>
										<option selected="selected"># OF FLYERS</option>
									</select>
								</div>
								<div class="row">
									<select name="TYPE OF FLYER" title="The type of flyer making the reservation" id="flyer-type">
										<option>FIRST TIME FLYER</option>
										<option>RETURN FLYER</option>
										<option>EXPERIENCED FLYER</option>
										<option>GROUP</option>
										<option selected="selected">TYPE OF FLYER</option>
									</select>
								</div>
								<div class="row">
									<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>
									<a href="#" class="btn"><em></em><span>LEARN MORE</span></a>
								</div>
							</fieldset>
						</form>
					</div>
				</article><!-- /.box -->

				<article class="box alt">
					<a href="#" class="photo"><img src="images/img04.jpg" width="184" height="158" alt="image description"></a>
					<div class="description">
						<div class="header-block">
							<h1>Reviews</h1>
						</div>
						<blockquote>
							<q>Best birthday ever! We were actually flying! I can’t wait...</q>
							<a href="#" class="more">READ MORE</a>
							<cite>-John Smith, 7/19/12</cite>
						</blockquote>
						<div class="star-block">
							<a href="#" class="star"><img src="images/star.png" width="77" height="13" alt="image description"></a>
						</div>
						<a href="#" class="btn red"><em></em><span>MORE REVIEWS</span></a>
					</div>
				</article><!-- /.box -->

			</div><!-- /.box-holder -->

		</div><!-- /.box-section -->

		<div class="content-block metal-bg">

			<section class="block-section">
				<article class="block">
					<div class="block-holder">
						<img src="images/img05.jpg" width="313" height="186" alt="image description" class="image">
						<div class="text-block">
							<h2>Book now and get more fly time</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						</div>
						<a href="#" class="btn red"><em></em><span>1 day special offer</span></a>
					</div>
				</article>
				<article class="block">
					<div class="block-holder">
						<img src="images/img06.jpg" width="313" height="186" alt="image description" class="image">
						<div class="text-block">
							<h2>FIND US ON FACEBOOK</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						</div>
						<a href="#" class="btn red"><em></em><span>ifly on FACEBOOK</span></a>
					</div>
				</article>
				<article class="block">
					<div class="block-holder">
						<img src="images/img07.jpg" width="313" height="186" alt="image description" class="image">
						<div class="text-block">
							<h2>LOREM IPSUM DOLOR SIT AMIT.</h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque feugiat.</p>
						</div>
						<a href="#" class="btn red"><em></em><span>LEARN MORE</span></a>
					</div>
				</article>
			</section><!-- /.block-section -->

			<section class="gallery-block">
				<h1>OUR FAVORITE iFLY PHOTOS &amp; VIDEOS</h1>
				<div class="gallery">
					<div class="gholder">
						<a class="btn-prev" href="#">&lt;</a>
						<a class="btn-next" href="#">&gt;</a>
						<div class="gmask">
							<ul>
								<li><a href="images/img08.jpg"><img src="images/img08.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img09.jpg"><img src="images/img09.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img10.jpg"><img src="images/img10.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img11.jpg"><img src="images/img11.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img12.jpg"><img src="images/img12.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img08.jpg"><img src="images/img08.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img09.jpg"><img src="images/img09.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img10.jpg"><img src="images/img10.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img11.jpg"><img src="images/img11.jpg" width="174" height="105" alt="image description"></a></li>
								<li><a href="images/img12.jpg"><img src="images/img12.jpg" width="174" height="105" alt="image description"></a></li>
							</ul>
						</div>
					</div>
				</div>
			</section><!-- /.gallery-block -->

			<div class="btn-block">
				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>
			</div><!-- /.btn-block -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->


	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	
</body>
</html>