<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'videoplayer/config.php';

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_upd {

	var $version = VIDEOPLAYER_VERSION;

	/* constructor */

	function __construct()
	{
		// Make a local reference to the ExpressionEngine super object

		$this->EE =& get_instance();
	}

	// --------------------------------------------------------------------

	/**
	 * Module Installer
	 *
	 * @access	public
	 * @return	bool
	 */
	function install()
	{
		$this->EE->load->dbforge();


		// module infos

		$data = array(
			'module_name' => 'Videoplayer',
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);

		$this->EE->db->insert('modules', $data);


		// actions

		$data = array(
			'class'		=> 'Videoplayer' ,
			'method'	=> 'ajax'
		);

		$this->EE->db->insert('actions', $data);


		// custom tables

		$this->create_tables();

		return TRUE;

	}

	// --------------------------------------------------------------------

	/**
	 * Module Uninstaller
	 *
	 * @access	public
	 * @return	bool
	 */
	function uninstall()
	{
		$this->EE->load->dbforge();

		$this->EE->db->select('module_id');

		$query = $this->EE->db->get_where('modules', array('module_name' => 'Videoplayer'));

		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');


		// remove actions

		$this->EE->db->where('class', 'Videoplayer');
		$this->EE->db->delete('actions');


		// remove module

		$this->EE->db->where('module_name', 'Videoplayer');
		$this->EE->db->delete('modules');


		// drop videoplayer_accounts table

		$this->EE->dbforge->drop_table('videoplayer_accounts');

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Module Updater
	 *
	 * @access	public
	 * @return	bool
	 */
	function update($current='')
	{
		// in any case we force set has_cp_backend to y
		// some people trying to downgrade to 2.x from 3.x
		// and then re-upgrade to 3.x found themselves without any backend page
	
		$data = array(
			'module_name' => 'Videoplayer',
			'has_cp_backend' => 'y',
		);

		$this->EE->db->where('module_name', "Videoplayer");
		$this->EE->db->update('modules', $data);
	
	
		// is current
		
		if ($current == $this->version)
		{
			return FALSE;
		}

		
		
		if ($current < '2.4')
		{

			// Turn off backend

			$data = array(
				'module_name' => 'Videoplayer',
				'has_cp_backend' => 'n',
			);

			$this->EE->db->where('module_name', "Videoplayer");
			$this->EE->db->update('modules', $data);


			// Add "ajax" action

			$data = array(
				'class'		=> 'Videoplayer' ,
				'method'	=> 'ajax'
			);

			$this->EE->db->insert('actions', $data);

		}
		
		
		// Create table for 3.0

		if($current < '3.0' || $current == "")
		{
			$this->create_tables();
		}
		
		
		// Disable fieldtype global settings
		
		if($current < '3.0.1')
		{
			$data = array(
				'has_global_settings' => 'n',
			);

			$this->EE->db->where('name', "videoplayer");
			$this->EE->db->update('fieldtypes', $data);
		}

		return TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Create Video Player tables
	 *
	 * @access	private
	 * @return	void
	 */
	private function create_tables()
	{
		$this->EE->load->dbforge();
		
		// Video Player Table : Accounts

		$fields = array(
            'id' => array(
							'type' => 'INT',
							'constraint' => 5,
							'unsigned' => TRUE,
							'auto_increment' => TRUE
						),
            'site_id' => array(
							'type' => 'INT',
							'constraint' => 5,
							'unsigned' => TRUE
						),
			'order' => array(
							'type' => 'INT',
							'constraint' => 3,
							'unsigned' => TRUE,
						),
			'service' => array(
								'type' => 'varchar',
								'constraint' => 30,
								'null' => TRUE,
								'default' => NULL
							),
			'api_key' => array(
								'type' => 'varchar',
								'constraint' => 200,
								'null' => TRUE,
								'default' => NULL
							),
			'api_secret' => array(
								'type' => 'varchar',
								'constraint' => 200,
								'null' => TRUE,
								'default' => NULL
							),
			'enabled' => array(
								'type' => 'int',
								'constraint' => 1,
								'unsigned' => TRUE
							),
			'is_authenticated' => array(
								'type' => 'int',
								'constraint' => 1,
								'unsigned' => TRUE
							),
			'authsub_session_token' => array(
								'type' => 'varchar',
								'constraint' => 50,
								'null' => TRUE,
								'default' => NULL
							),
			'oauth_access_token' => array(
								'type' => 'varchar',
								'constraint' => 50,
								'null' => TRUE,
								'default' => NULL
							),
			'oauth_access_token_secret' => array(
								'type' => 'varchar',
								'constraint' => 50,
								'null' => TRUE,
								'default' => NULL
							)
		);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('id', TRUE);
		$this->EE->dbforge->create_table('videoplayer_accounts');
	}

}
/* END Class */

/* End of file upd.videoplayer.php */
/* Location: ./system/expressionengine/third_party/videoplayer/upd.videoplayer.php */