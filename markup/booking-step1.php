<?php include("inc/head.php"); ?>
<body class="info-page booking step1">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step1" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<div class="continue">
					<a href="#" class="btn gc-balance"><em></em><span>Check gift card balance</span></a>
				</div>

				<h1 class="underline">Flyer Guide</h1>
				<h2 class="note">Pick which type of flyer you are to get more info or to book a flight.</h2>

				<div class="clearfix">

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>first time flyer</h2>
							<p>Never flown at iFLY before…this is you. Choose from five different packages. Click on 'More info' to see all the first-time flyer packages, or click on 'Book Now' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="booking-step2.php?tunnel=ifo&flyer_type=1ST_TIME" class="btn green"><em></em><span>BOOK NOW</span></a>
						</div>
						
					</article>

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>Return Flyer</h2>
							<p>If you've flown with us once before, congratulations! You're a return flyer. Click on 'More info' to see all the return flyer packages, or click on 'Book Now' if you already know what you want.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="booking-step2.php?tunnel=ifo&flyer_type=1ST_TIME" class="btn green"><em></em><span>BOOK NOW</span></a>
						</div>
						
					</article>

				</div><!-- /.clearfix -->

				<div class="clearfix">

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>Experienced Flyer</h2>
							<p> You're 'experienced' if you’re an iFLY regular, a competitive skydiver, or a member of the military with skydiving training. Click 'More info' to see them all, or 'Book Now' of you know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="booking-step2.php?tunnel=ifo&flyer_type=1ST_TIME" class="btn green"><em></em><span>BOOK NOW</span></a>
						</div>
						
					</article>

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>Group</h2>
							<p>If you're looking to fly six or more people, click on 'More info' to see all group flyer packages, or click on 'Book Now' if you already know what you want. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="booking-step2.php?tunnel=ifo&flyer_type=1ST_TIME" class="btn green"><em></em><span>BOOK NOW</span></a>
						</div>
						
					</article>

				</div><!-- /.clearfix -->

				<div class="divider"></div>

				<div class="clearfix">

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>Buy a Gift Card</h2>
							<p>Can't decide? Buy a gift card. You can choose a specific dollar amount or choose a package. <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="gifting-step2.php" class="btn green"><em></em><span>SHOP NOW</span></a>
						</div>
						
					</article>

					<article class="flier-type">
						<aside>
							<img src="/images/temp-booking-step1.jpg" alt="alt">
						</aside>
						<div>
							<h2>Redeem a Gift Card</h2>
							<p>Someone gave you a gift card? Lucky you! Click 'Redeem Now' to book your flight, or 'Check gift card balance' to see how much is on your card.  <a href="/booking-step1-overview.php" title="#" class="learn-more">More Info</a></p>
							<a href="gifting-step1.php" class="btn green"><em></em><span>REDEEM NOW</span></a>
						</div>
						
					</article>

				</div><!-- /.clearfix -->	

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step1();
		});

	</script>
</body>
</html>