<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ep_playa
 *
 * ----------------------------------------------------------------------------------------------
 * @package	EE2 
 * @subpackage	ThirdParty
 * @author	Andrea Fiore / Malcolm Elsworth 
 * @link	http://electricputty.co.uk 
 * @copyright	Copyright (c) 2011 Electric Putty Ltd.
 *
 */

class Ep_playa {


	var $preview_entry_id;
	var $is_preview;
	var $settings = NULL;


	function Ep_playa($settings=array())
	{		
		$this->EE =& get_instance();
		$this->settings = $settings;

		// -------------------------------------------
		// Load the library file and instantiate logger
		// -------------------------------------------
		require_once PATH_THIRD . '/ep_better_workflow/libraries/ep_workflow_logger.php';
		$this->action_logger = new Ep_workflow_logger($this->settings['advanced']['log_events']);
	}



	function remove_playas_from_data($data)
	{
		// Load the third party model
		$this->EE->load->model('ep_third_party');
		
		// Get all the playa fields
		$playa_fields = $this->EE->ep_third_party->get_all_fields('playa');
		
		if ($playa_fields->num_rows() > 0)
		{
			foreach ($playa_fields->result_array() as $row)
			{
				$this_entry_id = $data['entry_id'];
				$this_field_id = $row['field_id'];
				$this_field_name = 'field_id_'.$row['field_id'];
				$row_order = 0;

				// If this field is defined in our data array
				if (isset($data[$this_field_name]))
				{
					// Grab the value of the matrix field
					$playa_data = $data[$this_field_name];
					
					// If this field is also defined in the revision_post array update $mtx_data
					if(isset($data['revision_post'][$this_field_name]))
					{
						$playa_data = $data['revision_post'][$this_field_name];
					}
					
					// Delete all existing 'data' records for this field (as in, not 'delete' records)
					$this->EE->ep_third_party->delete_draft_data(array('entry_id' => $this_entry_id, 'field_id' => $this_field_id, 'type'=>'playa'));
					
					// Do we have any new playa data
					if (is_array($playa_data))
					{
						// ignore everything but the selections
						$playa_data = array_merge(array_filter($playa_data['selections']));
					
						// Loop through the playa_data and insert the relationships
						foreach($playa_data as $rel)
						{
							$this->EE->ep_third_party->update_draft_data($this_entry_id, $this_field_id, 'playa', '', $row_order, '', $rel);
							$row_order++;
						}
					}
				}
			}
		}
	
		return $data;
	}



	function update_playa_data_from_cache($data)
	{
		// Load the third party model
		$this->EE->load->model('ep_third_party');
		
		// Get all the playa fields
		$playa_fields = $this->EE->ep_third_party->get_all_fields('playa');
		
		if ($playa_fields->num_rows() > 0)
		{
			foreach ($playa_fields->result_array() as $row)
			{
				$this_entry_id = $data['entry_id'];
				$this_field_id = $row['field_id'];
				$this_field_name = 'field_id_'.$row['field_id'];
				$row_order = 0;

				// If this field is defined in our data array
				if (isset($data[$this_field_name]))
				{					
					// If the data is an array, we're good
					if(isset( $this->EE->session->cache['playa']['selections'][$this_field_id] ))
					{
						unset($data[$this_field_name]);
						$data[$this_field_name]['selections'] = $this->EE->session->cache['playa']['selections'][$this_field_id];
						
						// If this field is also defined in the revision_post array, update this
						if(isset($data['revision_post'][$this_field_name]))
						{
							unset($data['revision_post'][$this_field_name]);
							$data['revision_post'][$this_field_name]['selections'] = $this->EE->session->cache['playa']['selections'][$this_field_id];
						}
					}
				}
			}
		}

		return $data;
	}



	// -----------------------------------------------------------------------------------------------------------
	// Playa hook calls
	// -----------------------------------------------------------------------------------------------------------

	/** 
	 * Implements Playa hook 'playa_data_query'
	 */
	function data_query($dir, $entry_id, $field_ids, $col_ids, $row_ids, $rels, $filter_ids)
	{
        // Logging
        $this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_data_query(): Call data hook within Playa");
		$this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_data_query(): Is this entry '{$entry_id}' our preview entry is our preview_id '{$this->preview_entry_id}'");
     
        // If we are previewing load the draft Playa data
        if($this->is_preview && $entry_id == $this->preview_entry_id)
        {
            // Logging
            $this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_data_query(): Yes, this is our preview entry so get the draft Playa data");

            return $this->_get_draft_data_for_preview($dir, $entry_id, $field_ids, $col_ids, $row_ids, $filter_ids);
        }
        else
        {
            // Logging
            $this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_data_query(): No, this is a normal request so just return the playa relationships unaltered");
             
            return $rels;
        }

	}


	/** 
	 * Implements new native 'playa_fetch_rels_query' hook
	 */
	function fetch_rels_query($playaObj, $sql, $args)
	{
		if(is_null($args)) {
			return $this->EE->db->query($sql);
		}
		// Logging
		$this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_fetch_rels_query(): Call data hook within Playa");

		// Make sure we're dealing with an array for the entry_ids arg
		if(!is_array($args['entry_ids'])) $args['entry_ids'] = array($args['entry_ids']); 

		// If we are previewing AND we have a single entry_id - load the draft Playa data
		if($this->is_preview && in_array($this->preview_entry_id, $args['entry_ids']) && count($args['entry_ids']) == 1)
		{
			// Logging
			$this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_fetch_rels_query(): Yes, this is our preview entry so get the draft Playa data");

			return $this->_get_draft_data_for_preview($args['dir'], $args['entry_ids'][0], $args['field_ids'], $args['col_ids'], $args['row_ids'], $args['filter_ids'], $args['and']);
		}
		else
		{
			// Logging
			$this->action_logger->add_to_log("ext.ep_better_workflow: on_playa_fetch_rels_query(): No, this is a normal request so just execute the Playa query and return the record set");

			return $this->EE->db->query($sql);
		}
	}



	/** 
	 * Private method to retreive the draft relationships data from BWF's exp_ep_entry_drafts_thirdparty table
	 */
	private function _get_draft_data_for_preview($dir, $entry_id, $field_ids, $col_ids, $row_ids, $filter_ids)
	{
		switch($dir)
		{
			case 'children':

			$sql = 'SELECT DISTINCT(rel.data) AS entry_id,
				rel.entry_id AS parent_entry_id,
				rel.data AS child_entry_id
				FROM exp_ep_entry_drafts_thirdparty rel';
			
			$where[] = 'rel.type = \'playa\'';
			$where[] = 'rel.entry_id = '.$entry_id;
			
			$sql_end = 'ORDER BY rel.row_order';
			break;

			case 'parents':

			$sql = 'SELECT DISTINCT(rel.entry_id) AS entry_id,
				rel.entry_id AS parent_entry_id,
				rel.data AS child_entry_id
				FROM exp_ep_entry_drafts_thirdparty rel';
			
			$where[] = 'rel.type = \'playa\'';
			$where[] = 'rel.data = '.$entry_id;
			break;

			case 'siblings';

			$sql = "SELECT DISTINCT(rel.data) AS entry_id,
				rel.entry_id AS parent_entry_id,
				rel.data AS child_entry_id
				FROM exp_ep_entry_drafts_thirdparty rel
				INNER JOIN exp_ep_entry_drafts_thirdparty parent ON parent.entry_id = rel.entry_id";
			break;
		}
		

		// filter by field?
		if ($field_ids)
		{
			$where[] = 'rel.field_id IN ('.implode(',', $field_ids).')';
		}

		// filter by column?
		if ($col_ids)
		{
			$where[] = 'rel.col_id IN ('.implode(',', $col_ids).')';
		}

		// filter by row?
		if ($row_ids)
		{
			$where[] = 'rel.row_id IN ('.implode(',', $row_ids).')';
		}

		// filter by entry ID?
		if(is_array($filter_ids))
		{
			foreach ($filter_ids as $col => $entry_ids)
			{
				if ($entry_ids)
				{
					$entry_ids = str_replace('|', ',', $entry_ids);

					if ($not = (strncmp($entry_ids, 'not ', 4) == 0))
					{
						$entry_ids = substr($entry_ids, 4);
					}

					if($col == 'parent')
					{
						$where[] = "rel.entry_id".($not ? ' NOT' : '').' IN ('.$entry_ids.')';
					}
					else
					{
						$where[] = "rel.data".($not ? ' NOT' : '').' IN ('.$entry_ids.')';
					}					
				}
			}
		}

		if (isset($where))
		{
			$sql .= ' WHERE '.implode(' AND ', $where);
		}

		if (isset($sql_end))
		{
			$sql .= ' '.$sql_end;
		}

		return $this->EE->db->query($sql);	
	}

}