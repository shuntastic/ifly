/* 
	Authors: 
        Alex Hobday (alex@e-digitalgroup.com)
        Shun Smith (shun@coloringbookstudio.com)
*/

IFLY = {
    init : function () {
		
    	var self = this;

    	self.hasFlash();
        self.initHome();
        IFLY.calendarWidget.initQuickBook();
    	
        if($('form.uniform').length > 0){
            self.initUniform();
        }      

        if($('#parallax').length > 0){
            IFLY.parallax.init();
        }

        if($('#calendar-widget').length > 0) {
            IFLY.calendarWidget.init();            
        }
         
        if($('#event-widget').length > 0) {
            IFLY.calendarWidget.initEvents();            
        }
         
        if($('#yt-player').length > 0){
            IFLY.youtube.getID("yt-player");            
        } 

        if($('form.validate').length > 0){
            $('form.validate').validate();
        } 

        if($('#uniform-how-many-fliers').length > 0){
            self.initNumFlyers();
        }   
		
		if($('#home-review').length>0){
			IFLY.getHomepageReview();
		}

        if($('a.tooltip').length > 0){
            self.initToolTip();
        }   

        if($('a.legal').length > 0){
            self.initLegal();
        }   
        if($('a.iframe').length > 0){
            $(".iframe").colorbox({iframe:true, width:"600px", height:"450px"});
        }          
		if($('#flyer-location').length > 0) {
			self.getClosestTunnel();
		}
    },
	
	getClosestTunnel: function() {
		console.log('getClosestTunnel');
		locationHandler.geoip(null,getLatLong,locationHandler.onError);
		function getLatLong(rs, ip) {
			if ((rs.latitude != 0) && (rs.longitude != 0)) {
			  var lat = new Number(rs.latitude).toFixed(2);
			  var lon = new Number(rs.longitude).toFixed(2);
				var tunnelOutput = locationHandler.sortTunnels({latitude:lat,longitude:lon},locations);	
				$('select[id$="flyer-location"] option:selected').removeAttr('selected');
				$('select[id$="flyer-location"] option[value="'+tunnelOutput[0].tunnel+'"]').attr("selected","selected");
				$.uniform.update();
				 //console.log('tunnelOutput:',tunnelOutput);		  
			}
		}
		
	},

	initTimePicker : function () {
		$("#time-picker").delegate("label", "click", function () {
		var c = $(this).attr('class');
			if ( c == undefined) {
				var e = $("#time-picker").find('label.your-time');
				var s = e.siblings('input');
				e.removeAttr('class');
				s.removeAttr('checked');
				$(this).addClass('your-time');
				$(this).siblings('input').attr('checked',true);
				if($('#time-picker input.book-now').length >0) {
					$('#time-picker input.book-now').removeClass('disabled');
				}
				
			} 
		});
	},
    initHome : function () {

    	var self = this;

    	self.initCarousel();
    	self.initSlider();

        $(".youtube").colorbox({iframe:true, innerWidth:425, innerHeight:344});

        $(".gmask").delegate("a", "click", function (e) {
            e.preventDefault();
            $(this).colorbox();           
        });
    	
    },

    initCarousel : function () {

    	$(".gmask").jCarouselLite({
        	btnNext : ".gholder .btn-next",
        	btnPrev : ".gholder .btn-prev",
        	visible : 5,
        	scroll  : 5
    	});	

    },

    initSlider : function () {
        var ytID;
        var current_slide;
        var video_modal;
        var self = this;

    	$('#slider').anythingSlider({
    		theme 			: "default",
  			mode   			: "horiz", 
  			buildArrows		: true,
  			buildNavigation	: false,
            enableArrows    : true,
            appendForwardTo : $(".video-wrapper"),
            appendBackTo    : $(".video-wrapper"),
            buildStartStop  : false,
            hashTags        : false,

            //amimation
            autoPlay            : false,
            pauseOnHover        : true,
            stopAtEnd           : false,
            delay               : 3000,
            resumeDelay         : 15000,
            animationTime       : 600, 
            delayBeforeAnimate  : 0,

  			addWmodeToObject: 'transparent'
    	});

    	$('.menu').delegate("a", "click", function(e) {
    		var slide = $(this).attr('href').substring(1);
    		$('#slider').anythingSlider(slide);
    		e.preventDefault();
    		var parent = $(this).parent();
    		parent.siblings().removeClass("active");
    		parent.addClass('active');
			IFLY.parallax.lock();

            if ( $("#video-modal").is(":visible") ) {
                self.current_slide.toggle();
                self.video_modal.toggle();
                IFLY.youtube.stop("yt-player");
            }
            
  		});

        $("#slider").delegate("div.video a", "click", function(e) {
            e.preventDefault();
            
            self.current_slide = $(this).parents(".video-block");

            self.current_slide.toggle();

            self.video_modal = $("div#video-modal");
            self.video_modal.toggle();
			
			IFLY.parallax.pause();
            self.ytID   = $(this).attr("href").replace('#','');
            IFLY.youtube.init("yt-player", self.ytID);

        });

        $("#video-modal").delegate("a.btn, span.arrow a", "click", function(e) {
            e.preventDefault();
			//IFLY.parallax.trackMouse(true);            
            self.current_slide.toggle();
            self.video_modal.toggle();
            IFLY.youtube.stop("yt-player");
			IFLY.parallax.hideOverlay();
        });

        $("#video-modal").delegate("span.arrow.back a", "click", function(e) {
            var a = $(".menu li.active")
            var i = $('.menu li').index(a);
            var l = $('.menu li').length;
            
            a.removeClass('active');
            
            if(a == 0) {
                $(".menu li:last-child").addClass('active');
            }
            else {
                $(".menu li").eq(--i).addClass('active');   
            }
        });

        $("#video-modal").delegate("span.arrow.forward a", "click", function(e) {

            var a = $(".menu li.active")
            var i = $('.menu li').index(a);
            var l = $('.menu li').length;

            a.removeClass('active');
        
            if( a == (--l) ) {
                $(".menu li:first-child").addClass('active');
            }
            else {
                $(".menu li").eq(++i).addClass('active');   
            }
        });

    },

    initVideoPlayer : function () {
        $("#slider").delegate("div.video a", "click", function(e) {
            e.preventDefault();
            
            self.current_slide = $(this).parents(".video-block");

            self.current_slide.toggle();

            self.video_modal = $("div#video-modal");
            self.video_modal.toggle();
            
            IFLY.parallax.pause();
            self.ytID   = $(this).attr("href").replace('#','');
            IFLY.youtube.init("yt-player", self.ytID);

        });
    },

    hasFlash : function (){

		if(swfobject.hasFlashPlayerVersion("1")) {
			$('html').addClass('hasflash');
		}else{
			$('html').addClass('noflash');
		}

	},

	initUniform : function () {
		$("select, input:radio, input:file, input:checkbox").uniform();
	},

    initNumFlyers : function () {
        $("#uniform-how-many-fliers").on("change", function () {
            if( $('#uniform-how-many-fliers option:selected').val() == "13" ) {
                $.colorbox({ html : $(".modal.lp").html() });
            } 
        });
    },
	getTunnelLocation: function() {
			return 'ifo';
			//urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
	},
	
	loggedIn: false,
    initLogin : function () {
		
		if($('input.checkout').length > 0 && !loggedIn) {
			$('input.checkout').click(function() {
                $.colorbox({
					html : $(".modal.login").html(),
					onComplete: function() {
						//$('#lgn
						
					}
				});
				e.preventDefault();
			});
			
		}
		
        $("#uniform-how-many-fliers").on("change", function () {
            if( $('#uniform-how-many-fliers option:selected').val() == "13" ) {
                $.colorbox({ html : $(".modal.lp").html() });
            } 
        });
    },

    initLegal : function () {

        $("a.legal").on("click", function () {
            $.colorbox({ html : $(".modal.legal").html() });
        });
    },

    initToolTip : function () {

        $("#main").delegate(".tooltip", "click", function (e) {
            e.preventDefault();
            var title   = $(this).data('title');
            var content = $(this).data('content');
            var left    = $(this).offset().left;
            var top     = $(this).offset().top;
            
            $("#tooltip.overlay.event").css({
                            "left"      : left,
                            "top"       : top,
                            "display"   : 'block'
            })
            $('#tooltip h2').html(title);
            $('#tooltip p').html(content);
            $('#tooltip').fadeIn(300, function(){               
                $('#tooltip a.closeButton').click(function(e){
                    e.preventDefault();
                    $('#tooltip').fadeOut(100);
                }); 
            });
        });

        
    }



};

IFLY.parallax = {
    init: function() {
		if($('html').hasClass('hasflash')){
			$('#skyline').css({background:'#020a3d',display:'none'});
			var flashvars = {};
			var params = {wmode: "transparent",quality:"high",allowscriptaccess:"always",allownetworking:"all"};
			var attributes = {swliveconnect:"true"};
			swfobject.embedSWF("swf/parallax.swf", "parallax", "1370", "550", "10.0.0","expressInstall.swf", flashvars, params, attributes,IFLY.parallax.trackMouse);
		}
    },
	hideOverlay: function() {
         $("#skyline").css({display:'none'}).css({opacity : 0});    
	},
    trackMouse: function(pass) {
        if (pass){
            $("#skyline").css({display:'none'}).css({opacity : 0});    
        }
        
        $('#wrapper').mousemove(function(evt) {
            var offset = $(this).offset();
            var xPos = evt.pageX - offset.left;
            var paraObj = swfobject.getObjectById("parallax");
            paraObj.onHMove(xPos);
        });
    },
	lock: function() {
		$('#wrapper').unbind('mousemove');
		var paraObj = swfobject.getObjectById("parallax");
		paraObj.onHMove(1145);
	},
	pause: function() {
		$("#skyline").css({display:'block'}).animate({opacity :.8},200);    
		$('#wrapper').unbind('mousemove');
	}
};

IFLY.youtube = {

    init : function (id, ytid) {
        $("#"+id).tubeplayer({
            width   : 600, // the width of the player
            height  : 350, // the height of the player
            iframed : true,
            allowFullScreen : "true", // true by default, allow user to go full screen
            initialVideo    : ytid, // the video that is loaded into the player
            preferredQuality: "default",// preferred quality: default, small, medium, large, hd720
            onPlay: function(id){}, // after the play method is called
            onPause: function(){}, // after the pause method is called
            onStop: function(){}, // after the player is stopped
            onSeek: function(time){}, // after the video has been seeked to a defined point
            onMute: function(){}, // after the player is muted
            onUnMute: function(){} // after the player is unmuted
        });       
    },

    stop : function (id) {
        $("#"+id).tubeplayer("stop");
    },

    destroy : function () {
        $("#"+id).tubeplayer("destroy");
    },

    getID : function (elem) {
        var data = $("#"+elem).data("ytid");
        if ( data != undefined ) {
            IFLY.youtube.init(elem, $("#"+elem).data("ytid"));
        }
    },

};
IFLY.calendarWidget = {
    
    init: function() {
        $('#calendar-widget').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
            onSelect: this.callDaySelect,
            beforeShowDay:this.beforeShowDay
        });
    },               
    initEvents: function() {
        $('#event-widget').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
            onSelect: this.callEventsSelect,
            beforeShowDay:this.beforeEventsDay
        });
    },               
    initQuickBook: function() {
		if($('.box-holder #calendar-quickbook').length > 0)
		$('#calendar-quickbook').datepicker({
			dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
			buttonImageOnly:true,
			onSelect: function(e) {
				//console.log('e: '+e);
				$('#book-date span').html(e)
				$('#calendar-quickbook').css('display','none');	
			}
		});
		$('#book-date').click(function(e) {
                e.preventDefault(); 
				$('#calendar-quickbook').css('display','block');
				
//				$('#wrapper, #main').mousedown(function(e){
//					//if(e.target.id != 'calendar-quickbook' || e.target.id != 'book-date'){
//						$('#calendar-quickbook').fadeOut(100/*, function() {}*/);
//						$('#wrapper, #main').unbind('mousedown');
//					//}
//				}); 
				//beforeShowDay:calendarWidget.beforeShowDay
		});
    },               

    callDaySelect: function(dateText) {
        var date,
            selectedDate = new Date(dateText),
            i = 0,
            event = null;
    
        /* Determine if the user clicked an event: */
        while (i < IFLY.calendarWidget.calData.length && !event) {
            date = IFLY.calendarWidget.calData[i].date;
    
            if (selectedDate.valueOf() === date.valueOf()) {
                event = IFLY.calendarWidget.calData[i];
            }
            i++;
        }
        if (event) {
            var eventContent = '<a class="closeButton" href="#">close</a><h2>'+IFLY.calendarWidget.fullDate(event.date)+'<br />'+event.title+'</h2>'+IFLY.calendarWidget.truncate(event.description,50,'...<p><a href="'+event.url+'">MORE INFO</a></p>');
            $('div.event-overlay').html(eventContent).focus();
                $('div.event-overlay').fadeIn(300, function(){               
                    $('div.event-overlay a.closeButton').click(function(e){
                        e.preventDefault();
                        $('div.event-overlay').fadeOut(100, function() {$(this).html('')});
                    }); 
            });
    
    
        }
    },
    callEventsSelect: function(dateText) {
        var date,
            selectedDate = new Date(dateText),
            i = 0,
            event = null;
    
        /* Determine if the user clicked an event: */
        while (i < calData.length && !event) {
            date = this.calData[i].date;
    
            if (selectedDate.valueOf() === date.valueOf()) {
                event = IFLY.calendarWidget.calData[i];
            }
            i++;
        }
        if (event) {
			
//            var eventContent = '<a class="closeButton" href="#">close</a><h2>'+IFLY.calendarWidget.fullDate(event.date)+'<br />'+event.title+'</h2>'+IFLY.calendarWidget.truncate(event.description,50,'...<p><a href="'+event.url+'">MORE INFO</a></p>');
//            $('div.event-overlay').html(eventContent).focus();
//                $('div.event-overlay').fadeIn(300, function(){               
//                    $('div.event-overlay a.closeButton').click(function(){
//                        $('div.event-overlay').fadeOut(100, function() {$(this).html('')});
//                    }); 
//            });
        }
    },
    beforeEventsDay: function(date) {
        var result = [true, '', null];
        var matching = $.grep(IFLY.calendarWidget.calData, function(event) {
            return event.date.valueOf() === date.valueOf();
        });
    
        if (matching.length) {
            result = [true, 'highlight', null];
        }

        return result;
    },  
    beforeShowDay: function(date) {
        var result = [true, '', null];
        var matching = $.grep(IFLY.calendarWidget.calData, function(event) {
            return event.date.valueOf() === date.valueOf();
        });
    
        if (matching.length) {
            result = [true, 'highlight', null];
        }

        return result;
    },  

    fullDate: function(dateObj) {
        calendar = dateObj;
        day = calendar.getDay();
        month = calendar.getMonth();
        date = calendar.getDate();
        year = calendar.getYear();
        if (year < 1000)
        year+=1900
        cent = parseInt(year/100);
        g = year % 19;
        k = parseInt((cent - 17)/25);
        i = (cent - parseInt(cent/4) - parseInt((cent - k)/3) + 19*g + 15) % 30;
        i = i - parseInt(i/28)*(1 - parseInt(i/28)*parseInt(29/(i+1))*parseInt((21-g)/11));
        j = (year + parseInt(year/4) + i + 2 - cent + parseInt(cent/4)) % 7;
        l = i - j;
        emonth = 3 + parseInt((l + 40)/44);
        edate = l + 28 - 31*parseInt((emonth/4));
        emonth--;
        var dayname = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
        var monthname = new Array ("January","February","March","April","May","June","July","August","September","October","November","December" );
//      var fullD = dayname[day] + ", "+monthname[month] + " ";
        var fullD = monthname[month] + " ";
        if (date< 10)
            fullD+="0" + date + ", "
        else fullD+=date + ", ";
        fullD+=year
        
        return fullD;
    },

    truncate : function (text, limit, append) {
        if (typeof text !== 'string')
            return '';
        if (typeof append == 'undefined')
            append = '...';
        var parts = text.split(' ');
        if (parts.length > limit) {
            // loop backward through the string
            for (var i = parts.length - 1; i > -1; --i) {
                // if i is over limit, drop this word from the array
                if (i+1 > limit) {
                    parts.length = i;
                }
            }
            // add the truncate append text
            parts.push(append);
        }
        // join the array back into a string
        return parts.join(' ');
    }

};
IFLY.getHomepageReview = function() {
			var tunnel = IFLY.getTunnelLocation();
			var today = new Date();
			var call='method=get_review_top&controller=testimonials&format=json&tunnel='+tunnel;
			$.ajax({
				url: 'api_curl.php',
				type: 'post',
				dataType: 'json',
				data: call,
				success: function(response, textStatus, jqXHR){
					var data = response.data;
					var reviewDate = new Date(data.datesubmitted);
					$('#home-review').html('<q>'+(data.reviewText).trunc(56,true)+'</q>\
											<a href="/what-is-ifly/ratings-and-reviews" class="more">READ MORE</a>\
											<cite>-'+data.name+', '+reviewDate.mmddyyyy()+'</cite>');
					$('div.rating').html('<span class="star-rating star5">5 stars</span>');
				},
				error: handler.onError,
				complete: handler.onComplete
			});
	}	
var locations = [{name:'iFly Hollywood',lat:34.092809, long:-118.328661, tunnel:'hwd'},{name:'iFly Orlando',lat:28.538336, long:-81.379236, tunnel:'ifo'},{name:'iFly Seattle',lat:47.60621, long:-122.332071, tunnel:'seat'},{name:'iFly San Francisco',lat:37.77493, long:-122.419416, tunnel:'sfb'},{name:'iFly Austin',lat:30.267153, long:-97.743061, tunnel:'aus'}];

$(document).ready(function() {
	IFLY.init(); 
});
