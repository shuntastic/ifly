<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step6">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step6" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<div class="checkout-wrapper">
	
					<p class="lock">Secure Credit Card Payment</p>
					<p class="secure">This is a secure 128 bit SSL encrypted payment.</p>

					<form class="uniform">
						<fieldset>
							<div>
								<label>First Name</label>
								<input name="first_name" type="text" class="text">
							</div>
							<div>
								<label>Last Name</label>
								<input name="last_name" type="text" class="text">
							</div>
						</fieldset>

						<fieldset>
							<div>
								<label>Email Address</label>
								<input name="e_mail" type="text" class="text">
							</div>
							<div>
								<label>Billing Address</label>
								<input name="address" type="text" class="text">
							</div>
							<div>
								<label>Zip Code</label>
								<input name="zip" type="text" class="text zip">
							</div>
							<div>
								<label>Cell Phone Number</label>
								<input name="cell_phone" type="text" class="text phone3">
								<input name="cell_phone" type="text" class="text phone3">
								<input name="cell_phone" type="text" class="text phone4">
							</div>
							
						</fieldset>

						<fieldset class="payment-method">
							<label>Method of Payment</label>
							<div>
								<input name="paytype" type="radio" value="cc" checked>
								<label>Credit Card</label>
							</div>
							<div>
								<input name="paytype" type="radio" value="multiple">
								<label>Multiple forms of payment</label>
							</div>
							<div>
								<input name="paytype" type="radio" value="gc">
								<label>Gift Card</label>
							</div>
							<div>
								<input name="paytype" type="radio" value="cc-onfile">
								<label>Use my card on file</label>
							</div>
							<div>
								<input name="paytype" type="radio" value="ifly-account">
								<label>Use my iFly account</label>
							</div>
						</fieldset>

                    <div class="cc pay-option">
                        <fieldset class="cc-info">
                            <div>
                                <input name="credit_card_no" type="text" class="text" value="Credit Card #" size="23">
                                <label class="cc ir">Credit card</label>
                            </div>
                            <div class="expiry">
                                <select name="exp_month" id="month">
                                    <option>MONTH</option>
                                </select>
                            </div>
                            <div class="expiry">
                                <select name="exp_year" id="year">
                                    <option>YEAR</option>
                                </select>
                            </div>
                            <div>
                                <input type="text" name="cvv2" value="CCV2#" class="text phone3">
                                <input type="text" class="text phone3">
                                <label class="ccv ir">CCV</label>	
                            </div>
                            
                        </fieldset>
                        <fieldset class="save-cc">
                            <div>
                                <input type="radio" name="savecc">
                                <label for="savecc">Save my credit card information</label>
                            </div>	
                        </fieldset>
                        <div><input type="submit" value="PROCESS SALE" class="process"></div>
                  </div><!-- cc -->
                <div class="multiple pay-option disabled">
                    <fieldset class="mc-gcard">
                        <div><input type="checkbox" name="gift_cert" title="gift_cert"><label for="gift_cert">This payment is a gift certificate or gift card.</label></div>
                    </fieldset>
                    <fieldset class="cc-info">
                        <div>
                            <input type="text" name="credit_card_no" class="text" value="Credit Card #" size="22">
                            <label class="cc ir">Credit card</label>
                        </div>
                        <div class="expiry">
                            <select name="exp_month" id="month">
                                <option>MONTH</option>
                            </select>
                        </div>
                        <div class="expiry">
                            <select name="exp_year" id="year">
                                <option>YEAR</option>
                            </select>
                        </div>
                        <div>
                            <input type="text" name="ccv2" value="CCV2#" class="text" size="8">
                         </div>
                        <div>
                            <input type="text" value="$ Amount" name="$ Amount" class="text" size="10">
                            <input type="button" value="APPLY" class="apply">
                        </div>
                    </fieldset>
                    <fieldset class="gc-info">
                        <div>
                            <input type="text" class="text" value="16-digit gift card number" size="25">
                        </div>
                        <div>
                            <input type="text" value="$ Amount" name="$ Amount" class="text" size="10">
                            <input type="button" value="APPLY" class="apply">
                        </div>
                    </fieldset>
                    <div class="payment-summary">
                        <h3>Payment Summary</h3> 
                        <div class="tally">2
                           <div class="payment-title"><h3>&nbsp;</h3><h3>Card Type</h3><h3>Card Number</h3><h3>Payment Amount</h3></div>
                           <!--<div class="line-item"><div><input type="button" value="REMOVE" class="remove"></div><div class="card-type">Credit Card</div><div class="card-num">************5454</div><div class="card-total">$100.00</div></div>
                           <div class="line-item"><div><input type="button" value="REMOVE" class="remove"></div><div class="card-type">Gift Card</div><div class="card-num">************1234</div><div class="card-total line">$200.00</div></div>-->
                           <div class="pay-total">
                                <div class="total-text"><h3>Sale Total:</h3></div><div class="total-sale">&nbsp;</div><br />
                                <div class="total-text"><h3>Payments:</h3></div><div class="total-pay">&nbsp;</div><br />
                                <div class="total-text"><h3>Remaining Balance:</h3></div><div class="total-remain">&nbsp;</div>
                           </div>
                       </div>
                      <div><input type="submit" value="PROCESS SALE" class="process"></div>
                      </div>
                    </div> <!-- multiple -->
                    
                <div class="gc pay-option disabled">
                    <fieldset class="gc-info">
                        <div>
                            <input type="text" class="text" value="16-digit gift card number" size="25">
                        </div>
                    </fieldset>
                      <div><input type="submit" value="PROCESS SALE" class="process"></div>
                </div> <!-- gc -->
                
                <div class="cc-onfile pay-option disabled">
                    <fieldset class="mc-gcard">
                        <div><input type="checkbox" name="gift_cert" title="gift_cert"><label for="gift_cert">This payment is a gift certificate or gift card.</label></div>
                    </fieldset>
                    <fieldset class="cc-info">
                        <div>
                            <input type="text" name="credit_card_no" class="text" value="Credit Card #" size="22">
                            <label class="cc ir">Credit card</label>
                        </div>
                        <div class="expiry">
                            <select name="exp_month" id="month">
                                <option>MONTH</option>
                            </select>
                        </div>
                        <div class="expiry">
                            <select name="exp_year" id="year">
                                <option>YEAR</option>
                            </select>
                        </div>
                        <div>
                            <input type="text" name="ccv2" value="CCV2#" class="text" size="8">
                         </div>
                        <div>
                            <input type="text" value="$ Amount" name="$ Amount" class="text" size="10">
                            <input type="button" value="APPLY" class="apply">
                        </div>
                    </fieldset>
                    <fieldset class="gc-info disabled">
                        <div>
                            <input type="text" class="text" value="16-digit gift card number" size="25">
                        </div>
                    </fieldset>
                      <div><input type="submit" value="PROCESS SALE" class="process"></div>
                </div> <!-- cc-onfile -->
                
                <div class="ifly-account pay-option disabled">
                      <div><input type="submit" value="PROCESS SALE" class="process"></div>
                </div> <!-- ifly-account -->
                </div>
                </div>
                     </form>
				</div><!-- /.checkout-wrapper -->

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step6();
		});
	</script>
    
	
</body>
</html>