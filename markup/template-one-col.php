<?php include("inc/head.php"); ?>
<body class="info-page one-col">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<aside>
						<div class="video-wrapper">
							<a class="btn-prev" href="#">&lt;</a>
							<a class="btn-next" href="#">&gt;</a>
							<div class="video-player">
								<div id="yt-player" data-ytid="yP9GXL68w2w"></div>
							</div>
						</div><!-- /.video-wrapper -->
					</aside>

					<article>
						<h1>How does it work</h1>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras eu libero non tortor euismod interdum. Maecenas vestibulum orci tincidunt purus placerat eget porta nulla posuere. Nullam molestie elementum dui eu volutpat. Integer tempus ultricies iaculis. Aliquam vestibulum egestas ultrices. Nullam iaculis, tortor in rutrum sagittis, justo neque lobortis velit, ut sagittis neque magna eu ligula. Nulla a ornare libero. Donec accumsan elit nec mauris tincidunt ac aliquam magna viverra. Donec non ipsum ut augue cursus scelerisque ac vel nisl. Integer eleifend odio suscipit massa mattis placerat. Cras nisi purus, egestas ac bibendum id, condimentum rutrum mi. In in quam urna, at sodales urna. Proin nec ligula non sapien ultricies vehicula. Nullam auctor varius urna non lobortis. Nunc eu nisl vitae dui pretium interdum. Curabitur a neque a odio vehicula venenatis.</p>
						<p>Nulla mattis congue dolor, eu mattis nisl eleifend non. Phasellus volutpat cursus felis at vulputate. Duis orci libero, interdum eget varius fermentum, fermentum non libero. Proin sit amet est ipsum, vel dictum purus. Curabitur gravida commodo vulputate. Pellentesque vel velit vel magna ultricies faucibus ut quis lectus. Suspendisse in nunc turpis. Etiam tristique rutrum vestibulum. Etiam erat odio, porta in placerat nec, faucibus vitae lectus.</p>
					</article>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>