<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step2">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step2" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next disabled" href="#">&gt;</a>
				<div id="step-holder">
                	<div class="single-step">
                        <h1 class='underline'>BOOKING DETAILS</h1>
                        <h2 class="note">Choose when you want to fly, what type of flyer you are, <br />and how many of you are going.<br />
                        Click ‘Next’ to see our personal recommendations for you.</h2>
        
                        <div id="booking-details">
                            <div id="calendar-booking"></div>
                            <form action="#" class="uniform tunnel-locator">
                                <fieldset>					
                                    <div class="row">
                                        <a href="#" class="btn date"><em></em><span>FLIGHT DATE</span></a>
                                        <?php include ('inc/form-elements/select-flyer-type.php'); ?>
                                        <?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
                                    </div>
                                </fieldset>
                            </form>
        
                            <article class="tunnel-info">
                                <aside>
                                    <img src="/images/temp-booking-step2.jpg" alt="alt">
                                </aside>
                                <div>
                                    <h2>iFly Hollywood</h2>
                                    <ul>
                                        <li>100 Universal City Plz</li>
                                        <li>Universal City, CA 91608</li>
                                        <li>(818) 985-4359</li>
                                    </ul>
                                    <div class="row">
                                        <?php //include ('inc/form-elements/select-tunnel-locations.php'); ?>
                                        <select class="" name="tunnel" id="change-location">
                                            <option>FIND A DIFFERENT LOCATION</option>
                                            <option value="ifo">iFly Orlando</option>
                                            <option value="hwd">iFly Hollywood</option>
                                            <option value="seat">iFly Seattle</option>
                                            <option value="sfb">iFly SF Bay</option>
                                            <option value="aus">iFly Austin</option>
                                        </select>
                                    </div>
                                    <div class="row">
                                        <a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
                                    </div>
                                </div>
                                <div class="next">
                                    <input value="NEXT" type="button" class="next disabled"></input>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step2();
		});

	</script>
</body>
</html>