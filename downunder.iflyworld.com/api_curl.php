<?php
// dynamic var's for each element in post array
foreach($_POST as $k => $v) {
	$$k = $v;
}

// prepare data array for desired method
$data = $_POST;
$data['ip'] = $_SERVER['REMOTE_ADDR'];
// set apikey
//$data['apikey'] = "c5b3222c12abdd10d374bd75c6273b84";
$data['apikey'] = "c6af7b9ef5c13869332319b7d9087fe5";

// build cURL's URL
$url = "http://api.iflyworld.com/index.php/$controller/$method/format/$format";

// initialize cURL
$ch = curl_init();

// set cURL options
curl_setopt($ch, CURLOPT_POST, TRUE);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
curl_setopt($ch, CURLOPT_HEADER, FALSE);

// execute cURL
$result = curl_exec($ch);

// validate if it is a json string
$data = @json_decode($result); // supress warning using @ operator
if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
	$json_error['status'] = "JSON_ERR";
	$json_error['json'] = $result;
	$result = json_encode($json_error);
}

// process result
// $result = json_decode($result, TRUE);

echo $result;

?>