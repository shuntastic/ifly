<?php

if (!isset($_SESSION)) session_start();
 //session_destroy(); die;
 //print_r($_SESSION); die;

ini_set('error_log', '/tmp/testimonials.log');



//error_log("*****-------------- videos-by-tags ------*****\n");
//error_log('POST:');
//error_log(print_r($_POST,true));

$tunnel = "aus";
$page = 1;
$playlist = 0;
$resultsPerPage = 25;
$debug = 0;
$month = "";

if (isset($_POST['tunnel'])) $tunnel = $_POST['tunnel'];
if (isset($_POST['page'])) $page = $_POST['page'];
if (isset($_POST['playlist'])) $playlist = $_POST['playlist'];
if (isset($_POST['resultsPerPage'])) $resultsPerPage = $_POST['resultsPerPage'];
if (isset($_POST['month'])) $month = $_POST['month'];

if (isset($_GET['tunnel'])) $tunnel = $_GET['tunnel'];
if (isset($_GET['page'])) $page = $_GET['page'];
if (isset($_GET['playlist'])) $playlist = $_GET['playlist'];
if (isset($_GET['resultsPerPage'])) $resultsPerPage = $_GET['resultsPerPage'];
if (isset($_GET['debug'])) $debug = $_GET['debug'];
if (isset($_GET['month'])) $month = $_GET['month'];


switch(strtolower($tunnel)) {
    case 'ifo' : $keyword = "Orlando"; break;
    case 'seat': $keyword = "Seattle"; break;
    case 'sfb' : $keyword = "SF Bay"; break;
    case 'hwd' : $keyword = "Hollywood"; break;
    case 'aus' : $keyword = "Austin"; break;
}

require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Gdata_YouTube');

//if (!isset($_SESSION)) session_start();

if (isset($_POST['nocache']) || isset($_GET['nocache'])) {
    session_destroy();
    //error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : WIPED OUT CACHE ");
    die;
}


$videos = getVideos($keyword, $page, $resultsPerPage,$playlist,$month);
//error_log("========== HERES THE VIDEOS ============");
//error_log(print_r($videos, true));

$json_result = json_encode($videos,JSON_FORCE_OBJECT);
echo $json_result;

if ($debug == 1) {

    echo "<pre>";
    print_r($videos);
    echo "</pre>";


    echo "<pre>";
    print_r($_SESSION);
    echo "</pre>";

}

exit;

function getVideos($keyword, $page, $resultsPerPage,$playlist,$month) {
    //error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : getVideos for keyword:{$keyword} page:{$keyword} resultsPerPage:{$resultsPerPage} ");
    //$resultsPerPage = 50;
    $yt = new Zend_Gdata_YouTube();
    $yt->setMajorProtocolVersion(2);

    $result = array();

    $result['media'] = array();
    $result['results_per_page'] = $resultsPerPage;
    $result['current_playlist'] = $playlist;
    $result['total_videos'] = 0;
    $result['playlists']=array();
    $foundPlaylists = array();
    $latestPlaylistID = "";

    // retrieve a list of playlists matching keyword
    if (isset($_SESSION['playlist'][date("Y-m-d")][$keyword . $month])) {
        $foundPlaylists = $_SESSION['playlist'][date("Y-m-d")][$keyword . $month];
    } else {


        $feed = $yt->getPlaylistListFeed("iflytunnelvision");

       // error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : PLAYLIST (part I) feed count: " . print_r(count($feed), true));


        // get results
        foreach ($feed as $entry) {
           // error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : playlist title text: " . print_r($entry->getTitle()->getText(), true));
            //var_dump($entry);
            $playlistName = $entry->getTitle()->getText();
            // find matching keyword (ie.  Orlando)



                if (strstr(strtoupper($playlistName), strtoupper($keyword))) {
                    if ($month == "" || strstr(strtoupper($playlistName), strtoupper($month))) {
                        $foundPlaylists[$entry->getUpdated()->getText()]['count'] = $entry->getCountHint()->getText();
                        $foundPlaylists[$entry->getUpdated()->getText()]['id'] = $entry->getId()->getText();
                    }
                }

        }

        // sort in ascending order
        krsort($foundPlaylists);

        $_SESSION['playlist'][date("Y-m-d")][$keyword . $month] = $foundPlaylists;
    }

    // calculate the number of videos in each playlist and set the playlist id
    $i = 0;
    foreach ($foundPlaylists as $entry){
            $i++;
            $result['playlists'][$i] = $entry['count'];
            $result['total_videos'] += $entry['count'];

            // if playlist is not specified, use the latest one
            if ($playlist == 0){
                if ($latestPlaylistID == ""){
                    $latestPlaylistID = $entry['id'];
                }
            } else {
                if ($i == $playlist){
                    $latestPlaylistID = $entry['id'];
                }
            }


    }
    // maybe it wasn't found? return whatever we could find
    if ($latestPlaylistID == ""){
        foreach ($foundPlaylists as $entry){
            $latestPlaylistID = $entry['id'];
        }
    }
    // nice to have
    if ($result['total_videos']>0){
        $result['total_pages'] = ceil($result['total_videos'] / $resultsPerPage);
    }

    // using the playlist id, retrieve videos
    if (isset($_SESSION['media'][date("Y-m-d")][$keyword . $month][$playlist][$page])) {
        $result['media'] = $_SESSION['media'][date("Y-m-d")][$keyword . $month][$playlist][$page];
    } else {

        // with the most recent playlist...
        if ($latestPlaylistID != "") {
            $id = $latestPlaylistID;
            $idarray = explode(":",$id);
            if (is_array($idarray)){
                $id = $idarray[count($idarray)-1];
            }


            // get videos back in reversed position
            //$url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=position&start-index=" . (($page * $resultsPerPage) + 1) . "&max-results=" . $resultsPerPage . "";
            //$url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=reversedPosition";
            $url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=reversedPosition&start-index=" . ((($page -1) * $resultsPerPage) + 1) . "&max-results=" . $resultsPerPage . "";
            //echo $url;
            $query = $yt->newVideoQuery($url);

            // get the videos for the playlist
            $feed = $yt->getVideoFeed($query);
            $result['current_playlist_count'] = $feed->getTotalResults()->getText();
            // error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : video list result['count']: " . print_r($result['count'], true));

             $videos = array();
             foreach($feed->getEntry() as $entry) {
                 $videoObject = array();
                 $videoThumbnails = $entry->getVideoThumbnails();
                 $videoObject['videoTitle'] = $entry->getVideoTitle();
                 $videoObject['videoDescription'] = $entry->getVideoDescription();
                 $videoObject['videoID'] = $entry->getVideoId();
                 $videoObject['videoURL'] = $entry->getVideoWatchPageUrl();
                 // error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : video-testimonials videoTitle: " . print_r($videoObject['videoTitle'], true));


                 $fname = "/inetpub/wwwroot/testimonials.iflyworld.com/video-reviews/photos/{$videoObject['videoID']}.jpg";
                 if (!file_exists($fname)) {
                    $videoObject['videoThumb'] = $videoThumbnails[0]['url'];
                } else {
                    $videoObject['videoThumb'] = "http://testimonials.iflyworld.com/video-reviews/photos/{$videoObject['videoID']}.jpg";
                 }

                 $videos[] = $videoObject;
             }

             $result['media'] = $videos;

             $_SESSION['media'][date("Y-m-d")][$keyword . $month][$playlist][$page] = $result['media'];

        }
    }

    //error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : result: " . print_r($result, true));
    return $result;
}



?>
