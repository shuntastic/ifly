<?php include("inc/head.php"); ?>
<body class="home local seattle">

	<div id="city-name">
		<span>SEATTLE</span>
	</div>

	<div id="skyline"></div>

	<div id="wrapper">

		<?php include("inc/header.php"); ?>
		
		<?php include("inc/hero-local.php"); ?>

	</div><!-- /#wrapper -->

	<div id="main" role="main">

		<div class="content-block box-section">

			<div class="box-holder">

				<?php include("inc/flight-wizard.php"); ?>

				<?php include("inc/reviews.php"); ?>				

			</div><!-- /.box-holder -->

		</div><!-- /.box-section -->

		<div class="content-block metal-bg">

			<?php include("inc/tiles.php"); ?>			

			<?php include("inc/gallery.php"); ?>

			<div class="btn-block">
				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>
			</div><!-- /.btn-block -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->


	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>