// Analytics Function calls

IFLY.analytics = {
	localUA:'',
	subdomain:'',
	init:function() {
		var full = (window.location.host).split('.');
		IFLY.analytics.subdomain = full[0];
		switch(IFLY.analytics.subdomain) {
			case 'austin':
				IFLY.analytics.localUA = 'UA-36993250-1';
			break;
		
			case 'orlando':
				IFLY.analytics.localUA = 'UA-36980669-1';
			break;
		
			case 'hollywood':
				IFLY.analytics.localUA = 'UA-36983885-1';
			break;
		
			case 'seattle':
				IFLY.analytics.localUA = 'UA-36982683-1';
			break;
		
			case 'sfbay':
				IFLY.analytics.localUA = 'UA-36983056-1';
			break;
			
			case 'iflyworld':
			case 'www':
			case 'dev':
				//IFLY.analytics.subdomain = '';
				IFLY.analytics.localUA = 'UA-36987792-1';
			break;
		}
	//	console.log('IFLY.analytics.localUA',IFLY.analytics.localUA);
		if(_gaq) {
			_gaq.push(
				[IFLY.analytics.subdomain+'._setAccount', IFLY.analytics.localUA],
				[IFLY.analytics.subdomain+'._setDomainName', IFLY.analytics.subdomain+'.iflyworld.com'], 
				[IFLY.analytics.subdomain+'._trackPageview']
			);
		}
		$('a').not('.img').each(function() {
			var destLink = $(this).attr('href');
			if(destLink && destLink.substr(0,4) == 'http') {
				if($(this).attr('target') == '_blank') {
					$(this).click(function(e) {
						e.preventDefault();
						IFLY.analytics.recordOutboundLink(destLink, 'External Link','Clicks',destLink);
					});
				} else {
//					$(this).click(function(e) {
//						e.preventDefault();
//					});
				}
			}
		});
	},
	sendEvent: function(category, action, label, opt_value) {
	/*
		STRUCTURE REFERENCE:	
		_gaq.push(['_trackEvent', 'Contact Form', 'Submit', 'Austin']);
		_gaq.push(['austin._trackEvent', 'Contact Form', 'Submit', 'Austin']);
		USAGE:
		IFLY.analytics.sendEvent('Contact Form', 'Submit', 'Austin',null);
	*/	
		var localTrack = IFLY.analytics.subdomain+'._trackEvent';
		if(opt_value == null) {
			_gaq.push(['_trackEvent', category, action, label]);
			
			//ONLY ADD TRACKING IF LOCAL SITE
		//	if(IFLY.analytics.subdomain !='')
				_gaq.push([localTrack, category, action, label]);
		} else {
			_gaq.push(['_trackEvent', category, action, label, opt_value]);
			
			//ONLY ADD TRACKING IF LOCAL SITE
			//if(IFLY.analytics.subdomain !='')
				_gaq.push([localTrack, category, action, label, opt_value]);
		}
	},
	sendSocial: function(network,socialAction,opt_target, opt_pagePath) {
	/*
		STRUCTURE REFERENCE:	
		_gaq.push(['_trackSocial', 'Facebook', 'Like', 'Item/URL Liked', 'Pagepath of like']);
		_gaq.push(['austin._trackSocial', 'Facebook', 'Like', 'Item/URL Liked', 'Pagepath of like']);
		USAGE:
		IFLY.analytics.sendSocial('Facebook', 'Like', 'Item/URL Liked', 'Pagepath of like');
	*/	
		opt_target = (opt_target != null) ? opt_target : '';
		opt_pagePath = (opt_pagePath != null) ? opt_pagePath : '';
		_gaq.push(['_trackSocial', network, socialAction, opt_target, opt_pagePath]);
			
		//ONLY ADD TRACKING IF LOCAL SITE
	//	if(IFLY.analytics.subdomain !='') {
			var localTrack = IFLY.analytics.subdomain+'._trackSocial';
			_gaq.push([localTrack, network, socialAction, opt_target, opt_pagePath]);
//		}
	},
	recordOutboundLink: function(link, category, action, label) {
		_gat._getTrackerByName()._trackEvent(category, action, label);
	//	setTimeout('document.location = "' + link + '"', 100);
		setTimeout('window.open("' + link + '")', 100);
	},
	sale_processed: function(cartItems,cartTotal,orderID) {
	var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(urlParams.tunnel);
			_gaq.push(['_addTrans',
			   orderID,           // order ID - required
			   selTunnelInfo.title, // affiliation or store name
			   cartTotal,          // total - required
			   '',           // tax
			   '',          // shipping
			   '',       // city
			   '',     // state or province
			   ''             // country
			]);
			_gaq.push([IFLY.analytics.subdomain+'._addTrans',
			   orderID,           // order ID - required
			   selTunnelInfo.title, // affiliation or store name
			   cartTotal,          // total - required
			   '',           // tax
			   '',          // shipping
			   '',       // city
			   '',     // state or province
			   ''             // country
			]);
		$.each(cartItems, function(index,value) {
			_gaq.push(['_addItem',
			  orderID,           // order ID - necessary to associate item with transaction
			   this.id,           // SKU/code - required
			   this.name,        // product name
			   '',   // category or variation
			   this.price,          // unit price - required
			   this.qty               // quantity - required
			]);
		
			_gaq.push([IFLY.analytics.subdomain+'._addItem',
			  orderID,           // order ID - necessary to associate item with transaction
			   this.id,           // SKU/code - required
			   this.name,        // product name
			   '',   // category or variation
			   this.price,          // unit price - required
			   this.qty               // quantity - required
			]);
			
			if(this.price > 0) {
				_gaq.push(['_trackEvent', 'Purchases', 'Completed', this.name, this.price]);
				_gaq.push([IFLY.analytics.subdomain+'._trackEvent', 'Purchases', 'Completed', this.name, this.price]);
			}else {
				_gaq.push(['_trackEvent', 'Redemptions', 'Redeemed', this.name, this.price]);
				_gaq.push([IFLY.analytics.subdomain+'._trackEvent', 'Redemptions', 'Redeemed', this.name, this.price]);
			}
		
		});
			_gaq.push(['_trackTrans']);
			_gaq.push([IFLY.analytics.subdomain+'._trackTrans']);
	}
};
$(document).ready(function() {
	IFLY.analytics.init(); 
});
