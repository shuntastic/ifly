<?php
if($video)
{
	?>
	<div class="controls">

		<a class="videoplayer-preview-fullscreen">
			<?php echo $this->lang->line('fullscreen'); ?>
		</a>

		<div class="splitter-light-top"></div>

		<a class="videoplayer-preview-favorite<?php
		if($video['is_favorite'])
		{
			echo " videoplayer-preview-favorite-selected";
		}
		?>"><?php echo $this->lang->line('add_favorite'); ?></a>

	</div>

	<div class="videoplayer-preview-video">
		<?php
		if(isset($video))
		{
			echo $video['embed'];
		}
		?>
	</div>

	<div class="videoplayer-preview-description">
		<?php

		$description = $video['description'];

		if(strlen($description) > 0)
		{
			echo $description;
		} else {
			?>
			<p class="videoplayer-preview-description-empty"><?php echo $this->lang->line('no_description'); ?></p>
			<?php
		}
		?>
	</div>
	<?php
}
?>