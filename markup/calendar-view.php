<?php include("inc/head.php"); ?>
<body class="info-page two-col event-list">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<article>

						<div class="event">
							<p class="date outline">July 7, 2012</p>
							<h2 class="outline ">Sisters in SkyDiving - Tunnel Retreat</h2>
							<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
							<p><a href="#" class="more-info">MORE INFO</a></p>
						</div><!-- /.event -->
						

						<div class="event">
							<p class="date outline">July 7, 2012</p>
							<h2 class="outline 	">Sisters in SkyDiving - Tunnel Retreat</h2>
							<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
							<p><a href="#" class="more-info">MORE INFO</a></p>
						</div><!-- /.event -->

						<div class="event">
							<p class="date outline">July 7, 2012</p>
							<h2 class="outline ">Sisters in SkyDiving - Tunnel Retreat</h2>
							<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
							<p><a href="#" class="more-info">MORE INFO</a></p>
						</div><!-- /.event -->

						<div class="event">
							<p class="date outline">July 7, 2012</p>
							<h2 class="outline ">Sisters in SkyDiving - Tunnel Retreat</h2>
							<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
							<p><a href="#" class="more-info">MORE INFO</a></p>
						</div><!-- /.event -->

						<div class="event">
							<p class="date outline">July 7, 2012</p>
							<h2 class="outline ">Sisters in SkyDiving - Tunnel Retreat</h2>
							<p>Coaching from: Melanie Curtis, Melissa Nelson, Brianne Thompson, Kimberly Winslow, Amy Chemelecki, and Catriona Adam! Flight times from 6AM to 1PM!</p>
							<p><a href="#" class="more-info">MORE INFO</a></p>
						</div><!-- /.event -->
					</article>
					
					<aside>
						<div id="calendar-wrapper">
							<div id="event-widget"></div>
                            <div id="legend" class="clearfix">
							<span class="today">Today's Date<em></em></span>
							<span class="event">Event Date<em></em></span>
							<span class="current">Current Event<em></em></span>
						</div>

						</div>
					</aside>

				</section>
			
			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	
</body>
</html>