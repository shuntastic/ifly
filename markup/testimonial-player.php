<?php
session_start();
if (isset($_GET['videoID'])) $_SESSION['videoID'] = $_GET['videoID'];
if (isset($_GET['utm_campaign'])) $_SESSION['utm_campaign'] = $_GET['utm_campaign'];

if (!isset($_SESSION['videoID'])) $_SESSION['videoID'] = "7G-QD2RO28c";
if (!isset($_SESSION['utm_campaign'])) $_SESSION['utm_campaign'] = "media share";

$videoID = $_SESSION['videoID'];
$utm_campaign = $_SESSION['utm_campaign'];

if (isset($_GET['delete'])) {
	rename("C:\\inetpub\\wwwroot\\iflyworld\\video-reviews\\photos\\" . $_GET['videoID'] . ".jpg","C:\\inetpub\\wwwroot\\iflyworld\\video-reviews\\photos\\backup\\" . $_GET['videoID'] . ".jpg");	
	header("location: http://www.iflyworld.com");
}

if ( file_exists("photos/{$videoID}.jpg")) {
	$thumbnail = "<a href='http://www.iflyworld.com/video-reviews/index.php?videoID={$videoID}'><img border='0' align='right' height='150'  src='photos/{$videoID}.jpg' style='' /></a>";
}
else $thumbnail = $thumbnail = "<a href='http://www.iflyworld.com/video-reviews/index.php?videoID=7G-QD2RO28c'><img border='0' align='right' height='150'  src='images/thumb.png' style='' /></a>";
?>
<!DOCTYPE html>
<!-- saved from url=(0065)http://bl-nkcanvas.com/_projects/iFly/landingPage/?v=aEw-axwUpUE# -->
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link href="https://plus.google.com/104652435906847083124" rel="publisher" />
    <meta property="fb:admins" content="80717587543" />
	<title>iFLY Indoor Skydiving</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="viewport" content="initial-scale = 0.2, user-scalable = yes">
	<script id="twitter-wjs" src="./video_files/widgets.js"></script>
	<script src="./video_files/jquery.min.js"></script>
    <script type="text/javascript" src="./video_files/swfobject.js"></script> 
	<script src="./video_files/script.js?cache=0"></script>
    

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-33782739-1']);
  _gaq.push(['_setDomainName', 'iflyworld.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<link rel="stylesheet" href="htttp://www.iflyworld.com/css/styles.css">
<meta property="og:title" content="iFLY" />
<meta property="og:type" content="video" />
<meta property="og:url" content="http://www.iflyworld.com/video-reviews/index.php?videoID=<?php echo $videoID; ?>&utm_campaign=<?php echo $utm_campaign; ?>&utm_source=facebook&utm_medium=social&utm_content=testimonial" />
<meta property="og:site_name" content="iFLY World" />
<meta property="fb:admins" content="1115109887" />   
<meta property="og:description" content="In-Flight video at iFLY World">    
      
<meta property="og:image" content="http://www.iflyworld.com/video-reviews/photos/<?php echo $videoID; ?>.jpg" />
      <meta property="og:video" content="http://www.youtube.com/v/<?php echo $videoID; ?>?version=3&amp;autohide=1">
      <meta property="og:video:type" content="application/x-shockwave-flash">
      <meta property="og:video:width" content="1280">
      <meta property="og:video:height" content="720">      

</head>

<body data-twttr-rendered="true">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=409878702400471";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		<div id="social">
			<ul>
				<li id="social_share" style="vertical-align:top">
                
                <div class="fb-like" data-href="http://www.iflyworld.com/video-reviews/index.php?videoID=<?php echo $videoID; ?>" data-colorscheme="light" data-send="true" data-layout="button_count" data-width="90" data-show-faces="false"></div>           
                </li>
				<li id="twitter"><a onClick="sendTwitter()" href="#"></a></li>
			</ul>
		</div>
		<section id="video" class="container" style="background-color:rgba(14,32,150,0.1);">
			<div id="video_player">
	            <div id="video_player_outer">
                    <div id="player">
                        <iframe width="820" height="500" frameborder="0" allowfullscreen="" src="http://www.youtube.com/embed/<?php echo  $videoID; ?>"></iframe>            
                    </div>  
                </div>                  
                <div id="like_screen" style="display:none">	             
                	<a href="#" onClick="setVideo('<?php echo  $videoID; ?>'); return false;"><img border='0' src='./images/click-like-button.png'/></a>                 
	            </div>
                <div id="search_screen" style="display:none">	                             	
	            </div>
            </div>
		</section>
</body></html>