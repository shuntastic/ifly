		<header id="header">
			<div class="logo"><a href="/home.php">I Fly indoor skydiving</a></div>
			<div class="header-block">
				<div class="header-top">
					<strong class="phone">1-800-555-5555</strong>
					<nav class="add-nav">
						<ul>
							<li><a href="#">Shop Now</a></li>
							<li><a href="#">Waiver</a></li>
							<li><a href="#">Contact Us</a></li>
						</ul>
					</nav>
				</div><!-- /.header-top -->
				<div class="header-holder">
					<ul class="social">
						<li><a class="facebook" href="#">facebook</a></li>
						<li><a class="twitter" href="#">twitter</a></li>
					</ul>
					<form action="#" class="search-form">
						<fieldset>
							<input class="text" type="text" placeholder="SEARCH" />
							<input class="btn-search" type="submit" value="go" />
						</fieldset>
					</form>
				</div>
				<nav id="nav">
					<ul>
						<li id="what">
							<a href="#"><span class="ir">What is iFly?</span></a>
							<div class="drop">
								<ul>
									<li><a href="#">The Flying Experience</a></li>
									<li><a href="#">VIDEO TESTIMONIALS </a></li>
									<li><a href="#">RATINGS &amp; REVIEWS</a></li>
									<li><a href="#">FAQs</a></li>
									<li><a href="#">The Sport of Flying</a></li>
									<li><a href="#">About Us</a></li>
								</ul>
							</div>
						</li>
						<li id="shop">
							<a href="#"><span class="ir">BOOK NOW</span></a>
							<div class="drop">
								<ul>
									<li><a href="#">Book Your Flight</a></li>
									<li><a href="#">Buy a Gift Card</a></li>
									<li><a href="#">Redeem a Gift Card</a></li>
									<li><a href="#">Buy Pics &amp; Vids</a></li>
								</ul>
							</div>
						</li>
						<li id="media">
							<a href="#"><span class="ir">FLIGHT INFO</span></a>
							<div class="drop">
								<ul>
									<li><a href="#">Flyer Guide</a></li>
									<li><a href="#">First-Time Flyer</a></li>
									<li><a href="#">Return Flyer</a></li>
									<li><a href="#">Experienced Flyer</a></li>
									<li><a href="#">Group or Party</a></li>
									<li><a href="#">iFLY Pics &amp; Vids</a></li>
									<li><a href="#">Gift Cards</a></li>
								</ul>
							</div>
						</li>
						<li id="find">
							<a href="#"><span class="ir">>FIND A LOCATION</span></a>
						</li>
					</ul>
				</nav>
			</div><!-- /.header-block -->
		</header>