$('.bpTemplate').click(function (){
	if ($(this).is(':checked')){
		$(this).parent("td").siblings("td").children(".bp-hidden-fields").show();
	} else {
		$(this).parent("td").siblings("td").children(".bp-hidden-fields").hide().children("li").children("input").removeAttr("checked");
	}
});