<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Ep_matrix
 *
 * ----------------------------------------------------------------------------------------------
 * @package	EE2 
 * @subpackage	ThirdParty
 * @author	Andrea Fiore / Malcolm Elsworth 
 * @link	http://electricputty.co.uk 
 * @copyright	Copyright (c) 2011 Electric Putty Ltd.
 *
 */

class Ep_matrix {



	var $matrixObj;
	var $bundled_celltypes = array('text', 'date', 'file');
	var $settings = NULL;



	function Ep_matrix($settings=array())
	{		
		$this->EE =& get_instance();
		$this->settings = $settings;
		
		// -------------------------------------------
		// Load the library file and instantiate logger
		// -------------------------------------------
		require_once PATH_THIRD . '/ep_better_workflow/libraries/ep_workflow_logger.php';
		$this->action_logger = new Ep_workflow_logger($this->settings['advanced']['log_events']);
		
		// -------------------------------------------
		// Load the third party model
		// -------------------------------------------
		$this->EE->load->model('ep_third_party');
	}



	function remove_matrices_from_data($data)
	{
		// Get all the matrix fields
		$mtx_fields = $this->EE->ep_third_party->get_all_fields('matrix');
		
		if ($mtx_fields->num_rows() > 0)
		{
			foreach ($mtx_fields->result_array() as $row)
			{
				$this_entry_id = $data['entry_id'];
				$this_field_id = $row['field_id'];
				$this_field_name = 'field_id_'.$row['field_id'];
				$row_order = 0;
				$new_row_loop = 0;

				// If this field is defined in our data array - store it in the cache and update the data array before sending back
				if (isset($data[$this_field_name]))
				{					
					// Logging
					$this->action_logger->add_to_log("ep_matrix: remove_matrices_from_data(): ".$this_field_name. " Found in data");
					
					// Grab the value of the matrix field
					$mtx_data = $data[$this_field_name];
					
					// If this field is also defined in the revision_post array update $mtx_data
					if(isset($data['revision_post'][$this_field_name]))
					{
						$mtx_data = $data['revision_post'][$this_field_name];
					}
					
					// Now we have a block of Matrix data, process it
					$data = $this->_process_matrix_draft_data($mtx_data, $this_entry_id, $this_field_id, $this_field_name, $row_order, $new_row_loop, $data);
				}
			}
		}
		
		return $data;
	}



	function _process_matrix_draft_data($mtx_data, $this_entry_id, $this_field_id, $this_field_name, $row_order, $new_row_loop, $data)
	{

		// Set a blank value here so we don't run into any errors further along
		$this->EE->session->cache['ep_better_workflow']['matrix_fields']['last_col_type'] = '';

		// Load the Matrix field type
		// We want to leverage as much as we can from the original process
		$class = 'Matrix_ft';
		$this->EE->api_channel_fields->include_handler('matrix');

		if (class_exists($class))
		{
			$mx = new $class();
			$mx->settings['field_id'] = $this_field_id;
			$mx->settings['field_required'] = 'n';
			if (method_exists($mx, 'validate'))
			{
				// We're not really validating here, we just want the row settings which are stored in the cache by this method
				$mx->validate($mtx_data);

				// Create an empty array in case we can't actually get any 
				$mx_col_data = array();
				
				// Save this data in the BWF cache
				// As of 2.2.4 Matrix changed the way it caches its data
				if(isset($mx->cache['fields']))
				{
					$mx_col_data = $mx->cache['fields'][$this_field_id]['cols'];
				}
				else
				{
					// As of Matrix 2.3 this has chnaged this again (Thanks, Brandon)
					// Why use the field47 format - this is not used anywhere, ever by anyone
					if(isset($mx->cache['field_cols'][$this_field_id]))
					{
						$mx_col_data = $mx->cache['field_cols'][$this_field_id];
					}
					elseif(isset($mx->cache['field_cols']['field'.$this_field_id]))
					{
						$mx_col_data = $mx->cache['field_cols']['field'.$this_field_id];
					}
				}

				$this->EE->session->cache['ep_better_workflow']['matrix_fields'][$this_field_id]['cols'] = $mx_col_data;
			}
		}

		// Delete all existing 'data' records for this field (as in, not 'delete' records)
		$this->EE->ep_third_party->delete_draft_data(array('entry_id' => $this_entry_id, 'field_id' => $this_field_id, 'type'=>'matrix'));

		// Now we have the Matrix data loop through its rows, if we have any
		if (isset($mtx_data['row_order']))
		{
			foreach ($mtx_data['row_order'] as $row_name)
			{
				if(isset($mtx_data[$row_name]))
				{
					$row_data = $mtx_data[$row_name];
				}

				// If the row_name has the words 'row_new' in it we need to create a temp ID
				if (strpos($row_name, 'row_new') !== false)
				{
					$row_id = 'row_new_'.$new_row_loop;
				
					// There is an issue with this and SAFECRACKER!
					$data = $this->_update_matrix_data_in_array($data, $this_field_name, $row_name, $row_id);
				
					$new_row_loop++;
				}
				else
				{
					$row_id = substr($row_name, 7);
				}

				// Now that we have the raw data we need to check each col's *type* as they all have their own post processing
				// It's all pretty complex stuff but hopefully we can borrow a bunch of code from the *real* Matrix
				foreach($row_data as $col_name => $col_data)
				{					
					// Logging
					$this->action_logger->add_to_log("ep_matrix: _process_matrix_draft_data(): Looping through rows - current col: ".$col_name);

					$col_id = substr($col_name, 7);

					$col_data = $this->_do_type_specific_save($col_id, $col_name, $col_data, $row_name, $this_field_id, $this_field_name);

					// If this is a safe cracker file type we need to update the draft data
					if($this->EE->session->cache['ep_better_workflow']['matrix_fields']['last_col_type'] == 'safecracker_file')
					{
						if(isset($data['revision_post'][$this_field_name][$row_name][$col_name]))
						{
							$data['revision_post'][$this_field_name][$row_name][$col_name] = $col_data;
						}
						else
						{							
							$new_row_name = $this->EE->session->cache['ep_better_workflow']['matrix_fields']['temp_col_names'][$this_field_name][$row_name];
							$data['revision_post'][$this_field_name][$new_row_name][$col_name] = $col_data;
						}
					}

					// If this col is a Playa we need to grab the playa data and save it in our third party table
					if(isset($this->EE->session->cache['playa']['selections'][$this_field_id][$col_id]))
					{
						// First delete any existing Playa records for this cell
						$this->EE->ep_third_party->delete_matrix_playa_data($this_entry_id, $this_field_id, $row_id, $col_id);

						// Reset the Playa order counter
						$playa_order = 0;

						// Get the Playa selection data from the cache
						$cell_rels = $this->EE->session->cache['playa']['selections'][$this_field_id][$col_id][$row_name];

						// Now update the third party table with our new Playa data
						foreach($cell_rels as $rel)
						{
							$this->EE->ep_third_party->update_draft_data($this_entry_id, $this_field_id, 'playa', $row_id, $playa_order, $col_id, $rel);
							$playa_order++;
						}
					}
					
					// SafeCracker file does the very useful thing of creating a hidden field with the same name, so we need to 'not' add these to our third-party table
					if (strpos($col_name, '_hidden') === false)
					{
						$this->EE->ep_third_party->update_draft_data($this_entry_id, $this_field_id, 'matrix', $row_id, $row_order, $col_id, $col_data);
					}
				}
				$row_order++;
			}
		}

		// Remove all temp row IDs
		$data = $this->_remove_temp_row_ids($data, $this_field_name);

		// If there are any deleted rows data remove this from the data array before we send it back
		$data = $this->_save_matrix_delete_data($data, $this_entry_id, $this_field_name, $this_field_id);
		
		return $data;
	}



	private function _update_matrix_data_in_array($data, $this_field_name, $old_value, $new_value)
	{		
		// Do we have a defined sortorder for this field?
		if (isset($data['revision_post'][$this_field_name]['row_order']))
		{
			foreach($data['revision_post'][$this_field_name]['row_order'] as $key => $value)
			{
				if($value == $old_value)
				{
					// Save a reference to the old row id as we may need this later 
					$this->EE->session->cache['ep_better_workflow']['matrix_fields']['temp_col_names'][$this_field_name][$value] = $new_value."_temp";
					
					$data['revision_post'][$this_field_name]['row_order'][$key] = $new_value."_temp";					
					$data['revision_post'][$this_field_name][$new_value."_temp"] = $data['revision_post'][$this_field_name][$value];
					unset($data['revision_post'][$this_field_name][$value]);
				}
			}
		}
		return $data;
	}



	private function _remove_temp_row_ids($data, $this_field_name)
	{
		if (isset($data['revision_post'][$this_field_name]['row_order']))
		{
			// Create a temp array and looper
			$temp_array = array();
			$i = 0;
			
			foreach($data['revision_post'][$this_field_name]['row_order'] as $key => $value)
			{
				$row_id = $value;

				// If we created a temp row ID in the 'update_matrix_data_in_array' method, revert it to a matrix friendly format
				if (strpos($value, '_temp') !== false)
				{
					$row_id = substr($value, 0, strlen($value)-5);
				}
				
				$temp_array['row_order'][$i] = $row_id;
				$temp_array[$row_id] = $data['revision_post'][$this_field_name][$value];
				$i++;
			}
			if(isset($data['revision_post'][$this_field_name]['deleted_rows']))
			{
				$temp_array['deleted_rows'] = $data['revision_post'][$this_field_name]['deleted_rows'];
			}
			
			// Replace the record in the data array with our new, correctly ordered one
			$data['revision_post'][$this_field_name] = $temp_array;
			
		}
		else
		{
			$data['revision_post'][$this_field_name]['row_order'] = "0";	
		}
		return $data;
	}



	private function _save_matrix_delete_data($data, $this_entry_id, $this_field_name, $this_field_id)
	{
		// Do we have any deleted rows for this field?
		if (isset($data['revision_post'][$this_field_name]['deleted_rows']))
		{
			foreach($data['revision_post'][$this_field_name]['deleted_rows'] as $del_row)
			{
				$row_id = substr($del_row, 7);				
				$this->EE->ep_third_party->update_draft_data($this_entry_id, $this_field_id, 'matrix_delete', $row_id, '', '', '');
			}
			
			// Remove the actual deleted rows field from the data so the Publish page doesn't treat it like another row when it next loads
			unset($data['revision_post'][$this_field_name]['deleted_rows']);
		}
		return $data;
	}



	function get_draft_data_for_preview($matrixObj, $params, $sql)
	{
		$return_sql = $sql;

		$field_id = $matrixObj->field_id;
		$entry_id = $matrixObj->row['entry_id'];
		
		$has_params = FALSE;
		$where_clause = '';
		$order_clause = '';
		$limit_clause = '';
		
		// Process any params specified in the matrix tag
		$this->_process_params($field_id, $params, $has_params, $where_clause, $order_clause, $limit_clause);
		
		// Test the clauses match the params specifed in the template
		#echo "Where: ".$where_clause."<br />";
		#echo "Order by: ".$order_clause."<br />";
		#echo "Limit: ".$limit_clause."<br />";
		#echo "Has params: ".$has_params."<br />";
					
		// Get the rows we need for this matrix field
		$matrix_query = "SELECT distinct row_id FROM exp_ep_entry_drafts_thirdparty where entry_id = ".$entry_id." and field_id = ".$field_id." and type = 'matrix' ORDER BY row_order ASC";
		
		$matrix_rows = $this->EE->db->query($matrix_query);
		
		// If we have some Matrix data build and return our custom query
		if ($matrix_rows->num_rows() > 0)
		{
			// Define the *master* select query and a looper
			$the_select = "";
			$looper = 0;	
	
			foreach($matrix_rows->result() as $matrix_row)
			{
				$this_row_id = $matrix_row->row_id;

				if($looper > 0) $the_select .= " UNION ALL ";

				$the_select .= "SELECT DISTINCT";
				$query = $this->EE->db->query("SELECT distinct col_id FROM exp_ep_entry_drafts_thirdparty WHERE entry_id = ".$entry_id." AND field_id = ".$field_id."");
				foreach ($query->result() as $row)
				{
					$the_select .= "(SELECT data FROM exp_ep_entry_drafts_thirdparty WHERE type = 'matrix' AND col_id = ".$row->col_id." AND field_id = ".$field_id." AND entry_id = ".$entry_id." AND row_id = '".$this_row_id."') as col_id_" .$row->col_id.",";
				}
				$the_select .= "'".$this_row_id."' AS row_id FROM exp_ep_entry_drafts_thirdparty WHERE field_id = ".$field_id." AND entry_id = ".$entry_id;
				$looper++;
			}

			// Were any params specified ?
			if ( $has_params )
			{
				// create a temporary table to store the resultset
				$the_select = 'CREATE TEMPORARY TABLE exp_ep_matrix_temp '. $the_select;
				$this->EE->db->query($the_select);
				
				// Apply where, order and limit clauses
				// Using 1=1 so the code block lifted from Matrix doesn't need to change at all :)
				$filtered_sql = "SELECT * FROM exp_ep_matrix_temp WHERE 1 = 1 ". $where_clause . $order_clause . $limit_clause;
				$return = $this->EE->db->query($filtered_sql);
				
				// Drop the temporary table now in case there is another Matrix request in this template
				$this->EE->db->query('DROP TABLE IF EXISTS exp_ep_matrix_temp');
			}
			else
			{
				$return = $this->EE->db->query($the_select);
			}
		}
		// If we don't have any data return a query which will return nothing
		else
		{
			$return = $this->EE->db->query($matrix_query);
		}

		return $return;
	}



	// Thanks yet again, to Mark Croxton @croxton for leveraging Matrix's native param handling
	private function _process_params($field_id, $params, &$has_params, &$where_clause, &$order_clause, &$limit_clause)
	{
		// Get all the matrix field cols
		$query = $this->EE->db->select('col_id, col_type, col_label, col_name, col_instructions, col_width, col_required, col_search, col_settings')
		->where('field_id', $field_id)
		->order_by('col_order')
		->get('matrix_cols');

		if ($query->num_rows())
		{
			$cols = $query->result_array();	
			$col_ids_by_name = array();
			$select = 'row_id';
			$where = '';
			$select_mode = 'data';
			
			/* START block copied from ft.matrix.php lines 2018 > 2124 */
			/* Note: Brandon essentially copied this 'search:' param code from the Channel Entries module */
			foreach ($cols as &$col)
			{
				$col_id = 'col_id_'.$col['col_id'];
				$col_ids_by_name[$col['col_name']] = $col['col_id'];

				if ($select_mode == 'data') $select .= ', '.$col_id;

				if (isset($params['search:'.$col['col_name']]))
				{
					$has_params = TRUE;
					$terms = $params['search:'.$col['col_name']];

					if (strncmp($terms, '=', 1) == 0)
					{
						// -------------------------------------------
						//  Exact Match e.g.: search:body="=pickle"
						// -------------------------------------------
						$terms = substr($terms, 1);

						// special handling for IS_EMPTY
						if (strpos($terms, 'IS_EMPTY') !== FALSE)
						{
							$terms = str_replace('IS_EMPTY', '', $terms);

							$add_search = $this->EE->functions->sql_andor_string($terms, $col_id);

							// remove the first AND output by $this->EE->functions->sql_andor_string() so we can parenthesize this clause
							$add_search = substr($add_search, 3);

							$not = (strncmp($terms, 'not ', 4) == 0);
							$conj = ($add_search != '' && ! $not) ? 'OR' : 'AND';

							if ($not)
							{
								$where .= 'AND ('.$add_search.' '.$conj.' '.$col_id.' != "") ';
							}
							else
							{
								$where .= 'AND ('.$add_search.' '.$conj.' '.$col_id.' = "") ';
							}
						}
						else
						{
							$where .= $this->EE->functions->sql_andor_string($terms, $col_id).' ';
						}
					}
					else
					{
						// -------------------------------------------
						//  "Contains" e.g.: search:body="pickle"
						// -------------------------------------------
						if (strncmp($terms, 'not ', 4) == 0)
						{
							$terms = substr($terms, 4);
							$like = 'NOT LIKE';
						}
						else
						{
							$like = 'LIKE';
						}

						if (strpos($terms, '&&') !== FALSE)
						{
							$terms = explode('&&', $terms);
							$andor = (strncmp($like, 'NOT', 3) == 0) ? 'OR' : 'AND';
						}
						else
						{
							$terms = explode('|', $terms);
							$andor = (strncmp($like, 'NOT', 3) == 0) ? 'AND' : 'OR';
						}

						$where .= ' AND (';

						foreach ($terms as $term)
						{
							if ($term == 'IS_EMPTY')
							{
								$where .= ' '.$col_id.' '.$like.' "" '.$andor;
							}
							else if (preg_match('/^[<>]=?/', $term, $match)) // less than/greater than
							{
								$term = substr($term, strlen($match[0]));

								$where .= ' '.$col_id.' '.$match[0].' "'.$this->EE->db->escape_str($term).'" '.$andor;
							}
							else if (strpos($term, '\W') !== FALSE) // full word only, no partial matches
							{
								$not = ($like == 'LIKE') ? ' ' : ' NOT ';

								// Note: MySQL's nutty POSIX regex word boundary is [[:>:]]
								$term = '([[:<:]]|^)'.preg_quote(str_replace('\W', '', $term)).'([[:>:]]|$)';

								$where .= ' '.$col_id.$not.'REGEXP "'.$this->EE->db->escape_str($term).'" '.$andor;
							}
							else
							{
								$where .= ' '.$col_id.' '.$like.' "%'.$this->EE->db->escape_like_str($term).'%" '.$andor;
							}
						}

						$where = substr($where, 0, -strlen($andor)).') ';
					}
				}
			}
			
			// Update the where clause
			$where_clause = $where;


			// -------------------------------------------
			//  Orberby + Sort
			// -------------------------------------------
			$orderbys = (isset($params['orderby']) && $params['orderby']) ? explode('|', $params['orderby']) : array();
			$sorts    = (isset($params['sort']) && $params['sort']) ? explode('|', $params['sort']) : array();

			$all_orderbys = array();
			foreach ($orderbys as $i => $name)
			{
				$has_params = TRUE;
				$name = (isset($col_ids_by_name[$name])) ? 'col_id_'.$col_ids_by_name[$name] : $name;
				$sort = (isset($sorts[$i]) && strtoupper($sorts[$i]) == 'DESC') ? 'DESC' : 'ASC';
				$all_orderbys[] = $name.' '.$sort;
			}

			// Update the order clause as long as at least one order by condition has been specifed
			if(count($all_orderbys) > 0) $order_clause = ' ORDER BY '.implode(', ', $all_orderbys);


			// -------------------------------------------
			//  Offset and Limit
			// -------------------------------------------
			// if we're not sorting randomly, go ahead and set the offset and limit in the SQL
			if ((! isset($params['sort']) || $params['sort'] != 'random') && (isset($params['limit']) || isset($params['offset'])))
			{
				$has_params = TRUE;
				$offset = (isset($params['offset']) && $params['offset'] && $params['offset'] > 0) ? $params['offset'] . ', ' : '';
				$limit  = (isset($params['limit']) && $params['limit'] && $params['limit'] > 0) ? $params['limit'] : 100;

				// Update the limit clause
				$limit_clause = ' LIMIT ' . $offset . $limit;
			}
		}
	}



	private function _do_type_specific_save($col_id, $col_name, $col_data, $row_name, $field_id, $field_name)
	{
		// Set up a blank col settings array
		$col_info = array();
		
		// Get the col data from the cache
		$cols = $this->EE->session->cache['ep_better_workflow']['matrix_fields'][$field_id]['cols'];
		
		// Find the right item in our cols array
		foreach($cols as $col)
		{
			if($col['col_id'] == $col_id)
			{
				$col_info = $col;
			}
		}

		// As long as we have a col type in our data
		if(isset($col_info['col_type']))
		{
			// Set a session variable to we can do some post processing
			$this->EE->session->cache['ep_better_workflow']['matrix_fields']['last_col_type'] = $col_info['col_type'];
			
			// Borrowing Matrix method, get the relevant cell type
			$celltype = $this->_get_celltype($col_info['col_type']);

			// Give the celltype a chance to do what it wants with the data
			if (method_exists($celltype, 'save_cell'))
			{			
				// TODO: Figure out what happens with settings	
				// We've learnt that Playa needs the field_id
				$celltype->settings = array_merge($col_info['col_settings'], $col_info['celltype_settings']);
				$celltype->settings['col_id'] = $col_id;
				$celltype->settings['col_name'] = $col_name;
				$celltype->settings['row_name'] = $row_name;
				$celltype->settings['field_id'] = $field_id;
				$celltype->settings['field_name'] = $field_name;

				$col_data = $celltype->save_cell($col_data);
			}
		}
		
		return $col_data;
	}



	// --------------------------------------------------------------------

	/**
	 * Get Celltype Class
	 */
	private function _get_celltype_class($name, $text_fallback = FALSE)
	{
		// $name should look like exp_fieldtypes.name values
		if (substr($name, -3) == '_ft') $name = substr($name, 0, -3);
		$name = strtolower($name);

		// is this a bundled celltype?
		// Need to try and get this data from Matrix...
		if (in_array($name,  $this->bundled_celltypes))
		{
			$class = 'Matrix_'.$name.'_ft';

			if (! class_exists($class))
			{
				// load it from matrix/celltypes/
				require_once PATH_THIRD.'matrix/celltypes/'.$name.EXT;
			}
		}
		else
		{			
			$class = ucfirst($name).'_ft';
			$this->EE->api_channel_fields->include_handler($name);
		}

		if (class_exists($class))
		{
			// method_exists() is supposed to accept the class name (string),
			// but running into at least one server where that's not the case...
			$ft = new $class();

			if (method_exists($ft, 'display_cell'))
			{
				if (! isset($this->cache['celltype_global_settings'][$name]))
				{					
					$this->EE->db->select('settings');
					$this->EE->db->where('name', $name);
					$query = $this->EE->db->get('fieldtypes');

					$settings = $query->row('settings');
					$this->cache['celltype_global_settings'][$name] = is_array($settings) ? $settings : unserialize(base64_decode($settings));
				}
				return $class;
			}
		}

		return $text_fallback ? $this->_get_celltype_class('text') : FALSE;
	}



	// --------------------------------------------------------------------

	/**
	 * Get Celltype
	 */
	private function _get_celltype($name, $text_fallback = FALSE)
	{
		$class = $this->_get_celltype_class($name, $text_fallback);

		if (! $class) return FALSE;

		$celltype = new $class();

		$global_settings = $this->cache['celltype_global_settings'][$name];
		$celltype->settings = $global_settings && is_array($global_settings) ? $global_settings : array();

		return $celltype;
	}



}