<?php

/*
11-26-12

When a customer purchases a gift card, they will receive a link to this file in their confirmation email
The link contains an encrypted ID for the debitware_sales table where a link to the pdf file is

This file looks up the ID, gets the pdf file and displays it
*/

$error = false;

if (isset($_GET['id']) && isset($_GET['tunnel'])) {

	$tunnel = strtoupper($_GET['tunnel']);
	$data['id'] = $_GET['id'];
	$result = json_decode(ifly_world_api($tunnel, "debitware", "get_debitware", $data));

	if (isset($result->data)) {

		// retrieve PDF data:
		// print_r($result->data->pdf_filename);

		$filepath = "C:\\ProgramData\\Siriusware\\PDF\\" . $tunnel . "\\" . $result->data->pdf_filename;

		if (file_exists($filepath)){

			$pdf = file_get_contents($filepath);

			header("Content-type:application/pdf");

			header("Content-Disposition:attachment;filename=".$result->data->pdf_filename."");

			echo $pdf;

		} else {
			$error = true;
		}


	} else {
		$error = true;
	}


} else {
	$error = true;
}


if ($error) echo "Your certificate is not yet available. Please try again in a few minutes or contact support. ";


function ifly_world_api($tunnel, $controller, $method, $data)
	{


		$data["tunnel"]  = $tunnel;
		$apikey = "c6af7b9ef5c13869332319b7d9087fe5";
		$format = "json";

		$data['controller'] = $controller;
		$data['method'] = $method;
		$data['apikey'] = $apikey;
		$data['format'] = $format;



		$url = "http://api.iflyworld.com/index.php/$controller/$method/apikey/$apikey/format/$format";



		// initialize cURL
		$ch = curl_init();

		// set cURL options
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		// execute cURL
		$result = curl_exec($ch);

		// validate if it is a json string
		$data = @json_decode($result); // supress warning using @ operator
		if ($data === null && json_last_error() !== JSON_ERROR_NONE) {
			$json_error['status'] = "JSON_ERR";
			$json_error['json'] = $result;
			$result = json_encode($json_error);
		}

		return $result;
	}
?>