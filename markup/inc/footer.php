	<footer id="footer">
		<div class="block-holder">
			<div class="block">
				<h4>What is iFLY</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">The Flying Experience</a></li>
						<li><a href="#">VIDEO TESTIMONIALS </a></li>
						<li><a href="#">RATINGS &amp; REVIEWS</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>Book Now</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Book Your Flight</a></li>
						<li><a href="#">Buy a Gift Card</a></li>
						<li><a href="#">Redeem a Gift Card</a></li>
						<li><a href="#">Buy Pics &amp; Vids</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>My iFLY</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">My Profile</a></li>
						<li><a href="#">I.B.A Training Hours</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>Flight Info</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Flyer Guide</a></li>
						<li><a href="#">First-Time Flyer</a></li>
						<li><a href="#">Return Flyer</a></li>
						<li><a href="#">Experienced Flyer</a></li>
						<li><a href="#">Group or Party</a></li>
						<li><a href="#">iFLY Pics &amp; Vids</a></li>
						<li><a href="#">Gift Cards</a></li>
					</ul>
				</nav>
			</div>
			
			<div class="block last">
				<h4>Legal</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Tunnel Waiver</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<p>Copyright &copy; 2012 SkyVenture. Vivamus adipiscing ultrices lacus, ut feugiat nunc adipiscing id. </p>
	</footer>
	