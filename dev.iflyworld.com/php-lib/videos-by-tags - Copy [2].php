<?php
ini_set('error_log', '/tmp/testimonials.log');

$resultsPerPage = 50;

error_log("*****-------------- videos-by-tags ------*****\n");
error_log('POST:');
error_log(print_r($_POST,true));

$tunnel = $_POST['tunnel'];
$page = $_POST['page'];
$date = $_POST['date'];
$keyword = "Austin";
switch(strtolower($tunnel)) {
    case 'ifo' : $keyword = "Orlando"; break;
    case 'seat': $keyword = "Seattle"; break;
    case 'sfb' : $keyword = "SF Bay"; break;
    case 'hwd' : $keyword = "Hollywood"; break;
    case 'aus' : $keyword = "Austin"; break;
}

require_once 'Zend/Loader.php';
Zend_Loader::loadClass('Zend_Gdata_YouTube');

//if (!isset($_SESSION)) session_start();

if (isset($_POST['nocache'])) {
    session_destroy();
    error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : WIPED OUT CACHE "); 
    die;
}

//$videos = getVideos("Seattle",1);

$videos = getVideos($keyword, $page, $resultsPerPage);
error_log("========== HERES THE VIDEOS ============");
error_log(print_r($videos, true));

$json_result = json_encode($videos,JSON_FORCE_OBJECT);
echo $json_result;

exit;

//echo "<pre>";
//print_r($videos);
//echo "</pre>";
//
//echo "<pre>";
//print_r($_SESSION);
//echo "</pre>";
//

function getVideos($keyword, $page, $resultsPerPage) {
    error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : getVideos for keyword:{$keyword} page:{$keyword} resultsPerPage:{$resultsPerPage} "); 
    //$resultsPerPage = 50;
    $yt = new Zend_Gdata_YouTube();
    $yt->setMajorProtocolVersion(2);

    $result = array();
    $result['count'] = 0;
    $result['media'] = array();

    if (!isset($_SESSION['media'])) $_SESSION['media'] = array(); // initialize this array if it does not exist
    if (!isset($_SESSION['media'][$keyword])) $_SESSION['media'][$keyword] = array();
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")])) $_SESSION['media'][$keyword][date("Y-m-d")] = array(); // session cache only good for one day
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['count'])) $_SESSION['media'][$keyword][date("Y-m-d")]['count'] = 0;

    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['page'])) $_SESSION['media'][$keyword][date("Y-m-d")]['page'] = array();
    if (!isset($_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page])) $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page] = array();

    // USE CACHING IF POSSIBLE
    if (count($_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page]) != 0) {
        error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : USING CACHED RESULTS WITH PAGE = : " . print_r($_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page], true)); 
        $result['count'] =$_SESSION['media'][$keyword][date("Y-m-d")]['count'] ;
        $result['media'] = $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page];
        //return $result; // we alredy retrieved all the videos. just retrieve from cache
    } else {
        error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : NOT USING CACHED RESULTS ");
    }

    if ($_SESSION['media'][$keyword][date("Y-m-d")]['count'] !=0 ) {
        if ($page * $resultsPerPage + 1 > $_SESSION['media'][$keyword][date("Y-m-d")]['count']) return $result;
    }

    // get list of playlists
    $url = "https://gdata.youtube.com/feeds/api/users/iflytunnelvision/playlists?v=2";
    $query = $yt->newVideoQuery($url);
	//$query->setTime('all_time');
    $feed = $yt->getVideoFeed($query); 

    error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : PLAYLIST (part I) feed count: " . print_r(count($feed), true)); 
    $latestPlaylist = NULL;
    $playlistDate = null;
	$allPlaylists = array();
    foreach($feed as $entry) {
        error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : playlist title text: " . print_r($entry->getTitle()->getText(), true)); 
        //var_dump($entry);
        $playlistName = $entry->getTitle()->getText();
        // find matching keyword (ie.  Orlando)
        if (strstr($playlistName, $keyword)) {
            if (!$playlistDate){
                $latestPlaylist = $entry;
                $latestUpdated = $entry->getUpdated()->getText();
                $playlistDate =  strtotime($latestUpdated);
            } else if ($playlistDate < strtotime($entry->getUpdated()->getText()) ){
                $latestPlaylist = $entry;
                $latestUpdated = $entry->getUpdated()->getText();
                $playlistDate =  strtotime($latestUpdated);
            }
			$allPlaylists[] = $entry->getUpdated()->getText();
        }
    }

    //error_log('=====================');
    //error_log('LATEST PLAYLIST:' .  $latestPlaylist);
    //exit;
    error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : FINAL playlist title text: " . print_r($latestPlaylist->getTitle()->getText(), true)); 

    // with the most recent playlist...
    if ($latestPlaylist != NULL) {
        $id = $latestPlaylist->getId()->getText();
        $idarray = explode(":",$id);
        if (is_array($idarray)){
            $id = $idarray[count($idarray)-1];
        }

        $page--;
        // get videos back in reversed position
        //$url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=position&start-index=" . (($page * $resultsPerPage) + 1) . "&max-results=" . $resultsPerPage . "";
        $url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=reversedPosition&start-index=" . (($page * $resultsPerPage) + 1) . "&max-results=" . $resultsPerPage . "";
        //$url = "https://gdata.youtube.com/feeds/api/playlists/" . $id . "?v=2&orderby=reversedPosition";
        //echo $url;
        $query = $yt->newVideoQuery($url);

        // get the videos for the playlist
        $feed = $yt->getVideoFeed($query); 
        $result['count'] = $feed->getTotalResults()->getText();
        error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : video list result['count']: " . print_r($result['count'], true)); 
        $_SESSION['media'][$keyword][date("Y-m-d")]['count'] = $feed->getTotalResults()->getText();

         $videos = array();
         foreach($feed->getEntry() as $entry) {
             $videoObject = array();
             $videoThumbnails = $entry->getVideoThumbnails();
             $videoObject['videoTitle'] = $entry->getVideoTitle();
             $videoObject['videoDescription'] = $entry->getVideoDescription();
             $videoObject['videoID'] = $entry->getVideoId();
             $videoObject['videoURL'] = $entry->getVideoWatchPageUrl();
             error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : video-testimonials videoTitle: " . print_r($videoObject['videoTitle'], true)); 
			

             $fname = "/inetpub/wwwroot/testimonials.iflyworld.com/video-reviews/photos/{$videoObject['videoID']}.jpg";
             if (!file_exists($fname)) {
                $videoObject['videoThumb'] = $videoThumbnails[0]['url'];
            } else {
                $videoObject['videoThumb'] = "http://testimonials.iflyworld.com/video-reviews/photos/{$videoObject['videoID']}.jpg";
             }

             $videos[] = $videoObject;
         }

         //$videos = array_reverse($videos);

         $result['results_per_page'] = $resultsPerPage;
         $result['media'] = $videos;
         $_SESSION['media'][$keyword][date("Y-m-d")]['page'][$page] = $videos;
    }

    //error_log(__FILE__ . ":" . __FUNCTION__ . ":" . __LINE__ . " : result: " . print_r($result, true));
    return $result;
}



?>
