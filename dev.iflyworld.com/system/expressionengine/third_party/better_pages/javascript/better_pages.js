// User clicks a Better Pages thumb
$(".better_pages_thumb").click(function(){
	t = $(this).attr("id").split("_");
	
	if ($(this).hasClass("selected")){
		$(".better_pages_thumb").removeClass("selected");
		$("#pages__pages_template_id option:selected, #pages_template_id option:selected", this).remove();
		$(".bpSubmissionField").val("");
	} else {
		$(".better_pages_thumb").removeClass("selected");
		$(this).addClass("selected");
		$("#pages__pages_template_id option:selected, #pages_template_id option:selected", this).remove();
		$("#pages__pages_template_id option[value='"+t[2]+"'], #pages_template_id option[value='"+t[2]+"']").attr("selected", "selected");
		$(".better_pages_other_templates option[value='0']").attr("selected", "selected");
		$(".bpSubmissionField").val(t[2]);
	}
	
});

// User changes the pages template
$("#pages__pages_template_id, #pages_template_id").change(function(){
	$(".better_pages_thumb").removeClass("selected");
	$("#better_pages_"+$(this).val()).addClass("selected");
	$(".better_pages_other_templates option:selected", this).remove();
	$(".better_pages_other_templates option[value='"+$(this).val()+"']").attr("selected", "selected");
	$(".bpSubmissionField").val($(this).val());
});

// User selects from Better Pages other templates
$(".better_pages_other_templates").change(function(){
	$(".better_pages_thumb").removeClass("selected");
	$("#pages__pages_template_id option:selected, #pages_template_id option:selected", this).remove();
	$("#pages__pages_template_id option[value='"+$(this).val()+"'], #pages_template_id option[value='"+$(this).val()+"']").attr("selected", "selected");
	$(".bpSubmissionField").val($(this).val());
});

// User types into Better Pages URL field
$('#better_pages_url').bind('keypress keyup blur', function() {
    $("#pages__pages_uri, #pages_uri").val($(this).val());
});

// User clicks into Better Pages URL field for the first time
$('#better_pages_url').click(function(){
	if ($(this).attr("value") == "/example/pages/uri"){
		$(this).attr("value", "");
	}
});

// User types into Pages URL field
$('#pages__pages_uri, #pages_uri').bind('keypress keyup blur', function() {
    $("#better_pages_url").val($(this).val());
});

