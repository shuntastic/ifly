// Booking functions
IFLY.booking = {
	getFlightOverviewPackages: function(tLoc,fType,fDate) {
			var today = new Date();
			var call='method=get_recommended_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&flight_date='+fDate.yyyymmdd()+'&flyer_num=1';
//			console.log(' getFlightOverviewPackages call:',call);
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
					console.log("success function fired");
					var data = response.data;
					$.each(data, function(index,value) {
						var package = '<div class="package"><h2>'+ this.title + ' - $' + parseFloat(this.price_1).toMoney(2) + '</h2>\
											<p>'+this.long_description +'</p>\
											<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\
										</div>'
						$('#main article').append(package);
					});
				},
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	showRecPackages: function(tLoc,fType,fDate,fNum) {
			var today = new Date();
			var call='method=get_recommended_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&flight_date='+fDate.yyyymmdd()+'&flyer_num='+fNum;
//			console.log('showRecPackages call:',call);
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
//					console.log('showRecPackages response:',response);
					var data = packageData = response.data;
					$.each(data, function(index,value) {
						var package = '<tr class="poption'+index+'"><td class="package">'+this.title+'</td><td class="desc">'+this.cart_short_description+'&nbsp;<a href="#" class="more-info tooltip ir" data-title="'+this.title+'" data-content="'+this.cart_long_description+'">more info</a>	\
							</td><td>$'+parseFloat(this.price_1).toMoney(2)+'</td>	\
							<td><form action="#" class="uniform tunnel-locator">\
									<select name="1" class="quantity">\
										<option value="1">1</option>\
										<option value="2">2</option>\
										<option value="3">3</option>\
										<option value="4">4</option>\
										<option value="5">5</option>\
										<option value="6">6</option>\
										<option value="7">7</option>\
										<option value="8">8</option>\
										<option value="9">9</option>\
							</select></form></td>\
							<td class="price_total">$'+(parseFloat(this.qty)*parseFloat(this.price_1)).toMoney(2)+'</td>\
							<td><a href="booking-step4.php" class="btn green choose-package"><em></em><span>CHOOSE</span></a></td></tr>'
						$('table#rec-packages tbody').append(package);
						
						//CHANGE QUANTITY & SET PRICE BASED ON NITIAL DROPDOWN VALUE
						if (parseFloat(this.qty) > 1) {
							$('tr.poption'+index+' select[class$="quantity"] option:selected').removeAttr('selected');
							$('tr.poption'+index+' select[class$="quantity"] option[value="'+this.qty +'"]').attr("selected","selected");
						}
						
					});

					IFLY.initUniform();
					IFLY.initToolTip();
					$('form select.quantity').each(function(index) {
						$(this).change(function(){
							var baseTotal = parseFloat(packageData[index].price_1);
							var newTotal = $(this).val()*baseTotal;
							$('tr.poption'+index+' td.price_total').html('$'+newTotal.toMoney(2));
						});
					});
					$('a.choose-package').each(function(index){
						$(this).click(function(e) {
							var sTotal = parseFloat(packageData[index].price_1).toMoney(2);
							var totalPrice = parseFloat($('tr.poption'+index+' form select.quantity').val())*sTotal;
							var totalMinutes = parseFloat(packageData[index].minutes);
	//						var totalMinutes = parseFloat($('tr.poption'+index+' form select.quantity').val())*parseFloat(packageData[index].minutes);
							urlParams.select_total=totalPrice;
							urlParams.session_minutes=totalMinutes;
							urlParams.qty = $('tr.poption'+index+' form select.quantity').val();
							urlParams.padded_DCI = packageData[index].padded_DCI;
							
							var serializedData = $.param(urlParams);
							
							var dURL = $(this).attr('href') + '?'+serializedData;
							window.location = dURL;
							e.preventDefault();
						});
					});
					$.uniform.update();
					
			},
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	showAvailableTimes: function(x) {
					var data = x;
					var morning = data.morning;
					var afternoon = data.afternoon;
					var evening = data.evening;
					var twilight = data.overnight;
					
					function buildRow(x,y) {
						var z = '';
						var sessionClass = (x.ecommavailable == 0) ? 'class="unavailable"':'';
						if(y%2 == 0) {
							z += '<div class="row">';
						} 
							z += '<em><input type="radio" name="time" value="'+x.session_time+'"><label '+sessionClass+'>'+x.session_label+'</label></em>';
						if(y%2 == 1) {
						z += '</div>';
						}
						return z;
					}
					
					var package = '<fieldset class="first"><legend>Morning</legend>';
					$.each(morning, function(index,value) {
						package += buildRow(value, index);
					});
						package += '</fieldset><fieldset><legend>Afternoon</legend>';
					$.each(afternoon, function(index,value) {
						package += buildRow(value, index);
					});
						package += '</fieldset><fieldset><legend>Evening</legend>';
					$.each(evening, function(index,value) {
						package += buildRow(value, index);
					});
						package += '</fieldset><fieldset><legend>Late Night</legend>';
					$.each(twilight, function(index,value) {
						package += buildRow(value, index);
					});
						package +='</fieldset><div id="legend" class="clearfix"><span class="green"><em></em>Your Time</span><span class="white"><em></em>Available Times</span><span class="black"><em></em>Unavailable Times</span></div><div class="actions clearfix"><input type="submit" class="book-now disabled" value="BOOK NOW"><p>Flight times are held for 10 minutes and are subject to availability.<br />You must arrive 1 hour before your flight time.</p></div>';
						$('#time-picker form').html(package);
						
					$('body.step4 a.btn-next').addClass('disabled');
					$('#time-picker input.book-now, body.step4 a.btn-next').click(function(e) {
						if(!$(this).hasClass('disabled')) {	
							urlParams.session_time = $('#time-picker form input:checked').val();
							IFLY.booking.addToCartAndContinue();
						}
						e.preventDefault();

					});
	},
	
	getAvailableTimes: function(tLoc,sessionMinutes,fDate) {
			var call='method=get_sessions&controller=siriusware&format=json&tunnel='+tLoc+'&minutes='+sessionMinutes+'&date='+fDate.yyyymmdd();
//			console.log('getAvailableTimes call:',call);
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
//					console.log('response',response.data);
					packageData = response.data;
					IFLY.booking.showAvailableTimes(response.data);
				},
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	addToCartAndContinue: function() {
		if(urlParams.wwsale_id == '') {
			var call='method=create_wwsale_id&controller=siriusware&format=json&tunnel='+urlParams.tunnel;
//			console.log('addToCartAndContinue - create_wwsale_id call:',call);
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
						urlParams.wwsale_id = response.data;
						cookieHandler.create('wwsale_id',urlParams.wwsale_id);
						IFLY.booking.addToCart(true);
				},
				error: handler.onError,
				complete: handler.onComplete
			});
		} else{
			IFLY.booking.addToCart(true);
		}
			
	},
	addToCart: function(contFlag) {
		if	(!urlParams.gc_amount)
		urlParams.gc_amount = 0;
		
		if	(!urlParams.token)
		urlParams.token = 0;
		
		var call='method=add_to_cart&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&flyer_num='+urlParams.qty+'&qty='+urlParams.flyer_num+'&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+urlParams.padded_DCI+'&session_time='+urlParams.session_time+'&flight_date='+urlParams.flyer_date+'&gc_amount='+urlParams.gc_amount+'&token='+urlParams.token;
			console.log('addToCartAndContinue call:',call);
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
					//console.log('addToCart:',response.data);
						if(contFlag) {
							urlParams.cartData = jQuery.parseJSON(response.data);
							IFLY.booking.loadCartPage();
						} else {
						//call modal
							
						}
				},
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	
	//LOAD STEP BOOKING 5
	loadCartPage: function() {
			var serializedData = $.param(urlParams);
			var dURL = 'booking-step5.php';
//			var dURL = 'booking-step5.php?'+serializedData;
			$.ajax({
				url: dURL,
				type: 'post',
				dataType: 'html',
				data: serializedData,
				success: function(response, textStatus, jqXHR){
					var data = $(response);
					var single = data.find('div.single-step').html();
					single = $(single);
					$('#main div.single-step').html(single);
					IFLY.booking.getCartItems();
					
					
					$('body').removeClass('step4');
					$('body').addClass('step5');

				},
				error: handler.onError,
				complete: handler.onComplete
			});
	},
	
	//Displays cart items on cart page, initialize buttons
	getCartItems: function() {
		if(urlParams.cartData.length > 0) {
			var output = '';
			var data = urlParams.cartData;
			var total_price = 0;
			var total_discount = 0;
			var total_tax = 0;
			var total_fee = 0;
			
			$.each(data, function(index,value) {
				var reservationInfo = 'For '+this.options.flyer_num+' '+this.name.toLowerCase()+((parseFloat(this.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name+' on '+new Date(this.options.flight_date).showFullDate()+ ' at '+ this.options.flight_time;
				var package = '<tr class="item'+index+'"><td><span class="qty">'+this.qty+'</span><a href="javascript:void(0);" class="btn red remove"><em></em><span>REMOVE</span></a></td><td><h3>'+this.name+'</h3><p>'+reservationInfo+'</p></td><td><p>Need Short description'+' <a href="#" class="tooltip ir" data-title="some title" data-content="Need long description">more info</a></p></td><td><p>'+this.qty+'</p></td><td>$'+(parseFloat(this.price)).toMoney(2)+'</td><td>$'+(parseFloat(this.price)*parseFloat(this.qty)).toMoney()+'</td></tr>'
					
					output+=package;
					
					var price = (this.qty != null) ? (this.price*parseFloat(this.qty)) : this.price;
					var tax = (this.qty != null) ? (this.tax*parseFloat(this.qty)) : this.tax;
					var discount = (this.qty != null) ? (this.discount*parseFloat(this.qty)) : this.discount;
					var fee = (this.qty != null) ? (this.fee*parseFloat(this.qty)) : this.fee;
					
					total_price += price;
					total_tax += tax;
					total_discount += discount;
					total_fee += fee;
			});
			
			var grand_total = urlParams.cart_total = total_price - total_discount + total_tax + total_fee;
			var summary='<table class="main"><tr class="sales-total"><th>Sales Total</th><td>$'+(total_price).toMoney(2)+'</td></tr>';

				summary += (total_discount > 0) ? '<tr class="discount"><th>Discount</th><td>$'+(total_discount).toMoney(2)+'</td></tr>' : '';
				summary += '<tr class="sub-total"><th>Sub Total</th><td>$'+(total_price).toMoney(2)+'</td></tr>';
				summary += '<tr class="tax"><th>Tax</th><td>$'+(total_tax).toMoney(2)+'</td></tr>';
				summary += (total_fee > 0) ? '<tr class="fee"><th>Fee</th><td>$'+(total_fee).toMoney(2)+'</td></tr>' : '';
				summary += '<tr class="grand-total"><th>Grand Total</th><td>$'+(grand_total).toMoney(2)+'</td></tr></table>';
				//add submit button
				summary += '<form class="uniform"><input type="submit" value="checkout" class="checkout disabled"><label>I accept the restrictions<br />and requirements.</label><input type="checkbox" name="restrictions" value="yes"></form>'

				$('table#your-cart tbody').html(output);
				$('div#summary').html(summary);
				
				$('div#summary input[type="checkbox"]').change(function() {
					if(this.checked) {
						$('input.checkout').removeClass('disabled');
					} else {
						$('input.checkout').addClass('disabled');
					}
				})
				$('input.checkout').click(function(e) {
					if(!$(this).hasClass('disabled') && IFLY.loggedIn) {
						cookieHandler.create('ifly_cart_total',urlParams.cart_total);
						var dURL = IFLY.booking.init.step6path;
						window.location = dURL;
					} else if (!IFLY.loggedIn) {
						$.colorbox({html:$(".modal.login").html()});
						IFLY.booking.init.login();	
					}
					e.preventDefault();
				});
				
				$('#your-cart a.remove').each(function(index, value) {
					$(this).click(function() {
						var call='method=remove_from_cart&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&unique_id='+urlParams.cartData[index].id;
						$.ajax({
							url: api,
							type: 'post',
							dataType: dataType,
							data: call,
							success: function(response, textStatus, jqXHR){
								//$('#your-cart tr.item'+index).remove();
								urlParams.cartData.splice(index, 1);
								//console.log('urlParams.cartData',urlParams.cartData);
								IFLY.booking.getCartItems();
							
							},
							error: handler.onError,
							complete: handler.onComplete
						});
					});
				});
				IFLY.initToolTip();
	
		} else {
				$('table#your-cart').html('<tr><td>Your Cart is Empty</td></tr>');
				$('div#summary').html('<table class="main"><tr class="sales-total"><th>Sales Total</th><td>$0.00</td></tr></table>');
		}
		
	},
	togglePayments: function() {
		$('fieldset.payment-method input').click(function() {
			$('div.pay-option').each(function() {$(this).addClass('disabled')});
			$('div.pay-option.'+$(this).val()).removeClass('disabled');
		});
	},
	checkSession: function() {
		if(!urlParams.wwsale_id) {
			var x = cookieHandler.read('wwsale_id');
			var y = cookieHandler.read('ifly_cart_total');
			urlParams.wwsale_id = (x) ? x: '';
		}
	},
	checkForm: function(fName) {
			switch(fName) {
				case 'step2':
				//BOOKING DETAILS PAGE
					if ($('a.btn.date span').html()!='FLIGHT DATE' &&$("#flyer-type").val()!='FLYER TYPE' && $("#how-many-fliers").val()!='# OF FLYERS' && $("#how-many-fliers").val()!='13') {
						$('input.next, a.btn-next').removeClass('disabled');
					} else {
						$('input.next, a.btn-next').addClass('disabled');
					}
				break;	
				
				case 'step6':
				//BOOKING DETAILS PAGE
					
//					if ($('a.btn.date span').html()!='FLIGHT DATE' &&$("#flyer-type").val()!='FLYER TYPE' && $("#how-many-fliers").val()!='# OF FLYERS' && $("#how-many-fliers").val()!='13') {
//						$('input.next, a.btn-next').removeClass('disabled');
//					} else {
//						$('input.next, a.btn-next').addClass('disabled');
//					}
				break;	
				
			}
	}
	
}
IFLY.calendarWidget.initPickADateCalendar = function(date) {
		var today = new Date();
        $('#calendar-pickadate').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
			minDate: today,
			defaultDate:date,
            onSelect: function(e) {
				//console.log('calendar click',e);
				urlParams.flyer_date = e;
				$('#time-picker form').html('<img class="loader" src="/images/iFLY_loader.gif" />');
				var textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
				$('body.step4 h2.note').html(textStatus);
				sessionStatus = textStatus;
				IFLY.booking.getAvailableTimes(urlParams.tunnel, urlParams.session_minutes, new Date(e));
			},
            //beforeShowDay:this.beforeShowDay
        });
}
IFLY.booking.init = {
	step1path:'booking-step1.php',
	step2path:'/book-now/booking-step2',
	step3path:'booking-step3.php',
	step4path:'booking-step4.php',
	step5path:'booking-step5.php',
	step6path:'booking-step6.php',
	step1: function() {
			if($('body.booking.step1 .continue a').length>0){
				$('body.booking.step1 .continue a').click(function(e) {
					$.colorbox({ html : $(".modal.balance").html(), onComplete:initBalance});
					e.preventDefault();
				});
			}
			function initBalance() {
				$('#gc-balance a.submit').click(function(e) {
					var call='method=get_debitware_info&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&swipe_no='+$('#gc-balance input.text').val();
					console.log('call',call);
					$.ajax({
						url: api,
						type: 'json',
						dataType: dataType,
						data: call,
						success: function(response, textStatus, jqXHR){
							console.log
							var data = response.data;
							$('#gc-balance div.status').html('Your remaining balance is $'+parseFloat(data.sp_bal_dy).toMoney(2)+' (expires '+(new Date(data.expires)).mmddyyyy+')');
						},
						error: handler.onError,
						complete: handler.onComplete
					});
					e.preventDefault();
				});
			}
		
	},
	step2: function() {
			function formatCalendarNav() {
				console.log('BEFORE CALLED');
				$("#calendar-booking .ui-datepicker-buttonpane.ui-widget-content").html('').append('<a href="javascript:closeCalendar();">Close</a>');
			};
			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';

			if (urlParams.tunnel) {
				$('select[id$="change-location"] option:selected').removeAttr('selected');
				$('select[id$="change-location"] option[value="'+urlParams.tunnel+'"]').attr("selected","selected");
			}
			if (urlParams.flyer_type) {
				$('select[id$="flyer-type"] option:selected').removeAttr('selected');
				$('select[id$="flyer-type"] option[value="'+urlParams.flyer_type+'"]').attr("selected","selected");
			}
			$.uniform.update();
			if($('#calendar-booking').length > 0) {
				$('#calendar-booking').datepicker({
					dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
					showButtonPanel: true,
					buttonImageOnly:false,
					minDate: 0,
					beforeShow: formatCalendarNav,
					closeText: "close",
					onSelect: function(e) {
						//console.log('e: '+e);
						$('a.date span').html(e)
						$(this).hide();	
						IFLY.booking.checkForm('step2');
					}
				});
				
				$('#calendar-booking').hide();
				$('#booking-details a.date').click(function(e) {
					e.preventDefault(); 
					$('#calendar-booking').show();
				});
			}
			

			$('#booking-details select').change(function() {
				IFLY.booking.checkForm('step2');
			});
			$('input.next, a.btn-next').click(function(e) {
				if(!$(this).hasClass('disabled')) {
					urlParams = {
						tunnel : $('#change-location').val(),
						flyer_date: $('a.btn.date span').html(),
						flyer_num: $("#how-many-fliers").val(),
						flyer_type: $("#flyer-type").val(),
						flyer_type_name: $('#flyer-type option:selected').html(),
						tunnel_name: $('#change-location option:selected').html()
					}
						
					var serializedData = $.param(urlParams);
						
					var dURL = '/book-now/booking-step3/?'+serializedData;
					window.location = dURL;
				}
				e.preventDefault();
			});
	},
	step3: function() {
			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
			if (urlParams.tunnel && urlParams.flyer_type && urlParams.tunnel_name) {
				$('select#change-location').val(urlParams.flyer_num);
				var textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name;
				$('div.single-step h2.note').html(textStatus);
				
				IFLY.booking.showRecPackages(urlParams.tunnel,urlParams.flyer_type,new Date(urlParams.flyer_date), urlParams.flyer_num);
			}
		
	},
	step4: function() {
			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
			IFLY.initTimePicker();
			if (urlParams.tunnel_name && urlParams.flyer_type && urlParams.tunnel_name && urlParams.session_minutes) {
				//showAvailableTimes(tLoc,fType,fDate)
				$('select#change-location').val(urlParams.flyer_num);
				var textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
				sessionStatus = textStatus;
				
				$('body.step4 h2.note').html(textStatus);
				IFLY.calendarWidget.initPickADateCalendar(urlParams.flyer_date);
				
				IFLY.booking.getAvailableTimes(urlParams.tunnel, urlParams.session_minutes, new Date(urlParams.flyer_date));
				//showRecPackages: function(tLoc,fType,fDate)
			}
	},
	step5: function() {
		IFLY.initLogin();
		console.log('step5');
	},
	step6: function() {
		IFLY.pay.togglePayments();
	},
	login: function() {
		$('form#lgn a.login').click(function(e) {
			var call='method=log_in&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&username='+$('#lgn input[name="username"]').val()+'&password='+$('#lgn input[name="password"]').val();
			$.ajax({
				url: api,
				type: 'post',
				dataType: dataType,
				data: call,
				success: function(response, textStatus, jqXHR){
					//var data = response.data;
					IFLY.loggedIn = true;
					cookieHandler.create('ifly_username',$('#lgn input[name="username"]').val());
					$.colorbox.close();
					var dURL = IFLY.booking.init.step6path;
					window.location = dURL;
				},
				error: handler.onError,
				complete: handler.onComplete
			});
			
			e.preventDefault();
		});
		
	},
	redeemGiftCard: function() {
		if($('body.booking.step1 .continue a').length>0){
		 	$('body.booking.step1 .continue a').click(function(e) {
				$.colorbox({ html : $(".modal.balance").html(), onComplete:function(){initRedeem();}});
				e.preventDefault();
			});
		}
			function initRedeem() {
					console.log('initRedeem');
				$('#gc-balance a.submit').click(function(e) {
					var call='method=get_debitware_info&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&swipe_no='+$('#gc-balance input[name="gc_num"]').val();
					$.ajax({
						url: api,
						type: 'post',
						dataType: dataType,
						data: call,
						success: function(response, textStatus, jqXHR){
							var data = response.data;
							$('#gc-balance div.status').html('Your remaining balance is $'+parseFloat(data.sp_bal_dy).toMoney(2)+' (expires '+(new Date(data.expires)).mmddyyyy+')');
						},
						error: handler.onError,
						complete: handler.onComplete
					});
					e.preventDefault();
				});
			}
	}
	
}
IFLY.initTimePicker = function () {
		$("#time-picker").delegate("label", "click", function () {
		var c = $(this).attr('class');
			if ( c == undefined) {
				var e = $("#time-picker").find('label.your-time');
				var s = e.siblings('input');
				e.removeAttr('class');
				s.removeAttr('checked');
				$(this).addClass('your-time');
				$(this).siblings('input').attr('checked',true);
				$('body.step4 h2.note').html(sessionStatus + ' at ' + $(this).html());
				if($('#time-picker input.book-now').length >0) {
					$('#time-picker input.book-now').removeClass('disabled');
					$('body.step4 a.btn-next').removeClass('disabled');
				}
				
			} 
		});
}
IFLY.pay = {
	togglePayments: function() {
		$('fieldset.payment-method input').click(function() {
			$('div.pay-option').each(function() {$(this).addClass('disabled')});
			$('div.pay-option.'+$(this).val()).removeClass('disabled');
		});
	},
}
function initPreso() {
	$('article.flier-type a.btn').each(function(index, element) {
		$(this).click(function() {
			var dURL = IFLY.booking.init.step2path;
			window.location = dURL;
		});
		
	});
}

function getParams() {
	(function () {
		var match,
			pl     = /\+/g,  // Regex for replacing addition symbol with a space
			search = /([^&=]+)=?([^&]*)/g,
			decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
			query  = window.location.search.substring(1);
	
		while (match = search.exec(query))
		   urlParams[decode(match[1])] = decode(match[2]);
	})();
	console.log('urlParams',urlParams);
}
var urlParams = {};
var packageData, sessionStatus;
var api = '/api_curl.php';
var dataType = 'html json';

$(document).ready(function() {
	if($('body.booking').length>0) {
		getParams();
		IFLY.booking.checkSession();
	}
	//TEMP
	if ($('body.booking.step1').length > 0) {
		initPreso();
		IFLY.booking.init.step1();
	}
});
