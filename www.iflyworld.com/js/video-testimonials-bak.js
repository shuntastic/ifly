
IFLY.testimonials = {
	total_reviews:0,
	total_pages:0,
	current_page:0,
	page_number:1,
	star_count:[],
	init: function() {
		
			IFLY.current_tunnel = (IFLY.current_tunnel == null) ? IFLY.locationHandler.getCurrentTunnel() : (IFLY.current_tunnel != 'aus') ? IFLY.current_tunnel : 'ifo';
	}
}

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
jQuery(function($){
    //console.log('video-testimonials.js was loaded');
    $(document).ready(function() {
        var page = 1;
        var maxResults = 12;
        var resultCount = 0;
		
        
		$('#search_results_list_column').hide();
        $('#video_player_wrapper').fadeIn(500); 	
        
        $('#like_screen').hide();
		//$('#page_list').hide();
		

		//$('.pagination').show();
		
		$('#close_button').click(function(){
			console.log('closing...');
			var tunnel = $('#flyer-location').val();
			if (tunnel == 'unselected') {
				loadResultsPage('ifo', 1);
				$('#flyer-location').val('ifo').change();
				//$('#flyer-location').val('iFLY Orlando');
			}
			console.log('hiding video player...');
			console.log('done hiding video player...');
			$('#video_player_wrapper').hide();				   
			$('#like_screen').hide();

			console.log('showing results list...');
			$('#search_results').fadeIn(500);
			//$('#search_results_list_column').fadeIn(500);
			$('#search_results_list_column').fadeIn(500);
			$('#close_button_div').hide();
			$('#page_list').fadeIn(500);
		});
	
        if($('body.video-testimonials').length >0) {
            console.log("loaded video-testimonials.js");
		
            $('#flyer-location').change(function(e) {
                console.log('flyer-loc (tunnel) changed to: ' + $('#flyer-location').val());
                var tunnel = $('#flyer-location').val();
                page = 1;
                loadResultsPage(tunnel, page);
            });

            function gup( name )
            {
                name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
                var regexS = "[\\?&]"+name+"=([^&#]*)";
                var regex = new RegExp( regexS );
                var results = regex.exec( window.location.href );
                if( results == null )
                    return "7G-QD2RO28c";
                else
                    return results[1];
            }
            
            setVideo(gup('videoID'));
            $('#close_button_div').fadeIn(500);
        }

        $('#next_page').click( function() {
            console.log('next page');
            $('#video_player_wrapper').hide();				   
            var tunnel = $('#flyer-location').val();
            page = page + 1;
            if (page <= resultCount) {
                loadResultsPage(tunnel, page);
            }
        });

        $('#prev_page').click( function() {
            $('#video_player_wrapper').hide();				   
            var tunnel = $('#flyer-location').val();
            page = page - 1;
            if (page >= 1) {
                loadResultsPage(tunnel, page);
            }
        });

        function loadResultsPage(tunnel, page) {
            console.log('loadResultsPage called with page:' + page);
            
            $.post("/php-lib/videos-by-tags.php", {
                queryType: "all", 
                //maxResults: maxResults , 
                page: page,
                tunnel: tunnel
            },  function(response) {
                // data contains a count and the array of results...
                var response = $.parseJSON(response);
                resultCount = response.count;
                console.log('set resultCount to:' + resultCount);
                var pages = Math.floor(resultCount/maxResults);
                $('.active').html(page);
                $('.total').html(pages);
                var html ="<ul style='list-style-type:none'>";

                $.each(response.media, function(key, value) {  
                    //console.log('value:');
                    //console.log(value);
                    if(typeof value.videoThumb != 'undefined'){
                        videoID = value['videoID'];
                        //console.log('videoID:' + videoID);
                        title = value['videoTitle'];
                        url = value['videoURL'];
                        description = value['videoDescription'];
                        thumb = value['videoThumb'];

                        if (title != "iFLY Austin August") {							
                            html = html + "<li class='channel_item' onClick='setVideo(\"" + videoID + "\")'>";
                            html = html + "<div class='channel_title'>" + title + "</div>";
                            html = html + "<div class='channel_photo'>";
                            //html = html + "<img alt='thumb' width='120' height='90' src='" + thumb + "' />";
                            html = html + "<div alt='thumb' width='120' height='90' style='min-width:120px; min-height:90px; background-image:url(" + thumb + "); '  ></div>";
                            html = html + "</div>";
                            //html = html + "<div class='channel_url'><a href='" + url + "'>URL</a></div>";
                            //html = html + "<div class='channel_text'>" + description + "</div>";
                            html = html + "</li>";
                        }
                    }
                });

                html = html + "</ul>";
                $('#search_results_video_list').html(html);
                $('#search_results_list_column').fadeIn(500);
                $('#close_button_div').hide();

                console.log('hiding video player...');
                $('#video_player_wrapper').hide();				   
                console.log('done hiding video player...');
				$('#page_list').show();
                $('#like_screen').hide();
            });
        }

    }); // end onready handler
	
}); // end jquery wrapper
        

function setVideo(id){
	console.log('lcicked on setSvide');
	$('#page_list').hide();
    videoID = id;
    //Check for existing SWF
    if(isObject("player")){
        //replace object/element with a new div					 
        replaceSwfWithEmptyDiv("player");
    }

    $('#search_results_list_column').hide();				   
    $('#video_player_wrapper').show();				   
	$('#close_button_div').fadeIn(500);
    $('#like_screen').hide();

    var params = {
        allowScriptAccess: "always"
    };
    var atts = {
        id: "player"
    };
    swfobject.embedSWF("http://www.youtube.com/v/" + id + "?enablejsapi=1&playerapiid=ytplayer&version=3&hl=en_US&theme=light&color=red&hd=1&rel=0&showinfo=0&autoplay=1&autohide=1&loop=0&modestbranding=1",
        "player", "840", "500", "8", null, null, params, atts);
}
			
//function setPlayer(id){
//    var html = "";
//    //html = "<iframe width='820' height='500' src='http://www.youtube.com/embed/videoseries?list="+id+"&amp;hl=en_US&theme=light&color=red&hd=1&rel=0&showinfo=1&autoplay=1&autohide=1&loop=1&modestbranding=1' frameborder='0' allowfullscreen></iframe>";
//    html = "<iframe width='820' height='500' src='http://www.youtube.com/embed/"+id+"&autoplay=1&autohide=1&loop=1&modestbranding=1' frameborder='0' allowfullscreen></iframe>";
//
//    $('#video_player_outer').html(html);				
//    $('#video_player').show();				   
//    $('#search_results').hide();
//    $('#like_screen').hide();
//								
//}			

function onYouTubePlayerReady(playerId) {
    player = document.getElementById("player");	
    console.log('onYouTubePlayerReady');
    player.addEventListener("onStateChange", "onytplayerStateChange");
}
			
function onytplayerStateChange(newState) {			   			   	 
    console.log('onytplayerStateChange-newstate:' + newState);
    if (newState == 0){
        $('#player').hide();				   
        $('#like_screen').fadeIn(500);
    }
}
			
function playVideo(){
    player.playVideo();
}
			
//Support function: checks to see if target
//element is an object or embed element
function isObject(targetID){
    var isFound = false;
    var el = document.getElementById(targetID);
    if(el && (el.nodeName === "OBJECT" || el.nodeName === "EMBED")){
        isFound = true;
    }
    return isFound;
}
			 
//Support function: creates an empty
//element to replace embedded SWF object
function replaceSwfWithEmptyDiv(targetID){
    var el = document.getElementById(targetID);
    if(el){
        var div = document.createElement("div");
        el.parentNode.insertBefore(div, el);
			 
        //Remove the SWF
        swfobject.removeSWF(targetID);
			 
        //Give the new DIV the old element's ID
        div.setAttribute("id", targetID);
    }
}
			

