<!-- .videoplayer-box -->

<div class="videoplayer-box<?php

if($this->input->post('method'))
{
	// lightbox mode

	echo ' videoplayer-lightbox';
}

?>">


	<!-- .videoplayer-accounts -->

	<div class="videoplayer-accounts">

		<div class="top">
			<a class="videoplayer-close">
				<span class="splitter-top-bottom">
					<span class="splitter-top-right">
						<?php echo $this->lang->line('close'); ?>
					</span>
				</span>
			</a>
		</div>

		<!-- .videoplayer-services -->

		<ul class="videoplayer-services">
			<?php

			foreach($accounts as $account)
			{
				if($account->enabled && $account->is_authenticated)
				{
					?>
					<li class="videoplayer-<?php echo $account->service?>" data-service="<?php echo $account->service?>">
						<a data-method="videos" data-service="<?php echo $account->service?>" class="videoplayer-service"><?php echo $account->service?></a>
						<ul class="videoplayer-service-actions">
							<li><a data-method="service_search" class="videoplayer-service-search"><?php echo $this->lang->line('search'); ?></a></li>
							<li><a data-method="service_videos" class="videoplayer-service-videos"><?php echo $this->lang->line('videos'); ?></a></li>
							<li><a data-method="service_favorites" class="videoplayer-service-favorites"><?php echo $this->lang->line('favorites'); ?></a></li>
						</ul>
					</li>
					<?php
				}
			}
			?>

		</ul>

		<!-- /.videoplayer-services -->

		<div class="bottom">
			<a class="videoplayer-manage-btn" href="<?php echo $manage_link?>" target="_blank">
				<span class="splitter-bottom">
					<span class="splitter-bottom-right">
						<?php echo $this->lang->line('configure'); ?>
					</span>
				</span>
			</a>
		</div>

	</div>

	<!-- /.videoplayer-accounts -->



	<!-- .videoplayer-listings -->

	<div class="videoplayer-listings">

		<div class="top">

			<div class="splitter-top-bottom">
				<div class="splitter-top-left">
					<div class="splitter-top-right">
						<!-- .videoplayer-search -->

						<div class="videoplayer-search">
							<div class="search-bg">
								<div class="search-left">
									<div class="search-right">
										<input type="text" placeholder="Search videos" />
										<div class="videoplayer-search-reset">

										</div>
									</div>
								</div>
							</div>

						</div>

						<!-- /.videoplayer-search -->

						<!-- .videoplayer-title -->

						<h2 class="videoplayer-title-videos"><?php echo $this->lang->line('my_videos'); ?></h2>

						<h2 class="videoplayer-title-favorites"><?php echo $this->lang->line('favorites'); ?></h2>

						<!-- /.videoplayer-title -->
					</div>
				</div>
			</div>
		</div>

		<!-- .videoplayer-videos -->

		<div class="videoplayer-videos">
			<div class="videoplayer-videos-inject">
				<p class="videoplayer-videos-empty"><?php echo $this->lang->line('search_video'); ?></p>
			</div>
		</div>

		<!-- /.videoplayer-videos -->

		<div class="bottom">
			<div class="splitter-bottom">
				<div class="splitter-bottom-left">
					<div class="splitter-bottom-right">
					</div>
				</div>
			</div>
		</div>

	</div>

	<!-- /.videoplayer-listings -->



	<!-- .videoplayer-preview -->

	<div class="videoplayer-preview">

		<div class="top">
			<div class="splitter-top-bottom">
				<div class="splitter-top-left"></div>
			</div>
		</div>

		<div class="videoplayer-preview-inject"></div>


		<div class="bottom">
			<div class="splitter-bottom">
					<div class="splitter-bottom-left">

						<div class="videoplayer-controls">
							<div class="videoplayer-controls-in">
								<a class="videoplayer-submit videoplayer-btn"><?php echo $this->lang->line('select_video'); ?></a>
								<a class="videoplayer-cancel videoplayer-btn"><?php echo $this->lang->line('cancel'); ?></a>
								<div class="clear"></div>
							</div>
						</div>

					</div>

			</div>
		</div>
	</div>

	<!-- /.videoplayer-preview -->

</div>

<!-- /.videoplayer-box -->