<?php include("inc/head.php"); ?>
<body class="info-page one-col">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<aside>
						<img src="images/404.jpg" alt="404 error"  class="rounded-corners gradient-border">
					</aside>

					<article>
						<h1 class="uppercase">Uh Oh! Page not found.</h1>
						<p>Nulla mattis congue dolor, eu mattis nisl eleifend non. Phasellus volutpat cursus felis at vulputate. Duis orci libero, interdum eget varius fermentum, fermentum non libero. Proin sit amet est ipsum, vel dictum purus. Curabitur gravida commodo vulputate. Pellentesque vel velit vel magna ultricies faucibus ut quis lectus. Suspendisse in nunc turpis. Etiam tristique rutrum vestibulum. Etiam erat odio, porta in placerat nec, faucibus vitae lectus.</p>
					</article>

				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	
</body>
</html>