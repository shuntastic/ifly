<?php include("inc/head.php"); ?>
<body>

	<div id="skyline"></div>

	
	<div id="parallax-wrapper">
		<div id="parallax">
			<a href="http://www.adobe.com/go/getflash">
            	<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
        	</a>
		</div>
	</div>
	<!---->

	<div id="wrapper">

		<?php include("inc/header.php"); ?>
		
		<?php include("inc/hero.php"); ?>

	</div><!-- /#wrapper -->

	<div id="main" role="main">

		<div class="content-block box-section">

			<div class="box-holder">

				<?php include("inc/flight-wizard.php"); ?>

				<?php include("inc/reviews.php"); ?>				

			</div><!-- /.box-holder -->

		</div><!-- /.box-section -->

		<div class="content-block metal-bg">

			<?php include("inc/tiles.php"); ?>			

			<?php include("inc/gallery.php"); ?>

			<div class="btn-block">
				<a href="#" class="btn red large uppercase"><em></em><span>Already flown, find YOUR personal flight photos &amp; videos</span></a>
			</div><!-- /.btn-block -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->


	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>