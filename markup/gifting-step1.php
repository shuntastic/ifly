<?php include("inc/head.php"); ?>
<body class="info-page one-col booking gifting step1">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator-gifting-redeem.php"); ?>	

	<div id="main" class="gifting step1" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next" href="#">&gt;</a>

				<h1 class='underline'>Redeem a Gift Card</h1>
					<h2 class="note">You're just a few clicks away from booking your flight!<br />
                    Enter your 16-digital gift card number.<br />See example below for where the number is located.</h2>

				<form id="redeem" class="uniform">
					<input type="text" class="text" name="gift-code" value="ENTER YOUR 16 DIGIT GIFT CODE">
					<input type="submit" value="Purchase Gift Card" class="submit green">
				</form>

				<div id="redeem-examples">
					<p>Examples</p>
					<img src="/images/gifting-redeem-example.png">
				</div>

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.redeemGiftCard();
		});
    </script>
</body>
</html>