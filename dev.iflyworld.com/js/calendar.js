var calData = [];
					

var calendarWidget = {
	callDaySelect: function(dateText) {
		//console.log('calDaySelect:',dateText);
		var date,
			selectedDate = new Date(dateText),
			i = 0,
			event = null;
	
		/* Determine if the user clicked an event: */
		while (i < calData.length && !event) {
			date = calData[i].date;
	
			if (selectedDate.valueOf() === date.valueOf()) {
				event = calData[i];
			}
			i++;
		}
		if (event) {
			var eventContent = '<a class="closeButton" href="#">close</a><h2>'+calendarWidget.fullDate(event.date)+'<br />'+event.title+'</h2>'+truncate(event.description,50,'...<p><a href="'+event.url+'">MORE INFO</a></p>');
			$('div.event-overlay').html(eventContent).focus();
				$('div.event-overlay').fadeIn(300, function(){				 
					$('div.event-overlay a.closeButton').click(function(){
						$('div.event-overlay').fadeOut(100, function() {$(this).html('')});
					}); 
			});
	
	
		}
	},
	beforeShowDay: function(date) {
		var result = [true, '', null];
		var matching = $.grep(calData, function(event) {
			return event.date.valueOf() === date.valueOf();
		});
	
		if (matching.length) {
			result = [true, 'highlight', null];
		}
		return result;
	},	
	
	init: function() {
		if($('#calendar-widget').length > 0)
		$('#calendar-widget').datepicker({
			dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
			buttonImageOnly:true,
			onSelect: calendarWidget.callDaySelect,
			beforeShowDay:calendarWidget.beforeShowDay
		});
	},
	fullDate: function(dateObj) {
		calendar = dateObj;
		day = calendar.getDay();
		month = calendar.getMonth();
		date = calendar.getDate();
		year = calendar.getYear();
		if (year < 1000)
		year+=1900
		cent = parseInt(year/100);
		g = year % 19;
		k = parseInt((cent - 17)/25);
		i = (cent - parseInt(cent/4) - parseInt((cent - k)/3) + 19*g + 15) % 30;
		i = i - parseInt(i/28)*(1 - parseInt(i/28)*parseInt(29/(i+1))*parseInt((21-g)/11));
		j = (year + parseInt(year/4) + i + 2 - cent + parseInt(cent/4)) % 7;
		l = i - j;
		emonth = 3 + parseInt((l + 40)/44);
		edate = l + 28 - 31*parseInt((emonth/4));
		emonth--;
		var dayname = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
		var monthname = new Array ("January","February","March","April","May","June","July","August","September","October","November","December" );
//		var fullD = dayname[day] + ", "+monthname[month] + " ";
		var fullD = monthname[month] + " ";
		if (date< 10)
			fullD+="0" + date + ", "
		else fullD+=date + ", ";
		fullD+=year
		
		return fullD;
	}
};
function truncate (text, limit, append) {
    if (typeof text !== 'string')
        return '';
    if (typeof append == 'undefined')
        append = '...';
    var parts = text.split(' ');
    if (parts.length > limit) {
        // loop backward through the string
        for (var i = parts.length - 1; i > -1; --i) {
            // if i is over limit, drop this word from the array
            if (i+1 > limit) {
                parts.length = i;
            }
        }
        // add the truncate append text
        parts.push(append);
    }
    // join the array back into a string
    return parts.join(' ');
}

$(document).ready(function(){
	calendarWidget.init();
	
});

