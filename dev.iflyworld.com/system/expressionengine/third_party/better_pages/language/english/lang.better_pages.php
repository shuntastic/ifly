<?php
$lang = array(

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
//   F I E L D T Y P E
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
"label_url"					=> "Add custom URL",
"label_template"			=> "Select a template",
"label_other_templates"		=> "Other templates",

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
//	C O N F I G
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
"license_key"				=> "License Key",
"thumb_upload_directory"	=> "Thumbnail Upload Directory",
"thumb"						=> "Thumbnail",		
"thumbs"					=> "Thumbnails",
"thumb-instructions"		=> "Uploaded thumbnails should be 200px wide and of equal height.",
"upload"					=> "Upload",
"remove"					=> "Remove",
"include"					=> "Include",
"template_name"				=> "Template Name",
"add_thumb"					=> "Add Thumbnail",
"update_thumb"				=> "Update Thumbnail",
"hide_fields"				=> "Hide Selected Fields",
"field_hiding_method"		=> "Method for Hiding Fields",
"field_hiding_remove"		=> "Hide Field Entirely",
"field_hiding_collapse"		=> "Collapse Fields",

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
//	E R R O R S
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
"err_upload_directory"		=> "Please specify an upload directory",
"err_custom_url"			=> "Please choose a custom URL",

//
//
''=>''
);