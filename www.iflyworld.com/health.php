<?php
include("db/db.php");

$query = "select * from kiosk_status  ";

$data = "";
$rows = $db->getRowList("SELECT * FROM social_kiosk.kiosk_status");
foreach ($rows as $row){

	$kiosk_info = "";
	$kiosk_info_array = explode(",",$row->kiosk_data);
	foreach ($kiosk_info_array as $k=>$v){
		$kiosk_info .= str_replace("beacon","",str_replace(".txt","",$v)) . "<br>";
	}

if ($row->wowza_status == "INACTIVE") $wowza_status = "#FF0000"; else  $wowza_status = "#006600"; 
if ($row->capture_status == "INACTIVE") $capture_status = "#FF0000"; else  $capture_status = "#006600"; 
if ($row->kiosk_status == "INACTIVE") $kiosk_status = "#FF0000"; else  $kiosk_status = "#006600"; 


//003300

$data .= "
<tr >
    <td style='border-bottom:solid 1px #efefef'><span class='style6'>" . $row->tid . "</span></td>
    <td style='border-bottom:solid 1px #efefef'><span class='style6'>" . $row->youtube_date . "</span></td>
    <td style='border-bottom:solid 1px #efefef'><span class='style6'><strong><span style='color:$wowza_status'>" . $row->wowza_status . "</span></strong><br>" . $row->wowza_date  . "</span></td>
    <td style='border-bottom:solid 1px #efefef'><span class='style6'><strong><span style='color:$capture_status'>" . $row->capture_status . "</span></strong><br>" . $row->capture_date  . "</span></td>
    <td style='border-bottom:solid 1px #efefef'><span class='style6'><strong><span style='color:$kiosk_status'>" . $row->kiosk_status . "</span></strong><br>" . $row->kiosk_date  . "</span></td>
    <td style='border-bottom:solid 1px #efefef'><span class='style6'>" . $kiosk_info . "</span></td>
  </tr>

";

}


//print_r($rows);

?>

<style type="text/css">
<!--
.style13 {font-family: Verdana, Arial, Helvetica, sans-serif; color: #333333; font-size: 12px; }
.style15 {font-family: Verdana, Arial, Helvetica, sans-serif; color: #FFFFFF; }
-->
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Tunnel </span></td>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Updated</span></td>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Wowza Status</span></td>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Video  Status</span></td>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Kiosk Status</span></td>
    <td nowrap="nowrap" bgcolor="#0066CC"><span class="style15">Kiosk Client info</span></td>
  </tr>
  <tr>
    <td valign="top" bgcolor="#efefefef"><span class="style13">ID of the Tunnel</span></td>
    <td valign="top" bgcolor="#efefefef"><span class="style13">The last time that the server received an update from the social server. This indicates that the youtube uploader is working. (server time)</span></td>
    <td valign="top" bgcolor="#efefefef"><span class="style13">The last time wowza created a log entry, indicating a testimonial was recorded. ACTIVE indicates a log entry was created today. (local time)</span></td>
    <td valign="top" bgcolor="#efefefef"><span class="style13">The last time the video capture service created a log entry. ACTIVE indicates a log entry was created today. (local time)</span></td>
    <td valign="top" bgcolor="#efefefef"><span class="style13">The last time a kiosk communicated with the social server. ACTIVE indicates a log entry was created today. (local time)</span></td>
    <td valign="top" bgcolor="#efefefef"><span class="style13">The IP address and last communication time for each kiosk. (local time)</span></td>
  </tr>
  <?php echo $data; ?>
</table>
