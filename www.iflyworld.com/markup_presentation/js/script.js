/* 
	Author: Alex Hobday (alex@e-digitalgroup.com)
*/

IFLY = {

    init : function () {

    	var self = this;

    	self.hasFlash();
    	self.initUniform();

    	self.initHome();

        if($('#parallax').length > 0){
            IFLY.parallax.init();
        }
         
        if($('#yt-player').length > 0){
            IFLY.youtube.getID("yt-player");            
        }   
        if($('form.validate').length > 0){
            $('form.validate').validate();
        }   

    },

    initHome : function () {

    	var self = this;

    	self.initCarousel();
		self.initCalendarButton();
		
    	self.initSlider();

    	$(".popup-block a").on("click", function(e){
    		e.preventDefault();
    		$(".popup-block .drop-list").toggle();
    	});

        $(".gmask").delegate("a", "click", function (e) {
            e.preventDefault();
            $(this).colorbox();
        });
    	
    },
	
	initCalendarButton: function() {
		if($('#calendar-quickbook').length > 0)
		$('#calendar-quickbook').datepicker({
			dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
			buttonImageOnly:true,
			onSelect: function(e) {
				//console.log('e: '+e);
				$('#book-date span').html(e)
				$('#calendar-quickbook').css('display','none');	
			}
		});
		$('#book-date').click(function() {
				$('#calendar-quickbook').css('display','block');
				//beforeShowDay:calendarWidget.beforeShowDay
			});
		
	},

    initCarousel : function () {

    	$(".gmask").jCarouselLite({
        	btnNext : ".gholder .btn-next",
        	btnPrev : ".gholder .btn-prev",
        	visible : 5,
        	scroll  : 5
    	});	

    },

    initSlider : function () {

        var ytID;
        var current_slide;
        var video_modal;
        var self = this;
        var skyline     = $("#skyline");

    	$('#slider').anythingSlider({
    		theme 			: "default",
  			mode   			: "horiz", 
  			buildArrows		: false,
  			buildNavigation	: false,
  			buildStartStop	: false,
  			addWmodeToObject: 'transparent'
    	});

    	$('.menu').delegate("a", "click", function(e) {
    		var slide = $(this).attr('href').substring(1);
    		$('#slider').anythingSlider(slide);
    		e.preventDefault();
    		var parent = $(this).parent();
    		parent.siblings().removeClass("active");
    		parent.addClass('active');
  		});

        $("#slider").delegate("div.video a", "click", function(e) {
            e.preventDefault();
            
            self.current_slide = $(this).parents(".video-block");
            self.current_slide.toggle();

            self.video_modal = $("div#video-modal");
            self.video_modal.toggle();
			
			IFLY.parallax.pause();
//          $("#skyline").css({opacity: 0.25});
            self.ytID   = $(this).attr("href").replace('#','');
            IFLY.youtube.init("yt-player", self.ytID);

        });

        $("#video-modal").delegate("a.btn", "click", function(e) {
            e.preventDefault();
			IFLY.parallax.trackMouse(true);            
//          $("#skyline").css({opacity: 1});
            self.current_slide.toggle();
            self.video_modal.toggle();
            IFLY.youtube.stop("yt-player");
        });

    },

    hasFlash : function (){

		if(swfobject.hasFlashPlayerVersion("1")) {
			$('html').addClass('hasflash');
		}else{
			$('html').addClass('noflash');
		}

	},

	initUniform : function () {
		$("select").uniform();
	}
};

IFLY.parallax = {
    init: function() {
		if($('html').hasClass('hasflash')){
			$('#skyline').css({background:'#020a3d',display:'none'});
			var flashvars = {};
			var params = {wmode: "transparent",quality:"high",allowscriptaccess:"always",allownetworking:"all"};
			var attributes = {swliveconnect:"true"};
			swfobject.embedSWF("swf/parallax.swf", "parallax", "1370", "550", "10.0.0","expressInstall.swf", flashvars, params, attributes,IFLY.parallax.trackMouse);
		}
    },
    trackMouse: function(pass) {
        if (pass){
            $("#skyline").css({display:'none'}).css({opacity : 0});    
        }
        
        $('#wrapper').mousemove(function(evt) {
            var offset = $(this).offset();
            var xPos = evt.pageX - offset.left;
            var paraObj = swfobject.getObjectById("parallax");
            paraObj.onHMove(xPos);
        });
    },
	pause: function() {
		$("#skyline").css({display:'block'}).animate({opacity :.8},200);    
		$('#wrapper').unbind('mousemove');
	}
};

IFLY.youtube = {

    init : function (id, ytid) {
        $("#"+id).tubeplayer({
            width   : 600, // the width of the player
            height  : 350, // the height of the player
            iframed : true,
            allowFullScreen : "true", // true by default, allow user to go full screen
            initialVideo    : ytid, // the video that is loaded into the player
            preferredQuality: "default",// preferred quality: default, small, medium, large, hd720
            onPlay: function(id){}, // after the play method is called
            onPause: function(){}, // after the pause method is called
            onStop: function(){}, // after the player is stopped
            onSeek: function(time){}, // after the video has been seeked to a defined point
            onMute: function(){}, // after the player is muted
            onUnMute: function(){} // after the player is unmuted
        });       
    },

    stop : function (id) {
        $("#"+id).tubeplayer("stop");
    },

    destroy : function () {
        $("#"+id).tubeplayer("destroy");
    },

    getID : function (elem) {
        var data = $("#"+elem).data("ytid");
        if ( data != undefined ) {
            IFLY.youtube.init(elem, $("#"+elem).data("ytid"));
        }
    },

};

$(document).ready(function() {
	IFLY.init(); 
});
