<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_lib {

	var $services = array();

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{

		$this->EE =& get_instance();


		// Video Player cache path

		$this->_cache_path = APPPATH.'cache/videoplayer/';


		// create cache folder if it doesn't exist

		if(!file_exists($this->_cache_path))
		{
			@mkdir($this->_cache_path, 0777);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Get video services
	 *
	 * @access	private
	 * @param	string
	 * @return	string
	 */
	
	public function get_services($site_id)
	{
		$this->EE->load->helper('directory');

		$services_path = PATH_THIRD.'videoplayer/libraries/services/';

		$services_map = directory_map($services_path, 1);

		$services = array();


		foreach($services_map as $service)
		{
			// remove .php extension

			$service = substr($service, 0, -4);

			$class_name = "Videoplayer_".$service;

			if(!class_exists($class_name))
			{
				include_once(PATH_THIRD."videoplayer/libraries/services/".$service.".php");
			}

			$params = array('site_id' => $site_id);
			
			$OBJ = new $class_name($params);

			$services[$service] = $OBJ;
			$services[$service]->site_id = $site_id;
		}

		return $services;
	}

	// --------------------------------------------------------------------

	/**
	 * gets the video from given options
	 *
	 * @access	public
	 * @param	array
	 * @return	array
	 */
	function get_video($video_opts = array())
	{
		$site_id = $this->EE->config->item('site_id');
		
		if($this->EE->input->get_post('site_id'))
		{
			$site_id = $this->EE->input->get_post('site_id');
		}
		
		$services = $this->get_services($site_id);
		
		// set default video

		$video_id = false;

		$video = false;

		foreach($services as $service_key => $service)
		{
			if(method_exists($service, "get_video_id"))
			{
				// get default values

				$video = $service->video;

				if(isset($video_opts['default_size']))
				{
					if($video_opts['default_size'] === "no" || $video_opts['default_size'] === false || $video_opts['default_size'] === 0)
					{					
						unset($video['width']);
						unset($video['height']);	
					}
				}

				$video = array_merge($video,$video_opts);
				

				$video_id = $service->get_video_id($video_opts['url']);

				$video['service_key'] = $service_key;
				$video['service_name'] 			= $service->service_name;
				
				if($video_id)
				{
					$video['id'] = $video_id;


					// load embeds

					$video = $service->load_embed($video);


					// load metadata
					
					$video = $service->metadata($video);

					$video['video_found'] = true;
					
					break;
				}
			}
		}

		return $video;
	}

	// --------------------------------------------------------------------

	/**
	 * Get favorites for a specific service, with pagination support
	 *
	 * @access	public
	 * @param	string
	 * @param	integer
	 * @param	integer
	 * @return	void
	 */
	function get_favorites($service_key, $page, $per_page)
	{
		$this->EE->load->model('videoplayer_model');

		$site_id = $this->EE->input->get_post('site_id');

		$services = $this->get_services($site_id);

		$service = $services[$service_key];

		$accounts = $this->EE->videoplayer_model->get_accounts($site_id);

		$user = false;

		if(isset($accounts[$service_key]['username']))
		{
			$user = $accounts[$service_key]['username'];
		}


		$password = false;

		if(isset($accounts[$service_key]['password']))
		{
			$password = $accounts[$service_key]['password'];
		}



		if(method_exists($service, 'get_favorites'))
		{
			return $service->get_favorites($page, $per_page);
		}

		return array();
	}

	// --------------------------------------------------------------------

	/**
	 * Get favorites for a specific service
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function get_videos($service_key)
	{
		$this->EE->load->model('videoplayer_model');

		$site_id = $this->EE->input->get_post('site_id');
		$services = $this->get_services($site_id);


		$service = $services[$service_key];

		$accounts = $this->EE->videoplayer_model->get_accounts();

		$user = false;

		if(isset($accounts[$service_key]['username']))
		{
			$user = $accounts[$service_key]['username'];
		}

		$password = false;

		if(isset($accounts[$service_key]['password']))
		{
			$password = $accounts[$service_key]['password'];
		}

		if(method_exists($service, 'get_videos'))
		{
			if($user && !$password)
			{
				return $service->get_videos($user);
			} elseif($user && $password){
				return $service->get_videos($user, $password);
			} else {
				return $service->get_videos();
			}

		}

		return array();
	}

	// --------------------------------------------------------------------

	/**
	 * Check if video exists from the video page header status
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	function video_exists($url)
	{
		// set video URL in the parameters

		$params['url'] = $url;


		// get the video with given parameters

		$this->video = $this->get_video($params);
		$video = $this->video;

		if($video['url'])
		{
			$video_headers = get_headers($video['url']);

			if((strpos($video_headers[0], "200") !== false))
			{
				return true;
			}
		}

		return false;
	}

	// --------------------------------------------------------------------

	/**
	 * Format duration from seconds to 00:00
	 *
	 * @access	private
	 * @param	integer
	 * @return	string
	 */

	function format_duration($duration_seconds)
	{
		$temp_minutes = (floor($duration_seconds/60));
		$temp_seconds = ($duration_seconds - (60 * floor($duration_seconds/60)));

		if($temp_seconds < 10)
		{
			$temp_seconds = "0".$temp_seconds;
		}

		$r = $temp_minutes.":".$temp_seconds;

		return $r;
	}
}

// END Videoplayer_lib class

/* End of file videoplayer_lib.php */
/* Location: ./system/expressionengine/third_party/videoplayer/libraries/videoplayer_lib.php */