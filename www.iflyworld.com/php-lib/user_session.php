<?php 
	// this starts the session 
	session_start(); 
	
	if(isset($_POST["token"]) && !isset($_SESSION['token']))
		$_SESSION['token'] = $_POST["token"];

	if(isset($_POST["guest_no"]) && !isset($_SESSION['guest_no']))
		$_SESSION['guest_no'] = $_POST["guest_no"];

	if(isset($_POST["user_balance"]) && !isset($_SESSION['user_balance']))
		$_SESSION['user_balance'] = $_POST["user_balance"];

	if(isset($_POST["user_tunnel"]) && !isset($_SESSION['user_tunnel']))
		$_SESSION['user_tunnel'] = $_POST["user_tunnel"];

	//Print_r($_SESSION);
	$result = json_encode($_SESSION, TRUE);

echo $result;

?> 