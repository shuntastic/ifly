<?php include("inc/head.php"); ?>
<body class="info-page one-col ratings">
	
	<?php include("inc/header.php"); ?>

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<section class=" clearfix">

					<h1 class="uppercase">Ratings &amp; Reviews</h1>
					<div class="note">
						<h2>Read what our customers have to say about their experience at iFLY,<br />or write a review yourself.</h2>
					</div>
					

					<div id="ratings-dist">
						<span class="star-rating">X-star review</span> <span class="average"></span>
						<h2>Rating Distribution</h2>
						<div class="progress-bar prog5">
							5 stars <span><em></em></span>
						</div>
						<div class="progress-bar prog4">
							4 stars <span><em ></em></span>
						</div>
						<div class="progress-bar prog3">
							3 stars <span><em></em></span>
						</div>
						<div class="progress-bar prog2">
							2 stars <span><em></em></span>
						</div>
						<div class="progress-bar prog1">
							1 stars <span><em></em></span>
						</div>
						<p></p>

					</div>

					<div id="sort-pagination" class="clearfix">
						<div class="pagination">
							<div>
								<a href="#" class="prev ir" title="Previous Page">&lt;</a>
							</div>
							<div class="pages">
								<span class="active">1</span> of <span class="total">4</span>
							</div>
							<div>
								<a href="#" class="next ir" title="Next Page">&gt;</a>
							</div>
						</div>

						<div class="sort">
							<span>Sort by:</span>
							<form class="uniform">
								<select id="sort-recent">
									<option value="desc">MOST RECENT</option>
									<option value="asc">OLDEST</option>
								</select>
								<select id="sort-all">
									<option value="all">ALL</option>
									<option value="5">5-STAR</option>
									<option value="4">4-STAR</option>
									<option value="3">3-STAR</option>
									<option value="2">2-STAR</option>
									<option value="1">1-STAR</option>
								</select>
							</form>
						</div><!-- /.sort -->

					</div><!-- /#sort-pagination -->
					<div id="reviews">
                    
                    </div>
					<p><a href="#" class="btn more"><em></em><span>MORE</span></a></p>
				</section>

			</div><!-- /.
		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>

	<script type="text/javascript" src="js/ratingsreviews.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : 'ifo';
		});

	</script>
</body>
</html>