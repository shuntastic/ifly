<div id="better-pages">
	<div class="instruction_text">
		<p><strong><?= lang('label_url') ?></strong></p>
	</div>
	<?= $bpfield ?>
	
	<fieldset class="holder">
		<input id="better_pages_url" type="text" maxlength="128" dir="ltr" value="<?= $pages["uri"] ?>" name="better_pages_url">
	</fieldset>
	<div id="tmp-choices">
		<div class="instruction_text">
			<p><strong><?= lang('label_template') ?></strong></p>
		</div>
		<ul>
		<?php foreach ($templatesThumbs as $tt){ ?>
			<li class="better_pages_thumb<?php if ($tt["template_id"] == $pages["template"]){ ?> selected<?php } ?>" id="better_pages_<?= $tt["template_id"] ?>">
				<a href="javascript:;">
					<img src="<?= $tt["thumb"] ?>" alt="<?= $tt["group_name"] ?> | <?= $tt["template_name"] ?>" />
					<span class="tmp-choose">Select<span class="plus">+</span></span>
					<span class="tmp-name"><?= $tt["group_name"] ?> &raquo; <?= $tt["template_name"] ?></span>
					<span class="tmp-selected">I'm selected</span>
					
				</a>
			</li>
		<?php } ?>
		<?php if (sizeof($templatesNoThumbs) > 0){ ?>
			<li>
				<?php if (sizeof($templatesThumbs) > 0){ ?>
				<div class="instruction_text">
					<p><strong><?= lang('label_other_templates') ?></strong></p>
				</div>
				<?php } ?>
				<select name="better_pages_other_templates" class="better_pages_other_templates">
					<option value="0">More Templates</option>
					<?php foreach ($templatesNoThumbs as $tnt){ ?>
				  	<option value="<?= $tnt["template_id"] ?>"<?php if ($tnt["template_id"] == $pages["template"]){ ?> selected="selected"<?php } ?>><?= $tnt["group_name"] ?> &raquo; <?= $tnt["template_name"] ?></option>
					<?php } ?>
				</select>
				
			</li>
		<?php } ?>
		</ul>
	</div>
</div>