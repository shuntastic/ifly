<?php
/*
_|                                                      _|
_|_|_|     _|_|     _|_|   _|    _|   _|_|_| _|_|_|   _|_|_|_|
_|    _| _|    _| _|    _| _|    _| _|    _| _|    _|   _|
_|    _| _|    _| _|    _| _|    _| _|    _| _|    _|   _|
_|_|_|     _|_|     _|_|     _|_|_|   _|_|_| _|    _|     _|_|
                                 _|
                             _|_|

Description:		Better Pages Fieldtype for Expression Engine 2.0
Developer:			Booyant, Inc.
Website:			www.booyant.com/better_pages
Location:			./system/expressionengine/third_party/better_pages/ft.navee.php
Contact:			its.go.time@booyant.com  / 978.OKAY.BOB

*/


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Include config file
require_once PATH_THIRD.'better_pages/config'.EXT;

class Better_pages_ft extends EE_Fieldtype {
	
	var $info = array(
		'name'		=> 'Better Pages',
		'version'	=> BP_VERSION
	);
	
	function Better_pages_ft()
	{
		parent::EE_Fieldtype();
		$this->EE->load->model('tools_model');
	}
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	D I S P L A Y   F I E L D
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function display_field($data){
		$vars 						= array();
		$group_id					= 0;
		$js 						= "";
		$existing_entry				= false;
		$field_hiding_method		= "remove";
		
		
		if (array_key_exists("group_id", $this->settings)){
			$group_id 				= $this->settings["group_id"];
		}
		
		if (array_key_exists("field_hiding_method", $this->settings)){
			$field_hiding_method 	= $this->settings["field_hiding_method"];
		}
		
		if (!($group_id > 0)){
			$group_id				= $this->_getGroupId($_GET["channel_id"]);
		}
		
		$vars["templatesThumbs"]	= $this->_getGroupTemplateThumbArray($group_id);
		$vars["templatesNoThumbs"]	= $this->_getGroupTemplateNoThumbArray($group_id);
		$vars["pages"]["template"] 	= "";
		$vars["pages"]["uri"]		= "/example/pages/uri";
		$vars["bpfield"]			= form_input($this->field_name, '', 'id="'.$this->field_name.'" class="bpSubmissionField"');
		$pages						= $this->EE->config->item('site_pages');
		$selectedFields				= $this->_getSelectedGroupFieldsArray($group_id);
		
		if (
				isset($_GET["entry_id"]) && 
				array_key_exists($_GET["entry_id"], $pages[$this->EE->config->item('site_id')]["templates"]) && 
				array_key_exists($_GET["entry_id"], $pages[$this->EE->config->item('site_id')]["uris"])
		){	
			$vars["pages"]["template"]	= $pages[$this->EE->config->item('site_id')]["templates"][$_GET["entry_id"]];
			$vars["pages"]["uri"]		= $pages[$this->EE->config->item('site_id')]["uris"][$_GET["entry_id"]];
			$vars["bpfield"]			= form_input($this->field_name, $pages[$this->EE->config->item('site_id')]["templates"][$_GET["entry_id"]], 'id="'.$this->field_name.'" class="bpSubmissionField"');
			$existing_entry				= true; 
		}
			
		// JS | CSS | Language File
			$this->_betterPagesJS();
			$this->_betterPagesCSS();
			$this->EE->lang->loadfile('better_pages');
			
			// Let's build the dynamic JS
			$js .= "function array_key_exists (key, search) {
   		 				if (!search || (search.constructor !== Array && search.constructor !== Object)) {
        					return false;
    					}
 
    					return key in search;
					}
					
					function oc(a){
					  var o = {};
					  for(var i=0;i<a.length;i++)
					  {
					    o[a[i]]='';
					  }
					  return o;
					}
					
					bpHidden = new Array();
					
					";
			
			
			foreach ($selectedFields as $k=>$v){
				$js .= "bpHidden[".$k."] = new Array();
				
				";
				foreach($v as $k1=>$v1){
					$js .= "bpHidden['".$k."'][".$k1."] = '".$v1."';\n";
				}
			}
			
			
			$js .= "
				function bpHideFields(method,t,container){
					
					var bpExemptFields 	= new Array('hold_field_url_title','hold_field_title','hold_field_entry_date', 'hold_field_expiration_date', 'hold_field_comment_expiration_date', 'hold_field_category', 'hold_field_new_channel', 'hold_field_status',  'hold_field_author', 'hold_field_options', 'hold_field_ping', 'hold_field_pages__pages_uri', 'hold_field_pages__pages_template_id', 'hold_field_pages_uri', 'hold_field_pages_template_id');
					
					var bpExemptFieldsString	= '';
					
					for(i=0;i<bpExemptFields.length;i++) {
						if (i > 0) {
							bpExemptFieldsString += ', ';
						}
        				bpExemptFieldsString += '#'+bpExemptFields[i];
  					}
  					
  					
					
					/////////////////////////////////////////////////////////////
					// Check to see if the template clicked has fields to hide
					////////////////////////////////////////////////////////////
					
					if (array_key_exists(t, bpHidden)) {
					
						///////////////////////////////////////////////////////////////////
						// Loop through each publish field and see if it should be hidden
						//////////////////////////////////////////////////////////////////
						
						// Fields on the current tab
						
						$('#'+container+' .publish_field').each(function(){
							id = $(this).attr('id').split('_');
							if (!(id[2] in oc(bpHidden[t])) && !($(this).attr('id') in oc(bpExemptFields))){
								if (method == 'collapse'){
									$(this).children('div').children('fieldset').parent('div').slideDown(333);
								} else {
									$(this).slideDown(333);
								}
							}
						});
					
						for (x in bpHidden[t]){
							if (method == 'collapse'){
								$('#'+container+' #sub_hold_field_'+bpHidden[t][x]).slideUp(333);
							} else {
								$('#'+container+' #hold_field_'+bpHidden[t][x]).slideUp(333);
							}
						}
						
						// Fields in other tabs.
						
						$('.publish_field').parent('div').not('#'+container).children('.publish_field').each(function(){
							id = $(this).attr('id').split('_');
							if (!(id[2] in oc(bpHidden[t])) && !($(this).attr('id') in oc(bpExemptFields))){
								if (method == 'collapse'){
									$(this).children('div').children('fieldset').parent('div').removeClass('bp-hidden');
								} else {
									$(this).removeClass('bp-hidden');
								}
							}
						});
						
						for (x in bpHidden[t]){
							if (method == 'collapse'){
								$('#sub_hold_field_'+bpHidden[t][x]).parent('.publish_field').parent('div').not('#'+container).children('.publish_field').children('#sub_hold_field_'+bpHidden[t][x]).addClass('bp-hidden');
							} else {
								$('#hold_field_'+bpHidden[t][x]).parent('div').not('#'+container).children('#hold_field_'+bpHidden[t][x]).addClass('bp-hidden');
							}
						}
						
					
					/////////////////////////////////////////////////////////////
					// Template has no hidden fields, show all
					////////////////////////////////////////////////////////////
					
					} else {
						if (method == 'collapse'){
							
							// Fields on the current tab
							
							$('#'+container+' .publish_field').not(bpExemptFieldsString).children('div').children('fieldset').parent('div').slideDown(333);
							
							// Fields in other tabs
							
							$('.publish_field').parent('div').not('#'+container).children('.publish_field').not(bpExemptFieldsString).children('div').children('fieldset').parent('div').removeClass('bp-hidden');
							
						} else {
						
							// Fields on the current tab
							
							$('#'+container+' .publish_field').not(bpExemptFieldsString).slideDown(333);
							
							// Fields on other tabs
							
							$('.publish_field').parent('div').not('#'+container).children('.publish_field').not(bpExemptFieldsString).removeClass('bp-hidden');
						}
					}
				}
			";
			
			$js .= "
					/////////////////////////////////////////////////
					//  User clicks a template thumb
					////////////////////////////////////////////////
					
					$('.better_pages_thumb a').click(function(){
						container	= $(this).parents('.publish_field').parent('div').attr('id');
						t 			= $(this).parent('li').attr('id').split('_');
						bpHideFields('".$field_hiding_method."', t[2], container);
					});
					
					/////////////////////////////////////////////////
					//  User selects a template from dropdown
					////////////////////////////////////////////////
					
					$('.better_pages_other_templates').change(function(){		
						container	= $(this).parents('.publish_field').parent('div').attr('id');			
						t 			= $(this).val();
						bpHideFields('".$field_hiding_method."',t, container);
						
					});
					
					bpTabName = $('#better-pages').parents('.publish_field').parent('div').attr('id');
				";
		
			
			if ($existing_entry) {
				if ($field_hiding_method == "collapse"){
					$js .= "
					
					
					for (x in bpHidden[".$vars["pages"]["template"]."]){
								
								// Fields in current tab
								$('#'+bpTabName+' #sub_hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).hide();
								
								// Fields on other tabs
								$('#sub_hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).parent('.publish_field').parent('div').not('#'+bpTabName).children('div').children('#sub_hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).addClass('bp-hidden');
							}";
				} else {
					$js .= "for (x in bpHidden[".$vars["pages"]["template"]."]){
								// Fields in current tab
								$('#'+bpTabName+' #hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).hide();
								
								// Fields on other tabs
								$('#hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).parent('div').not('#'+bpTabName).children('#hold_field_'+bpHidden[".$vars["pages"]["template"]."][x]).addClass('bp-hidden');
								
							}";
				}
			}

			$this->EE->javascript->output(array($js));
			$this->EE->javascript->compile();
				
		return $this->EE->load->view('/index', $vars, TRUE);		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	S A V E
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function save($data){
		return $data;
	}
	

	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	V A L I D A T E
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function validate($data){
	
		// Help me, helpers
		$this->EE->lang->loadfile('better_pages');
		
		if ($this->settings['field_required'] == 'y'){		
			if ($_POST["better_pages_url"] == "/example/pages/uri" || strlen(trim($_POST["better_pages_url"])) == 0){
				return lang('err_custom_url');
			}
		}
		
		return true;	
	}
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	D I S P L A Y   G L O B A L    S E T T I N G S
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function display_global_settings(){		
		
		// Help me, helpers
		$this->EE->lang->loadfile('better_pages');
		$this->EE->load->library('table');
		$this->_betterPagesJS('global_settings');
		
		// Let's declare some variables
		$returnMe 	= "";
		$settings	= array();
		$thumbs 	= $this->_getThumbArray();
		
		if (is_array($this->settings)){
			$settings = array_merge($this->settings, $_POST);
		} else {
			$settings["upload_directory"] 		= "";
			$settings["field_hiding_method"]	= "";
		}

		if (!isset($settings["upload_directory"])){
			$settings["upload_directory"] = "";
		}
	
		if (!isset($settings["field_hiding_method"])){
			$settings["field_hiding_method"] = "";
		}
		
		// Upload Directory
		$upload_directories	= $this->EE->tools_model->get_upload_preferences($this->EE->session->userdata('group_id'));
		
		foreach($upload_directories->result() as $row){
			$allowed_dirs[] = $row->id;
		}	
		
		$upload = $this->_getUploadDirectoryObject($allowed_dirs);
		if ($upload->num_rows() > 0){
			foreach ($upload->result() as $r){
				$options[$r->id] = $r->name;
			}
		}
		
		$this->EE->table->set_template(array(
			'table_open'    => '<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">',
				'row_start'     => '<tr class="even">',
				'row_alt_start' => '<tr class="odd">'
		));
		
		$this->EE->table->set_heading(array('data' => lang('thumb_upload_directory'), 'style' => 'width: 100%'));
		$this->EE->table->add_row(
			form_dropdown('upload_directory', $options, $settings["upload_directory"])
		);
		$returnMe .= $this->EE->table->generate();
		$this->EE->table->clear();	
		
		// Hidden Fields
		
		$options = array(
			'remove' 	=> lang('field_hiding_remove'), 
			'collapse' 	=> lang('field_hiding_collapse'), 
		);
		
		$this->EE->table->set_template(array(
			'table_open'    => '<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">',
				'row_start'     => '<tr class="even">',
				'row_alt_start' => '<tr class="odd">'
		));
		
		$this->EE->table->set_heading(array('data' => lang('field_hiding_method'), 'style' => 'width: 100%'));
		$this->EE->table->add_row(
			form_dropdown('field_hiding_method', $options, $settings["field_hiding_method"])
		);
		$returnMe .= $this->EE->table->generate();
		$this->EE->table->clear();
		
		// Templates
		
		$returnMe .= "<h1>".lang("thumbs")."</h1><p>".lang("thumb-instructions")."</p>";
		
		$templates	= $this->_getTemplateArray();

		if (sizeof($templates)>0){
			foreach ($templates as $k=>$v){	
				$returnMe .= "<h2>".$k."</h2>";
				
				$this->EE->table->set_template(array(
					'table_open'    => '<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">',
						'row_start'     => '<tr class="even">',
						'row_alt_start' => '<tr class="odd">'
				));
	
				$this->EE->table->set_heading(
					array('data' => lang('thumb'), 'style' => 'width: 200px'), 
					array('data' => lang('template_name'), 'style' => 'width: 200px'),
					lang('upload'), 
					array('data' => lang('remove'), 'style' => 'width: 10px')
				);
				
					foreach ($v as $k=>$v){
						$thumb 			= "";
						$uploadText		= lang("add_thumb");
						
						if (array_key_exists($v["id"], $thumbs)){
							$thumb 		= "<img style='width:200px' src='".$thumbs[$v["id"]]["url"].$thumbs[$v["id"]]["thumb"]."' />";
							$uploadText	= lang("update_thumb");
						}
						
						$uploadLink		= "<a href='javascript:;' class='bpUploadBtn' id='".$v["id"]."'>".$uploadText."</a>";
							
						$this->EE->table->add_row(
							$thumb,
							$v["name"],
							$uploadLink,
							form_checkbox('bpDelete_'.$v["id"], 'delete', FALSE)
						);
					}
				$returnMe .= $this->EE->table->generate();
				$this->EE->table->clear();	
			}
		}
		

		return $returnMe;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	S A V E   G L O B A L   S E T T I N G S
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	
	function save_global_settings(){
		
		$this->EE->lang->loadfile('better_pages');
			
		// Validation
		$errors = array();
		
		if (!$_POST["upload_directory"]){
			array_push($errors, lang('err_upload_directory')); 
		}
		
		if (sizeof($errors)>0){
			// error handling
		} else {
			// Lets do this thing.
			$uploadDir[0]	= $_POST["upload_directory"];
			$dir			= $this->_getUploadDirectoryObject($uploadDir);
			
			if ($dir->num_rows() > 0){
				$row = $dir->row();
				
				$config['upload_path'] = $row->server_path;
				$config['allowed_types'] = 'gif|jpg|png';
				
				$this->EE->load->library('upload', $config);
			}
			
			foreach ($_POST as $k=>$v){
				if (strpos($k,"bpThumb") !== false){
					
				} elseif (strpos($k,"bpDelete") !== false){
					$id = explode("_", $k);
					$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
					$this->EE->db->where("template_id", $id[1]);
					$this->EE->db->delete("better_pages_thumbs");
				} else {
					$settings[$k] = $v;
				}
			}
			
			foreach ($_FILES as $k=>$v){
				if (strpos($k,"bpThumb") !== false && $_FILES[$k]['size'] > 0){
					$this->EE->upload->do_upload($k);
					$id = explode("_", $k);
					$thumb = $this->EE->upload->data();
					
					// Delete any existing entries
					$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
					$this->EE->db->where("template_id", $id[1]);
					$this->EE->db->delete("better_pages_thumbs"); 
					
					// Insert new item
					$data = array(
						'site_id'			=> $this->EE->config->item('site_id'),
						'upload_dir_id' 	=> $_POST["upload_directory"],
						'template_id'		=> $id[1],
						'thumb'				=> $thumb["file_name"]
					);

					$this->EE->db->insert('better_pages_thumbs', $data);
				}
			}
			return $settings;
		}
		
	}
	
	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	D I S P L A Y   S E T T I N G S
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function display_settings($data){	
		
		// Help me, helpers
		$this->EE->lang->loadfile('better_pages');
		$this->EE->load->library('table');
		$this->_betterPagesJS('display_settings');
		$this->_betterPagesCSS('display_settings');
		
		// Let's declare some variables
		$returnMe 				= "";
		$selectedTemplates		= $this->_getSelectedTemplateArray($_GET["group_id"]);
		$thumbs 				= $this->_getThumbArray();
		$vars["groupFields"]	= $this->_getGroupFieldsObject($_GET["group_id"]);
		$vars["selectedFields"]	= $this->_getSelectedGroupFieldsArray($_GET["group_id"]);
			
		// Templates
		$templates	= $this->_getTemplateArray();
		if (sizeof($templates)>0){
			foreach ($templates as $k=>$v){	
				$returnMe .= "<h3>".$k."</h3>";
				$this->EE->table->set_template(array(
					'table_open'    => '<table class="mainTable padTable" border="0" cellspacing="0" cellpadding="0">',
						'row_start'     => '<tr class="even">',
						'row_alt_start' => '<tr class="odd">'
				));
				
				$this->EE->table->set_heading(
					array('data' => lang('thumb'), 'style' => 'width: 200px'),
					array('data' => lang('template_name'), 'style' => 'width: 200px'),
					lang('hide_fields'),
					array('data' => lang('include'), 'style' => 'width: 10px')
				);
				
					foreach ($v as $k=>$v){
						// Selected Templates
						if (in_array($v["id"], $selectedTemplates)){
							$templateChecked = TRUE;
						} else {
							$templateChecked = FALSE;
						}
						
						// Thumbs
						$thumb = "";
						
						if (array_key_exists($v["id"], $thumbs)){
							$thumb = "<img style='width:200px' src='".$thumbs[$v["id"]]["url"].$thumbs[$v["id"]]["thumb"]."' />";
						}
						
						// Hidden Fields
						$vars["template_id"]	= $v["id"];
						$vars["group_id"]		= $_GET["group_id"];
						$vars["show_fields"]	= $templateChecked;
						$hidden					= $this->EE->load->view('/hidden', $vars, TRUE);
						
						$includeData = array(
							'name'		=> 'bpTemplate_'.$v["id"],
							'value'		=> $v["id"],
							'checked'	=> $templateChecked,
							'class'		=> 'bpTemplate',
						
						);
						
						$this->EE->table->add_row(
							$thumb,
							$v["name"],
							$hidden,
							form_checkbox($includeData)
						);
					}
				$returnMe .= $this->EE->table->generate();
				$this->EE->table->clear();	
			}
		}
		

		return $returnMe;
		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	S A V E   S E T T I N G S
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function save_settings($data){		
		
		// Delete any existing entries in better_pages_templates
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("group_id", $_POST["group_id"]);
		$this->EE->db->delete("better_pages_templates"); 
		
		// Delete any existing entries in better_pages_hidden
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("group_id", $_POST["group_id"]);
		$this->EE->db->delete("better_pages_hidden"); 
				
		foreach ($_POST as $k=>$v){
				if (strpos($k,"bpTemplate") !== false){
					
					// Insert new Template item
					$data = array(
						'site_id'			=> $this->EE->config->item('site_id'),
						'group_id' 			=> $_POST["group_id"],
						'template_id'		=> $v
					);

					$this->EE->db->insert('better_pages_templates', $data);
				
				} elseif (strpos($k,"bpHidden") !== false){
					
					$bits	= explode("_", $v);
					
					// Insert new Hidden item
					$data = array(
						'site_id'			=> $this->EE->config->item('site_id'),
						'group_id' 			=> $_POST["group_id"],
						'template_id'		=> $bits[1],
						'field_id'			=> $bits[0]
					);

					$this->EE->db->insert('better_pages_hidden', $data);
				}
			}
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	I N S T A L L
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function install(){
		$this->EE->load->dbforge();
		$fields = array(
						'bp_id'				=> array('type' 		 => 'int',
													'constraint'	 => '10',
													'unsigned'		 => TRUE,
													'auto_increment' => TRUE),
						'site_id'			=> array('type'=>'int', 'constraint'=>'10'),
						'upload_dir_id'		=> array('type'=>'int', 'constraint'=>'10'),
						'template_id'		=> array('type'=>'int', 'constraint'=>'10'),
						'thumb'				=> array('type'=>'varchar', 'constraint'=>'255')		
						);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('bp_id', TRUE);
		$this->EE->dbforge->create_table('better_pages_thumbs', TRUE);	
		unset($fields);
		
		$fields = array(
						'bpTemplate_id'		=> array('type' 		 => 'int',
													'constraint'	 => '10',
													'unsigned'		 => TRUE,
													'auto_increment' => TRUE),
						'site_id'			=> array('type'=>'int', 'constraint'=>'10'),
						'group_id'			=> array('type'=>'int', 'constraint'=>'10'),
						'template_id'		=> array('type'=>'int', 'constraint'=>'10')	
						);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('bpTemplate_id', TRUE);
		$this->EE->dbforge->create_table('better_pages_templates', TRUE);	
		unset($fields);
		
		
		$fields = array(
						'bpHidden_id'		=> array('type' 		 => 'int',
													'constraint'	 => '10',
													'unsigned'		 => TRUE,
													'auto_increment' => TRUE),
						'site_id'			=> array('type'=>'int', 'constraint'=>'10'),
						'group_id'			=> array('type'=>'int', 'constraint'=>'10'),
						'template_id'		=> array('type'=>'int', 'constraint'=>'10'),
						'field_id'			=> array('type'=>'int', 'constraint'=>'10')	
						);

		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('bpHidden_id', TRUE);
		$this->EE->dbforge->create_table('better_pages_hidden', TRUE);	
		unset($fields);
		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	U N I N S T A L L
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
		
	function uninstall(){
		
		// If you want the uninstall function to completely wipe any Better Pages info
		// uncomment the remaining lines in this function.
		
		//$this->EE->load->dbforge();		
		//$this->EE->dbforge->drop_table('better_pages_thumbs');
		//$this->EE->dbforge->drop_table('better_pages_templates');
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	J A V A S C R I P T
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function _betterPagesJS($file=''){
		if (strlen($file)>0){
			$this->EE->cp->load_package_js($file);
		} else {
			$this->EE->cp->load_package_js('better_pages');
		}		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	C S S
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function _betterPagesCSS($file=''){
		switch ($file) {
    		case "display_settings":
				$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$this->EE->config->item('theme_folder_url').$this->_themeFolderDirectory().'better_pages/css/display_settings.css" />');
				break;
    		default:
      			$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$this->EE->config->item('theme_folder_url').$this->_themeFolderDirectory().'better_pages/css/better_pages.css" />');
		}	
		
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	T H E M E   F O L D E R   D I R E C T O R Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	
	function _themeFolderDirectory(){
		// Figure out if the theme is in the third_party folder or not
		$directory = "";
		
		if (is_dir($this->EE->config->item('theme_folder_path').'third_party/better_pages')){
			$directory = "third_party/";
		}
		
		return $directory;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   T E M P L A T E   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getTemplateArray(){
		$templates = array();
		$this->EE->db->select("t.template_name, t.template_id, tg.group_name");
		$this->EE->db->from("templates AS t");
		$this->EE->db->join("template_groups tg", "t.group_id=tg.group_id");
		$this->EE->db->where("t.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->order_by("tg.group_name, t.template_name");
		$q = $this->EE->db->get();
		
		$group = "";
		$i = 1;
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				if ($r->group_name !== $group){
					$group = $r->group_name;
					$i = 1;	
				}
				if ($r->template_name == "index"){
					$templates[$group][0]["id"] 	= $r->template_id;
					$templates[$group][0]["name"] 	= $r->template_name;
				} else {
					$templates[$group][$i]["id"] 	= $r->template_id;
					$templates[$group][$i]["name"] 	= $r->template_name;
					$i++;
				}
			}
		}
		
		$q->free_result();
		
		return $templates;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   T H U M B   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getThumbArray(){
		$thumbs = array();
		$this->EE->db->select("th.*, up.url");
		$this->EE->db->from("better_pages_thumbs AS th");
		$this->EE->db->join("upload_prefs AS up", "up.id = th.upload_dir_id", "LEFT OUTER");
		$this->EE->db->where("th.site_id", $this->EE->config->item('site_id'));
		$q = $this->EE->db->get();
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				$thumbs[$r->template_id]["bp_id"] 	= $r->bp_id;
				$thumbs[$r->template_id]["thumb"] 	= $r->thumb;
				$thumbs[$r->template_id]["url"] 	= $r->url;
			}
		}
		
		$q->free_result();
		return $thumbs;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   G R O U P   T E M P L A T E   T H U M B   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getGroupTemplateThumbArray($group_id){
		$t = array();
				
		$this->EE->db->select("bpt.*, bpth.thumb, up.url, t.template_name, tg.group_name");
		$this->EE->db->from("better_pages_templates as bpt");
		$this->EE->db->join("better_pages_thumbs as bpth", "bpth.template_id = bpt.template_id", "LEFT OUTER");
		$this->EE->db->join("templates as t", "t.template_id = bpt.template_id", "LEFT OUTER");
		$this->EE->db->join("template_groups as tg", "t.group_id = tg.group_id", "LEFT OUTER");
		$this->EE->db->join("upload_prefs AS up", "up.id = bpth.upload_dir_id", "LEFT OUTER");
		$this->EE->db->where("bpt.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("bpth.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("up.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("t.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("tg.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("bpt.group_id", $group_id);
		$this->EE->db->order_by("tg.group_name, t.template_name");
		
		$q = $this->EE->db->get();
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				$temp["template_id"]	= $r->template_id;
				$temp["thumb"]			= $r->url.$r->thumb;
				$temp["template_name"]	= $r->template_name;
				$temp["group_name"]		= $r->group_name;
				
				array_push($t,$temp);
			}
		}
		
		$q->free_result();
		return $t;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   G R O U P   T E M P L A T E   N O   T H U M B   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getGroupTemplateNoThumbArray($group_id){ 
		$t = array();
				
		$this->EE->db->select("bpt.*, bpth.thumb, t.template_name, tg.group_name");
		$this->EE->db->from("better_pages_templates as bpt");
		$this->EE->db->join("better_pages_thumbs as bpth", "bpth.template_id = bpt.template_id", "LEFT OUTER");
		$this->EE->db->join("templates as t", "t.template_id = bpt.template_id", "LEFT OUTER");
		$this->EE->db->join("template_groups as tg", "t.group_id = tg.group_id", "LEFT OUTER");
		$this->EE->db->where("bpt.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("t.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("tg.site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("bpt.group_id", $group_id);
		$this->EE->db->where("bpth.thumb", NULL);
		$this->EE->db->order_by("tg.group_name, t.template_name");
		
		$q = $this->EE->db->get();
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				$temp["template_id"]	= $r->template_id;
				$temp["template_name"]	= $r->template_name;
				$temp["group_name"]		= $r->group_name;
				
				array_push($t,$temp);
			}
		}
		
		$q->free_result();
		return $t;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   S E L E C T E D   T E M P L A T E   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getSelectedTemplateArray($group_id){
		$templates = array();
		$this->EE->db->select("template_id");
		$this->EE->db->from("better_pages_templates");
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("group_id", $group_id);
		$q = $this->EE->db->get();
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				array_push($templates, $r->template_id);
			}
		}
		
		$q->free_result();
		return $templates;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   U P L O A D   D I R E C T O R Y   O B J E C T
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getUploadDirectoryObject($dirs){
		$directories = array();
		$this->EE->db->select("id, name, server_path, url");
		$this->EE->db->from("upload_prefs");
		$this->EE->db->where_in('id', $dirs);
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->order_by("name");
		return $this->EE->db->get();
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   G R O U P   I D
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getGroupId($channel_id){
		$group_id = 0;
		$this->EE->db->select("field_group");
		$this->EE->db->where("channel_id", $channel_id);
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$q = $this->EE->db->get("channels");
		if ($q->num_rows() > 0){
				$row 		= $q->row();
				$group_id	= $row->field_group;
		}
		return $group_id;
	}
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   G R O U P   F I E L D S   O B J E C T
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getGroupFieldsObject($group_id){
		$this->EE->db->select("field_id, field_label");
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("field_required", "n");
		$this->EE->db->where("group_id", $group_id);
		$this->EE->db->where("field_type !=", "better_pages");
		$this->EE->db->order_by("field_order");
		return $this->EE->db->get("channel_fields");
	}	
	
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>
	//	G E T   S E L E C T E D   G R O U P   F I E L D S   A R R A Y
	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~>>

	function _getSelectedGroupFieldsArray($group_id){
		$selected = array();
		$this->EE->db->select("*");
		$this->EE->db->where("site_id", $this->EE->config->item('site_id'));
		$this->EE->db->where("group_id", $group_id);
		$q = $this->EE->db->get("better_pages_hidden");
		
		if ($q->num_rows() > 0){
			foreach ($q->result() as $r){
				if (!array_key_exists($r->template_id, $selected)){
					$selected[$r->template_id] = array();
				}
				
				array_push($selected[$r->template_id], $r->field_id);
			}
		}
		
		return $selected;
	}	
	
}