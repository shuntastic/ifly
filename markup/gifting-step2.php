<?php include("inc/head.php"); ?>
<body class="info-page one-col booking gifting step2">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator-gifting.php"); ?>	

	<div id="main" class="gifting step2" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next" href="#">&gt;</a>

				<h1 class='underline'>The iFLY Gift Card.<br />Wow them with the gift of flight.</h1>
					<h2 class="note">Never expires • Redeemable online or on-site</h2>
                    <p class="intro">Choose What type of flyer the recipient is and the iFLY location they would want to go to. The number of flyers is optional. Package gift cards include all applicable taxes. </p>
	
    			<div id="booking-details">
					<form action="#" class="uniform tunnel-locator">
						<fieldset>
							<div class="row">
								<?php include ('inc/form-elements/select-flyer-type.php'); ?>
								<?php include ('inc/form-elements/select-how-many-flyers.php'); ?>
							</div>
						</fieldset>
					

						<article class="tunnel-info">
							<aside>
								<img src="/images/temp-booking-step2.jpg" alt="alt">
							</aside>
							<div>
								<h3>iFly Hollywood</h3>
								<ul>
									<li>100 Universal City Plz</li>
									<li>Universal City, CA 91608</li>
									<li>(818) 985-4359</li>
								</ul>
								<div class="row">
									<a href="#" class="btn map-locator"><em></em><span>Map Location</span><div></div></a>
								</div>
								<div class="row">
									<select class="" name="FIND A DIFFERENT LOCATION" id="change-location">
										<option>FIND A DIFFERENT LOCATION</option>
										<option>SEATTLE</option>
										<option>AUSTIN</option>
									</select>
								</div>
							</div>
							<div class="next">
								<input type="submit" value="Next" class="next">
							</div>
						</article>

					</form>

				</div>

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	
</body>
</html>