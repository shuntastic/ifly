<?php include("inc/head.php"); ?>
<body class="info-page one-col booking gifting step3">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator-gifting.php"); ?>	

	<div id="main" class="gifting step3" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next" href="#">&gt;</a>

				<h1 class='underline'>Here are Your Recommended Gift Cards</h1>
				<div class="intro">
					<p>Choose a package gift card, or choose a dollar amount. We’ll email you the digital gift card within 24 hours. Just forward it on to your recipient. Redeemable online or at any iFLY location.</p>
				</div>

				<div class="options">
					<h3>Pick a Package</h3>
					<h3>Or</h3>
					<h3>Dollar Amount</h3>
				</div>

				<div id="packages">

					<table class="main packages">
						<thead>
							<tr>
								<th>Package</th>
								<th>Description</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="package">Earn Your Wings </td>
								<td class="desc">Earn Your Wings with our most popular first time flyer package. Includes two flights for one person in the wind tunnel. <a href="#" class="tooltip ir" data-title="Title" data-content="This is some content">more info</a></td>
								<td class="price">
									<span>$59.95<em class="asterisk">*</em></span>
									<form action="#" class="uniform tunnel-locator">
										<select name="1" id="quantity">
											<option>1</option>
											<option>2</option>
											<option>3</option>
										</select>
										<input type="submit" value="ADD" class="add">
									</form>
								</td>
							</tr>
							<tr>
								<td class="package">Earn Your Wings </td>
								<td class="desc">Earn Your Wings with our most popular first time flyer package. Includes two flights for one person in the wind tunnel. <a href="#" class="tooltip ir" data-title="Title" data-content="This is some content">more info</a></td>
								<td class="price">
									<span>$59.95<em class="asterisk">*</em></span>
									<form action="#" class="uniform tunnel-locator">
										<select name="1" id="quantity">
											<option>1</option>
											<option>2</option>
											<option>3</option>
										</select>
										<input type="submit" value="ADD" class="add">
									</form>
								</td>
							</tr>
							<tr>
								<td class="package">Earn Your Wings </td>
								<td class="desc">Earn Your Wings with our most popular first time flyer package. Includes two flights for one person in the wind tunnel. <a href="#" class="tooltip ir" data-title="Title" data-content="This is some content">more info</a></td>
								<td class="price">
									<span>$59.95<em class="asterisk">*</em></span>
									<form action="#" class="uniform tunnel-locator">
										<select name="1" id="quantity">
											<option>1</option>
											<option>2</option>
											<option>3</option>
										</select>
										<input type="submit" value="ADD" class="add">
									</form>
								</td>
							</tr>
						</tbody>
					</table>

					<p class="note">* Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec gravida volutpat orci, ac congue est eleifend aliquam. <a href="#" class="legal"> continue reading</a></p>


				</div> <!-- /#packages -->


				<form class="uniform amount">
					<div class="row">
						<input type="radio" name="amount" value="100" class="large">
						<label>$100</label>
					</div>
					<div class="row">
						<input type="radio" name="amount" value="200" class="large">
						<label>$200</label>
					</div>
					<div class="row">
						<input type="radio" name="amount" value="300" class="large">
						<label>$300</label>
					</div>
					<div class="row">
						<input type="radio" name="amount" value="400" class="large">
						<label>$400</label>
					</div>
					<div class="row">
						<input type="radio" name="amount" value="" class="large">
						<input type="text" name="amount" class="text amount" value="other amount">
						
					</div>
					<input type="submit" value="Purchase Gift Card" class="purchase">
				</form>

			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>
	
</body>
</html>