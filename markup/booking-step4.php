<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step4">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" class="step4" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next" href="#">&gt;</a>
				<div class="single-step">
                    <h1 class='underline'>PICK A DATE &amp; TIME</h1>
                    <h2 class="note"></h2>
    
                    <div id="calendar-wrapper">
                        <div id="calendar-pickadate"></div>
                        <p>Feel free to change your date.</p>
                    </div>
    
                    <div id="time-picker">
                        <form>
                        
                        </form>
                            
                    </div><!-- /#time-picker -->
				</div>
			</div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step4();
		});
	</script>
	
</body>
</html>