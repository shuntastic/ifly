<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
========================================================
Plugin TweetLine Version 2.1
--------------------------------------------------------
Copyright: David Dexter (Brilliant2.com) 
Author: David Dexter - www.brilliant2.com 
(EE 2.0 Update by Alex Porter - www.streetalchemydesign.com)
License: Absolutely Freeware - Use It and Abuse It.... 
http://www.brilliant2.com 
--------------------------------------------------------
This addon may be used free of charge. Should you have 
the opportunity to use it for commercial projects then 
I applaud you! 
========================================================
File: pi.tweetline.php
--------------------------------------------------------
Purpose: Grab a feed from twitter for a give user
========================================================
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
OR OTHER DEALINGS IN THE SOFTWARE.
========================================================
*/

$plugin_info = array(
                 'pi_name'          => 'Modulo',
                 'pi_version'       => '2.0',
                 'pi_author'        => 'David Dexter',
                 'pi_author_url'    => 'http://www.brilliant2.com/',
                 'pi_description'   => 'The modulo operation finds the remainder of division of one number by another',
                 'pi_usage'         => modulo::usage()
               );


class Modulo {

    var $return_data;

    // ----------------------------------------
    //  Find and Replace
    // ----------------------------------------

    function Modulo()
    {
        $this->EE =& get_instance();
                        
        // fetch params
        $dividend 	= $this->EE->TMPL->fetch_param('dividend');
        $divisor    = $this->EE->TMPL->fetch_param('divisor');
        $this->return_data = $dividend % $divisor;
    }
    // END
    
    
// ----------------------------------------
//  Plugin Usage
// ----------------------------------------

// This function describes how the plugin is used.

function usage() {
ob_start(); 
?>
In computing, the modulo operation finds the remainder of division of one number by another.

Given two numbers, a (the dividend) and n (the divisor), a modulo n (abbreviated as a mod n) is 
the remainder, on division of a by n. For instance, the expression "7 mod 3" would evaluate to 1, 
while "9 mod 3" would evaluate to 0. Although typically performed with a and n both being integers, 
many computing systems allow other types of numeric operands.

Parameters

1) Dividend (required)
2) Divisor (required)

{exp:modulo dividend="dividend" divisor="divisor"}
<?php
$buffer = ob_get_contents();
	
ob_end_clean(); 

return $buffer;
}
// END

}
// END CLASS
?>