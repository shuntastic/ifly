/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

(function($) {

  	// plugin definition

	$.fn.videoplayer = function(options)
	{
		
		$.fn.videoplayer.keyboard.init();
		
		// build main options before element iteration
		// iterate and reformat each matched element

		return this.each(
			function()
			{
				field = $(this);

				$.fn.videoplayer.fields.init(field);
			}
		);
	};


	// current variables

	$.fn.videoplayer.box_loaded = false;
	$.fn.videoplayer.current_field = false;


	// --------------------------------------------------------------------

	/**
	 * Main initialize
	 *
	 * @access	private
	 * @return	void
	 */
  	$.fn.videoplayer.init = function(){

		$.fn.videoplayer.lightbox.init();


  		// matrix compatibility

  		if(typeof(Matrix) != "undefined")
  		{
			Matrix.bind("videoplayer", "display", function(cell) {

				// we remove event triggers because they are all going to be redefined
				// will be improved with single field initialization

				if (cell.row.isNew)
				{
					var field = $('> .videoplayer-field', cell.dom.$td);

					$.fn.videoplayer.fields.init(field);
				}
			});
		}
 	};

	// --------------------------------------------------------------------

	/**
	 * Fields
	 *
	 */
 	$.fn.videoplayer.fields = {
 		init:function(field)
 		{
			//console.log('field', field);

 			inputValue = $('input[type="hidden"]', field).attr('value');

 			if(inputValue != "")
 			{
				field.find('.preview').html('');
				field.find('.preview').css('display', 'block');
				field.find('.preview').addClass('videoplayer-field-preview-loading');

	  			video_page = inputValue;

				data = {
					'method': 'field_preview',
					'video_page': video_page,
					'site_id': VideoPlayer.site_id
				};

	 			$('input[type="hidden"]', field).attr('value', video_page);

				$.ajax({
				  url: VideoPlayer.ajax_endpoint,
				  type:"post",
				  data : data,
				  success: function( data ) {

					//console.log('after ajax');

			  		field.find('.preview').html(data);
					field.find('.preview').removeClass('videoplayer-field-preview-loading');
				  }
				});

 				$('.change', field).css('display', 'inline-block');
 				$('.remove', field).css('display', 'inline-block');
 			}
 			else
 			{
 				$('.add', field).css('display', 'inline-block');
 			}

			$('.add', field).click(function(){
				$.fn.videoplayer.fields.add(field);
			});


			$('.change', field).click(function(){
				$.fn.videoplayer.fields.change(field);
			});

			$('.remove', field).click(function(){
				$.fn.videoplayer.fields.remove(field);
			});

			$('.videoplayer-field-embed-btn').live('click', function() {
				$('.videoplayer-overlay').css('display', 'block');
				$('.videoplayer-overlay').addClass('videoplayer-overlay-loading');

				data = {
					'method': $(this).data('method'),
					'video_page': $(this).data('video-page'),
					'site_id': VideoPlayer.site_id
				};

				$.ajax({
				  url: VideoPlayer.ajax_endpoint,
				  type:"post",
				  data : data,
				  success: function( data ) {

			  		$('body').append(data);

					$('.videoplayer-overlay').removeClass('videoplayer-overlay-loading');
					$.fn.videoplayer.lightbox.resize();

				  }
				});
			});

 		},

 		callback_add: function()
 		{
 			field = $.fn.videoplayer.current_field;

			field.find('.add').css('display', 'none');
			field.find('.change').css('display', 'inline-block');
			field.find('.remove').css('display', 'inline-block');
			field.find('.preview').html('');
			field.find('.preview').css('display', 'block');
			field.find('.preview').addClass('videoplayer-field-preview-loading');

  			video_page = $('.videoplayer-preview').data('video-page');

			data = {
				'method': 'field_preview',
				'video_page': video_page,
				'site_id': VideoPlayer.site_id
			};

 			$('input[type="hidden"]', field).attr('value', video_page);

			$.ajax({
			  url: VideoPlayer.ajax_endpoint,
			  type:"post",
			  data : data,
			  success: function( data ) {

				//console.log('after ajax');

		  		field.find('.preview').html(data);
				field.find('.preview').removeClass('videoplayer-field-preview-loading');
			  }
			});
 		},

 		add:function(field)
 		{
 			$.fn.videoplayer.current_field = field;
 			$.fn.videoplayer.open();
 		},

 		change:function(field)
 		{
 			$.fn.videoplayer.current_field = field;
 			$.fn.videoplayer.open();

 			// video page

 			var video_page = field.find('input').attr('value');

			// ajax browse to account

			var data = {
				method: 'box_preview',
				service: $.fn.videoplayer.current_service(),
				site_id: VideoPlayer.site_id,
				video_page: video_page,
				autoplay: 0
			}

			$('.videoplayer-preview').data('video-page', video_page);


			$.fn.videoplayer.browser.go(data, 'preview', function() {
				$('.videoplayer-controls').css('display', 'block');
			});
 		},

 		remove:function(field)
 		{
 			$.fn.videoplayer.close();
 			
 			field.find('input[type="hidden"]').attr('value', '');

			field.find('.add').css('display', 'inline-block');
			field.find('.change').css('display', 'none');
			field.find('.remove').css('display', 'none');
			field.find('.preview').css('display', 'none');
 		}
 	};


	// --------------------------------------------------------------------

	/**
	 * Open
	 *
	 */
 	$.fn.videoplayer.open = function()
 	{
 		$.fn.videoplayer.lightbox.show();

		var data = {
			method: 'box',
			site_id: VideoPlayer.site_id
		};

		if(!$.fn.videoplayer.box_loaded)
		{
			//console.log(VideoPlayer.ajax_endpoint);

			$.ajax({
			  url: VideoPlayer.ajax_endpoint,
			  type:"post",
			  data : data,
			  success: function( data ) {

				// console.log(data);

		  		$('body').append(data);
	  			$.fn.videoplayer.lightbox.resize();
	  			$.fn.videoplayer.box_loaded = true;

				$.fn.videoplayer.accounts.init();
				$.fn.videoplayer.search.init();
				$.fn.videoplayer.videos.init();
				$.fn.videoplayer.footer.init();

				if($('.videoplayer-service').length > 0)
				{
					$($('.videoplayer-service')[0]).trigger('click');
				}

				$($('.videoplayer-search input')[0]).focus();
				
				$.fn.videoplayer.search.textFocus = true;
				
				// console.log('focusin1');
			  }
			});
		} else {
			
			var selectedAction = $('.videoplayer-services > li.selected > ul a.selected');
			
			if(selectedAction.length > 0)
			{
				selectedAction = selectedAction[0];
				
				if($(selectedAction).hasClass('videoplayer-service-search'))
				{
					$($('.videoplayer-search input')[0]).focus();
					
					$.fn.videoplayer.search.textFocus = true;
		
					// console.log('focusin2');
				}
			}
		}
 	};

	// --------------------------------------------------------------------

	/**
	 * Close
	 *
	 */
 	$.fn.videoplayer.close = function() {
 		$.fn.videoplayer.lightbox.hide();
 	};

	// --------------------------------------------------------------------

	/**
	 * Current service
	 *
	 */
	$.fn.videoplayer.current_service = function()
	{
		var current_service = $($('.videoplayer-accounts li.selected a')[0]).data('service');
		return current_service;
	};

	// --------------------------------------------------------------------

	/**
	 * Accounts
	 *
	 */
  	$.fn.videoplayer.accounts = {
  		init:function()
  		{

	  		$('.videoplayer-close').live('click', function(){

 				$.fn.videoplayer.lightbox.hide();

	  		});

			// services

			$('.videoplayer-accounts > ul > li > a').live('click', function() {

				if($(this).parent().hasClass('selected'))
				{
					return false;
				}


				var el = $(this);

				current_method = $('.videoplayer-accounts ul ul > li a.selected').data('method');

				$('.videoplayer-accounts > ul > li').removeClass('selected');
				$('.videoplayer-accounts ul ul > li a').removeClass('selected');

				$(this).parent().addClass('selected');

				method_found = false;

				$(this).parent().find('ul > li a').each(function() {
					if($(this).data('method') == current_method)
					{
						method_found = true;
						$(this).trigger('click');

					}
				});

				if(!method_found)
				{
					$(this).parent().find('ul > li:first-child a').addClass('selected');

					q = $($('.videoplayer-search input')[0]).attr('value');


					// ajax browse to account

					var data = {
						method: 'service_search',
						service:$.fn.videoplayer.current_service(),
						site_id: VideoPlayer.site_id,
						q: q
					}

					$.fn.videoplayer.browser.go(data, 'videos');
				}

				$('.videoplayer-accounts > ul > li').each(function() {
					if(!el.hasClass('selected'))
					{
						$(this).parent().find('ul').slideUp({easing:'easeOutCubic', duration:400});
					}
				});

				$(this).parent().find('ul').slideDown({easing:'easeOutCubic', duration:400});
			});


			// service actions

			$('.videoplayer-accounts ul ul > li > a').live('click', function() {

				$('.videoplayer-accounts ul ul > li > a').removeClass('selected');

				$(this).addClass('selected');

				if(!$(this).hasClass('videoplayer-service-search')) {
					$.fn.videoplayer.search.textFocus = false;
					// console.log('focusout');
				}

				if($(this).hasClass('videoplayer-service-search'))
				{
					$('.videoplayer-title-videos').css('display', 'none');
					$('.videoplayer-title-favorites').css('display', 'none');

					$('.videoplayer-search').css('display', 'block');
					
					$($('.videoplayer-search input')[0]).focus();
					
					$.fn.videoplayer.search.textFocus = true;
					
					// console.log('focusin3');
				}
				else if($(this).hasClass('videoplayer-service-videos'))
				{
					$('.videoplayer-search').css('display', 'none');
					$('.videoplayer-title-favorites').css('display', 'none');

					$('.videoplayer-title-videos').css('display', 'block');
				}
				else if($(this).hasClass('videoplayer-service-favorites'))
				{
					$('.videoplayer-search').css('display', 'none');
					$('.videoplayer-title-videos').css('display', 'none');

					$('.videoplayer-title-favorites').css('display', 'block');
				}

				q = $('.videoplayer-search input')[0];
				q = $(q).attr('value');

				//q = q.trim();

				method = $(this).data('method');

				//console.log(q, method);

				data = {
					'method': method,
					'q':  q,
					'site_id': VideoPlayer.site_id,
					'service':$(this).parent().parent().parent().find('> a').data('service')
				};

				$.fn.videoplayer.browser.go(data, 'videos');
			});


			// fire out some stuff at init

			$('.videoplayer-accounts li ul').css('display', 'none');

			$('.videoplayer-accounts > ul > li').each(function() {
				if($(this).hasClass('selected'))
				{
					$(this).find('ul').slideDown({easing:'easeOutCubic', duration:400});
					$(this).find('ul li:first-child a').trigger('click');
				}
			});
  		}
 	};

	// --------------------------------------------------------------------
	
	$.fn.videoplayer.keyboard = {
		
		init:function()
		{
			$(document).live('keyup', function(e) {
				
				// is the box already loaded ?
			
				if($('.videoplayer-box').length == 1)
				{
					// is the box visible ?
					
					if($('.videoplayer-box').css('display') == 'block')
					{
					
						// is the focus on the search field					
						
						$('.videoplayer-search input').live('focusin', function(){
							$.fn.videoplayer.search.textFocus = true;
							// console.log('focus in');
						});
						
						$('.videoplayer-search input').live('focusout', function(){
							$.fn.videoplayer.search.textFocus = false;
							// console.log('focus out');
						});

						// 27 = [esc]
						// 13 = [enter]
						
						switch(e.keyCode)
						{
							// esc : hide lightbox
							
							case 27: 
							
							$.fn.videoplayer.lightbox.hide();
							
							break;
							
							
							// enter : select video
							
							case 13:
							// console.log('prev', $('.videoplayer-preview-video').length, $.fn.videoplayer.search.textFocus);
							
							if($('.videoplayer-preview-video').length == 1 && $.fn.videoplayer.search.textFocus == false)
							{
								$('.videoplayer-submit').trigger('click');
							}
							else
							{
								// console.log('no', $('.videoplayer-preview-video').length, $.fn.videoplayer.search.textFocus);
							}
							
							break;
						}
					}
				}
			});
			
		}
	};
	
	/**
	 * Search
	 *
	 */
  	$.fn.videoplayer.search = {
  	
  		textFocus: false,

  		init:function()
  		{
  			// search reset

  			$('.videoplayer-search-reset').live('click', function(){
  				$('.videoplayer-search input').attr('value', '');
  				$(this).css('display', 'none');
  				$('.videoplayer-search input').trigger('keyup');
  			});


  			// live key watcher

			$.fn.videoplayer.search.timer = false;

			var abort = false;

  			$('.videoplayer-search input').live('keydown', function(e) {

				//console.log('down'+ e.keyCode);

  				if(e.keyCode == 91) //command
  				{
  					abort=true;
  				}

  			}).live('keyup',
				function(e) {

					$('.videoplayer-accounts > ul > li.selected ul li a').removeClass('selected');
					$('.videoplayer-accounts > ul > li.selected ul li:first-child a').addClass('selected');

					//console.log($.fn.videoplayer.search.timer);

					var el = $(this);
					var q = el.attr('value');

					if(q !== "")
					{
						$('.videoplayer-search-reset').css('display', 'block');
					}
					else
					{
						$('.videoplayer-search-reset').css('display', 'none');
					}

					//console.log('up'+ e.keyCode);

					if(abort == true && e.keyCode != 91)
					{
						abort = false;
						return false;
					}


					switch(e.keyCode)
					{
						case 91: // command
							abort = false;
							return false;
						break;

						case 18: // alt
						case 16: // shift
						case 37:
						case 38:
						case 39:
						case 40:
							return false;
						break;
					}


					if($.fn.videoplayer.search.timer)
					{
						clearTimeout($.fn.videoplayer.search.timer);
					}

					if(!$.fn.videoplayer.search.timer)
					{
		  				$('.videoplayer-videos-inject').addClass('videoplayer-frame-loading');
	  					$('.videoplayer-videos-empty').css('display', 'none');
					}

					$.fn.videoplayer.search.timer = setTimeout(function() {

						var data = {
							method: 'service_search',
							service:$.fn.videoplayer.current_service(),
							site_id: VideoPlayer.site_id,
							q: q
						};

						$.fn.videoplayer.browser.go(data, 'videos');

					}, 500);
				}
  			);
		}
 	};

	// --------------------------------------------------------------------

	/**
	 * Videos
	 *
	 */
  	$.fn.videoplayer.videos = {
  		init:function()
  		{
  			// clickable video list

			$('.videoplayer-videos li').live('click', function() {

				if($(this).hasClass('videoplayer-videos-more'))
				{
					return false;
				}

				$('.videoplayer-preview-inject').html('');

				$('.videoplayer-videos li').removeClass('selected');

				$(this).addClass('selected');


				// ajax browse to account

				var data = {
					method: 'box_preview',
					service:$.fn.videoplayer.current_service(),
					site_id: VideoPlayer.site_id,
					video_page:$(this).data('video-page'),
					autoplay:1
				}

				$('.videoplayer-preview').data('video-page', $(this).data('video-page'));

				$('.videoplayer-controls').css('display', 'block');

				$.fn.videoplayer.browser.go(data, 'preview', function() {

					// init favorite button
					
					$('.videoplayer-controls').css('display', 'block');
				});
			});


			// set as favorite

			$('.videoplayer-preview-favorite').live('click', function() {
				favorite_enabled = false;
				if($(this).hasClass('videoplayer-preview-favorite-selected'))
				{
				  	$(this).removeClass('videoplayer-preview-favorite-selected');
				}
				else
				{
				  	$(this).addClass('videoplayer-preview-favorite-selected');
				  	favorite_enabled = true;
				}

				// ajax browse to account
				var data = {
					method: 'favorite',
					service:$.fn.videoplayer.current_service(),
					site_id: VideoPlayer.site_id,
					video_page:$('.videoplayer-preview').data('video-page'),
					favorite_enabled:favorite_enabled
				}

				$.ajax({
				  url: VideoPlayer.ajax_endpoint,
				  type:"post",
				  data : data,
				  success: function( data ) {
				  	//console.log('favorite success');

				  }
				});

			});


			// fullscreen mode

			$('.videoplayer-preview-fullscreen').live('click', function() {
				if($('.videoplayer-box').hasClass('videoplayer-fullscreen'))
				{
					$('.videoplayer-box').removeClass('videoplayer-fullscreen');
				}
				else
				{
					$('.videoplayer-box').addClass('videoplayer-fullscreen');
				}
			});


			// load more video when scrolled to absolute bottom

			$('.videoplayer-videos-more').live('click', function() {

				if($(this).find('.videoplayer-videos-more-btn').css('display') != "none")
				{
					$(this).find('.videoplayer-videos-more-btn').css('display', 'none');
					$(this).find('.videoplayer-videos-more-loading').css('display', 'inline');

					var q = $('.videoplayer-search input').attr('value');

					var data = {
						method: $('.videoplayer-services > li.selected li a.selected').data('method'),
						service:$.fn.videoplayer.current_service(),
						site_id: VideoPlayer.site_id,
						q: q,
						page: $(this).data('next-page')
					};


					//$.fn.videoplayer.browser.go(data, 'videos');

					$.ajax({
					  url: VideoPlayer.ajax_endpoint,
					  type:"post",
					  data : data,
					  beforeSend:function()
					  {
					  },
					  success: function( data ) {

						$('.videoplayer-videos-more').remove();

						var html_before = $('.videoplayer-videos ul').html();

						var html = html_before + data;

						$('.videoplayer-videos ul').html(html);
						//$('.videoplayer-videos ul').append('<li>Next page loaded</li>');
					  }
					});
				}

				//console.log('load more videos');
			});

			$($('.videoplayer-videos').get(0)).scroll(function(eventData) {
				scrollDifference = $(this).get(0).scrollHeight - $(this).scrollTop();

				if(scrollDifference == $(this).height())
				{
					$('.videoplayer-videos-more').trigger('click');
				}
			});
		}
 	};

	// --------------------------------------------------------------------

	/**
	 * Browser
	 *
	 */
  	$.fn.videoplayer.browser = {

  		current_request : [],

  		go:function(data, frame, callback)
  		{
  			if(typeof($.fn.videoplayer.browser.current_request[frame]) != "undefined")
  			{
  				if($.fn.videoplayer.browser.current_request[frame] != false)
  				{
	  				$.fn.videoplayer.browser.current_request[frame].abort();

	  				$.fn.videoplayer.browser.current_request[frame] = false;
  				}
  			}
  			else
  			{
  				$.fn.videoplayer.browser.current_request[frame] = false;
  			}

			$.fn.videoplayer.browser.current_request[frame] = $.ajax({
			  url: VideoPlayer.ajax_endpoint,
			  type:"post",
			  data : data,
			  beforeSend:function()
			  {
			  	$('.videoplayer-'+frame).addClass('videoplayer-frame-loading');
				$('.videoplayer-videos-empty').css('display', 'none');
			  },
			  success: function( data ) {

			  	$.fn.videoplayer.browser.current_request[frame] = false;

				$('.videoplayer-'+frame+'-inject').html(data);
			  	$('.videoplayer-'+frame).removeClass('videoplayer-frame-loading');

			  	//console.log($.fn.videoplayer.search.timer);
				if($.fn.videoplayer.search.timer){
					clearTimeout($.fn.videoplayer.search.timer);
					$.fn.videoplayer.search.timer = false;
			  		//console.log("timer clear");
				}

		  		if(typeof(callback) == "function")
		  		{
		  			callback();
		  		}
			  }
			});
  		}
 	};

	// --------------------------------------------------------------------

	/**
	 * Footer
	 *
	 */
  	$.fn.videoplayer.footer = {
  		init:function()
  		{
  			$('.videoplayer-controls').css('display', 'none');
			
	  		$('.videoplayer-submit').live('click', function(){

				$.fn.videoplayer.lightbox.hide();

				$.fn.videoplayer.fields.callback_add();


	  		});

	  		$('.videoplayer-cancel').live('click', function(){

 				$.fn.videoplayer.lightbox.hide();

	  		});
	  	}
 	};

	// --------------------------------------------------------------------

	/**
	 * Overlay
	 *
	 */
  	$.fn.videoplayer.lightbox = {
  		init:function()
  		{
  			// init box

  			//console.log('videoplayer.lightbox.init');
  			// init overlay
  			$('body').prepend('<div class="videoplayer-overlay"></div>');

  			$('.videoplayer-overlay').click(function() {
  				$.fn.videoplayer.lightbox.hide();
  			});

  			$.fn.videoplayer.lightbox.resize();
  		},

  		resize: function()
  		{
  			// window

  			winW = $(window).width();
  			winH = $(window).height();


  			// box

  			boxW = $('.videoplayer-lightbox').width();
  			boxH = $('.videoplayer-lightbox').height();

  			boxLeft = Math.round((winW - boxW) / 2);
  			boxTop = Math.round((winH - boxH) / 2);

  			$('.videoplayer-lightbox').css('left', boxLeft);
  			$('.videoplayer-lightbox').css('top', boxTop);


  			// manager

  			manageW = $('.videoplayer-manage').width();
  			manageH = $('.videoplayer-manage').height();

  			manageLeft = Math.round((winW - manageW) / 2);
  			manageTop = Math.round((winH - manageH) / 2);

  			$('.videoplayer-manage').css('left', manageLeft);
  			$('.videoplayer-manage').css('top', manageTop);

  			// field embed

  			fieldEmbedW = $('.videoplayer-field-embed').width();
  			fieldEmbedH = $('.videoplayer-field-embed').height();

  			var fieldEmbedLeft = Math.round((winW - fieldEmbedW) / 2);
  			var fieldEmbedTop = Math.round((winH - fieldEmbedH) / 2);

  			$('.videoplayer-field-embed').css('left', fieldEmbedLeft);

  			$('.videoplayer-field-embed').css('top', fieldEmbedTop);
  		},

		hide:function()
		{
			//console.log('videoplayer.lightbox.hide');

			$('.videoplayer-overlay').css('display', 'none');
			$('.videoplayer-box').css('display', 'none');
			$('.videoplayer-manage').css('display', 'none');
			$('.videoplayer-field-embed').css('display', 'none');
			$('.videoplayer-preview-inject').html('');
			$('.videoplayer-field-embed').remove();
			$('.videoplayer-fullscreen').removeClass('videoplayer-fullscreen');
		},

		show:function()
		{
			$('.videoplayer-overlay').css('display', 'block');
			$('.videoplayer-box').css('display', 'block');
			if(typeof($('.videoplayer-preview').data('video-page')) !== "undefined")
			{
				//console.log('videoplayer.lightbox.show');

				var data = {
					method: 'box_preview',
					video_page:$('.videoplayer-preview').data('video-page'),
					site_id: VideoPlayer.site_id
				};

				$.fn.videoplayer.browser.go(data, 'preview', function() {
					$('.videoplayer-controls').css('display', 'block');
				});
				
			}
		}
	};


	// Initialization

	$(document).ready(function() {
		$.fn.videoplayer.init();
	});


	// Resize window

	$(window).resize(function() {
  		$.fn.videoplayer.lightbox.resize();
	});

})(jQuery);

$().ready(function()
{
	$('.videoplayer-field').videoplayer();
});

/* End of file videoplayer.field.js */
/* Location: ./themes/third_party/videoplayer/js/videoplayer.field.js */