<?php

if (! defined('BWF_VER'))
{
	define('BWF_NAME', 'Better Workflow');
	define('BWF_VER',  '1.4');
}

$config['name']    = BWF_NAME;
$config['version'] = BWF_VER;