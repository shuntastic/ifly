// Reviews functions
IFLY.ratingsreviews = {
	total_reviews:0,
	total_pages:0,
	current_page:1,
	page_number:1,
	star_count:[],
	init: function() {
				
//				//COMMENT OUT WHEN ALL LOCATIONS LIVE		
//				if (IFLY.current_tunnel == null) {
//					var full = window.location.host;
//					var subCheck = full.split('.');
//					var locRef = jQuery.map(IFLY.tunnels, function(obj) {
//						if(obj.subdomain === subCheck[0])
//							 return obj; // or return obj.name, whatever.
//					});
//					
//					if(locRef.length >0) {
//						IFLY.current_tunnel = locRef[0].code;
//					}
//				}
			getParams();
			if(urlParams.search==''){
				$('#ratingSearch, a.btn.search').css('display','inline-block');
			}
/**/			if($('select.locations').length > 0) {
				IFLY.locationHandler.setTunnelDropdown();
				$("select.locations").change(function(e) {
						IFLY.current_tunnel = $("select.locations").val();
						IFLY.ratingsreviews.getSummary();
						//IFLY.videotestimonials.loadResultsPage(tunnel, IFLY.videotestimonials.currentPage);
				});
			}
			IFLY.current_tunnel = (IFLY.current_tunnel == null) ? IFLY.locationHandler.getCurrentTunnel() :  IFLY.current_tunnel;
			if(!IFLY.locationHandler.isLocalSite()) {
				$('a.take-survey').css('visibility','hidden');
			}
			
			IFLY.ratingsreviews.getSummary();
			if($('a.btn.more').length > 0) {
				$('a.btn.more').click(function(e) {
					IFLY.ratingsreviews.getMoreReviews();
					$(this).parent('p').append(IFLY.showLoader());
					e.preventDefault();
				});
			}
			if($('select#sort-all').length > 0) {
				$('select#sort-all').change(function(){
						IFLY.ratingsreviews.current_page = 1;
						IFLY.ratingsreviews.getReviews(null);
//						if($(this).val() != 'all') {
//							IFLY.ratingsreviews.getReviewsByRating($(this).val());
//						} else {
//							IFLY.ratingsreviews.getReviews();
//						}
					});
			}
			if($('select[id$="sort-recent"]').length > 0) {
				$('select[id$="sort-recent"]').change(function(){
						IFLY.ratingsreviews.current_page =1;
						IFLY.ratingsreviews.getReviews(null);
					});
			}
			if($('.pagination a.next').length > 0) {
				$('.pagination a.next').click(function(e) {
					if (IFLY.ratingsreviews.current_page != IFLY.ratingsreviews.total_pages) {
						IFLY.ratingsreviews.current_page +=1;
						IFLY.ratingsreviews.getReviews(null);
					}
					e.preventDefault();
				});
			}
			if($('.pagination a.prev').length > 0) {
				$('.pagination a.prev').click(function(e) {
					if (IFLY.ratingsreviews.current_page > 1) {
						IFLY.ratingsreviews.current_page -=1;
						IFLY.ratingsreviews.getReviews(null);
					}
					e.preventDefault();
				});
			}
			$('a.btn.search').unbind('click').click(function() {
				var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+IFLY.ratingsreviews.current_page+'&reviewsort='+$('select[id$="sort-recent"] option:selected').val()+'&starfilter='+$('select[id$="sort-all"] option:selected').val()+'&search=';
				console.log('click',$('input#ratingSearch').val());
				if ($('input#ratingSearch').val() != 'Search by Instructor' && $('input#ratingSearch').val() != '') {
					IFLY.ratingsreviews.getReviews($('input#ratingSearch').val());
				}
				
				//Search by Instructor
			});
	},
	getSummary: function() {
			if($('#ratings-dist').length>0) {
					$('#reviews').html(IFLY.showLoader());
					var call='method=get_review_summary&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel;
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){

/*{"data":{"stars":{"star_count":461,"star_count0":"0","star_percent0":"0","star_count1":"1","star_percent1":"0.2","star_count2":"4","star_percent2":"0.9","star_count3":"7","star_percent3":"1.5","star_count4":"60","star_percent4":"12.9","star_count5":"389","star_percent5":"83.8"},"recommend":{"Yes":"471","total":"479","No":"8"}},"status":"OK","message":"get_review_summary data returned"}*/							
							var data = IFLY.ratingsreviews.rating_summary = response.data;
						//	console.log('rating summary data',response);
							IFLY.ratingsreviews.total_reviews = parseInt(data.recommend.total);
							IFLY.ratingsreviews.total_pages = Math.ceil(IFLY.ratingsreviews.total_reviews/5);
	//						IFLY.ratingsreviews.total_pages = IFLY.ratingsreviews.current_page = data.pages;
						
							var avg = ((( parseInt(data.stars.star_count5) * 100 ) + (parseInt(data.stars.star_count4) * 80 ) + (parseInt(data.stars.star_count3) * 60 ) + (parseInt(data.stars.star_count2) * 40 ) + (parseInt(data.stars.star_count1) * 20 ) )  / (parseInt(data.stars.star_count5) + parseInt(data.stars.star_count4) + parseInt(data.stars.star_count3) + parseInt(data.stars.star_count2) + parseInt(data.stars.star_count1)) );
                        	var starAvg = Math.round(avg / 20 * 10) / 10;
							if (!isNaN(Math.round(avg))) $('span.average').html(starAvg + " out of 5 stars");
							var starClass = (starAvg > 4) ? 'star5' : (starAvg > 3 && starAvg <= 4) ? 'star4' : (starAvg > 2 && starAvg <= 3) ? 'star3' : (starAvg > 2 && starAvg <= 3) ? 'star2' : 'star1';
							
							IFLY.ratingsreviews.star_count = [];
							$('span.star-rating').removeClass('star5').removeClass('star4').removeClass('star3').removeClass('star2').removeClass('star1').addClass(starClass);
							$('.progress-bar.prog1').html('1 stars <span><em></em></span>');
							$('.progress-bar.prog2').html('2 stars <span><em></em></span>');
							$('.progress-bar.prog3').html('3 stars <span><em></em></span>');
							$('.progress-bar.prog4').html('4 stars <span><em></em></span>');
							$('.progress-bar.prog5').html('5 stars <span><em></em></span>');
							
							$('.progress-bar.prog1 em').animate({width:(parseFloat(data.stars.star_percent1))},1000);
							$('.progress-bar.prog2 em').animate({width:(parseFloat(data.stars.star_percent2))},1000);
							$('.progress-bar.prog3 em').animate({width:(parseFloat(data.stars.star_percent3))},1000);
							$('.progress-bar.prog4 em').animate({width:(parseFloat(data.stars.star_percent4))},1000);
							$('.progress-bar.prog5 em').animate({width:(parseFloat(data.stars.star_percent5))},1000);
							$('.progress-bar.prog1').append('&nbsp;'+data.stars.star_count1);
							$('.progress-bar.prog2').append('&nbsp;'+data.stars.star_count2);
							$('.progress-bar.prog3').append('&nbsp;'+data.stars.star_count3);
							$('.progress-bar.prog4').append('&nbsp;'+data.stars.star_count4);
							$('.progress-bar.prog5').append('&nbsp;'+data.stars.star_count5);
							IFLY.ratingsreviews.star_count.push(data.stars.star_count1)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count2)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count3)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count4)
							IFLY.ratingsreviews.star_count.push(data.stars.star_count5)
					
							IFLY.ratingsreviews.current_page = 1;
						
							$('#ratings-dist p').html(data.recommend.Yes+' out of '+data.recommend.total+' would recommend iFLY to a friend!');
							IFLY.ratingsreviews.getReviews(null);
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
},
	getReviews: function(instructor) {
		
				if(IFLY.ratingsreviews.total_pages>=0) {
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+IFLY.ratingsreviews.current_page+'&reviewsort='+$('select[id$="sort-recent"] option:selected').val()+'&starfilter='+$('select[id$="sort-all"] option:selected').val()+'&search=';
					if(instructor != null) {
						console.log('isntructor', instructor);
						call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page=&reviewsort=&starfilter=&search='+instructor;
					}
			//			console.log('call',call);
				$('#reviews').html(IFLY.showLoader());
				$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
							console.log('response',response);
							var data = response.data;
							if(data.data) {
								IFLY.ratingsreviews.displayReviews(data.data, false);
							
								IFLY.ratingsreviews.updatePagination(data.page, data.total, data.pages);
							} else {
								IFLY.killLoader();
							}
						},
						error: handler.onError,
						complete: handler.onComplete
					});
			}
	},
	getMoreReviews: function() {
			if(IFLY.ratingsreviews.current_page>0 && IFLY.ratingsreviews.current_page!=IFLY.ratingsreviews.total_pages) {
				IFLY.ratingsreviews.current_page += 1;
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+parseInt(IFLY.ratingsreviews.current_page)+'&reviewsort='+$('select[id$="sort-recent"] option:selected').val()+'&starfilter='+$('select[id$="sort-all"] option:selected').val()+'&search=';
					//console.log('call',call);
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
							IFLY.killLoader();
							var data = response.data;
							IFLY.ratingsreviews.displayReviews(data.data, true);
							IFLY.ratingsreviews.updatePagination(data.page,data.total,data.pages);
							
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				} else {
					
					IFLY.killLoader();

				}
	},
	getReviewsByRating: function(rating) {
		
			if(IFLY.ratingsreviews.current_page>0) {
//				 var pageOffset = Math.floor(parseInt(IFLY.ratingsreviews.star_count[parseInt(rating)-1])/5);
//				 pageOffset = (pageOffset == 0)? 1 : pageOffset;
//				 console.log('pageOffset',pageOffset);
					$('#reviews').html(IFLY.showLoader());
					var call='method=get_reviews&controller=testimonials&format=json&tunnel='+IFLY.current_tunnel+'&page='+parseInt(IFLY.ratingsreviews.current_page)+'&starfilter='+rating+'&reviewsort='+$('select[id$="sort-recent"] option:selected').val()+'&search=';
					//console.log('call',call);
					$.ajax({
						url: '/api_curl.php',
						type: 'post',
						dataType: 'json',
						data: call,
						success: function(response, textStatus, jqXHR){
//							console.log('response',response);
							var data = response.data;
							IFLY.ratingsreviews.displayReviews(data.data, false);
							IFLY.ratingsreviews.updatePagination(data.page,data.total,data.pages);
							
						},
						error: handler.onError,
						complete: handler.onComplete
					});
				}
	},
	
	displayReviews: function(data,append) {
						var sortedData = ($('select#sort-recent').val() == 'asc') ? data : data.reverse();
	//					var sortedData = ($('select#sort-recent').val() == 'asc') ? data : data.reverse();
						var reviews ='';
						$.each(sortedData, function(index,value) {
							var showID = (urlParams.search=='') ? '<span><br /><em>Instructor:</em> '+this.inst+'</span><span class="custid"><br /><em>Customer ID:</em> '+this.custid+'</span>' : '';
	//						var showID = (urlParams.custid && urlParams.custid == 'true') ? '<span class="custid"><br /><em>Customer ID:</em> '+this.custid+'</span>' : '';
							var review = '<div class="review clearfix"><div class="info"><span class="star-rating star'+this.stars+'">'+this.stars+'-star review</span><span>'+this.datesubmitted+'</span><em>by '+this.name+'</em><span>'+this.city+' '+this.state+'</span>'+showID+'</div><div class="comment"><h2>'+this.reviewTitle+'</h2><p>'+this.reviewText+'</p></div></div>';
							reviews += review;
							
						});
						if(append) {
							$('#reviews').append(reviews);
						} else {
							$('#reviews').html(reviews);
						}
	},
	updatePagination: function(currentPage,active,totalPages) {
		//pages = parseInt(pages);
//		active = parseInt(active);
//		IFLY.ratingsreviews.current_page = pages - active;
//		IFLY.ratingsreviews.total_pages = parseInt(total)/5;
//		$('.pages span.active').html(Math.ceil(IFLY.ratingsreviews.total_pages - parseInt(active)));
//		$('.pages span.total').html(Math.floor(IFLY.ratingsreviews.total_pages));
		
		IFLY.ratingsreviews.total_pages = Math.ceil(totalPages);
		
		$('.pages span.active').html(Math.ceil(currentPage));
		$('.pages span.total').html(Math.floor(totalPages));
					

	}
}

var urlParams = {};
var packageData, sessionStatus;
var api = '/api_curl.php';
var dataType = 'html json';

$(document).ready(function() {
	//if($('body.ratings').length>0)
	//IFLY.ratingsreviews.init();
	//getParams();
});
