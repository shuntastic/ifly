1.5.1
	Bugfix : where the hide_from_nav function was not working
	
1.5
	Added : the use of structure hide_from_nav param

1.4
	Added : MSM ready

1.3
	Added : support to show all navigations for Navee and Taxonomy
	Bugfix : minor

1.2.4
	Bugfix : where the index.php was set, the plugin give in the structure mode a MySQL error

1.2.3
	Update documentation

1.2.2
	Bugfix : where the modify date is 'array' for the structure sitemap.

1.2.1
	Small Bug fix.

1.2
	Bugfix : where taxonomy file is loaded even when it is not there.

1.1
	Support for Navee and Taxonomy.
	
1.0
	Release