<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_helper {

	var $version = "3.0";

	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->EE =& get_instance();


		// prepare cache for head files

		if (! isset($this->EE->session->cache['videoplayer']['head_files']))
		{
			$this->EE->session->cache['videoplayer']['head_files'] = false;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Insert JS code
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function insert_js($str)
	{
		$this->EE->cp->add_to_head('<script type="text/javascript">' . $str . '</script>');
	}

	// --------------------------------------------------------------------

	/**
	 * Insert JS file
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function insert_js_file($file)
	{
		$this->EE->cp->add_to_head('<script charset="utf-8" type="text/javascript" src="'.$this->_theme_url().'js/'.$file.'?'.$this->version.'"></script>');
	}

	// --------------------------------------------------------------------

	/**
	 * Insert CSS file
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function insert_css_file($file)
	{
		$this->EE->cp->add_to_head('<link rel="stylesheet" type="text/css" href="'.$this->_theme_url().'css/'.$file.'?'.$this->version.'" />');
	}

	// --------------------------------------------------------------------

	/**
	 * Load heading files once (load_head_files)
	 *
	 * @access	private
	 * @return	void
	 */
	function include_resources()
	{
		$this->EE->load->library('videoplayer_lib');

		if (!$this->EE->session->cache['videoplayer']['head_files'])
		{

			$js = "	var VideoPlayer = VideoPlayer ? VideoPlayer : new Object();
					VideoPlayer.ajax_endpoint = '".$this->endpoint_url()."';
					VideoPlayer.site_id = '".$this->EE->config->item('site_id')."';
				";

			$this->insert_js($js);

			$this->insert_css_file('videoplayer.box.css');
			$this->insert_css_file('videoplayer.field.css');

			$this->insert_js_file('jquery.debounce-1.0.5.js');
			$this->insert_js_file('jquery.easing.1.3.js');
			$this->insert_js_file('videoplayer.field.js');

			$this->EE->session->cache['videoplayer']['head_files'] = true;
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Theme URL
	 *
	 * @access	private
	 * @return	string
	 */
	function _theme_url()
	{
		$url = $this->EE->config->item('theme_folder_url')."third_party/videoplayer/";
		return $url;
	}

	// --------------------------------------------------------------------

	/**
	 * Get cache file
	 *
	 * @access	private
	 * @param	string
	 * @return	string
	 */
	function cache_get($id)
	{
		$this->EE->load->helper('file');

		if ( ! file_exists($this->_cache_path.$id))
		{
			return FALSE;
		}

		$data = read_file($this->_cache_path.$id);

		$data = unserialize($data);

		if (time() >  $data['time'] + $data['ttl'])
		{
			unlink($this->_cache_path.$id);
			return FALSE;
		}

		return $data['data'];
	}

	// --------------------------------------------------------------------

	/**
	 * Save cache file with time to live
	 *
	 * @access	private
	 * @param	string
	 * @param	string
	 * @param	integer
	 * @return	string
	 */
	function cache_save($id, $data, $ttl = 60)
	{
		$this->EE->load->helper('file');

		$contents = array(
				'time'		=> time(),
				'ttl'		=> $ttl,
				'data'		=> $data
			);

		if (write_file($this->_cache_path.$id, serialize($contents)))
		{
			@chmod($this->_cache_path.$id, 0777);
			return TRUE;
		}

		return FALSE;

	}

	// --------------------------------------------------------------------

	/**
	 * Get authentication for a specific method & service
	 *
	 * @access	private
	 * @param	string
	 * @param	string
	 * @return	string
	 */
	function authentication_url($method, $service)
	{
		// redirect url

		$redirect = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."&site_id=".$this->EE->config->item('site_id')."&success=true";

		$redirect = base64_encode(serialize($redirect));


		// auth, oauth, authsub url

		return $this->endpoint_url().AMP."method=".$method.AMP.'service='.$service.AMP."site_id=".$this->EE->config->item('site_id').AMP."redirect=".$redirect;
	}

	// --------------------------------------------------------------------

	/**
	 * Endpoint base URL for frontend & cp
	 *
	 * @access	public
	 * @return	void
	 */
	function endpoint_url()
	{
		$site_url = $this->EE->functions->fetch_site_index(0, 0);

		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
		{
			$site_url = str_replace('http://', 'https://', $site_url);
		}

		$action_id = $this->fetch_action_id('Videoplayer', 'ajax');

		$url = $site_url.QUERY_MARKER.'ACT='.$action_id;

		return $url;
	}

	// --------------------------------------------------------------------

	/**
	 * A copy of the standard fetch_action_id method that was unavailable from here
	 *
	 * @access	private
	 * @return	void
	 */
	private function fetch_action_id($class, $method)
	{
		$this->EE->db->select('action_id');
		$this->EE->db->where('class', $class);
		$this->EE->db->where('method', $method);
		$query = $this->EE->db->get('actions');

		if ($query->num_rows() == 0)
		{
			return FALSE;
		}

		return $query->row('action_id');
	}

	// --------------------------------------------------------------------

	/**
	 * Endpoint serving given method (for ajax)
	 *
	 * @access	public
	 * @return	void
	 */
	function endpoint_output()
	{

		$this->EE->load->library('videoplayer_lib');

		if (! class_exists('Videoplayer_endpoint'))
		{
			require_once PATH_THIRD.'videoplayer/endpoint.php';
		}

		$endpoint = new Videoplayer_endpoint();


		$method = $this->EE->input->get_post('method');

		if($method)
		{
			if(method_exists($endpoint, $method))
			{
				$endpoint->{$method}();
				die();
			}
		}
	}
}

// END Videoplayer_helper class

/* End of file helper.php */
/* Location: ./system/expressionengine/third_party/videoplayer/helper.php */