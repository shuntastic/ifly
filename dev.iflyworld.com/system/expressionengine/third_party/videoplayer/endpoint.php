<?php

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_endpoint {

	/**
	 * Constructor
	 */
	function __construct()
	{
		$this->EE =& get_instance();



		// load helper

		if (! class_exists('Videoplayer_helper'))
		{
			require_once PATH_THIRD.'videoplayer/helper.php';
		}

		$this->helper = new Videoplayer_helper();
	}

	// --------------------------------------------------------------------

	/**
	 * Display the error and dies
	 *
	 * @access	public
	 * @param	string
	 * @return	void
	 */
	function error($msg)
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		echo '<div class="videoplayer-videos-error"><strong>'.$this->EE->lang->line('error_occured').'</strong><div class="videoplayer-videos-error-code">'.$msg.'</div></div>';
		die();
	}

	// --------------------------------------------------------------------

	/**
	 * Authenticate with username and password, then redirects
	 *
	 * @access	public
	 * @return	void
	 */
	function auth()
	{
		$this->EE->load->library('videoplayer_lib');
		
		$site_id = $this->EE->config->item('site_id');
		$services = $this->EE->videoplayer_lib->get_services($site_id);
		
	
		$service = $this->EE->input->get_post('service');

		$redirect = $this->EE->input->get('redirect');
		$redirect = unserialize(base64_decode($redirect));

		$username = $this->EE->input->post('username');
		$password = $this->EE->input->post('password');

		$services[$service]->auth($username, $password);

		$this->EE->functions->redirect($redirect);
	}

	// --------------------------------------------------------------------

	/**
	 * Oauth authentication
	 *
	 * @access	public
	 * @return	void
	 */
	function oauth()
	{

		$this->EE->load->model('videoplayer_model');

		$this->EE->load->helper('url');
		
		$site_id = $this->EE->input->get_post('site_id');

		$services = $this->EE->videoplayer_lib->get_services($site_id);

		$service = $this->EE->input->get_post('service');

		$endpoint_url = $this->helper->endpoint_url();

		$services[$service]->oauth($endpoint_url);
	}

	/**
	 * AuthSub authentication
	 *
	 * @access	public
	 * @return	void
	 */
	// --------------------------------------------------------------------

	function authsub()
	{
		$this->EE->load->model('videoplayer_model');

		$this->EE->load->helper('url');
		
		$site_id = $this->EE->input->get_post('site_id');

		$services = $this->EE->videoplayer_lib->get_services($site_id);
		
		$service = $this->EE->input->get_post('service');

		$endpoint_url = $this->helper->endpoint_url();

		$services[$service]->authsub($endpoint_url);
	}

	// --------------------------------------------------------------------

	/**
	 * Clear all authentication parameters
	 *
	 * @access	public
	 * @return	void
	 */
	function oauth_clear()
	{
		$_SESSION["access_token"] 				= NULL;
		$_SESSION["access_token_secret"] 		= NULL;
		$_SESSION["oauth_access_token"] 		= NULL;
		$_SESSION["oauth_access_token_secret"] 	= NULL;
		$_SESSION['oauth_state'] 				= NULL;
	}

	// --------------------------------------------------------------------

	/**
	 * Videos for a service
	 *
	 * @access	public
	 * @return	void
	 */
	function videos()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		$this->videoplayer_allowed();

		$vars['service'] = $this->EE->input->post('service');

		echo $this->EE->load->view('box/videos', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Load the Video Player box
	 *
	 * @access	public
	 * @return	void
	 */
	function box()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		$this->videoplayer_allowed();

		// load models, libraries & helpers

		$this->EE->load->model('videoplayer_model');
		$this->EE->load->library('videoplayer_lib');
		$this->EE->load->helper(array('url', 'form'));
		$this->EE->lang->loadfile('videoplayer');


		// load accounts preferences
		
		$site_id = $this->EE->input->post('site_id');
		
		$vars['accounts'] = $this->EE->videoplayer_model->get_accounts($site_id);


		// get services

		$vars['services'] = $this->EE->videoplayer_lib->get_services($site_id);


		// helper

		$vars['helper'] = $this->helper;


		// manage link

		$vars['manage_link'] = $this->EE->config->item('cp_url').'?S='.$this->EE->session->userdata('session_id').AMP.'D=cp'.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=videoplayer';

		// echo view

		echo $this->EE->load->view('box/box', $vars, true);

		die();
	}

	// --------------------------------------------------------------------

	/**
	 * Checks if the user has access to a channel which has a videoplayer fieldtype associated
	 *
	 * @access	public
	 * @return	void
	 */
	function videoplayer_allowed()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		// load models
		
		$this->EE->load->model('channel_model');
		$this->EE->load->model('field_model');
		

		$allowed_channels = $this->EE->functions->fetch_assigned_channels(true);

		$can_access_videoplayer = false;

		foreach($allowed_channels as $channel)
		{
			$query = $this->EE->channel_model->get_channel_info($channel);

			if ($query->num_rows() > 0)
			{
				$row = $query->row();

				$fields = $this->EE->field_model->get_fields($row->field_group);

				foreach($fields->result() as $field)
				{
					if($field->field_type == "matrix")
					{
						$this->EE->db->where('site_id', $field->site_id);
						$this->EE->db->where('field_id', $field->field_id);
						
						$matrix_query = $this->EE->db->get('matrix_cols');
						
						foreach ($matrix_query->result() as $matrix_row)
						{
							if($matrix_row->col_type == "videoplayer")
							{
								$can_access_videoplayer = true;	
							}
						}
					}
										
					if($field->field_type == "videoplayer")
					{
						$can_access_videoplayer = true;
					}
				}
			}
		}

		
		if(!$can_access_videoplayer)
		{
			echo $this->EE->lang->line('cant_access_videoplayer');
			die();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Set a video as favorite
	 *
	 * @access	public
	 * @return	void
	 */
	function favorite()
	{
		$this->videoplayer_allowed();

		$site_id = $this->EE->input->get_post('site_id');
		
		$services = $this->EE->videoplayer_lib->get_services($site_id);

		$service = $this->EE->input->post('service');

		$video_page = $this->EE->input->post('video_page');

		$video_id = $services[$service]->get_video_id($video_page);


		// check if already a fav

		$already_fav = $services[$service]->is_favorite($video_id);


		if($already_fav)
		{
			// remove favorite if it is
			
			$services[$service]->remove_favorite($video_id);
		}
		else
		{
			// add favorite if it's not

			$services[$service]->add_favorite($video_id);
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Load the video HTML embed with ajax
	 *
	 * @access	public
	 * @return	void
	 */
	function video($video_url = false)
	{
		if(!$video_url)
		{
			$video_url = $this->EE->input->get('video_url');
		}

		if($video_url)
		{
			$video_url = urldecode($video_url);


			// video options

			$video_opts = array();
			$video_opts['url'] = $video_url;
			$video_opts['width'] = 376;
			$video_opts['height'] = 250;

			$video = $this->get_video($video_opts);

			echo $video['embed'];
		}

		die();
	}

	// --------------------------------------------------------------------

	/**
	 * Box preview
	 *
	 * @access	public
	 * @return	void
	 */
	function box_preview()
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		$this->videoplayer_allowed();

		$site_id = $this->EE->input->get_post('site_id');

		$services = $this->EE->videoplayer_lib->get_services($site_id);

		$video_page = $this->EE->input->post('video_page');

		$autoplay = (integer) $this->EE->input->post('autoplay');

		$opts = array(
			'url' => $video_page,
			'width' => 460,
			'height' => 259,
			'autoplay' => $autoplay,
			'autohide' => 1
		);

		//var_dump($opts);



		$video = $this->EE->videoplayer_lib->get_video($opts);
		
		// is_favorite ?

		$video['is_favorite'] = $services[$video['service_key']]->is_favorite($video['id']);


		$vars['video'] = $video;

		echo $this->EE->load->view('box/preview', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Field preview
	 *
	 * @access	public
	 * @return	void
	 */
	function field_preview()
	{
		$this->videoplayer_allowed();

		$site_id = $this->EE->config->item('site_id');
		$services = $this->EE->videoplayer_lib->get_services($site_id);
		
		$video_page = $this->EE->input->post('video_page');

		$opts = array(
			'url' => $video_page,
			'width' => 300,
			'height' => 169,
			'autohide' => true
		);

		$vars['video'] = $this->EE->videoplayer_lib->get_video($opts);

		echo $this->EE->load->view('field/preview', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Field embed
	 *
	 * @access	public
	 * @return	void
	 */
	function field_embed()
	{
		$this->videoplayer_allowed();

		$site_id = $this->EE->config->item('site_id');
		$services = $this->EE->videoplayer_lib->get_services($site_id);
		
		$video_page = $this->EE->input->post('video_page');

		$opts = array(
			'url' => $video_page,
			'width' => 854,
			'height' => 483,
			'autoplay' => true
		);

		$vars['video'] = $this->EE->videoplayer_lib->get_video($opts);

		echo $this->EE->load->view('field/embed', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * Pagination
	 *
	 * @access	private
	 * @return	void
	 */
	private function pagination()
	{
		$pagination['per_page'] = 18;

		$pagination['page'] = $this->EE->input->post('page');

		if(!$pagination['page'])
		{
			$pagination['page'] = 1;
		}

		$pagination['next_page'] = $pagination['page'] + 1;

		return $pagination;
	}

	// --------------------------------------------------------------------

	/**
	 * Service search
	 *
	 * @access	public
	 * @return	void
	 */
	function service_search()
	{
		$this->videoplayer_allowed();

		try {
			$site_id = $this->EE->input->get_post('site_id');
			$services = $this->EE->videoplayer_lib->get_services($site_id);

			$service = $this->EE->input->post('service');

			$q = $this->EE->input->post('q');
			$q = trim($q);

			$pagination = $this->pagination();

			$vars['videos'] = array();

			if(!empty($q))
			{
				if(strpos($q, "http") !== false)
				{

					$opts = array(
						'url' => $q
					);

					$video = $this->EE->videoplayer_lib->get_video($opts);
					$vars['videos'] = array($video);
				}
				else
				{
					$vars['videos'] = $services[$service]->search($q, $pagination['page'], $pagination['per_page']);
				}


			}

			$vars['q'] = $q;
			$vars['service'] = $service;

			$vars['videos'] = $services[$service]->post_process_videos($vars['videos']);
			$vars['pagination'] = $pagination;

			echo $this->EE->load->view('box/videos', $vars, true);
		}
		catch(Exception $e)
		{
			$this->error($e->getMessage());
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Service favorites
	 *
	 * @access	public
	 * @return	void
	 */
	function service_favorites()
	{
		$this->videoplayer_allowed();

		try {
			$this->EE->load->model('videoplayer_model');

			$videos = array();

			$service = $this->EE->input->post('service');

			$pagination = $this->pagination();

			$videos = $this->EE->videoplayer_lib->get_favorites($service, $pagination['page'], $pagination['per_page']);


			$vars['videos'] = $videos;
			$vars['pagination'] = $pagination;

			echo $this->EE->load->view('box/videos', $vars, true);
		}
		catch(Exception $e)
		{
			$this->error($e->getMessage());
		}
	}

	// --------------------------------------------------------------------

	/**
	 * Service My Videos
	 *
	 * @access	public
	 * @return	void
	 */
	function service_videos()
	{
		$this->videoplayer_allowed();

		try {
			$this->EE->load->model('videoplayer_model');

			$videos = array();

			$service = $this->EE->input->post('service');


			$videos = $this->EE->videoplayer_lib->get_videos($service);

			$vars['videos'] = $videos;

			$vars['pagination'] = $this->pagination();

			echo $this->EE->load->view('box/videos', $vars, true);
		}
		catch(Exception $e)
		{
			$this->error($e->getMessage());
		}
	}

	// --------------------------------------------------------------------
}

// END Videoplayer_endpoint class

/* End of file endpoint.php */
/* Location: ./system/expressionengine/third_party/videoplayer/endpoint.php */