<?php include("inc/head.php"); ?>
<body class="info-page one-col booking step3">
	
	<?php include("inc/header.php"); ?>

	<?php include("inc/progress-indicator.php"); ?>	

	<div id="main" role="main">

		<div class="content-block">

			<div class="content-wrapper rounded-corners gradient-border clearfix">

				<a class="btn-prev" href="#">&lt;</a>
				<a class="btn-next disabled" href="#">&gt;</a>
				<div class="single-step">
                
				<h1 class='underline'>HERE ARE YOUR RECOMMENDED PACKAGES</h1>
				<h2 class="note"></h2>

				<table id="rec-packages" class="main">
					<thead>
						<tr>
							<th>Package</th>
							<th>Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
                    
					</tbody>
				</table>

				<div class="actions">
					<a href="#" class="btn"><em></em><span>VIEW ALL PACKAGES</span></a>
				</div>
                
            </div>
        </div><!-- /.content-wrapper -->

		</div><!-- /.content-block -->

	</div><!-- /#main -->

	<?php include("inc/footer.php"); ?>
	<?php include("inc/scripts.php"); ?>
	<?php include("inc/modals.php"); ?>	
	<script type="text/javascript" src="js/booking.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			IFLY.booking.init.step3();
		});

	</script>
	
</body>
</html>