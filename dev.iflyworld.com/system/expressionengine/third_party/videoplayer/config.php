<?php

if (! defined('VIDEOPLAYER_NAME'))
{
	define('VIDEOPLAYER_NAME', 'Video Player');
	define('VIDEOPLAYER_VERSION',  '3.1.1');
}

// NSM Addon Updater
$config['name'] = VIDEOPLAYER_NAME;
$config['version'] = VIDEOPLAYER_VERSION;
$config['nsm_addon_updater']['versions_xml'] = 'http://dukt.net/addons/video-player/release-notes.rss';
