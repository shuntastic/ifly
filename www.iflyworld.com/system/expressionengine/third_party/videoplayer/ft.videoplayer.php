<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'videoplayer/config.php';

/**
 * Video Player - by Dukt
 *
 * @package		Video Player
 * @version		Version 3.1.1
 * @author		Benjamin David
 * @copyright	Copyright (c) 2012 - Dukt
 * @license		http://dukt.net/addons/video-player/license
 * @link		http://dukt.net/addons/video-player/
 *
 */

class Videoplayer_ft extends EE_Fieldtype {

	var $info = array(
		'name'		=> VIDEOPLAYER_NAME,
		'version'	=> VIDEOPLAYER_VERSION
	);

	var $has_array_data = TRUE;

	/**
	 * Constructor
	 *
	 */
	function __construct()
	{

		parent::EE_Fieldtype();


		// load video player package

		$this->EE->load->add_package_path(PATH_THIRD . 'videoplayer/');


		// load language file

		$this->EE->lang->loadfile('videoplayer');


		// load helper

		if (! class_exists('Videoplayer_helper'))
		{
			require_once PATH_THIRD.'videoplayer/helper.php';
		}

		$this->helper = new Videoplayer_helper;


		// prepare cache for head files

		if (! isset($this->EE->session->cache['videoplayer']['head_files']))
		{
			$this->EE->session->cache['videoplayer']['head_files'] = false;
		}

	}

	// --------------------------------------------------------------------

	/**
	 * Install Function
	 *
	 * @access	public
	 * @return	array
	 */
	function install()
	{
		return array(
			'video_url'	=> '',
		);
	}

	// --------------------------------------------------------------------

	/**
	 * Display Field
	 *
	 * @access	public
	 * @param	array
	 * @return	array
	 */
	function display_field($data)
	{
		return $this->_display_field($data);
	}

	// --------------------------------------------------------------------

	/**
	 * Matrix Compatibility
	 *
	 * @access	public
	 * @param	array
	 * @return	array
	 */
	function display_cell($data)
	{
		return $this->_display_field($data, 'matrix');
	}

	// --------------------------------------------------------------------

	/**
	 * Common function for native and Matrix display field
	 *
	 * @access	private
	 * @param	array
	 * @param	string
	 * @return	string
	 */
	function _display_field($data, $type=false)
	{
		// load language file
		
		$this->EE->lang->loadfile('videoplayer');
		
		
		// load videoplayer library

		$this->EE->load->library('videoplayer_lib');
		$this->EE->load->model('videoplayer_model');


		$this->helper->include_resources();
		
		$site_id = $this->EE->config->item('site_id');
		$services = $this->EE->videoplayer_lib->get_services($site_id);

		$video_url = urldecode($data);

		$video_opts['url'] = $video_url;
		$video_opts['width'] = 376;
		$video_opts['height'] = 250;

		$vars['video'] = $this->EE->videoplayer_lib->get_video($video_opts);

		if($type == "matrix")
		{
			$vars['hidden_input'] = form_hidden($this->cell_name, $data);
		}
		else
		{
			$vars['hidden_input'] = form_hidden($this->field_name, $data);
		}


		// load accounts

		$vars['accounts'] = $this->EE->videoplayer_model->get_accounts();


		// check that at least one account is enabled and authenticated

		$vars['any_account_exists'] = false;

		foreach($vars['accounts'] as $k => $account)
		{
			if($account->enabled == true && $account->is_authenticated == true)
			{
				$vars['any_account_exists'] = true;
			}
		}

		return $this->EE->load->view('field/field', $vars, true);
	}

	// --------------------------------------------------------------------

	/**
	 * {video} - Rendering Tag & Tag Pair
	 *
	 * @access	public
	 * @return	string
	 */
	function replace_tag($data, $params = array(), $tagdata = FALSE)
	{
		// load videoplayer library

		$this->EE->load->library('videoplayer_lib');


		if(empty($data))
		{
			return "";
		}

		$params['url'] = $data;

		$this->video = $this->EE->videoplayer_lib->get_video($params);

		$video = $this->video;

		if($tagdata)
		{

			// rendering {video}{/video} pair

			if($video['video_found'])
			{

				// date format

				$local_date = $video['date'];

				if (preg_match_all("#".LD."date format=[\"|'](.+?)[\"|']".RD."#", $tagdata, $matches))
				{
					foreach ($matches['1'] as $match)
					{
						$tagdata = preg_replace("#".LD."date format=.+?".RD."#", $this->EE->localize->decode_date($match, $local_date), $tagdata, 1);
					}
				}

			}

			$conditionals = $this->EE->functions->prep_conditionals($tagdata, $video);

			$out = $this->EE->functions->var_swap($conditionals, $video);
		}
		else
		{
			// rendering {video}

			if($video['video_found'])
			{
				$out = $video['embed'];
			}
			else
			{
				$out = $video['error'];
			}
		}

		return $out;

	}

	// --------------------------------------------------------------------

	/**
	 * {video:single} Alias for replace_tag()
	 *
	 * @access	public
	 * @return	bool
	 */
	function replace_single($data, $params = array(), $tagdata = FALSE)
	{
		return $this->replace_tag($data, $params, $tagdata);
	}

	// --------------------------------------------------------------------

	/**
	 * {video:details} Alias for replace_tag()
	 *
	 * @access	public
	 * @return	bool
	 */
	function replace_details($data, $params = array(), $tagdata = FALSE)
	{
		return $this->replace_tag($data, $params, $tagdata);
	}

	// --------------------------------------------------------------------

	/**
	 * {video:pair} Alias for replace_tag()
	 *
	 * @access	public
	 * @return	bool
	 */
	function replace_pair($data, $params = array(), $tagdata = FALSE)
	{
		return $this->replace_tag($data, $params, $tagdata);
	}

	// --------------------------------------------------------------------

	/**
	 * Check if video exists
	 *
	 * @access	public
	 * @return	bool
	 */

	function replace_video_exists($data, $params=array(), $tagdata=false)
	{
		// load videoplayer library

		$this->EE->load->library('videoplayer_lib');

		$url = $data;

		return $this->EE->videoplayer_lib->video_exists($url);
	}

	// --------------------------------------------------------------------

	/**
	 * No Video Tag (DEPRECATED)
	 *
	 * @access	public
	 * @return	bool
	 */
	function replace_no_video($data, $params = array(), $tagdata = FALSE)
	{
		return false;
	}

	// --------------------------------------------------------------------

}
// END Videoplayer_ft class

/* End of file ft.videoplayer.php */
/* Location: ./system/expressionengine/third_party/videoplayer/ft.videoplayer.php */