// Booking functions
IFLY.booking = {
	api: '/api_curl.php',
	dataType: 'json',
	callApi: function(callStr,callbackfunc,onError,onComplete) {
		try {
			$.ajax({
				url: IFLY.booking.api,
				type: 'post',
				dataType: IFLY.booking.dataType,
				data: callStr,
				success: callbackfunc,
				error: (onError != null) ? onError : handler.onError,
				complete: (onComplete != null) ? onComplete : handler.onComplete
			});

		  } catch(err) {
			  console.log('CALLBACK ERROR', err);
		  //Handle errors here
		  }	
  },
  showApiErrorMessage: function(messaging) {
		$.colorbox({width:400,height:400,html:'<div class="timeout row">'+messaging+'</div><div class="row cont-shopping"><a href="/book-now" class="btn"><em></em><span>CONTINUE SHOPPING</span></a></div>'});  
  },
	getFlightOverviewPackages: function(tLoc,fType) {
			var today = new Date();
			var call='method=get_all_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&item_type=flight';
			IFLY.booking.callApi(call, success,null,null);
//			console.log('call',call);
			$('div.packages').html(IFLY.showLoader());
			function success(response, textStatus, jqXHR){
				sessionData = response.data;
				console.log('response',sessionData);
	 			$('div.packages').html('&nbsp;');
				$.each(sessionData, function(index,value) {
					var package = '<div class="package"><h3>'+ this.title + ' - $' + parseFloat(this.price_1).toMoney(2) + '</h3>\
										<p>'+this.flight_description +'</p>\
										<a href="#" class="btn green"><em></em><span>BOOK NOW</span></a>\
									</div>'
					$('div.packages').append(package);
				});
					$('div.package a.btn.green').each(function(index){
						$(this).click(function(e) {
	//						if(sessionData[index].padded_DCI =='FLIGHTS   BLOCKS    60MINBL1 ') {
							if(sessionData[index].padded_DCI =='FLIGHTS   BLOCKS    60MINBL1  ') {
								$.colorbox({ 
									html : $("#block-time").html(),
									width:"500px" 
								});
							} else {
								urlParams.padded_DCI = sessionData[index].padded_DCI;
								var serializedData = $.param(urlParams);
								var dURL = IFLY.booking.init.step2path + '?'+serializedData;
								window.location = dURL;
							}
							
							e.preventDefault();
						});
					});
			}
	},
	showRecPackages: function(tLoc,fType,fDate,fNum) {
			$('table#rec-packages').html(IFLY.showLoader());
			var call='method=get_recommended_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&flight_date='+fDate.yyyymmdd()+'&flyer_num='+fNum;

			IFLY.booking.callApi(call,success,null,null);
			function success(response, textStatus, jqXHR){
					var data = sessionData = response.data;
					IFLY.booking.displayPackages(data,null);

					$('a.btn.view-all span').html('VIEW ALL PACKAGES');
					$('a.btn.view-all').unbind('click').click(function(e) {
						IFLY.booking.showAllPackages(urlParams.tunnel,urlParams.code,new Date(urlParams.flyer_date), urlParams.flyer_num,'flight');
						e.preventDefault();
					});

			}
			
	},
	showAllPackages: function(tLoc,fType,fDate,fNum,iType) {
			$('table#rec-packages').html(IFLY.showLoader());
			var call='method=get_all_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&item_type='+iType;
			IFLY.booking.callApi(call, success,null,null);
			function success(response, textStatus, jqXHR){
					var data = sessionData = response.data;
					var messaging = response.message.split('||');

					$('a.btn.view-all span').html('VIEW RECOMMENDED PACKAGES');
					$('a.btn.view-all').unbind('click').click(function(e) {
						IFLY.booking.showRecPackages(urlParams.tunnel,urlParams.code,new Date(urlParams.flyer_date), urlParams.flyer_num);
						e.preventDefault();
					});
					
					IFLY.booking.displayPackages(data, messaging);
			}
	},
	//showGiftPackages: function(tLoc,fType,fNum) {
	showGiftPackages: function(tLoc,fType) {
			
			function success(response, textStatus, jqXHR){
					var data = sessionData = response.data;
//					console.log('gift card data',data);
					
					var messaging = response.message.split('||');

					$('table#rec-packages').html('<thead><tr><th>Package</th><th>Description</th><th>Price</th></tr></thead><tbody></tbody>');
					//$('#packages table.packages').html('<thead><tr><th>Package</th><th>Description</th><th>Price</th></tr></thead><tbody></tbody>');
					$.each(data, function(index,value) {
						var quantity = 1;
	//					var quantity = (this.qty) ? parseInt(this.qty) : parseInt(this.max_qty);
						var multiplier = (this.qty) ? parseInt(this.qty) : 1;
						var dropd = '';
						for(i = 1; i <=quantity; i++) {
							dropd += '<option value="'+i+'">'+i+'</option>';
						}
						var package = '<tr class="poption'+index+'"><td class="package">'+this.title+'</td><td class="desc">'+this.cart_short_description+'&nbsp;<a href="#" class="more-info tooltip ir" data-title="'+this.title+'" data-content="'+this.cart_long_description+'">more info</a></td>\
							<td class="price"><span class="amount">$'+(multiplier*parseFloat(this.price_1)).toMoney(2)+'<em class="asterisk">*</em></span><br /><form action="#" class="uniform tunnel-locator"><select name="1" class="quantity">'+dropd+'</select></form><input type="submit" value="ADD" class="add"></td></tr>'
							$('table#rec-packages tbody').append(package)
							
						//$('#packages table.packages tbody').append(package);
						
						//CHANGE QUANTITY & SET PRICE BASED ON NITIAL DROPDOWN VALUE
						if (this.qty && quantity > 1) {
							$('tr.poption'+index+' select[class$="quantity"] option:selected').removeAttr('selected');
							$('tr.poption'+index+' select[class$="quantity"] option[value="'+quantity +'"]').attr("selected","selected");
						}
						//UPDATE MESSAGE STATUS
						if(messaging != null) {
							$(' div.single-step h1.underline').html(messaging[0]);
							$('div.single-step div.intro p').html(messaging[1]);
						}
					});

					IFLY.initUniform();
					IFLY.initToolTip();
					$('form select.quantity').each(function(index) {
						$(this).change(function(){
							//urlParams.flyer_num = (parseFloat(sessionData[index].qty) == $(this).val()) ? sessionData[index].qty : '';
							var baseTotal = parseFloat(sessionData[index].price_1);
							var newTotal = $(this).val()*baseTotal;
							$('tr.poption'+index+' td.price span.amount').html('$'+newTotal.toMoney(2)+'<em class="asterisk">*</em>');
						});
					});
					$('input.add').each(function(index){
						$(this).unbind('click').click(function(e) {
							var sTotal = parseFloat(sessionData[index].price_1).toMoney(2);
							var totalPrice = parseFloat($('tr.poption'+index+' form select.quantity').val())*sTotal;
							var totalMinutes = parseFloat(sessionData[index].minutes);
							urlParams.select_total=totalPrice;
							urlParams.session_minutes=totalMinutes;
							urlParams.qty = $('tr.poption'+index+' form select.quantity').val();
							urlParams.padded_DCI = sessionData[index].padded_DCI;
							var serializedData = $.param(urlParams);
							//var dURL = IFLY.booking + '?'+serializedData;
							//window.location = dURL;
							
							IFLY.booking.addGiftToCartAndContinue();
							e.preventDefault();
							
							//return false;
						});
					});
					
					$('form.amount input.purchase').unbind('click').click(function(e) {
							if($('form.amount span.checked input[name="amount"]').hasClass('variable')) {
								urlParams.gc_amount = $('form.amount input[name="amount"].other').val();
							} else {
								urlParams.gc_amount = $('form.amount span.checked input').val();
							}
//							console.log('urlParams.gc_amount',urlParams.gc_amount);
							
//							var sTotal = parseFloat(sessionData[index].price_1).toMoney(2);
//							var totalPrice = parseFloat($('tr.poption'+index+' form select.quantity').val())*sTotal;
//							var totalMinutes = parseFloat(sessionData[index].minutes);
//							urlParams.select_total=totalPrice;
//							urlParams.session_minutes=totalMinutes;
//							urlParams.qty = $('tr.poption'+index+' form select.quantity').val();
//							urlParams.padded_DCI = sessionData[index].padded_DCI;
							urlParams.qty = 1;
							var serializedData = $.param(urlParams);
							//var dURL = IFLY.booking + '?'+serializedData;
							//window.location = dURL;
							
							IFLY.booking.addGiftToCartAndContinue();
							e.preventDefault();
							
							//return false;
						});
//					$('input.add').unbind('submit').submit(function(e){
//							
//						
//						return false;
//					});
				//	$.uniform.update();
			}
			$('table#rec-packages').html(IFLY.showLoader());
			var call='method=get_all_flights&controller=siriusware&format=json&tunnel='+tLoc+'&flyer_type='+fType+'&item_type=gift_certificate';
//			console.log('calling gift card ',call);
			function showError(response, textStatus, jqXHR) {
				console.log('ERROR RESPONSE', response);
				
			}
			IFLY.booking.callApi(call, success,showError,null);
			
	},
	showAllFlightPackages:function(response) {
			var data = sessionData = response.data;
			var messaging = response.message.split('||');

			$('a.btn.view-all span').html('VIEW RECOMMENDED PACKAGES');
			$('a.btn.view-all').unbind('click').click(function(e) {
				IFLY.booking.showRecPackages(urlParams.tunnel,urlParams.code,new Date(urlParams.flyer_date), urlParams.flyer_num);
				e.preventDefault();
			});
			
			IFLY.booking.displayPackages(data, messaging);
	},
	displayPackages: function(data,messaging) {
					//$('table#rec-packages tbody').html(' ');
					$('table#rec-packages').html('<thead><tr><th>Package</th><th>Description</th><th>Price</th><th>Quantity</th><th>Total</th><th>&nbsp;</th></tr></thead><tbody></tbody>');
					$.each(data, function(index,value) {
					//	console.log('displayPackages data', this);
	//					var quantity = (this.qty) ? parseInt(this.qty) : parseInt(this.max_qty);
						if(!isNaN(parseFloat(index))) {
							var quantity = (this.max_qty) ? parseInt(this.max_qty) : (this.qty) ? parseInt(this.qty) : 1;
							var multiplier = (this.qty) ? parseInt(this.qty) : 1;
							var dropd = '';
							for(i = 1; i <=parseInt(this.max_qty); i++) {
								dropd += (i == parseInt(this.qty)) ? '<option value="'+i+'" selected>'+i+'   </option>' : '<option value="'+i+'">'+i+'   </option>';
	//							dropd += '<option value="'+i+'">'+i+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>';
							}
							var package = '<tr class="poption'+index+'"><td class="package">'+this.title+'</td><td class="desc">'+this.cart_short_description+'&nbsp;<a href="#" class="more-info tooltip ir" data-title="'+this.title+'" data-content="'+this.cart_long_description+'">more info</a>	\
								</td><td>$'+parseFloat(this.price_1).toMoney(2)+'</td>	\
								<td><form action="#" class="uniform tunnel-locator">\
								<select name="1" class="quantity">'+dropd+'</select></form></td>\
								<td class="price_total">$'+(multiplier*parseFloat(this.price_1)).toMoney(2)+'</td>\
								<td><a href="'+IFLY.booking.init.step4path+'" class="btn green choose-package"><em></em><span>CHOOSE</span></a></td></tr>'
							$('table#rec-packages tbody').append(package);
							
							//CHANGE QUANTITY & SET PRICE BASED ON NITIAL DROPDOWN VALUE
//							if (this.qty && quantity > 1) {
//								$('tr.poption'+index+' select[class$="quantity"] option:selected').removeAttr('selected');
//								$('tr.poption'+index+' select[class$="quantity"] option[value="'+quantity +'"]').attr("selected","selected");
//							}
							//UPDATE MESSAGE STATUS
							if(messaging == null) {
								$(' div.single-step h1.underline').html('RECOMMENDED PACKAGES');
					//			var textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name;
								var textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+' at '+urlParams.tunnel_name;
								$('div.single-step h2.note').html(textStatus);
							} else {
								$(' div.single-step h1.underline').html(messaging[0]);
								$('div.single-step h2.note').html(messaging[1]);
	
							}
						}
					});

					IFLY.initUniform();
					IFLY.initToolTip();
					$('form select.quantity').each(function(index) {
						$(this).change(function(){
							urlParams.flyer_num = (parseFloat(sessionData[index].qty) == $(this).val()) ? sessionData[index].qty : '';
							var baseTotal = parseFloat(sessionData[index].price_1);
							var newTotal = $(this).val()*baseTotal;
							$('tr.poption'+index+' td.price_total').html('$'+newTotal.toMoney(2));
						});
					});
					$('a.choose-package').each(function(index){
						$(this).unbind('click').click(function(e) {
							
							if(sessionData[index].padded_DCI =='FLIGHTS   BLOCKS    60MINBL1  ') {
								$.colorbox({ 
									html : $("#large-parties").html(),
									width:"500px" 
								});
							} else {
								var sTotal = parseFloat(sessionData[index].price_1).toMoney(2);
								var totalPrice = parseFloat($('tr.poption'+index+' form select.quantity').val())*sTotal;
								var totalMinutes = parseFloat(sessionData[index].minutes)*parseFloat($('tr.poption'+index+' form select.quantity').val());
		//						var totalMinutes = parseFloat($('tr.poption'+index+' form select.quantity').val())*parseFloat(sessionData[index].minutes);
								if(sessionData[index].voucher_number) {
									//urlParams.tunnel = sessionData[index].tunnel;
									urlParams.voucher_number = sessionData[index].voucher_number;
									urlParams.flyer_type_name = sessionData[index].title;
								//	cookieHandler.create('ifly_giftcard_num',urlParams.voucher_number);	
								}
								urlParams.select_total=totalPrice;
								urlParams.session_minutes=totalMinutes;
								urlParams.qty = $('tr.poption'+index+' form select.quantity').val();
								urlParams.padded_DCI = sessionData[index].padded_DCI;
								
								var serializedData = $.param(urlParams);
								var full = window.location.host
	
								var dURL = 'https://'+full + IFLY.booking.init.step4path + '?'+serializedData;
		//						var dURL = $(this).attr('href') + '?'+serializedData;
								window.location = dURL;
							}
							
							e.preventDefault();
						});
					});
					$.uniform.update();
	},
	
	showAvailableTimes: function(x) {
					var data = x;
					var dates = [];
					var package = '';
//					console.log('data',data);
					
					function buildRow(x,y) {
						var z = '';
						var sessionClass = ((x.ecomm_available == 0 && x.session_available == 0) || (x.session_available == 0)) ? 'class="unavailable"':'';
						if(y%2 == 0) {
							z += '<div class="row">';
						} 
							z += '<em><input type="radio" name="time" value="'+x.session_time+'"><label '+sessionClass+'>'+x.session_label+'</label></em>';
						if(y%2 == 1) {
						z += '</div>';
						}
						return z;
					}
				var openTime = true;
				var display = [];
				//UNCOMMENT AND TEST AFTER DATA FIX
					var ctr = 0;
					var skipTime = false;
					$.each(data,function(index,value) {
						//console.log('showAvailableTimes data', value.length);
						if(value.length == undefined) {
							package += (ctr == 0) ? '<fieldset class="first"><legend>'+index+'</legend>' : '<fieldset><legend>'+index+'</legend>';
							$.each(value, function(index,value) {
								package += buildRow(value, index);
							});
							package += '</fieldset>'
							ctr++;
						} else {
							skipTime = true;
						}
					});
					
						package +='</fieldset><div id="legend" class="clearfix"><span class="green"><em></em>Your Time</span><span class="white"><em></em>Available Times</span><span class="black"><em></em>Unavailable Times</span></div><div class="actions clearfix"><input type="submit" class="book-now disabled" value="BOOK NOW"><p>Flight times are held for 10 minutes and are subject to availability.<br />You must arrive 1 hour before your flight time.</p></div>';
						$('#time-picker form').html(package);
						if(skipTime) {
								$('fieldset.first').addClass('no-late');
//							if($('fieldset.first').length >0) {
//								$('fieldset.first').addClass('no-late');
//							} else{
//								$('#time-picker form').html('<div>No Dates are available for this day.<br />Please select another date.');
//								$("#calendar-pickadate").datepicker("option", "minDate", 1);
//							}
						}
						
					$('body.step4 a.btn-next').addClass('disabled');
					$('#time-picker input.book-now, body.step4 a.btn-next').unbind('click').click(function(e) {
						if(!$(this).hasClass('disabled')) {	
							urlParams.session_time = $('#time-picker form input:checked').val();
							$('#time-picker form').html(IFLY.showLoader());
							IFLY.booking.addToCartAndContinue();
						}
						e.preventDefault();

					});
	},
	
	getAvailableTimes: function(tLoc,sessionMinutes,fDate) {
	
			$('#time-picker form').html(IFLY.showLoader())
			var call='method=get_sessions&controller=siriusware&format=json&tunnel='+tLoc+'&minutes='+sessionMinutes+'&date='+fDate.yyyymmdd();
			IFLY.booking.callApi(call, success,null,null);
			function success(response, textStatus, jqXHR){
					sessionData = response.data;
					IFLY.booking.showAvailableTimes(sessionData);
			}
	},
	addGiftToCartAndContinue: function() {
		$('#step-holder .single-step').html(IFLY.showLoader());
		
		if(urlParams.wwsale_id == null) {
			var call='method=create_wwsale_id&controller=siriusware&format=json&tunnel='+urlParams.tunnel;
			function success(response, textStatus, jqXHR){
					urlParams.wwsale_id = response.data;
					cookieHandler.create('wwsale_id',urlParams.wwsale_id);
					IFLY.booking.addToCart(true,true);
			}
			IFLY.booking.callApi(call, success,null,null);
	
		} else{
			IFLY.booking.addToCart(true,true);
		}
			
	},
	addToCartAndContinue: function() {
		if(urlParams.wwsale_id == null) {
			var call='method=create_wwsale_id&controller=siriusware&format=json&tunnel='+urlParams.tunnel;

			function success(response, textStatus, jqXHR){
						urlParams.wwsale_id = response.data;
						cookieHandler.create('wwsale_id',urlParams.wwsale_id);
						if(urlParams.voucher_number) {
							IFLY.booking.addToCart(true,true);
						} else {
							IFLY.booking.addToCart(true,false);
						}
			}
			IFLY.booking.callApi(call, success,null,null);
	
		} else{
			if(urlParams.voucher_number) {
				IFLY.booking.addToCart(true,true);
			} else {
				IFLY.booking.addToCart(true,false);
			}
		}
			
	},
	addToCart: function(contFlag,giftFlag) {
		if	(!urlParams.promo_code)
		urlParams.promo_code = '';
		
		if	(!urlParams.gc_amount)
		urlParams.gc_amount = 0;
		
		if	(!urlParams.token)
		urlParams.token = '';
		
		function dciSuccess(response, textStatus, jqXHR){
			urlParams.padded_DCI = response.data[0];
			callAdd('method=add_to_cart&controller=siriusware&format=json&voucher_number=&tunnel='+urlParams.tunnel+'&flyer_num='+urlParams.flyer_num+'&qty='+urlParams.qty+'&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+urlParams.padded_DCI+'&session_time=&flight_date=&gc_amount='+urlParams.gc_amount+'&token=');
		}
		
		
		if(giftFlag) {
			if (urlParams.gc_amount >0) {
				var dciCall = 'method=get_gift_card_dci&controller=siriusware&format=json&tunnel='+urlParams.tunnel;
				IFLY.booking.callApi(dciCall, dciSuccess,callError,null);
			} else {
				if (!urlParams.voucher_number) urlParams.voucher_number ='';
				if (!urlParams.flyer_date) urlParams.flyer_date ='';
				if (!urlParams.session_time) urlParams.session_time ='';
				
				callAdd('method=add_to_cart&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&flyer_num=&qty='+urlParams.qty+'&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+urlParams.padded_DCI+'&voucher_number='+urlParams.voucher_number+'&session_time='+urlParams.session_time+'&flight_date='+urlParams.flyer_date+'&gc_amount=&token=');
			}
			
		}	else {
			callAdd('method=add_to_cart&controller=siriusware&format=json&voucher_number=&tunnel='+urlParams.tunnel+'&flyer_num='+urlParams.flyer_num+'&qty='+urlParams.qty+'&wwsale_id='+urlParams.wwsale_id+'&padded_DCI='+urlParams.padded_DCI+'&session_time='+urlParams.session_time+'&flight_date='+urlParams.flyer_date+'&gc_amount='+urlParams.gc_amount+'&token='+urlParams.token);
	
		}
		
		//var call=(giftFlag) ?  : ;

		function callAdd(call) {
			//console.log('call',call);
			IFLY.booking.callApi(call, success,callError,null);

		}
		function success(response, textStatus, jqXHR){
//			console.log('addToCart:',response);
			if(response.status =="ERR") {
				cookieHandler.remove('wwsale_id');
				IFLY.booking.showApiErrorMessage(response.message)
			} else {
				if(contFlag) {
//					console.log('contFlag response.data',response.data);
					//urlParams.cartData = jQuery.parseJSON(response.data);
					
					try {
						delete urlParams.qty;
						delete urlParams.padded_DCI;
						delete urlParams.session_time;
						delete urlParams.session_minutes;
						delete urlParams.flyer_date;
						delete urlParams.gc_amount;
						delete urlParams.token;
					} catch(error) {
						console.log(error);	
					}
					IFLY.booking.loadCartPage();
				} else {
				//call modal
				}
			}
		}
		function callError(response, textStatus, jqXHR){
			var data = $(response.responseText)
			var locRef = $.inArray('data', data)
					console.log('locRef',locRef,data);
			if(locRef >-1) {
					var dataCap = $(data[locRef]);
					console.log('dataCap',dataCap);
			}
			
		}

	},
	
//LOAD STEP BOOKING 5
	loadCartPage: function() {
			var serializedData = $.param(urlParams);
		//	console.log('serializedData',serializedData);

			var full = window.location.host
			//window.location.host is subdomain.domain.com
			var dURL = 'https://'+full + IFLY.booking.init.step5path;
	//		var dURL = IFLY.booking.init.step5path+'?'+serializedData;
	
			window.location = dURL;
	},
//	loadCartPage: function() {
//			var serializedData = $.param(urlParams);
////			console.log('serializedData',serializedData);
////			var dURL = IFLY.booking.init.step5path;
//			$.ajax({
//				url: IFLY.booking.init.step5path,
//				type: 'post',
//				dataType: 'html',
//				data: serializedData,
//				success: function(response, textStatus, jqXHR){
//					var data = $(response);
//					var single = data.find('div.single-step').html();
//					single = $(single);
//					$('#main div.single-step').html(single);
//					IFLY.booking.getCartItems();
//					
//					$('body').removeClass('step3');
//					$('body').removeClass('step4');
//					$('body').addClass('step5');
//
//				},
//				error: handler.onError,
//				complete: handler.onComplete
//			});
//	},
	
	//Displays cart items on cart page, initialize buttons
	getCartItems: function() {
			var cartcall = 'method=get_cart&controller=siriusware&format=json&account=null&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id;
			
			$('table#your-cart tbody').html(IFLY.showLoader());
			IFLY.booking.callApi(cartcall, getCartObj,null,null);			
			function getCartObj(response, textStatus, jqXHR){
				urlParams.cartData = jQuery.parseJSON(response.data);
				//console.log('urlParams.cartData',urlParams.cartData);	

		//		console.log('getCartItems',urlParams.cartData);
				if(urlParams.cartData != null && urlParams.cartData.length > 0) {
					var output = '';
					var data = urlParams.cartData;
					var total_price = 0;
					var total_discount = 0;
					var total_tax = 0;
					var total_fee = 0;
					var resSummary = [];
	
					$.each(data, function(index,value) {
						var reservationInfo = '';
						var flyer_num = '-';
						if(this.options) {
							reservationInfo +='<ul class="resInfo">';
							$.each(this.options, function(index, value) {
								if(index=='flyer_num') {
									if(value!='' && value !="UNDEFINED") {
										flyer_num = value;
										reservationInfo += (isNaN(parseFloat(index))) ? '<li>'+index.replace(/_/g, " ")+': '+ value+'</li>' : '<li>'+value+'</li>' ;
									}
								} else {
										reservationInfo += (isNaN(parseFloat(index))) ? '<li>'+index.replace(/_/g, " ")+': '+ value+'</li>' : '<li>'+value+'</li>' ;
								}
							});
							reservationInfo +='</ul>';
						}
						
						
						var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(urlParams.tunnel);
						//+' at '+ selTunnelInfo.title
						var confirmationInfo = '<div class="resDetail">'+ this.name  + reservationInfo +'</div>'
						resSummary.push(confirmationInfo);
						//var pQty = (this.name =='iFLY Gift Card') ? '-' : this.qty;
						var package = '<tr class="item'+index+'"><td><span class="qty">'+this.qty+'</span><a href="javascript:void(0);" class="btn red remove"><em></em><span>REMOVE</span></a></td><td class="package"><h3>'+this.name+'</h3><p>'+reservationInfo+'</p></td><td class="desc"><p>'+this.cart_short_description+' <a href="#" class="tooltip ir" data-title="'+this.name+'" data-content="'+this.cart_long_description+'">more info</a></p></td><td><h3>'+flyer_num+'</h3></td><td class="tally">$'+(parseFloat(this.price)).toMoney(2)+'</td><td>$'+(parseFloat(this.price)*parseFloat(this.qty)).toMoney()+'</td></tr>'
							
							output+=package;
							
							var price = (this.qty != null) ? (this.price*parseFloat(this.qty)) : this.price;
							var tax = (this.tax != null) ? (this.tax) : 0;
							//var discount = (this.discount != null) ? (this.discount) : 0;
							var discount = (this.discount != null && this.qty != null) ? (this.discount*parseFloat(this.qty)) : this.discount;
							var fee = (this.fee != null) ? (this.fee) : 0
							
				
							total_price += price;
							total_tax += tax;
							total_discount += discount;
							total_fee += fee;
					});
					
					// DISPLAY CART SUMMARY
					var grand_total = urlParams.cart_total = total_price - total_discount + total_tax + total_fee;
					var summary='<table class="main"><tr class="sales-total"><th>Sales Total</th><td class="tally">$'+(total_price).toMoney(2)+'</td></tr>';
		
						summary += (total_discount > 0) ? '<tr class="discount"><th>Discount</th><td class="tally">$'+(total_discount).toMoney(2)+'</td></tr>' : '';
						summary += (total_discount > 0) ? '<tr class="sub-total"><th>Sub Total</th><td class="tally">$'+(total_price - total_discount).toMoney(2)+'</td></tr>' : '';
						summary += '<tr class="tax"><th>Tax</th><td class="tally">$'+(total_tax).toMoney(2)+'</td></tr>';
						summary += (total_fee > 0) ? '<tr class="fee"><th>Fee</th><td class="tally">$'+(total_fee).toMoney(2)+'</td></tr>' : '';
						summary += '<tr class="grand-total"><th>Grand Total</th><td class="tally">$'+(grand_total).toMoney(2)+'</td></tr></table>';
						//add submit button
						summary += '<form class="uniform"><input type="submit" value="checkout" class="checkout disabled"><label>I accept the restrictions<br />and requirements.</label><input type="checkbox" name="restrictions" value="yes"></form>'
		
						$('table#your-cart tbody').html(output);
						$('div#summary').html(summary);
						
						$('div#summary input[type="checkbox"]').change(function() {
							if(this.checked) {
								$('input.checkout').removeClass('disabled');
							} else {
								$('input.checkout').addClass('disabled');
							}
						})
						$('input.checkout').click(function(e) {
							
							//SET COOKIES FOR CONFIRMATION PAGE
							if(!$(this).hasClass('disabled')) {
								
								cookieHandler.create('ifly_username',$('#lgn input[name="username"]').val());
								cookieHandler.create('ifly_tunnel',urlParams.tunnel);
								cookieHandler.create('ifly_cart_total',(grand_total).toMoney(2));
								delete urlParams.voucher_number;

								var confirmationDetails = '';
								$.each(resSummary, function(index,value) {
									confirmationDetails += (index==0) ? value : ' and ' +value;
									confirmationDetails += (index == (this.length-1)) ? '.' : '';
								});
								
								cookieHandler.create('ifly_reservation_details',confirmationDetails);
//								console.log('cookie',cookieHandler.read('ifly_reservation_details'));
								
								var dURL = IFLY.booking.init.step6path;
								window.location = dURL;
	
								//CHECK IF THE USER IS LOGGED IN
								//DISABLED UNTIL LOGIN FUNCTIONALITY COMPLETED
		//						if(!$(this).hasClass('disabled') && IFLY.loggedIn()) {
	//								var dURL = IFLY.booking.init.step6path;
	//								window.location = dURL;
	//							} else if (!IFLY.loggedIn()) {
	//								$.colorbox({html:$(".modal.login").html(), onComplete:IFLY.booking.init.login});
	//								//IFLY.booking.init.login();	
	//							}
							}
							e.preventDefault();
						});
						
						$('#your-cart a.remove').each(function(index, value) {
							$(this).unbind('click').click(function() {
								var call='method=remove_from_cart&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&unique_id='+urlParams.cartData[index].id;
								IFLY.booking.callApi(call, success,null,null);
								function success(response, textStatus, jqXHR){
										//$('#your-cart tr.item'+index).remove();
										urlParams.cartData.splice(index, 1);
										//console.log('urlParams.cartData',urlParams.cartData);
										IFLY.booking.getCartItems();
									
								}
									
							});
							$('#promo-code form').unbind('submit').submit(function(e) {
								var $form = $(this);
								$inputs = $form.find("input");
								serializedData = $form.serialize();
//								console.log('serializedData',serializedData);
								//var cardNum=$('#gc-balance.overlay input:text[name=gc_num]').val();
								$('table#your-cart tbody').html(IFLY.showLoader());
								var call='method=apply_promo&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&'+serializedData;
								function success(response, textStatus, jqXHR){
										urlParams.cartData = jQuery.parseJSON(response.data);
										
										console.log('response',response);
										var discCheck = 0;
										$.each(urlParams.cartData, function(index,value) {
											discCheck += this.discount
										});
										if(discCheck == 0) {
											$.colorbox({width:400,height:300,html:'<div class="row"><p>Your promotion code is not valid for this type of flight item.</p></div>'});
										}
										IFLY.booking.getCartItems();
//										else {
//											IFLY.booking.getCartItems();
//										}
									//	$('#gc-balance div.status').html('Your remaining balance is $'+parseFloat(data.sp_bal_dy).toMoney(2)+' (expires '+new Date(data.expires)+')');
									//	$('#gc-balance div.status').html('Your remaining balance is $'+parseFloat(data.sp_bal_dy).toMoney(2));
									}
								function error(response, textStatus, jqXHR){
									console.log('error response',response);
									$.colorbox({width:400,height:300,html:'<div class="row"><p>Your Promo code was not found.</p></div>'+response});
								}
								console.log('call',call);
								IFLY.booking.callApi(call, success,error,null);
							return false;
							});
							
							
						});
						IFLY.initToolTip();
						IFLY.initUniform();
						$.uniform.update();
	
			} else {
					$('table#your-cart').html('<tr><td>Your Cart is Empty</td></tr>');
					$('div#summary').html('<table class="main"><tr class="sales-total"><th>Sales Total</th><td>$0.00</td></tr></table>');
					cookieHandler.remove('wwsale_id');

			}
		}
	},
//	togglePayments: function() {
//		$('fieldset.payment-method input').click(function() {
//			$('div.pay-option').each(function() {$(this).addClass('disabled')});
//			$('div.pay-option.'+$(this).val()).removeClass('disabled');
//			$.uniform.update();
//		});
//	},
	checkSession: function() {
		if(!urlParams.wwsale_id) {
		//	var a = cookieHandler.read('ifly_giftcard_num')
			var x = cookieHandler.read('wwsale_id');
			var y = cookieHandler.read('ifly_cart_total');
			var z =	cookieHandler.read('ifly_tunnel');
			
		//	urlParams.voucher_number = (a) ? a: ''
			urlParams.wwsale_id = (x) ? x: null;
			urlParams.cart_total = (y) ? y: null;
			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel: (z) ? z: null;
		}
	},
	checkForm: function(fName, tDiv) {
			switch(fName) {
				
				case 'step2':
				//BOOKING DETAILS PAGE
					if ($('a.btn.date span').html()!='FLIGHT DATE' &&$("#flyer-type").val()!='FLYER TYPE' && $("#how-many-fliers").val()!='# OF FLYERS' && $("#how-many-fliers").val()!='13' && $('select[id$="flyer-location"]').val() != 'FLIGHT LOCATION') {
						$('input.next, a.btn-next').removeClass('disabled');
					} else {
						$('input.next, a.btn-next').addClass('disabled');
					}
				break;	
				
				case 'tunnel-locator':
				//BOOKING DETAILS PAGE
					if (tDiv.find('a.btn.date span').html()!='FLIGHT DATE' &&tDiv.find("#flyer-type").val()!='FLYER TYPE' && tDiv.find("#how-many-fliers").val()!='# OF FLYERS' && tDiv.find("#how-many-fliers").val()!='13') {
						tDiv.find('input.find-flights').removeClass('disabled');
					} else {
						tDiv.find('input.find-flights').addClass('disabled');
					}
				break;	
				
				case 'step6':
				//CARD PAYMENT PAGE
				var $form = $('#process_sale input');
				$.each($form, function(index,value) {
						$(this).unbind('keyup').keyup(function(e) {
	//					$(this).unbind('focusin').unbind('focusout').focusin().focusout(function(e) {
						//console.log('CHECK FORM');
						if($('#process_sale').valid()) {
							$('input.process').removeClass('disabled');
						} else {
							$('input.process').addClass('disabled')	
						}
					});
				});
				
				break;	
				
			}
	},
getGiftBalance: function(tDiv) {
		// GIFT CARD BALANCE INIT
		if($(tDiv).length>0){
			$(tDiv).click(function(e) {
				var $form = $('.modal.balance').html();
				$.colorbox({html:$form,
							//width:700,
							height:320,
							onComplete:function() {
										//	$('#gc-balance div.status').html('');
										//	$('#gc-balance input.submit').css('display','block');
											$('#gc-balance form').validate();
											$('#gc-balance form').unbind('submit').submit(function(e) {
													if($('#gc-balance form').valid()) {
														var $form = $(this)
														$inputs = $form.find("input");
														serializedData = $form.serialize();
//														console.log(serializedData);
														//var cardNum=$('#gc-balance.overlay input:text[name=voucher_number]').val();
														var call='method=redeem_voucher&controller=siriusware&format=json&tunnel='+IFLY.current_tunnel+'&'+serializedData;
//														console.log('call',call);
														function success(response, textStatus, jqXHR){
//															console.log('response',response);
															var data = response.data;
															
															$('#gc-balance input.submit').css('display','none');
															
															if (response.message =='dollar_based') {
																var cardbalance = parseFloat(data);
																$('#gc-balance div.status').html('Your remaining balance is $'+Math.abs(cardbalance).toMoney(2));
																$('#gc-balance.overlay').append('<div class="row cont-shopping"><a href="/book-now/booking-step2/?tunnel='+response.tunnel+'&'+serializedData+'" class="btn"><em></em><span>CONTINUE SHOPPING</span></a></div>');
																
															} else {
//																var availPkgs = '';
																if (data.length >0) {
																	
																	var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(response.tunnel);
																	var localSite = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';
																	var dURL = localSite + IFLY.booking.init.step3path + '?'+serializedData+'&tunnel='+response.tunnel;
																	window.location = dURL;

//																	$.each(data, function(index,value) {
//																		availPkgs += this.title
//																		if(data.length > 1) {
//																			if(index == (data.length -2)) {
//																				availPkgs +=', or ';
//																			} else {
//																				availPkgs +=', ';
//																			}
//																		}
//																	});
//																	$('#gc-balance div.status').html('This card is good for'+availPkgs+'.');
//																	$('#gc-balance div.status').append('<div class="row continue"><a href="/book-now/booking-step3/?tunnel='+IFLY.current_tunnel+'&'+serializedData+'" class="btn"><em></em><span>CONTINUE SHOPPING</span></a></div>');
																} else {
																	$('#gc-balance div.status').html(response.message);
																}
															}
															//$.colorbox.resize();
														}
														function error(response, textStatus, jqXHR){
															console.log('response',response);
															$('#gc-balance div.status').html('This card number was not found.');
														}
														
														IFLY.booking.callApi(call,success,error,null)
													}
												return false;
											});
										},
									onClosed:function() {
										$('#gc-balance div.status').html('');
										$('#gc-balance input.submit').css('display','block');
									}
				});
				e.preventDefault();
			});
		}
	
}
}
IFLY.calendarWidget.initPickADateCalendar = function(date) {
		var today = new Date();
		if(urlParams.tunnel =='aus') today = new Date('January 15, 2013');
		
        $('#calendar-pickadate').datepicker({
            dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
            buttonImageOnly:true,
			minDate: today,
			defaultDate:date,
            onSelect: function(e) {
				//console.log('calendar click',e);
				urlParams.flyer_date = e;
				$('#time-picker form').html(IFLY.showLoader());
				var textStatus = '';
				if(!urlParams.voucher_number) {
					textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
	//				textStatus = 'For '+urlParams.flyer_num+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
				} else {
					textStatus = 'For '+urlParams.flyer_type_name.toLowerCase()+'<br />on '+today.showFullDate();
				}
				$('body.step4 h2.note').html(textStatus);
				sessionStatus = textStatus;
				$('html, body').animate({scrollTop: $('body.step4 #calendar-pickadate').offset().top}, 600);
				
				IFLY.booking.getAvailableTimes(urlParams.tunnel, urlParams.session_minutes, new Date(e));
			}
            //beforeShowDay:this.beforeShowDay
        });
}
IFLY.booking.init = {
	step1path:'booking-step1.php',
	step2path:'/book-now/booking-step2/',
	step3path:'/book-now/booking-step3',
	step4path:'/book-now/booking-step4/',
	step5path:'/book-now/booking-step5/',
	step6path:'/book-now/booking-step6/',
	step7path:'/book-now/booking-step7/',
	step3giftingPath:'/book-now/gifting-step3/',
	
//INITIALIZES OVERVIEW PAGE AND FLYER TYPE PAGES
	step1: function() {
		getParams();
		IFLY.current_tunnel = urlParams.tunnel = IFLY.locationHandler.getCurrentTunnel();
		//CLEAR OUT PREVIOUS SESSION COOKIES
		cookieHandler.remove('ifly_reservation');
		cookieHandler.remove('ifly_reservation_details');
	//	cookieHandler.remove('ifly_giftcard_num');

		//PACKAGES ADDED TO FLIER TYPE PAGE
		if($('body.booking.step1 .continue a').length>0){
			IFLY.booking.getGiftBalance('body.booking.step1 .continue a');
		}
		
		if($('body.booking.step1 div.packages').length >0) {
			if(!urlParams.code) {
				var sitePath = window.location.pathname.split('/');
				switch (sitePath[2]) {
					case 'first-time-flyer':
						urlParams.code="1ST_TIME";
					break;
					
					case 'group-or-party':
						urlParams.code="GROUPS";
					break;
					
					case 'return-flyer':
						urlParams.code="RETURN";
					break;
					
					case 'experienced-flyer':
						urlParams.code="BLOCKS";
					break;
				}
			}
			if($('#flyer-location').length > 0) {
			$('select[id$="flyer-location"] option:selected').removeAttr('selected');
			$('select[id$="flyer-location"] option[value="'+IFLY.current_tunnel+'"]').attr("selected","selected");
			$.uniform.update('select[id$="flyer-location"]');
			}
			// 
			IFLY.booking.getFlightOverviewPackages(IFLY.current_tunnel, urlParams.code);

			$('select[id$="flyer-location"]').change(function() {
				if($(this).val() != 'FLIGHT LOCATION') {
					IFLY.current_tunnel = urlParams.tunnel = $(this).val();
					IFLY.booking.getFlightOverviewPackages(IFLY.current_tunnel, urlParams.code);
				}
			});
			
			//REMOVE HARD CODE WHEN FLYER LOCATION DROP DOWN ADDED TO PAGE
			//IFLY.booking.getFlightOverviewPackages(IFLY.current_tunnel, urlParams.code);
			
		} else {
		//	var flyerTypes = $('article.flier-type');
			$('article.flier-type').each(function() {
				var currentURL = $(this).find('p a').attr('href');
				var newURL = currentURL;
				if($(this).data('code') != null || $(this).data('code') != undefined)
					newURL += '?code='+$(this).data('code');
				$(this).find('p a').attr('href',newURL);
			});
			
		}
		
	},
	step2: function() {
		getParams();
			if (urlParams.code) {
				$('select[id$="flyer-type"] option:selected').removeAttr('selected');
				$('select[id$="flyer-type"] option[value="'+urlParams.code+'"]').attr("selected","selected");
			}
			if(urlParams.flyer_num) {
				$('select[id$="how-many-fliers"] option:selected').removeAttr('selected');
				$('select[id$="how-many-fliers"] option[value="'+urlParams.flyer_num+'"]').attr("selected","selected");
			}
			
				//console.log('flyer-location',$('#flyer-location option').length, IFLY.current_tunnel);
				if(IFLY.current_tunnel) {
					//var tunOpts = $('select[id$="flyer-location" option]')
					var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
//					console.log('setTunnelDropdown selTunnelInfo',selTunnelInfo);
					$('body.booking.step2 .tunnel-info h2, body.booking.step2 .tunnel-info h2').html(selTunnelInfo.title);
					IFLY.locationHandler.updateAddress(IFLY.current_tunnel);
					$.uniform.update();
				}

				$('select[id$="flyer-location"]').change(function() {
					if($(this).val() != 'FLIGHT LOCATION' && $(this).val() != '') {
						IFLY.current_tunnel = urlParams.tunnel = $(this).val();
						IFLY.locationHandler.updateAddress(IFLY.current_tunnel);
						if($('#calendar-booking'))  {
							IFLY.calendarWidget.setCalendarDay('#calendar-booking');
							$('a.btn.date span').html('FLIGHT DATE');
							urlParams.flyer_date = null; 
						}
							IFLY.booking.checkForm('step2',null);
					}
				});
			$.uniform.update();
			if($('#calendar-booking')) {
				 IFLY.calendarWidget.calInitDate = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : 0;
				// console.log('calInitDate',calInitDate);
				IFLY.calendarWidget.getClosedDates();
				$('#calendar-booking').datepicker({
					dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
					showButtonPanel: false,
					buttonImageOnly:false,
					minDate: IFLY.calendarWidget.calInitDate,
					beforeShowDay: IFLY.calendarWidget.unavailable,
					closeText: "close",
					onSelect: function(e) {
						//console.log('e: '+e);
						$('a.date span').html(e)
						$(this).hide();	
						IFLY.booking.checkForm('step2',null);
					}
				});
				if(urlParams.flyer_date) {
					$('#calendar-booking').datepicker("setDate", new Date(urlParams.flyer_date));
					$('a.date span').html(urlParams.flyer_date);
				}

				$('#calendar-booking').hide();
				$('#booking-details a.date').click(function(e) {
					e.preventDefault(); 
					$('#calendar-booking').show();
				});
			}

			$('#booking-details select').change(function() {
				IFLY.booking.checkForm('step2',null);
			});
			$('input.next, a.btn-next').click(function(e) {
				if(!$(this).hasClass('disabled')) {
						urlParams.tunnel = IFLY.current_tunnel,
						urlParams.flyer_date= $('a.btn.date span').html(),
						urlParams.flyer_num=$("#how-many-fliers").val(),
						urlParams.code=$("#flyer-type").val(),
						urlParams.flyer_type_name=$('#flyer-type option:selected').html(),
						urlParams.tunnel_name=$('#flyer-location option:selected').html()
	
					//	cookieHandler.create('ifly_tunnel',urlParams.tunnel);

					var serializedData = $.param(urlParams);
					
					var full = window.location.host;
					var subCheck = full.split('.');
					//var localSite = '';
				//	if(subCheck[0] =='www' || subCheck[0] =='iflyworld') {
						var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
						var localSite = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';
				//	}
					
					var dURL = localSite + IFLY.booking.init.step3path+ '?'+serializedData;
					window.location = dURL;
				}
				e.preventDefault();
			});
	},
	step2gifting: function() {
		getParams();
		//console.log('step2gifting');
			IFLY.current_tunnel = urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : IFLY.locationHandler.getCurrentTunnel();
			IFLY.locationHandler.setTunnelDropdown();
			
			IFLY.booking.checkForm('step2',null);
			if (urlParams.tunnel) {
				$('select[id$="flyer-type"] option:selected').removeAttr('selected');
				$('select[id$="flyer-type"] option[value="'+urlParams.code+'"]').attr("selected","selected");
			}
			if (IFLY.current_tunnel) {
				//var tunnelRef= IFLY.tunnels.code.indexOf(urlParams.tunnel);	
				var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
			//	console.log('setTunnelDropdown selTunnelInfo',selTunnelInfo);
				$('body.booking.step2 .tunnel-info h2, body.booking.step2 .tunnel-info h2').html(selTunnelInfo.title);
				IFLY.locationHandler.updateAddress(IFLY.current_tunnel);
				$.uniform.update();
//				console.log('tunnelRef',IFLY.tunnels);
				$('select[id$="flyer-location"]').change(function() {
					IFLY.current_tunnel = urlParams.tunnel = $(this).val();
					IFLY.locationHandler.updateAddress(IFLY.current_tunnel);
				});
			}

			$('#booking-details select').change(function() {
				IFLY.booking.checkForm('step2',null);
			});
			$('input.next, a.btn-next').click(function(e) {
				if(!$(this).hasClass('disabled')) {
						urlParams.tunnel = $('#flyer-location').val(),
						//urlParams.flyer_num=$("#how-many-fliers").val(),
						urlParams.code=$("#flyer-type").val(),
						urlParams.flyer_type_name=$('#flyer-type option:selected').html(),
						urlParams.tunnel_name=$('#flyer-location option:selected').html()
						var serializedData = $.param(urlParams);
						//cookieHandler.create('ifly_tunnel',urlParams.tunnel);

						var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);
						var localSite = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';

							
						var dURL = localSite + IFLY.booking.init.step3giftingPath+'?'+serializedData;
						window.location = dURL;
				}
				e.preventDefault();
			});
	},
//RECOMMENDED PACKAGES PAGE INIT
	step3: function() {
			getParams();
			IFLY.booking.checkSession();

			urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : IFLY.locationHandler.getCurrentTunnel();
			//urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : IFLY.current_tunnel;

			if(urlParams.voucher_number) {
				if (urlParams.tunnel && urlParams.code&&urlParams.flyer_date&&urlParams.flyer_num) {
					$('select#change-location').val(urlParams.flyer_num);
					IFLY.booking.showRecPackages(urlParams.tunnel,urlParams.code,new Date(urlParams.flyer_date), urlParams.flyer_num);
				} else {
					var call='method=redeem_voucher&controller=siriusware&format=json&tunnel='+IFLY.locationHandler.getCurrentTunnel()+'&voucher_number='+urlParams.voucher_number;
//					console.log('call',call);
					function success(response, textStatus, jqXHR){
							console.log('response',response);
							var data = sessionData = response.data;
						//	urlParams.tunnel = response.tunnel;
							var messaging = response.message.split('||');
							cookieHandler.create('ifly_tunnel',urlParams.tunnel);
							$('div.single-step div.actions').html('<h2>You may apply your gift card when you checkout</h2>');
							
							IFLY.booking.displayPackages(data,messaging);
					}
				
				IFLY.booking.callApi(call, success,null,null);
			}
				
			} else if (urlParams.tunnel && urlParams.code&&urlParams.flyer_date&&urlParams.flyer_num) {
				$('select#change-location').val(urlParams.flyer_num);
				IFLY.booking.showRecPackages(urlParams.tunnel,urlParams.code,new Date(urlParams.flyer_date), urlParams.flyer_num);
			} else {
				var dURL = IFLY.booking.init.step2path; //+'?'+serializedData;
					window.location = dURL;
			}
		
	},
//RECOMMENDED GIFT CARD PACKAGES PAGE INIT
	step3gifting: function() {
			getParams();
//			IFLY.current_tunnel = urlParams.tunnel = (urlParams.tunnel) ? urlParams.tunnel : IFLY.locationHandler.getCurrentTunnel();
//			IFLY.locationHandler.setTunnelDropdown();
			if($('a#check-balance').length>0){
				IFLY.booking.getGiftBalance('a#check-balance');
			}

			IFLY.booking.checkSession();
				//			 if (urlParams.tunnel && urlParams.code&&urlParams.flyer_num) {
			if (urlParams.tunnel && urlParams.code) {
				//				$('select#change-location').val(urlParams.flyer_num);
				IFLY.booking.showGiftPackages(urlParams.tunnel,urlParams.code);
				//			IFLY.booking.showGiftPackages(urlParams.tunnel,urlParams.code, urlParams.flyer_num);
			} else {
				var dURL = IFLY.booking.init.step2giftingPath; //+'?'+serializedData;
				window.location = dURL;
			}
				
		},
//TIME PICKER PAGE INIT
	step4: function() {
			getParams();
		
			IFLY.booking.checkSession();
			IFLY.current_tunnel = urlParams.tunnel;
			if(urlParams.flyer_date=='' || urlParams.flyer_date==null) {
				var today = new Date();
				if (IFLY.current_tunnel == 'aus') {
				today = new Date('January 15, 2013');
				}else {
				today.setDate(today.getDate() + 7);
				}
				urlParams.flyer_date = today.mmddyyyy();
			}
			IFLY.initTimePicker();
			if (urlParams.tunnel && urlParams.session_minutes) {

//				$('select#change-location').val(urlParams.flyer_num);
				var textStatus = '';
				if(!urlParams.voucher_number) {
	//				textStatus = 'For '+urlParams.qty+' '+urlParams.flyer_type_name.toLowerCase()+((parseFloat(urlParams.flyer_num) > 1) ? 's' : '')+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
					textStatus = 'For '+urlParams.qty+' '+urlParams.flyer_type_name.toLowerCase()+' at '+urlParams.tunnel_name+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
				} else {
					textStatus = 'For '+urlParams.flyer_type_name.toLowerCase()+'<br />on '+new Date(urlParams.flyer_date).showFullDate();
				}
				sessionStatus = textStatus;
				$('body.step4 h2.note').html(textStatus);
				IFLY.calendarWidget.initPickADateCalendar(urlParams.flyer_date);
				IFLY.booking.getAvailableTimes(urlParams.tunnel, urlParams.session_minutes, new Date(urlParams.flyer_date));
				//showRecPackages: function(tLoc,fType,fDate)
			}
	},
//CART PAGE INIT
	step5: function() {
		getParams();
		IFLY.booking.checkSession();
		IFLY.booking.getCartItems();
	},
	//PROCESS SALE/PAYMENT PAGE
	step6: function() {
		getParams();
		IFLY.booking.checkSession();
		if (urlParams.cart_total) {
			if(parseFloat(urlParams.cart_total).toMoney(2) == 0.00)
				$('div.pay-option, fieldset.payment-method').each(function() {$(this).addClass('disabled')});
				$('.sale-submit').prepend('<label class="total-due">Total Due: $'+parseFloat(urlParams.cart_total).toMoney(2)+'</label>');	
		}
		
		if(urlParams.voucher_number) {
//			IFLY.pay.initGC();
//			$('fieldset.payment-method input:radio[value="gc"]').attr('checked',true);
//			$('fieldset.gc-info input.text').attr('value',urlParams.voucher_number);
//			$.uniform.update('fieldset.payment-method');
			
		} else {
			IFLY.pay.initCCselector();
			IFLY.pay.initCreditCard();
		}
		
		IFLY.pay.togglePayments();
		
		IFLY.booking.checkForm('step6',null);

	//	urlParams.guest_id = (cookieHandler.read('guest_id') != '' && cookieHandler.read('guest_id') != null) ? cookieHandler.read('guest_id') : null;
	
		//GET CART TOTAL
		IFLY.booking.callApi('method=get_cart&controller=siriusware&format=json&account=null&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id, IFLY.pay.getCartObj,null,null);			

		//IFLY.pay.remainingBalance = urlParams.cart_total;
		
//		IFLY.pay.initCreditCard();
		if(!IFLY.loggedIn()) $('.log-req, fieldset.save-cc').each(function(){$(this).css('display','none')});
		
		
	},
	
//CONFIRMATION PAGE INIT
	step7: function() {
		getParams();
		IFLY.current_tunnel = urlParams.tunnel = (urlParams.tunnel != null) ? urlParams.tunnel : IFLY.locationHandler.getCurrentTunnel();
		var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(IFLY.current_tunnel);

		cookieHandler.remove('ifly_tunnel');
		//FORMATTING RESERVATION CONFIRMATION INFO
		var reservationNum =cookieHandler.read('ifly_reservation');
//		var tunnel =cookieHandler.read('ifly_tunnel');
		var resInfo =cookieHandler.read('ifly_reservation_details');
	//	console.log('resInfo',resInfo);
		var confirmationMessage = 'Thank you for your purchase!';
		confirmationMessage+= (resInfo != null) ? '<br />Your purchase includes:'+resInfo :'';
		confirmationMessage+='<br />Your reservation number is: <span>'+reservationNum+'</span>';
		
		$('h2#res-info').html(confirmationMessage);

		if($('a.btn.map-locator').length >0) {
				var mapURL = selTunnelInfo.map.replace(/&amp;/g, "&");
				$('a.btn.map-locator').attr('href',mapURL)	
		}
		$('a.add-email').unbind('click').click(function(e) {
			$('fieldset.send-email').prepend('<div class="row extra-email"><input type="text" name="email" class="text" value="ENTER EMAIL ADDRESS"></div>');
			e.preventDefault();
		});
		function emailSent(response, textStatus, jqXHR) {
				$.colorbox({width:600,height:500,html:'<div class="row continue"><h2>Your emails have been sent</h2><a href="/book-now" class="btn"><em></em><span>CONTINUE SHOPPING</span></a><br /><a href="/" class="btn"><em></em><span>RETURN TO HOME</span></a></div>'});
		}
		$('#email-waiver').unbind('submit').submit(function(e) {
				$form = $(this);
				$inputs = $form.find("input");
				serializedData = $form.serialize();
				console.log('serializedData');
				IFLY.booking.callApi('method=send_email&controller=siriusware&format=json&from_email=admin@iflyworld.com&'+serializedData, emailSent,null,null);			
				return false;
		});
		$('a.waiver').each(function() {
			var	newURL = $(this).attr('href')+'?res='+urlParams.res+'&guest='+urlParams.guest+'&tunnel='+urlParams.tunnel;
			$(this).attr('href',newURL);
		});
		$('a.print-confirm').click(function(e){
			var disp_setting="toolbar=yes,location=no,directories=yes,menubar=yes,"; 
			disp_setting+="scrollbars=yes,width=650, height=600, left=100, top=25"; 
			var content_vlue = document.getElementById("main").innerHTML; 
			var docprint=window.open("","",disp_setting); 
			docprint.document.open(); 
			docprint.document.write('<html><head><title>iFly Reservation Confirmation</title>'); 
			docprint.document.write('</head><body onLoad="self.print()"><center>');          
			docprint.document.write(content_vlue);          
			docprint.document.write('</center></body></html>'); 
			docprint.document.close(); 
			docprint.focus(); 
			e.preventDefault();
		});
		//FACEBOOK
		function faceCallback(response) {
			console.log('callback '+response);
		  //document.getElementById('msg').innerHTML = "Post ID: " + response['post_id'];
		}
		$('a.facebook').click(function(e) {
			var obj = {
				  method: 'feed',
				  link: 'http://www.iflyworld.com',
				  //picture: 'http://http://209.114.36.146/wp-content/themes/seedmatters/images/packets/packet_img_'+index+'.png',
				  name: 'iFly Indoor Skydiving',
				  /*caption: 'Reference Documentation',*/
				  description: 'iFLY is a state of the art vertical wind tunnel that provides a safe way to experience true human body-flight. We fly people from 3 to 103 years old. No experience necessary and all training is included. Make your dreams a reality!'
			};
			e.preventDefault();
			FB.ui(obj, faceCallback);
		});
		
	},
	login: function() {
		$.uniform.update('form#lgn, form#register-ifly-acct');
	$('form#lgn').data('validator', null).validate({
		   rules: {
			 web_user: "required",
			 password: "required"
		   },
		   messages: {
			 web_user: "Please enter your username",
			 password: "Please enter your password"
		   }
		});
		$('form#register-ifly-acct').validate({
		   rules: {
			 web_user: "required",
			 password: "required"
		   },
		   messages: {
			 web_user: "Please enter a username",
			 password: "Please enter a password"
		   }
		});
		
		
		$('#lgn a.login').unbind('click').click(function(e) {
			if($('form#lgn').valid()) {
				$form = $('form#lgn');
				$inputs = $form.find("input");
				serializedData = $form.serialize();
				var call='method=log_in&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&'+serializedData;
				console.log('call',call);
				IFLY.booking.callApi(call,loginConfirmed,badLogin,badLogin);
			}
			function loginConfirmed(response, textStatus, jqXHR) {
				console.log('loginConfirmed',response);
				$.colorbox.close();
				var dURL = IFLY.booking.init.step6path;
				window.location = dURL;
			}
			function badLogin(response, textStatus, jqXHR) {
				console.log('badLogin',response);
				$('#cboxLoadedContent div.status p').html(response.message);
			}
			e.preventDefault();
		});

		//CHECKOUT AS A GUEST
//		$('#login.overlay a.guest').click(function(e) {
//			//var serializedData = $.param(urlParams.cartData.id);
//			var dURL = IFLY.booking.init.step6path; //+'?'+serializedData;
//			console.log('dURL',dURL);
//			window.location = dURL;
//			e.preventDefault();
//		});
		$('a.btn.myifly').click(function(e) {
			//$('form#register-ifly-acct').css('display','block');
			$('form#register-ifly-acct').fadeIn(300);
			$('form#lgn').fadeTo(300, .15);
//		$.uniform.update();
			
			$('#register-ifly-acct a.register').unbind('click').click(function(e) {
		//		if($('form#register-ifly-acct').valid()) {
					console.log('clicked');
					$form = $('form#register-ifly-acct');
					$inputs = $form.find('input');
					serializedData = $form.serialize();
					var call='method=new_guest&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&'+serializedData;
					console.log('call',call);
		//		}
//				function setPassword(response, textStatus, jqXHR){
//					cookieHandler.create('ifly_userID',response.data);
//					console.log
//					var call='method=set_password&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&'+serializedData+'&token='+response.data;
//					IFLY.booking.callApi(call,continuetoCheckout,registrationError,null);
//					console.log('setPassword call',call);
//				}
				function continuetoCheckout(response,textStatus, jqXHR) {
					console.log('continuetoCheckout response:',response);
					cookieHandler.create('guest_id',response.data);
					$('#cboxLoadedContent div.status p').html('Registration complete! Continuing to checkout.');
					setTimeout(continueCheckout,2000)
					function continueCheckout() {
						var dURL = IFLY.booking.init.step6path;
						window.location = dURL;
					}
				}
				function registrationError(response, textStatus, jqXHR) {
					$('#cboxLoadedContent div.status p').html(response.message);
					console.log('registrationError',response);
				}
				IFLY.booking.callApi(call,continuetoCheckout,registrationError,null);
				e.preventDefault();
			
			});
//			row.animate({marginTop:-84, height:84},800, function() {
//				$(this).append('<form id="register-ify-acct" name="register" method="post" class="validate" novalidate="novalidate"><fieldset><div class="clearfix"><input type="text" name="e_mail" class="text required username" size="6" value="EMAIL"><input type="text" name="password" class="text required password" size="6" value="PASSWORD"><a href="#" class="btn login"><em></em><span>REGISTER</span></a></div></fieldset></form>');
//			});
			
			e.preventDefault();
		});

	},
	
	//GIFT CARD REDEEM
	redeemGiftCard: function() {
			$('form#redeem').submit(function(e) {
				var $form = $(this)
				$inputs = $form.find("input");
				var passSerialized
				serializedData = passSerialized = $form.serialize();
				$(this).append(IFLY.showLoader());

				console.log(serializedData);
				var call='method=redeem_voucher&controller=siriusware&format=json&tunnel='+IFLY.locationHandler.getCurrentTunnel()+'&'+serializedData;
				console.log('call',call);
				IFLY.booking.callApi(call, success,null,null);
	
				function success(response, textStatus, jqXHR){
						console.log('response',response);
						IFLY.killLoader();
						sessionData = response.data;
						if(response.message =='dollar_based') {
							var cardbalance = parseFloat(sessionData);
//							var amountStatus = '<p>Your remaining balance is $'+Math.abs(cardbalance).toMoney(2)+'</p><div class="row cont-shopping"><a href="/book-now/booking-step2/?tunnel='+IFLY.current_tunnel+'&'+serializedData+'" class="btn"><em></em><span>CONTINUE SHOPPING</span></a></div>';
// VOUCHER NUMBER DOESN'T NEED TO BE PASSED FOR DOLLAR BASED VOUCHERS

							var amountStatus = '<p>Your remaining balance is $'+Math.abs(cardbalance).toMoney(2)+'</p><div class="row cont-shopping"><a href="/book-now/booking-step2/?tunnel='+IFLY.current_tunnel+'" class="btn"><em></em><span>CONTINUE SHOPPING</span></a></div>';
							$.colorbox({html:amountStatus,width:400,height:300, onComplete:function() {/**/}});

							//IFLY.booking.showApiErrorMessage(response.message);
						} else if (response.status =="ERR") {
							IFLY.booking.showApiErrorMessage(response.message);
						} else {
							var selTunnelInfo = IFLY.locationHandler.getLocationInfoByCode(response.tunnel);
							var localSite = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';

							var dURL = localSite + IFLY.booking.init.step3path + '?'+serializedData+'&tunnel='+response.tunnel;
							window.location = dURL;
						}
				}
				
				function error(response, textStatus, jqXHR){
					var data = $(response.responseText);
					var package = $(data[1]);
					console.log('response',package);

					$('#gc-balance div.status').html('This card number was not found.');
				}
				return false;
			});
		
			function initRedeem() {
					console.log('initRedeem');
			}
	}
	
}
IFLY.initTimePicker = function () {
		$("#time-picker").delegate("label", "click", function () {
		var c = $(this).attr('class');
			if ( c == undefined) {
				
				var e = $("#time-picker").find('label.your-time');
				var s = e.siblings('input');
				e.removeAttr('class');
				s.removeAttr('checked');
				$(this).addClass('your-time');
				$(this).siblings('input').attr('checked',true);
				$('body.step4 h2.note').html(sessionStatus + ' at ' + $(this).html());
				//sessionStatus = sessionStatus + ' at ' + $(this).html();
				if($('#time-picker input.book-now').length >0) {
					$('#time-picker input.book-now').removeClass('disabled');
					$('body.step4 a.btn-next').removeClass('disabled');
				}
				$('html, body').animate({scrollTop: $('body.step4 #time-picker').offset().top}, 600);
	//			$('html, body').animate({scrollTop: $('body.step4 h2.note').offset().top}, 600);
			} 
		});
}
IFLY.pay = {
	paymentSummary:[],
	remainingBalance: 0 ,
	totalSubmitted: 0 ,
	method: 'cc',
	getCartObj: function(response, textStatus, jqXHR){
			urlParams.cartData = jQuery.parseJSON(response.data);
	//		console.log('urlParams.cartData',urlParams.cartData);	
				if(urlParams.cartData != null && urlParams.cartData.length > 0) {
					var output = '';
					
					var data = urlParams.cartData;
					var total_price = 0;
					var total_discount = 0;
					var total_tax = 0;
					var total_fee = 0;
					var resSummary = [];
					$.each(data, function(index,value) {
//						console.log('this',this);
							var price = (this.qty != null) ? (this.price*parseFloat(this.qty)) : this.price;
							var tax = (this.tax != null) ? (this.tax) : 0;
						//	var discount = (this.discount != null) ? (this.discount) : 0;
							var discount = (this.discount != null && this.qty != null) ? (this.discount*parseFloat(this.qty)) : this.discount;
							var fee = (this.fee != null) ? (this.fee) : 0
							
							total_price += price;
							total_tax += tax;
							total_discount += discount;
							total_fee += fee;
					});
					
					// DISPLAY CART SUMMARY
					urlParams.cart_total = total_price - total_discount + total_tax + total_fee;
					IFLY.pay.remainingBalance = urlParams.cart_total = urlParams.cart_total;
					IFLY.pay.remainingBalance = parseFloat(IFLY.pay.remainingBalance).toMoney(2)
				}
		},
	sale_processed: function(response, textStatus, jqXHR){
						console.log('response: SALE PROCESSED',response);
						//urlParams.cartData = jQuery.parseJSON(response.data);
						if(response.status =='ERR') {
							IFLY.booking.showApiErrorMessage(response.message);
						} else {
							var confirmData = response.data
							urlParams.guest = confirmData.encrypted_guest_no;
							urlParams.reservation_confirmation = confirmData.wwsale_id;
							//cookieHandler.remove('ifly_giftcard_num');
							cookieHandler.create('ifly_reservation',urlParams.reservation_confirmation);
							delete urlParams.cart_total;
							
							var call='method=destroy_cart&controller=siriusware&format=json&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id;
							$.ajax({
								url: IFLY.booking.api,
								type: 'post',
								dataType: IFLY.booking.dataType,
								data: call,
								success: IFLY.pay.confirm_reservation, 
								error: function(response, textStatus, jqXHR){
									console.log('error response',response);
								},
								complete: handler.onComplete
							});
						}
					},

	confirm_reservation: function(response, textStatus, jqXHR){
							console.log('response',response);
							cookieHandler.remove('wwsale_id');
							var dURL = IFLY.booking.init.step7path+'?res='+urlParams.reservation_confirmation+'&guest='+urlParams.guest;
							console.log('dURL',dURL);
							window.location = dURL;
						},
	updateMultiTotal: function() {
		IFLY.pay.remainingBalance = parseFloat(IFLY.pay.remainingBalance).toMoney(2);
		console.log('IFLY.pay.remainingBalance',IFLY.pay.remainingBalance);
		
		var paySummary = '<div class="total-text"><h3>Sale Total:</h3></div><div class="total-sale"><h3>$'+parseFloat(urlParams.cart_total).toMoney(2)+'</h3></div><br /><div class="total-text"><h3>Payments:</h3></div><div class="total-pay"><h3>$'+parseFloat(IFLY.pay.totalSubmitted).toMoney(2)+'</h3></div><br /><div class="total-text"><h3>Remaining Balance:</h3></div><div class="total-remain"><h3>$'+parseFloat(IFLY.pay.remainingBalance).toMoney(2)+'</h3></div>';
		$('div.payment-summary .pay-total').html(paySummary);
		if(IFLY.pay.remainingBalance >0) {
			$('input.process').addClass('disabled');
		} else {
			$('input.process').removeClass('disabled');
		}
		
	},
	togglePayments: function() {
		$('fieldset.payment-method input').click(function() {
			$('div.pay-option').each(function() {$(this).addClass('disabled')});
			$('#process_sale div.pay-fields div.pay-option.'+$(this).val()).removeClass('disabled');
			
//			$('#process_sale div.pay-fields').html($('div.pay-option.'+$(this).val()).html());
			IFLY.pay.method = $(this).val();
			switch($(this).val()) {
				case 'cc':
					IFLY.pay.initCreditCard();
//					$('.sale-submit label.total-due').css('display','block');
				break;
				
				case 'multiple':
					IFLY.pay.initMultiple();
//					$('.sale-submit label.total-due').css('display','none');
				break;
				
				case 'gc':
					IFLY.pay.initGC();
//					$('.sale-submit label.total-due').css('display','block');
				break;
				
				case 'cc-onfile':
					IFLY.pay.initCConfile();
//					$('.sale-submit label.total-due').css('display','block');
				break;
				
				case 'ifly-account':
					IFLY.pay.initAccount();
//					$('.sale-submit label.total-due').css('display','block');
				break;
				
			}

			// $("select, input:checkbox, input:radio, input:file").update(); 
			$('.pay-fields').uniform();
			IFLY.booking.checkForm('step6',null);
			
		});
	},
	initCreditCard: function() {
		$('#process_sale').not('.pay-options.disabled').data('validator', null).validate({
		   rules: {
			 first_name: "required",
			 last_name: "required",
			 address: "required",
			 cell_phone: "required",
			 zip: "required",
			 e_mail: {
			   required: true,
			   email: true
			 },
			 credit_card_no: {
				 required: true,
				 creditcard: true
			 },
			 exp_month: "required",
			 exp_year: "required",
			 cvv2: "required"
		   },
		   messages: {
			 first_name: "First name is required",
			 last_name: "Last name is required",
			 address: "Address is required",
			 zip: "required",
			 cell_phone: "A phone number is needed",
			 exp_month: "required",
			 exp_year: "required",
			 cvv2: "required",
			 e_mail: {
			   required: "A valid email address is needed"
			 }
		   }
		});

		$('#process_sale').unbind('submit').submit(function(e) {
			if($(this).valid()){
				$form = $('#process_sale');
				$('.disabled').remove();
				$inputs = $form.find("input");
				//serializedData = $form.serialize();
				
				var $formObj = $inputs.serializeObject();
				//$('#process_sale select#month :selected').val();
				
				if(!$formObj.credit_card_no) $formObj.credit_card_no = '';
				
				var cell_phone = $formObj.cell_phone1+'-'+$formObj.cell_phone2+'-'+$formObj.cell_phone3;
				var processcall='method=process_sale&controller=siriusware&format=json&account=&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&amount_1='+parseFloat(urlParams.cart_total).toMoney(2)+'&cell_phone_1='+cell_phone+'&first_name_1='+$formObj.first_name+'&last_name_1='+$formObj.last_name+'&address_1='+$formObj.address+'&zip_1='+$formObj.zip+'&e_mail_1='+$formObj.e_mail+'&credit_card_no_1='+$formObj.credit_card_no+'&exp_month_1='+$('#process_sale select#month :selected').val() +'&exp_year_1='+$('#process_sale select#year :selected').val() +'&cvv2_1='+$formObj.cvv2+'&amount_2=&first_name_2=&last_name_2=&address_2=&zip_2=&e_mail_2=&cell_phone_2=&credit_card_no_2=&exp_month_2=&exp_year_2=&cvv2_2=&amount_3=&first_name_3=&last_name_3=&address_3=&zip_3=&e_mail_3=&cell_phone_3=&credit_card_no_3=&exp_month_3=&exp_year_3=&cvv2_3=&amount_4=&first_name_4=&last_name_4=&address_4=&zip_4=&e_mail_4=&cell_phone_4=&credit_card_no_4=&exp_month_4=&exp_year_4=&cvv2_4=&zip=';
				
				console.log('call',processcall);
				IFLY.booking.callApi(processcall, IFLY.pay.sale_processed,null,null);
				$('.sale-submit').html(IFLY.showLoader());
			}
			
			return false;
		});
		
	},
	initMultiple: function() {
		$('#process_sale').not('.pay-options.disabled').data('validator', null).validate({
		   rules: {
			 first_name: "required",
			 last_name: "required",
			 address: "required",
			 zip: "required",
			 cell_phone1: "required",
			 cell_phone2: "required",
			 cell_phone3: "required",
			 e_mail: {
			   required: true,
			   email: true
			 },
			 credit_card_no: {
				 creditcard: true
			 },
			 credit_card_no: {
				 creditcard: true
			 },
			 exp_month: "required",
			 exp_year: "required",
			 cvv2: "required"
		   },
		   messages: {
			 first_name: "First name is required",
			 last_name: "Last name is required",
			 address: "Address is required",
			 zip: "required",
//			 cell_phone1: "A phone number is needed",
			 exp_month: "required",
			 exp_year: "required",
			 cvv2: "required",
			 e_mail: {
			   required: "A valid email address is needed"
			 }
		   }
		});
		function showPaymentSummary() {
			if(!$('div.payment-summary .tally').length > 0) {
					$('div.payment-summary').html('<h3>Payment Summary</h3><div class="tally"><div class="payment-title"><h3>&nbsp;</h3><h3>Card Type</h3><h3>Card Number</h3><h3>Payment Amount</h3></div><div class="items"></div><div class="pay-total"></div></div>');
			}
			
			if(IFLY.pay.paymentSummary.length > 0) {
				$('div.payment-summary div.items').html('');
				$.each(IFLY.pay.paymentSummary, function(index, value){
				//	$this = $(value);
				//	var $form = $this.serializeObject();
					var $form = value;
					console.log('$form.credit_card_no',$form.credit_card_no);

					var paymethod = ($form.credit_card_no) ? 'Credit Card' : 'Gift Card';
					
					var transactionString = '<div class="line-item"><div><input type="button" data-amount="'+$form.amount+'" value="REMOVE" class="remove"></div><div class="card-type"><h3>'+paymethod+'</h3></div><div class="card-num">'
					transactionString += ($form.credit_card_no) ? '<h3>**** **** **** '+($form.credit_card_no.toString()).substr($form.credit_card_no,-4) : $form.amount;
					
					transactionString += '</h3></div><div class="card-total"><h3>$'+parseFloat($form.amount).toMoney(2)+'</h3></div></div>';
					$('div.payment-summary div.items').append(transactionString);
				});
			}

			$('input.remove').each(function(index,value) {
				$(this).unbind('click').click(function(e) {

					var remAmount = $(this).data('amount');
					IFLY.pay.remainingBalance += remAmount;
					IFLY.pay.totalSubmitted -= remAmount;
					IFLY.pay.updateMultiTotal();
					IFLY.pay.paymentSummary.splice(index);
					showPaymentSummary();
					//$(this).parent('.line-item').remove();
					return false;
				});
			});
			
			var nameBase = IFLY.pay.paymentSummary.length +1;

	}
		
		$('input[name="gift_cert"]').change(function() {
			if($(this).is(':checked')) {
				$('.multiple.pay-option .cc-info').attr("disabled", true).addClass('disabled');
				$('.multiple.pay-option .gc-info').attr('disabled', false).removeClass('disabled');
			} else {
				$('.multiple.pay-option .cc-info').attr('disabled', false).removeClass('disabled');
				$('.multiple.pay-option .gc-info').attr('disabled', true).addClass('disabled');
			}
		   $.uniform.update();
		})

		$('#process_sale').delegate('input.apply','click',  function(e){
			if($('#process_sale').valid()){
				var tHtml = $('#process_sale').html();
				$('.disabled').remove();
				$form = $('#process_sale');
				//$inputs = $form.not('.disabled');
				$inputs = $form.find('input')
				var $inputObj = $inputs.serializeObject();
				console.log('$inputObj',$inputObj)
				//$inputs = $fields.find('input');
				$inputObj.amount = ($inputObj.amount > IFLY.pay.remainingBalance) ? IFLY.pay.remainingBalance : parseFloat($inputObj.amount).toMoney(2);

				IFLY.pay.paymentSummary.push($inputObj);
				
				$('#process_sale').html(tHtml)
				$('#process_sale input:radio[value="multiple"]').attr('checked','checked');
				
				showPaymentSummary();
				
				IFLY.pay.remainingBalance -= parseFloat($inputObj.amount).toMoney(2);
				IFLY.pay.totalSubmitted += parseFloat($inputObj.amount);
				IFLY.pay.updateMultiTotal();
				
//				if(IFLY.pay.remainingBalance > 0) {
					$('#process_sale').get(0).reset();
					$.uniform.restore("select, input:radio, input:file, input:checkbox");
					IFLY.initUniform();
//				} else {
//					$('input.process').addClass('cancel');	
//				}
//				   $.uniform.update('#process_sale input, #process_sale select');
			}
				e.preventDefault();
				if(IFLY.pay.remainingBalance <= 0) $('#process_sale').data('validator', null);
		});

		$('#process_sale').unbind('submit').submit(function(e) {
	//		if($(this).valid()&& IFLY.pay.remainingBalance == 0){
			if(IFLY.pay.remainingBalance == 0){
					var call = 'method=process_sale&controller=siriusware&format=json&account=&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id;
				$.each(IFLY.pay.paymentSummary, function(index, value){
					var appendCall = ''
				//	$this = $(value);
				//	var $form = $this.serializeObject();
					var $form = value;
					var cell_phone = $form.cell_phone1+'-'+$form.cell_phone2+'-'+$form.cell_phone3;
						
				//	'method=process_sale&controller=siriusware&format=json&account=&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&amount_1='+urlParams.cart_total+'&cell_phone_1='+cell_phone+'&first_name_1='+$formObj.first_name+'&last_name_1='+$formObj.last_name+'&address_1='+$formObj.address+'&zip_1='+$formObj.zip+'&e_mail_1='+$formObj.e_mail+'&credit_card_no_1='+$formObj.credit_card_no+'&exp_month_1='+$('#process_sale select#month :selected').val() +'&exp_year_1='+$('#process_sale select#year :selected').val() +'&cvv2_1='+$formObj.cvv2	
						
					appendCall +='&amount_'+(index+1)+'='+$form.amount+'&first_name_'+(index+1)+'='+$form.first_name+'&last_name_'+(index+1)+'='+$form.last_name+'&address_'+(index+1)+'='+$form.address+'&zip_'+(index+1)+'='+$form.zip+'&e_mail_'+(index+1)+'='+$form.e_mail+'&cell_phone_'+(index+1)+'='+cell_phone;
					
					if($form.credit_card_no != null && $form.credit_card_no != '') {
						appendCall += '&credit_card_no_'+(index+1)+'='+$form.credit_card_no +'&cvv2_'+(index+1)+'='+$form.cvv2
					}else {
						appendCall += '&account_'+(index+1)+'='+$form.gift_cert;
					}
					call += appendCall;
				});
				var payPadding='';
				
				switch (IFLY.pay.paymentSummary.length) {
					case 1:
						payPadding = '&amount_2=&first_name_2=&last_name_2=&address_2=&zip_2=&e_mail_2=&cell_phone_2=&credit_card_no_2=&exp_month_2=&exp_year_2=&cvv2_2=&amount_3=&first_name_3=&last_name_3=&address_3=&zip_3=&e_mail_3=&cell_phone_3=&credit_card_no_3=&exp_month_3=&exp_year_3=&cvv2_3=&amount_4=&first_name_4=&last_name_4=&address_4=&zip_4=&e_mail_4=&cell_phone_4=&credit_card_no_4=&exp_month_4=&exp_year_4=&cvv2_4=&zip=';
					break;
					case 2:
						payPadding = '&amount_3=&first_name_3=&last_name_3=&address_3=&zip_3=&e_mail_3=&cell_phone_3=&credit_card_no_3=&exp_month_3=&exp_year_3=&cvv2_3=&amount_4=&first_name_4=&last_name_4=&address_4=&zip_4=&e_mail_4=&cell_phone_4=&credit_card_no_4=&exp_month_4=&exp_year_4=&cvv2_4=&zip=';
					break;
					case 3:
						payPadding = '&amount_4=&first_name_4=&last_name_4=&address_4=&zip_4=&e_mail_4=&cell_phone_4=&credit_card_no_4=&exp_month_4=&exp_year_4=&cvv2_4=&zip=';
					break;
					
				}
				call += payPadding;
				
				console.log('call',call);
				IFLY.booking.callApi(call, IFLY.pay.sale_processed,null,null);
				$('.sale-submit').html(IFLY.showLoader());

				}
		
				return false;
		});
		showPaymentSummary();
		IFLY.pay.updateMultiTotal();
	
	},
	initGC: function() {
		$('#process_sale').not('.pay-options.disabled').data('validator', null).validate({
		   rules: {
			 first_name: "required",
			 last_name: "required",
			 address: "required",
			 zip: "required",
			 cell_phone: "required",
			 e_mail: {
			   required: true,
			   email: true
			 }
		   },
		   messages: {
			 first_name: "First name is needed",
			 last_name: "Last name is needed",
			 address: "Address is needed",
			 zip: "required",
			 cell_phone: "A phone number is needed",
			 e_mail: {
			   required: "A valid email address is needed"
			 }
		   }
		});
		$('#process_sale').unbind('submit').submit(function(e) {
			
			if($(this).valid()){
				$form = $('#process_sale');
				$('.disabled').remove();
				$inputs = $form.find("input");
				//serializedData = $form.serialize();
				
				var $formObj = $inputs.serializeObject();
				//$('#process_sale select#month :selected').val();
				
				if(!$formObj.credit_card_no) $formObj.credit_card_no = '';
			
				var cell_phone = $formObj.cell_phone1+'-'+$formObj.cell_phone2+'-'+$formObj.cell_phone3;
				
//				if(urlParams.voucher_number) {
//					$formObj.credit_card_no = urlParams.voucher_number;
//				}
				
				var processcall='method=process_sale&controller=siriusware&format=json&account=&tunnel='+urlParams.tunnel+'&wwsale_id='+urlParams.wwsale_id+'&amount_1='+urlParams.cart_total+'&cell_phone_1='+cell_phone+'&first_name_1='+$formObj.first_name+'&last_name_1='+$formObj.last_name+'&address_1='+$formObj.address+'&zip_1='+$formObj.zip+'&e_mail_1='+$formObj.e_mail+'&credit_card_no_1='+$formObj.credit_card_no+'&exp_month_1=&exp_year_1=&cvv2_1=&amount_2=&first_name_2=&last_name_2=&address_2=&zip_2=&e_mail_2=&cell_phone_2=&credit_card_no_2=&exp_month_2=&exp_year_2=&cvv2_2=&amount_3=&first_name_3=&last_name_3=&address_3=&zip_3=&e_mail_3=&cell_phone_3=&credit_card_no_3=&exp_month_3=&exp_year_3=&cvv2_3=&amount_4=&first_name_4=&last_name_4=&address_4=&zip_4=&e_mail_4=&cell_phone_4=&credit_card_no_4=&exp_month_4=&exp_year_4=&cvv2_4=&zip=';
				
				console.log('call',processcall);
				function processError(response, textStatus, jqXHR) {
					console.log('processError response',response);
				}
				IFLY.booking.callApi(processcall, IFLY.pay.sale_processed,processError,null);
				$('.sale-submit').html(IFLY.showLoader());
			}
			return false;
//			e.preventDefault();
		});
		
	},
	initCConfile: function() {
		$('#process_sale').not('.pay-options.disabled').data('validator', null).validate({
		   rules: {
			 first_name: "required",
			 last_name: "required",
			 address: "required",
			 zip: "required",
			 cell_phone1: "required",
			 cell_phone2: "required",
			 cell_phone3: "required",
			 e_mail: {
			   required: true,
			   email: true
			 }
		   },
		   messages: {
			 first_name: "First name is required",
			 last_name: "Last name is required",
			 address: "Address is required",
			 zip: "required",
			 cell_phone: "A phone number is needed",
			 e_mail: {
			   required: "A valid email address is needed"
			 }
		   }
		});
	},
	initAccount: function() {
		$('#process_sale').not('.pay-options.disabled').data('validator', null).validate({
		   rules: {
			 first_name: "required",
			 last_name: "required",
			 address: "required",
			 zip: "required",
			 cell_phone1: "required",
			 cell_phone2: "required",
			 cell_phone3: "required",
			 e_mail: {
			   required: true,
			   email: true
			 }
		   },
		   messages: {
			 first_name: "First name is required",
			 last_name: "Last name is required",
			 address: "Address is required",
			 zip: "required",
			 cell_phone1: "required",
			 cell_phone2: "required",
			 cell_phone3: "required",
			 e_mail: {
			   required: "A valid email address is needed"
			 }
		   }
		});
	},
	initCCselector: function() {
		// credit card colorization
		// capture current text field content
		var content = $('.pay-fields input[name="credit_card_no"]').val();
		// define event to check for highlighting
		$('.pay-fields input[name="credit_card_no"]').unbind('keyup').keyup(function() { 
			// if the content has changed
			if ($('.pay-fields input[name="credit_card_no"]').val() != content) {
				// reset the content var to current input value
				content = $('.pay-fields input[name="credit_card_no"]').val();
				// if the cc number currently has less than three digits
				if ($('.pay-fields input[name="credit_card_no"]').val().length < 3) {
					// set all credit card images to grey
					$("span.amex").css('background-position','-76px -175px');
					$("span.visa").css('background-position','0px -175px');
					$("span.mastercard").css('background-position','-38px -175px');
					$("span.discover").css('background-position','-113px -175px');
					$("span.iflycard").css('background-position','-152px -175px');
				}
				// if the cc num currently has 3 or more chars
				if ($('.pay-fields input[name="credit_card_no"]').val().length >= 3) {
					// set them all to grey, before colorizing correct one
					$('label.cc span').each(function() {
						//$(this).css('background-position','0px -152px');
					});
					var firstThree = $('.pay-fields input[name="credit_card_no"]').val().substring(0,3);
					switch(true) {
						// in case it is American Express
						case ((firstThree >= "300") && (content <= "399")) :
							$("span.amex").css('background-position','-76px -152px');
							break;
						// in case it is Visa
						case ((firstThree >= "400") && (content <= "499")) :
							$("span.visa").css('background-position','0px -152px');
							break;
						// in case it is MasterCard
						case ((firstThree >= "510") && (content <= "559")) :
							$("span.mastercard").css('background-position','-38px -152px');
							break;
						// in case it is Discover
						case (firstThree == "601") :
							$("span.discover").css('background-position','-113px -152px');
							break;
						// in case it is Siriusware Debitware
						case ((firstThree == "603") || (firstThree == "561")) :
							$("span.iflycard").css('background-position','-152px -152px');
							break;
						default :
							break;
					}
				}
			}
		});
	}
}
IFLY.findLocation = {
	tunnelLocatorCurrentBn:'',
	tunnelLocatorLocations:[],
	tunnelLocatorAlphabetical:'',
	tunnelLocatorCurrent:'',
	sortByLocation: function() {

	 $('.tunnel-sort').html(IFLY.findLocation.tunnelLocatorAlphabetical);
//			IFLY.locationHandler.geoip(null,sortByIP,IFLY.locationHandler.onError);
//			function sortByIP(rs, ip) {
//				if ((rs.latitude != 0) && (rs.longitude != 0)) {
//					var lat = new Number(rs.latitude);
//					var lon = new Number(rs.longitude);
//					
//					var tunnelOutput = IFLY.locationHandler.sortTunnels({latitude:lat,longitude:lon},IFLY.findLocation.tunnelLocatorLocations);
//					
//					// console.log('rs',rs,'IFLY.findLocation.tunnelLocatorLocations',IFLY.findLocation.tunnelLocatorLocations);
//					//console.log('tunnelOutput',tunnelOutput);
//					
//					$.each(tunnelOutput, function(index,value) {
//						var outputHTML = '<section class="clearfix">'+tunnelOutput[index].html+'</section>';
//						$('.tunnel-sort').prepend(outputHTML)
//					});
//					$.uniform.update('.tunnel-sort');
//					
//				}
//			}
	},
	sortByAlpha: function() {
		function compare(a,b) {
		  if (a.title.toUpperCase() < b.title.toUpperCase())
			 return -1;
		  if (a.title.toUpperCase() > b.title.toUpperCase())
			return 1;
		  return 0;
		}
		IFLY.findLocation.tunnelLocatorLocations.sort(compare);
		// console.log('SORTED IFLY.findLocation.tunnelLocatorLocations', IFLY.findLocation.tunnelLocatorLocations);
		
		$('.tunnel-sort').html('&nbsp;')
		$.each(IFLY.findLocation.tunnelLocatorLocations, function(index,value) {
			var outputHTML = '<section class="clearfix">'+IFLY.findLocation.tunnelLocatorLocations[index].html+'</section>';
			$('.tunnel-sort').append(outputHTML)
		});
		$.uniform.update('.tunnel-sort');

	},
	tunnelLocatorBookingInit: function() {
		
		IFLY.findLocation.tunnelLocatorAlphabetical = $('.tunnel-sort').html();
		$locations = $('.tunnel-sort section.clearfix');
		//////////////////// DISABLING FUNCTIONALITY UNTIL COMPLETED AND UNTIL ADDITIONAL TUNNELS LAUNCH	
		$('#uniform-tunnel-order').css('display','none');
		///////////////////
	
	//	$('.tunnel-sort').html('&nbsp;')
		IFLY.calendarWidget.getClosedDates();
		$('#tunnel-booking').datepicker({
				dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
				showButtonPanel: false,
				buttonImageOnly:false,
				beforeShowDay: IFLY.calendarWidget.unavailable,
				closeText: "close",
				onSelect: function(e) {
					//console.log('e: '+e);
					$(IFLY.findLocation.tunnelLocatorCurrentBn).html(e)
					$(this).hide();	
					IFLY.booking.checkForm('step2',null);
				}
			});

			$('#tunnel-booking').hide();

		$.each($locations, function(index, value) {
			var obj = {};
			//var calInitDate = 0;
			var latlong = $(this).find('article').data('latlong').split(',');
			obj.lat = latlong[0];
			obj.lon = latlong[1];
			$(this).find('select#flyer-type, select#flyer_num').change(function() {
				IFLY.booking.checkForm('tunnel-locator', $(this));
			});
			
			obj.title = $(this).find('article h3').html();
			
//			var selTunnelInfo = IFLY.locationHandler.getLocationInfoByTitle($(this).find('article h3').html());
//			console.log('selTunnelInfo',selTunnelInfo);
			
		//	if(obj.title != 'iFLY Austin' && obj.title != 'iFLY Orlando') {
			if(true) {
				//REMOVE THIS AFTER OTHER TUNNEL SITES ARE LAUNCHED
				$(this).find('form.tunnel-locator').css('display','none');
			}
			$(this).find('a.btn.date').click(function(e) {
				$('#tunnel-booking').css('top',$(this).offset().top-340);
					IFLY.findLocation.tunnelLocatorCurrentBn = $(this).find('span');
					$.datepicker._clearDate('#tunnel-booking');
					if(obj.title == 'iFLY Austin') {
						$('#tunnel-booking').datepicker("option", "minDate", new Date("January 15, 2013"));
						console.log('obj.title',obj.title);
					} else {
						$('#tunnel-booking').datepicker("option", "minDate", 0);
					}
					$('#tunnel-booking').show();
					e.preventDefault();
					IFLY.booking.checkForm('tunnel-locator', $(this));
			});
			obj.html = $(this).html();
			IFLY.findLocation.tunnelLocatorLocations.push(obj);
		});
//		console.log('IFLY.findLocation.tunnelLocatorLocation',IFLY.findLocation.tunnelLocatorLocations);
//		IFLY.findLocation.sortByLocation();
		
		$('select#tunnel-order').change(function() {
			var tunnelOrd = $(this).find('option:selected').val();
			(tunnelOrd =='closest') ? IFLY.findLocation.sortByLocation() : IFLY.findLocation.sortByAlpha();
			//console.log('tunnelOrd',tunnelOrd);
		})
		
		 //var  calInitDate = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : 0;
//			$('form.tunnel-locator a.btn.date').each(function() {
//				$(this).click(function(e) {
//					$('#tunnel-booking').css('top',$(this).offset().top-340);
//						IFLY.findLocation.tunnelLocatorCurrentBn = $(this).find('span');
//						var  calInitDate = (IFLY.current_tunnel =='aus') ? new Date("January 15, 2013") : 0;
//						$("#calendar-booking").datepicker("option", "minDate", calInitDate);
//
//						$('#tunnel-booking').show();
//						e.preventDefault(); 
//				});
//			});
			$('form.tunnel-locator').each(function(index) {
				$(this).unbind('submit').submit(function() {
					if(!$(this).find('input.find-flights').hasClass('disabled')) {
						var selTunnelInfo = IFLY.locationHandler.getLocationInfoByTitle(tunnelResults[index].title);
						//var localSite = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com';
						$inputs = $form.find("select");
						var $formObj = $inputs.serializeObject();

					//	var tunnelResults = IFLY.locationHandler.sortTunnels({latitude:IFLY.findLocation.tunnelLocatorLocations[index].lat,longitude:IFLY.findLocation.tunnelLocatorLocations[index].lon},IFLY.tunnels);
						urlParams.tunnel = selTunnelInfo.code;
						urlParams.flyer_date = $(this).find('btn.date span').html();
						urlParams.tunnel_name = selTunnelInfo.title;

						urlParams.flyer_num=$formObj.flyer_num;
						urlParams.code=$formObj.flyer_num;
						urlParams.flyer_type_name=$(this).find('select[name="code"] option:selected').html();

						var serializedData = $.param(urlParams);
						
						var dURL = 'http://'+selTunnelInfo.subdomain+'.iflyworld.com'+IFLY.booking.init.step3path + '?'+serializedData;
//						console.log('dURL',dURL);
						window.location = dURL;
					}
						//e.preventDefault(); 
						return false;
				});
			});
			
		
	}

};
var urlParams = {};
var sessionData, sessionStatus;

$(document).ready(function() {
	
	//SHOWS CART BUTTON IF SALE ID IS PRESENT
	if($('#header nav.add-nav').length>0 && cookieHandler.read('wwsale_id') !=null) {
			var full = window.location.host;
			var dURL = 'https://'+full + '/book-now/booking-step5';
			$('#header nav.add-nav ul li:first-child').html('<a href="'+dURL+'">View Cart</a>');
	} else {
			//$('#header nav.add-nav ul li:first-child').html('<a href="/book-now/">Book Now</a><');
	}
	
	if($('body.gifting.step1 .check-balance a.launch').length>0){
		IFLY.booking.getGiftBalance('.check-balance a.launch');
	}
	
	$("form#lgn input[type=text]").live().click(function() {
		$(this).select();
	});

	if($('body.booking').length>0) {
//		getParams();
	}
	//TEMP
	if ($('body.booking.step1').length > 0) {
//		initPreso();
		//cookieHandler.remove('wwsale_id');
		IFLY.booking.init.step1();
	}
	if ($('body.booking.step2')) {
	//	IFLY.booking.init.step2();
	}
	if ($('body.gifting.step2').length > 0) {
		//IFLY.booking.init.step2gifting();
	}
	if ($('body.gifting.step3').length > 0) {
//		IFLY.booking.init.step3gifting();
	}
//	if ($('body.booking.step5').length > 0) {
//		IFLY.booking.init.step5();
//	}

//	if ($('body.booking.step6').length > 0) {
//		IFLY.booking.init.step6();
//	}
//	if ($('body.booking.step7').length > 0) {
	//	IFLY.booking.init.step7();
//	}
	if($('body.gifting.redeem').length >0) {
		IFLY.booking.init.redeemGiftCard();
	}
	if($('body.tunnel-locator').length >0) {
		IFLY.findLocation.tunnelLocatorBookingInit();
	}
	
	if($('body.booking a.btn-prev').length >0) {
		$('a.btn-prev').unbind('click').click(function(e) {
			if(!$(this).hasClass('disabled')) {
				//console.log('back-clicked');
				window.history.back();	
				e.preventDefault();
			}
		});
	}
});
