var pages = 1;

/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
 IFLY.videotestimonials = {
	init: function() {
        
			var host = 'http://dev.iflyworld.com';
			//console.log('current host:' + host);
			var tunnel = null;
			var default_tunnel = 'ifo';
			
			var maxResults = 12;
			var resultCount = 0;
			var page = 1;
			var pages = 1;
			$('.total').html(pages);
			hideSocial();
		
            // http://testimonials.iflyworld.com/video-reviews/index.php?videoID=x0_oVgVzZOA
            var params = vt_getUrlVars();
            var vidID = params['videoID'];
            console.log('incoming POST vidID:' + vidID);
            tunnel = params['tunnel'];
            console.log('incoming POST tunnel:' + tunnel);
            if (tunnel != undefined) {
                $("select.locations").val(tunnel).change();
            }
			
			
            // if the user changes tunnels, stop videos and get results for that tunnel...
            $("select.locations").change(function(e) {
                tunnel = $("select.locations").val();
                // stop the video player if it was running
                player = document.getElementById("player");	
                if (player != undefined && isObject("player")) {
                    player.stopVideo();
                }
                page = 1;
                loadResultsPage(host, tunnel, page);
            });

			
            if (vidID == undefined) {
                console.log('no incoming video url, getting content list');
                var selected_tunnel = $("select.locations").val();
                if (selected_tunnel == 'unselected' && tunnel == undefined) {
                    tunnel = chooseDefaultTunnel(default_tunnel);
                }
                loadResultsPage(host, tunnel, 1);

                $('#video_player_wrapper').hide();				   
                $('#search_results').fadeIn(500);
                $('#search_results_list_column').show();
                // show pagination only if more than 1 page
                if ($('.total').html() > 1) {
                    $('#page_list').show();
                    $('.pagination').fadeIn(500);
                }
                //$('#vt_social').show();
                $('#close_button_div').hide();
                hideSocial();
                //$('#like_screen').hide();

            } else {
                console.log('a video link was given:' + vidID);
                $('#search_results').hide();
                $('#search_results_list_column').hide();
                $('#player').show();				   
                $('#video_player_wrapper').show(); 	
                $('#page_list').hide();
                $('.pagination').hide();
                $('#like_screen').hide();
                setVideo(vidID);
            }

            // --- cancel the video, hide video and social, show search list and pagination ---
            $('#close_button').click(function(){
                var close_button_value = $('#close_button').val();
                console.log('closing... button currently says:' + close_button_value);
                player = document.getElementById("player");	
                if (player != undefined && isObject("player")) {
                    player.stopVideo();
                }
                var tunnel = $("select.locations").val();
                if (tunnel == 'unselected') {
                    chooseDefaultTunnel(default_tunnel);
                    //$("select.locations").val('ifo').change();
                }
                //loadResultsPage(host, tunnel, 1);
                console.log('hiding video player...');
                $('#video_player_wrapper').hide();				   
                $('#like_screen').hide();
                $('#close_button_div').hide();
                $('#player').hide();				   
                console.log('done hiding video player...');

                if (tunnel == 'unselected') {
                    tunnel = chooseDefaultTunnel(default_tunnel);
                    // if no tunnel, use ifo
                    //$("select.locations").val('ifo').change();
                    $("select.locations").val('seat').change();
                    var tunnel = $("select.locations").val();
                    console.log('forced tunnel to:' + tunnel);
                }
                loadResultsPage(host, tunnel, 1);

                console.log('hiding the video player');
                $('#video_player_wrapper').hide();				   
                
                console.log('showing results list...');
                $('#search_results_list_column').show();
                $('#search_results').fadeIn(500);
                // show the page navigation if there is at least 1 page
                if ($('.total').html() >= 2) {
                    $('.pagination').fadeIn(500);
                    $('#page_list').fadeIn(500);
                }
            });
	
            $('#next_page').click( function() {
                var tunnel = $("select.locations").val();
                page = page + 1;
                if (page <= $('.total').html()) {
                    loadResultsPage(host, tunnel, page);
                }
            });

            $('#prev_page').click( function() {
                var tunnel = $("select.locations").val();
                page = page - 1;
                if (page >= 1) {
                    loadResultsPage(host, tunnel, page);
                }
            });
		
	},
	initPagination: function(data,tDiv) {
				IFLY.photovid.clearItems(tDiv);
				if (data.length > IFLY.photovid.thumbsPerPage) {
					$('.footer-nav a.more').css('display','inline-block');	
					IFLY.photovid.totalThumbs = data.length;
					IFLY.photovid.lastThumb = 0;
					
					$('.footer-nav a.more').unbind('click').click(function(e) {
						addMore(tDiv);
						e.preventDefault();
					});

					$(window).scroll(function() {
					   if($(window).scrollTop() + $(window).height() == $(document).height()) {
							addMore(tDiv);
					   }
					});
				
					function addMore(disDiv){
						var nextSet = IFLY.photovid.lastThumb + IFLY.photovid.thumbsPerPage;
						var maxNum = (nextSet <= IFLY.photovid.totalThumbs) ? nextSet : IFLY.photovid.totalThumbs;
						var thumbSet = [];
						for(i=IFLY.photovid.lastThumb; i<maxNum; i++) {
							thumbSet.push(IFLY.photovid.session[i]);
						}
						//console.log('IFLY.photovid.lastThumb', IFLY.photovid.lastThumb,'maxNum',maxNum,'thumbSet',thumbSet.length);
						IFLY.photovid.lastThumb = maxNum;
						IFLY.photovid.displayThumbs(disDiv,thumbSet);
						if(IFLY.photovid.lastThumb == IFLY.photovid.totalThumbs) $('.footer-nav a.more').css('display','none');
					} 
					addMore(tDiv);
					
					// return thumbSet;
				} else {
					$('.footer-nav a.more').css('display','none');	
						IFLY.photovid.displayThumbs(tDiv,data);
					// return data;
			}
		}
 }
  
  
function chooseDefaultTunnel(default_tunnel) {
    $("select.locations").val(default_tunnel).change();
    var selected_tunnel = $("select.locations").val();
    console.log('forced tunnel to:' + selected_tunnel);
    tunnel = selected_tunnel;
    
    return default_tunnel;
}  


function hideSocial() {
    console.log('HIDESOCIAL CALLED');
    $('#vt_social').css('left','-3000px');
    //$('#vt_social').hide();
}  

function showSocial() {
    console.log('SHOWSOCIAL CALLED');
    $('#vt_social').css('left','0px');
    //$('#vt_social').show();
}
  
function loadResultsPage(host, tunnel, page) {
    var maxResults = 12;
    var resultCount = 0;
    //console.log('loadResultsPage called with page:' + page);

    $.post(host + "/php-lib/videos-by-tags.php", {
        queryType: "all", 
        //maxResults: maxResults , 
        page: page,
        tunnel: tunnel
    },  function(response) {
        // data contains a count and the array of results...
        var response = $.parseJSON(response);
        resultCount = response.count;
        pages = Math.floor(resultCount/maxResults);
        $('.active').html(page);
        $('.total').html(pages);
        var html ="<ul style='list-style-type:none'>";
        $.each(response.media, function(key, value) {  
            if(typeof value.videoThumb != 'undefined'){
                videoID = value['videoID'];
                title = value['videoTitle'];
                title = title.replace(/ Flight Video /,'');
                title = title.replace(/\'s/,'');
                url = value['videoURL']; 
                description = value['videoDescription'];
                thumb = value['videoThumb'];
                if (title != "iFLY Austin August") {		
                    html = html + "<li class='vt_channel_item' onClick='setVideo(\"" + videoID + "\")'>";
                    html = html + "<div class='vt_channel_title'>" + title + "</div>";
                    html = html + "<div class='vt_channel_photo vt_channel_photo'>";
                    //html = html + "<img alt='thumb' width='120' height='90' src='" + thumb + "' />";
                    html = html + "<div alt='thumb' width='120' height='90' style='min-width:120px; min-height:90px; background-size:120px 90px; background-image:url(" + thumb + "); '  ></div>";
                    html = html + "</div>";
                    html = html + "</li>";
                }
            }
        });

        html = html + "</ul>";
        // showing search results
        $('#search_results').fadeIn(500);
        $('#search_results_video_list').html(html);
        $('#search_results_list_column').fadeIn(500);
        $('#close_button_div').hide();
        hideSocial();
        
        // hide video player
        $('#video_player_wrapper').hide();				   
        // show page list only if greater than 1 page
        if ($('.total').html() > 1) {
            $('#page_list').show();
        } else {
            $('#page_list').hide();
        }
    });
}
  
// default video (joe jennings) -- deprecated
function gup( name )
{
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    // explode the URL
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( window.location.href );
    if( results == null )
        return "7G-QD2RO28c";
    else
        return results[1];
}
            
// load/que a video
function setVideo(id){
    console.log('clicked on setVideo for id:' + id);
    $('#page_list').hide();
    $('#close_button_div').show();
    videoID = id;
    //Check for existing SWF
    if(isObject("player")){
        //replace object/element with a new div					 
        replaceSwfWithEmptyDiv("player");
    }

    $('#search_results_list_column').hide();				   
    $('#video_player_wrapper').show();				   
    showSocial();

    $('#like_screen').hide();

    var params = {
        allowScriptAccess: "always"
    };
    var atts = {
        id: "player"
    };
    swfobject.embedSWF("http://www.youtube.com/v/" + id + "?enablejsapi=1&playerapiid=ytplayer&version=3&hl=en_US&theme=light&color=red&hd=1&rel=0&showinfo=0&autoplay=1&autohide=1&loop=0&modestbranding=1",
        "player", "840", "500", "8", null, null, params, atts);
}
			

//function onYouTubePlayerReady(playerId) {
//    console.log('onYouTubePlayerReady  - with playerId:' + playerId);
//    $('#like_screen').hide();
//    $('#close_button_div').show();
//    player = document.getElementById("player");	
//    player.addEventListener("onStateChange", "onytplayerStateChange");
//}
			
function onytplayerStateChange(newState) {			   			   	 
    console.log('*** onytplayerStateChange-newstate:' + newState);
    // if end of video...
    if (newState == 0){
        $('#player').hide();				   
        $('#like_screen').fadeIn(500);
        $('#close_button_div').hide();
        $('#page_list').show();
        //$('#fb_list').show();
        //$('#vt_social').show();
        showSocial();
        //$('#vt_social').fadeIn(500);
    }
    if (newState == 1 || newState == 3){
        $('#like_screen').hide();
        $('#close_button_div').show();
        $('#page_list').hide();
        //$('#fb_list').show();
        //$('#vt_social').show();
        showSocial();
        //$('#player').hide();				   
        //$('#like_screen').fadeIn(500);
        //$('#fb_list').fadeIn(500);
    }
    
}
			
function playVideo(){
    //$('#vt_social').show();
    showSocial();
    player.playVideo();
}
			
//Support function: checks to see if target
//element is an object or embed element
function isObject(targetID){
    var isFound = false;
    var el = document.getElementById(targetID);
    if(el && (el.nodeName === "OBJECT" || el.nodeName === "EMBED")){
        isFound = true;
    }
    return isFound;
}
			 
//Support function: creates an empty
//element to replace embedded SWF object
function replaceSwfWithEmptyDiv(targetID){
    var el = document.getElementById(targetID);
    if(el){
        var div = document.createElement("div");
        el.parentNode.insertBefore(div, el);
			 
        //Remove the SWF
        swfobject.removeSWF(targetID);
			 
        //Give the new DIV the old element's ID
        div.setAttribute("id", targetID);
    }
}
		
function sendFacebok(){
    window.open('https://www.facebook.com/sharer/sharer.php?t=Check my video&u=' + window.location + '')
}


function sendTwitter(){
    window.open('http://twitter.com/home?status=Check my video: ' + window.location )
}
  
function vt_getUrlVars(){
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
$(document).ready(function() {
	if($('body.video-testimonials').length >0) {
		IFLY.videotestimonials.init(); 
	}
});
