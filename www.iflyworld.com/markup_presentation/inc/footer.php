	<footer id="footer">
		<div class="block-holder">
			<div class="block">
				<h4>What is iFLY</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">How it Works</a></li>
						<li><a href="#">About iFLY</a></li>
						<li><a href="#">Video Testimonials</a></li>
						<li><a href="#">Ratings &amp; Reviews</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>Shop Now</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Overview</a></li>
						<li><a href="#">First Time Flyers</a></li>
						<li><a href="#">Return Flyers</a></li>
						<li><a href="#">Experience Flyers</a></li>
						<li><a href="#">Group or Party</a></li>
						<li><a href="#">Buy a Gift Card</a></li>
						<li><a href="#">Redeem a Gift Card</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>Pics &amp; Vids</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Find Your Photos &amp; Videos</a></li>
						<li><a href="#">iFLY Photos &amp; Videos</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>My iFLY</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">My iFLY Page</a></li>
						<li><a href="#">My Media</a></li>
						<li><a href="#">My Profile</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>iFLY Franchises </h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Start an iFLY Franchise</a></li>
						<li><a href="#">Existing Franchisees</a></li>
					</ul>
				</nav>
			</div>
			<div class="block">
				<h4>I.B.A.</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">About the Sport</a></li>
						<li><a href="#">About the IBA</a></li>
					</ul>
				</nav>
			</div>
			<div class="block last">
				<h4>Legal</h4>
				<nav class="footer-list">
					<ul>
						<li><a href="#">Waiver</a></li>
						<li><a href="#">Terms</a></li>
						<li><a href="#">Privacy</a></li>
					</ul>
				</nav>
			</div>
		</div>
		<p>Copyright &copy; 2012 SkyVenture. Vivamus adipiscing ultrices lacus, ut feugiat nunc adipiscing id. </p>
	</footer>
	